/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('CpHome.Application', {
    extend: 'Ext.app.Application',
    name: 'CpHome',
    
//    mainView: 'CpHome.view.HomeView',

    controllers: [
        'AppMainController',
        'CPLogin.controller.CPLoginController',
    ],
    
    stores: [
        'NavigationStore',
        'PanelStore',
    ],
    
    model: [
        'AppModel',
    ],
    
    requires: [
        'Ext.plugin.Viewport',
        'Ext.window.MessageBox',
        'CpHome.view.HomeView',
        'CpHome.view.HomePanel',
        'Ext.window.Toast'
    ],
      
    refs: [{
            ref: 'UserLoginWindowView',
            selector: 'userLoginWindowView'
    }],
    launch: function () {
        var me = this;
        
        MineralPro.config.Runtime.appName = 'CPHome'   
        MineralPro.config.RuntimeUtility.disableBackspace();
        MineralPro.config.RuntimeUtility.enablePasswordChecking();
        MineralPro.config.RuntimeUtility.ImplementVtypes();
//        MineralPro.config.RuntimeUtility.addThemeOptions();
        
        setTimeout(function () {
            Ext.globalEvents.fireEvent('checkUserSession');
            
            setTimeout(function () {
                
                if(MineralPro.config.Runtime.appraisalYear.length == 0){
                    me.getStore('MineralPro.store.AppraisalYearStore').load({
                        callback: function(){
                            var appYearStore = me.getStore('MineralPro.store.AppraisalYearStore')
                            var activeYearIndex = appYearStore.find('activeAppraisalYear', 'Y')
                            if(activeYearIndex != -1){
                                MineralPro.config.Runtime.appraisalYear = appYearStore.getAt(activeYearIndex).get('appraisalYear')
                            }else{
                                MineralPro.config.Runtime.appraisalYear = new Date().getFullYear();
                            }
                        }
                    })                  
                }   
                
                //me.getStore('MineralPro.store.AppraisalYearStore').load();               

            }, 200);
            
        }, 200);
        Ext.on('resize', function () {
            var windowLogin = me.getUserLoginWindowView();
            if (windowLogin) {
                windowLogin.center();
            }
        });
    },
//	mainView: 'MineralPro.view.MainViewPort'
});
