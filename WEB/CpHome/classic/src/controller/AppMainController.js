///**
// *Responsible for constructing the viewport and 
// *initializing the user information to the global variable
// */

Ext.define('CpHome.controller.AppMainController', {
    extend: 'Ext.app.Controller',

    init: function(){              
        var me=this;
        me.listen({
            global: {            
                loadViewPort: me.loadViewPort
            }            
        })               
    },
    
    /**
     * Load the viewport for user interaction with the system
     */
    loadViewPort: function(){
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppMainController loadViewPort.')     
                           
        Ext.create('CpHome.view.HomeView').show();

        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppMainController loadViewPort.')
    }
    
});
