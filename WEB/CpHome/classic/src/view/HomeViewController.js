Ext.define('CpHome.view.HomeViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.HomeViewController',
    onAfterRender: function (container) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering HomeViewController onAfterRender.')

        var me = this;
        var view = me.getView();
        view.down('#welcome_user').setText(MineralPro.config.Runtime.emailAddress)

        MineralPro.config.RuntimeUtility.DebugLog('Leaving HomeViewController onAfterRender.')
    }


});
