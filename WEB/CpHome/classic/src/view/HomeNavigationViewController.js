Ext.define('CpHome.view.HomeNavigationViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.HomeNavigationViewController',
    
    onAfterRender: function (tabPanel) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering HomeNavigationViewController onAfterRender.')

        var store = Ext.data.StoreManager.lookup('NavigationStore');
        var items = [];

        store.load({
            callback: function (records, operation, success) {
                var arrayIcon = new Array("", "f007", "f0c0", "f19c", "f0f7", "f0f2", "f0ea", "f235", "f275", "f05a");
            //    items.push({
            //         xtype: 'panel',
            //         title: 'Favorite',
            //         glyph: 'f115@FontAwesome',
            //         tabConfig: {
            //             xtype: 'tab',
            //             id: "tabFavorite",
            //             itemId: "tabFavorite",
            //             textAlign: 'left',
            //             listeners: {
            //                 'click': function (menu, e, item, eOpts) {
            //                     var win = window.open('../CpHome', '_blank');
            //                     win.focus();
            //                 }
            //             }
            //         }
            //     });
                // do something after the load finishes
                Ext.each(records, function (rec) {
                    items.push({
                        xtype: 'panel',
                        title: rec.get('displayName'),
                        glyph: arrayIcon[rec.get('idApp')] + '@FontAwesome',
                        tabConfig: {
                            xtype: 'tab',
                            id: "tab" + rec.get('idApp'),
                            itemId: "tab" + rec.get('idApp'),
                            textAlign: 'left',
                            listeners: {
                                'click': function (menu, e, item, eOpts) {				
									if (rec.get('url')==""){
										Ext.Msg.alert('Status', 'The '+rec.get('displayName')+' App is not yet available.');
									}
									else{
										var win = window.open('../' + rec.get('url'), '_blank');
										win.focus();
									}
                                }
                            }
                        }
                    });
                    // console.log(rec.get('appName'));
                });
 
                items.push({
                    xtype: 'panel',
                    title: 'About',
                    glyph: 'f059@FontAwesome',
                    tabConfig: {
                        xtype: 'tab',
                        id: "tabAbout",
                        itemId: "tabAbout",
                        textAlign: 'left',
                        listeners: {
                            'click': function (menu, e, item, eOpts) {
                                Ext.Msg.alert('About MineralPro', 'Version 1.0 <br> Copyright 2017. All right reserved.<br>');
                            }
                        }
                    }
                });
				
                tabPanel.add(items);
            }
        });
        MineralPro.config.RuntimeUtility.DebugLog('Leaving HomeNavigationViewController onAfterRender.')
    }


});
