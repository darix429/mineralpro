Ext.define('CpHome.view.HomePanel', {
    extend: 'Ext.panel.Panel',
    alias : 'widget.HomePanel',
    controller: 'HomePanelViewController',
    requires: [
        'CpHome.view.HomePanelViewController'
    ],
    layout: {
        type: 'vbox'
    },
    initComponent: function() {
        var me = this;
        
        me.items = [{
            itemId: 'card',
            xtype: 'container',
            autoScroll: true,
            layout: {
                type: 'table',
                columns: 5,
                tdAttrs: {style: 'padding: 10px;'}
            },
            listeners: {
                afterrender: 'onAfterRender'
            },
            plugins: 'responsive',
            responsiveConfig: {
                changeColums:{}
            },
            responsiveFormulas: {
                changeColums: function (context) {
                    var width = context.width;
                    if(me.items.items[0] != undefined){
                        var columns = me.items.items[0].layout.columns;
                        if (width <= 670 && width > 450){
                            me.items.items[0].layout.columns = 1;
                            return true;
                        }else if (width <= 890 && width > 670){
                            me.items.items[0].layout.columns = 2;
                            return true;
                        }else if (width <= 1110 && width > 890){
                            me.items.items[0].layout.columns = 3;
                            return true;
                        }else if (width <= 1330 && width > 1110){
                            me.items.items[0].layout.columns = 4;
                            return true;
                        }else if (width <= 1550 && width > 1330){
                            me.items.items[0].layout.columns = 5;
                            return true;
                        }else if (width <= 1770 && width > 1550){
                            me.items.items[0].layout.columns = 6;
                            return true;
                        }else if (width <= 1990 && width > 1770){
                            me.items.items[0].layout.columns = 7;
                            return true;
                        }
                    }
                }
            }
        }];
        this.callParent(arguments);
    },
});