Ext.define('CpHome.view.HomeNavigationView', {
    extend: 'Ext.tab.Panel',
    
    requires: ['CpHome.view.HomeNavigationViewController'],
    
    controller: 'HomeNavigationViewController',
    
    alias: 'widget.HomeNavigationView',
    itemId: 'navigationtab',
    activeTab: 0,
    tabPosition: 'left',
    tabRotation: 0,
    minTabWidth: 275,

    listeners: {
        afterrender: 'onAfterRender'
    }
});