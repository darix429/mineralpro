Ext.define('CpHome.view.HomePanelViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.HomePanelViewController',
    onAfterRender: function (container) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering HomePanelViewController onAfterRender.')

        var store = Ext.data.StoreManager.lookup('PanelStore');
        var items = [];

        store.load({
            callback: function (records, operation, success) {
                // do something after the load finishes
                Ext.each(records, function (rec) {
                    items.push({
                        xtype: 'panel',
                        width: 200,
                        height: 240,
						title: rec.get('displayName'),
                        html: '<center><img src="' + rec.get('imageUrl') + '" style="max-width:200px; min-height: 133px; cursor: pointer;"/></center>',
                        autoEl: {
							title: 'Click '+rec.get('displayName') +' to open a New Tab, Shift + Click to open New Browser',
                        },
						listeners: {
						   'render': function(panel) {
							   panel.body.on('click', function() {
									if (rec.get('url')==""){
										Ext.Msg.alert('Status', 'The '+rec.get('displayName')+' App is not yet available.');
									}
									else{
										var win = window.open('../' + rec.get('url'), '_blank');
										win.focus();
									}								   
							   });
							}
						}
                    });
//                    console.log(rec.get('appName'));
                });
                items.push({
                    xtype: 'panel',
                    width: 200,
                    height: 240,
					title: 'About Minerals Pro',
                    html: '<center><img src="images/circle/about.png" style="max-width:200px; min-height: 133px; cursor: pointer;"/></center>',
					autoEl: {
						title: 'About MineralPro',
					},
					listeners: {
					   'render': function(panel) {
						   panel.body.on('click', function() {
								Ext.Msg.alert('About MineralPro', 'Version 1.0 <br> Copyright 2017. All right reserved.<br>');
							   
						   });
						}
					}
                });
                container.add(items);


            }
        });
        MineralPro.config.RuntimeUtility.DebugLog('Leaving HomePanelViewController onAfterRender.')
    }

});
