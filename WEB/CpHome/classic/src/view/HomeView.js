/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('CpHome.view.HomeView', {
       extend : 'Ext.container.Viewport',
       requires: ['CpHome.view.HomeViewController'],
       controller: 'HomeViewController',
	   
	   
	   
    requires : ['Ext.layout.container.Border'],
    layout: 'border',
    defaults: {
        collapsible: true,
        split: true
    },
	
    items: [{
        region: 'north',
		height: 110,
		header: false,
		title: 'MineralPro',
		layout: 'hbox',
		width: '100%',
		
		bodyStyle: {
			background: 'url(../resources/images/mineralpro-banner.png)',
			backgroundSize: '50% 100%',
			backgroundRepeat: 'no-repeat',
			backgroundPosition: 'top right',
			backgroundColor: '#499ceb'
		},
	    	items:[{
                xtype: 'container',
                width: '20%',
				backgroundColor: '#157fcc',
                items: {
                    xtype: 'image',
                    src: '../resources/images/mineralpro-logo.png',
                    margin: '20 0 0 20',
                    height: 80                        
                },	
				
			},{
                xtype: 'panel',
                width: '80%',
                layout: 'left',
				
				margin: '80 30 0 0',
	            layout: {
	            	type: 'hbox',
	            	align: 'bottom',
					pack: 'end'
	            },
				bodyStyle: {
					backgroundColor: 'transparent',
					color: '#FFFFFF',
				},				

				items:[{
					xtype : 'image',
					glyph : 'xf007@FontAwesome',
			        itemId : 'iconuser',
			        id : 'iconuser',
			        padding: 5,
                    docked: 'right',
                    style: {
                        background: '#135895'
                    },
				},{
			    	xtype: 'label',
		            html: 'team_bp4@tingkersoft.com',
			    	itemId: 'welcome_user',
                    padding: 5,
                    style: {
                        background: '#135895'
                    },
			    	cls: 'user-font'
			    },{
			    	xtype: 'button',
			    	glyph : 'xf08b@FontAwesome',
			    	text: 'Logout',
			    	tooltip: 'Logout User',
			    	border:false,
			    	padding: 5,
			    	docked: 'right',
					listeners: {
						click: function (){
							Ext.globalEvents.fireEvent('userLogout')
						}
					}
		    	}]
			}],
			
			collapsible: false,
			split: true
    },{
        region : 'west',
        
        width : 200,
		itemId: 'pnlnavigation',
	    title: 'Menu',
	    glyph : 'xf0ca@FontAwesome',
	    layout: 'fit',
	    items: [{
	        xtype: 'HomeNavigationView',
	        width: 250
	    }],
/* 	    dockedItems: [{
	    	xtype: 'panel',
	    	dock: 'bottom',
	        bodyStyle: {
	            'background-color': '#404040'
	        },
	        items: [{
	            	xtype: 'displayfield',
	            	dock: 'bottom',
	            	value: '<p style="color: white; text-align: center; font-weight: bold; font-size: 12px;">For further support, email us at <a style="color: #668cff" href="mailto:team_bp4@tingkersoft.com?Subject=[CamaproHELP]" target="_top"">team_bp4@tingkersoft.com</a></p>',
	            	padding: '0 2 0 2',
					
	        }]
		}], */
    },{
        region : 'center',
       	items:[{
	    	xtype:'HomePanel',
			autoScroll: true,
	    }],
		layout: 'fit',
        collapsible: false 
    }],	
	
    listeners: {
       afterrender: 'onAfterRender'
    }
});