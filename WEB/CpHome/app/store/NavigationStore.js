Ext.define('CpHome.store.NavigationStore', {
    extend: 'MineralPro.store.BaseStore',
    model: 'CpHome.model.AppModel',
     proxy: {
        type: 'ajax',
        url: '/api/CPHome/GetNavigationTab',
        reader: {
            type: 'json'
        }
    },
        
    listeners:{
        beforeload: function(){
            Ext.getBody().mask('Loading...')              
        }, 

        load: function(store, records, successful, operation, eOpts ){
            Ext.getBody().unmask();                  
        }
    }
}); 
