Ext.define('CpHome.model.AppModel', {
    extend: 'MineralPro.model.BaseModel',
    fields: [
           {name: 'idApp', type: 'int'},
           {name: 'appName',  type: 'string'}]
});