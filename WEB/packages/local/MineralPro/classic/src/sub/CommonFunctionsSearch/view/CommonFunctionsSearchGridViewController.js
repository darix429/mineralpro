//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('MineralPro.sub.CommonFunctionsSearch.view.CommonFunctionsSearchGridViewController', {
    extend: 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.CommonFunctionsSearchGridViewController',
    onSearchDataGrid: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering CommonFunctionsSearchGridViewController onSearchDataGrid.')
        var me = this;
        var view = me.getView();
        view.down('#searchButtonItemId').disable(true);
        var store = view.getStore();
        var columns = view.getColumns();
        if (view.down('#searchTextFieldItemId').value.length > 0) {
            store.getProxy().extraParams = {
                searchOption: view.down('#searchTypeComboItemId').value,
                searchValue: view.down('#searchTextFieldItemId').value,
            }
            
            store.load();

//            store.load({
//                callback: function (records, operation, success, a) {
//
//                    Ext.each(columns, function (col) {
//                        if (col.dataIndex != 'fieldName') {
//                            view.headerCt.remove(col)
//                        }
//                    })
//
//                    var colArray = Ext.decode(operation._response.responseText).columnField.split(',')
//                    var colLength = view.columns.length;
//
//                    Ext.each(colArray, function (col) {
//                        col = col.trim();
//                        var column;
//                        if (MineralPro.config.Runtime.appName == 'Minerals'
//                            && (col == 'Lease #' || col == 'Owner #' || col == 'Econ Unit #')) {
//
//                                column = Ext.create('Ext.grid.column.Column', {
//                                    text: col,
//                                    dataIndex: col,
//                                    flex: 1,
//                                    renderer: function (value) {
//                                        return '<a href="javascript:void(0);" >' + value + '</a>';
//                                    },
//                                    listeners: {
//                                        click: function (cell, td, rowIndex, cellIndex, e, record, value, index) {
//                                            me.onOpenSelectedTabView(record, rowIndex, col);
//                                        }
//                                    }
//                                });
//                        } else {
//                            column = Ext.create('Ext.grid.column.Column', {
//                                text: col,
//                                dataIndex: col,
//                                flex: 1
//                            });
//                        }
//                        view.headerCt.insert(colLength, column);
//                        colLength++
//                    })
//                    view.getView().refresh();
//                    view.down('#searchButtonItemId').enable(true);
//                }
//            });
        } else {
            Ext.Msg.show({
                iconCls: MineralPro.config.Runtime.searchMsgErrorIcon,
                title: MineralPro.config.Runtime.searchMsgErrorTitle,
                msg: MineralPro.config.Runtime.searchMsgErrorMsg,
                buttons: Ext.Msg.OK,
                fn: function(){
                    view.down('#searchButtonItemId').enable(true);
                }
            });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CommonFunctionsSearchGridViewController onSearchDataGrid.')
    },
    /**
     * Clear value for all filter
     */
    onClearListFilter: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering CommonFunctionsSearchGridViewController onClearListFilter.')
        var me = this;
        var cols = me.getView().columns
        var view = me.getView();
        var store = view.getStore();
        store.getProxy().setExtraParams({});
        view.down().items.items[1].setValue('');
        view.down().items.items[2].setValue('');
        view.down().items.items[3].setValue('');
        Ext.each(cols, function (col) {
            if (col.filterElement)
                col.filterElement.setValue('');
        });
        store.reload();
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CommonFunctionsSearchGridViewController onClearListFilter.')
    },
    
     onOpenSelectedTabView: function (record, rowIndex, dataIndex) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsGridViewController onOpenSelectedTabView.')
        var me = this;
        var centerView = me.getView().up('centerView');
        var westView = centerView.up().down('westView');
        var favoriteTree = westView.down('#favoriteTreeId');
        var menuTree = westView.down('#menuTreeId');
        var dataTree=true;
        var index = 0;
        
        var functionName = '';
        var functionTab = '';
        var functionId = '';
        var functionDataIndex = '';
            
        if(dataIndex == 'Lease #'){
            functionName = 'Manage Lease';
            functionTab = 'MineralValuationManageLease';
            functionId = '#MineralsLeasesId';
            functionDataIndex = 'leaseId'
        }else if(dataIndex == 'Owner #' || dataIndex == 'Owners'){
            functionName = 'Manage Owner';
            functionTab = 'ProperRecordsManageOwner';
            functionId = '#ProperRecordsManageOwnersId';
            functionDataIndex = 'ownerId'        
        }else if(dataIndex == 'Econ Unit #'){
            functionName = 'Manage Unit';
            functionTab = 'MineralValuationManageUnit';
            functionId = '#UnitsViewId';
            functionDataIndex = 'unitId'   
        }
        
        favoriteTree.getStore().each(function(c, fn){
            if(c.data.functionName == functionName){
                index = fn;
                dataTree=false;
            }
        })            
        menuTree.getStore().each(function(c, fn){
            if(c.data.functionName == functionName){
                index = fn;
                dataTree=true;
            }
        })
        
        if(dataTree == true){
            menuTree.getSelectionModel().select(index);
            setTimeout(function () {
                var tabPanel = centerView.down(functionTab);
                var cols = centerView.down(functionId).down('grid').columns;
                
                tabPanel.setActiveTab(0);
                Ext.each(cols, function (col) {
                    if(col.dataIndex == functionDataIndex){
                        col.filterElement.setValue(record.get(dataIndex).toString().trim());
                    }
                });
            },800 )
        }else{
            favoriteTree.getSelectionModel().select(index);
            setTimeout(function () {
                var tabPanel = centerView.down(functionTab);
                var cols = centerView.down(functionId).down('grid').columns;

                tabPanel.setActiveTab(0);
                Ext.each(cols, function (col) {
                    if(col.dataIndex == functionDataIndex){
                        col.filterElement.setValue(record.get(dataIndex).toString().trim());
                    }
                });
            },800 )
        }
		
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsGridViewController onOpenSelectedTabView.')
    },
});