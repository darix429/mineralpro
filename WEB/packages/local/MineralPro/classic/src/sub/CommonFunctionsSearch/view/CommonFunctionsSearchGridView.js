
Ext.define('MineralPro.sub.CommonFunctionsSearch.view.CommonFunctionsSearchGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.CommonFunctionsSearchGridView',
    store: 'MineralPro.sub.CommonFunctionsSearch.store.CommonFunctionsSearchStore',
    requires: ['MineralPro.sub.CommonFunctionsSearch.view.CommonFunctionsSearchGridViewController',
    ],
    controller: 'CommonFunctionsSearchGridViewController',
    initComponent: function () {
        var me = this;
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')]

        me.columns = [{
                header: 'RRC #',
                flex: 1
            }, {
                header: 'Lease/Unit Name',
                flex: 1
            }, {
                header: 'Lease #',
                flex: 1
            }, {
                header: 'Field',
                flex: 1
            }, {
                header: 'Appraisal Value',
                flex: 1
            }]

        var searchTextField = {
            xtype: 'textfield',
            width: 300,
            labelAlign: 'right',
            itemId: 'searchTextFieldItemId',
            fieldLabel: 'Search Value',
            labelStyle: 'color: white;',
            emptyText: 'Type Search Value',
            enableKeyEvents: true,
            listeners: {
                afterrender: function () {
                    var me = this;
                    setTimeout(function () {
                        me.focus()
                    }, 600);
                },
                keyup: function (field, e, eOpts) {
                    if (e.getKey() == e.ENTER) {
                        if (!field.up('grid').down('#searchButtonItemId').isDisabled()) {
                            field.up('grid').down('#searchButtonItemId').fireEvent('click')
                        }
                    }
                }
            }
        }

        var searchTypeCombo = {
            xtype: 'combo',
            width: 300,
            labelAlign: 'right',
            typeAhead: true,
            displayField: 'displayField',
            valueField: 'displayField',
            itemId: 'searchTypeComboItemId',
            value: 'MINERAL APPRAISAL RRC#',
            store: [
                'MINERAL APPRAISAL RRC#',
                'MINERAL FIELD NAME',
                'MINERAL LEASE #',
                'MINERAL LEASE NAME',
                'MINERAL LEGAL DESCRIPTION',
                'MINERAL UNIT #',
                'MINERAL UNIT NAME',
                'OPERATOR NAME (Lease)',
                'OPERATOR NAME (Unit)',
                'OPERATOR NAME (Appraisal)',
                'OPERATOR NAME (Mineral Rights)',
                'OWNER # (Mineral Rights)',
                'OWNER NAME (Mineral Rights)',
                'OWNER ADDRESS (Mineral Rights)',
                'AGENT NAME (Mineral Rights)',
            ],
            enableKeyEvents: true,
            listeners: {
                keyup: function (field, e, eOpts) {
                    if (e.getKey() == e.ENTER) {
                        field.up('grid').down('#searchButtonItemId').fireEvent('click')
                    }
                }
            }
        }

        var searchButton = {
            xtype: 'button',
            width: 90,
            iconCls: 'search',
            tooltip: 'Search',
            text: 'Search',
            itemId: 'searchButtonItemId',
            listeners: {
                click: 'onSearchDataGrid'
            }
        }

        me.tools = [
            searchTextField,
            searchTypeCombo,
            searchButton,
                    // MineralPro.config.util.MainGridToolCmpt.clearFilterButton
        ],

        Ext.StoreMgr.lookup(me.getStore()).on({           
            load: function (store, records, successful, operation, eOpts ) {
                                         
                if (operation._response) {                    
                    if (operation._response.responseText 
                            && operation._response.responseText.indexOf('invalid query params') == -1) {
                                               
                        var columns = me.getColumns();

                        Ext.each(columns, function (col) {
                            if (col.dataIndex != 'fieldName') {
                                me.headerCt.remove(col)
                            }
                        })

                        var colArray = Ext.decode(operation._response.responseText).columnField.split(',')
                        var colLength = columns.length;

                        Ext.each(colArray, function (col) {
                            col = col.trim();
                            var column;
                            if (MineralPro.config.Runtime.appName == 'Minerals'
                                    && (col == 'Lease #' || col == 'Owner #' || col == 'Econ Unit #')) {

                                column = Ext.create('Ext.grid.column.Column', {
                                    text: col,
                                    dataIndex: col,
                                    flex: 1,
                                    renderer: function (value) {
                                        return '<a href="javascript:void(0);" >' + value + '</a>';
                                    },
                                    listeners: {
                                        click: function (cell, td, rowIndex, cellIndex, e, record, value, index) {
                                            me.getController().onOpenSelectedTabView(record, rowIndex, col);
                                        }
                                    }
                                });
                            } else {
                                column = Ext.create('Ext.grid.column.Column', {
                                    text: col,
                                    dataIndex: col,
                                    flex: 1
                                });
                            }
                            me.headerCt.insert(colLength, column);
                            colLength++
                        })                       
                    }
                }
                me.getView().refresh();
                me.down('#searchButtonItemId').enable(true);
            }
        })

        me.callParent();
    },
});
