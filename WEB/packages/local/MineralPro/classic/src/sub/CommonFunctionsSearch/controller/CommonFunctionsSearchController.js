///**
// * Handles the controller processing for Client List
// */

Ext.define('MineralPro.sub.CommonFunctionsSearch.controller.CommonFunctionsSearchController', {
    extend: 'Ext.app.Controller',
  
    stores: ['MineralPro.sub.CommonFunctionsSearch.store.CommonFunctionsSearchStore',  
    ],  
    
    views: [
    'MineralPro.sub.CommonFunctionsSearch.view.CommonFunctionsSearchView',
    'MineralPro.sub.CommonFunctionsSearch.view.CommonFunctionsSearchGridView',
    ],          
    
    requires: ['Ext.form.ComboBox',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden',
//    'MineralPro.sub.CommonFunctionsSearch.config.util.MainGridToolCmpt'
    ]
});


