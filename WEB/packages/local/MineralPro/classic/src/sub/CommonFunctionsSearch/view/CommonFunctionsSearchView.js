Ext.define('MineralPro.sub.CommonFunctionsSearch.view.CommonFunctionsSearchView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.CommonFunctionsSearch',
    layout:'border',
    
    id: 'CommonFunctionsSearchId',
    
    items: [{
        xtype:'CommonFunctionsSearchGridView',
        html:'center',
        region:'center'
    }]
});     

