Ext.define('MineralPro.config.RuntimeUtility', {
    statics: {
        /**
         * Prints out the message on the console debugger based from debugMode
         * @param(var) msg
         */
        DebugLog: function (msg) {
            if (MineralPro.config.Runtime.debugMode) {
                console.log(msg)
            }
        }, //end DebugLog
        /**
         *Adds VTypes being used by the system. Should be called only once.
         */
        ImplementVtypes: function () {
            Ext.apply(Ext.form.field.VTypes, {
                'phone': function () {
                    var re = /^[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{4}[\(\)\.\- ]{0,}$/;
                    return function (v) {
                        return re.test(v);
                    };
                }(),
                'phoneText': 'The phone number format is invalid, ie: 123-456-7890 (dashes optional) Or (123) 456-7890',
                'fax': function () {
                    var re = /^[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{3}[\(\)\.\- ]{0,}[0-9]{4}[\(\)\.\- ]{0,}$/;
                    return function (v) {
                        return re.test(v);
                    };
                }(),
                'faxText': 'The fax format is invalid',
                'zipCode': function () {
                    var re = /^\d{5}(-\d{4})?$/;
                    return function (v) {
                        return re.test(v);
                    };
                }(),
                'zipCodeText': 'The zip code format is invalid, e.g., 94105-0011 or 94105',
                'url': function () {
                    var re = /^([\dA-Za-z\.-@]+)\.([a-z\.]{2,25})([\/\w \.-]*)*\/?$/;
                    return function (v) {
                        return re.test(v);
                    };
                }(),
                'urlText': 'The URL format is invalid, e.g.,mysite.com',
                //for color
                'colorText': 'This field must be a valid hex web color.',
                'color': function () {
                    var re = /^#(?:[0-9a-f]{3}){1,2}$/i;
                    return function (v) {
                        return re.test(v);
                    };
                }()
            });
        }, //end ImplementVtypes
        disableBackspace: function () {
            Ext.getBody().addListener('keydown', function (e) {
                if (e.getTarget().type != 'text'
                        && e.getKey() == '8'
                        && e.getTarget().type != 'textarea'
                        && e.getTarget().type != 'password') {
                    e.preventDefault();
                }
            });
        }, //end disableBackspace
        enablePasswordChecking: function () {
            Ext.apply(Ext.form.VTypes, {
                password: function (val, field) {
                    if (field.initialPassField) {
                        var pwd = Ext.getCmp(field.initialPassField);
                        return (val == pwd.getValue());
                    }
                    return true;
                },
                passwordText: 'The passwords entered do not match'
            });
        }, //end enablePasswordChecking
        /**
         * Pops up an alert message with OK button
         * @param(var) title
         * @param(var) msg
         * @param(constructor) obj *optional
         * @param(var) object to focus inside obj *optional
         */
        ShowAlertOk: function (title, msg, obj, focus) {
            Ext.Msg.show({
                title: title,
                msg: msg,
                buttons: Ext.Msg.OK,
                fn: function () {
                    if (obj) {
                        if (obj.get(focus)) {
                            obj.get(focus).focus();
                        }
                    }
                }
            });
        }, //end ShowAlertOk
        addThemeOptions: function () {
            function getQueryParam(name, queryString) {
                var match = RegExp(name + '=([^&]*)').exec(queryString || location.search);
                return match && decodeURIComponent(match[1]);
            }

            function hasOption(opt) {
                var s = window.location.search;
                var re = new RegExp('(?:^|[&?])' + opt + '(?:[=]([^&]*))?(?:$|[&])', 'i');
                var m = re.exec(s);

                return m ? (m[1] === undefined ? true : m[1]) : false;
            }

            function changeCSS(href) {
//                var oldlink = document.getElementsByTagName("link").item(0)
                var oldlink = document.getElementById("mainTheme");
                oldlink.setAttribute("href", href);
            }

            var defaultTheme = 'classic',
                    requires = [
                        'Ext.window.MessageBox',
                        'Ext.toolbar.Toolbar',
                        'Ext.form.field.ComboBox',
                        'Ext.form.FieldContainer',
                        'Ext.form.field.Radio'

                    ],
                    defaultQueryString, theme, toolbar;

            theme = getQueryParam('theme') || defaultTheme;
            changeCSS("../resources/theme/" + theme + "-all.css");

            Ext.require(requires);

            Ext.onReady(function () {

                if (hasOption('nocss3')) {
                    Ext.supports.CSS3BorderRadius = false;
                    Ext.getBody().addCls('x-nbr x-nlg');
                }

                if (hasOption('nlg')) {
                    Ext.getBody().addCls('x-nlg');
                }

                function setParam(param) {
                    var queryString = Ext.Object.toQueryString(
                            Ext.apply(Ext.Object.fromQueryString(location.search), param)
                            );
                    location.search = queryString;
                }

                function removeParam(paramName) {
                    var params = Ext.Object.fromQueryString(location.search);

                    delete params[paramName];

                    location.search = Ext.Object.toQueryString(params);
                }

                if (hasOption('no-toolbar') || /no-toolbar/.test(document.cookie)) {
                    return;
                }

                setTimeout(function () {
                    toolbar = Ext.widget({
                        xtype: 'toolbar',
                        border: true,
                        rtl: false,
                        id: 'options-toolbar',
                        floating: true,
                        fixed: true,
                        defaultAlign: Ext.optionsToolbarAlign || 'tr-tr',
                        alignOffset: [-(Ext.getScrollbarSize().width + 5), 0],
                        preventFocusOnActivate: true,
                        draggable: {
                            constrain: true
                        },
                        defaults: {rtl: false},
                        items: [{
                                xtype: 'combo',
                                width: 220,
                                labelWidth: 55,
                                fieldLabel: 'Theme',
                                displayField: 'name',
                                valueField: 'value',
                                labelStyle: 'cursor:move;',
                                margin: '0 5 0 0',
                                queryMode: 'local',
                                store: Ext.create('Ext.data.Store', {
                                    fields: ['value', 'name'],
                                    data: [
                                        {value: 'classic', name: 'Classic'},
                                        {value: 'my-gray-theme', name: 'My-Gray-Theme'},
                                    ]
                                }),
                                value: theme,
                                listeners: {
                                    select: function (combo) {
                                        var theme = combo.getValue();
                                        if (theme !== defaultTheme) {
                                            setParam({theme: theme});

                                        } else {
                                            removeParam('theme');
                                        }
                                    }
                                }
                            }, {
                                xtype: 'tool',
                                type: 'close',
                                handler: function () {
                                    toolbar.destroy();
                                }
                            }]
                    });
                    toolbar.show();
                }, 800);
            });
        }, //end addThemeOptions

        onTabChange: function (tab) {
            var me = this;
            me.DebugLog('Entering RuntimeUtility onTabChange.')

            var activeTab = tab.getActiveTab();
            var grid = activeTab.down('grid');
            var store = grid.getStore();
            var selectionModel = grid.getSelectionModel();
            var getIndex = function(){
                var selectedIndex = 0;
                var mainPanel = grid.mainPanelAlias ? grid.up(grid.mainPanelAlias) : false;
                var mainGrid = tab.down('grid');
                var includeRRC = mainGrid && mainPanel.selectedRRCNumberIndex ? mainGrid.getColumns().map(function(col){ return col.dataIndex; }).indexOf(mainPanel.selectedRRCNumberIndex) > -1 : false;
                    
                if(mainPanel){
                    selectedIndex = store.findBy(function(rec){
                        var selectedId = rec.get(mainPanel.selectedIdIndex) == mainPanel.selectedId;
                        var rrcNumber = rec.get(mainPanel.selectedRRCNumberIndex) ? rec.get(mainPanel.selectedRRCNumberIndex) == mainPanel.selectedRRCNumber : true;
                        rrcNumber = includeRRC ? rrcNumber : true;
                        return selectedId && rrcNumber;
                    });
                }
                return selectedIndex;
            }
            if (grid) {
                //  Ext.suspendLayouts();   
                //  Ext.getBody().suspendEvents(true);   
                var store = grid.getStore();
                store.getProxy().extraParams = {
                    selectedId: tab.selectedId,
                    selectedRRCNumber: tab.selectedRRCNumber,
                    selectedJE: tab.selectedJournalEntryId
                };

                if (store.tabChangeReload) {
                    me.DebugLog('STORE WILL BE RELOADED.')
                    grid.getStore().reload({
                        callback: function () {
                            me.DebugLog('will be selecting index '+getIndex())
                            grid.getSelectionModel().select(getIndex(), false, false);
                            if(grid.selectFirstRow == false){
                                grid.getSelectionModel().deselectAll();
                            }
                             
                            setTimeout(function () {
                                if (grid.down('#' + grid.firstFocus)) {
                                    grid.down('#' + grid.firstFocus).filterElement.focus();
                                } else if (activeTab.down('form')) {
                                    if (activeTab.down('form').firstFocus)
                                        activeTab.down('#' + activeTab.down('form').firstFocus).focus();
                                }
                            }, 300);
                            // Ext.getBody().resumeEvents(true);
                            //  Ext.resumeLayouts();
                        }
                    });
                } else{     
                    setTimeout(function(){
                        me.DebugLog('STORE WILL BE NOT BE RELOADED.')
                        grid.getSelectionModel().deselectAll();
                        if(tab.selectedId){    
                            // var index = grid.getStore().find(tab.selectedIdIndex, tab.selectedId, false, false, true);                        
                            // if (index == -1){
                            //     index = 0;
                            // }
                            grid.getSelectionModel().select(getIndex(), false, false);
                        }else{
                            grid.getSelectionModel().select(getIndex(), false, false);
                        }                    
                        Ext.globalEvents.fireEvent('checkLoadingStores');//located in CPLoginController   
                    }, 500)
                } 
            } else {
                Ext.getBody().unmask()
            }
            
            var centerView = activeTab.up('mainViewPort').down('centerView');   
            var buttonArr = centerView.query('button')
            var actionColArr = centerView.query('actioncolumn')
            Ext.each(buttonArr, function(button){
                if (button.itemId != 'clearFilterButton' && button.itemId != 'searchButtonItemId'
                        && button.xtype != 'tab' && button.alwaysShow != true)
                    button.setHidden(MineralPro.config.Runtime.readOnly)
            });

            Ext.each(actionColArr, function(actionCol){
                if(actionCol.alwaysShow != true){
                    actionCol.setHidden(MineralPro.config.Runtime.readOnly)
                }
                if(actionCol.itemId){
                    actionCol.setHidden(false)
                }
            });

            me.DebugLog('Leaving RuntimeUtility onTabChange.')
        }

    }
})