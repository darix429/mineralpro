
Ext.define('MineralPro.config.view.BaseHeaderView', {
    extend: 'Ext.form.Panel',
    width: '100%',
    margin: '5 5 5 5',
    
    bodyStyle: {
        background: 'transparent'
    },

    initComponent: function() {       
        var me = this;
      
        me.callParent(arguments);        
    }
    
})