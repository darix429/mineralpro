
Ext.define('MineralPro.config.view.BaseAddEditFormView', {
    extend: 'Ext.window.Window',
    requires: ['MineralPro.config.view.BaseAddEditFormViewController'],
    controller: 'baseAddEditFormViewController',
    
    layout: 'fit',
    // autoShow: true,
    modal: true,
    defaults:{ maskRe: /[^'\^]/ },
    listeners: {
        afterRender: function(thisForm, options){
            var me = this
            
            setTimeout(function (){
                var focus = me.defaultFocus;
                if (me.defaultFocus.indexOf('#') == -1){
                    focus = '#'+me.defaultFocus
                }      
                me.down(focus).focus();
            },500)           
            
            me.keyNav = Ext.create('Ext.util.KeyNav', me.el, {
                enter: function(){
                   me.down('#saveId').fireEvent('click', me.down('#saveId'))
                },            
                esc: function(){
                        setTimeout(function (){
                            me.close()
                        },500)
                    },

                N: {////for add and new 
                    alt: true,
                    fn: function () {
                        me.down('#addnew').fireEvent('click', me.down('#addnew'))
                        return false;
                    }
                },
                A: {///for add to grid
                    alt: true,
                    fn: function () {
                        me.down('#saveId').fireEvent('click', me.down('#saveId'))
                        return false;
                    }
                },
                C: {///for change to grid
                    alt: true,
                    fn: function () {
                        me.down('#saveId').fireEvent('click', me.down('#saveId'))
                        return false;
                    }
                },
               S: {//save on image upload
                    alt: true,
                    fn: function () {
                        me.down('#saveId').fireEvent('click', me.down('#saveId'))
                        return false;
                    }
                },
                'tab': {
                   shift:true,
                   fn: function (e) {
                        setTimeout(function (){
                                       
                            var activeElement = Ext.get(Ext.Element.getActiveElement());
                            var activeWindow = Ext.WindowMgr.front
                            var focus = activeWindow.defaultFocus;
                            var defaultFocus = activeWindow.items;
                            var activeIndex = activeElement.component.tabIndex;
                            //activeWindow.setAlwaysOnTop(true);
                            if(activeIndex == 0){
                                activeWindow.down(focus).focus();
                            } else {           
                                if(!activeIndex){
                                    var mb = Ext.Msg.show({
                                        iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                                        title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                                        msg:"You're not allowed to exit the form",              
                                        buttons: Ext.Msg.OK,
                                        fn: function(){
                                            activeWindow.down(focus).focus();
                                        }
                                    });
                                }
                            }
                                       
                        },200)
                    }
                },
                scope: me
            });
        }
    }

    
})