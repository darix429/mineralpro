
Ext.define('MineralPro.config.view.BaseView', {
    extend: 'Ext.form.Panel',
    requires: 'MineralPro.config.view.BaseViewController',
    controller: 'baseViewController',
    closeActon: 'hide',
    
    initComponent: function() {       
        var me = this;

        me.listeners= {
            beforeclose: me.getController().onBeforeClose
        }
      
        me.callParent(arguments);        
    }
    
})