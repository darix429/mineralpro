
Ext.define('MineralPro.config.view.BaseTabPanelView', {
    extend: 'Ext.tab.Panel',
    requires: 'MineralPro.config.view.BaseTabPanelViewController',
    controller: 'baseTabPanelViewController',

    listeners: {
        tabchange: 'onTabChange',
        beforetabchange: 'onBeforeTabChange',
        beforeclose: 'onBeforeClose'
    },
    
    closeActon: 'hide',

    initComponent: function() {       
        var me = this;

//        me.listeners= {
//            tabchange: me.getController().onTabChange,
//            beforetabchange: me.getController().onBeforeTabChange,
//            beforeclose: me.getController().onBeforeClose
//        }
      
        me.callParent(arguments);        
    }
    
})