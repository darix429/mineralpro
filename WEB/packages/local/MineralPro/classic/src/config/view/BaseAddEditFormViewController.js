
Ext.define('MineralPro.config.view.BaseAddEditFormViewController', {
    extend : 'Ext.app.ViewController',
    alias: 'controller.baseAddEditFormViewController',
    /**
     * Add the data from the form into the data grid and clears the window data for new inputs
     */
    onAddAndNew: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAddEditFormViewController onAddAndNew.')
        var me = this;                     
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore); 
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
                
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();                        
        if(form.isValid()){
            record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
            record.set(values);    
            listStore.insert(0,record);
            
            listGrid.getSelectionModel().select(0, true);  
			
            
            //this part of the code will reset the data of the form window.
            //form.reset()
            //var new_record = Ext.create(listStore.model.displayName)//mainListModel
            var new_record = Ext.create(listGrid.mainListModel)                    
            form.loadRecord(new_record);
            view.focus(view.defaultFocus)
            //listGrid.fireEvent('afterlayout');
            
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        }  
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAddEditFormViewController onAddAndNew.')
    },
    
    /**
     * Add the data from the form into the data grid
     */
    onAddListToGrid: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAddEditFormViewController onAddListToGrid.')
        var me = this;                     
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore); 
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();  

        if(form.isValid()){
            record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
            record.set(values);    
            win.close();
            listStore.insert(0,record);
               
            listGrid.getSelectionModel().select(0, true);  
            listGrid.getView().focusRow(0);   
            //listGrid.fireEvent('afterlayout');
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAddEditFormViewController onAddListToGrid.')
    },
    /**
     * Edit the data from the form into the data grid
     */
    onEditListToGrid: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAddEditFormViewController onEditListToGrid.')

        var me = this;        
        var view = me.getView();
        var mainPanel = Ext.getCmp(view.mainPanelId);
        var listGrid = mainPanel.down(view.listGridAlias)
        
        var win = button.up('window');
        var form = win.down('form');
        //var record = form.getRecord();
	var record = listGrid.getStore().getAt(view.editRecordIndex);
        var values = form.getValues();
        
        if(form.isValid()){
            record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
            record.set(values);                

            if(listGrid.withDetailsView){
                var detailView = mainPanel.down(listGrid.detailAliasName);
                detailView.loadRecord(record);
            }
            
            win.close();                             
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        } 
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAddEditFormViewController onEditListToGrid.')
    },
    
    /**
     * Check if new data to add already exist in the table.
     */
    onAddCheckExisting: function(e, event, opt){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAddEditFormViewController onAddCheckExisting.')
        
        var me = this;  
        var view = me.getView();
        setTimeout(function (){                       
            var showAlert = false;

            if(!e.destroyed){
                var listStore = Ext.StoreMgr.lookup(view.listGridStore);

                if (listStore.findRecord(e.name, e.value, 0, false, false, true)) {
                    showAlert = true;
                    if (view.id.toLowerCase().indexOf("edit") != -1) {
                        if (listStore.getAt(view.editRecordIndex).get(e.name) == e.value) {
                            showAlert = false;
                        }
                    }                                     
                }
                if(showAlert){
                     Ext.Msg.show({
                        iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
                        title:MineralPro.config.Runtime.addExistingDataWarningTitle,
                        msg:MineralPro.config.Runtime.addExistingDataWarningMsg,
                        buttons: Ext.Msg.OK,
                        fn: function(){
                            e.focus();
                        }
                    });
                } 
            }           
        },300)
                                     
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAddEditFormViewController onAddCheckExisting.')    
    },
    
    onCheckZipCode: function(me,e,key){
        if((e.getCharCode != 8) && (me.value.length == 5)){
            me.setValue(me.value + '-')
        }
    },
    onCheckNumber: function(me,e,key){
        if((e.getCharCode != 8) && (me.value.length == 3 || me.value.length == 7)){                           
            me.setValue(me.value + '-')
        }
    },
    onCheckAddress: function(me,e,key){
        var window = me.up('window')
        if(window.down('#addrLine1').value == window.down('#addrLine2').value
            && window.down('#addrLine1').value.length > 0
            && window.down('#addrLine1').value.length > 0){
                Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.duplicateAddressAlertIcon,
                    title:MineralPro.config.Runtime.duplicateAddressAlertTitle,    
                    msg:MineralPro.config.Runtime.duplicateAddressAlertMsg,               
                    buttons: Ext.Msg.OK,
                    fn: function(){
                        me.focus();
                    }
                });
            }
    },
    
    
    
});
