//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('MineralPro.config.view.BaseViewController', {
    extend : 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.baseViewController',
   
    /**
     * Resposible saving unsaved data before closing tab
     */
    onBeforeClose: function(close){  
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseViewController onBeforeClose.')   
        
        var me=this
        var saveData = false
        var saveStore = []
        var arrayGrid= me.query('grid'); 
                            
        for(var x=0; x<arrayGrid.length; x++){
            var store = arrayGrid[x].getStore();
            
            if(store.getNewRecords().length > 0 
                    || store.getUpdatedRecords().length > 0 
                    || store.getRemovedRecords().length > 0){
                    saveStore.push(store)
                    saveData = true
            } 
            
        }
        
        if(saveData){           
            Ext.Msg.show({
                title:MineralPro.config.Runtime.tabCloseConfirmationTitle,    
                msg:MineralPro.config.Runtime.tabCloseConfirmationMsg,
                iconCls:MineralPro.config.Runtime.tabCloseConfirmationIcon,
                buttons: Ext.Msg.YESNO,               
                scope: this,
                width: 250,
                fn: function(btn) { 
                    var centerView = me.up('centerView');
                    if (btn === 'yes'){
                        Ext.each(saveStore, function(store){       
                            store.getProxy().extraParams = {
                                createLength: store.getNewRecords().length,
                                updateLength: store.getUpdatedRecords().length, 
                            };    
                            store.sync();  
                            store.commitChanges();
                        })
                        centerView.remove(me);
                    }else{                                              
                        Ext.each(saveStore, function(store){
                            store.rejectChanges()
                        });                                                 
                        centerView.remove(me);
                    }                                        
                }
            });           
            return false; 
        }
         
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseViewController onBeforeClose.')
    }
   
});