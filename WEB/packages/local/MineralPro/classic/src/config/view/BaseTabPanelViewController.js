//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('MineralPro.config.view.BaseTabPanelViewController', {
    extend : 'Ext.app.ViewController',
    alias: 'controller.baseTabPanelViewController',
   
  /**
     * Resposible for focusing selected tab on the navigation Tree.
     */      
    onTabChange: function(tabPanel, newCard, oldCard, eOpts){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseTabPanelViewController onTabChange.')        
        
        MineralPro.config.RuntimeUtility.onTabChange(tabPanel);            
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseTabPanelViewController onTabChange.')

    },
    
    onBeforeTabChange: function(tabPanel, newCard, oldCard, eOpts){       
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseTabPanelViewController onBeforeTabChange.') 
        
        Ext.get(document.activeElement).blur();    
        var activeTab = oldCard;
        var saveData = false
        var saveStore = []   
        var arrayGrid= activeTab.query('grid');        
        var index = tabPanel.items.indexOf(activeTab)   
        var appraisal = false

        if(parseInt(tabPanel.appraisalTabIndex) == index || parseInt(tabPanel.appraisalDetailsTab) == index){
            if(activeTab.down('form').getRecord()){
                 var record = activeTab.down('form').getRecord();
                 var values = activeTab.down('form').getValues();       
                 record.set(values);
            }
            appraisal = true
        }
        
        //this is for appraisalList
//        if(activeTab.down('#filterByExactItemId')){
////            if(!activeTab.down('#filterByExactItemId').checked){
//                var cols = activeTab.down('grid').columns
//                var selectedId = tabPanel.selectedId
//                var selectedRRCNumber = tabPanel.selectedRRCNumber
//                var currentIndex = 0;
//                                
//                Ext.each(cols, function (col) {
//                    if (col.filterElement)
//                        col.filterElement.setValue('');
//                });
//                
//                var appraisalStore = activeTab.down('grid').getStore()
//
//                appraisalStore.each(function (rec) {
//                    if (rec.get(tabPanel.selectedIdIndex) == selectedId) {
//                        if (rec.get(tabPanel.selectedRRCNumberIndex) == selectedRRCNumber) {
//                            return false;
//                        }
//                    }
//                    currentIndex++;
//                })
//           
//                activeTab.down('grid').getSelectionModel().select(currentIndex, false, false)               
////            }
//        }
        
//        if(parseInt(tabPanel.appraisalDetailsTab) == index){
//            if(activeTab.down('form').getRecord()){
//                 var record = activeTab.down('form').getRecord();
//                 var values = activeTab.down('form').getValues();       
//                 record.set(values);
//            }
//            appraisal = true
//        }
                     
//        setTimeout(function (){     
            for(var x=0; x<arrayGrid.length; x++){
                var store = arrayGrid[x].getStore();                  
                if(appraisal){
                    if(store.getNewRecords().length > 0){
                        var newRec = store.getNewRecords()[0]
                            var exclude = 'rrcNumber,fieldId,'
                                +'wellDescription,wellTypeCd,maxYearLife,comment,productionEquipmentCount,'
                                +'productionWellSchedule,productionEquipmentValue,serviceEquipmentCount,'
                                +'serviceWellSchedule,serviceEquipmentValue,injectionEquipmentCount,'
                                +'injectionWellSchedule,injectionEquipmentValue,'
                                +'discountRate,grossOilPrice,grossProductPrice,'
                                +'grossWorkingInterestGasPrice,grossRoyaltyInterestGasPrice,operatingExpense,'
                                +'dailyAverageOil,dailyAverageGas,dailyAverageProduct'
                            var fieldArray = exclude.split(',')
                            Ext.each(fieldArray, function(field){
                                if(newRec.get(field) != 0 && newRec.get(field) != '' 
                                        && newRec.get(field) != '0'){                                   
                                    saveStore.push(store)
                                    saveData = true    
                                    if(activeTab.down('#saveToDbButtonItemId').isDisabled() 
                                        || activeTab.down('#saveToDbButtonItemId').isHidden()){
                                        saveData = false;
                                    }
                                    return false;
                                }                           
                            })
                        
                    }else if(store.getUpdatedRecords().length > 0){
                        saveStore.push(store)
                        saveData = true           
                        if(activeTab.down('#saveToDbButtonItemId').isDisabled() 
                            || activeTab.down('#saveToDbButtonItemId').isHidden()){
                            saveData = false;
                        }
                    } 
                }else{
                    if(store.getNewRecords().length > 0 
                        || store.getUpdatedRecords().length > 0 
                        || store.getRemovedRecords().length > 0){
                        saveStore.push(store)
                        saveData = true                                            
                    } 
                }
                
                x=arrayGrid.length
            }

            if(saveData){           
                Ext.Msg.show({
                    title:MineralPro.config.Runtime.tabChangeConfirmationTitle,    
                    msg:MineralPro.config.Runtime.tabChangeConfirmationMsg,
                    iconCls:MineralPro.config.Runtime.tabChangeConfirmationIcon,
                    buttons: Ext.Msg.YESNO,               
                    scope: this,
                    width: 250,
                    fn: function(btn) { 
                        if (btn === 'yes'){
                            Ext.each(saveStore, function(store){     
                                var params = store.getProxy().extraParams
                                    params['createLength'] = store.getNewRecords().length
                                    params['updateLength'] = store.getUpdatedRecords().length
                                if(appraisal){                                                                    
                                    store.getAt(0).set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser); 
                                    params['origRrcNumber'] = store.getAt(0).get('origRrcNumber');                                    
                                    params['newRecord'] = activeTab.down('form').newAppraisal
                                    params['newCalculation'] = activeTab.down('form').newCalculation
                                    params['updateCalc'] = false;                                                                                                        
                                }
                                
                                store.getProxy().extraParams = params
//                                store.getProxy().extraParams = {
//                                    createLength: store.getNewRecords().length,
//                                    updateLength: store.getUpdatedRecords().length, 
//                                };    
                                
                                var storeReload = false;
                                var reload = 'Owner,Unit,Lease,Appraisal'
                                var reloadArr = reload.split(',');
                                Ext.each(reloadArr, function(rel){
                                    if(Ext.util.Format.lowercase(store.proxy.api.read).indexOf(Ext.util.Format.lowercase(rel))){
                                        storeReload=true;
                                        return false;
                                    }
                                })
                                
                                store.sync({
                                    callback: function(){
                                        if(storeReload)
                                            store.reload();
                                    }
                                });  
                                
                                store.commitChanges();
                            })
                            tabPanel.setActiveTab(newCard)
                            
//                            Ext.each(saveStore, function(store){
//                                store.rejectChanges()
//                                if(parseInt(tabPanel.appraisalTabIndex) == index){
//                                    if(store.count() > 0){
//                                        activeTab.down('form').loadRecord(store.getAt(0));
//                                    }
//                                }
//                                
//                                if(parseInt(tabPanel.appraisalDetailsTab) == index){
//                                    if(store.count() > 0){
//                                        activeTab.down('form').loadRecord(store.getAt(0));
//                                    }
//                                }
//                            }); 
                            var mainPanel = activeTab.up(activeTab.mainPanelAlias);
                            var grid = activeTab.down('grid');
                            var parentTab = false;
                            if(mainPanel.selectedId != null){
                                parentTab = true;
                            }  
                            if(parentTab){
                                 grid.getSelectionModel().select(0, false, false);
                            }

//                            if(parseInt(tabPanel.appraisalTabIndex) == index){
//                                activeTab.down('form').newAppraisal = false
//                                activeTab.down('form').newCalculation = false
////                                activeTab.down('form').loadRecord(store.getAt(0))
//                            }
                            
//                            if(parseInt(tabPanel.appraisalDetailsTab) == index){
//                                activeTab.down('form').newAppraisal = false
//                                activeTab.down('form').newCalculation = false
////                                activeTab.down('form').loadRecord(store.getAt(0))
//                            }
                            
                            tabPanel.setActiveTab(newCard)
                        }else{                                              
                            Ext.each(saveStore, function(store){
                                store.rejectChanges()
                                if(parseInt(tabPanel.appraisalTabIndex) == index){
                                    if(store.count() > 0){
                                        activeTab.down('form').loadRecord(store.getAt(0));
                                    }
                                }
                                
                                if(parseInt(tabPanel.appraisalDetailsTab) == index){
                                    if(store.count() > 0){
                                        activeTab.down('form').loadRecord(store.getAt(0));
                                    }
                                }
                            }); 
                            tabPanel.setActiveTab(newCard)
                        }                                        
                    }
                }); 
                return false;
            }
//        },100)                          
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseTabPanelViewController onBeforeTabChange.')
        
    },
    
    onBeforeClose: function(tabPanel, newCard, oldCard, eOpts){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseTabPanelViewController onBeforeClose.')
        
        var saveData = false,   
            saveStore = [],
            arrayGrid= tabPanel.getActiveTab().query('grid'); 
        var activeTab = tabPanel.getActiveTab()
        var index = tabPanel.items.indexOf(activeTab)   
        var appraisal = false

        if(parseInt(tabPanel.appraisalTabIndex) == index || parseInt(tabPanel.appraisalDetailsTab) == index){
            if(activeTab.down('form').getRecord()){
                 var record = activeTab.down('form').getRecord();
                 var values = activeTab.down('form').getValues();       
                 record.set(values);
            }
            appraisal = true
        }
        
        for(var x=0; x<arrayGrid.length; x++){
                var store = arrayGrid[x].getStore();                  
                if(appraisal){
                    if(store.getNewRecords().length > 0){
                        var newRec = store.getNewRecords()[0]
                            var exclude = 'rrcNumber,fieldId,'
                                +'wellDescription,wellTypeCd,maxYearLife,comment,productionEquipmentCount,'
                                +'productionWellSchedule,productionEquipmentValue,serviceEquipmentCount,'
                                +'serviceWellSchedule,serviceEquipmentValue,injectionEquipmentCount,'
                                +'injectionWellSchedule,injectionEquipmentValue,'
                                +'discountRate,grossOilPrice,grossProductPrice,'
                                +'grossWorkingInterestGasPrice,grossRoyaltyInterestGasPrice,operatingExpense,'
                                +'dailyAverageOil,dailyAverageGas,dailyAverageProduct'
                            var fieldArray = exclude.split(',')
                            Ext.each(fieldArray, function(field){
                                if(newRec.get(field) != 0 && newRec.get(field) != '' 
                                        && newRec.get(field) != '0'){                                   
                                    saveStore.push(store)
                                    saveData = true 
                                    return false;
                                }                           
                            })
                        
                    }else if(store.getUpdatedRecords().length > 0){
                        saveStore.push(store)
                        saveData = true                                            
                    } 
                }else{
                    if(store.getNewRecords().length > 0 
                        || store.getUpdatedRecords().length > 0 
                        || store.getRemovedRecords().length > 0){
                        saveStore.push(store)
                        saveData = true                                            
                    } 
                }
                
                x=arrayGrid.length
            }
        
        if(saveData){           
            Ext.Msg.show({
                title:MineralPro.config.Runtime.tabChangeConfirmationTitle,    
                msg:MineralPro.config.Runtime.tabCloseConfirmationMsg,
                iconCls:MineralPro.config.Runtime.tabCloseConfirmationIcon,
                buttons: Ext.Msg.YESNO,               
                scope: this,
                width: 250,
                fn: function(btn) { 
                    var centerView = tabPanel.up('centerView');
                    if (btn === 'yes'){
                            Ext.each(saveStore, function(store){     
                                var params = store.getProxy().extraParams
                                    params['createLength'] = store.getNewRecords().length
                                    params['updateLength'] = store.getUpdatedRecords().length
                                if(appraisal){                                                                    
                                    store.getAt(0).set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser); 
                                    params['origRrcNumber'] = store.getAt(0).get('origRrcNumber');                                    
                                    params['newRecord'] = activeTab.down('form').newAppraisal
                                    params['newCalculation'] = activeTab.down('form').newCalculation
                                    params['updateCalc'] = false;                                                                                                        
                                }
                                
                                store.getProxy().extraParams = params
//                                store.getProxy().extraParams = {
//                                    createLength: store.getNewRecords().length,
//                                    updateLength: store.getUpdatedRecords().length, 
//                                };    
                                store.sync();  
                                store.commitChanges();
                            })
                        setTimeout(function (){   
                            centerView.remove(tabPanel);
                        },100)
                    }else{                                              
                        Ext.each(saveStore, function(store){
                            store.rejectChanges()
                        });       
                        setTimeout(function (){   
                            centerView.remove(tabPanel);
                        },100)   
                        
                    }                                        
                }
            });           
            return false; 
        }
        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseTabPanelViewController onBeforeClose.')
    },
   
});
