
Ext.define('MineralPro.config.view.BaseDetailView', {
    extend: 'Ext.form.Panel',
    frame:true,
    autoScroll: true,
    split:true,
    collapsible: true,
    draggable:true,
    bodyPadding:5,
    closeActon: 'hide',
    defaults: {
        width: 230,
        labelWidth: 100
    },
    defaultType: 'textfield'
})