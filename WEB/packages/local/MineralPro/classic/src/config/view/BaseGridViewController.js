//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('MineralPro.config.view.BaseGridViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.baseGridViewController',
    /**
     * Sync the client grid data to database (add, edit, delete)
     */
    onSyncListGridData: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onSyncListGridData.')
        var me = this;       
        var view = me.getView();
        var listStore = view.getStore();
                                  
        var dirty = listStore.getNewRecords().length > 0
        || listStore.getUpdatedRecords().length > 0
        || listStore.getRemovedRecords().length > 0;
        if (dirty) {
            Ext.Msg.show({
                title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                msg:MineralPro.config.Runtime.syncGridMsgConfimationMsg,
                iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                buttons: Ext.Msg.YESNO,
                scope: this,
                width: 250,
                fn: function (btn) {
                    if (btn === 'yes') {
                        view.getSelectionModel().deselectAll(true);
                        listStore.getProxy().extraParams = {
                            createLength: listStore.getNewRecords().length,
                            updateLength: listStore.getUpdatedRecords().length
                        };

                        listStore.sync({
                            success: function(batch, options){
                                var mainPanel = view.up(view.mainPanelAlias)
                                var getIndex = function(){
                                    /* Get selected Row if exists, otherwise select index 0 */
                                    var selectedIndex = 0;
                                    if(mainPanel){
                                        selectedIndex = listStore.findBy(function(rec){
                                            var selectedId = rec.get(mainPanel.selectedIdIndex) == mainPanel.selectedId;
                                            var rrcNumber = mainPanel.selectedRRCNumberIndex ? rec.get(mainPanel.selectedRRCNumberIndex) == mainPanel.selectedRRCNumber : true;
                                            return selectedId && rrcNumber;
                                        });
                                    }
                                    return selectedIndex > -1 ? selectedIndex : 0;
                                }

                                if(mainPanel.selectedId || !mainPanel.hasOwnProperty('selectedId')){
                                    if(mainPanel.hasOwnProperty('selectedId') && mainPanel.selectedId){
                                        listStore.getProxy().extraParams = {
                                            selectedId: mainPanel.selectedId
                                        };
                                    }
                                    listStore.reload({
                                        callback: function (record, operation, success){
                                            if(success){
                                                view.getSelectionModel().select(getIndex(), false, false);
                                            }
                                            else{
                                                Ext.Msg.show({
                                                    title:'Oops!',
                                                    msg:'Something went wrong while retrieving Leases',
                                                    buttons: Ext.Msg.OK
                                                });
                                            }
                                        }
                                    });
                                }
                                else{
                                    view.getSelectionModel().select(getIndex(), false, false);
                                }
                                if(view.affectedComboStore){
                                    var affectedComboStoreArr = view.affectedComboStore;
                                    Ext.each(affectedComboStoreArr, function(store){
                                        Ext.StoreMgr.lookup(store).reload();
                                    })
                                }
                            },
                            failure: function(batch, options){
                                Ext.Msg.show({
                                    iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                                    title:MineralPro.config.Runtime.listSyncMsgErrorTitle,
                                    msg:MineralPro.config.Runtime.listSyncMsgErrorMsg,
                                    buttons: Ext.Msg.OK
                                });  
                            }
                        });
                        listStore.commitChanges();
                    }
                }
            });
        } else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                title:MineralPro.config.Runtime.listSyncMsgErrorTitle,
                msg:MineralPro.config.Runtime.listSyncMsgErrorMsg,
                buttons: Ext.Msg.OK
            });                      
        }                         
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onSyncListGridData.')
    },
    /**
     * Show up the Add List form
     */
    onOpenAddListForm: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onOpenAddListForm.')
        var me = this;
        var mainView = me.getView();
        var record = Ext.create(mainView.mainListModel);      
        var addView = Ext.widget(mainView.addFormAliasName);
        
        record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
        addView.down('form').loadRecord(record);
        addView.show()
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onOpenAddListForm.')
    },
    /**
     * Open up the Edit form for editing
     */
    onOpenEditListForm: function (grid, rowIndex, colIndex) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onOpenEditListForm.')
        var me = this;
        var mainView = me.getView();
        var rec = mainView.getStore().getAt(rowIndex);        
        var headerDetailStore = Ext.StoreMgr.lookup(mainView.headerInfoStore)      
        if(mainView.headerDetail && !grid.selection.dirty){
            setTimeout(function(){
                if(headerDetailStore.isLoading()){
                mainView.showEdit = true; 
                mainView.rowIndex = rowIndex
                }else{
                    var editView = Ext.widget(mainView.editFormAliasName);
                    editView.editRecordIndex = rowIndex   
                    editView.down('form').loadRecord(mainView.headerDetail);   
                    editView.show();
                } 
            }, 300)              
        }else{
            var editView = Ext.widget(mainView.editFormAliasName);
            editView.editRecordIndex = rowIndex   
            editView.down('form').loadRecord(rec); 
            editView.show();  
        }      
          
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onOpenEditListForm.')
    },
    /**
     * Clear value for all filter
     */
    onClearListFilter: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onClearListFilter.')
        var me = this;
        var cols = me.getView().columns

        Ext.each(cols, function (col) {
            if (col.filterElement)
                col.filterElement.setValue('');
        });

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onClearListFilter.')
    },
    /**
     * Default to select first row in Main List Panel
     */
    onAfterLayout: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onAfterLayout.')

        var view = this.getView();   
        if (view.getStore().getAt(0)) {
            var selection = view.getSelectionModel().getSelection();
            if (selection.length == 0)
                view.getSelectionModel().select(0, false, true);

            view.getController().onLoadSelectionChange(view, view.getSelectionModel().getSelection());
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onAfterLayout.')
    },
    /**
     * Reject changes on grid
     */
    onCancelGrid: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onCancelGrid.')
        var me = this
        var view = me.getView()
        var isTab = view.up(view.mainPanelAlias).down('tab');
        if (isTab) {
            var arrayGrid = view.up(view.mainPanelAlias).getActiveTab().query('grid');

            Ext.each(arrayGrid, function (grid) {
                grid.getStore().rejectChanges();
            });

        } else {
            var arrayGrid = view.up(view.mainPanelAlias).query('grid');

            Ext.each(arrayGrid, function (grid) {
                grid.getStore().rejectChanges();
            });
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onCancelGrid.')
    },
     /**
     * load Records to the Details View
     */
    onLoadSelectionChange: function (model, records) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onLoadSelectionChange.')
        // var me = model.view.ownerCt;
        var me = this;
        var mainView = me.getView();
        var mainPanel = mainView.up(mainView.mainPanelAlias);
        var parentTab = false;
        var showHeader = false;
        if(mainPanel.selectedId != null && records.length > 0){
            parentTab = true;
        }
        if(parentTab){
            if(mainView.withChildTab){
                if(records.length > 0){
                    mainPanel.selectedId = records[0].get(mainPanel.selectedIdIndex);
                    mainPanel.selectedName = records[0].get(mainPanel.selectedNameIndex);  
                }
                var headerDetailStore = Ext.StoreMgr.lookup(mainView.headerInfoStore);
                headerDetailStore.getProxy().extraParams = {
                    selectedId: mainPanel.selectedId
                };
                headerDetailStore.load({
                    callback: function(rec){
                        if(rec.length>0){
                            showHeader = true; 
                            mainView.headerDetail = rec[0]
                        }
                        updateHeader(mainView, rec, showHeader);
                        if (mainView.withDetailsView) {
                            var detailView = mainView.up(mainView.mainPanelId).down(mainView.detailAliasName);

                            if(records[0]){
                                if(records[0].dirty){
                                    detailView.loadRecord(records[0])  
                                }else if (rec.length > 0){
                                    detailView.loadRecord(rec[0]);
                                }                                           
                            } else if (rec.length > 0){
                                detailView.loadRecord(rec[0]);
                            }                      
                        } 
                        if(mainView.showEdit){
                            mainView.getController().showEditViewViaHeader();
                        }
                    }
                });                  
            }                       
        }else if (mainView.withDetailsView) {
            var detailView = mainView.up(mainView.mainPanelId).down(mainView.detailAliasName);
            if(records.length > 0){
                detailView.loadRecord(records[0]);
            }                      
            updateHeader(mainView, records, showHeader);                  
        }else{
            updateHeader(mainView, records, showHeader);
        }


        function updateHeader (mainView, records, showHeader){    
            MineralPro.config.Runtime.headerInfoXtype='';
            if(mainView.headerInfo && mainView.headerInfo.length>0 && records.length > 0 && showHeader){
                var headerInfo = new Ext.create(mainView.headerInfo);
                headerInfo.loadRecord(records[0])          
                MineralPro.config.Runtime.headerInfoXtype=headerInfo                
                Ext.globalEvents.fireEvent('updateHeader');
            }              
        }          

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onLoadSelectionChange.')
    },
    
    showEditViewViaHeader: function(){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController showEditViewViaHeader.')
        var me = this;
        var mainView = me.getView();
        var editView = Ext.widget(mainView.editFormAliasName);
        editView.editRecordIndex = mainView.rowIndex
        editView.down('form').loadRecord(mainView.headerDetail); 
        editView.show();    
        mainView.showEdit = false;
        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController showEditViewViaHeader.')
    }
      
});
