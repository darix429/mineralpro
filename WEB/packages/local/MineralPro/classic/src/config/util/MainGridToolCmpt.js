Ext.define('MineralPro.config.util.MainGridToolCmpt', {
    singleton: true, //default configuration for global variable

    addNewDataButton: {
        xtype: 'button',
        itemId: 'addNewDataButtonItemId',
        iconCls:MineralPro.config.Runtime.addNewBtnIconCls,
        listeners: {
            click: 'onOpenAddListForm',
            beforerender: function () {
                var me = this;
                me.tooltip = me.up('grid').addButtonTooltip
                me.text = me.up('grid').addButtonText
            },
            afterrender: function(){
                var me = this
                    me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    },
    saveToDbButton: {
        xtype: 'button',
        itemId: 'saveToDbButtonItemId',
        iconCls:MineralPro.config.Runtime.saveToDbBtnIconCls,
        tooltip:MineralPro.config.Runtime.saveGridBtnText,
        text:MineralPro.config.Runtime.saveGridBtnTooltip,
        disabled: true,
        listeners: {
            click: 'onSyncListGridData',
            afterrender: function(){
                var me = this
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    },
    clearFilterButton: {
        xtype: 'button',
        itemId: 'clearFilterButtonItemId',
        iconCls:MineralPro.config.Runtime.refreshBtnIconCls,
        tooltip:MineralPro.config.Runtime.refreshBtnTooltip,
        text:MineralPro.config.Runtime.refreshBtnText,
        itemId: 'clearFilterButton',
        listeners: {
            click: 'onClearListFilter'
        }
    },
    saveButton: {
        xtype: 'button',
        itemId: 'saveButtonItemId',
        iconCls:MineralPro.config.Runtime.saveToDbBtnIconCls,        
        listeners: {
            click: 'onSyncListGridData',
            beforerender: function () {
                var me = this;
                me.tooltip = me.up('panel').saveButtonTooltip
                me.text = me.up('panel').saveButtonText
            },
            afterrender: function(){
                var me = this
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    },
    cancelGridButton: {
        xtype: 'button',
        itemId: 'cancelGridButtonItemId',
        iconCls:MineralPro.config.Runtime.cancelGridIconCls,
        tooltip:MineralPro.config.Runtime.cancelGridTooltip,
        text:MineralPro.config.Runtime.cancelGridText,
        listeners: {
            click: 'onCancelGrid'
        }
    },
    
     comboSelection: {
        xtype: 'combo',
        width: 350,
        labelAlign: 'right',
        itemId: 'comboSelectionItemId',
        typeAhead: true,
//        forceSelection: true,
        queryMode: 'local',
        displayField: 'displayField',
        valueField: 'valueField',
        labelStyle: 'color: white;',
        listeners: {
            beforerender: function () {
                var me = this
                var grid = me.up('grid')
                me.setStore(grid.gridToolComboStore)
                me.setFieldLabel(grid.gridToolComboFieldLabel)
                me.emptyText = grid.gridToolComboEmptyText

            },
            /// added for focus on dropdown
            afterrender: function (field) {
                Ext.defer(function () {
                    field.focus(true, 100);
                }, 10);                
            },
            change: function (fieldLabel, newval, oldval) {
                var me = this
                var listStore = me.up('grid').getStore();
                listStore.getProxy().extraParams = {
                    sec: newval
                }
                listStore.load();                              
            },
//            beforequery: function(queryVV){
//                queryVV.combo.expand();
//                queryVV.combo.store.load();
//                return false;
//            }
        }
    }
    
})


    
