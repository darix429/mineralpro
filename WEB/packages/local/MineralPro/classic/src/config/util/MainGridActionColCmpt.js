Ext.define('MineralPro.config.util.MainGridActionColCmpt', {
    singleton: true, //default configuration for global variable

    editCls: {
        icon:MineralPro.config.Runtime.editCls,
        tooltip:MineralPro.config.Runtime.editTooltip,
        handler: function (grid, rowIndex, colIndex, item, e, record) {
            this.up('grid').getSelectionModel().select(rowIndex)
            if (record.get('rowDeleteFlag') != 'D') {
                this.up('grid').getController().onOpenEditListForm(grid, rowIndex, colIndex, item)           
            } else {
                Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.editDeletedDataWarningCls,
                    title:MineralPro.config.Runtime.editDeletedDataWarningTitle,
                    msg:MineralPro.config.Runtime.editDeletedDataWarningMsg,
                    buttons: Ext.Msg.OK
                });
            }
        }, 
        isDisabled: function (view, rowindex, colIndex, item, record) {
            if(record.get('collectionExtractDt')){
                if (record.get('collectionExtractDt') != '1900-01-01') {
                    return true;
               } 
            }
            if(view.grid.down('#saveToDbButtonItemId').hidden == true){
                   return true; 
            }
            if(record.get('editRecord') == false){
                return true; 
            }
        }        
    },
    deleteCls: {
        getClass: function (colConfig, cellData, record, rowIndex, colIndex, dataStore) {
            if (record.get('rowDeleteFlag') == 'D') {
                return(MineralPro.config.Runtime.undoDeleteCls);
            } else {
                return(MineralPro.config.Runtime.deleteCls);
            }
        },
        getTip: function (colConfig, cellData, record, rowIndex, colIndex, dataStore) {
            if (record.get('rowDeleteFlag') == 'D') {
                return (MineralPro.config.Runtime.undoDeleteTooltip);
            } else {
                return (MineralPro.config.Runtime.deleteTooltip);
            }
        },
        handler: function (gridView, rowIndex, colIndex, clickedItem, eventObj, record, row) {
            this.up('grid').getSelectionModel().select(rowIndex)
            if (record.get('rowDeleteFlag') == 'D') {
                record.set('rowDeleteFlag', record.previousValues.rowDeleteFlag);                                      
            } else {
                record.set('rowDeleteFlag', 'D');            
            }
        },
        isDisabled: function (view, rowindex, colIndex, item, record) {
            if(record.get('enableDeleteCount')){
                if (record.get('enableDeleteCount') != 0) {
                    return true;
               }
            }
            if(view.grid.down('#saveToDbButtonItemId').hidden == true){
                   return true; 
            }
            if(record.get('deleteRecord') == false){
                return true; 
            }
        }
    },
})


    