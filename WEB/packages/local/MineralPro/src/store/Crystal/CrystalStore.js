/**
 * Store for crystal reports db sync
 */

Ext.define('MineralPro.store.Crystal.CrystalStore', {
    extend: 'MineralPro.store.BaseStore',
	autoLoad: true,
    proxy: {
        api: {
            read: '/api/Crystal/syncToDB', 
        }      
    }
});
