Ext.define('MineralPro.store.CdReasonStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'MineralPro.model.JECodeTypeModel',

    proxy: {
        api: {
            read: '/api/Packages/MineralPro/ReadReasonCodeType',
        }      
    }
    
}); 
