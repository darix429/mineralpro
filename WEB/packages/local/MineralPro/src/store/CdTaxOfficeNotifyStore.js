Ext.define('MineralPro.store.CdTaxOfficeNotifyStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'MineralPro.model.JECodeTypeModel',

    proxy: {
        api: {
            read: '/api/Packages/MineralPro/ReadTaxOfficeNotifyCodeType',
        }      
    }
    
}); 
