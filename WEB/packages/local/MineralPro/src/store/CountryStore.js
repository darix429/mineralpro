Ext.define('MineralPro.store.CountryStore', {
    extend: 'MineralPro.store.BaseStore',
   // autoLoad: true,
    
    model: 'MineralPro.model.CountryModel',

    proxy: {
        api: {
            read: '/api/Packages/MineralPro/ReadCountry',
        }      
    }
    
}); 
