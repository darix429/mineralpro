
Ext.define('MineralPro.store.BaseStore', {
    extend: 'Ext.data.Store',
//    autoLoad: true,
    showMask: false,
    tabChangeReload: true,
    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success',
             messageProperty : 'data'
        },
        writer: {
            type: 'json',
            writeAllFields: 'true'
        }
    },
    listeners: {
        beforeload: function () {
            var me = this;
            var params = me.getProxy().extraParams
            params['appraisalYear'] = MineralPro.config.Runtime.appraisalYear;
            params['dataCount'] = me.getCount();
            params['idAppUser'] = MineralPro.config.Runtime.idAppUser;
            me.getProxy().extraParams = params

            if (me.showMask) {
                Ext.getBody().mask('Loading...')
            }

        },
        beforesync: function () {
            var me = this;
            var params = me.getProxy().extraParams
            params['appraisalYear'] = MineralPro.config.Runtime.appraisalYear;
            me.getProxy().extraParams = params
        },
        load: function (store, records, successful, operation, eOpts) {
            // Ext.getBody().unmask();
            if(successful){
                var me = this;
                if (me.showMask) {
                    Ext.globalEvents.fireEvent('checkLoadingStores');//located in CPLoginController                                                                
                }
            }
            else{
                var error = operation.getError() || '';
                var storeId = store.getId() || '';
                if(typeof error == 'string'){
                    error = error.toLowerCase();
                }
                else{
                    var statusText = error.statusText || '';
                    var errMsg = error.response && error.response.responseText ? error.response.responseText : '';
                        
                    error = {
                        statusText: statusText,
                        error: errMsg,
                        storeId: storeId
                    }
                    error = JSON.stringify(error);
                }
                if(error != 'do not refresh the data'){
                    MineralPro.config.RuntimeUtility.DebugLog('error while loading store ');
                    MineralPro.config.RuntimeUtility.DebugLog(error);
                    // Ext.Msg.show({
                    //     title: 'Error',
                    //     msg: 'Unable to load data',
                    //     buttons: Ext.Msg.OK,
                    // });

                    // var errorWindow = Ext.ComponentQuery.query('errorwindow')[0];
                    // if(!errorWindow){
                    //     Ext.create('widget.errorwindow',{ //This class has been defined on Minerals.config.view.Help.ErrorWindow
                    //         store: store,
                    //         error: error
                    //     }).show();
                    // }
                    // else{
                    //     //The error Window already exists. Just update its properties
                    //     errorWindow.store = store;
                    //     errorWindow.error = error;
                    //     errorWindow.down('#errorMsg').getEl().dom.querySelector('#expand p.store-id-cont').innerHTML = store.getId();
                    // }
                }
                else{
                    Ext.getBody().unmask();
                }
            }
        },
        update: function (store) {
            if(store.view != null && store.view != ''){
                var dirty = store.getNewRecords().length > 0
                        || store.getUpdatedRecords().length > 0
                        || store.getRemovedRecords().length > 0;
                if (dirty) {
                    if (store.view.down('#saveToDbButtonItemId')) {
                        store.view.down('#saveToDbButtonItemId').enable(true)
                    } else if (store.view.up('form')) {
                        if (store.view.up('form').down('#saveToDbButtonItemId')) {
                            store.view.up('form').down('#saveToDbButtonItemId').enable(true)
                        }
                    }
                } else {
                    if (store.view.down('#saveToDbButtonItemId')) {
                        store.view.down('#saveToDbButtonItemId').disable(true)
                    } else if (store.view.up('form')) {
                        if (store.view.down('#saveToDbButtonItemId')) {
                            store.view.down('#saveToDbButtonItemId').disable(true)
                        }
                    }
                }
            }
        },
        add: function (store) {
            if(store.view != null && store.view != ''){
                var dirty = store.getNewRecords().length > 0
                        || store.getUpdatedRecords().length > 0
                        || store.getRemovedRecords().length > 0;
                if (dirty) {
                    if (store.view.down('#saveToDbButtonItemId')) {
                        store.view.down('#saveToDbButtonItemId').enable(true)
                    } else if (store.view.up('form')) {
                        if (store.view.down('#saveToDbButtonItemId')) {
                            store.view.down('#saveToDbButtonItemId').enable(true)
                        }
                    }
                } else {
                    if (store.view.down('#saveToDbButtonItemId')) {
                        store.view.down('#saveToDbButtonItemId').disable(true)
                    } else if (store.view.up('form')) {
                        if (store.view.up('form').down('#saveToDbButtonItemId')) {
                            store.view.up('form').down('#saveToDbButtonItemId').disable(true)
                        }
                    }
                }
            }
        }
    }
});