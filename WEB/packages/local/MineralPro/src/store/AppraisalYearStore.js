Ext.define('MineralPro.store.AppraisalYearStore', {
    extend: 'MineralPro.store.BaseStore',
   // autoLoad: true,
    
    model: 'MineralPro.model.BaseModel',

    proxy: {
        api: {
            read: '/api/Packages/MineralPro/ReadAppraisalYear',
        }      
    },
    listeners: {
        beforeload: function () {
            var me = this;
            var params = me.getProxy().extraParams
            params['idAppUser'] = MineralPro.config.Runtime.idAppUser;
            params['appraisalYear'] = MineralPro.config.Runtime.appraisalYear;
            me.getProxy().extraParams = params

            if (me.showMask) {
                Ext.getBody().mask('Loading...')
            }

        },
        beforesync: function () {
            var me = this;
            var params = me.getProxy().extraParams
            params['idAppUser'] = MineralPro.config.Runtime.idAppUser;
            params['appraisalYear'] = MineralPro.config.Runtime.appraisalYear;
            me.getProxy().extraParams = params
        },
    }
    
}); 
