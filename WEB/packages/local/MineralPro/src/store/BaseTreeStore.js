
Ext.define('MineralPro.store.BaseTreeStore', {
    extend: 'Ext.data.TreeStore',
    
    listeners:{
        beforeload: function(){
            var me = this;
            var params = me.getProxy().extraParams
            params['appraisalYear']=MineralPro.config.Runtime.appraisalYear;         
            me.getProxy().extraParams=params
        },
        beforesync: function(){
            var me = this;
            var params = me.getProxy().extraParams
            params['appraisalYear']=MineralPro.config.Runtime.appraisalYear;
            me.getProxy().extraParams=params
        }
    }
});
