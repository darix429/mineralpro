Ext.define('MineralPro.store.CdJeHoldStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'MineralPro.model.JECodeTypeModel',

    proxy: {
        api: {
            read: '/api/Packages/MineralPro/ReadJeHoldCodeType',
        }      
    },
    listeners: {
        load: function(store){
            var rec = { 'displayField': '-', 'ValueField': 0 };
            store.insert(0,rec);    
        }
    }
    
}); 
