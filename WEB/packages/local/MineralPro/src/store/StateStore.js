Ext.define('MineralPro.store.StateStore', {
    extend: 'MineralPro.store.BaseStore',
 //   autoLoad: true,
    
    model: 'MineralPro.model.StateModel',

    proxy: {
        api: {
            read: '/api/Packages/MineralPro/ReadState',
        }      
    }
    
}); 
