Ext.define('MineralPro.sub.CommonFunctionsNote.store.NoteStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
  
    model: 'MineralPro.sub.CommonFunctionsNote.model.NoteModel',
    proxy: {

        api: {
        //    create: '',
            read: '/api/MineralPro/CommonFunctionNote/ReadNote',
       //     update: ''
//            destroy: ''
        }
       
    }
}); 
