Ext.define('MineralPro.model.StateModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'stateCd', type: "int", defaultValue: 1},
        {name: 'stateName', type: "string", defaultValue: ''},
    ]
});