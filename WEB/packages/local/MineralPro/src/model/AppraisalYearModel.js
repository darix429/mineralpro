Ext.define('MineralPro.model.AppraisalYearModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'appraisalYear', type: "string", defaultValue: ''},
        {name: 'activeAppraisalYear', type: "string", defaultValue: ''},
        {name: 'appUser', type: "string", defaultValue: ''},
        {name: 'idJournalEntry', type: "string", defaultValue: ''},
        {name: 'batchId', type: "string", defaultValue: ''},
    ]
});