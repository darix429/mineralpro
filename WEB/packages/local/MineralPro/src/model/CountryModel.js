Ext.define('MineralPro.model.CountryModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'countryCd', type: "int", defaultValue: 1},
        {name: 'countryName', type: "string", defaultValue: ''},
    ]
});