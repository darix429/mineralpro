Ext.define('MineralPro.model.JECodeTypeModel', {
    extend: 'Ext.data.Model',
    fields:[
        {name: 'valueField', type: "string", defaultValue: ''},
        {name: 'displayField', type: "string", defaultValue: ''},
        {name: 'shortdesc', type: "string", defaultValue: ''}
    ]
});