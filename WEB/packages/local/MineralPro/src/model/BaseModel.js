Ext.define('MineralPro.model.BaseModel', {
    extend: 'Ext.data.Model',
    requires: [
        'Ext.data.Field'
    ],

    fields: [{
            name: 'rowUpdateDt',
            persist: false
        },{
            name: 'rowUpdateUserid',
            type: 'int',
            defaultValue: 1
        },{
            name: 'rowUpdateUserName',
            defaultValue: ''
        },{
            name: 'rowDeleteFlag',           
            defaultValue: ''
        }]
})