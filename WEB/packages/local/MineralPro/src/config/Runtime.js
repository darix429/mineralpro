/**
 * Contains the global configuration during runtime.
 */

Ext.define('MineralPro.config.Runtime', {
    singleton: true, //default configuration for global variable
    debugMode: true, //boolean (true: enable logging, false: disable logging)
    
//*************************************
//**System information configurations**
//*************************************
    
    //global variable used in the application
    addController:'',
    addControllerAlert:'',
    access_level: false,
    readOnly: true,
    //below are user informations
    appName: '',
    headerInfoXtype: '',
    
    //below are user informations   
    idAppUser:'',
    idOrganization: '',
    organizationName: '',
    organizationType: '',
    idSecurityGroup: '',
    securityGroup:'',    
    emailAddress:'',
	ameAbbreviation:'',
    //appraisalYear
    appraisalYear: '',
    
    editCls: '../resources/icons/pencil.png',//'edit',
    editTooltip: 'Click to edit',
    
    
    
//**************************Used for util functions***********************    
    //Action column cls
    deleteCls: 'delete',
    deleteTooltip: 'Delete record',
    undoDeleteCls: 'undo',
    undoDeleteTooltip: 'Undo Delete',
    
    //save button cls
    saveToDbBtnIconCls: 'save',
    addNewBtnIconCls: 'add',
    
    //save grid btn to database configuration
    saveGridBtnText: 'Save to Database',
    saveGridBtnTooltip: 'Save to Database',
    
    //refresh button filter configuration
    refreshBtnIconCls: 'refreshFilter',
    refreshBtnTooltip: 'Refresh Filter',
    refreshBtnText: 'Undo Filter',
    
    //List sync to grid messages and alerts no data
    listSyncMsgErrorIcon: 'wrn',
    listSyncMsgErrorTitle: 'Error',
    listSyncMsgErrorMsg: 'There is no new data to be saved.',
    
    //add data to grid error
    addEditToGridMsgErrorIcon: 'wrn',
    addEditToGridMsgErrorTitle: 'Error',
    addEditToGridMsgErrorMsg: 'Please check data on the highlighted fields.',
    
    //Note sync to grid messages and alerts with data
    syncGridMsgConfimationIcon: 'wrn',
    syncGridMsgConfimationTitle: 'Confirm',
    syncGridMsgConfimationMsg: 'Are you sure you want to commit changes?',

     //crystal report button filter configuration
    crystalIconCls: '../resources/icons/crystal-reports.png',
    crystalTooltip: 'Crystal Report',

    //list seletion unsaved data
    addExistingDataWarningCls: 'wrn',
    addExistingDataWarningTitle: 'Alert',
    addExistingDataWarningMsg: 'Data is already in the database.',
    
    //list seletion unsaved data
    editDeletedDataWarningCls: 'wrn',
    editDeletedDataWarningTitle: 'Alert',
    editDeletedDataWarningMsg: 'Please undelete data before editing.',


    //tab close confirmation
    tabCloseConfirmationIcon: 'wrn',
    tabCloseConfirmationTitle: 'Confirm',
    tabCloseConfirmationMsg: 'Do you want to save changes to database?',
	
	//tab message when leaving unsaved changes
    tabChangeConfirmationTitle: 'Warning',
    tabChangeConfirmationMsg: 'Do you want to save changes to database?',
    tabChangeConfirmationIcon: 'wrn',
    
    duplicateAddressAlertIcon: 'wrn',
    duplicateAddressAlertTitle: 'Alert',
    duplicateAddressAlertMsg: 'Please check addess is duplicated.',
     
    //refresh button filter configuration
    cancelGridIconCls: 'delete',
    cancelGridTooltip: 'Cancel Changes',
    cancelGridText: 'Cancel',

    //List sync to grid messages and alerts no data
    searchMsgErrorIcon: 'wrn',
    searchMsgErrorTitle: 'Error',
    searchMsgErrorMsg: 'Please enter seach value.',

    
})
