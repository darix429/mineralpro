# MineralPro/sass/etc

This folder contains miscellaneous SASS files. Unlike `"MineralPro/sass/etc"`, these files
need to be used explicitly.
