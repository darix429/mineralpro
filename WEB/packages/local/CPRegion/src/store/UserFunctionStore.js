/**
 * Store for active user application
 */

Ext.define('CPRegion.store.UserFunctionStore', {
    extend: 'MineralPro.store.BaseStore',
    model: 'CPRegion.model.UserFunctionModel',
    proxy: {
        type: 'ajax',
        url: '/api/Packages/CPRegion/GetUserFunction', 
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
       }
    }
});
