/**
 * Store for active user application
 */

Ext.define('CPRegion.store.JEStore', {
     extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'CPRegion.model.JEModel',

    proxy: {
        api: {
            read: '/api/Packages/CPRegion/GetJEData',
            create: '/api/Packages/CPRegion/CreateJEData',
           // update: '/api/Packages/CPRegion/UpdateJEData',
        }      
    }
});
