/**
 * Store for active user application
 */

Ext.define('CPRegion.store.HeaderStore', {
    extend: 'MineralPro.store.BaseStore',
    model: 'CPRegion.model.HeaderModel',
    proxy: {
        type: 'ajax',
        url: '/api/Packages/CPRegion/GetHeaderData', 
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
       }
    }
});
