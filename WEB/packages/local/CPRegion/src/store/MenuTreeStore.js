/**
 * Store for the Navigation Tree
 */

Ext.define('CPRegion.store.MenuTreeStore', {
    extend: 'MineralPro.store.BaseTreeStore',
    model: 'CPRegion.model.TreeModel',
    proxy: {
        type: 'ajax',
        url: '/api/Packages/CPRegion/GetMenuTree', 
        reader: {
            type: 'json',
            successProperty: 'success'
        },
        sorters: [{
            property: 'text',
            direction: 'ASC'
        }]
    }
});

