/**
 * Model for objects being manipulated by the user
 */

Ext.define('CPRegion.model.JEModel', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'idJournalEntry',
        defaultValue: ''
    },{
        name: 'appUser',
        defaultValue: MineralPro.config.Runtime.idAppUser
    },{
        name: 'appraisalYear',
        defaultValue: ''    
    },{
        name: 'batchId',
        defaultValue: ''
    },{
        name: 'openDt',
        defaultValue: ''
    },{
        name: 'closeDt',
        defaultValue: '1900-01-01'
    },{
        name: 'jeReasonCd',
        defaultValue: ''
    },{
        name: 'description',
        defaultValue: ''
    },{
        name: 'taxOfficeNotifyCd',
        defaultValue: ''
    },{
        name: 'jeHoldCd',
        defaultValue: ''
    },{
        name: 'collectionExtractDt',
        defaultValue: '1900-01-01'
    },{
        name: 'selectedAppraisalYear',
        defaultValue: ''
    },{
        name: 'nameAbbreviation',
        defaultValue: ''
    },{
        name: 'batchId',
        defaultValue: ''
    }]
});