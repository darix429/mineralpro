/**
 * Model for objects being manipulated by the user
 */

Ext.define('CPRegion.model.HeaderModel', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'accountNumber',
        defaultValue: ''
    },{
        name: 'geoAccountNumber',
        defaultValue: ''
    },{
        name: 'appraisalYear',
        defaultValue: ''    
    },{
        name: 'ownerName',
        defaultValue: ''
    },{
        name: 'situsAddress',
        defaultValue: ''
    },{
        name: 'totalMarketValue',
        defaultValue: ''
    },{
        name: 'lastValueMethod',
        defaultValue: ''
    },{
        name: 'lastValuationYear',
        defaultValue: ''
    },{
        name: 'excemptionList',
        defaultValue: ''
    }]
});