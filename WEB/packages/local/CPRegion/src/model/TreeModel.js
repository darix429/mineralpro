/**
 * Model for Navigation Tree view
 */

Ext.define('CPRegion.model.TreeModel', {
    extend : 'Ext.data.Model',
    fields : [ {
        name : 'lockedInd',
        defaultValue: 0
    }, {
        name : 'idSubSystemFunction',
        defaultValue: 1
    }, {
        name : 'subSystemName',
        defaultValue: ''
    }, {
        name : 'functionName',
        defaultValue: ''
    }, {
        name : 'text',
        defaultValue: ''
    }, {
        name : 'expanded',
        type : 'boolean',
        defaultValue: false
    }, {
        name : 'leaf',
        type : 'boolean',
        defaultValue: false
    }, {
    name: 'qtip',
    mapping: 'text'
    },]
});

