/**
 * Model for objects being manipulated by the user
 */

Ext.define('CPRegion.model.UserFunctionModel', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'idAppUser',
        defaultValue: 1
    },{
        name: 'idSubSystemFunction',
        defaultValue: 1
    },{
        name: 'idOrganization',
        defaultValue: 1
    },{
        name: 'subSystem',
        defaultValue: ''
    },{
        name: 'subSystemDisplay',
        defaultValue: ''
    },{
        name: 'appFunction',
        defaultValue: ''
    },{
        name: 'appFunctionDisplay',
        defaultValue: ''
    },{
        name: 'securityCd',
        defaultValue: 1
    },{
        name: 'activeTabInd',
        defaultValue: 0
    }]
});