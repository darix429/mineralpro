Ext.define('CPRegion.config.RuntimeUtility', {
    statics: {
        onTabChange: function (tab) {
            var me = this;               
            MineralPro.config.RuntimeUtility.DebugLog('Entering RuntimeUtility onTabChange.')  
            var arrayGrid = tab.getActiveTab().query('grid')
            Ext.each(arrayGrid, function (grid) {
                grid.getStore().reload({ 
                    callback: function() {
                        grid.getSelectionModel().select(0, false, false);
                    }
                });
            })
            MineralPro.config.RuntimeUtility.DebugLog('Leaving RuntimeUtility onTabChange.')
        }

    }
})