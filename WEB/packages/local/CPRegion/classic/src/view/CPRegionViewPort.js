/**
 * Main view port of the application
 */

Ext.define('CPRegion.view.CPRegionViewPort', {
    extend : 'Ext.container.Viewport',
    alias : 'widget.mainViewPort',
    requires : ['Ext.layout.container.Border'],
    layout: 'border',
    defaults: {
        collapsible: true,
        split: true
    },
    items: [{
        region: 'north',
        xtype : 'northView',
        height: 140 //Original Height 100
    },{
        region : 'west',
        xtype : 'westView',
        width : 250,
    },{
        region : 'center',
        xtype : 'centerView',  
        collapsible: false 
    }]
});

