///**
// * View for the north region of the regionwork
// */
Ext.define('CPRegion.view.region.NorthView', {
    extend: 'Ext.panel.Panel',
//    extend: 'Ext.container.Container',
    requires: ['CPRegion.view.region.NorthViewController'],
    controller: 'northViewController',
    alias: ['widget.northView'],

    addFormAliasName: 'jEAddFormView',
    mainListModel: 'CPRegion.model.JEModel',
    store: 'CPRegion.store.JEStore',							
    height: '100%',
    header: false,
    title: 'MineralPro Header',
    layout: 'hbox',
    width: '100%',
    id: 'northViewId',
    bodyStyle: {
		backgroundColor: '#12548f',
		color: '#ffffff'
    },
    initComponent: function () {
        var me = this;
        me.items = [{
                xtype: 'container',
                width: '20%',
                items: {
                    xtype: 'image',
                    src: '../resources/images/MineralPro-logo.png',
                    margin: '30 0 0 15',
                    height: 55,
                    cls: 'homeImage',
                    
                    listeners:{
                        el: {
                            click: function() {
                                var url = window.location.href
                                var appName = MineralPro.config.Runtime.appName.replace(' ', '').trim()
                                url = url.toLowerCase().replace(appName.toLowerCase(), 'CPHome')
                                window.open(url);
                            }
                        }
                    }
                },

            }, {
                xtype: 'container',
                itemId: 'headerForm',
                width: '80%',
                layout: 'vbox',
				cls: 'headerForm',
                defaults: {
                    xtype: 'container',
                    width: '100%'
                },
                items: [{                    
                    layout: 'hbox',
                    margin: '5, 5, 5, 5',
                    defaults: {
                        xtype: 'container',
                        height: 30,
                        layout: 'hbox',
                        },
					
                    items: [{      
                            width: '60%',
                            items: [{
                                fieldLabel: 'Appraisal Year',
                                labelWidth: 100,
                                xtype: 'combobox',
                                itemId: 'yearItemId',
                                displayField: 'appraisalYear',
                                valueField: 'appraisalYear',
                                queryMode: 'local',
                                style: {
                                        color: '#ffffff'								
                                },
                                store: 'MineralPro.store.AppraisalYearStore',
                                tabIndex: 1,
                                width: 200,
                                enableKeyEvents: true,
                                listeners: {
                                    change: 'onChangeAppraisalYear',
                                    // beforequery: function(queryVV){
                                    //     queryVV.combo.expand();
                                    //     queryVV.combo.store.load();
                                    //     return false;
                                    // }
                                }
                            },{
                                name: 'idJournalEntry',   
                                xtype: 'hidden',
                                margin: '0 0 10',
                                itemId:'idJournalEntryItemId',
                                labelAlign: 'right',     
                            },{
                                name: 'batchId',   
                                xtype: 'hidden',
                                margin: '0 0 10',
                                itemId:'batchIdItemId',
                                labelAlign: 'right',     
                            },{
                                xtype: 'button',
                                text: 'Start Journal Entry',
                                itemId: 'startJEItemId',     
                                margin: '3, 0, 0, 10',
                                hidden: true,
                                listeners: {
                                    click: 'onClickStartJe',
                                    afterrender: function(){
                                        var me = this;                                              
                                        var combo = me.up('northView').down('#yearItemId');
                                        combo.setValue(MineralPro.config.Runtime.appraisalYear)
                                        }                                 
                                }
                            },{
                                xtype: 'button',
                                text: 'Stop Journal Entry',
                                itemId: 'stopJEItemId',     
                                margin: '3, 0, 0, 10',
                                hidden: true,
                                listeners: {
                                    click: 'onClickStopJe',
                                    afterrender: function(){
                                        var me = this;                                              
                                        var combo = me.up('northView').down('#yearItemId');
                                        combo.setValue(MineralPro.config.Runtime.appraisalYear)
                                        }                                 
                                }
                            }]                                         
//                        },{  
//                            width: '33%',
//                            items: {
//                                xtype: 'displayfield',
//                                itemId: 'headerTitleItemId',
//                            }      
                        },{
                            width: '39%',
                            items: {               									
                                xtype: 'panel',
                                width: '100%',
                                layout: {
                                    type: 'hbox',
                                    align: 'bottom',
                                    pack: 'end',
                                },
                                bodyStyle: {
                                    backgroundColor: 'transparent',
									color: '#ffffff',									
                                },				

                                items:[{
                                    xtype : 'image',
                                    glyph : 'xf007@FontAwesome',
                                    itemId : 'iconuser',
                                    id : 'iconuser',
                                    padding: 5,
                                    docked: 'right'
                                },{
                                    xtype: 'label',
                                    html: MineralPro.config.Runtime.emailAddress,
                                    itemId: 'welcome_user',
                                    padding: 5,
                                    cls: 'user-font'
                                },{
                                    xtype: 'button',
                                    glyph : 'xf08b@FontAwesome',
                                    text: 'Logout',
                                    tooltip: 'Logout User',
                                    border:false,
                                    padding: 5,
                                    docked: 'right',
                                    listeners: {
                                        click: function (){
                                            onLogout()
                                        }
                                    }
                                }]	
                            }							                              
                        }]

                },{
                    itemId: 'headerInfoItemId',
                    height: 95,//Original value 60 
                    layout: 'vbox',             
                }]


            }]



        me.callParent(arguments);
    }

})