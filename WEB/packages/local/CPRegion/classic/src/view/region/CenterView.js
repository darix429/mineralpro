///**
// * View for the center region of the regionwork
// */
Ext.define('CPRegion.view.region.CenterView', {
    extend: 'Ext.tab.Panel',
    alias: ['widget.centerView'],
    requires: ['CPRegion.view.region.CenterViewController'],
    controller: 'centerViewController',
    itemId: 'centerView',
    id: 'centerView',
    defaults: {
        bodyPadding: 5
    },
    autoDestroy: false,
    closeAction: 'hide',
    items: [{           
            title: 'Introduction',
            itemId: 'introductionItemId',
            xtype: 'defaultPageView',
            hidden: true,
//            closable: true,
//            html: 'Introduction for the first time user can be added here...'
    }],
    initComponent: function() {       
        var me = this;

        me.listeners= {
            tabchange: me.getController().onTabChange,
            beforetabchange: me.getController().onBeforeTabChange,
            remove: me.getController().onTabRemove,
            beforeremove: me.getController().onTabBeforeRemove
            }
      
        me.callParent(arguments);        
    }
});
