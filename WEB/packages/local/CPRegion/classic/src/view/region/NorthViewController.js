//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('CPRegion.view.region.NorthViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.northViewController',
   
    onChangeAppraisalYear: function(combo, newVal, oldVal, record){
        MineralPro.config.RuntimeUtility.DebugLog('Entering NorthViewController onChangeAppraisalYear.')
        MineralPro.config.Runtime.appraisalYear = combo.getValue()

        var comboStore = combo.getStore()
        var selectedRecord = comboStore.findRecord('appraisalYear', combo.getValue());
        var selectedRecordOld = comboStore.findRecord('appraisalYear', oldVal);
        MineralPro.config.Runtime.readOnly = true;   
        var stopJe = true;     
        var mainViewPort = combo.up('mainViewPort');
        var westViewTreeArray = mainViewPort.down('westView').query('treepanel');
        var centerView = mainViewPort.down('centerView');      
        var jeButton = combo.up('northView').down('#startJEItemId');
        var jeStopButton = combo.up('northView').down('#stopJEItemId');     
        var buttonArr = centerView.query('button')
        var actionColArr = centerView.query('actioncolumn')
        var idJournalEntry = selectedRecord.get('idJournalEntry')
        var batchId = selectedRecord.get('batchId')
        combo.up('northView').down('#batchIdItemId').setValue(batchId);
        combo.up('northView').down('#idJournalEntryItemId').setValue(idJournalEntry);

        if(MineralPro.config.Runtime.appName == 'Information Services'){
            MineralPro.config.Runtime.readOnly = false;
            Ext.globalEvents.fireEvent('changeAppraisalYear');
            Ext.each(westViewTreeArray, function(westViewTree){
                var params = westViewTree.getStore().getProxy().extraParams
                params['optionJE'] = 'startJE';
                westViewTree.getStore().getProxy().extraParams = params
                westViewTree.getStore().reload();           
            })
            buttonArr = centerView.query('button')
            Ext.each(buttonArr, function(button){
                if (button.itemId != 'clearFilterButton' && button.itemId != 'searchButtonItemId'
                        && button.xtype != 'tab' && button.alwaysShow != true)
                    button.setHidden(MineralPro.config.Runtime.readOnly)
            });
            
            Ext.each(actionColArr, function(actionCol){
                if(actionCol.alwaysShow != true){
                    actionCol.setHidden(MineralPro.config.Runtime.readOnly)
                }
                if(actionCol.itemId){
                    actionCol.setHidden(false)
                }
                    
            });
            
            var activeTab = centerView.getActiveTab();
            if(activeTab.activeTab){
                activeTab = activeTab.getActiveTab();
            }
            
            var grid = activeTab.down('grid');
            if(grid){
                grid.getStore().reload();
            }
            
        }
        else {
            if(selectedRecord.get('activeAppraisalYear') == 'Y'){
                var checkJE = true;
                MineralPro.config.Runtime.readOnly = false;
                if(oldVal){
                    if(selectedRecordOld.get('appUser')){
                        stopJe = this.StopJe(
                            selectedRecordOld.get('idJournalEntry'),
                            selectedRecordOld.get('batchId'),
                            oldVal,
                            MineralPro.config.Runtime.readOnly
                        );  
                    }else{
                        comboStore.each(function(c, fn){
                            if(c.data.appUser){
                                checkJE = false;
                                combo.setValue(c.data.appraisalYear);
                                jeStopButton.show();
                                jeButton.hide(); 
                            }
                        })
                    } 
                } 
                if(checkJE){
                    jeButton.hide();
                    jeStopButton.hide();

                    MineralPro.config.Runtime.readOnly = false;


                }
            }
            else {
                if(selectedRecord.get('appUser')){
                    
                    Ext.Msg.show({
                        iconCls: MineralPro.config.Runtime.listSyncMsgErrorIcon,
                        title: 'Alert!',    
                        msg: 'JE '+ idJournalEntry +' for Appraisal Year '+ newVal +' is Open, please Close JE.',               
                        buttons: Ext.Msg.OK,
                        closable: false,
                        fn: function(){ 
                            buttonArr = centerView.query('button')
                            actionColArr = centerView.query('actioncolumn')
                            MineralPro.config.Runtime.readOnly = false;
                            Ext.each(westViewTreeArray, function(westViewTree){
                                var params = westViewTree.getStore().getProxy().extraParams
                                params['optionJE'] = 'startJE';
                                westViewTree.getStore().getProxy().extraParams = params
                                westViewTree.getStore().reload();           
                            })
                            
                            Ext.each(buttonArr, function(button){
                                if (button.itemId != 'clearFilterButton' && button.itemId != 'searchButtonItemId'
                                        && button.xtype != 'tab' && button.alwaysShow != true)
                                    button.setHidden(MineralPro.config.Runtime.readOnly)
                            });

                            Ext.each(actionColArr, function(actionCol){
                                    if(actionCol.alwaysShow != true){
                                        actionCol.setHidden(MineralPro.config.Runtime.readOnly)
                                    }
                            });

                            var activeTab = centerView.getActiveTab();
                            if(activeTab.activeTab){
                                activeTab = activeTab.getActiveTab();
                            }

                            var grid = activeTab.down('grid');
                            if(grid){
                                grid.getStore().reload();
                            }                       
                        }
                    }); 
                    jeStopButton.show();
                    
                }
                else {
                    
                    jeButton.show(); 
                    MineralPro.config.Runtime.readOnly = true;
                    if(oldVal){
                        if(selectedRecordOld.get('appUser')){
                            stopJe = this.StopJe(
                                selectedRecordOld.get('idJournalEntry'),
                                selectedRecordOld.get('batchId'),
                                oldVal,
                                MineralPro.config.Runtime.readOnly
                            );  
                        }else{
                            comboStore.each(function(c, fn){
                                if(c.data.appUser){
                                    combo.setValue(c.data.appraisalYear);
                                    jeStopButton.show();
                                    jeButton.hide(); 
                                }
                            })
                        } 
                    }   
                }
            }
            Ext.globalEvents.fireEvent('changeAppraisalYear');
            Ext.each(westViewTreeArray, function(westViewTree){
                var params = westViewTree.getStore().getProxy().extraParams
                params['optionJE'] = 'stopJE';
                westViewTree.getStore().getProxy().extraParams = params
                westViewTree.getStore().reload();           
            })
            buttonArr = centerView.query('button')
            Ext.each(buttonArr, function(button){
                if (button.itemId != 'clearFilterButton' && button.itemId != 'searchButtonItemId'
                        && button.xtype != 'tab' && button.alwaysShow != true)
                    button.setHidden(MineralPro.config.Runtime.readOnly)
            });
            
            Ext.each(actionColArr, function(actionCol){
                if(actionCol.alwaysShow != true){
                    actionCol.setHidden(MineralPro.config.Runtime.readOnly)
                }
                if(actionCol.itemId){
                    actionCol.setHidden(false)
                }
                    
            });
            
            var activeTab = centerView.getActiveTab();
            if(activeTab.activeTab){
                activeTab = activeTab.getActiveTab();
            }

            //reload all stores with 'Combo' in the name
            var allStoreArr = Ext.StoreMgr.items
            Ext.each(allStoreArr, function(store){
                if(store.getId().indexOf('Minerals') != -1){
                    if(store.showMask != true){
                        store.reload();
                    }
                }
            })
                        
            //put API for allowing store to refetch the data.           
            var grid = activeTab.down('grid');
            if(grid){
                Ext.Ajax.request({
                    url: '/api/Packages/CPRegion/ChangeAppraisalYear',
                    method: 'POST',
                    params: {
                        idAppUser: MineralPro.config.Runtime.idAppUser,
                    },
                    success: function (form, data) {
                        var hasFilterAllRowsPlugin = grid.getPlugin('filterAllRows');
                        if(hasFilterAllRowsPlugin){
                            var filterData = grid.getFilterData();//getFilterData is bound on FilterAllRows Plugin
                            var filterCount = Ext.Object.getValues(filterData).filter(function(val){return val.trim();}).length;
                            if(!filterCount){
                                grid.clearGridFilters();
                            }
                        }
                        grid.getStore().reload();
                    }
                });

                var mainPanel = grid.up(grid.mainPanelAlias);
                if(mainPanel && grid.mainPanelAlias !== '' && grid.mainPanelAlias !== undefined){
                    if(mainPanel.selectedId){
                        mainPanel.setActiveTab(0);
                    }
                    
                } else {
                    var form = activeTab.down('form')
                    if(form && form.mainPanelAlias !== '' && form.mainPanelAlias !== undefined){
                        var mainView = form.up(form.mainPanelAlias);
                        mainView.setActiveTab(0);
                    } else {
                        console.log('MainPanelAlias is not Defined')
                    }
                }
            }
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving NorthViewController onChangeAppraisalYear.')
    },
    
     onClickStartJe: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering NorthViewController onClickStartJe.')
		var me = this;  
        var view = me.getView();
        var YearSelected = view.down('#yearItemId').value;
        var addView = Ext.widget(view.addFormAliasName);
        var record = Ext.create(view.mainListModel); 

        record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
        record.set('nameAbbreviation', MineralPro.config.Runtime.nameAbbreviation)
        record.set('selectedAppraisalYear', YearSelected)
        
        addView.down('form').loadRecord(record);
        addView.show();
        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving NorthViewController onClickStartJe.')
	},
    onClickStopJe: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering NorthViewController onClickStopJe.')
		var me = this;  
        var view = me.getView();
        var YearSelected = view.down('#yearItemId').value;
        var batchId = view.down('#batchIdItemId').value;
        var idJournalEntry = view.down('#idJournalEntryItemId').value;
        this.StopJe(idJournalEntry,batchId); 
        MineralPro.config.RuntimeUtility.DebugLog('Leaving NorthViewController onClickStopJe.')
	},    
    saveJournalEntry: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering NorthViewController saveJournalEntry.') 
        var me = this;
        var view = me.getView();
        var win = button.up('window');
        var listStore = Ext.StoreMgr.lookup('JEStore');
        var northView = Ext.getCmp('northViewId');
        var startButton = northView.down('#startJEItemId');
        var stopButton = northView.down('#stopJEItemId');
        var comboStore = northView.down('#yearItemId').getStore();
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues(); 
        if(form.isValid()){
            record.set(values);   
            listStore.insert(0,record);
            var dirty = listStore.getNewRecords().length > 0
                    || listStore.getUpdatedRecords().length > 0
                    || listStore.getRemovedRecords().length > 0;
                    if(dirty){
                        Ext.Msg.show({
                        title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                        msg:'Are you sure you want to commit changes and Start Jouranl Entry?',
                        iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                        buttons: Ext.Msg.YESNO,
                        scope: this,
                        width: 250,
                        fn: function (btn) {
                            if (btn === 'yes') {
                                Ext.getBody().mask('Loading...') 
                                win.mask('Loading...')
                                listStore.getProxy().extraParams = {
                                    createLength: listStore.getNewRecords().length,
                                    jeProcess: 'start' 
                                };
                                var APIresponse = false;
                                listStore.sync({
                                    callback: function (records, operation, success) {
                                       // console.log(records.operations[0].error.response.responseText);
                                       // check if the API return is success or not
                                         if(!records.operations[0].success){
                                             Ext.Msg.show({
                                                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                                                title:'Process Failed',    
                                                msg:'Oops! There was an error in generating your JE! <br/> Please send a screenshot of this error to your Mineral.Pro representative and try your JE again. <br/><br/>'+records.operations[0].error.response.responseText,               
                                                buttons: Ext.Msg.OK
                                            });
                                            listStore.remove(record, true);
                                            win.unmask();
                                            Ext.getBody().unmask();
                                         }else{
                                            
                                            comboStore.load({
                                                callback: function(records, operation, success) {
                                                var appraisalYearStore = this.findRecord('appraisalYear', northView.down('#yearItemId').value);
                                                northView.down('#idJournalEntryItemId').setValue(appraisalYearStore.get('idJournalEntry'));
                                                northView.down('#batchIdItemId').setValue(appraisalYearStore.get('batchId'));
                                                }
                                            });
                                            startButton.hide();
                                            stopButton.show();
                                            APIresponse = true;
                                            win.unmask();
                                            win.close();
                                            
                                        }
                                  
                                        if(APIresponse){
                                            
                                            var mainViewPort = northView.up('mainViewPort');
                                            var westViewTreeArray = mainViewPort.down('westView').query('treepanel');
                                            var centerView = mainViewPort.down('centerView');      
                                            var buttonArr = centerView.query('button');
                                            var actionColArr = centerView.query('actioncolumn');
                                            MineralPro.config.Runtime.readOnly = false;
                                            Ext.each(westViewTreeArray, function(westViewTree){
                                                var params = westViewTree.getStore().getProxy().extraParams
                                                params['optionJE'] = 'startJE';
                                                westViewTree.getStore().getProxy().extraParams = params
                                                westViewTree.getStore().reload();           
                                            })
                                            
                                            Ext.each(buttonArr, function(button){
                                                if (button.itemId != 'clearFilterButton' && button.itemId != 'searchButtonItemId'
                                                        && button.xtype != 'tab' && button.alwaysShow != true)
                                                    button.setHidden(MineralPro.config.Runtime.readOnly)
                                            });

                                            Ext.each(actionColArr, function(actionCol){
                                                    if(actionCol.alwaysShow != true){
                                                        actionCol.setHidden(MineralPro.config.Runtime.readOnly)
                                                    }
                                            });

                                            var activeTab = centerView.getActiveTab();
                                            if(activeTab.activeTab){
                                                activeTab = activeTab.getActiveTab();
                                            }

                                            var grid = activeTab.down('grid');
                                            if(grid){
                                                grid.getStore().reload();
                                            }
                                            Ext.getBody().unmask() 
                                            
                                        }
                                    }
                                });
                            }
                        }
                        })
                    }
         }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        }    
        MineralPro.config.RuntimeUtility.DebugLog('Leaving NorthViewController saveJournalEntry.')				  
    },
    	StopJe: function(idJournalEntry,batchId, oldVal, readOnly){
        MineralPro.config.RuntimeUtility.DebugLog('Entering NorthViewController StopJe.')
        var me = this;  
        var view = me.getView();
        var YearSelected = view.down('#yearItemId').value;
        var record = Ext.create(view.mainListModel); 
        var listStore = Ext.StoreMgr.lookup('JEStore');
        var startButton = view.down('#startJEItemId');
        var stopButton = view.down('#stopJEItemId');
        var comboStore = view.down('#yearItemId').getStore();

        record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
        record.set('nameAbbreviation', MineralPro.config.Runtime.nameAbbreviation)
        record.set('selectedAppraisalYear', YearSelected)
        record.set('idJournalEntry', idJournalEntry)
        record.set('batchId', batchId)
        listStore.insert(0,record);

        var dirty = listStore.getNewRecords().length > 0
                    || listStore.getUpdatedRecords().length > 0
                    || listStore.getRemovedRecords().length > 0;
                    if(dirty){
                        Ext.Msg.show({
                        title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                        msg:'Are you sure you want to Stop Journal Entry?',
                        iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                        buttons: Ext.Msg.YESNO,
                        scope: this,
                        width: 250,
                        fn: function (btn) {
                            if (btn === 'yes') {
                                Ext.getBody().mask('Loading...') 
                                listStore.getProxy().extraParams = {
                                    createLength: listStore.getNewRecords().length,
                                    jeProcess: 'stop' 
                                };
                                var APIresponse = false;
                                listStore.sync({
                                    callback: function (records, operation, success) {
                                        // check if the API return is success or not
                                        if(!records.operations[0].success){
                                            Ext.Msg.show({
                                                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                                                title:'Process Failed', 
                                                //msg:'test',   
                                                msg:'Oops! There was an error in stopping your JE! Please send a screenshot of this error to your Mineral.Pro representative and try your JE again. <br/><br/>'+records.operations[0].error.response.responseText,               
                                                buttons: Ext.Msg.OK
                                            });
                                            
                                            listStore.remove(record, true);
                                            Ext.getBody().unmask();
                                        }else{
                                            APIresponse = true;
                                            comboStore.load();
                                            if(readOnly){
                                                startButton.show();
                                            }
                                            if(!oldVal){
                                                startButton.show();
                                            }
                                            stopButton.hide();
                                            Ext.getBody().unmask();
                                        }
                                        if(APIresponse){
                                            var mainViewPort = view.up('mainViewPort');
                                            var westViewTreeArray = mainViewPort.down('westView').query('treepanel');
                                            var centerView = mainViewPort.down('centerView');      
                                            var buttonArr = centerView.query('button');
                                            var actionColArr = centerView.query('actioncolumn');
                                            if(readOnly == undefined){
                                                readOnly = true;
                                            }
                                            MineralPro.config.Runtime.readOnly = readOnly;

                                            //var readOnly = true;

                                            Ext.each(westViewTreeArray, function(westViewTree){
                                                var params = westViewTree.getStore().getProxy().extraParams
                                                params['optionJE'] = 'stopJE';
                                                westViewTree.getStore().getProxy().extraParams = params
                                                westViewTree.getStore().reload();           
                                            })

                                            Ext.each(buttonArr, function(button){
                                                if (button.itemId != 'clearFilterButton' && button.itemId != 'searchButtonItemId'
                                                        && button.xtype != 'tab' && button.alwaysShow != true)
                                                    button.setHidden(MineralPro.config.Runtime.readOnly)
                                            });

                                            Ext.each(actionColArr, function(actionCol){
                                                    if(actionCol.alwaysShow != true){
                                                        actionCol.setHidden(MineralPro.config.Runtime.readOnly)
                                                    }
                                                    if(actionCol.itemId){
                                                        actionCol.setHidden(false)
                                                    }
                                            });

                                            var activeTab = centerView.getActiveTab();
                                            if(activeTab.activeTab){
                                                activeTab = activeTab.getActiveTab();
                                            }

                                            var grid = activeTab.down('grid');
                                            if(grid){
                                                grid.getStore().reload();
                                            }
                                        }
                                    }
                                });

                            }else{
                                listStore.remove(record, true);
                                if(oldVal){
                                    view.down('#yearItemId').setValue(oldVal);
                                    startButton.hide();
                                }
                            }
                        }
                        })
                    }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving NorthViewController StopJe.')
	},
});
