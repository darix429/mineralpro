///**
// * View for the west region of the regionwork
// */

Ext.define('CPRegion.view.region.WestView', {
    extend: 'Ext.panel.Panel',
    header: false,
    alias: 'widget.westView',
    requires: ['Ext.data.TreeStore', 'CPRegion.view.region.WestViewController'],
    controller: 'westViewController',
//    layout: 'fit',
 	bodyStyle:{
		'background-color': '#32404e',
	},    
    initComponent: function () {
        var me = this;               
        me.items = [{
            xtype: 'treepanel',
            store: 'FavoriteTreeStore',
            rootVisible: false,
            itemId: 'favoriteTreeId',
            minHeight: 0,
            maxHeight: 200,
            title: 'My Favorite',
            columns: [{
                    xtype: 'treecolumn',
                    width: '85%',
                    dataIndex: 'text',
                }, {
                    xtype: 'actioncolumn',
                    width: '9%',
                    items: [{
                            icon: '../resources/icons/remove_favorite.png',
                            tooltip: 'Remove from favorites',
                            hidden: true,
                            handler: function (grid, rowIndex, colIndex, item, e, record) {
                                me.getController().onProcessFavorite('removeFavorite',grid, record)                           
                            }
                    }]
            }],
            listeners: {
                select: me.getController().onTreeNodeSelect,
                beforeselect: me.getController().nodeBeforeSelect,
                itemmouseenter: function (view, record, item, rowIndex, e, eOpts) {
                    if(record.get('leaf')){
                        var cmp = Ext.select('#' + Ext.get(item).id + ' [src~="../resources/icons/remove_favorite.png"]'); 
                        cmp.removeCls('x-hidden-display')
                        cmp.show();
                    }
                },
                itemmouseleave: function (view, record, item, rowIndex, e, eOpts) {
                    var cmp = Ext.select('#' + Ext.get(item).id + ' [src~="../resources/icons/remove_favorite.png"]');
                    cmp.hide();
                }
            },
            viewConfig: {
                loadMask: false
            }
        },{
            xtype: 'treepanel',
            store: 'MenuTreeStore',
            itemId: 'menuTreeId',
            rootVisible: false,
            title: 'Menu Selection',
//            maxHeight: Ext.getBody().getViewSize().height  - 300,
            columns: [{
                    xtype: 'treecolumn',
                    width: '85%',
                    height: '20',
                    dataIndex: 'text',
                }, {
                    xtype: 'actioncolumn',
                    width: '9%',
                    items: [{
                            icon: '../resources/icons/add_favorite.png',
                            tooltip: 'Add to favorites',
							
                            hidden: true,
                            handler: function (grid, rowIndex, colIndex, item, e, record) {
                                me.getController().onProcessFavorite('addFavorite',grid, record)
                            }
                    }]
            }],
            listeners: {
                select: me.getController().onTreeNodeSelect,
                beforeselect: me.getController().nodeBeforeSelect,
                itemmouseenter: function (view, record, item, rowIndex, e, eOpts) {
                    if(record.get('leaf')){
                        var cmp = Ext.select('#' + Ext.get(item).id + ' [src~="../resources/icons/add_favorite.png"]'); 
                        cmp.removeCls('x-hidden-display')
                        cmp.show();
                    }
                },
                itemmouseleave: function (view, record, item, rowIndex, e, eOpts) {
                    var cmp = Ext.select('#' + Ext.get(item).id + ' [src~="../resources/icons/add_favorite.png"]');
                    cmp.hide();
                }
            },
            viewConfig: {
                loadMask: false
            }
        }],              
        me.callParent(arguments);
    },
    listeners: {
        resize: function(panel, width, height, oldWidth, oldHeight, eOpts){
            var favTree = panel.down('#favoriteTreeId');
            var menuTree = panel.down('#menuTreeId');           
            menuTree.setMaxHeight(panel.getHeight()-favTree.getHeight())
        }
    }
});