//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('CPRegion.view.region.WestViewController', {
    extend : 'Ext.app.ViewController',
    alias: 'controller.westViewController',
    
    /**
     * Check for access if granted or not (read/write=2, readonly=1, noaccess=0)
     * Also responsible for adding controller.
     */
    nodeBeforeSelect: function (view, record, index, e) {     
        var me = this;
        var centerView=me.up('mainViewPort').down('centerView');
        Ext.getBody().mask('Loading...')
        var task = new Ext.util.DelayedTask(function () {      
        MineralPro.config.RuntimeUtility.DebugLog('Entering WestController nodeBeforeSelect.')
        if (record && record != 'I') {
            if (record.get('leaf') == true) {
                if (record.get('lockedInd') == '0') {
                    MineralPro.config.RuntimeUtility.ShowAlertOk('Alert', 'You have no access on this function.')
                    Ext.getBody().unmask()
                    return false;
                } else {
                    var appName = record.get('appName').replace(/ /g, '');
                    var subSystem = record.get('subSystemName').replace(/ /g, '');
                    var appFunction = record.get('functionName').replace(/ /g, '');                  
                    
                    if(subSystem.toLowerCase() == 'commonfunctions'){
                        appName = 'MineralPro';
                    }
                    
                    var addController = appName+'.sub.' + subSystem + appFunction+ '.controller.' + subSystem + appFunction + 'Controller';                          
                    var addControllerAlert = '<div style="text-align:center"> ' +
                                'Function <b>' + record.data.text + '</b> under ' + record.parentNode.data.text +
                                ' is not yet on the system.<br>Please contact support.</div>';
                        
                    MineralPro.config.Runtime.addController = addController;
                    MineralPro.config.Runtime.addControllerAlert = addControllerAlert;
                    Ext.globalEvents.fireEvent('addFunctionControllerSelect');                    
                }
            }
        }       
        MineralPro.config.RuntimeUtility.DebugLog('Leaving WestController nodeBeforeSelect.')
        
        });        
        task.delay(400); 
    },
    
    /**
     *Resposible for adding/focusing tab in center view if node was selected.
     */
    onTreeNodeSelect: function(view, record, item, index, event) {
        var me = this;
        var centerView=me.up('mainViewPort').down('centerView');
        var task = new Ext.util.DelayedTask(function () {
                
        MineralPro.config.RuntimeUtility.DebugLog('Entering WestViewController onTreeNodeSelect.')
               
        if(item > 0 && record.get('leaf') == true){                      

            var tabName = record.get('text').replace(/ /g, '');   
            var appFunction = record.get('functionName').replace(/ /g,''); 
            var subSystem = record.get('subSystemName').replace(/ /g, '');
            // var addTabPanel=true;

            // //get items on each tab and check if tab already exist
            // centerView.items.each(function(c){
            //     if(c.itemId == appFunction){
            //         addTabPanel=false;
            //     }//end if(c.name == name)
            // })//end tabPanel.items.each
            
            // if(addTabPanel){                                  
            //     if(record.get('lockedInd') == '1'){                 
            //         MineralPro.config.Runtime.access_level = true;                          
            //     }else if(record.get('lockedInd') == '2'){                 
            //         MineralPro.config.Runtime.access_level = false;                          
            //     }        
            //     centerView.add({
            //         xtype: subSystem+appFunction,
            //         title: tabName,
            //         itemId: appFunction,
            //         closable: true,
            //         height: '100%',
            //         record: record,
            //         autoDestroy: false,
            //         closeAction: 'hide'
            //     })              
            //     me.up('westView').getController().addUserFunctionStore(record.get('idSubSystemFunction'))        
            // }//end if(addTabPanel)
            // centerView.setActiveTab(appFunction); 
            var tab = Ext.ComponentQuery.query(subSystem + appFunction)[0];
            var isTabInCenterView = centerView.down(subSystem + appFunction);
            if(!tab && !isTabInCenterView){
                if(record.get('lockedInd') == '1'){                 
                    MineralPro.config.Runtime.access_level = true;                          
                }else if(record.get('lockedInd') == '2'){                 
                    MineralPro.config.Runtime.access_level = false;                          
                }
                centerView.add({
                    xtype: subSystem + appFunction,
                    title: tabName,
                    itemId: appFunction,
                    closeAction: 'hide',
                    height: '100%',
                    autoDestroy: false,
                    closable: true,
                    record: record
                });
                me.up('westView').getController().addUserFunctionStore(record.get('idSubSystemFunction'));
            }
            else if(tab && !isTabInCenterView){
                centerView.insert(tab);
            }
            centerView.setActiveTab(appFunction);
        }
        Ext.globalEvents.fireEvent('checkLoadingStores');//located in CPLoginController
        MineralPro.config.RuntimeUtility.DebugLog('Leaving WestViewController onTreeNodeSelect.')
        
        });        
        task.delay(600); 
        
    },
    /**
     * Responsible for adding the function in user_app_object table in the database
     * @param(var) obj
     */
    addUserFunctionStore: function(obj){
        MineralPro.config.RuntimeUtility.DebugLog('Entering WestController addUserFunctionStore.')
        var me = this,
        UserFunctionStore = Ext.StoreMgr.lookup('UserFunctionStore'); 
                                     
        UserFunctionStore.getProxy().extraParams = {
            process: 'insert',
            idAppUser: MineralPro.config.Runtime.idAppUser,
            idOrganization: MineralPro.config.Runtime.idOrganization,
            idSubSystemFunction: obj,
            appName: MineralPro.config.Runtime.appName
        };                                 
        UserFunctionStore.load();
        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving WestController addUserFunctionStore.')
    },//end addUserFunctionStore
    
    onProcessFavorite: function(process, grid, record){
        MineralPro.config.RuntimeUtility.DebugLog('Entering WestController onProcessFavorite.')  
        Ext.Ajax.request({
            url: '/api/Packages/CPRegion/ProcessFavoriteTree',
            method: 'GET',
            params: {
                process: process,
                idAppUser: MineralPro.config.Runtime.idAppUser,
                idSubSystemFunction: record.get('idSubSystemFunction')
            },
            success: function (form, data) {               
              Ext.StoreMgr.lookup('MenuTreeStore').reload();
              Ext.StoreMgr.lookup('FavoriteTreeStore').reload();
            }
        });    
        
        var panel = this.getView();
        var favTree = panel.down('#favoriteTreeId');
        var menuTree = panel.down('#menuTreeId');           
        menuTree.setMaxHeight(panel.getHeight()-favTree.getHeight())
        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving WestController onProcessFavorite.')
    }//end onProcessFavorite
});