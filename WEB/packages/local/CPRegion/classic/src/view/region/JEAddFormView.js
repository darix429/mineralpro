
Ext.define('CPRegion.view.region.JEAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['CPRegion.view.region.NorthViewController'],
    controller: 'northViewController',
    alias: 'widget.jEAddFormView',
    id: 'jEAddFormViewId',
    title: 'Add Journal Entry',
    defaultFocus: '#jeReasonCditemId',
    bodyPadding: 5,
    width: 800,
    height: 485,
    mainPanelId: 'northViewId', //id of the main panel
    northViewStore: 'CPRegion.store.JEStore', //store for note grid
    northViewAlias: 'northView',
    listGridStore: 'Minerals.sub.ProperRecordsManageJournalEntry.store.JournalEntriesStore', //store for Lease Owner grid
    listGridAlias: 'JournalEntriesGridView', 
    initComponent: function() {
        var me = this;

        me.items = [{
            xtype: 'form',
            defaults:{ maskRe: /[^'\^]/},
            margin:'0 5 0 0',
            bodyPadding: 5,
            items: [{
                    xtype: 'container',
                    combineErrors: true,
                    layout: 'hbox', 
                    labelAlign: 'right',
                    defaults: {
                        width:'25%',
                    },
                    items: [{
                        fieldLabel: 'Appraisal Year',
                        xtype: 'displayfield',
                        labelAlign: 'right',
                        msgTarget: 'side', 
                        name: 'selectedAppraisalYear', 
                    },{
                        fieldLabel: 'BatchId',
                        xtype: 'displayfield',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        hidden: true,
                        name: 'batchId',
                        itemId: 'batchIdItemId',  
                    },{
                        fieldLabel: 'Open Date',
                        xtype: 'displayfield',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        name: 'openDt', 
                        hidden: true,
                        itemId: 'openDtItemId', 
                    },{
                        fieldLabel: 'Close Date',
                        xtype: 'displayfield',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        name: 'closeDt',
                        hidden: true, 
                        itemId: 'closeDtItemId', 
                    }]
                },{
                    xtype: 'container',
                    combineErrors: true,
                    layout: 'hbox', 
                    labelAlign: 'right',
                    defaults: {
                        width:'50%',
                    },
                    items: [{
                        fieldLabel: 'User',
                        xtype: 'displayfield',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        name: 'nameAbbreviation', 
                    },{
                        fieldLabel: 'Collection Extract Date',
                        xtype: 'displayfield',
                        labelWidth: '35px',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        hidden: true,
                        name: 'collectionExtractDt',
                        itemId: 'collectionExtractDtItemId',  
                    }]
                },{
                    xtype: 'fieldcontainer',
                    fieldLabel: 'Reason',
                    combineErrors: true,
                    layout: 'hbox', 
                    labelAlign: 'right',
                    defaults: {
                        hideLabel: 'true'
                    },
                    items: [
                    //     {
                    //     fieldLabel: 'Reason',
                    //     xtype: 'textfield',
                    //     //selectOnFocus: true,
                    //     tabIndex: 1,
                    //     itemId: 'reasonItemId',
                    //     labelWidth: 70,
                    //     name : 'reason',
                    //     margin: '0 0 10',
                    //     labelAlign: 'right',
                    //     allowBlank: false,
                    //     maxLength: 50,
                    //     listeners:{
                    //         change: function(a, newValue, oldValue){
                    //             var reasonStore = me.down('#jeReasonCditemId').getStore();
                    //             var record = reasonStore.findRecord('shortdesc', newValue);
                    //             if (record){
                    //                 var value = record.get('valueField');
                    //                 me.down('#jeReasonCditemId').setValue(value);
                    //             }else{
                    //                 me.down('#jeReasonCditemId').setValue('');
                    //             }
                    //         }
                    //     }
                    // }, 
                    {
                        fieldLabel: 'Reason Combobox',
                        selectOnFocus: true,
                        tabIndex: 1,
                        name : 'jeReasonCd',
                        xtype:'combo',
                        flex: 2,
                        itemId:'jeReasonCditemId',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        displayField: 'displayField',
                        valueField: 'valueField',   
                        store: 'MineralPro.store.CdReasonStore',
                        //typeAhead: true,
                        allowBlank: true,
                        queryMode:'local',
                        listeners:{
                            change: function(a, newValue, oldValue){
                                var reasonStore = a.getStore();
                                var record = reasonStore.findRecord('valueField', newValue);
                                me.down('#jeReasonItemId').setValue(me.down('#jeReasonCditemId').getRawValue());
                                // if (record){
                                //     var value = record.get('shortdesc');
                                //     me.down('#reasonItemId').setValue(value);
                                // }else{
                                //     me.down('#reasonItemId').setValue('');
                                // }
                            },
                        },
                        forceSelection: true,
                        msgTarget: 'side' 
                    },{
                        name: 'jeReason',
                        xtype: 'hidden',
                        margin: '0 0 10',
                        itemId:'jeReasonItemId',
                        labelAlign: 'right',     
                    }]
                }, {
                   fieldLabel: 'Description',
                    xtype: 'textareafield',
                    name : 'description',
                    itemId: 'descriptionId',
                    tabIndex: 2,
                    width: 770,
                    height: 170, 
                    labelAlign: 'right',
                    //allowBlank: false,
					enforceMaxLength: true,
					maxLength: 255,
					//bind: '{value}',
					
                },
                //  {
				// 	xtype: 'box',
				// 	msgTarget: 'side' ,
				// 	bind: '{count}' + '/255',
				// 	height: 32,
				// 	style: {
				// 		textAlign: 'right'							
				// 	},
				// },
                 {
                    xtype: 'container',
                    layout: 'hbox',
                    margin: '0 0 5 0',
                    labelAlign: 'right',
                    items: [{
                        fieldLabel: 'Tax Office',
                        selectOnFocus: true,
                        tabIndex: 3,
                        name : 'taxOfficeNotifyCd',
                        xtype:'combo',
                        itemId:'taxOfficeNotifyCdItemId',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        displayField: 'displayField',
                        valueField: 'valueField',
                        flex: 1,   
                        store: 'MineralPro.store.CdTaxOfficeNotifyStore',
                        typeAhead: true,
                        allowBlank: true,
                        queryMode:'local',
                        listeners:{
                            Select:function(record){ 
                                me.down('#taxOfficeNotifyItemId').setValue(me.down('#taxOfficeNotifyCdItemId').getRawValue());
                            }
                        }, 
                        forceSelection: true,
                        msgTarget: 'side' 
                    },{
                        name: 'taxOfficeNotify',
                        xtype: 'hidden',
                        margin: '0 0 10',
                        itemId:'taxOfficeNotifyItemId',
                        labelAlign: 'right',     
                    }, {
                        fieldLabel: 'Hold JE',
                        selectOnFocus: true,
                        tabIndex: 4,
                        name : 'jeHoldCd',
                        flex: 1,
                        xtype:'combo',
                        itemId:'jeHoldCdItemId',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        displayField: 'displayField',   
                        valueField: 'valueField',   
                        store: 'MineralPro.store.CdJeHoldStore',
                        typeAhead: true,
                        allowBlank: true,
                        queryMode:'local',
                        listeners:{
                            Select:function(record){ 
                                me.down('#jeHoldItemId').setValue(me.down('#jeHoldCdItemId').getRawValue());
                            }
                        }, 
                        forceSelection: true,
                        msgTarget: 'side' 
                    },{
                        name: 'jeHold',
                        xtype: 'hidden',
                        margin: '0 0 10',
                        itemId:'jeHoldItemId',
                        labelAlign: 'right',     
                    }]
                }]
            }];
        this.callParent(arguments);
    },
	// viewModel: {

    //         formulas: {
    //             count: {
    //                 bind: '{value}',
    //                 get: function(value){
    //                     return value.length;
    //                 }
    //             }
    //         }
    //     },
    buttons: [{
            xtype: 'button',
            text: 'OK',
            itemId: 'okItmeId',
           // disabled: true,
            listeners: {
                click: 'saveJournalEntry'
            }
        },
	
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});