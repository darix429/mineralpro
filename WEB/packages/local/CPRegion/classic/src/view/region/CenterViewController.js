//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('CPRegion.view.region.CenterViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.centerViewController',
    init: function () {
        var me = this
        me.addSessionTab()
    },
    /**
     * Resposible for adding the active tab during creation
     */
    addSessionTab: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering CenterViewController addSessionTab.')
        var me = this;
        var centerView = me.getView();
        var UserFunctionStore = Ext.StoreMgr.lookup('UserFunctionStore');
            
        Ext.getBody().mask('Loading...')
//        Ext.getBody().suspendEvents(true);
        
//        var task = new Ext.util.DelayedTask(function () {           
            UserFunctionStore.getProxy().extraParams = {
                process: 'select',
                idAppUser: MineralPro.config.Runtime.idAppUser,
                idOrganization: MineralPro.config.Runtime.idOrganization,
                idSecurityGroup: MineralPro.config.Runtime.idSecurityGroup,
                appName: MineralPro.config.Runtime.appName
            };

            UserFunctionStore.load({
                callback: function (records, operation, success) {
                    if (success) {
                        if (records.length > 0)
//                            centerView.items.items[0].close()
                        var menuTreeStore = Ext.StoreMgr.lookup('MenuTreeStore').lastOptions.node.childNodes;
                        var favoriteTreeStore = Ext.StoreMgr.lookup('FavoriteTreeStore').lastOptions.node.childNodes;
                        var leaf;
                        var appName;
                        var subSystem;
                        var appFunction;
                        var addController;
                        var tabName;

                        for (var i = 0; i < records.length; i++) {
                            
                            if (records[i].get('securityCd') == 1) {
                                MineralPro.config.Runtime.access_level = true;
                            } else if (records[i].get('securityCd') == 2) {
                                MineralPro.config.Runtime.access_level = false;
                            }
                            
                            //this portion will get its tree record for tab change selection
                            for (var x = 0; x < menuTreeStore.length; x++) {
                                if (menuTreeStore[x].get('text') == records[i].get('subSystemNameDisplay')) {
                                    for (var y = 0; y < menuTreeStore[x].childNodes.length; y++) {
                                        if (menuTreeStore[x].childNodes[y].data.text == records[i].get('functionNameDisplay')) {
                                            leaf = menuTreeStore[x].childNodes[y];
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }
                            
                              //this portion will get its tree record for tab change selection
                            for (var x = 0; x < favoriteTreeStore.length; x++) {
                                if (favoriteTreeStore[x].get('text') == records[i].get('subSystemNameDisplay')) {
                                    for (var y = 0; y < favoriteTreeStore[x].childNodes.length; y++) {
                                        if (favoriteTreeStore[x].childNodes[y].data.text == records[i].get('functionNameDisplay')) {
                                            leaf = favoriteTreeStore[x].childNodes[y];
                                            break;
                                        }
                                    }
                                    break;
                                }
                            }

                            appName = records[i].get('appName').replace(/ /g, '');
                            subSystem = records[i].get('subSystemName').replace(/ /g, '');
                            appFunction = records[i].get('functionName').replace(/ /g, '');
                            tabName = records[i].get('functionNameDisplay').replace(/ /g, '');
                            
                            if(subSystem.toLowerCase() == 'commonfunctions'){
                                appName = 'MineralPro';
                            }
                            
                            addController = appName+'.sub.' + subSystem + appFunction + '.controller.' + subSystem + appFunction + 'Controller'
                                                     
                            MineralPro.config.Runtime.addController = addController;
                                                                           
                            Ext.globalEvents.fireEvent('addFunctionControllerLoad');
                            var tab = Ext.ComponentQuery.query(subSystem + appFunction)[0];
                            var isTabInCenterView = centerView.down(subSystem + appFunction);
                            if(!tab){
                                centerView.add({
                                    xtype: subSystem + appFunction,
                                    title: tabName,
                                    itemId: appFunction,
                                    closeAction: 'hide',
                                    height: '100%',
                                    autoDestroy: false,
                                    closable: true,
                                    record: leaf
                                })
                            }
                            else if(tab && !isTabInCenterView){
                                centerView.insert(tab);
                            }
                            if (records[i].get('activeTabInd') == 'Y') {
                                centerView.setActiveTab(appFunction);
                            }
                        }
                        if(centerView.getActiveTab().title == 'Introduction' && centerView.items.items[1]){
                            centerView.setActiveTab(1);
                        }
                    }
                    Ext.getBody().unmask()
//                    Ext.getBody().resumeEvents(true);
                }
            });
            
            MineralPro.config.RuntimeUtility.DebugLog('Leaving CenterViewController addSessionTab.')

//        });
//        task.delay(400);

    }, //end addSessionTab
    /**
     * Resposible for focusing selected tab on the navigation Tree.
     */
    onTabChange: function (tabPanel, newCard, oldCard, eOpts) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering CenterViewController onTabChange.')
        if(newCard.title == 'Introduction' && tabPanel.items.items[1]){
            tabPanel.setActiveTab(tabPanel.items.items[1]);
        }
        else{
            var me = this;
            var UserFunctionStore = Ext.StoreMgr.lookup('UserFunctionStore');           
            var menuTree = me.up('mainViewPort').down('westView').down('#menuTreeId');
            var favoriteTree = me.up('mainViewPort').down('westView').down('#favoriteTreeId');
            Ext.getBody().mask('Loading...')                            
            //        var task = new Ext.util.DelayedTask(function () {   
                if (newCard.record){
                    menuTree.getSelectionModel().select(newCard.record, true, true)
                    favoriteTree.getSelectionModel().select(newCard.record, true, true)
                }else{
                    menuTree.getSelectionModel().deselectAll(true);
                    favoriteTree.getSelectionModel().deselectAll(true);
                }
                if (newCard.record) {

                    setTimeout(function(){ 
                        UserFunctionStore.getProxy().extraParams = {
                            process: 'update',
                            idAppUser: MineralPro.config.Runtime.idAppUser,
                            idOrganization: MineralPro.config.Runtime.idOrganization,
                            idSubSystemFunction: newCard.record.get('idSubSystemFunction'),
                            appName: MineralPro.config.Runtime.appName
                        };

                        UserFunctionStore.load();
                    }, 2000);
                }


                var activeTab = tabPanel.getActiveTab();
                var grid = activeTab.down('grid')                    

                if(grid){
                    //                Ext.suspendLayouts();
                    //                Ext.getBody().suspendEvents(true);
                    var gridStore = grid.getStore();
                    if(gridStore.tabChangeReload){
                        if(activeTab.selectedId && activeTab.getActiveTab().down('grid')){
                            activeTab.getActiveTab().down('grid').getStore().reload();
                        }
                        gridStore.reload({ 
                            callback: function() {
                                if(activeTab.selectedId){
                                    var index = grid.getStore().find(activeTab.selectedIdIndex, activeTab.selectedId, false, false, true);
                                    grid.getSelectionModel().deselectAll();
                                    if (index == -1){
                                        index = 0;
                                    }
                                    grid.getSelectionModel().select(index, false, false);
                                }else{
                                    grid.getSelectionModel().deselectAll();
                                    grid.getSelectionModel().select(0, false, false);
                                }

                                setTimeout(function (){                             
                                    if(grid.down('#'+grid.firstFocus))
                                        grid.down('#'+grid.firstFocus).filterElement.focus();
                                }, 300);                      
                                //                        Ext.getBody().resumeEvents(true);
                                //                        Ext.resumeLayouts();
                            }
                        });                    
                    }else{
                        setTimeout(function(){
                            grid.getSelectionModel().deselectAll();                       
                            if(activeTab.selectedId){                            
                               var index = grid.getStore().find(activeTab.selectedIdIndex, activeTab.selectedId, false, false, true);                       
                               if (index == -1){
                                   index = 0;
                               }                        
                               grid.getSelectionModel().select(index, false, false);                        
                           }else{
                               grid.getSelectionModel().select(0, false, false);                       
                           }                    
                           Ext.globalEvents.fireEvent('checkLoadingStores');//located in CPLoginController   
                       }, 500)
                    }               
                }else{                
                 Ext.getBody().unmask() 
             }

             var centerView = me.up('mainViewPort').down('centerView');   
             var buttonArr = centerView.query('button')
             var actionColArr = centerView.query('actioncolumn')
             Ext.each(buttonArr, function(button){
                if (button.itemId != 'clearFilterButton' && button.itemId != 'searchButtonItemId'
                    && button.xtype != 'tab' && button.alwaysShow != true)
                    button.setHidden(MineralPro.config.Runtime.readOnly)
            });

             Ext.each(actionColArr, function(actionCol){
                if(actionCol.alwaysShow != true){
                    actionCol.setHidden(MineralPro.config.Runtime.readOnly)
                }
                if(actionCol.itemId){
                    actionCol.setHidden(false)
                }
            });
            // me.up('mainViewPort').down('northView').down('#yearItemId').fireEvent('change',
            //    me.up('mainViewPort').down('northView').down('#yearItemId'),
            //    me.up('mainViewPort').down('northView').down('#yearItemId').getValue(),
            //    me.up('mainViewPort').down('northView').down('#yearItemId').getValue())
            // reload all store under that tab
            // CPRegion.config.RuntimeUtility.onTabChange(me);
            // Ext.getBody().unmask()
            // });
            // task.delay(400);
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CenterViewController onTabChange.')
    },//end onTabChange
    onTabBeforeRemove: function(tab, cmpToRemove, eOpts){
        MineralPro.config.RuntimeUtility.DebugLog('Entering CenterViewController onTabBeforeRemove.');
        MineralPro.config.RuntimeUtility.DebugLog(tab);
        var remainingTabs = tab.items.items.filter(function(item){ return !item.config.hidden;}).length;
        if(remainingTabs == 1){
            Ext.toast({
                title: 'Oops!',
                html: 'The system needs at least one tab',
                iconCls: 'fa fa-warning',
                align: 't'
            });
            MineralPro.config.RuntimeUtility.DebugLog('Leaving CenterViewController onTabBeforeRemove.')
            return false;
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CenterViewController onTabBeforeRemove.')
        return true;
    },
    /**
     * Resposible for updating data in the database that a tab is removed.
     */
    onTabRemove: function (tabPanel, newCard, oldCard, eOpts) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering CenterViewController onTabRemove.')
        var me = this;
        var UserFunctionStore = Ext.StoreMgr.lookup('UserFunctionStore');

        if (newCard.record) {
            UserFunctionStore.getProxy().extraParams = {
                process: 'delete',
                idAppUser: MineralPro.config.Runtime.idAppUser,
                idOrganization: MineralPro.config.Runtime.idOrganization,
                idSubSystemFunction: newCard.record.get('idSubSystemFunction'),
                appName: MineralPro.config.Runtime.appName
            };
            UserFunctionStore.load({
                callback: function(){
                    Ext.getBody().unmask() 
                }
            });
        }

        if (me.items.length == 0) {
            var menuTree = me.up('mainViewPort').down('westView').down('#menuTreeId');
            var favoriteTree = me.up('mainViewPort').down('westView').down('#favoriteTreeId');
                menuTree.getSelectionModel().select(newCard.record, true, true)
                favoriteTree.getSelectionModel().select(newCard.record, true, true)
        }
        // if(tabPanel.items.items.length > 1){
        //     if(tabPanel.getActiveTab().title == 'Introduction'){
        //         tabPanel.setActiveTab(tabPanel.items.items[1])
        //     }
        // }
        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CenterViewController onTabRemove.')
    },
    onBeforeTabChange: function (tabPanel, newCard, oldCard, eOpts) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering CenterViewController onBeforeTabChange.')
               
        Ext.get(document.activeElement).blur();
        
//        var me=this
        var activeTab = oldCard
        var tabMainGrid = activeTab.down('grid');
        if(tabMainGrid && tabMainGrid.headerInfoAlias){
            var headerInfo = Ext.ComponentQuery.query(tabMainGrid.headerInfoAlias)[0];
            if(headerInfo && typeof headerInfo.getForm == 'function' && headerInfo.getForm()){
                headerInfo.getForm().reset(true);
                headerInfo.destroy();
            }
        }
        if(oldCard.down('tab')){
            activeTab = oldCard.getActiveTab()
        }
        var saveData = false
        var saveStore = []
        var arrayGrid= activeTab.query('grid'); 
//        setTimeout(function (){                     
            for(var x=0; x<arrayGrid.length; x++){
                var store = arrayGrid[x].getStore();
                if(store.getNewRecords().length > 0 
                        || store.getUpdatedRecords().length > 0 
                        || store.getRemovedRecords().length > 0){
                        saveStore.push(store)
                        saveData = true
                }   
                x=arrayGrid.length
            }

            if(saveData){
                var error = false;
                var totalInterest = 0;
                if(activeTab.xtype == 'MineralsLeaseOwners'){
                    for(var x=0; x<arrayGrid.length; x++){
                        var store = arrayGrid[x].getStore();
                        store.clearFilter(true);
                        Ext.each(store.data.items, function(el, index, items){
                            if(el.data.rowDeleteFlag == ''){
                                if(el.data.interestType == 'UNASSIGNED') {
                                    error = true;    
                                }
                                if(el.data.interestPercent > 1 || el.data.interestPercent == '-0') {
                                    error = true;    
                                }
                                totalInterest = parseFloat(totalInterest)+parseFloat(el.data.interestPercent);
                            }
                        })
                    }
                    if(parseFloat(totalInterest*1).toFixed(8) <= 1){
                        saveData = true;
                    } else {
                        saveData = false;
                    }
                }

                if(error){
                    Ext.Msg.show({
                        title:MineralPro.config.Runtime.tabChangeConfirmationTitle,    
                        msg:'There were Errors in the Grid, Your Data Will Not Be Saved ...<br /> Do you want to Proceed?',
                        iconCls:MineralPro.config.Runtime.tabChangeConfirmationIcon,
                        buttons: Ext.Msg.YESNO,               
                        scope: this,
                        width: 450,
                        fn: function(btn) { 
                            if (btn === 'yes'){
                                Ext.each(saveStore, function(store){                 
                                    store.removeAll();
                                    store.rejectChanges();                                
                                }); 
                                tabPanel.setActiveTab(newCard)
                            } else  {

                            }
                        }
                    });
                } else {
                    Ext.Msg.show({
                        title:MineralPro.config.Runtime.tabChangeConfirmationTitle,    
                        msg:MineralPro.config.Runtime.tabChangeConfirmationMsg,
                        iconCls:MineralPro.config.Runtime.tabChangeConfirmationIcon,
                        buttons: Ext.Msg.YESNO,               
                        scope: this,
                        width: 250,
                        fn: function(btn) { 
                            if (btn === 'yes'){
                                if(saveData){
                                    if(parseFloat(totalInterest*1).toFixed(8)<1 && activeTab.xtype == 'MineralsLeaseOwners'){
                                        Ext.Msg.show({
                                            title:'Warning',
                                            msg:'Total interest percent is not equal to 100%. <br />Do you want to proceed?',
                                            iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                                            buttons: Ext.Msg.YESNO,
                                            scope: this,
                                            width: 300,
                                            fn: function (btn) {
                                                if (btn === 'yes') {
                                                    Ext.each(saveStore, function(store){    
                                                        var params = store.getProxy().extraParams
                                                            params['createLength'] = store.getNewRecords().length
                                                            params['updateLength'] = store.getUpdatedRecords().length
                                                        store.getProxy().extraParams = params
                                                        
                            //                                store.getProxy().extraParams = {
                            //                                    createLength: store.getNewRecords().length,
                            //                                    updateLength: store.getUpdatedRecords().length, 
                            //                                };    
                            //                                console.log(store)
                                                        var storeReload = false;
                                                        var reload = 'Owner,Unit,Lease,Appraisal'
                                                        var reloadArr = reload.split(',');
                                                        Ext.each(reloadArr, function(rel){
                                                            if(Ext.util.Format.lowercase(store.proxy.api.read).indexOf(Ext.util.Format.lowercase(rel))){
                                                                storeReload=true;
                                                                return false;
                                                            }
                                                        })
                                                            
                                                        store.sync({
                                                            callback: function(){
                                                                if(storeReload){
                                                                    store.reload();
                                                                }
                                                                if(activeTab.xtype == 'MineralsLeaseOwners'){
                                                                    var mainView = Ext.getCmp('MineralValuationManageLeaseId');
                                                                    var leaseStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseStore');
                                                                    var leaseId = mainView.selectedId;
                                                                    var leaseGridStore = mainView.down('LeasesGridView').getStore();
                                                                    var leaseGridSelect= mainView.down('LeasesGridView').getSelectionModel();
                                                                    var getSelectedLease = leaseStore.find('leaseId', leaseId, 0, false, false, true);
                                                                    leaseGridSelect.deselectAll();
                                                                    leaseGridSelect.select(getSelectedLease, false, false);
                                                                }
                                                            }
                                                        });  
                                                        store.commitChanges();
                                                    })
                                                    tabPanel.setActiveTab(newCard)
                                                }
                                            }
                                        });
                                    } else {
                                        Ext.each(saveStore, function(store){    
                                            var params = store.getProxy().extraParams
                                                params['createLength'] = store.getNewRecords().length
                                                params['updateLength'] = store.getUpdatedRecords().length
                                            store.getProxy().extraParams = params
                                            
                //                                store.getProxy().extraParams = {
                //                                    createLength: store.getNewRecords().length,
                //                                    updateLength: store.getUpdatedRecords().length, 
                //                                };    
                //                                console.log(store)
                                            var storeReload = false;
                                            var reload = 'Owner,Unit,Lease,Appraisal'
                                            var reloadArr = reload.split(',');
                                            Ext.each(reloadArr, function(rel){
                                                if(Ext.util.Format.lowercase(store.proxy.api.read).indexOf(Ext.util.Format.lowercase(rel))){
                                                    storeReload=true;
                                                    return false;
                                                }
                                            })
                                                
                                            store.sync({
                                                callback: function(){
                                                    if(storeReload){
                                                        store.reload();
                                                    }
                                                    if(activeTab.xtype == 'MineralsLeaseOwners'){
                                                        var view = arrayGrid[0];
    
                                                        var mainPanel = view.up(view.mainPanelAlias)
                                                        if(mainPanel.selectedId){
                                                            store.getProxy().extraParams = {
                                                                selectedId: mainPanel.selectedId
                                                            };
                                                        }
                                                        // leaseStore.reload({
                                                        //     callback: function () {
                                                                var mainView = Ext.getCmp('MineralValuationManageLeaseId');
                                                                var leaseGridStore = mainView.down('LeasesGridView').getStore();
                                                                var leaseGridSelect= mainView.down('LeasesGridView').getSelectionModel();
                                                                var getSelectedLease = store.find('leaseId', mainPanel.selectedId, 0, false, false, true);
                                                                leaseGridSelect.deselectAll();
                                                                leaseGridSelect.select(getSelectedLease, false, false);                                    
                                                        //     }
                                                        // });
                                                        store.reload({
                                                            callback: function () {
                                                            if (view.getStore().getAt(0)) { 
                                                                    view.getSelectionModel().select(0, false, false);
                                                                }                                     
                                                            }
                                                        });
                                                    }
                                                }
                                            });  
                                            store.commitChanges();
                                        })
                                        tabPanel.setActiveTab(newCard)
                                    }
                                } else {
                                    Ext.Msg.show({
                                        iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                                        title: 'Lease Interest Pct Exeed',    
                                        msg: 'The total Lease Interest Pct have Exeeded 1.00000000',               
                                        buttons: Ext.Msg.OK
                                    });
                                }
                            } else {                                              
                                Ext.each(saveStore, function(store){                 
                                    store.removeAll();
                                    store.rejectChanges();                                
                                });   
                                tabPanel.setActiveTab(newCard)
                            }                                        
                        }
                    });
                }
                return false;
            }
//        },100)               
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CenterViewController onBeforeTabChange.')
        
    },
});
