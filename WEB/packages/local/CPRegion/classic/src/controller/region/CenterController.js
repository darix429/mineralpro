///**
// * Handles tab change and closing of tabs
// */
    
Ext.define('CPRegion.controller.region.CenterController', {
    extend: 'Ext.app.Controller',
    requires: ['Ext.layout.container.Table', 'Ext.form.Label'],
    stores: ['UserFunctionStore'],
    views: ['region.CenterView'],
    
    refs:[{
        ref: 'CenterView',
        selector: 'centerView'
    }],
    
    init: function () {
        var me = this;

        me.listen({
            global: {
                addFunctionControllerLoad: me.addFunctionControllerLoad,
//                checkLoadingStores: me.checkLoadingStores
            }
        })
    },
    
    addFunctionControllerLoad: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering CenterController addFunctionControllerLoad.')
        var me = this;
        try {                
            me.getController(MineralPro.config.Runtime.addController);
        } catch (err) {
            console.log(err)
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CenterController addFunctionControllerLoad.')
    },
//    checkLoadingStores: function(){
//        MineralPro.config.RuntimeUtility.DebugLog('Entering CenterController checkLoadingStores.')
//        var me = this;
//        var centerView = me.getCenterView();
//        var activeTab = centerView.getActiveTab()
//        
//        if(activeTab.selectedId != null){
//            activeTab = activeTab.getActiveTab();
//        }
//
//        var grid = activeTab.down('grid');
//
//        if(grid){
//            var store = grid.getStore();
//            if(!store.isLoading())
//                Ext.getBody().unmask();
//        }else{
//             Ext.getBody().unmask();
//        }
//        MineralPro.config.RuntimeUtility.DebugLog('Leaving CenterController checkLoadingStores.')
//    }
    
});
