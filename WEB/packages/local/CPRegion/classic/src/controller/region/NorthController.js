///**
// * Handles any interaction on north region of the regionwork.
// */

Ext.define('CPRegion.controller.region.NorthController', {
    extend: 'Ext.app.Controller',
    views: ['region.NorthView',
	'region.JEAddFormView'],
    stores: ['HeaderStore','JEStore'],
      
    refs:[{
        ref: 'NorthView',
        selector: 'northView'
    }],
    
    init: function(){
        var me=this;
        me.listen({
            global: {            
                updateHeader: me.updateHeader
            }
        })
    },
    
    updateHeader: function(){
        MineralPro.config.RuntimeUtility.DebugLog('Entering NorthController updateHeader.')
            var me = this;
            var northView = me.getNorthView();
            var headerInfoXtype = MineralPro.config.Runtime.headerInfoXtype;           
            var headerInfoItemId = northView.down('#headerInfoItemId');    
            headerInfoItemId.removeAll(true);
            
            if(headerInfoXtype){                             
                    headerInfoItemId.add(headerInfoXtype);                    
            }
                                 
        MineralPro.config.RuntimeUtility.DebugLog('Leaving NorthController updateHeader.')
    }
});

