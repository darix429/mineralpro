///**
// * Handles the adding of tab and focusing of tab
// */
Ext.define('CPRegion.controller.region.WestController', {
    extend: 'Ext.app.Controller',
    requires: ['CPRegion.store.MenuTreeStore', 'CPRegion.store.FavoriteTreeStore'],
    views: ['region.WestView'],
    store: ['MenuTreeStore', 'FavoriteTreeStore'],
    init: function () {
        var me = this;
        me.getWestViewSecurity();

        me.listen({
            global: {
                addFunctionControllerSelect: me.addFunctionControllerSelect,
            }
        })
    },
    /**
     * Fetch treeview content based on security group
     */
    getWestViewSecurity: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering WestController getWestViewSecurity.')
        var me = this;
        var menuTreeStore = me.getStore('MenuTreeStore');
        var favoriteTreeStore = me.getStore('FavoriteTreeStore');
        
        if(MineralPro.config.Runtime.appName == 'Information Services'){
            menuTreeStore.getProxy().extraParams = {
                process: 'update',
                idSecurityGroup: MineralPro.config.Runtime.idSecurityGroup,
                idAppUser: MineralPro.config.Runtime.idAppUser,
                appName: MineralPro.config.Runtime.appName,
                optionJE: 'startJE'
            };
            favoriteTreeStore.getProxy().extraParams = {
                process: 'update',
                idSecurityGroup: MineralPro.config.Runtime.idSecurityGroup,
                idAppUser: MineralPro.config.Runtime.idAppUser,
                appName: MineralPro.config.Runtime.appName,
                optionJE: 'startJE'
            };
        }else{    
            menuTreeStore.getProxy().extraParams = {
                process: 'update',
                idSecurityGroup: MineralPro.config.Runtime.idSecurityGroup,
                idAppUser: MineralPro.config.Runtime.idAppUser,
                appName: MineralPro.config.Runtime.appName,
            };
            favoriteTreeStore.getProxy().extraParams = {
                process: 'update',
                idSecurityGroup: MineralPro.config.Runtime.idSecurityGroup,
                idAppUser: MineralPro.config.Runtime.idAppUser,
                appName: MineralPro.config.Runtime.appName,
            };
        }
        menuTreeStore.load();
        favoriteTreeStore.load();
        MineralPro.config.RuntimeUtility.DebugLog('Leaving WestController getWestViewSecurity.')
    },
    addFunctionControllerSelect: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering WestController addFunctionController.')
        var me = this;
        try {
            //Ext.suspendLayouts();     
            me.getController(MineralPro.config.Runtime.addController);
            //Ext.resumeLayouts();
        } catch (err) {
            MineralPro.config.RuntimeUtility.ShowAlertOk('Alert', MineralPro.config.Runtime.addControllerAlert)
            console.log(err)
            Ext.getBody().unmask()
            return false;
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving WestController addFunctionController.')
    }
});