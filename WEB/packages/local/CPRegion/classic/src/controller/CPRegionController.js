///**
// * Handles any interaction on north region of the regionwork.
// */

Ext.define('CPRegion.controller.CPRegionController', {
    extend: 'Ext.app.Controller',
    requires: [
        'CPRegion.config.Runtime', 
        'CPRegion.config.RuntimeUtility',
        'CPRegion.view.CPRegionViewPort', 
        
        'CPRegion.controller.region.CenterController',
        'CPRegion.controller.region.NorthController',
        'CPRegion.controller.region.WestController',
        'Ext.form.Panel',
        
    ],
});

