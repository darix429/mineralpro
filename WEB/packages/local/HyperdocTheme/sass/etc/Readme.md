# HyperdocTheme/sass/etc

This folder contains miscellaneous SASS files. Unlike `"HyperdocTheme/sass/etc"`, these files
need to be used explicitly.
