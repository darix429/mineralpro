//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('CPLogin.view.UserLoginWindowViewController', {
    extend : 'Ext.app.ViewController',
    alias: 'controller.UserLoginWindowViewController',

    onLogin: function(btn) {
    	MineralPro.config.RuntimeUtility.DebugLog('Entering UserLoginWindowViewController onLogin.')
        var me = this;
        btn.up('#formlogin').mask('Checking credentials...');
        var loginForm = me.getView();                
        //Need to use look-up to get the store since the assiociated view is a window, 
        //which does not support store declaration.
        var userStore =  Ext.data.StoreManager.lookup('UserLoginStore');
        
        userStore.getProxy().extraParams = {
            process: 'Login',
            email_address: loginForm.down('#email_address').value,
            password: loginForm.down('#password').value,
            appraisalYear: MineralPro.config.Runtime.appraisalYear
//            appraisalYear: new Date().getFullYear()
        };
        userStore.load({
            callback: function(records, operation, success) {
                btn.up('#formlogin').unmask();
                if(success){   
                    Ext.globalEvents.fireEvent('checkUserInfo');
                    // if(userStore.count() && userStore.getAt(0) && userStore.getAt(0).get('comment') == 'Authenticated')
                    // {
                    //     window.location.reload();   
                    // }
                }
            }
        });                      

        MineralPro.config.RuntimeUtility.DebugLog('Leaving UserLoginWindowViewController onLogin.')
    },
    
    onCancel: function() {
    	MineralPro.config.RuntimeUtility.DebugLog('Entering UserLoginWindowViewController onClearAll.')
        var me = this;
        var loginForm = me.getView();  
      
        loginForm.down('#email_address').setValue('');
        loginForm.down('#password').setValue('');      
        loginForm.down('#email_address').focus();
        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving UserLoginWindowViewController onClearAll.')
    }
   
});