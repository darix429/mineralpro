/**
 * Created by x0182374 on 9/9/2016.
 */
Ext.define('CPLogin.view.UserLoginWindowView', {
    extend: 'Ext.window.Window',
    alias: 'widget.userLoginWindowView',
    requires: [
        'CPLogin.view.UserLoginWindowViewController',
        'Ext.Container',
        'Ext.window.Toast'
    ],
    controller: 'UserLoginWindowViewController',
    constrain: true,
    autoShow: true,
    height: '100%',
    width: '100%',
    header: false,
    border: false,
    closable: false,
    resizable: false,
    style: 'border: none; borderRadius: 0px',

    bodyStyle: {
        background: 'url(../resources/images/sky.jpg)',
        backgroundRepeat: 'no-repeat',
        backgroundSize: '100% 100%'
    },
    layout: {
        type: 'vbox',
        align: 'center',
        pack: 'center'
    },
    items: [
        {
            style: 'border: none; borderRadius: 25px; backgroundColor:transparent;',
            bodyStyle: {
                background: 'rgba(255, 255, 255, 0.12)',
				textAlign: 'center'
            },
            frame: true,
            height: 350,
            itemId: 'formlogin',
            width: 350,
            bodyPadding: 10,
            collapsible: false,
            // title: 'MineralPro Login',
            titleAlign: 'center',
            items: [{
                    xtype: 'image',
                    src: '../resources/images/mineralpro-logo.png',
                    margin: '40px auto 30px auto',
					height: 75
                    
            },{
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'center',
                        pack: 'center'
                    },
                    items: [{
                        xtype: 'image',
                        src: '../resources/images/mail.png',
                        cls: 'loginImgIcon'
                    },{
                        xtype: 'textfield',
                        itemId: 'email_address',
                        cls: 'emailtext',
                        style: 'vertical-align: top',
                        tabIndex: 1,
                        hasFocus: true,
                        margin: '10 5 0 30',
                        // fieldLabel: 'Email Address',
                        labelWidth: 70,
                        allowBlank: false,
                        allowOnlyWhitespace: false,
                        enforceMaxLength: true,
                        maxLength: 50,
                        maxLengthText: '50',
                        emptyText: 'Email Address',
                        //vtype: 'alphanum',
                        width: 215,
                        listeners: {
                            afterrender: function (thisfield) {
                                thisfield.focus(false, 500);
                            }
                        }
                    }]
                },{
                    xtype: 'container',
                    layout: {
                        type: 'hbox',
                        align: 'center',
                        pack: 'center'
                    },
                    items: [{
                        xtype: 'image',
                        src: '../resources/images/password.png',
                        cls: 'loginImgIcon'
                    },{
                        xtype: 'textfield',
                        itemId: 'password',
                        cls: 'passtext',
                        tabIndex: 2,
                        style: 'vertical-align: top',
                        margin: '10 0 5 30',
                        // fieldLabel: 'Password',
                        labelWidth: 70,
                        inputType: 'password',
                        allowBlank: false,
                        allowOnlyWhitespace: false,
                        enforceMaxLength: true,
                        maxLength: 30,
                        maxLengthText: '30',
                        emptyText: 'Password',
                        width: 215,
                    }],
                }, {
                    xtype: 'container',
                    margin: '30 10 20 30',
                    layout: {
                        type: 'hbox',
                        pack: 'center'
                    },
                    items: [{
                            xtype: 'button',
                            scale: 'medium',
                            width: '150px',
                            text: 'Login',
                            itemId: 'login_id',
                            tabIndex: 3,
                            listeners: {
                                click: 'onLogin'
                            }
                        }, {
                            //***************************
                            // Just add space betwen buttons
                            //***************************
                            width: 5
                        }, 
                        // {
                        //     xtype: 'button',
                        //     scale: 'medium',
                        //     text: 'Cancel',
                        //     itemId: 'cancel_id',
                        //     tabIndex: 4,
                        //     listeners: {
                        //         click: 'onCancel'
                        //     }
                        // }
                        ]
                }]
        }],
    listeners: {
        afterRender: function (thisForm, options) {
            var me = this
            me.keyNav = Ext.create('Ext.util.KeyNav', me.el, {
                enter: function () {
                    if (!me.down('#login_id').disabled)
                        me.down('#login_id').fireEvent('click',
                                me.down('#login_id'))
                },
                esc: function () {
                    if (!me.down('#login_id').disabled)
                        me.down('#cancel_id').fireEvent('click',
                                me.down('#cancel_id'))
                },
                scope: me
            });
        }
    }
});