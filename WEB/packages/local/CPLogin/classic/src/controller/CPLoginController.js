///**
// *Controls security configuration.
// */
Ext.define('CPLogin.controller.CPLoginController', {
    extend: 'Ext.app.Controller',

    requires : [ 
        'MineralPro.config.Runtime', 
        'CPLogin.store.UserLoginStore',        
    ],

    views : ['UserLoginWindowView'],
    
    stores : ['MineralPro.store.AppraisalYearStore'],

    refs:[{
        ref: 'UserLoginWindowView',
        selector: 'userLoginWindowView'
    },{
        ref: 'MainViewPort',
        selector: 'mainViewPort'
    }],
    
    init: function(){
        var me=this;
        me.listen({
            global: {            
                checkUserSession: me.checkUserSession,
                showUserLogin: me.showUserLogin,
                checkUserInfo: me.checkUserInfo,
                userPickOrganization: me.userPickOrganization,
                registerUserSession: me.registerUserSession,
                changeAppraisalYear: me.changeAppraisalYear,
                userLogout: me.userLogout,
                passwordChanged: me.passwordChanged,
                checkLoadingStores: me.checkLoadingStores
                
            }
        })
    },
    /**
     * Checks if an a session exist for the user.
     */
    checkUserSession: function(){
    	MineralPro.config.RuntimeUtility.DebugLog('Entering CPLoginController checkUserSession.')
        var me = this;
        var me = this;
               
        var userStore = me.getStore('UserLoginStore');
        userStore.load({
            callback: function(records, operation, success) {
                if(success){                              
                    if(userStore.count()==0){
                        Ext.globalEvents.fireEvent('showUserLogin');
                    }else{
                        Ext.globalEvents.fireEvent('checkUserInfo');
                    }     
                }
            }
        });                             
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CPLoginController checkUserSession.')
    },
    /**
     * Show the user login form without viewport
     */       
    showUserLogin: function(){
    	MineralPro.config.RuntimeUtility.DebugLog('Entering CPLoginController showUserLogin.')
	        Ext.widget('userLoginWindowView').show();        
            var me = this;  
	        var loginForm = me.getUserLoginWindowView();       
	        loginForm.down('#email_address').focus();
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CPLoginController showUserLogin.')
    },
//    /**
//     * Check user information if authenticated.
//     */
    checkUserInfo: function(){
    	MineralPro.config.RuntimeUtility.DebugLog('Entering CPLoginController checkUserInfo.')
        var me = this;          
        var userStore = me.getStore('UserLoginStore');
        var comment = userStore.getAt(0).data.comment;
                    
        if(comment=='Authenticated'){      
            // setTimeout(function () {
            // }, 100);
            Ext.globalEvents.fireEvent('registerUserSession');
            Ext.globalEvents.fireEvent('loadViewPort');
            if(me.getUserLoginWindowView()){
                me.getUserLoginWindowView().destroy()
            }
            
        }else{
            MineralPro.config.RuntimeUtility.ShowAlertOk('Alert', comment+'.', '', 'username')            
        }        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CPLoginController checkUserInfo.')
    },

    /**
     * Update global variable for user information and 
     * Register session on server side.
     */
    registerUserSession: function(){
        MineralPro.config.RuntimeUtility.DebugLog('Entering CPLoginController registerUserSession.')
        var me = this;       
        var userStore = me.getStore('UserLoginStore');  
        var currentYear = (function(){var date = new Date; return date.getFullYear()})();
        MineralPro.config.Runtime.idAppUser = userStore.getAt(0).data.idAppUser;    
        MineralPro.config.Runtime.idOrganization = userStore.getAt(0).data.idOrganization;
        MineralPro.config.Runtime.organizationName = userStore.getAt(0).data.organizationName;
        MineralPro.config.Runtime.idSecurityGroup = userStore.getAt(0).data.idSecurityGroup;
        MineralPro.config.Runtime.securityGroup = userStore.getAt(0).data.securityGroup;
        MineralPro.config.Runtime.emailAddress = userStore.getAt(0).data.emailAddress;
        MineralPro.config.Runtime.appraisalYear = userStore.getAt(0).data.appraisalYear || currentYear;
		MineralPro.config.Runtime.nameAbbreviation = userStore.getAt(0).data.nameAbbreviation;		  
        Ext.StoreMgr.lookup('MineralPro.store.AppraisalYearStore').load();
        ///////////////////////////////////////////////////////////////////// 
        //set user information into cookie///////////////////////////////////
        /////////////////////////////////////////////////////////////////////
        Ext.Ajax.request({
            url: userStore.getProxy().url,
            method: 'POST',
            params: {
                setCookie: 'setCookie',
                idAppUser: MineralPro.config.Runtime.idAppUser,
                idOrganization: MineralPro.config.Runtime.idOrganization,
                organizationName: MineralPro.config.Runtime.organizationName,
                idSecurityGroup: MineralPro.config.Runtime.idSecurityGroup,
                securityGroup: MineralPro.config.Runtime.securityGroup,
                emailAddress: MineralPro.config.Runtime.emailAddress,
				nameAbbreviation: MineralPro.config.Runtime.nameAbbreviation,
                comment: userStore.getAt(0).data.comment,  
                appraisalYear: MineralPro.config.Runtime.appraisalYear, 
            }
        });
        ////////////////////////////////////////////////////////////////////
        
        //destroy userstore to release memory
        // userStore.destroy();        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CPLoginController registerUserSession.')
    },
    
    /**
     * Update appraisalYear and reset cookie session
     */
    changeAppraisalYear: function(){
        MineralPro.config.RuntimeUtility.DebugLog('Entering CPLoginController registerUserSession.')
        var me = this;       
        var userStore = me.getStore('UserLoginStore');  
     
        ///////////////////////////////////////////////////////////////////// 
        //set user information into cookie///////////////////////////////////
        /////////////////////////////////////////////////////////////////////
        Ext.Ajax.request({
            url: userStore.getProxy().url,
            method: 'POST',
            params: {
                setCookie: 'setCookie',
                idAppUser: MineralPro.config.Runtime.idAppUser,
                idOrganization: MineralPro.config.Runtime.idOrganization,
                organizationName: MineralPro.config.Runtime.organizationName,
                idSecurityGroup: MineralPro.config.Runtime.idSecurityGroup,
                securityGroup: MineralPro.config.Runtime.securityGroup,
                emailAddress: MineralPro.config.Runtime.emailAddress,
				nameAbbreviation: MineralPro.config.Runtime.nameAbbreviation,
                comment: 'Authenticated',  
                appraisalYear: MineralPro.config.Runtime.appraisalYear, 
            }
        });
        ////////////////////////////////////////////////////////////////////
        
        //destroy userstore to release memory
        // userStore.destroy();        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CPLoginController registerUserSession.')
    },
    
    /**
     * Resposible for logging out the user and killing its session
     */
    userLogout: function(){
        MineralPro.config.RuntimeUtility.DebugLog('Entering CPLoginController userLogout.')
        var me = this;
        var userLoginStore = me.getStore('UserLoginStore');
        var resetPage = function(){
            userLoginStore.loadRawData([]);
            MineralPro.config.Runtime.idAppUser = '';
            MineralPro.config.Runtime.idOrganization = '';
            MineralPro.config.Runtime.organizationName = '';
            MineralPro.config.Runtime.idSecurityGroup = '';
            MineralPro.config.Runtime.securityGroup = '';
            MineralPro.config.Runtime.emailAddress = '';
            MineralPro.config.Runtime.appraisalYear = '';
            MineralPro.config.Runtime.nameAbbreviation = '';
            me.getMainViewPort().destroy();
            me.showUserLogin();
            window.location.reload(true); //reload(true) to reload page from server instead from cache
        };
        try{
            Ext.Msg.show({
                title:'Logout',
                msg: "Are you sure you want to Logout?",
                buttons: Ext.Msg.YESNO,
                fn: function (btn){
                    if(btn=="yes") {
                        if(!userLoginStore){
                            resetPage();
                        }
                        else{
                            userLoginStore.getProxy().extraParams = {
                                logout: 'logout'
                            }; 
                            userLoginStore.load({
                                callback: function(records, operation, success) {
                                    // Reset Runtime Variables regardless of operation status
                                    resetPage();
                                }
                            })
                        }
                    }
                }
            });                                    
        }
        catch(e){
            resetPage();
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CPLoginController userLogout.')
    },
    
    
     /**
     * Resposible for logging out the user and killing its session
     */
    passwordChanged: function(){
        MineralPro.config.RuntimeUtility.DebugLog('Entering CPLoginController passwordChanged.')
        var me = this;
//        console.log(idAppUserChangedPass)
//        console.log(MineralPro.config.Runtime.idAppUser)
        if(idAppUserChangedPass == MineralPro.config.Runtime.idAppUser){
//            Ext.Msg.show({
//                title:'Password Update',
//                msg: "Your password has been updated. Please login using the new password.",
//                buttons: Ext.Msg.OK,
//                fn: function () {
                    var userLoginStore = me.getStore('UserLoginStore');
                        userLoginStore.getProxy().extraParams = {
                            logout: 'logout'
                        }; 
                        userLoginStore.load({
                            callback: function(records, operation, success) {
                                window.location.reload();  
                            }
                        })
                        //destroy mainViewPort to release memory
                        me.getMainViewPort().destroy();                                                                                
//                    }          
//            });    
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CPLoginController passwordChanged.')
    },
    
    checkLoadingStores: function(){
        MineralPro.config.RuntimeUtility.DebugLog('Entering CPLoginController checkLoadingStores.')
        var centerView = Ext.getCmp('centerView');
        
        if(centerView){
            var activeTab = centerView.getActiveTab()

            if(activeTab.selectedId != null){
                activeTab = activeTab.getActiveTab();
            }

            var grid = activeTab.down('grid');
            if(grid){
                var store = grid.getStore();
                var getIndex = function(){
                    var selectedIndex = 0;
                    var mainPanel = grid.mainPanelAlias ? grid.up(grid.mainPanelAlias) : false;
                    var includeRRC = grid && mainPanel.selectedRRCNumberIndex ? grid.getColumns().map(function(col){ return col.dataIndex; }).indexOf(mainPanel.selectedRRCNumberIndex) > -1 : false;
                        
                    if(mainPanel){
                        selectedIndex = store.findBy(function(rec){
                            var selectedId = rec.get(mainPanel.selectedIdIndex) == mainPanel.selectedId;
                            var rrcNumber = rec.get(mainPanel.selectedRRCNumberIndex) ? rec.get(mainPanel.selectedRRCNumberIndex) == mainPanel.selectedRRCNumber : true;
                            rrcNumber = includeRRC ? rrcNumber : true;
                            return selectedId && rrcNumber;
                        });
                    }
                    return selectedIndex > -1 ? selectedIndex : 0;
                }
                if(!store.isLoading()){
                    Ext.getBody().unmask();
                    if(store.getCount() && grid.getSelectionModel().getSelected().length == 0){
                        MineralPro.config.RuntimeUtility.DebugLog('CPLoginController will be selecting '+getIndex())
                        if(getIndex() > -1){
                            grid.getSelectionModel().select(getIndex(), false, false);
                        }
                    }
                }
            }else{
                 Ext.getBody().unmask();
            }
        }else{
            Ext.getBody().unmask();
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CPLoginController checkLoadingStores.')
    }

});

