/**
 * Store for user login information
 */

Ext.define('CPLogin.store.UserLoginStore', {
    extend: 'Ext.data.Store',
    model: 'CPLogin.model.UserLoginModel',
    proxy: {
        type: 'ajax',
        url: '/api/Packages/CPLogin/GetUser', 
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success'
       }
    }
});
