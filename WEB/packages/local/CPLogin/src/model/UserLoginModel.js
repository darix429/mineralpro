/**
 * Model for user login information
 */

Ext.define('CPLogin.model.UserLoginModel', {
    extend: 'MineralPro.model.BaseModel',
    fields: [{
        name: 'idAppUser',
        defaultValue: 1
    },{
        name: 'organizationType',
        defaultValue: ''
    },{
        name: 'idOrganization',
        defaultValue: 1
    },{
        name: 'organizationName',
        defaultValue: ''
    },{            
        name: 'idSecurityGroup',
        defaultValue: 1
    },{
        name: 'securityGroup',
        defaultValue: ''
    },{
        name: 'emailAddress',
        defaultValue: ''
    },{
        name: 'nameAbbreviation',
        defaultValue: ''
    },{
        name: 'comment',
        defaultValue: ''
    },{
        name: 'appraisalYear',
        defaultValue: ''    
    }]
});