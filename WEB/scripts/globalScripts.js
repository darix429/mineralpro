var idAppUserChangedPass;
var socket = io.connect('54.70.218.181',{
	"transports": ['websocket']
});
    socket.on('passwordChanged',function(idAppUser){
        idAppUserChangedPass = idAppUser;
        Ext.globalEvents.fireEvent('passwordChanged');
    });
      
function onLogout() {
    Ext.globalEvents.fireEvent('userLogout');
}        
         


//URL Links for Crystal reports that will allow changes after production build.
var crystalServer = '';

/*--------- TITLE APPEND -------*/	
var titleAppend = 'Test';
var hostname = window.location.hostname; 
var serverName = "";
var contWord = hostname.includes("ecadcamapro");
var aws = hostname.includes("54.70.218.181");
if(contWord == true){
        
	if(hostname == 'ecadcamapro2' || hostname == '192.168.50.24' || hostname == 'ecadcamapro2.ectorcad.org'){
		titleAppend = 'Test'
		//runs on ECAD (ECADCAMAPRO2)
	}
	else{
		titleAppend = 'Production'
		//runs on ECAD (ECADCAMAPRO)
	}
	
}
else{
	if(hostname == 'localhost'){
		titleAppend = 'Dev'
		//runs on localhost
	}
	if(aws == true){
		titleAppend = 'QA'
		//runs on AWS
	}
	
}
document.title = document.title + " - " + titleAppend;