/**
 * Tingkersoft FilterRowBegins plugin for MineralPro
 * 
 * @version 0.0.1
 * @author Dan
 */

Ext.define('Ext.ux.grid.FilterRowBegins', {
    extend: 'Ext.util.Observable',
    options: {
        marginWidth: 100
    },
    init: function (grid) {
        this.grid = grid;
        this.grid.addCls('filter-row');
        var view = grid.getView();
        grid.on('afterRender', this.renderFields, this);
        grid.on('resize', this.resizeFields, this);
        grid.on('columnresize', this.resizeFields, this);
        grid.on('runFilter', this.onChange, this)
    },
    'renderFields': function () {
        var grid = this.grid;
        var filterRow = this;
        var cols = (grid.columns);
        var gridId = grid.id;
        Ext.each(cols, function (col) {
            var filterDivId = gridId + "-filter-" + col.id;
            Ext.get(col.id).appendChild({
                tag: 'div',
                cls: 'x-small-filterEle filterElement',
                id: filterDivId,
                html: ''
            });
            if (!col.hidden) {

                var filterEle = col.filterElement;
                var editor = '';
                if (filterEle) {
                    if (filterEle.getXType() == 'combo') {
                        filterEle.on('change',
                                this.applyChangeEvent, this,
                                editor);
                    } else {
                        filterEle.on('change',
                                this.applyChangeEvent, this,
                                filterEle);
                    }
                    new Ext.Panel({
                        border: false,
                        layout: 'fit',
                        items: filterEle,
                        renderTo: filterDivId
                    });
                }

            }
        }, this);
    },
    onChange: function () {
        Ext.suspendLayouts();
        this.onChangeFilter({
            filter: this,
            data: this.getFilterData()
        });
        Ext.resumeLayouts(true);
    },
    onChangeFilter: function (filterRow) {

        var grid = this.grid;
        grid.store.baseParams = {};
        
        grid.suspendEvents();
        
        grid.store.clearFilter();
        // grid.getSelectionModel().deselectAll(true);
        var filterFn = function(){
            var show = true;
            grid.store.filterBy(function (record) {
                for (var i in filterRow.data) { 
                    //i is the index for the column
                    if (i && filterRow.data[i] !== null && filterRow.data[i] !== '') {                  
                        var value = record.get(i); //this is the storeData to be tested
                        var value2 = filterRow.data[i]; //this is the filterValue

                        if (value == null) {
                            value = ''
                        }else if (value == false) {
                            value = '0'
                        } else if (value == true) {
                            value = '1'
                        }
                     
                        if (value2 == null) {
                            show = true;
                            break;
                        }else if (value2 == ''){
                            show = true;                        
                            break;
                        }
                        
                        value = value.toString().toLowerCase()
                        value2 = value2.toString().toLowerCase();
                        
                        var beginsWith = grid.down('#filterByBeginsWithItemId')
                        if(beginsWith.checked){
                            if (value.startsWith(value2)) {
                                show = true;
                            } else {
                                show = false;
                                break;
                            }
                        }else{
                            if (value.indexOf(value2) > -1) {
                                show = true;
                            } else {
                                show = false;
                                break;
                            }
                        }
                        
                    }              
                }
                return show;
            });
        };
        filterFn();
        grid.store.on('load', function () {
            if(grid.store){
                grid.store.clearFilter();
                filterFn();
            }
        });
        grid.resumeEvents();
        if(grid.selModel){
            if(grid.selModel.selectionMode == 'SINGLE'){
                grid.getSelectionModel().select(0);
            }
        }
        
    },
    resizeFields: function () {
        var grid = this.grid;
        var cols = grid.columns;

        Ext.each(cols, function (col) {
            if (!col.hidden) {
                var filterEle = col.filterElement;
                if (filterEle) {
                    filterEle.setWidth(col.width
                            - this.options.marginWidth);
                }
            }
        }, this);
    },
    applyChangeEvent: function (filterEle) {
        if (filterEle.filterOption == 'NoFilter') {
            filterEle.filterOption = '';
        }
        this.onChange();
    },
    getFilterData: function () {
        var grid = this.grid;
        var cols = grid.columns;
        var data = {};
        var value = '';
        var dataIndex = '';
        Ext.each(cols, function (col) {
            if (!col.hidden) {
                var filterEle = col.filterElement;
                if (filterEle) {
                    value = filterEle.getValue();
                    if (filterEle.getXType() == 'datefield'
                            && value && value.format) {
                        value = value.format(filterEle.format);
                    }
                    dataIndex = filterEle.dataIndex
                            ? filterEle.dataIndex
                            : col.dataIndex;
                    data[dataIndex] = value;
                }
            }
        });
        return data;
    }
});