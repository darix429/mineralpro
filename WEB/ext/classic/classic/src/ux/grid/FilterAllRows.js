/**
 * Tingkersoft FilterRowBegins plugin for MineralPro
 *
 * @version 0.0.1
 * @author Dan
 */

Ext.define('Ext.ux.grid.FilterAllRows', {
    extend : 'Ext.AbstractPlugin',
    options: {
        marginWidth: 100
    },
    init: function (grid) {
        this.setCmp(grid);
        // Ext.apply(cmp, this.cmpOverrides);
        this.cmpOverrides();
        grid.addCls('filter-row');
        var view = grid.getView();
        grid.on('afterRender', this.renderFields, this);
        grid.on('resize', this.resizeFields, this);
        grid.on('columnresize', this.resizeFields, this);
        grid.on('runFilter', this.onChange, this)

        grid.getFilterData = this.getFilterData.bind(this);
        grid.clearGridFilters = this.clearAll.bind(this);

    },
    cmpOverrides: function(){
        var me = this;
        var grid = me.getCmp();
        me.filterCategory = {
            xtype: 'cycle',
            showText: true,
            alwaysShow: true,
            width: 150,
            itemId: 'searchCategory',
            prependText: 'Filter:  ',
            listeners: {
                change: function(button, checkitem, eOpts){
                    grid.down('button#retrieveButton').click();
                    window.setTimeout(function(){
                        button.blur();
                    },100);
                    // var value = checkitem.getValue();
                    // if(value!='all'){

                    //     grid.down('button#retrieveButton').click();
                    // }
                    // else if(value == 'all'){
                    //     grid.getStore().clearFilter();
                    // }
                }
            },
            menu: {
                xtype: 'menu',
                width: 150,
                showSeparator: true,
                items: [
                    {
                        xtype: 'menucheckitem',
                        text: 'Exact Match',
                        checked: true,
                        value: 'exact',
                        group: 'searchType'
                    },
                    {
                        xtype: 'menucheckitem',
                        text: 'Starts With',
                        value: 'start',
                        group: 'searchType'
                    },
                    {
                        xtype: 'menucheckitem',
                        text: 'Contains',
                        value: 'contain',
                        group: 'searchType'
                    },
                    // {
                    //     xtype: 'menucheckitem',
                    //     text: 'Load All Data',
                    //     value: 'all',
                    //     group: 'searchType'
                    // }
                ]
            }
        };

        me.retrieveButton = {
            xtype: 'button',
            itemId: 'retrieveButton',
            width: 100,
            alwaysShow: true,
            text: 'Search',
            handler: function(){
                var cols = grid.getColumns();
                if(cols && cols.length){
                    Ext.each(cols, function (col) {
                        if (!col.hidden) {
                            var filterEle = col.filterElement;
                            var editor = '';
                            if (filterEle && filterEle.getValue()!='') {
                                filterEle.fireEvent('change',filterEle, filterEle.getValue, '', {
                                    retrieve: true
                                });
                            }
                        }
                    }, this);
                }
            }
        };
        grid.tools = grid.tools || [];
        me.undoFilterBtn = {
            xtype: 'button',
            tooltip: 'Clear Search',
            width: 100,
            alwaysShow: true,
            text: 'Clear',
            handler: function(btn){
                me.clearAll();
            }
        };
        grid.tools = [{
            xtype: 'container',
            width: '100%',
            layout: {
                type: 'hbox',
                pack: 'start'
            },
            items: [
                me.filterCategory,
                me.retrieveButton,
                me.undoFilterBtn,
                {
                    xtype: 'container',
                    flex: 1,
                    layout: {
                        type: 'hbox',
                        pack: 'end'
                    },
                    items: grid.tools
                }
            ]
        }]
        /*Store Functionality Starts here*/
        var mainPanel = grid.up(grid.mainPanelAlias);
        var defaultSelectedId = mainPanel.selectedId; //Need default value '0'
        var defaultSelectedIdIndex = mainPanel.selectedIdIndex;
        var defaultSelectedName = mainPanel.selectedName; //Need default value '0'
        var defaultSelectedNameIndex = mainPanel.selectedNameIndex;
        var store = Ext.getStore(grid.store);
        var getIndex = function(){
            var selectedIndex = 0;
            var includeRRC = grid && mainPanel.selectedRRCNumberIndex ? grid.getColumns().map(function(col){ return col.dataIndex; }).indexOf(mainPanel.selectedRRCNumberIndex) > -1 : false;                
            if(mainPanel){
                selectedIndex = store.findBy(function(rec){
                    var selectedId = rec.get(mainPanel.selectedIdIndex) == mainPanel.selectedId;
                    var rrcNumber = rec.get(mainPanel.selectedRRCNumberIndex) ? rec.get(mainPanel.selectedRRCNumberIndex) == mainPanel.selectedRRCNumber : true;
                    rrcNumber = includeRRC ? rrcNumber : true;
                    return selectedId && rrcNumber;
                });
            }
            return selectedIndex > -1 ? selectedIndex : 0;
        };
        store.setRemoteFilter(true);
        store.on({
            beforesync: {
                fn: function(options, eOpts){
                    if(options.hasOwnProperty('create')){
                        options.create.filter(function(rec){ 
                            return rec.get('rowDeleteFlag') == 'D';
                        }).forEach(function(rec){
                            var recIndex = store.indexOf(rec);
                            if(recIndex > -1){
                                store.removeAt(recIndex);   
                            }
                        })
                    }
                    if(store.getRemovedRecords().length){
                        store.commitChanges();
                    }
                }
            },
            beforeload: {
                fn: function(store, operation, eOpts){
                    Ext.getBody().mask('Loading');
                    var extraParams = store.getProxy().extraParams;
                    var filterData = grid.getFilterData();//getFilterData is bound on FilterAllRows Plugin
                    var filterCount = Ext.Object.getValues(filterData).filter(function(val){return val.trim();}).length;
                    // var filterCount = Object.values(filterData).filter(function(val){return val.trim();}).length;
                    if(filterCount == 0){
                        Ext.getBody().unmask();
                        store.query('rowDeleteFlag','D').each(function(rec,i){
                            var recIndex = store.indexOf(rec);
                            if(recIndex > -1){
                                store.removeAt(recIndex);   
                            }
                        });
                        if(store.getRemovedRecords().length){
                            store.commitChanges();
                        }
                        grid.getSelectionModel().deselectAll();
                        grid.getSelectionModel().select(getIndex(), false, false);
                        return false;
                    }
                    extraParams['searchBy'] = grid.down('cycle#searchCategory').getActiveItem().getValue();
                }
            },
            load: {
                fn: function(store, records, successful, operation, eOpts){
                    Ext.getBody().unmask();
                    var selectedIndex = store.findBy(function(rec){
                        return rec.get(mainPanel.selectedIdIndex) == mainPanel.selectedId;
                    });
                    if(isNaN(parseInt(selectedIndex)) || parseInt(selectedIndex) == -1){
                        selectedIndex = 0;
                        
                        var headerInfo = Ext.ComponentQuery.query(grid.headerInfoAlias)[0];
                        var detailView = grid.up(grid.mainPanelAlias).down(grid.detailAliasName);
                        if(headerInfo && typeof headerInfo.getForm == 'function' && headerInfo.getForm()){
                            headerInfo.getForm().reset(true);
                        }
                        if(detailView && typeof detailView.getForm == 'function' && detailView.getForm()){
                            detailView.getForm().reset(true);
                        }
                        /*------------------------------ START ------------------------------
                        | Clear out selected ID info on mainPanel
                        */
                        grid.getSelectionModel().deselectAll(true);
                        mainPanel.selectedId =  defaultSelectedId; //Need default value '0'
                        mainPanel.selectedIdIndex =  defaultSelectedIdIndex;
                        mainPanel.selectedName =  defaultSelectedName; //Need default value '0'
                        mainPanel.selectedNameIndex =  defaultSelectedNameIndex;
                        /*
                        | Clear out selected ID info on mainPanel
                        | ------------------------------ END ------------------------------
                        */
                    }

                    if(successful){
                        // var filterData = grid.getFilterData();//getFilterData is bound on FilterAllRows Plugin
                        // // var filterCount = Ext.Object.getValues(filterData).filter(function(val){return val.trim();}).length;
                        // var filterCount = Object.values(filterData).filter(function(val){return val.trim();}).length;
                        // if(filterCount==0){
                        //     grid.store.clearFilter(true);
                        // }
                        // if(records.length > 0 && grid.selModel){
                        //     if(grid.selModel.selectionMode == 'SINGLE'){
                        //         grid.getSelectionModel().select(0);
                        //         grid.fireEvent('selectionchange',grid.getSelectionModel(), grid.getSelectionModel().getSelection());
                        //         MineralPro.config.RuntimeUtility.DebugLog('END firing selectionchange');
                        //     }
                        // }
                    }
                    else{
                        var descriptiveErrorMsg = '';
                        if(operation.getError() && operation.getError().statusText){
                            descriptiveErrorMsg += '<p><span style="font-weight:bold;">Error Details: </span><span style="color:red;">'+operation.getError().statusText+'.</span></p>'
                        }
                        Ext.Msg.show({
                            title: 'Oops!',
                            msg: "Something went wrong while retrieving data."+descriptiveErrorMsg,
                            buttons: Ext.Msg.OK
                        });
                    }
                }
            }
        })
        /*Store Functionality Ends here*/
    },
    clearAll: function(){
        
        var me = this;
        var grid = me.getCmp();
        var columns = grid.getColumns();
        grid.getStore().clearFilter(true);
        grid.getStore().removeAll(true);
        if(columns){
            Ext.each(columns, function (col) {
                if (!col.hidden) {
                    var filterEle = col.filterElement;
                    if(filterEle){
                        filterEle.setValue('');
                    }
                }
            }, this);
        }
        var headerInfo = Ext.ComponentQuery.query(grid.headerInfoAlias)[0];
        var detailView = grid.up(grid.mainPanelAlias).down(grid.detailAliasName);

        if(headerInfo && typeof headerInfo.getForm == 'function' && headerInfo.getForm()){
            headerInfo.getForm().reset(true);
        }
        if(detailView && typeof detailView.getForm == 'function' && detailView.getForm()){
            detailView.getForm().reset(true);
        }
        /*
        | Reset mainPanel selected
        | Start
        */
        var mainPanel = grid.up(grid.mainPanelAlias);
        mainPanel.selectedId =  '0'; //Need default value '0'
        // mainPanel.selectedIdIndex =  '';
        mainPanel.selectedName =  '0'; //Need default value '0'
        // mainPanel.selectedNameIndex =  '';
        /*
        | Reset mainPanel selectedRow
        | End
        */
        
        grid.getStore().loadRawData([]);
        grid.getSelectionModel().deselectAll(true);
    },
    renderFields: function () {
        var grid = this.getCmp();
        var filterRow = this;
        var cols = grid.getColumns();
        var gridId = grid.id;

        Ext.each(cols, function (col) {
            var filterDivId = gridId + "-filter-" + col.id;
            if(Ext.get(col.id)){
                Ext.get(col.id).appendChild({
                    tag: 'div',
                    cls: 'x-small-filterEle filterElement',
                    id: filterDivId,
                    html: ''
                });
            }
            if (!col.hidden) {

                var filterEle = col.filterElement;
                var editor = '';
                if (filterEle) {
                    if (filterEle.getXType() == 'combo') {
                        filterEle.on('change', this.applyChangeEvent, this, editor);
                    } else {
                        filterEle.on('change', this.applyChangeEvent, this, filterEle);
                    }
                    filterEle.search = function(){
                        grid.down('#retrieveButton').click();
                    };
                    filterEle.on({
                        focusenter: function(){
                            col.sortable = false;
                        },
                        focusleave: function(){
                            col.sortable = true;
                        },
                        keydown: function(field, e){
                            if(e.getKey() === Ext.event.Event.ENTER){
                                grid.down('#retrieveButton').click();
                            }
                        }
                    })
                    new Ext.Panel({
                        border: false,
                        layout: 'fit',
                        items: filterEle,
                        renderTo: filterDivId
                    });
                }

            }
        }, this);
    },
    onChange: function () {
        Ext.suspendLayouts();
        this.onChangeFilter({
            filter: this,
            data: this.getFilterData()
        });
        Ext.resumeLayouts(true);
    },
    onChangeFilter: function (filterRow) {

        var grid = this.getCmp();
        grid.store.baseParams = {};

        grid.suspendEvents();

        grid.store.clearFilter();
        var extraParams = grid.store.getProxy().extraParams;
        extraParams['filterBy'] = grid.down('cycle#searchCategory').getActiveItem().getValue();
        // grid.getSelectionModel().deselectAll(true);
        var me = this;
        grid.getStore().on({
            beforeload: {
                fn: function(store, operation, eOpts){
                    Ext.getBody().mask('Loading...');
                    var extraParams = store.getProxy().extraParams;
                    extraParams['searchBy'] = grid.down('cycle#searchCategory').getActiveItem().getValue();
                },
                single: true
            },
            load: {
                fn: function(store, records, successful, operation, eOpts){
                    Ext.getBody().unmask();
                    grid.getSelectionModel().deselectAll(true);
                    /*
                    | would always clear out header and detials forms
                    | I've added a timeout just a work arround to header and details updating
                    */
                    // window.setTimeout(function() {
                        var headerInfo = Ext.ComponentQuery.query(grid.headerInfoAlias)[0];
                        var detailView = grid.up(grid.mainPanelAlias).down(grid.detailAliasName);
                        if(headerInfo && typeof headerInfo.getForm == 'function' && headerInfo.getForm()){
                            headerInfo.getForm().reset(true);
                        }
                        if(detailView && typeof detailView.getForm == 'function' && detailView.getForm()){
                            detailView.getForm().reset(true);
                        }
                    // }, 500);

                    /*
                    | Reset mainPanel selectedRow
                    | Start
                    */
                    var mainPanel = grid.up(grid.mainPanelAlias);
                    mainPanel.selectedId =  '0'; //Need default value '0'
                    // mainPanel.selectedIdIndex =  '';
                    mainPanel.selectedName =  '0'; //Need default value '0'
                    // mainPanel.selectedNameIndex =  '';
                    /*
                    | Reset mainPanel selected
                    | End
                    */

                    if(successful){
                        var filterCount = Ext.Object.getValues(me.getFilterData()).filter(function(value){ return value!='';}).length;
                        // var filterCount = Object.values(me.getFilterData()).filter(function(value){ return value!='';}).length;
                        if(filterCount==0){
                            grid.store.clearFilter(true);
                        }
                        if(records.length > 0 && grid.selModel){
                            if(grid.selModel.selectionMode == 'SINGLE'){
                                grid.getSelectionModel().select(0);
                                grid.fireEvent('selectionchange',grid.getSelectionModel(), grid.getSelectionModel().getSelection());
                            }
                        }
                    }
                    else{
                        var descriptiveErrorMsg = '';
                        if(operation.getError() && operation.getError().statusText){
                            descriptiveErrorMsg += '<p><span style="font-weight:bold;">Error Details: </span><span style="color:red;">'+operation.getError().statusText+'.</span></p>'
                        }
                        Ext.Msg.show({
                            title: 'Oops!',
                            msg: "Something went wrong while retrieving data. "+descriptiveErrorMsg,
                            buttons: Ext.Msg.OK
                        });
                    }
                },
                single: true
            }
        });
        var filterFn = function(){
            var show = true;
            var filters = [];
            for(var key in filterRow.data){
                if(filterRow.data[key] != ''){
                    filters.push({
                        property: key,
                        value: filterRow.data[key]
                    });
                }
            }
            if(filters.length){
                grid.store.filter(filters);
            }
        };
        filterFn();
        grid.resumeEvents();

    },
    resizeFields: function () {
        var grid = this.getCmp();
        var cols = grid.getColumns();

        Ext.each(cols, function (col) {
            if (!col.hidden) {
                var filterEle = col.filterElement;
                if (filterEle) {
                    filterEle.setWidth(col.width - this.options.marginWidth);
                }
            }
        }, this);
    },
    applyChangeEvent: function (filterEle, newValue, oldValue, eOpts) {
        if (filterEle.filterOption == 'NoFilter') {
            filterEle.filterOption = '';
        }
        if(eOpts && eOpts.retrieve){
            this.onChange();
        }
    },
    getFilterData: function () {
        var grid = this.getCmp();
        var cols = grid.getColumns();
        var data = {};
        var value = '';
        var dataIndex = '';
        Ext.each(cols, function (col) {
            if (!col.hidden) {
                var filterEle = col.filterElement;
                if (filterEle) {
                    value = filterEle.getValue();
                    if (filterEle.getXType() == 'datefield'
                            && value && value.format) {
                        value = value.format(filterEle.format);
                    }
                    dataIndex = filterEle.dataIndex
                            ? filterEle.dataIndex
                            : col.dataIndex;
                    data[dataIndex] = value;
                }
            }
        });
        return data;
    }
});
