//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('ARB.config.view.BaseViewController', {
    extend : 'ARB.config.view.BaseGridViewController',
    alias: 'controller.baseViewController',
   
    /**
     * Resposible saving unsaved data before closing tab
     */
    onBeforeClose: function(close){  
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseViewController onBeforeClose.')   
        
         var me=this,
            saveData = false,   
            saveStore = [],
            arrayGrid= me.query('grid'); 
        
        for(var x=0; x<arrayGrid.length; x++){
            var store = arrayGrid[x].getStore();
            
            if(store.getNewRecords().length > 0 
                    || store.getUpdatedRecords().length > 0 
                    || store.getRemovedRecords().length > 0){
                    saveStore.push(store)
                    saveData = true
            } 
            
        }
        
        if(saveData){  
            Ext.Msg.show({
                title:ARB.config.Runtime.tabCloseConfirmationIcon,    
                msg:ARB.config.Runtime.tabCloseConfirmationMsg,
                iconCls:ARB.config.Runtime.tabCloseConfirmationIcon,
                buttons: Ext.Msg.YESNO,               
                scope: this,
                width: 250,
                fn: function(btn) { 
                    if (btn === 'yes'){
                        Ext.each(saveStore, function(store){       
                            store.getProxy().extraParams = {
                                createLength: store.getNewRecords().length,
                                updateLength: store.getUpdatedRecords().length, 
                            };    
                            store.sync({
                                callback: function(records, operation, success) {
                                    store.reload();
                                }
                            });  
                            store.commitChanges();
                        })
                        me.close()
                    }else{
                        Ext.each(saveStore, function(store){
                            store.rejectChanges()
                        })                          
                        me.close()
                    }                                        
                }
            });  
            return false;       
        }
                //.getTabBar().hide();
//        var me=this
//        var withSaveData=false
//        var store_list = []
//        
//        me.items.each(function(item){
//            if(item.getXTypes().indexOf('grid') > -1){
//                var store = item.getStore()
//                if(store.getNewRecords().length > 0 
//                    || store.getUpdatedRecords().length > 0 
//                    || store.getRemovedRecords().length > 0){
//                    store_list.push(store)
//                    withSaveData = true
//                }                    
//            }
//            else if (!(item.getXTypes().indexOf('splitter') > -1)){
//                if(item.down('grid')){
//                var itemSecondary = item.down('grid')
//                var storeSecondary = itemSecondary.getStore()
//                if(storeSecondary.getNewRecords().length > 0 
//                    || storeSecondary.getUpdatedRecords().length > 0 
//                    || storeSecondary.getRemovedRecords().length > 0){
//                    store_list.push(storeSecondary)
//                    withSaveData = true
//                }
//                }
//            }           
//        })   
//       
//        if(withSaveData){  
//            Ext.Msg.show({
//                title:ARB.config.Runtime.tabCloseConfirmationIcon,    
//                msg:ARB.config.Runtime.tabCloseConfirmationMsg,
//                iconCls:ARB.config.Runtime.tabCloseConfirmationIcon,
//                buttons: Ext.Msg.YESNO,               
//                scope: this,
//                width: 250,
//                fn: function(btn) { 
//                    if (btn === 'yes'){
//                        Ext.each(store_list, function(store){       
//                            store.getProxy().extraParams = {
//                                createLength: store.getNewRecords().length,
//                                updateLength: store.getUpdatedRecords().length, 
//                                id_organization: store.data.items[0].data.note_filters,
//                                id_code_type: store.data.items[0].data.id_code_type,
//                                id_client: store.data.items[0].data.id_client,
//                                id_store: store.data.items[0].data.id_store,
//                                id_product: store.data.items[0].data.id_product
//                            };    
//                            store.sync({
//                                callback: function(records, operation, success) {
//                                    store.reload();
//                                }
//                            });  
//                            store.commitChanges();
//                        })
//                        me.close()
//                    }else{
//                        Ext.each(store_list, function(store){
//                            store.rejectChanges()
//                        })                          
//                        me.close()
//                    }                                        
//                }
//            });  
//            return false;       
//        }
//        return true;
        
        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseViewController onBeforeClose.')
    }
   
});