//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('ARB.config.view.BaseGridViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.baseGridViewController',
    /**
     * Sync the client grid data to database (add, edit, delete)
     */
    onSyncListGridData: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onSyncListGridData.')
        var me = this;       
        var view = me.getView();
        var listStore = view.getStore();
                                  
        var dirty = listStore.getNewRecords().length > 0
        || listStore.getUpdatedRecords().length > 0
        || listStore.getRemovedRecords().length > 0;
        if (dirty) {
            Ext.Msg.show({
                title:ARB.config.Runtime.syncGridMsgConfimationTitle,
                msg:ARB.config.Runtime.syncGridMsgConfimationMsg,
                iconCls:ARB.config.Runtime.syncGridMsgConfimationIcon,
                buttons: Ext.Msg.YESNO,
                scope: this,
                width: 250,
                fn: function (btn) {
                    if (btn === 'yes') {
                        view.getSelectionModel().deselectAll(true);
                        listStore.getProxy().extraParams = {
                            createLength: listStore.getNewRecords().length,
                            updateLength: listStore.getUpdatedRecords().length
                        };

                        listStore.sync({
                            callback: function (records, operation, success) {                                
                                listStore.reload({
                                    callback: function () {
                                        if (view.getStore().getAt(0)) { 
                                                view.getSelectionModel().select(0, false, false);
                                        }                                     
                                    }
                                });
                            }
                        });
                        listStore.commitChanges();
                    }
                }
            });
        } else {
            Ext.Msg.show({
                iconCls:ARB.config.Runtime.listSyncMsgErrorIcon,
                title:ARB.config.Runtime.listSyncMsgErrorTitle,
                msg:ARB.config.Runtime.listSyncMsgErrorMsg,
                buttons: Ext.Msg.OK
            });                      
        }                         
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onSyncListGridData.')
    },
    /**
     * Responsible for changing data in detail and note panel after grid selection
     */
    onLoadSelectionChange: function (model, records) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onLoadSelectionChange.')

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onLoadSelectionChange.')
    },
    /**
     * Show up the Add List form
     */
    onOpenAddListForm: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onOpenAddListForm.')
        var me = this;
        var mainView = me.getView();
        var record = Ext.create(mainView.mainListModel);      
        var addView = Ext.widget(mainView.addFormAliasName);
        
        record.set('rowUpdateUserid', MineralPro.config.Runtime.rowUpdateUserid)
        addView.down('form').loadRecord(record);
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onOpenAddListForm.')
    },
    /**
     * Open up the Edit form for editing
     */
    onOpenEditListForm: function (grid, rowIndex, colIndex) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onOpenEditListForm.')
        var me = this;
        var mainView = me.getView();
        var rec = mainView.getStore().getAt(rowIndex);
        var editView = Ext.widget(mainView.editFormAliasName);
        
        editView.down('form').loadRecord(rec);       
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onOpenEditListForm.')
    },
    /**
     * Clear value for all filter
     */
    onClearListFilter: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onClearListFilter.')
        var me = this;
        var cols = me.getView().columns

        Ext.each(cols, function (col) {
            if (col.filterElement)
                col.filterElement.setValue('');
        });

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onClearListFilter.')
    },
    /**
     * Default to select first row in Main List Panel
     */
    onAfterLayout: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onAfterLayout.')

        var view = this.getView();   
        if (view.getStore().getAt(0)) {
            var selection = view.getSelectionModel().getSelection();
            if (selection.length == 0)
                view.getSelectionModel().select(0, false, true);

            view.getController().onLoadSelectionChange(view, view.getSelectionModel().getSelection());
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onAfterLayout.')
    },
    /**
     * Reject changes on grid
     */
    onCancelGrid: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onCancelGrid.')
        var me = this
        var view = me.getView()
        var isTab = view.up(view.mainPanelAlias).down('tab');
        if (isTab) {
            var arrayGrid = view.up(view.mainPanelAlias).getActiveTab().query('grid');

            Ext.each(arrayGrid, function (grid) {
                grid.getStore().rejectChanges();
            });

        } else {
            var arrayGrid = view.up(view.mainPanelAlias).query('grid');

            Ext.each(arrayGrid, function (grid) {
                grid.getStore().rejectChanges();
            });
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onCancelGrid.')
    },
      
});