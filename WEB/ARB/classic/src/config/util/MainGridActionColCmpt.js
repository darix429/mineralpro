Ext.define('ARB.config.util.MainGridActionColCmpt', {
    singleton: true, //default configuration for global variable

    editCls: {
        icon:ARB.config.Runtime.editCls,
        tooltip:ARB.config.Runtime.editTooltip,
        handler: function (grid, rowIndex, colIndex, item, e, record) {
            this.up('grid').getSelectionModel().select(rowIndex)
            if (record.get('rowDeleteFlag') != 'D') {
                this.up('grid').getController().onOpenEditListForm(grid, rowIndex, colIndex, item)
            } else {
                Ext.Msg.show({
                    iconCls:ARB.config.Runtime.editDeletedDataWarningCls,
                    title:ARB.config.Runtime.editDeletedDataWarningTitle,
                    msg:ARB.config.Runtime.editDeletedDataWarningMsg,
                    buttons: Ext.Msg.OK
                });
            }
        },         
    },
    deleteCls: {
        getClass: function (colConfig, cellData, record, rowIndex, colIndex, dataStore) {
            if (record.get('rowDeleteFlag') == 'D') {
                return(ARB.config.Runtime.undoDeleteCls);
            } else {
                return(ARB.config.Runtime.deleteCls);
            }
        },
        getTip: function (colConfig, cellData, record, rowIndex, colIndex, dataStore) {
            if (record.get('rowDeleteFlag') == 'D') {
                return(ARB.config.Runtime.undoDeleteTooltip);
            } else {
                return(ARB.config.Runtime.deleteTooltip);
            }
        },
        handler: function (gridView, rowIndex, colIndex, clickedItem, eventObj, record, row) {
            this.up('grid').getSelectionModel().select(rowIndex)
            if (record.get('rowDeleteFlag') == 'D') {
                record.set('rowDeleteFlag', record.previousValues.rowDeleteFlag);                                        
            } else {
                record.set('rowDeleteFlag', 'D');            
            }
        }
    },
})


    