Ext.define('ARB.config.util.MainGridToolCmpt', {
    singleton: true, //default configuration for global variable

    addNewDataButton: {
        xtype: 'button',
        iconCls:ARB.config.Runtime.addNewBtnIconCls,
        listeners: {
            click: 'onOpenAddListForm',
            beforerender: function () {
                var me = this;
                me.tooltip = me.up('grid').addButtonTooltip
                me.text = me.up('grid').addButtonText
            },
            afterrender: function(){
                var me = this
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    },
    saveToDbButton: {
        xtype: 'button',
        iconCls:ARB.config.Runtime.saveToDbBtnIconCls,
        tooltip:ARB.config.Runtime.saveGridBtnText,
        text:ARB.config.Runtime.saveGridBtnTooltip,
        listeners: {
            click: 'onSyncListGridData',
            afterrender: function(){
                var me = this
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    },
    clearFilterButton: {
        xtype: 'button',
        iconCls:ARB.config.Runtime.refreshBtnIconCls,
        tooltip:ARB.config.Runtime.refreshBtnTooltip,
        text:ARB.config.Runtime.refreshBtnText,
        listeners: {
            click: 'onClearListFilter'
        }
    },
    saveButton: {
        xtype: 'button',
        iconCls:ARB.config.Runtime.saveToDbBtnIconCls,
        listeners: {
            click: 'onSyncListGridData',
            beforerender: function () {
                var me = this;
                me.tooltip = me.up('panel').saveButtonTooltip
                me.text = me.up('panel').saveButtonText
            },
            afterrender: function(){
                var me = this
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    },
    cancelGridButton: {
        xtype: 'button',
        iconCls:ARB.config.Runtime.cancelGridIconCls,
        tooltip:ARB.config.Runtime.cancelGridTooltip,
        text:ARB.config.Runtime.cancelGridText,
        listeners: {
            click: 'onCancelGrid'
        }
    },
    
     comboSelection: {
        xtype: 'combo',
        width: 350,
        labelAlign: 'right',
        typeAhead: true,
        displayField: 'displayField',
        valueField: 'displayField',
        listeners: {
            beforerender: function () {
                var me = this
                var grid = me.up('grid')
                me.setStore(grid.gridToolComboStore)
                me.setFieldLabel(grid.gridToolComboFieldLabel)
                me.emptyText = grid.gridToolComboEmptyText

            },
            /// added for focus on dropdown
            afterrender: function (field) {
                Ext.defer(function () {
                    field.focus(true, 100);
                }, 10);                
            },
            change: function (fieldLabel, newval, oldval) {
                var me = this
                var listStore = me.up('grid').getStore();
                listStore.getProxy().extraParams = {
                    sec: newval
                }
                listStore.load();                              
            },
            beforequery: function(queryVV){
                queryVV.combo.expand();
                queryVV.combo.store.load();
                return false;
            }
        }
    },
    
})


    