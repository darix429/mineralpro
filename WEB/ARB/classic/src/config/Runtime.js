/**
 * Contains the global configuration during runtime.
 */

Ext.define('ARB.config.Runtime', {
    singleton: true, //default configuration for global variable

//**********************************************
//**shared configuration for all sub functions**
//**********************************************   
    //Action Column icons
    editCls: '../resources/icons/pencil.png',//'edit',
    editTooltip: 'Click to edit',
    
    //Action column cls
    deleteCls: 'delete',
    deleteTooltip: 'Delete record',
    undoDeleteCls: 'undo',
    undoDeleteTooltip: 'Undo Delete',
    
    //save button cls
    saveToDbBtnIconCls: 'save',
    addNewBtnIconCls: 'add',
    
    //save grid btn to database configuration
    saveGridBtnText: 'Save to Database',
    saveGridBtnTooltip: 'Save to Database',
    
    //refresh button filter configuration
    refreshBtnIconCls: 'refreshFilter',
    refreshBtnTooltip: 'Refresh Filter',
    refreshBtnText: 'Undo Filter',
    
    //List sync to grid messages and alerts no data
    listSyncMsgErrorIcon: 'wrn',
    listSyncMsgErrorTitle: 'Error',
    listSyncMsgErrorMsg: 'There is no new data to be saved.',
    
    //add data to grid error
    addEditToGridMsgErrorIcon: 'wrn',
    addEditToGridMsgErrorTitle: 'Error',
    addEditToGridMsgErrorMsg: 'Please check data on the highlighted fields.',
    
    //Note sync to grid messages and alerts with data
    syncGridMsgConfimationIcon: 'wrn',
    syncGridMsgConfimationTitle: 'Confirm',
    syncGridMsgConfimationMsg: 'Are you sure you want to commit changes?',
    	
    //list seletion unsaved data
    addExistingDataWarningCls: 'wrn',
    addExistingDataWarningTitle: 'Alert',
    addExistingDataWarningMsg: 'Data is already in the database.',
    
    //list seletion unsaved data
    editDeletedDataWarningCls: 'wrn',
    editDeletedDataWarningTitle: 'Alert',
    editDeletedDataWarningMsg: 'Please undelete data before editing.',


    //tab close confirmation
    tabCloseConfirmationIcon: 'wrn',
    tabCloseConfirmationTitle: 'Confirm',
    tabCloseConfirmationMsg: 'Do you want to save changes before closing?',
    
    actionColWidth: 50,  
   
    multipleEditAlertIcon: 'wrn',
    multipleEditAlertTitle: 'Alert',
    multipleEditAlertMsg: 'Please select record for multiple change.',
    
    duplicateAddressAlertIcon: 'wrn',
    duplicateAddressAlertTitle: 'Alert',
    duplicateAddressAlertMsg: 'Please check addess is duplicated.',
    
    

//*************************************************
//**individual configuration for each subfunction**
//*************************************************
    //Security Group configuration
    
    securityGroupEditSecurityCodeIcon: '../resources/icons/col_drop.png',
    securityGroupEditSecurityCodeTitle: 'Click to edit',
    
    securityGroupDeleteBtnToolTip: 'Delete Security Group',
    securityGroupDeleteBtnText: 'Delete Security Group',
    
    securityGroupDeleteAlertIcon: 'wrn',
    securityGroupDeleteAlertTitle: 'Alert',
    securityGroupDeleteAlertMsg: 'Please select security group to delete.',
    
    securityGroupDeleteConfirmationIcon: 'wrn',
    securityGroupDeleteConfirmationTitle: 'Confirm',
    securityGroupDeleteConfirmationMsg: 'Are you sure you want to delete security group ',
    
    securityGroupEditBtnIcon: 'edit',
    securityGroupEditBtnText: 'Edit Checked',
    securityGroupEditBtnTooltip: 'Edit Checked',
  

    //Security UserList configuration
    securityUserListNoContactAlertIcon: 'wrn',
    securityUserListNoContactAlertTitle: 'Alert',
    securityUserListNoContactAlertMsg: 'No contact created for this Organization name.',
     
    //refresh button filter configuration
    cancelGridIconCls: 'delete',
    cancelGridTooltip: 'Cancel Changes',
    cancelGridText: 'Cancel',
    
    //tooltip configuration
    tooltipImageWidth: 200,
    tooltipImageHeight: 200,
    
    //global
    inlineEditIcon: '../resources/icons/b_inline_edit.png',
    inlineEditComboIcon: '../resources/icons/col_drop.png',
    inlineEditComboTitle: 'Click to edit',

})