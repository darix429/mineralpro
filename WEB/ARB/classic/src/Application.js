/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('ARB.Application', {
    extend: 'Ext.app.Application',
    name: 'ARB',
    controllers: [
        'AppMainController',
        'CPLogin.controller.CPLoginController',
        'CPRegion.controller.CPRegionController',
        'ARB.sub.DefaultPage.controller.DefaultPageController'
    ],    
    stores:[
        'MineralPro.store.AppraisalYearStore'
    ],
    requires: [
        'Ext.form.field.Radio',
        'Ext.ux.grid.FilterRow',
        'ARB.config.Runtime', 
        'ARB.config.util.MainGridActionColCmpt',
        'ARB.config.util.MainGridToolCmpt',
        'ARB.config.util.PopUpWindowCmpt',
        
        'MineralPro.sub.CommonFunctionsSearch.controller.CommonFunctionsSearchController',
    ],
    refs: [{
            ref: 'UserLoginWindowView',
            selector: 'userLoginWindowView'
    }],
    launch: function () {
        var me = this;               
        MineralPro.config.Runtime.appName = 'ARB'   
        MineralPro.config.RuntimeUtility.disableBackspace();
        MineralPro.config.RuntimeUtility.enablePasswordChecking();
        MineralPro.config.RuntimeUtility.ImplementVtypes();
                        
//        MineralPro.config.RuntimeUtility.addThemeOptions();                       
        setTimeout(function () {
            Ext.globalEvents.fireEvent('checkUserSession');
                setTimeout(function () {
                    if(MineralPro.config.Runtime.appraisalYear.length == 0){
                         MineralPro.config.Runtime.appraisalYear = new Date().getFullYear();
                    }
                    me.getStore('MineralPro.store.AppraisalYearStore').load();
                }, 200);
        }, 200);

        Ext.on('resize', function () {
            var windowLogin = me.getUserLoginWindowView();
            if (windowLogin) {
                windowLogin.center();
            }
        });
    },
//	mainView: 'MineralPro.view.MainViewPort'
});
