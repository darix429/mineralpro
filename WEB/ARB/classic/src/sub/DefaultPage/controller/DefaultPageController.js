///**
// * Handles the controller processing for Client List
// */

Ext.define('ARB.sub.DefaultPage.controller.DefaultPageController', {
    extend: 'Ext.app.Controller',
  
    models: [
    ],
    
    stores: [
    ],  
    
    views: [
        'ARB.sub.DefaultPage.view.DefaultPageView',
    ],          
    
    requires: ['Ext.form.ComboBox',
        'Ext.grid.column.Action',
        'Ext.form.FieldSet',
        'Ext.form.field.Hidden'
    ]
});


