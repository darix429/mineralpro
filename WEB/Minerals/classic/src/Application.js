/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('Minerals.Application', {
    extend: 'Ext.app.Application',
    name: 'Minerals',
    controllers: [
        'AppMainController',
        'CPLogin.controller.CPLoginController',
        'CPRegion.controller.CPRegionController',
        'Minerals.sub.DefaultPage.controller.DefaultPageController'
    ],
    
    model:[
        'Minerals.model.MineralValuation.NotesModel',
        'Minerals.model.MineralValuation.AppraisalModel',
        'Minerals.model.MineralValuation.FieldsModel',
        'Minerals.model.MineralValuation.QuickNoteModel',
        
        'Minerals.model.ProperRecords.AgencyModel',
        'Minerals.model.ProperRecords.OwnerComboModel',
        'Minerals.model.ProperRecords.GravityCdModel',
        'Minerals.model.MineralValuation.FieldComboModel'
    ],
    
    stores: [        
        'MineralPro.store.CountryStore',
        'MineralPro.store.StateStore',
        'MineralPro.store.AppraisalYearStore',
		'MineralPro.store.CdJeHoldStore',
        'MineralPro.store.CdReasonStore',
        'MineralPro.store.CdTaxOfficeNotifyStore',
        'MineralPro.store.Crystal.CrystalStore',
        'Minerals.store.ProperRecords.AgencyStore',
        'Minerals.store.ProperRecords.OwnerComboStore',
        'Minerals.store.ProperRecords.ExemptionStore',
        'Minerals.store.MineralValuation.QuickNoteStore',
        'Minerals.store.MineralValuation.NoteTypeStore',
        'Minerals.store.ProperRecords.GravityCdStore',
        'Minerals.store.MineralValuation.OperatorComboStore',
        'Minerals.store.MineralValuation.FieldComboStore',
        'Minerals.store.MineralValuation.WellTypeComboStore',
    ],
    
    requires: [
        'Ext.form.field.Radio',
        'Ext.ux.grid.FilterRow',
        
        'MineralPro.config.util.MainGridActionColCmpt',
        'MineralPro.config.util.MainGridToolCmpt',
        'MineralPro.config.util.PopUpWindowCmpt',
        'MineralPro.sub.CommonFunctionsSearch.controller.CommonFunctionsSearchController',
        
        'Minerals.config.Runtime', 
        'Minerals.config.view.Note.BaseNoteGridView',
        'Minerals.config.view.Note.BaseNoteAddEditFormView',
        'Minerals.config.view.Help.SendHelp',
        'Minerals.config.view.Help.ErrorWindow',
        
        'Minerals.config.view.Appraisal.BaseAppraisalFormView',
        'Minerals.config.view.Field.BaseFieldGridView',       
        
        'Minerals.sub.MineralValuationManageLease.controller.MineralValuationManageLeaseController',
        'Minerals.sub.ProperRecordsManageOperator.controller.ProperRecordsManageOperatorController',
        'Minerals.sub.ProperRecordsManageOwner.controller.ProperRecordsManageOwnerController',
        'Minerals.sub.MineralValuationManageUnit.controller.MineralValuationManageUnitController',
        'Minerals.sub.ProperRecordsManageAgent.controller.ProperRecordsManageAgentController',
        'Minerals.sub.ProperRecordsManageField.controller.ProperRecordsManageFieldController',
        'Minerals.sub.MineralValuationManageAppraisal.controller.MineralValuationManageAppraisalController',
		'Minerals.sub.ProperRecordsManageJournalEntry.controller.ProperRecordsManageJournalEntryController'						 
       
    ],
    refs: [{
            ref: 'UserLoginWindowView',
            selector: 'userLoginWindowView'
    }],
    launch: function () {
        var me = this;              
        MineralPro.config.Runtime.appName = 'Minerals'  
        MineralPro.config.RuntimeUtility.disableBackspace();
        MineralPro.config.RuntimeUtility.enablePasswordChecking();
        MineralPro.config.RuntimeUtility.ImplementVtypes();
                        
//        MineralPro.config.RuntimeUtility.addThemeOptions();                      
        setTimeout(function () {
            Ext.globalEvents.fireEvent('checkUserSession');
            
               setTimeout(function () {
                
               if(MineralPro.config.Runtime.appraisalYear.length == 0){
                    me.getStore('MineralPro.store.AppraisalYearStore').load({
                        callback: function(){
                            var appYearStore = me.getStore('MineralPro.store.AppraisalYearStore')
                            var activeYearIndex = appYearStore.find('activeAppraisalYear', 'Y')
                            if(activeYearIndex != -1){
                                MineralPro.config.Runtime.appraisalYear = appYearStore.getAt(activeYearIndex).get('appraisalYear')
                            }else{
                                MineralPro.config.Runtime.appraisalYear = new Date().getFullYear();
                            }
                            
                           me.getStore('MineralPro.store.CountryStore').load();
                           me.getStore('MineralPro.store.StateStore').load();
                           // me.getStore('MineralPro.store.AppraisalYearStore').load();              
                           me.getStore('Minerals.store.ProperRecords.AgencyStore').load();
                           me.getStore('Minerals.store.ProperRecords.OwnerComboStore').load();
                           me.getStore('Minerals.store.ProperRecords.ExemptionStore').load();
                           me.getStore('Minerals.store.MineralValuation.QuickNoteStore').load();
                           me.getStore('Minerals.store.MineralValuation.NoteTypeStore').load();
                           me.getStore('Minerals.store.ProperRecords.GravityCdStore').load();
                           me.getStore('Minerals.store.MineralValuation.OperatorComboStore').load();
                           me.getStore('Minerals.store.MineralValuation.FieldComboStore').load();
                           me.getStore('Minerals.store.MineralValuation.WellTypeComboStore').load();
                        }
                    })                  
               }else{
                    me.getStore('MineralPro.store.CountryStore').load();
                    me.getStore('MineralPro.store.StateStore').load();
                    // me.getStore('MineralPro.store.AppraisalYearStore').load();              
                    me.getStore('Minerals.store.ProperRecords.AgencyStore').load();
                    me.getStore('Minerals.store.ProperRecords.OwnerComboStore').load();
                    me.getStore('Minerals.store.ProperRecords.ExemptionStore').load();
                    me.getStore('Minerals.store.MineralValuation.QuickNoteStore').load();
                    me.getStore('Minerals.store.MineralValuation.NoteTypeStore').load();
                    me.getStore('Minerals.store.ProperRecords.GravityCdStore').load();
                    me.getStore('Minerals.store.MineralValuation.OperatorComboStore').load();
                    me.getStore('Minerals.store.MineralValuation.FieldComboStore').load();
                    me.getStore('Minerals.store.MineralValuation.WellTypeComboStore').load();
               }                                  
           }, 700);
            
       }, 200);
        Ext.on('resize', function () {
            var windowLogin = me.getUserLoginWindowView();
            if (windowLogin) {
                windowLogin.center();
            }
        });
    },
//	mainView: 'MineralPro.view.MainViewPort'
});
