///**
// * Handles the controller processing for ProperRecordsManageOwner
// */

Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.controller.ProperRecordsManageJournalEntryController', {
    extend: 'Ext.app.Controller',
  
    models: [
        'Minerals.sub.ProperRecordsManageJournalEntry.model.JEBatchesModel',
        'Minerals.sub.ProperRecordsManageJournalEntry.model.JournalEntriesModel',
        'Minerals.sub.ProperRecordsManageJournalEntry.model.LeaseOwnersModel',
    ],
    
    stores: [
        'Minerals.sub.ProperRecordsManageJournalEntry.store.JEBatchesStore',
        'Minerals.sub.ProperRecordsManageJournalEntry.store.JournalEntriesStore',
        'Minerals.sub.ProperRecordsManageJournalEntry.store.LeaseOwnersStore',
    ],  
    
    views: [
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntryView',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntryHeaderView',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JEBatches.JEBatchesView',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JEBatches.JEBatchesGridView',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JEBatches.JEBatchesDetailView',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JEBatches.JEBatchesGridViewController',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JEBatches.JEBatchesAddEditFormView',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JEBatches.JEBatchesAddEditFormViewController',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JournalEntriesDetailView',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JEAddEditFormView',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JEAddEditFormViewController',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JEModifyHoldReasonFormView',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JEModifyHoldReasonFormViewController',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JEModifyHoldReasonGridView',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JournalEntriesGridView',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JournalEntriesGridViewController',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JournalEntriesView',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.LeaseOwners.LeaseOwnersGridViewController',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.LeaseOwners.LeaseOwnersGridView',
        'Minerals.sub.ProperRecordsManageJournalEntry.view.LeaseOwners.LeaseOwnersView',

    ],          
    
    requires: ['Ext.form.ComboBox',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden'],

});


    
