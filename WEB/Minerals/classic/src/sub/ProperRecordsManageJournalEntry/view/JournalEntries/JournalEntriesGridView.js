
Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JournalEntriesGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.JournalEntriesGridView',
    store:'Minerals.sub.ProperRecordsManageJournalEntry.store.JournalEntriesStore',

    requires: ['Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JournalEntriesGridViewController'],
    controller: 'JournalEntriesGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'jeIdItemId', 
    mainPanelAlias: 'ProperRecordsManageJournalEntry', //widget alias name of the main panel
    mainListModel: 'Minerals.sub.ProperRecordsManageJournalEntry.model.JournalEntriesModel',     
    
    editFormAliasName: 'JEntryEditFormView',
    //selectFirstRow: true,

    detailAliasName: 'JournalEntriesDetailView',
    withDetailsView: true, //true if it has details view
    withChildTab: true,
    
    firstFocus: 'jeIdItemId',    
    
         
    columns:[{ 
        header: 'JE ID',
        flex     : 0,
       // maxWidth: 75,
        dataIndex: 'idJournalEntry',
        itemId: 'jeIdItemId',
        filterElement: new Ext.form.TextField()
    },{ 
        header: 'Appr Year',
        flex     : 0,
       // maxWidth: 75,
        dataIndex: 'selectedAppraisalYear',
        itemId: 'appraisalYearItemId',
        filterElement: new Ext.form.TextField()
    },{   
        header: 'User',
        flex     : 1, 
      //  minWidth: 160,
        dataIndex: 'nameAbbreviation',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Reason',
        flex     : 2,
      //  minWidth: 100,
        dataIndex: 'jeReason',
        filterElement:new Ext.form.TextField()    
    },{   
        header: 'Process Date',
        flex     : 0,
      //  minWidth: 100,
        dataIndex: 'collectionExtractDt',
        filterElement:new Ext.form.TextField()     
    },{  
        header: 'Tax Officer Notify',
        flex     : 1,
      //  minWidth: 100,
        dataIndex: 'taxOfficeNotify',
        filterElement:new Ext.form.TextField()     
    },{  
        header: 'Hold Reason',
        flex     : 1,
      //  minWidth: 100,
        dataIndex: 'jeHold',
        filterElement:new Ext.form.TextField()     
    },{
    
     xtype: 'actioncolumn',
        width: 70,
        itemId: 'JEReprotItemId',
        items: [MineralPro.config.util.MainGridActionColCmpt.editCls,
                MineralPro.config.util.MainGridActionColCmpt.deleteCls,{
                icon:MineralPro.config.Runtime.crystalIconCls,
                tooltip:MineralPro.config.Runtime.crystalTooltip,
                handler: function (grid, rowIndex, colIndex, item, e, record) {
                        this.up('grid').getController().generateCrystalReport(grid, rowIndex, colIndex, item, e, record)
                },
            },
            
            
            //MineralPro.config.util.MainGridActionColCmpt.deleteCls
        ],
        // listeners: {
        //     afterrender: function(){
        //         var me = this       
        //         me.setHidden(MineralPro.config.Runtime.access_level)
        //     }
        // }
    }],
 
    initComponent: function () {  
        var me = this;
        // me.selModel= Ext.create('Ext.selection.CheckboxModel',{
        //     checkOnly: true,
        //      listeners: {
        //         selectionchange: function(){
        //             var items = me.getSelectionModel().getSelected().length;
        //             var mainPanel = me.up(me.mainPanelAlias);
        //             var store = me.getStore();
        //             var detailView = me.up(me.mainPanelId).down(me.detailAliasName);
                   
                    
        //             if(mainPanel.selectedId == 99999999 && store.data.length != 0){
        //                 if(items){
        //                     me.down('#ModifyHoldReasonId').enable();
        //                     me.down('#ProcessJEId').disable();
        //                 }else{
        //                     me.down('#ModifyHoldReasonId').disable();
        //                     me.down('#ProcessJEId').enable();
        //                     detailView.reset();
        //                 }
        //             }else{
        //                 if(items){
        //                     me.down('#ModifyHoldReasonId').enable();
        //                     me.down('#ProcessJEId').disable();
        //                 }else{
        //                     me.down('#ModifyHoldReasonId').disable();
        //                     me.down('#ProcessJEId').disable();
        //                     detailView.reset();
        //                 }
        //             }
                    
        //         }
        //     } 
        // }),  
        
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')]

        var ModifyHoldReason = {
                xtype: 'button',
                tooltip: 'Modify Hold Reason',
                text: 'Modify Hold Reason',
                iconCls: 'b_inline_edit',
                //disabled: true,
                itemId: 'ModifyHoldReasonId',
                listeners: {
                    click: 'onModifyHoldReason',
                    afterrender: function(){
                        var me = this
                        me.setHidden(MineralPro.config.Runtime.access_level)
                    }
                }
            }

        var ProcessJE = {
                xtype: 'button',
                disabled: true,
                itemId: 'ProcessJEId',
                tooltip: 'Process Journal Entry',
                text: "Process JE's",
                iconCls: 'calculator',
                listeners: {
                    click: 'onProcessJE',
                    afterrender: function(){
                        var me = this
                        me.setHidden(MineralPro.config.Runtime.access_level)
                    }
                }
            }    
        me.tools= [
        ModifyHoldReason,
        ProcessJE,
        MineralPro.config.util.MainGridToolCmpt.saveToDbButton,                   
        MineralPro.config.util.MainGridToolCmpt.clearFilterButton],

        me.callParent(arguments);
        
    },
    listeners: {
        selectionchange: 'onLoadSelectionChange',
        afterlayout: 'onAfterLayout',
    }    
});
