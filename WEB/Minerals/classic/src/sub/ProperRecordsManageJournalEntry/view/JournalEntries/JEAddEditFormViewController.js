//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JEAddEditFormViewController', {
    extend: 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.JEAddEditFormViewController',
    
    onEditListToGrid: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAddEditFormViewController onEditListToGrid.')
        var me = this;
        var view = me.getView();
        var mainPanel = Ext.getCmp('JournalEntryViewId');
        var listGrid = mainPanel.down(view.listGridAlias)
        
        var win = button.up('window');
        var form = win.down('form');
        //var record = form.getRecord();
	    var record = listGrid.getStore().getAt(view.editRecordIndex);
        var values = form.getValues();
        if(form.isValid()){
            record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
            record.set('appUser', MineralPro.config.Runtime.idAppUser);
            record.set('nameAbbreviation', MineralPro.config.Runtime.nameAbbreviation);
            record.set(values);   
           // record.set('jeReason', values.reason+'-'+values.jeReason);
            record.set('jeReason', values.jeReason);             

            if(listGrid.withDetailsView){
                var detailView = mainPanel.down(listGrid.detailAliasName);
                detailView.loadRecord(record);
            }
            
            win.close();                             
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        } 
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAddEditFormViewController onEditListToGrid.')
    },
});