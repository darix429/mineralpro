
Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JournalEntriesDetailView', {
    extend: 'MineralPro.config.view.BaseDetailView',
    alias: 'widget.JournalEntriesDetailView',
    
    title: 'Details',
    itemId: 'JournalEntriesDetailViewId',
    defaults: {
        width: 275,
        labelWidth: 93
    },   
    items: [{
        xtype:'textfield',
        fieldLabel:'Batch ID',
        name: 'batchId',
        readOnly: true
    },{
        xtype:'textfield',
        fieldLabel:'JE ID',
        name: 'idJournalEntry',
        readOnly: true
    },{
        xtype:'textfield',
        fieldLabel:'Open Dt',
        name: 'openDt',
        readOnly: true
    },{
        xtype:'textfield',
        fieldLabel:'Close Dt',
        name: 'closeDt',
        readOnly: true
    },{
        xtype:'textarea',
        fieldLabel:'Description',
        name: 'description',
        readOnly: true
    }]
});