Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JEModifyHoldReasonGridView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JEModifyHoldReasonFormViewController'],
    controller: 'JEModifyHoldReasonFormViewController',
    alias: 'widget.JEModifyHoldReasonGridView',

    title: 'Journal Entries Grid',
    id: 'JEModifyHoldReasonGridViewId',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#appraisalYearItemId',
    width: 1000,
    height: 585,
    
    mainPanelId: 'JournalEntryViewId', //id of the main panel
    listGridStore: 'Minerals.sub.ProperRecordsManageJournalEntry.store.JournalEntriesStore', //store for Lease Owner grid
    listGridAlias: 'JournalEntriesGridView',
    //calcAppraisalStore: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalCalcStore',
    
    //mainPanelId: 'MineralValuationManageAppraisalId', //id of the main panel


    initComponent: function () {
        var me = this; 
        me.items = [{
            xtype: 'grid',
            id:'gridView',
            store: 'Minerals.sub.ProperRecordsManageJournalEntry.store.JournalEntriesStore',
            mainListModel: 'Minerals.sub.ProperRecordsManageJournalEntry.model.JournalEntriesModel',
            columns:[{ 
                header: 'JE ID',
                flex     : 0,
               // maxWidth: 75,
                dataIndex: 'idJournalEntry',
                itemId: 'jeIdItemId',
                filterElement: new Ext.form.TextField()
            },{ 
                header: 'Appr Year',
                flex     : 0,
               // maxWidth: 75,
                dataIndex: 'selectedAppraisalYear',
                itemId: 'appraisalYearItemId',
                filterElement: new Ext.form.TextField()
            },{   
                header: 'User',
                flex     : 1, 
              //  minWidth: 160,
                dataIndex: 'nameAbbreviation',
                filterElement:new Ext.form.TextField()
            },{   
                header: 'Reason',
                flex     : 2,
              //  minWidth: 100,
                dataIndex: 'jeReason',
                filterElement:new Ext.form.TextField()    
            },{   
                header: 'Process Date',
                flex     : 0,
              //  minWidth: 100,
                dataIndex: 'collectionExtractDt',
                filterElement:new Ext.form.TextField()     
            },{  
                header: 'Tax Officer Notify',
                flex     : 1,
              //  minWidth: 100,
                dataIndex: 'taxOfficeNotify',
                filterElement:new Ext.form.TextField()     
            },{  
                header: 'Hold Reason',
                flex     : 1,
              //  minWidth: 100,
                dataIndex: 'jeHold',
                filterElement:new Ext.form.TextField()     
            }],

            
            
            selModel: Ext.create('Ext.selection.CheckboxModel',{
                checkOnly: true,
                listeners: {
                   selectionchange: function(){
                    var grid = Ext.getCmp('gridView')
                    var items = grid.getSelectionModel().getSelected().length;
                    var mainPanel = me.up(me.mainPanelAlias);
                    var store = grid.getStore();
                           if(items){
                                me.down('#ModifyHoldReasonId').enable();
                           }else{
                                me.down('#ModifyHoldReasonId').disable();
                           }
                   }
               }
            }), 
            
            plugins: [Ext.create('Ext.ux.grid.FilterRow')]                             
        }];
    
    
        var ModifyHoldReason = {
            xtype: 'button',
            tooltip: 'Modify Hold Reason',
            text: 'Modify Hold Reason',
            iconCls: 'b_inline_edit',
            disabled: true,
            itemId: 'ModifyHoldReasonId',
            listeners: {
                click: 'onModifyHoldReason',
                afterrender: function(){
                    var me = this
                    me.setHidden(MineralPro.config.Runtime.access_level)
                }
            }
        }
        
        me.buttons= [ModifyHoldReason,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]

        me.callParent(arguments);
    },
    
    listeners:{
        close: function(){
            var gridselection = this.down('grid');
            var cols = gridselection.columns;

            Ext.each(cols, function (col) {
            if (col.filterElement)
                col.filterElement.setValue('');
            });
        }

    },  
    
});