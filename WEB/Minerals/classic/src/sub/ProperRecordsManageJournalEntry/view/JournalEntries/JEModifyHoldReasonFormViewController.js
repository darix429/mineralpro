//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JEModifyHoldReasonFormViewController', {
    extend: 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.JEModifyHoldReasonFormViewController',
    onEditListToGrid: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAddEditFormViewController onEditListToGrid.')
        var me = this;        
        var view = me.getView();
        var mainPanel = Ext.getCmp(view.mainPanelId);
        var listStore = Ext.getStore('Minerals.sub.ProperRecordsManageJournalEntry.store.JournalEntriesStore');
        var listGrid = Ext.getCmp('gridView');
        
        var win = button.up('window');
        var form = win.down('form');
        //var record = form.getRecord();
        var selected = listGrid.getSelectionModel().getSelection();
        var values = form.getValues();
        
        if(form.isValid()){
            if(selected.length>0){
                listGrid.up('window').close()  
                Ext.each(selected, function(el, index, items){
                    var record = listStore.findRecord('idJournalEntry', el.data.idJournalEntry , 0, false, false, true);
                        record.set('jeHold', values.jeHold);
                        record.set('jeHoldCd', values.jeHoldCd);
                        record.set('nameAbbreviation', MineralPro.config.Runtime.nameAbbreviation);
                        record.set('appUser', MineralPro.config.Runtime.idAppUser);
                        record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
                })
            }else{
                Ext.MessageBox.show({  
                    iconCls: Minerals.config.Runtime.multipleEditAlertIcon,
                    title: Minerals.config.Runtime.multipleEditAlertTitle,    
                    msg: 'Please select/check record to be Modify.',                
                    buttons: Ext.Msg.OK
                });
            }                
            win.close();                             
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        } 
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAddEditFormViewController onEditListToGrid.')
    },
    onModifyHoldReason: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering JournalEntryGridController onModifyHoldReason.')
        
        var me = this;
        var grid = Ext.getCmp('gridView')
        var store = grid.getStore();
        var selected = grid.getSelectionModel().getSelection();
        var dirty = store.getRemovedRecords().length > 0; // || store.getNewRecords().length > 0 || store.getUpdatedRecords().length > 0;
        if(!dirty){
            if(selected.length>0){   
                //var view = Ext.getCmp('JEModifyHoldReasonFormViewId');
                //if(view){
                //       Ext.getCmp('JEModifyHoldReasonFormViewId').show();
                //}else{
                var view = new Ext.widget('JEModifyHoldReasonFormView');
                //}
                var record = Ext.create(grid.mainListModel);
                record.set(selected[0].data);
                record.set('nameAbbreviation', MineralPro.config.Runtime.nameAbbreviation)
                record.set('appUser', MineralPro.config.Runtime.idAppUser)
                view.down('form').loadRecord(record);
                view.show();
            }else{
                Ext.Msg.show({
                    iconCls: Minerals.config.Runtime.multipleEditAlertIcon,
                    title: Minerals.config.Runtime.multipleEditAlertTitle,    
                    msg: Minerals.config.Runtime.multipleEditAlertMsg,                
                    buttons: Ext.Msg.OK
                });
            }
        }else{
            Ext.Msg.show({
                    iconCls: Minerals.config.Runtime.multipleEditAlertIcon,
                    title: 'Warning',    
                    msg: "Deleted row can't Modify Hold Reason",                
                    buttons: Ext.Msg.OK
                });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving JournalEntryGridController onModifyHoldReason.')
    },
});