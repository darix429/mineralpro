Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JournalEntriesView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.JournalEntriesView',
    layout:'border',
    
    id: 'JournalEntriesViewId',
    
    items: [{
           // title: 'Owners',
            xtype: 'JournalEntriesGridView',
            region:'center'
        },{
            xtype: 'JournalEntriesDetailView',
            region:'east'
        }] 
});    