//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JournalEntriesGridViewController', {
    extend:'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.JournalEntriesGridViewController',
    onLoadSelectionChange: function (model, records, a, b) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering JournalEntriesGridViewController onLoadSelectionChange.')
            var me = this.getView();
            
            var mainPanel = me.up(me.mainPanelAlias);
            var parentTab = false;

            if(mainPanel.selectedId != null){
                parentTab = true;
            }
            
        if (parentTab) {
            var detailView = me.up(me.mainPanelId).down(me.detailAliasName);
            if(records.length > 0){
                mainPanel.selectedJournalEntryId = records[0].get('idJournalEntry');
                mainPanel.appYear = records[0].get('selectedAppraisalYear');
                detailView.loadRecord(records[0]);
            }                 
        }
      MineralPro.config.RuntimeUtility.DebugLog('Leaving JournalEntriesGridViewController onLoadSelectionChange.')
    },
  
    generateCrystalReport: function(grid, rowIndex, colIndex, item, e, record){
        MineralPro.config.RuntimeUtility.DebugLog('Entering JournalEntriesGridViewController generateCrystalReprot.')
            var me = this.getView();
            var rec = me.getStore().getAt(rowIndex)
            var mainPanel = me.up(me.mainPanelAlias);
            var reportID = record.get('reportId');
            var idJournalEntry = record.get('idJournalEntry')
            grid.getSelectionModel().select(rowIndex)
            // var url = window.location.origin + '/CrystalReport/'
            //                     +reportID+'/'
            //                     +mainPanel.selectedId+'/'
            //                     +idJournalEntry+'/'
                        
            //            window.open(url);
                    Ext.Ajax.request({
                            waitMsg: 'Generating Journal Report Preview...',
                            url: '/CrystalReport/'+reportID+'',
                            method:'GET',
                            params: {
                               batch_id: mainPanel.selectedId,
                               je_id: idJournalEntry
                            },
                            success: function(response){
                                var el = Ext.DomHelper.append(Ext.getBody(), {
                                    tag:          'a',
                                    href:          response.request.requestOptions.url,
                                    modal:true,
                                    target: '_blank'
                                });
                                el.click();
                            },
                            // failed: function(response){
                            //     Ext.MessageBox.show({
                            //         title:'Export Failed',
                            //         msg: 'Cannot process your request.',
                            //         buttons: Ext.Msg.OK,
                            //         icon: Ext.Msg.ERROR,
                            //     });
                            // }
                        });
            // Ext.MessageBox.show({
            //     title:'Generate Journal Entry Report',
            //     msg: 'Downloaded File Info <br /><br />'+
            //         'File name: JournalEntryReport-'+Ext.Date.format(new Date(), 'Y-m-d H:i')+'<br />'+
            //         'Batch ID: '+Ext.getCmp('JournalEntryViewId').selectedId+' <br />'+
            //         'User: '+MineralPro.config.Runtime.nameAbbreviation+'<br />',
            //     buttons: Ext.Msg.OKCANCEL,
            //     icon: Ext.Msg.INFO,
            //     fn: function(btn){
                        
            //         if (btn == "ok"){
                              
            //         }
            //     }                
            // });
        MineralPro.config.RuntimeUtility.DebugLog('Leaving JournalEntriesGridViewController generateCrystalReprot.')
    },
  
     onProcessJE: function(button, rowIndex){
        MineralPro.config.RuntimeUtility.DebugLog('Entering JournalEntriesGridViewController onProcessJE.')
        var me = this;
        var view = me.getView();
        var store = view.getStore();
        
        Ext.Msg.show({
            title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
            msg:'Are you sure you want to commit changes?',
            iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
            buttons: Ext.Msg.YESNO,
            scope: this,
            width: 250,
            fn: function (btn) {
                if (btn === 'yes') {
                    Ext.getBody().mask('Loading...') 
                    store.each(function(record){
                        record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
                        record.set('batchId', 'newBatchId');
                    }); 
                    store.getProxy().extraParams = {
                        updateLength: store.getUpdatedRecords().length,
                        process: 'processJe'
                    };
                    store.sync({
                        callback: function (records, operation, success) {
                            store.load();
                            var centerView = me.getView().up('centerView');
                            var westView = centerView.up().down('westView');
                            var favoriteTree = westView.down('#favoriteTreeId');
                            var menuTree = westView.down('#menuTreeId');
                            var dataTree=true;
                            var index = 0;
                            
                            favoriteTree.getStore().each(function(c, fn){
                                if(c.data.functionName == 'Manage Journal Entry'){
                                    index = fn;
                                    dataTree=false;
                                }
                            })            
                            menuTree.getStore().each(function(c, fn){
                                if(c.data.functionName == 'Manage Journal Entry'){
                                    index = fn;
                                    dataTree=true;
                                }
                            })
                            
                            if(dataTree == true){
                                menuTree.getSelectionModel().select(index);
                                setTimeout(function () {
                                    var tabPanel = centerView.down('ProperRecordsManageJournalEntry');
                                    tabPanel.setActiveTab(0);
                                },700 )
                            }else{
                                favoriteTree.getSelectionModel().select(index);
                                setTimeout(function () {
                                    var tabPanel = centerView.down('ProperRecordsManageJournalEntry');
                                    tabPanel.setActiveTab(0);
                                },700 )
                            }
                            Ext.getBody().unmask(); 
                            }
                    });
                }
            }
        })
        MineralPro.config.RuntimeUtility.DebugLog('Leaving JournalEntriesGridViewController onProcessJE.')
    },
  
    onModifyHoldReason: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering JournalEntryGridController onModifyHoldReason.')
        var view = Ext.getCmp('JEModifyHoldReasonGridViewId');
        if(view){
               Ext.getCmp('JEModifyHoldReasonGridViewId').show();
        }else{
             view = new Ext.widget('JEModifyHoldReasonGridView');
             view.show();
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving JournalEntryGridController onModifyHoldReason.')
    },
    onAfterLayout: function(){
        MineralPro.config.RuntimeUtility.DebugLog('Entering JournalEntriesGridViewController onAfterLayout.')
        var me = this;
        var grid = me.getView();
        var mainPanel = grid.up(grid.mainPanelAlias);
        var store = grid.getStore();
        
        if(mainPanel.selectedId == 99999999 && store.data.length != 0){
            grid.down('#ProcessJEId').enable();
        }else{
            grid.down('#ProcessJEId').disable();
        }
        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving JournalEntriesGridViewController onAfterLayout.')
    },
    onSyncListGridData: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onSyncListGridData.')
        var me = this;       
        var view = me.getView();
        var listStore = view.getStore();
                                  
        var dirty = listStore.getNewRecords().length > 0
        || listStore.getUpdatedRecords().length > 0
        || listStore.getRemovedRecords().length > 0;
        if (dirty) {
            Ext.Msg.show({
                title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                msg:MineralPro.config.Runtime.syncGridMsgConfimationMsg,
                iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                buttons: Ext.Msg.YESNO,
                scope: this,
                width: 250,
                fn: function (btn) {
                    if (btn === 'yes') {
                        view.getSelectionModel().deselectAll(true);
                        listStore.getProxy().extraParams = {
                            createLength: listStore.getNewRecords().length,
                            updateLength: listStore.getUpdatedRecords().length
                        };

                        listStore.sync({
                            callback: function (records, operation, success) { 
                                var mainPanel = view.up(view.mainPanelAlias)
                                if(mainPanel.selectedId){
                                    listStore.getProxy().extraParams = {
                                        selectedId: mainPanel.selectedId
                                    };
                                }
                                listStore.reload({
                                    callback: function () {
                                        // if (view.getStore().getAt(0)) { 
                                        //         view.getSelectionModel().select(0, false, false);
                                        // }                                     
                                    }
                                });
                                if(view.affectedComboStore){
                                    var affectedComboStoreArr = view.affectedComboStore;
                                        Ext.each(affectedComboStoreArr, function(store){
                                             Ext.StoreMgr.lookup(store).reload();
                                        })
                                }
                            }
                        });
                        listStore.commitChanges();
                    }
                }
            });
        } else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                title:MineralPro.config.Runtime.listSyncMsgErrorTitle,
                msg:MineralPro.config.Runtime.listSyncMsgErrorMsg,
                buttons: Ext.Msg.OK
            });                      
        }                         
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onSyncListGridData.')
    },
});
