Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JEAddEditFormView', {
    extend: 'CPRegion.view.region.JEAddFormView',
    requires: ['Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JEAddEditFormViewController'],
    controller: 'JEAddEditFormViewController',
    alias: 'widget.JEntryEditFormView',
    
    title: 'Edit Journal Entry',
    defaultFocus: '#reasonItemId',

    listeners: {
        afterrender: function(){
            var me = this;
            me.down('#batchIdItemId').show();
            me.down('#openDtItemId').show();
            me.down('#closeDtItemId').show();
            me.down('#collectionExtractDtItemId').show();
        }
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});