Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JEModifyHoldReasonFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntries.JEModifyHoldReasonFormViewController'],
    controller: 'JEModifyHoldReasonFormViewController',
    alias: 'widget.JEModifyHoldReasonFormView',
    
    title: 'Modify Hold Reason',
    id: 'JEModifyHoldReasonFormViewId',
    defaultFocus: '#jeHoldCdItemId',
    bodyPadding: 5,
    layout:'fit',
    mainPanelId: 'JournalEntryViewId', //id of the main panel
    listGridStore: 'Minerals.sub.ProperRecordsManageJournalEntry.store.JournalEntriesStore', //store for Lease Owner grid
    listGridAlias: 'JournalEntriesGridView', 
    initComponent: function() {
        var me = this;

        me.items = [{
            xtype: 'form',
            defaults:{ 
                maskRe: /[^'\^]/,
                labelWidth: 100,
                width: 400
            },
            margin:'0 0 0 0',
            bodyPadding: 0,
            items: [{
                    xtype: 'container',
                    fieldLabel: 'Reason',
                    combineErrors: true,
                    layout: 'hbox', 
                    labelAlign: 'right',
                    defaults:{
                        width:'50%',
                    },
                    items: [{
                        fieldLabel: 'Appraisal Year',
                        xtype: 'displayfield',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        margin:'0 0 0 0', 
                        name: 'selectedAppraisalYear', 
                    },{
                        fieldLabel: 'Batch ID',
                        margin:'0 0 0 0',
                        xtype: 'displayfield',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        name: 'batchId',  
                    }]
                },{
                    fieldLabel: 'User',
                    xtype: 'displayfield',
                    margin:'0 0 0 0',
                    labelAlign: 'right',
                    msgTarget: 'side',
                    name: 'nameAbbreviation',  
                },{
                        fieldLabel: 'Hold JE',
                        selectOnFocus: true,
                        tabIndex: 2,
                        name : 'jeHoldCd',
                        xtype:'combo',
                        flex: 2,
                        itemId:'jeHoldCdItemId',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        displayField: 'displayField',
                        valueField: 'valueField',   
                        store: 'MineralPro.store.CdJeHoldStore',
                        typeAhead: true,
                        allowBlank: true,
                        queryMode:'local',
                        listeners:{
                            change: function(a, newValue, oldValue){
                                me.down('#jeHoldItemId').setValue(me.down('#jeHoldCdItemId').getRawValue());
                            }
                        },
                        forceSelection: true,
                        msgTarget: 'side' 
                    }, {       
                        xtype: 'hidden',     
                        name: 'jeHold',   
                        queryMode:'local',
                        itemId:'jeHoldItemId' 
                    }]
            }];
        this.callParent(arguments);
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});