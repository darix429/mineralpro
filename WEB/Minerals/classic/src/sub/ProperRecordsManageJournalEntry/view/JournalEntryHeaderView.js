Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntryHeaderView' ,{
    extend: 'MineralPro.config.view.BaseHeaderView',
    alias : 'widget.ProperRecordsManageJournalEntryHeader',  
    layout: 'hbox',
    defaults: {
        style: {borderColor: '#000000', borderStyle: 'solid', borderWidth: '1px'},
        xtype: 'fieldset',
        layout: 'vbox',
        width: '24%',
        height: 95,
        margin: '0, 2, 0, 2',
        cls: 'header_fieldset',
    },
    items: [{
        title: 'Batch Information',
        width: '30%',
        layout: 'hbox',
        defaults: {
            xtype: 'container',
            layout: 'vbox',
            width: '100%',
            height: 95
        },
            items: [{
                defaults: {
                    xtype: 'displayfield',
                    fieldStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
                    labelStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px',
                    margin: '0 0 0 2',
                    padding: '0 0 0 0',
                    width: '100%',
                    labelWidth: 80,
                },               
                items:[{
                    labelWidth: 50,
                    fieldLabel: 'Batch ID:',
                    name: 'batchId',
                }]
            }]
    },{
        title: 'Before Information',
        width: '30%',
        layout: 'vbox',
        defaults: {
            xtype: 'container',
            layout: 'vbox',
            width: '100%',
        },
            items: [{
                defaults: {
                    xtype: 'displayfield',
                    fieldStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
                    labelStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px',
                    margin: '0 7 0 2',
                    padding: '0 0 0 0',
                    width: '100%',
                    labelWidth: 100,
                    renderer: function (val, meta) {
                        return  Ext.util.Format.number(val, '$000,000');
                    },
                },               
                items:[{
                    labelWidth: 210,
                    fieldLabel: 'Total Market Value :',
                    name: 'beforeOwnerValue',
                }]
            }]
    },{
        title: 'After Information',
        width: '40%',
        layout: 'vbox',
        defaults: {
            xtype: 'container',
            layout: 'vbox',
            width: '100%',
        },
            items: [{
                defaults: {
                    xtype: 'displayfield',
                    fieldStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
                    labelStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px',
                    margin: '0 7 0 2',
                    padding: '0 0 0 0',
                    width: '100%',
                    labelWidth: 100,
                    renderer: function (val, meta) {
                        return  Ext.util.Format.number(val, '$000,000');
                    },
                },               
                items:[{
                    labelWidth: 310,
                    fieldLabel: 'Total Market Value :',
                    name: 'afterOwnerValue',
                }]
            }]
    }],   
});    
