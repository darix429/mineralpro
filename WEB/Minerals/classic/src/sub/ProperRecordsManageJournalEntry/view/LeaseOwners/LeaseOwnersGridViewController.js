
Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.LeaseOwners.LeaseOwnersGridViewController', {
    extend: 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.JELeaseOwnersGridViewController',

    generateCrystalReprot: function(grid, rowIndex, colIndex, item, e, record){
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseOwnersGridViewController generateCrystalReprot.')
         var me = this.getView();
            var mainPanel = me.up(me.mainPanelAlias);
            var reportID = record.get('reportId');
            var idJournalEntry = record.get('idJournalEntry')
            grid.getSelectionModel().select(rowIndex)
            // var url = window.location.origin + '/CrystalReport/'
            //                     +reportID+'/'
            //                     +mainPanel.selectedId+'/'
            //                     +idJournalEntry+'/'
                               
                        
            //            window.open(url);
                    Ext.Ajax.request({
                            waitMsg: 'Generating Journal Report Preview...',
                            url: '/CrystalReport/'+reportID+'',
                            method:'GET',
                            params: {
                               batch_id: mainPanel.selectedId,
                               je_id: idJournalEntry
                            },
                            success: function(response){
                                var el = Ext.DomHelper.append(Ext.getBody(), {
                                    tag:          'a',
                                    href:          response.request.requestOptions.url,
                                    modal:true,
                                    target: '_blank'
                                });
                                el.click();
                            },
                            // failed: function(response){
                            //     Ext.MessageBox.show({
                            //         title:'Export Failed',
                            //         msg: 'Cannot process your request.',
                            //         buttons: Ext.Msg.OK,
                            //         icon: Ext.Msg.ERROR,
                            //     });
                            // }
                        });
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseOwnersGridViewController generateCrystalReprot.')
    },
    onOpenSelectedTabView: function (record, rowIndex) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseOwnersGridViewController onOpenSelectedTabView.')
        var me = this;
		var centerView = me.getView().up('centerView');
		var westView = centerView.up().down('westView');
        var favoriteTree = westView.down('#favoriteTreeId');
        var menuTree = westView.down('#menuTreeId');
        var dataTree=true;
        var index = 0;
        
        favoriteTree.getStore().each(function(c, fn){
            if(c.data.functionName == 'Manage Lease'){
                index = fn;
                dataTree=false;
            }
        })            
        menuTree.getStore().each(function(c, fn){
            if(c.data.functionName == 'Manage Lease'){
                index = fn;
                dataTree=true;
            }
        })
        
        if(dataTree == true){
            menuTree.getSelectionModel().select(index);
            setTimeout(function () {
                var tabPanel = centerView.down('MineralValuationManageLease');
                var cols = centerView.down('#MineralsLeasesId').down('grid').columns;
                
                tabPanel.setActiveTab(0);
                Ext.each(cols, function (col) {
                    if(col.dataIndex == 'leaseId'){
                        col.filterElement.setValue(record.data.leaseId);
                    }
                });
            },800 )
        }else{
            favoriteTree.getSelectionModel().select(index);
            setTimeout(function () {
                var tabPanel = centerView.down('MineralValuationManageLease');
                var cols = centerView.down('#MineralsLeasesId').down('grid').columns;

                tabPanel.setActiveTab(0);
                Ext.each(cols, function (col) {
                    if(col.dataIndex == 'leaseId'){
                        col.filterElement.setValue(record.data.leaseId);
                    }
                });
            },800 )
        }
		
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseOwnersGridViewController onOpenSelectedTabView.')
    },
});