
Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.LeaseOwners.LeaseOwnersGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.JELeaseOwnersGridView',
    store:'Minerals.sub.ProperRecordsManageJournalEntry.store.LeaseOwnersStore',
    requires: ['Minerals.sub.ProperRecordsManageJournalEntry.view.LeaseOwners.LeaseOwnersGridViewController'],
    controller: 'JELeaseOwnersGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'appraisalYearItemId', 
    mainPanelAlias: 'ProperRecordsManageJournalEntry', //widget alias name of the main panel
    mainListModel: 'Minerals.sub.ProperRecordsManageJournalEntry.model.LeaseOwnerstModel',     

   
    firstFocus: 'appraisalYearItemId',    

    
    iconCls: 'icon-grid',
    // features: [{
    //         id: 'group',
    //         ftype: 'groupingsummary',
    //         groupHeaderTpl: '{columnName}:{name}',
    //        // hideGroupedHeader: true,
    //         startCollapsed: true  
    //     }],
    columns: [{ 
        header: 'JE ID',
        flex     : 0,
       // maxWidth: 75,
        dataIndex: 'idJournalEntry',
        filterElement: new Ext.form.TextField()
    },{   
        header: 'Lease ID',
        flex     : 1, 
      //  minWidth: 160,
        dataIndex: 'leaseId',
        filterElement:new Ext.form.TextField(),
        renderer: function (value) {
                        return '<a href="javascript:void(0);" >' + value + '</a>';
                    },
        listeners: {
            click: function (cell, td, rowIndex, cellIndex, e, record, value, index) {
                   this.up('grid').getController().onOpenSelectedTabView(record, rowIndex);
            }
        }
    },{   
        header: 'Owner ID',
        flex     : 1,
      //  minWidth: 100,
        dataIndex: 'ownerId',
        filterElement:new Ext.form.TextField()    
    },{   
        header: 'Interest Type',
        flex     : 1,
      //  minWidth: 100,
        dataIndex: 'interestType',
        filterElement:new Ext.form.TextField()     
    },{  
        header: 'App Year',
        flex     : 1,
      //  minWidth: 100,
        dataIndex: 'appraisalYear',
        filterElement:new Ext.form.TextField()     
    },{  
        header: 'Before/After',
        flex     : 1,
      //  minWidth: 100,
        dataIndex: 'beforeAfter',
        filterElement:new Ext.form.TextField()     
    },{  
        header: 'Int Pct',
        flex     : 1,
      //  minWidth: 100,
        dataIndex: 'interestPercent',
        filterElement:new Ext.form.TextField()     
    },{  
        header: 'Owner Name',
        flex     : 1,
      //  minWidth: 100,
        dataIndex: 'ownerName',
        filterElement:new Ext.form.TextField()     
    },{  
        header: 'Agent Name',
        flex     : 1,
      //  minWidth: 100,
        dataIndex: 'agentName',
        filterElement:new Ext.form.TextField()     
    },{  
        header: 'Market Value',
        flex     : 1,
      //  minWidth: 100,
        align: 'right',
        dataIndex: 'ownerValue',
         renderer: function(val, meta) {
            return  Ext.util.Format.number(val, '$000,000');
        },
        filterElement:new Ext.form.TextField()     
    },{  
        header: 'Reason',
        flex     : 1,
      //  minWidth: 100,
        dataIndex: 'jeReason',
        filterElement:new Ext.form.TextField()     
    },{  
        header: 'User',
        flex     : 1,
      //  minWidth: 100,
        dataIndex: 'userName',
        filterElement:new Ext.form.TextField()     
    },{  
        header: 'Tax Office',
        flex     : 1,
      //  minWidth: 100,
        dataIndex: 'taxOfficeNotify',
        filterElement:new Ext.form.TextField()     
    },{  
        header: 'Hold Rsn',
        flex     : 1,
      //  minWidth: 100,
        dataIndex: 'jeHold',
        filterElement:new Ext.form.TextField()     
    },{
    
     xtype: 'actioncolumn',
        width: 50,
        itemId: 'LOReprotItemId',
        items: [{
                icon:MineralPro.config.Runtime.crystalIconCls,
                tooltip:MineralPro.config.Runtime.crystalTooltip,
                handler: function (grid, rowIndex, colIndex, item, e, record) {
                        this.up('grid').getController().generateCrystalReprot(grid, rowIndex, colIndex, item, e, record)
                },   
            },
            //MineralPro.config.util.MainGridActionColCmpt.deleteCls
        ],
        // listeners: {
        //     afterrender: function(){
        //         var me = this       
        //         me.setHidden(MineralPro.config.Runtime.access_level)
        //     }
        // }
    }],

   
    initComponent: function () {  
        var me = this;
      
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')];

        if(true){
                
                var noteStore = Ext.StoreMgr.lookup(me.store);                             
                noteStore.on({
                    load: function(store, records, successful, operation, eOpts ){
                            // var mainPanel = me.up(me.mainPanelAlias);
                            // me.down('#rrcNumberItemId').setText('RRC Number: ' + mainPanel.selectedRRCNumber);                            
                    },
                });
                
                noteStore.on({
                    beforeload: function (s) {
                        var mainPanel = Ext.getCmp('JournalEntryViewId')
                        var params = this.getProxy().extraParams
                        params['selectedId'] = mainPanel.selectedId
                        params['selectedJournalEntryId'] = mainPanel.selectedJournalEntryId
                        params['appYear'] = mainPanel.appYear
                        this.getProxy().extraParams = params;
                    }
                });

                noteStore.on({
                    beforesync: function () {
                        var mainPanel = Ext.getCmp('JournalEntryViewId')
                        var params = this.getProxy().extraParams
                        params['idAppUser'] = MineralPro.config.Runtime.idAppUser;
                        params['selectedId'] = mainPanel.selectedId
                        params['selectedJournalEntryId'] = mainPanel.selectedJournalEntryId
                        params['appYear'] = mainPanel.appYear
                        this.getProxy().extraParams = params;
                    },
                })
        }

        me.tools= [
        // MineralPro.config.util.MainGridToolCmpt.addNewDataButton,
        // MineralPro.config.util.MainGridToolCmpt.saveToDbButton, 
       // MineralPro.config.util.MainGridToolCmpt.displayField,                   
        MineralPro.config.util.MainGridToolCmpt.clearFilterButton
        ],   
        
        me.callParent(arguments);
        
    }    
});