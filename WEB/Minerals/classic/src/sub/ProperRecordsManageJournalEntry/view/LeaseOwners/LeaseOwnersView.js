Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.LeaseOwners.LeaseOwnersView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.JELeaseOwnersView',
    layout:'border',
    
    id: 'JELeaseOwnersViewId',
    
    items: [{
            xtype: 'JELeaseOwnersGridView',
            html:'center',
            region:'center'
        }] 
});    