
Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JEBatches.JEBatchesDetailView', {
    extend: 'MineralPro.config.view.BaseDetailView',
    alias: 'widget.JEBatchesDetailView',
    
    title: 'Batch Details',
    itemId: 'jEBatchesDetail',
    defaults: {
        width: 275,
        labelWidth: 93
    },   
    items: [
    // {
    //     xtype:'textfield',
    //     fieldLabel:'Non-Value JE’s:',
    //     name: 'valueChangeCount',
    //     readOnly: true
    // },
    // {
    //     xtype:'textfield',
    //     fieldLabel:'Value JE`s',
    //     name: 'nonValueChangeCount',
    //     readOnly: true
    // },
    {
        xtype:'textfield',
        fieldLabel:'Lease Owner Changes',
        name: 'leaseOwnerCount',
        readOnly: true
    },
    {
        xtype:'textarea',
        fieldLabel:'Description',
        name: 'description',
        readOnly: true
    }]
});