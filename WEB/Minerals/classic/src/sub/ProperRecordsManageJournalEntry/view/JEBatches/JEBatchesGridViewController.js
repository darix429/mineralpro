
Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JEBatches.JEBatchesGridViewController', {
    extend: 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.JEBatchesGridViewController',

    GenerateReport: function(button, rowIndex){
        MineralPro.config.RuntimeUtility.DebugLog('Entering JEBatchesGridViewController GenerateReport.')
       
        MineralPro.config.RuntimeUtility.DebugLog('Leaving JEBatchesGridViewController GenerateReport.')
    },
    generateCrystalReprot: function(grid, rowIndex, colIndex, item, e, record){
        MineralPro.config.RuntimeUtility.DebugLog('Entering JEBatchesGridViewController generateCrystalReprot.')
         var me = this.getView();
            var mainPanel = me.up(me.mainPanelAlias);
            var reportID = record.get('reportId');
            var idJournalEntry = 0;
            grid.getSelectionModel().select(rowIndex)
            // var url = window.location.origin + '/CrystalReport/'
            //                     +reportID+'/'
            //                     +mainPanel.selectedId+'/'
            //                     +idJournalEntry+'/'
                        
            //            window.open(url);
                    Ext.Ajax.request({
                            waitMsg: 'Generating Journal Report Preview...',
                            url: '/CrystalReport/'+reportID+'',
                            method:'GET',
                            params: {
                               batch_id: mainPanel.selectedId,
                               je_id: idJournalEntry
                            },
                            success: function(response){
                                var el = Ext.DomHelper.append(Ext.getBody(), {
                                    tag:          'a',
                                    href:          response.request.requestOptions.url,
                                    modal:true,
                                    target: '_blank'
                                });
                                el.click();
                            },
                            // failed: function(response){
                            //     Ext.MessageBox.show({
                            //         title:'Export Failed',
                            //         msg: 'Cannot process your request.',
                            //         buttons: Ext.Msg.OK,
                            //         icon: Ext.Msg.ERROR,
                            //     });
                            // }
                        });
        MineralPro.config.RuntimeUtility.DebugLog('Leaving JEBatchesGridViewController generateCrystalReprot.')
    },
    onLoadSelectionChange: function (model, records) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onLoadSelectionChange.')
          var me = this;

          if (!me.xtype) {
                me = me.getView();              
           }
            
          var mainPanel = me.up(me.mainPanelAlias);
            var parentTab = false;
            var showHeader = false;
            if(mainPanel.selectedId != null){
                //var mainStore = me.getStore();
                //var rowIndex = mainStore.find('batchId', mainPanel.selectedId, 0, false, false, true);
                //me.getSelectionModel().select(rowIndex);
                parentTab = true;
            }
            
            if (parentTab) {
                var detailView = me.up(me.mainPanelId).down(me.detailAliasName);

                showHeader = true;

                if(records.length > 0){
                    mainPanel.selectedId = records[0].get(mainPanel.selectedIdIndex);
                    mainPanel.selectedName = records[0].get(mainPanel.selectedNameIndex);

                    var headerDetailStore = Ext.StoreMgr.lookup(me.headerInfoStore);
                        headerDetailStore.getProxy().extraParams = {
                        selectedId: mainPanel.selectedId
                    };

                    detailView.loadRecord(records[0]);
                }                      
                updateHeader(me, records, showHeader);                  
            }else{
                detailView.loadRecord(records[0]);
                updateHeader(me, records, showHeader);
            }
            
                                       
            function updateHeader (me, records, showHeader){    
                MineralPro.config.Runtime.headerInfoXtype='';
                if(me.headerInfo && me.headerInfo.length>0 && records.length > 0 && showHeader){
                    var headerInfo = new Ext.create(me.headerInfo);
                    headerInfo.loadRecord(records[0])          
                   MineralPro.config.Runtime.headerInfoXtype=headerInfo                
              }              
               Ext.globalEvents.fireEvent('updateHeader');
            }          
       
      MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onLoadSelectionChange.')
    }
    
});