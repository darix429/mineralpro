
Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JEBatches.JEBatchesGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.JEBatchesGridView',
    store:'Minerals.sub.ProperRecordsManageJournalEntry.store.JEBatchesStore',
    requires: ['Minerals.sub.ProperRecordsManageJournalEntry.view.JEBatches.JEBatchesGridViewController'],
    controller: 'JEBatchesGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'batchIdtemId', 
    mainPanelAlias: 'ProperRecordsManageJournalEntry', //widget alias name of the main panel
    mainListModel: 'Minerals.sub.ProperRecordsManageJournalEntry.model.JEBatchesModel',
    
    firstFocus: 'batchIdtemId',
    detailAliasName: 'JEBatchesDetailView',   
    withDetailsView: true, //true if it has details view

    headerInfo: 'Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntryHeaderView',
    headerInfoStore: 'Minerals.sub.ProperRecordsManageJournalEntry.store.JEBatchesStore',

    withChildTab: true,
    iconCls: 'icon-grid',
    editFormAliasName: 'JEBatchesEditFormView',
    //selectFirstRow: false,

    
    columns:[{ 
        header: 'Batch ID',
        flex     : 0,
        sortable: true,
        dataIndex: 'batchId',
        itemId: 'batchIdtemId',
        filterElement: new Ext.form.TextField()
    },{   
        header: 'Create Date',
        flex     : 1,
        sortable: true,
        dataIndex: 'createDt',
        filterElement:new Ext.form.TextField(),
    },{   
        header: 'Process Date',
        flex     : 1,
        sortable: true,
        dataIndex: 'processDt',
        filterElement:new Ext.form.TextField()    
     },{   
        header: 'Nbr of JE`s',
        flex     : 0,
        sortable: true,
        dataIndex: 'jeCount',
        filterElement:new Ext.form.TextField()     
    },{   
        header: 'Total Market Value (Before)',
        flex     : 1,
        sortable: true,
        align: 'right',
        dataIndex: 'beforeOwnerValue',
        renderer: function(val, meta) {
            return  Ext.util.Format.number(val, '$000,000');
        },
        filterElement:new Ext.form.TextField() 
    },{   
        header: 'Total Market Value (After)',
        flex     : 1,
        sortable: true,
        align: 'right',
        dataIndex: 'afterOwnerValue',
        renderer: function(val, meta) {
            return  Ext.util.Format.number(val, '$000,000');
        },
        filterElement:new Ext.form.TextField()      
    },{
    
     xtype: 'actioncolumn',
        width: 50,
        itemId: 'JEBReprotItemId',
        items: [MineralPro.config.util.MainGridActionColCmpt.editCls,{
                icon:MineralPro.config.Runtime.crystalIconCls,
                tooltip:MineralPro.config.Runtime.crystalTooltip,
                handler: function (grid, rowIndex, colIndex, item, e, record) {
                        this.up('grid').getController().generateCrystalReprot(grid, rowIndex, colIndex, item, e, record)
                },   
            },
            //MineralPro.config.util.MainGridActionColCmpt.deleteCls
        ],
        // listeners: {
        //     afterrender: function(){
        //         var me = this       
        //         me.setHidden(MineralPro.config.Runtime.access_level)
        //     }
        // }
    }],
      
    initComponent: function () {  
        var me = this;
       
        
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')]

        var GenerateReportButton = {
                xtype: 'button',
                disabled: true,
                itemId: 'GenerateReportButtonButtonId',
                tooltip: 'Generate Report',
                text: 'Generate Report',
                iconCls: '',
                listeners: {
                    click: 'GenerateReport',
                    afterrender: function(){
                        var me = this
                        me.setHidden(MineralPro.config.Runtime.access_level)
                    }
                }
            } 
           
        me.tools= [   
        //GenerateReportButton,        
        MineralPro.config.util.MainGridToolCmpt.saveToDbButton,                     
        MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
        me.callParent(arguments);
        
    }  
});
