Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JEBatches.JEBatchesAddEditFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.sub.ProperRecordsManageJournalEntry.view.JEBatches.JEBatchesAddEditFormViewController'],
    controller: 'JEBatchesAddEditFormViewController',
    alias: 'widget.JEBatchesEditFormView',
    
    mainPanelId: 'JournalEntryViewId', //id of the main panel
    listGridStore: 'Minerals.sub.ProperRecordsManageJournalEntry.store.JEBatchesStore',
    listGridAlias: 'JEBatchesGridView', 

    title: 'Edit Description',
    defaultFocus: '#descriptionId',
    
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'form',
            defaults:{ maskRe: /[^'\^]/},
            margin:'0 5 0 0',
            bodyPadding: 5,
            items: [{
                    fieldLabel: 'Batch ID',
                    xtype: 'displayfield',
                    labelAlign: 'right',
                    msgTarget: 'side',
                    itemId: 'batchItemId',
                },{
                    fieldLabel: 'Description',
                    xtype: 'textareafield',
                    name : 'description',
                    itemId: 'descriptionId',
                    tabIndex: 3,
                    width: 750,
                    height: 190, 
                    labelAlign: 'right',
					maxLength: 50
                    //allowBlank: false,
                }]
            
        }];
        
        this.callParent(arguments);
    },
    listeners:{
        afterrender: function(){
            var me = this;          
            var mainPanel = Ext.getCmp(me.mainPanelId)
            me.down('#batchItemId').setValue(mainPanel.selectedId);      
        },
    },
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});