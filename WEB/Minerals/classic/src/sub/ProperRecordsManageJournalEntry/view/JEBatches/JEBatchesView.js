Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JEBatches.JEBatchesView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.JEBatchesView',
    layout:'border',
    
    id: 'JEBatchesViewId',
    
    items: [{
            xtype: 'JEBatchesGridView',
            html:'center',
            region:'center'
        },{
            xtype: 'JEBatchesDetailView',
            region:'east'
        }] 
});    