Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.view.JournalEntryView' ,{
    extend: 'MineralPro.config.view.BaseTabPanelView',
    alias : 'widget.ProperRecordsManageJournalEntry',
    layout:'border',
    
    
    selectedId: '0',//Need default value '0'
    selectedIdIndex: 'batchId',
    //selectedName: '0',//Need default value '0'
    //selectedNameIndex: 'leaseName',
    //selectedRRCNumber: '-1',//Need default value '-1', '0' is used for LeaseNote
    //selectedRRCNumberIndex: 'rrcNumber',

    id: 'JournalEntryViewId',

    items: [{
            title: 'JE Batches',
            xtype: 'JEBatchesView'
        },{
            title: 'Journal Entries',
            xtype: 'JournalEntriesView'
        },{    
            title: 'Lease Owners',
            xtype: 'JELeaseOwnersView'
        }] 

});    