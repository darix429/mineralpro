//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.ProperRecordsManageField.view.ManageFieldAddFormViewController', {
    extend: 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.ManageFieldAddFormViewController', 

    onAddAndNew: function (button) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering ManageFieldAddFormViewController onAddAndNew.')
        var me = this;
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore);
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);

        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();
        var gravityCode = view.down('#gravityCode');
        if (gravityCode.getRawValue() != 'UNASSIGNED') {
            if (form.isValid()) {
                record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                record.set(values);
                listStore.insert(0, record);

                listGrid.getSelectionModel().select(0, false, true);

                form.reset()
                var new_record = Ext.create(listGrid.mainListModel)
                form.loadRecord(new_record);

                setTimeout(function () {
                    view.down('#gravityDesc').setValue(view.down('#gravityCode').getRawValue());
                }, 100);  

                listGrid.fireEvent('afterlayout');

            } else {
                Ext.Msg.show({
                    iconCls: MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: MineralPro.config.Runtime.addEditToGridMsgErrorTitle,
                    msg: MineralPro.config.Runtime.addEditToGridMsgErrorMsg,
                    buttons: Ext.Msg.OK
                });
            }
        }else{
             gravityCode.focus()
             me.showAddEditAlert()
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving ManageFieldAddFormViewController onAddAndNew.')
    },onAddListToGrid: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering ManageFieldAddFormViewController onAddListToGrid.')
        var me = this;                     
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore); 
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();  
        var gravityCode = view.down('#gravityCode');
        if (gravityCode.getRawValue() != 'UNASSIGNED') {
            if(form.isValid()){
                record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                record.set(values);    
                win.close();
                listStore.insert(0,record);
                
                listGrid.getSelectionModel().select(0, true);  
                listGrid.getView().focusRow(0);   
                //listGrid.fireEvent('afterlayout');
            }else {
                Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                    msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                    buttons: Ext.Msg.OK
                });
            }
        }else{
             gravityCode.focus()
             me.showAddEditAlert()
        }    
        MineralPro.config.RuntimeUtility.DebugLog('Leaving ManageFieldAddFormViewController onAddListToGrid.')
    },
    onEditListToGrid: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering ManageFieldAddFormViewController onEditListToGrid.')

        var me = this;        
        var view = me.getView();
        var mainPanel = Ext.getCmp(view.mainPanelId);
        var listGrid = mainPanel.down(view.listGridAlias)
        
        var win = button.up('window');
        var form = win.down('form');
        //var record = form.getRecord();
	    var record = listGrid.getStore().getAt(view.editRecordIndex);
        var values = form.getValues();
        var gravityCode = view.down('#gravityCode');
        if (gravityCode.getRawValue() != 'UNASSIGNED') {
            if(form.isValid()){
                record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                record.set(values);                

                if(listGrid.withDetailsView){
                    var detailView = mainPanel.down(listGrid.detailAliasName);
                    detailView.loadRecord(record);
                }
                
                win.close();                             
            }else {
                Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                    msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                    buttons: Ext.Msg.OK
                });
            }
        }else{
             gravityCode.focus()
             me.showAddEditAlert()
        } 
        MineralPro.config.RuntimeUtility.DebugLog('Leaving ManageFieldAddFormViewController onEditListToGrid.')
    },
    showAddEditAlert: function (){
        Ext.Msg.show({
            iconCls: Minerals.config.Runtime.addEditTaxUnitMsgErrorIcon,
            title: Minerals.config.Runtime.addEditTaxUnitMsgErrorTitle,
            msg: Minerals.config.Runtime.addEditTaxUnitMsgErrorMsg,
            buttons: Ext.Msg.OK
        });
    },
});