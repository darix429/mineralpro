Ext.define('Minerals.sub.ProperRecordsManageField.view.ManageFieldEditFormView', {
    extend: 'Minerals.sub.ProperRecordsManageField.view.ManageFieldAddFormView',
    //requires: ['Minerals.sub.ProperRecordsManageField.view.ManageFieldEditFormViewController'],
    //controller: 'ManageFieldEditFormViewController',
    alias: 'widget.ManageFieldEditFormView',
    
    
    title: 'Edit Field',
    defaultFocus: '#fieldName',

    listeners: {
        afterrender: function(){
            var me = this;
            var fieldId = me.down('#fieldId');
                fieldId.disable(true);
               // fieldId.show();
        }
    },

    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});