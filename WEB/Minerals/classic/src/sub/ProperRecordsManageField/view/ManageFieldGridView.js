
Ext.define('Minerals.sub.ProperRecordsManageField.view.ManageFieldGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.ManageFieldGridView',
    store:'Minerals.sub.ProperRecordsManageField.store.ManageFieldStore',

    requires: ['Minerals.sub.ProperRecordsManageField.view.ManageFieldGridViewController'],
    controller: 'ManageFieldGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'fieldId', 
    mainPanelAlias: 'ProperRecordsManageField', //widget alias name of the main panel
    mainListModel: 'Minerals.sub.ProperRecordsManageField.model.ManageFieldModel',     

    addFormAliasName: 'ManageFieldAddFormView', //widget form to call for add list 
    editFormAliasName: 'ManageFieldEditFormView', //widget form to call for edit
   
    affectedComboStore: ['Minerals.store.MineralValuation.FieldComboStore'],//combos store for field

    firstFocus: 'fieldId',    
    
    addButtonTooltip: 'Add New Field',
    addButtonText: 'Add New Field',
        
    columns:[{ 
        header: 'Field No.',
        flex     : 1,
        dataIndex: 'fieldId',
        itemId: 'fieldId',
        filterElement: new Ext.form.TextField()
    },{   
        header: 'Field Name',
        flex     : 1,
        dataIndex: 'fieldName',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Field Depth',
        flex     : 1,
        dataIndex: 'fieldDepth',
        filterElement:new Ext.form.TextField()    
    },{   
        header: 'Gravity Code',
        flex     : 1,
        dataIndex: 'gravityDesc',
        filterElement:new Ext.form.TextField()     
    },{
     xtype: 'actioncolumn',
        width: Minerals.config.Runtime.actionColWidth,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls,
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    }],
 
    initComponent: function () {  
        var me = this;
               
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],       
               
        me.tools= [
        MineralPro.config.util.MainGridToolCmpt.addNewDataButton
        ,MineralPro.config.util.MainGridToolCmpt.saveToDbButton                   
        ,MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
        me.callParent(arguments);
        
    }    
});