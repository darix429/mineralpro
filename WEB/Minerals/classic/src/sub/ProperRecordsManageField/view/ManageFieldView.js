Ext.define('Minerals.sub.ProperRecordsManageField.view.ManageFieldView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.ProperRecordsManageField',
    layout:'border',
    
    id: 'ProperRecordsManageFieldId',
    
    items: [{
        xtype:'ManageFieldGridView',
        html:'center',
        region:'center'
    }]
});    