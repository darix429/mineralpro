Ext.define('Minerals.sub.ProperRecordsManageField.view.ManageFieldAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.sub.ProperRecordsManageField.view.ManageFieldAddFormViewController'],
    controller: 'ManageFieldAddFormViewController',
    alias: 'widget.ManageFieldAddFormView',

    title: 'Add New Field',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#fieldId',
    width: 450,
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'ProperRecordsManageFieldId', //id of the main panel
    listGridStore: 'Minerals.sub.ProperRecordsManageField.store.ManageFieldStore', //store for note grid
    listGridAlias: 'ManageFieldGridView', 

    initComponent: function () {
        var me = this; me.items = [{
            xtype: 'form',
            defaults:{ 
                maskRe: /[^'\^]/,
                labelWidth: 130,
                width: 400
            },
            bodyPadding:5,
            items: [{          
                fieldLabel: 'Field No.',               
                selectOnFocus: true,
                tabIndex: 1,
                itemId: 'fieldId',
                name: 'fieldId',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
                maxLength: 9,
                maskRe:/[0-9]/,
                listeners: {
                     blur: 'onAddCheckExisting',
                     change: function( field, newval, oldval ) {
                        if(newval != '' && newval > 0){
                            field.setValue(newval*1);
                        } else {
                            field.setValue('');
                        }
                    }
                }
               // hidden: true,
            },{
                fieldLabel: 'Field Name',
                selectOnFocus: true,
                tabIndex: 2,
                name: 'fieldName',
                itemId: 'fieldName',
                xtype: 'textfield',
                minValue: '0',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
                maxLength: 50,
                listeners: {
                     blur: 'onAddCheckExisting'
                }
            },{
                fieldLabel: 'Field Depth',
                selectOnFocus: true,
                tabIndex: 2,
                name: 'fieldDepth',
                xtype: 'textfield',
                maskRe:/[0-9.]/,
                maxLength: 9,
                minValue: '0',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
            },{
                fieldLabel: 'Gravity Code',
                xtype: 'combo',
                selectOnFocus: true,
                tabIndex: 3,
                name: 'gravityCd',
                itemId: 'gravityCode',
                typeAhead: true,
                forceSelection: true,
                allowBlank: false,
                displayField: 'gravityDesc',
                valueField: 'gravityCd',
                store: 'Minerals.store.ProperRecords.GravityCdStore',
                listeners:{
                    change:function(record){
                        me.down('#gravityDesc').setValue(me.down('#gravityCode').getRawValue());
                            
                    }
                     
                },
                labelAlign: 'right',
                msgTarget: 'side',
                queryMode: 'local',
                            
            },{
                 fieldLabel: 'gravityDesc',
                 name: 'gravityDesc',
                 xtype: 'hidden',
                 margin: '0 0 10',
                 itemId:'gravityDesc',
                 labelAlign: 'right',    
            }],
        }];

        me.callParent(arguments);
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
        MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});
