///**
// * Handles the controller processing for ProperRecordsManageField
// */

Ext.define('Minerals.sub.ProperRecordsManageField.controller.ProperRecordsManageFieldController', {
    extend: 'Ext.app.Controller',
  
    models: [
        'Minerals.sub.ProperRecordsManageField.model.ManageFieldModel'
    ],
    
    stores: [
        'Minerals.sub.ProperRecordsManageField.store.ManageFieldStore',
    ],  
    
    views: [
        'Minerals.sub.ProperRecordsManageField.view.ManageFieldView',
        'Minerals.sub.ProperRecordsManageField.view.ManageFieldGridView',
        'Minerals.sub.ProperRecordsManageField.view.ManageFieldAddFormView',
        'Minerals.sub.ProperRecordsManageField.view.ManageFieldEditFormView',
    ],          
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden']
});


