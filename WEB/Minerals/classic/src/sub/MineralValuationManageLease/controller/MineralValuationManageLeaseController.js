///**
// * Handles the controller processing for MineralValuationManageLease
// */

Ext.define('Minerals.sub.MineralValuationManageLease.controller.MineralValuationManageLeaseController', {
    extend: 'Ext.app.Controller',
  
    models: [
        'Minerals.model.ProperRecords.OwnerComboModel',
        'Minerals.sub.MineralValuationManageLease.model.OperatorComboModel',
        'Minerals.sub.MineralValuationManageLease.model.LeaseModel',
        'Minerals.sub.MineralValuationManageLease.model.LeaseOwnerModel',
        'Minerals.sub.MineralValuationManageLease.model.LeaseNoteModel',
        'Minerals.sub.MineralValuationManageLease.model.LeaseTaxUnitModel',       
    ],
    
    stores: [
        'Minerals.store.ProperRecords.OwnerComboStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseInterestTypeStore',
//        'Minerals.sub.MineralValuationManageLease.store.OperatorComboStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseChangeReasonStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseStatusStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseOwnerStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseNoteStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseTaxUnitStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseTaxUnitCodeStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseTaxUnitTypeStore',
//        'Minerals.sub.MineralValuationManageLease.store.OperatorComboStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseAppraisalStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseCalcAppraisalStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseFieldsStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseOwnersTransferCalculationStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseOwnersTransferLeaseStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseOwnersTransferStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseHeaderDetailStore',
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsOwnerTransferStore',
        
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalOperatingExpStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalOilDailyAvgStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalGasDailyAvgStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalProdDailyAvgStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedProdStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedSvrcStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedInjStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSubjectIdStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSubjectNameStore',
        'Minerals.sub.MineralValuationManageLease.store.LeaseNoteAppraisalStore'
    ],  
    
    views: [
        'Minerals.sub.MineralValuationManageLease.view.LeaseView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseHeaderView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteAddFormView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteEditFormView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteDetailView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteGridView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteGridViewController',
        'Minerals.sub.MineralValuationManageLease.view.Leases.LeasesView',
        'Minerals.sub.MineralValuationManageLease.view.Leases.LeasesAddFormView',
        'Minerals.sub.MineralValuationManageLease.view.Leases.LeasesAddFormViewController',
        'Minerals.sub.MineralValuationManageLease.view.Leases.LeasesEditFormView',
        'Minerals.sub.MineralValuationManageLease.view.Leases.LeasesGridView',
        'Minerals.sub.MineralValuationManageLease.view.Leases.LeasesGridViewController',
        'Minerals.sub.MineralValuationManageLease.view.Leases.LeasesDetailView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseTaxUnits.LeaseTaxUnitsView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseTaxUnits.LeaseTaxUnitsAddFormView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseTaxUnits.LeaseTaxUnitsAddFormViewController',
        'Minerals.sub.MineralValuationManageLease.view.LeaseTaxUnits.LeaseTaxUnitsEditFormView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseTaxUnits.LeaseTaxUnitsGridView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseTaxUnits.LeaseTaxUnitsGridViewController',
        'Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersImportDO',
        'Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersImportDOController',
        'Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersAddFormView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersAddFormViewController',
        'Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersEditFormView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersGridView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersGridViewController',
        'Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersDetailView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersTransferBaseFormView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersTransferFormViewController',
        'Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersTransferMultipleFormView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersTransferCalculationGridFormView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersTransferSelectedLeaseFormView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseFields.LeaseFieldsView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseFields.LeaseFieldsGridView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseAppraisal.LeaseAppraisalView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseAppraisal.LeaseAppraisalFormView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseAppraisalNote.LeaseAppraisalNoteView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseAppraisalNote.LeaseAppraisalNoteDetailView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseAppraisalNote.LeaseAppraisalNoteGridView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseAppraisalNote.LeaseAppraisalNoteAddFormView',
        'Minerals.sub.MineralValuationManageLease.view.LeaseAppraisalNote.LeaseAppraisalNoteEditFormView'

    ],          
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden'],

    init: function(){
        var me = this;
        
        setTimeout(function () {
            me.getStore('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedProdStore').load();
            me.getStore('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedSvrcStore').load();
            me.getStore('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedInjStore').load();
        }, 200);
    }

});


    
