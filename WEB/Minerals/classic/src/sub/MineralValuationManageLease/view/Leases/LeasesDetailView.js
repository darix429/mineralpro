Ext.define('Minerals.sub.MineralValuationManageLease.view.Leases.LeasesDetailView', {
    extend: 'MineralPro.config.view.BaseDetailView',
    alias: 'widget.LeasesDetailView',

    mainPanelId: 'MineralValuationManageLeaseId', //id of the main panel
    listGridStore: 'Minerals.sub.MineralValuationManageLease.store.LeaseStore', //store for note grid
    listGridAlias: 'LeasesGridView',
    
    title: 'Lease Details',
    defaults: {
        width: 300,
        labelWidth: 70,
    },   
    items: [{
        xtype:'textfield',
        fieldLabel:'Change Reason',
        name: 'changeReason',
        labelAlign: 'right',
        readOnly: true 
    }, {
        xtype:'textfield',
        fieldLabel:'Operator',
        name: 'operatorName',
        labelAlign: 'right',
        readOnly: true 
    }, {
        xtype:'container',
        layout:'fit',
        defaults: {
        width: 300,
        labelWidth: 70,
        },
        items:[{
            xtype:'displayfield',
            fieldLabel:'Unit',
            fieldCls: 'x-form-text',
            name: 'unitName',
            labelAlign: 'right',
            readOnly: true,
            renderer: function (value) {
                return '<a href="javascript:void(0);" >' + value + '</a>';
            },
            listeners:{
                change:function(record){
                    var UNameView = this;
                    UNameView.up().hide();
                    if(UNameView.rawValue)
                    {
                    UNameView.up().show();
                    }
                },
                afterrender:function(component){
                    var UNameView = this;
                    component.getEl().on('click', function() { 
                         component.up('tabpanel').down('grid').getController().onOpenSelectedTabView(UNameView.rawValue);
                    });  
                    UNameView.up().hide();
                    if(UNameView.rawValue)
                    {
                    UNameView.up().hide();
                    }
                },
  
            }

        }]
         
    }, {
        xtype:'textfield',
        fieldLabel:'Acreage',
        name: 'acres',
        labelAlign: 'right',
        readOnly: true 
    }, {
        xtype:'textfield',
        fieldLabel:'Lease Year',
        name: 'leaseYear',
        labelAlign: 'right',
        readOnly: true 
    }, {
        fieldLabel: 'Comment',
        xtype: 'textareafield',
        itemId: 'note',
		selectOnFocus: true,
        tabIndex: 3,
        width: 300,
        height: 'auto',
        name : 'comment',
        resizable: true,
        margin: '0 0 10',
        labelAlign: 'right',
        msgTarget: 'side',
        enableKeyEvents : true,
        maxLength: 50,
        listeners  : {
            'keyup' : {
                fn: function(record) {
                    var view = this.up();
                    var mainView = Ext.getCmp(view.mainPanelId);
                    var listStore = Ext.StoreMgr.lookup(view.listGridStore);
                    var detailStore = Ext.StoreMgr.lookup('Minerals.sub.MineralValuationManageLease.store.LeaseHeaderDetailStore');  
                    var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
                    var selectedRec = listGrid.getSelection();
                    var leaseId = selectedRec[0].get('leaseId');
                    var findRec = listStore.findRecord('leaseId',leaseId);
                    var findOldRec = detailStore.findRecord('leaseId',leaseId);
                    if(findOldRec.get('comment') !== this.value || this.value == '') {
                        findRec.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                        findRec.set('comment',this.value);
                    } else {
                        findRec.commit();
                    }
                }
            }
        }
    }]
});