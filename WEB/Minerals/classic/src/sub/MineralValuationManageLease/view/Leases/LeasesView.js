Ext.define('Minerals.sub.MineralValuationManageLease.view.Leases.LeasesView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.MineralsLeases',
    layout:'border',
    
    id: 'MineralsLeasesId',
    
    items: [{
            //title: 'Leases',
            xtype: 'LeasesGridView',
            html:'center',
            region:'center'
        },{
            xtype: 'LeasesDetailView',
            region:'east'
        }] 
});    