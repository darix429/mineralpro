//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.MineralValuationManageLease.view.Leases.LeasesGridViewController', {
    extend: 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.LeasesGridViewController',

     onOpenSelectedTabView: function (record, rowIndex) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeasesGridViewController onOpenSelectedTabView.')
        var me = this;
		var centerView = me.getView().up('centerView');
		var westView = centerView.up().down('westView');
        var favoriteTree = westView.down('#favoriteTreeId');
        var menuTree = westView.down('#menuTreeId');
        var dataTree=true;
        var index = 0;
        
        favoriteTree.getStore().each(function(c, fn){
            if(c.data.functionName == 'Manage Unit'){
                index = fn;
                dataTree=false;
            }
        })            
        menuTree.getStore().each(function(c, fn){
            if(c.data.functionName == 'Manage Unit'){
                index = fn;
                dataTree=true;
            }
        })
        
        if(dataTree == true){
            menuTree.getSelectionModel().select(index);
            setTimeout(function () {
                var tabPanel = centerView.down('MineralValuationManageUnit');
                var grid = centerView.down('#UnitsViewId').down('grid');
                var cols = grid.columns;
                
                tabPanel.setActiveTab(0);
                Ext.each(cols, function (col) {
                    if(col.dataIndex == 'unitName'){
                        col.filterElement.setValue(record.trim());
                    }
                });
                var hasFilterAllRowsPlugin = grid.getPlugin('filterAllRows');
                if(hasFilterAllRowsPlugin){
                    grid.fireEvent('runFilter');
                }
            },800 )
        }else{
            favoriteTree.getSelectionModel().select(index);
            setTimeout(function () {
                var tabPanel = centerView.down('MineralValuationManageUnit');
                var grid = centerView.down('#UnitsViewId').down('grid');
                var cols = grid.columns;

                tabPanel.setActiveTab(0);
                Ext.each(cols, function (col) {
                    if(col.dataIndex == 'unitName'){
                        col.filterElement.setValue(record.trim());
                    }
                });
                var hasFilterAllRowsPlugin = grid.getPlugin('filterAllRows');
                if(hasFilterAllRowsPlugin){
                    grid.fireEvent('runFilter');
                }
            },800 )
        }
		
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeasesGridViewController onOpenSelectedTabView.')
    },
});