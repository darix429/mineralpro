Ext.define('Minerals.sub.MineralValuationManageLease.view.Leases.LeasesAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.sub.MineralValuationManageLease.view.Leases.LeasesAddFormViewController'],
    controller: 'LeasesAddFormViewController',
    alias: 'widget.LeasesAddFormView',

    title: 'Add New Lease',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#leaseNameId',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'MineralValuationManageLeaseId', //id of the main panel
    listGridStore: 'Minerals.sub.MineralValuationManageLease.store.LeaseStore', //store for note grid
    listGridAlias: 'LeasesGridView',
    layout: 'fit',
    bodyPadding: 5,
    
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'form',
            defaults:{ maskRe: /[^'\^]/ },
            layout: 'hbox',
            bodyPadding: 5,
            items:[{
                xtype: 'fieldset',
                flex: 1,
                margin:'0 0 5 0',
                title: 'Lease Information',
                autoheight: true,
                defaults:{
                    labelWidth:130,
                    width:400,
                    maskRe: /[^'\^]/ 
                }, 
                items: [{
                    fieldLabel: 'Lease Name',
                    itemId: 'leaseNameId',
					selectOnFocus: true,
                    tabIndex: 1,
                    name: 'leaseName',
                    xtype: 'textfield',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    maxLength:50,
                    allowBlank: false,
                    msgTarget: 'side',
                    enableKeyEvents: true,
                    // - Date: January 11, 2018
                    // - Bug Id: 8673918
                    //start
                    // listeners: {
                    //       blur: 'onAddCheckExisting'                    
                    // }
                    //end
                    
                },{
                    fieldLabel: 'Division Order Date',
                    itemId: 'divisionOrderDtItemId',
					selectOnFocus: true,
                    tabIndex: 2,
                    name: 'divisionOrderDt',
                    xtype: 'datefield',
                    format: 'Y-m-d',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    msgTarget: 'side',
                    allowBlank: true,
                },{       
                        // xtype: 'hidden',     
                        // name: 'divisionOrderDt',   
                        // queryMode:'local',
                        // itemId:'divisionOrderDtItemId',
                        // listeners:{
                        //     change:function(record){
                        //         var value = me.down('#divisionOrderDtItemId').getValue()
                                
                        //         console.log(value)
                        //         me.down('#divisionOrderDatetItemId').setValue(value.substr(0,10));
                        //     }
                        // } 
                },{
                    fieldLabel: 'Division Order Desc',
                    itemId: 'divisionOrderDescription',
					selectOnFocus: true,
                    tabIndex: 3,
                    name: 'divisionOrderDescription',
                    xtype: 'textfield',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    msgTarget: 'side',
                    enableKeyEvents: true,
                    allowBlank: true,
                    maxLength:50,
                },{
                    fieldLabel: 'Legal Description',
                    itemId: 'description',
					selectOnFocus: true,
                    tabIndex: 4,
                    name: 'description',
                    xtype: 'textareafield',
                    width: 400,
                    height: 80,
                    margin: '0 0 10',
                    labelAlign: 'right',
                    msgTarget: 'side',
                    enableKeyEvents: true,
                    allowBlank: false,
                    maxLength:60,
                },{
                    fieldLabel: 'Comment',
                    xtype: 'textareafield',
                    itemId: 'note',
					selectOnFocus: true,
                    tabIndex: 5,
                    width: 400,
                    height: 80,
                    name : 'comment',
                    resizable: true,
                    margin: '0 0 10',
                    labelAlign: 'right',
                    allowBlank: true,
                    msgTarget: 'side',
                    maxLength:50,
                }]  
            },{
                xtype: 'form',
                defaults:{ maskRe: /[^'\^]/ },
                layout: 'vbox',
                items:[{
                    xtype: 'fieldset',
                    defaults:{ 
                        maskRe: /[^'\^]/,
                        width: 350,
                        labelWidth: 100 
                    },
                    flex: 1,
                    margin:'0 0 0 5',
                    title: 'Lease Details',
                    autoheight: true, 
                    items: [{
                        fieldLabel: 'Operator ID',
                        selectOnFocus: true,
                        maskRe:/[0-9]/,
                        tabIndex: 6,
                        name : 'operatorComboId',
                        xtype:'combo',
                        itemId:'operatorComboItemId',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        displayField: 'operatorId',
                        valueField: 'operatorName',
                        readOnly: false,
                        emptyText: 'UNASSIGNED',    
                        store: 'Minerals.store.MineralValuation.OperatorComboStore',
                        typeAhead: true,
                        // forceSelection:true,
                        allowBlank: false,
                        queryMode:'local',
                        listeners:{
                            select:function(record){
                                me.down('#operatorNameComboItemId').setValue(me.down('#operatorComboItemId').getRawValue());
                            }
                        },
                        validator : function(value) {
                            var store = this.getStore()
                            var result = store.findRecord('operatorId', value)
                            if(result){
                                return true
                            } else {
                                return 'must be selected on the list'
                            }
                        }
                    },{
                        fieldLabel: 'Operator Name',
                        selectOnFocus: true,
                        tabIndex: 7,
                        name : 'operatorId',
                        xtype:'combo',
                        itemId:'operatorNameComboItemId',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        displayField: 'operatorName',
                        valueField: 'operatorId',
                        readOnly: false,
                        emptyText: 'UNASSIGNED',    
                        store: 'Minerals.store.MineralValuation.OperatorComboStore',
                        typeAhead: true,
                        forceSelection: true,
                        allowBlank: false,
                        queryMode:'local',
                        listeners:{
                            change:function(record){
                                var operatorId = me.down('#operatorNameComboItemId').getValue()
                                if(!isNaN(operatorId)){
                                    me.down('#operatorComboItemId').setValue(operatorId);
                                    me.down('#operatorNameItemId').setValue(me.down('#operatorNameComboItemId').getRawValue());
                                } else {
                                    me.down('#operatorComboItemId').setValue('');
                                }
                            }
                        }
                    },{       
                        xtype: 'hidden',     
                        name: 'operatorName',   
                        queryMode:'local',
                        itemId:'operatorNameItemId' 
                    },{
                        fieldLabel: 'Unit',
                        selectOnFocus: true,
                        tabIndex: 8,
                        name: 'unitName',
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        readOnly: true, 
                        //validateBlank: true,
                        //allowBlank: false,
                        msgTarget: 'side',
                        // vtype:'phone',
                        enableKeyEvents: true,
                        listeners : {
                            change:function(record){
                                var UNameView = this;
                                UNameView.hide();

                                if(UNameView.rawValue!=0) //Visible for lease with Unit
                                {
                                    me.down('#operatorComboItemId').setReadOnly(true);
                                    me.down('#operatorNameComboItemId').setReadOnly(true);
                                    UNameView.show();
                                }else{
                                    me.down('#operatorComboItemId').setReadOnly(false);
                                    me.down('#operatorNameComboItemId').setReadOnly(false);
                                }
                            
                            },
                            afterrender:function(){
                                var UNameView = this;
                                UNameView.hide();
                                if(UNameView.rawValue!=0)
                                {
                                    me.down('#operatorNameItemId').readOnly = true;
                                    UNameView.show();
                                }else{
                                    me.down('#operatorNameItemId').readOnly = false;
                                }
                            }

                        }
                    },{
                        fieldLabel: 'Acreage',
                        selectOnFocus: true,
                        tabIndex: 9,
                        name: 'acres',
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        maskRe:/[0-9.]/,
                        allowBlank: false,
                        msgTarget: 'side',
                        enableKeyEvents: true,
                        validator: function(v) {
                            if (isNaN(v*1)) {
                                return "Must be a decimal or whole number";
                            }
                            return true;
                        }
                    },{
                        fieldLabel: 'Lease Year',
                        selectOnFocus: true,
                        tabIndex: 10,
                        name: 'leaseYear',
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        maskRe:/[0-9]/,
                        maxLength:4,
                        maxValue:Ext.Date.format(new Date(), 'Y'),
                        msgTarget: 'side',
                        validator : function(v) {
                            var year = Ext.Date.format(new Date(), 'Y');
                            if ((v*1)>=(year-100) && (v*1)<=++year || v=='' || v==0) {
                                return true;  
                            }
                                return "Invalid input Year range from 1960 to 2017";
                        }
                    }
                    ]  
                },{
                    xtype: 'fieldset',
                    defaults:{ 
                        maskRe: /[^'\^]/,
                        width: 350,
                        labelWidth: 100 
                    },
                    flex: 1,
                    margin:'5 0 0 5',
                    autoheight: true, 
                    items: [{
                        fieldLabel: 'CAT Code',
                        selectOnFocus: true,
                        tabIndex: 11,
                        name : 'cadCategoryCd',
                        xtype:'combo',
                        itemId:'cadCategoryCd',
                        margin: '10 0 10',
                        labelAlign: 'right',
                        displayField: 'cadCategory',
                        valueField: 'cadCategoryCd',   
                        store: 'Minerals.sub.MineralValuationManageLease.store.LeaseStatusStore',
                        typeAhead: true,
                        queryMode:'local',
                        listeners:{
                            change:function(record){ 
                                me.down('#cadCategory').setValue(me.down('#cadCategoryCd').getRawValue());
                            }
                            
                        }, 
                        forceSelection: true,
                        allowBlank: true,
                        msgTarget: 'side'  
                    },{       
                        xtype: 'hidden',     
                        name: 'cadCategory',   
                        queryMode:'local',
                        itemId:'cadCategory' 
                    },{
                        fieldLabel: 'Change Reason',
                        selectOnFocus: true,
                        tabIndex: 12,
                        name : 'changeReasonCd',
                        xtype:'combo',
                        itemId:'changeReasonCd',
                        margin: '10 0 10',
                        labelAlign: 'right',
                        displayField: 'changeReason',
                        valueField: 'changeReasonCd',   
                        store: 'Minerals.sub.MineralValuationManageLease.store.LeaseChangeReasonStore',
                        typeAhead: true,
                        queryMode:'local',
                        listeners:{
                            Select:function(record){ 
                                me.down('#changeReason').setValue(me.down('#changeReasonCd').getRawValue());
                            }
                        }, 
                        forceSelection: true,
                        allowBlank: true,
                        msgTarget: 'side'  
                    },{       
                        xtype: 'hidden',     
                        name: 'changeReason',   
                        queryMode:'local',
                        itemId:'changeReason' 
                    }]
                }]  
            }]
            
        }];

        this.listeners = {
            afterrender: function(){
                var correctValue = me.down('#divisionOrderDtItemId').value;
                me.down('#divisionOrderDtItemId').setValue(correctValue.substr(0,10));
            }
        }

        this.callParent(arguments);
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
        MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});
