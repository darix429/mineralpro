Ext.define('Minerals.sub.MineralValuationManageLease.view.Leases.LeasesEditFormView', {
    extend: 'Minerals.sub.MineralValuationManageLease.view.Leases.LeasesAddFormView',
    alias: 'widget.LeasesEditFormView',
    
    title: 'Edit Lease',
    defaultFocus: '#leaseNameId',

    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});