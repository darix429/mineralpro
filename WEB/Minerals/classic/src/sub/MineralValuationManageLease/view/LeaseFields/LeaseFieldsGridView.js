
Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseFields.LeaseFieldsGridView', {
    extend: 'Minerals.config.view.Field.BaseFieldGridView',
    alias: 'widget.LeaseFieldsGridView',
               
    store: 'Minerals.sub.MineralValuationManageLease.store.LeaseFieldsStore',
               
    //below are default items needed for grid checking 
//    firstLoadItemId: 'dateItemId', 
    mainPanelAlias: 'MineralValuationManageLease', //widget alias name of the main panel
    mainPanelId: 'MineralValuationManageLeaseId', //widget alias name of the main panel
//    mainListModel: 'Minerals.model.MineralValuation.NotesModel',     
//
//    addFormAliasName: 'UnitNotesAddFormView', //widget form to call for add list 
//    editFormAliasName: 'UnitNotesEditFormView', //widget form to call for edit
   

//    firstFocus: 'dateItemId',    
    
//    addButtonTooltip: 'Add New Note',
//    addButtonText: 'Add New Note',
        
      
});