Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseFields.LeaseFieldsView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.LeaseFieldsView',
    layout:'border',
    
    id: 'LeaseFieldsViewId',
    
    items: [{
            xtype: 'LeaseFieldsGridView',
            html:'center',
            region:'center'
        }] 
});    