//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersGridViewController', {
    extend:'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.LeaseOwnersGridViewController',

    onSyncListGridData: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onSyncListGridData.')
        var me = this;       
        var view = me.getView();
        var listStore = view.getStore();
        var leaseStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseStore');
        var mainView = Ext.getCmp('MineralValuationManageLeaseId');
        var leaseId = mainView.selectedId;
        var validData = true;
        var totalInterest = 0;
            
        var ScanStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseOwnerStore');
        var allRecords = ScanStore.getData().getSource() || ScanStore.getData();
        allRecords.each( 
            function(owner){
                if(owner.data.rowDeleteFlag == ''){
                    if(owner.data.interestTypeCd !==0){
                        totalInterest = parseFloat(totalInterest)+parseFloat(owner.data.interestPercent);
                    }
                    if(owner.data.interestType == 'UNASSIGNED') {
                        validData = false;    
                    }
                   // if(owner.data.interestPercent > 1 || owner.data.interestPercent == '-0') {
                    if(owner.data.interestPercent == '-0') {
                        validData = false;    
                    }
                }
            }
        );
        
        // leaseStore.clearFilter(true);
        // Ext.each(listStore.data.items, function(el, index, items){
        //     if(el.data.rowDeleteFlag == ''){
        //         if(el.data.interestType == 'UNASSIGNED') {
        //             validData = false;    
        //         }
        //         if(el.data.interestPercent > 1 || el.data.interestPercent == '-0') {
        //             validData = false;    
        //         }
        //         totalInterest = parseFloat(totalInterest)+parseFloat(el.data.interestPercent);
        //     }
        // });
        
        //console.log(totalInterest);
         //   
                if(validData) {                  
                    var dirty = listStore.getNewRecords().length > 0
                    || listStore.getUpdatedRecords().length > 0
                    || listStore.getRemovedRecords().length > 0;
                    if (dirty) {
                        if(parseFloat(totalInterest*1).toFixed(8) <= 1){
                            if(parseFloat(totalInterest*1).toFixed(8)<1){
                                Ext.Msg.show({
                                    title:'Warning',
                                    msg:'Total interest percent is not equal to 100%. <br />Do you want to proceed?',
                                    iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                                    buttons: Ext.Msg.YESNO,
                                    scope: this,
                                    width: 300,
                                    fn: function (btn) {
                                        if (btn === 'yes') {
                                            Ext.getBody().mask('Loading...')
                                            view.getSelectionModel().deselectAll(true);
                                            listStore.getProxy().extraParams = {
                                                createLength: listStore.getNewRecords().length,
                                                updateLength: listStore.getUpdatedRecords().length
                                            };                                
                                            listStore.sync({
                                                callback: function (records, operation, success) {
                                                    if(mainView.selectedId){
                                                        listStore.getProxy().extraParams = {
                                                            selectedId: mainView.selectedId
                                                        };
                                                    }
                                                    // leaseStore.reload({
                                                    //     callback: function () {
                                                            var leaseGridStore = mainView.down('LeasesGridView').getStore();
                                                            var leaseGridSelect= mainView.down('LeasesGridView').getSelectionModel();
                                                            var getSelectedLease = leaseStore.find('leaseId', leaseId, 0, false, false, true);
                                                            leaseGridSelect.deselectAll();
                                                            leaseGridSelect.select(getSelectedLease, false, false);                                    
                                                    //     }
                                                    // });
                                                    listStore.reload({
                                                        callback: function () {
                                                            if (view.getStore().getAt(0)) { 
                                                                view.getSelectionModel().select(0, false, false);
                                                            } else {
                                                                view.down('#ExportDOButtonId').disable();
                                                                view.down('#DeleteSelectedButtonId').disable();
                                                                view.down('#TransferToMultipleGroupButtonId').disable();
                                                            }                                     
                                                        }
                                                    });
                                                    Ext.getBody().unmask()
                                                }
                                            });
                                            listStore.commitChanges();
                                        }
                                    }
                                });
                            } else {
                                Ext.Msg.show({
                                    title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                                    msg:MineralPro.config.Runtime.syncGridMsgConfimationMsg,
                                    iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                                    buttons: Ext.Msg.YESNO,
                                    scope: this,
                                    width: 250,
                                    fn: function (btn) {
                                        if (btn === 'yes') {
                                            Ext.getBody().mask('Loading...')
                                            view.getSelectionModel().deselectAll(true);
                                            listStore.getProxy().extraParams = {
                                                createLength: listStore.getNewRecords().length,
                                                updateLength: listStore.getUpdatedRecords().length,
                                            };                                
                                            listStore.sync({
                                                callback: function (records, operation, success) { 
                                                    if(mainView.selectedId){
                                                        listStore.getProxy().extraParams = {
                                                            selectedId: mainView.selectedId
                                                        };
                                                    }
                                                    // leaseStore.reload({
                                                    //     callback: function () {
                                                            var leaseGridStore = mainView.down('LeasesGridView').getStore();
                                                            var leaseGridSelect= mainView.down('LeasesGridView').getSelectionModel();
                                                            var getSelectedLease = leaseStore.find('leaseId', leaseId, 0, false, false, true);
                                                            leaseGridSelect.deselectAll();
                                                            leaseGridSelect.select(getSelectedLease, false, false);                                    
                                                    //     }
                                                    // });
                                                    listStore.reload({
                                                        callback: function () {
                                                            if (view.getStore().getAt(0)) { 
                                                                view.getSelectionModel().select(0, false, false);
                                                            } else {
                                                                view.down('#ExportDOButtonId').disable();
                                                                view.down('#DeleteSelectedButtonId').disable();
                                                                view.down('#TransferToMultipleGroupButtonId').disable();
                                                            }                                     
                                                        }
                                                    });
                                                    Ext.getBody().unmask()
                                                }
                                            });
                                            listStore.commitChanges();
                                        }
                                    }
                                });
                            }
                        } else{
                            Ext.Msg.show({
                                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                                title: 'Lease Interest Pct Exeed',    
                                msg: 'The total Lease Interest Pct have Exeeded 1.00000000. <br />Do you want to proceed?',   
                                buttons: Ext.Msg.YESNO,
                                scope: this,
                                fn: function (btn) {
                                    if (btn === 'yes') {
                                        Ext.getBody().mask('Loading...')
                                        view.getSelectionModel().deselectAll(true);
                                        listStore.getProxy().extraParams = {
                                            createLength: listStore.getNewRecords().length,
                                            updateLength: listStore.getUpdatedRecords().length
                                        };                                
                                        listStore.sync({
                                            callback: function (records, operation, success) {
                                                if(mainView.selectedId){
                                                    listStore.getProxy().extraParams = {
                                                        selectedId: mainView.selectedId
                                                    };
                                                }
                                                // leaseStore.reload({
                                                //     callback: function () {
                                                        var leaseGridStore = mainView.down('LeasesGridView').getStore();
                                                        var leaseGridSelect= mainView.down('LeasesGridView').getSelectionModel();
                                                        var getSelectedLease = leaseStore.find('leaseId', leaseId, 0, false, false, true);
                                                        leaseGridSelect.deselectAll();
                                                        leaseGridSelect.select(getSelectedLease, false, false);                                    
                                                //     }
                                                // });
                                                listStore.reload({
                                                    callback: function () {
                                                        if (view.getStore().getAt(0)) { 
                                                            view.getSelectionModel().select(0, false, false);
                                                        } else {
                                                            view.down('#ExportDOButtonId').disable();
                                                            view.down('#DeleteSelectedButtonId').disable();
                                                            view.down('#TransferToMultipleGroupButtonId').disable();
                                                        }                                     
                                                    }
                                                });
                                                Ext.getBody().unmask()
                                            }
                                        });
                                        listStore.commitChanges();
                                    }
                                }
                            });
                        }    
                    } else {
                        Ext.Msg.show({
                            iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                            title:MineralPro.config.Runtime.listSyncMsgErrorTitle,
                            msg:MineralPro.config.Runtime.listSyncMsgErrorMsg,
                            buttons: Ext.Msg.OK
                        });                      
                    }
                } else{
                    Ext.Msg.show({
                        iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                        title: 'Invalid Data',    
                        msg: 'New Added Record from Import DO has an Invalid Type of Interest',               
                        buttons: Ext.Msg.OK
                    });
                }
            
                                 
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onSyncListGridData.')
    },
    onTransferMultipleToMultipleGroup: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseOwnersGridViewController onTransferMultipleToMultipleGroup.')
        var me = this;
        var grid = me.getView();
        var store = grid.getStore();
        var selected = grid.getSelectionModel().getSelection();
        var dirty = store.getNewRecords().length > 0
                || store.getUpdatedRecords().length > 0
                || store.getRemovedRecords().length > 0;
        if(!dirty){
            if(selected.length>0){   
               var view = Ext.getCmp('LeaseOwnersTransferBaseFormViewId');
                   if(view){
                       Ext.getCmp('LeaseOwnersTransferBaseFormViewId').show();
                       var gridleaseleasae = view.down('LeaseOwnersTransferMultipleFormView');
                       gridleaseleasae.getSelectionModel().deselectAll();
                  }else{
                     view = new Ext.widget('LeaseOwnersTransferBaseFormView');
                      //  view = new Ext.create('Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersTransferBaseFormView');
                     }
                var gridlease = view.down('LeaseOwnersTransferSelectedLeaseFormView');
                var gridcalc = view.down('LeaseOwnersTransferCalculationGridFormView');
                var leasestore = gridlease.getStore();
                var calcStore = gridcalc.getStore();
                var gridlOwners = view.down('LeaseOwnersTransferMultipleFormView');
                var gridlOwnersStore = gridlOwners.getStore();
                gridlOwnersStore.rejectChanges();
                gridlOwnersStore.reload({
                    callback: function () {
                        Ext.each(selected, function (item) {
                            var getSelectedIndex = gridlOwnersStore.find('ownerId', item.data.ownerId, 0, false, false, true);
                            gridlOwnersStore.removeAt(getSelectedIndex);
                        });
                        
                    }
                })
                leasestore.removeAll();
                calcStore.removeAll();

                leasestore.insert(0, selected);
                leasestore.each(function(c, fn){
                    c.set('ownerTransferPct', c.data.interestPercent);
                })
                gridlease.getSelectionModel().select(0, true);
                gridlease.getView().focusRow(0); 
                store.reload();
                view.show();
            }else{
                Ext.Msg.show({
                    iconCls: Minerals.config.Runtime.multipleEditAlertIcon,
                    title: Minerals.config.Runtime.multipleEditAlertTitle,    
                    msg: Minerals.config.Runtime.multipleEditAlertMsg,                
                    buttons: Ext.Msg.OK
                });
            }
        }else{
            Ext.Msg.show({
                    iconCls: Minerals.config.Runtime.multipleEditAlertIcon,
                    title: 'Warning',    
                    msg: 'Please save changes first.',                
                    buttons: Ext.Msg.OK
                });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseOwnersGridViewController onTransferMultipleToMultipleGroup.')
    },
    uploadExcelFile: function(fileInfo){
        var me = this;
        var grid = me.getView();
        var listStore = grid.getStore();
        var form = fileInfo.up();
        var filename = fileInfo.value.replace(/C:\\fakepath\\/g, '');
        form.submit({
            url: '/api/excel/upload',
            waitMsg: 'Uploading your file...',
            success: function(fm, action) {
                //Get data from Imported data
                listStore.on({
                    load: {
                        fn: function(store,records,successful,operation,eOpts){
                            grid.getView().refresh();
                            var importData = action.result.data;
                            var rec = me.getImportToGrid(importData, filename);
                            var record = rec.record;
                            var changedRowIndexes = rec.changedRowIndexes;
                            var rowIndexes = rec.rowIndexes;
                            var sMessage = rec.sMessage;
                            var removedRecordCount = 0;
                                listStore.getData().each(function(record,i){
                                    // if(record.data.OwnerValues >= 0 || record.data.OwnerValues !== ''){
                                    //     record.set({'OwnerValues':'To be calculated'})        
                                    // }
                                    if(rowIndexes.indexOf(i)==-1){
                                        removedRecordCount ++;
                                        record.set({'rowDeleteFlag':'D'});
                                        record.set({'OwnerValues':'To be calculated'})
                                        record.dirty = true; 
                                    }
                                });
                            
                                //grid.getView().refresh();
                                for (var i = 0; i < record.length; i++) {
                                    listStore.insert(0,record[i]);
                                }

                            Ext.Msg.show({
                                iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
                                title:'Upload Completed',
                                width: 450,
                                padding:20,
                                msg: removedRecordCount>0?sMessage.replace('</fieldset>','<p>Total Removed: '+removedRecordCount+'</p></fieldset>'):sMessage,
                                buttons: Ext.Msg.OK
                            });
                        },
                        single: true
                    }
                });
                listStore.load();
                //me.getView().close();
            },
            failure: function(form, response) {
                Ext.Msg.alert('Invalid File Format', "<p style='text-align:left;padding:0;margin:0'>This File has invalid format and cannot be opened.</p>");
            }
        });
    },
    onImportDO: function(fileInfo){
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseOwnersGridViewController onTransferMultipleToMultipleGroup.')

        var me = this;
        var filename = fileInfo.value.replace(/C:\\fakepath\\/g, '');
        var uploadValid = true;
        var message = "<p style='text-align:center;padding:0;margin:0;font-weight:bold;font-size:13px;'>File is not supported for Import Division Order<ul style='margin:0'>";

        var str = filename.split('_');
        var importLeaseId = str[1];
        var mainView = Ext.getCmp('MineralValuationManageLeaseId');
        var leaseId = mainView.selectedId;

        if(isNaN(filename)){
            var file = filename.split('.');
            if (file[file.length-1]=='xlsx' || file[file.length-1]=='xls') {
                uploadValid = true;
            } else if (file[file.length-1]=='xml'||file[file.length-1]=='xlt'||file[file.length-1]=='xlm'){
                uploadValid = false;
                message += "<li>This File format need to convert <br>into 'xls' or 'xlsx' Format</li>";
            } else if(!importLeaseId) {
                uploadValid = false;
                message += "<li>Please use Downloaded File Format.</li>";
            } else {
                uploadValid = false;
                message += "<li>Invalid File Format.</li>";
            }
        } else {
            message += "<li>No input File specified</li>";
        }

         if(uploadValid) {
            if(importLeaseId==leaseId || !importLeaseId){
                me.uploadExcelFile(fileInfo);
            } else {                
                Ext.MessageBox.show({
                    title:'Warning',
                    iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
                    msg: '<p style="line-height:18px; margin:0px">You are using Different lease ID '+
                    'from Import File Lease ID: '+importLeaseId+' to '+
                    'Current Lease ID: '+leaseId+' <br />'+
                    'Do you want to proceed?',
                    buttons: Ext.Msg.YESNO,
                    //icon: Ext.Msg.WARNING,
                    fn: function(btn){                           
                        if (btn == "yes"){
                            sameDocument = false;
                            me.uploadExcelFile(fileInfo);
                        }
                    }
                });
            }
         } else {                
             Ext.Msg.show({
                 iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
                 title:'Error',
                 msg: message+'</li></ul>',
                 buttons: Ext.Msg.OK
             });
         }
        //  var me = this;
        //  var view = Ext.widget('LeaseOwnersImportDO')
        //  view.show();
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseOwnersGridViewController onTransferMultipleToMultipleGroup.')
    },
    getImportToGrid: function(data, input) {
        var me = this;
        var grid = me.getView();
        var leaseId = Ext.getCmp('MineralValuationManageLeaseId').selectedId;
        var leaseName = Ext.getCmp('MineralValuationManageLeaseId').selectedName;
        var headerStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseHeaderDetailStore');
        var leaseHeaderData = headerStore.findRecord('leaseId', leaseId, 0, false, false, true);
        var listStore = grid.getStore();
        var ownerStore = Ext.getStore('Minerals.store.ProperRecords.OwnerComboStore');
        var appraisalValue = leaseHeaderData.get('appraisedValue');
        var nCount = 0, eCount = 0, dCount = 0, dNames = '', dErrors = '';
        var StoreRecord = []
        var totalInterest = 0;
        var royaltyInterestPercent = 0;
        var workingInterestPercent = 0;
        var royaltyInterestValues = 0;
        var workingInterestValues = 0;
        var interestTypeStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseInterestTypeStore');
        var changedRowIndexes = [];
        var rowIndexes = [];

        grid.getStore().each( 
            function(owner){
                if(owner.data.rowDeleteFlag == '' && owner.data.interestTypeCd !==0){
                    // if(owner.data.interestTypeCd !==4){
                    //     royaltyInterestPercent = parseFloat(royaltyInterestPercent)+parseFloat(owner.data.interestPercent);
                    //     royaltyInterestValues = parseFloat(royaltyInterestValues)+parseFloat(owner.data.OwnerValues);
                    // } else {
                    //     workingInterestPercent = parseFloat(workingInterestPercent)+parseFloat(owner.data.interestPercent);
                    //     workingInterestValues = parseFloat(workingInterestValues)+parseFloat(owner.data.OwnerValues);
                    // }
                    owner.set('rowDeleteFlag','D')
                }
            }
        );
        
        if(data[0].Owner_ID && data[0].Owner_Name){
            var tmp = 0;
            var saveInfo = "[ Required to be resolved ]";
            for (var i = 0; i < data.length; i++) {
                dErrors = '<ul>';
                tmp = dCount;   
                var gridRecord = new listStore.model;
                var interestTypeCd = 0;
                var uName = 'Owner ID';
                var uValue = data[i].Owner_ID;
                var newRecord = true;
                if(data[i].Owner_ID == '' || isNaN(data[i].Owner_ID*1)){
                    uName = 'Owner Name';
                    uValue = data[i].Owner_Name;                     
                }

                if(!isNaN(data[i].Owner_ID*1) && data[i].Owner_ID > 0){
                    var interestType = interestTypeStore.findRecord('interestType', data[i].Interest_Type, 0, false, false, true);
                    var ownerDataValue = 0;
                    var owner_Name = data[i].Owner_Name.toUpperCase(true);
                    var owner_Name2 = data[i].Owner_Name2.toUpperCase(true);
                    var interest_Type = data[i].Interest_Type.toUpperCase(true);

                    if(interestType && interest_Type !== 'UNASSIGNED'){
                        interestTypeCd = interestType.get('interestTypeCd');
                        if(interestTypeCd == 4){
                            ownerDataValue = ((workingInterestValues/workingInterestPercent)*data[i].Interest_Percent);
                        }
                        if(interestTypeCd !== 4 && interestTypeCd !== 0){
                            ownerDataValue = ((royaltyInterestValues/royaltyInterestPercent)*data[i].Interest_Percent);
                        }
                        totalInterest = totalInterest + (1*data[i].Interest_Percent);
                    } else {
                        dErrors += '<li>Invalid Interest Type - <p style="color:red">'+data[i].Interest_Type+'</p></li>'
                        data[i].Interest_Type = 'UNASSIGNED';
                        ++dCount;                       
                    }

                    if(isNaN(data[i].Interest_Percent*1)){
                        dErrors += '<li>Invalid Interest Percent - <p style="color:red">'+data[i].Interest_Percent+'</p></li>'
                        data[i].Interest_Percent = '-0';
                        ++dCount;
                    }

                    if(data[i].Interest_Percent > 1){
                        dErrors += '<li>Interest Percent Exeeds limit - <p style="color:red">'+data[i].Interest_Percent+'</p></li>'
                        ++dCount;
                    }
// Check Names if it match the records -----------------------------------------
                    var ownerStore = Ext.getStore('Minerals.store.ProperRecords.OwnerComboStore');
                    var exist = ownerStore.findRecord('valueField', data[i].Owner_ID, 0, false, false, true)

                    if(exist){
                        //var existNameStore = ownerStore.findRecord('name1', data[i].Owner_Name, 0, false, false, true);
                        var ownerName1 = exist.data.name1;
                        var ownerName2 = exist.data.name2;
                        var namematch1 = false;
                        var namematch2 = false;
                        var saveData = true;                       

                        if(saveData){

                            if(ownerName1.charCodeAt(ownerName1.length-1) == 32){
                                ownerName1 = ownerName1.slice(0, -1)                        
                            }

                            if(ownerName2.charCodeAt(ownerName2.length-1) == 32){
                                ownerName2 = ownerName2.slice(0, -1)                        
                            }

                            if(ownerName2.length == 1 && ownerName2.charCodeAt(0)==32){
                                ownerName2 = '';
                            }

                            if(ownerName1 === owner_Name){
                                namematch1 = true;
                            }
                            if(ownerName2 === owner_Name2){
                                namematch2 = true;
                            }
                        }

                        if(saveData) {
                            if(namematch1){
                                if(namematch2){
                                    
                                    var allRecords = listStore.getData().getSource() || listStore.getData();
                                    allRecords.each(function (record, indx) {
                                        if (record.data.ownerId == data[i].Owner_ID) {
                                            record.set('rowDeleteFlag','')
                                            if (record.data.interestType == interest_Type) {
                                                newRecord = false;
                                                record.set({'rowDeleteFlag':''});
                                                rowIndexes.push(indx);
                                                if (record.data.interestPercent*1 !== data[i].Interest_Percent*1) {
                                                    record.set(data[i]);
                                                    record.set('leaseId', leaseId);
                                                    record.set('leaseName', leaseName);
                                                    record.set('interestType', data[i].Interest_Type.toUpperCase(true));
                                                    record.set('interestTypeCd', interestTypeCd);
                                                    record.set('interestPercent', data[i].Interest_Percent);
                                                    record.set('ownerName', owner_Name);
                                                    record.set('Owner_Name', ownerName1);
                                                    record.set('Owner_Name2', ownerName2);
                                                    record.set('ownerId', data[i].Owner_ID);
                                                    record.set('OwnerValues', '-');
                                                    ++eCount;
                                                    changedRowIndexes.push(indx);
                                                }
                                            }
                                        }
                                    })
                                    
                                    if(newRecord){
                                        ++nCount;//Array Count for new Records
                                        gridRecord.set(data[i]);
                                        gridRecord.set('leaseId', leaseId);
                                        gridRecord.set('leaseName', leaseName);
                                        gridRecord.set('interestType', data[i].Interest_Type.toUpperCase(true));
                                        gridRecord.set('interestTypeCd', interestTypeCd);
                                        gridRecord.set('interestPercent', data[i].Interest_Percent);
                                        gridRecord.set('ownerName', owner_Name);
                                        gridRecord.set('Owner_Name', ownerName1);
                                        gridRecord.set('Owner_Name2', ownerName2);
                                        gridRecord.set('ownerId', data[i].Owner_ID);
                                        gridRecord.set('OwnerValues', '-');
                                        StoreRecord[nCount] = gridRecord;
                                    }

                                } else {
                                    dErrors += '<li>Owner_Name 2 Does not match - <p style="color:red">'+data[i].Owner_Name+', '+data[i].Owner_Name2+'</p></li>'
                                    ++dCount;
                                    saveInfo = "[ Record cannot be saved ]"

                                }
                            } else {
                                dErrors += '<li>Owner_Name 1 Does not match - <p style="color:red">'+data[i].Owner_Name+', '+data[i].Owner_Name2+'</p></li>'
                                ++dCount;
                                saveInfo = "[ Record cannot be saved ]"
                            }
                        }   
                    } else {
                        dErrors += '<li>Owner ID Does Not Exist - <p style="color:red">'+data[i].Owner_Name+', '+data[i].Owner_Name2+'</p></li>'
                        saveInfo = "[ Record cannot be saved ]"
                        ++dCount;
                    }

                    if(tmp !== dCount){
                        dNames +='<fieldset style="border: #D3D3D3 1px solid;margin-bottom:2px">'+uName+': '+uValue+' - '+saveInfo+' '+dErrors+'</ul></fieldset>'
                    }
                } else {
                    if(data[i].Owner_Name){
                        dNames +='<fieldset style="border: #D3D3D3 1px solid;margin-bottom:2px">'+uName+': '+uValue+'<p style="color:red">OWNER ID IS REQUIRED</p></fieldset>'
                        ++dCount; //Count for Excluded records that has invalid input
                    }
                }
            }
            var sMessage = '<p><b>File name: '+ input +'</b></p><fieldset><p>Total Update Record: '+(eCount)+'</p>'+'<p>Total New Record: '+(nCount)+'</p>'+'<p>Total Interest Percent Added: ('+Ext.util.Format.number((parseFloat(totalInterest).toFixed(8)*100),'0.0')+' %)</p></fieldset>';
            if(dCount>0){
                if(dCount==1){
                    sMessage += '<br /><b>Error list: '+ dCount +' Error Found</b><p>'+dNames+'</p>';
                } else {
                    sMessage += '<br /><b>Error list: '+ dCount +' Errors Found</b><p>'+dNames+'</p>';
                }
            }
            // Ext.Msg.show({
            //     iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
            //     title:'Upload Completed',
            //     width: 450,
            //     padding:20,
            //     msg: sMessage,
            //     buttons: Ext.Msg.OK
            // });
        } else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
                title:'Invalid Format',
                msg: "<p>Invalid File Format. Can't Read from this file source <br/>- Please  download File Format on Import DO</p>",
                buttons: Ext.Msg.OK
            });

        }
        return {
            record: StoreRecord,
            changedRowIndexes: changedRowIndexes,
            rowIndexes: rowIndexes,
            sMessage: sMessage
        }
    },
    onExportDO: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseOwnersGridViewController onTransferMultipleToMultipleGroup.')
        
        var me = this.getView();
        var main = this;
        var grid = me.getView();
        var store = grid.getStore();
        var selected = grid.getSelectionModel().getSelection();
        var listStore = grid.getStore(); 
        var listGrid = me.getView();
        var leaseId = Ext.getCmp('MineralValuationManageLeaseId').selectedId;
        var leaseName = Ext.getCmp('MineralValuationManageLeaseId').selectedName;
        var selectedName = '';
        var form = button.up();
        var enabledExport = true;

        Ext.each(store.getNewRecords(), function(el, index, items){
                for(var i=0; i<selected.length; i++){
                    if(selected[i].data.ownerId == el.data.ownerId){
                        enabledExport = false;
                    }
                }
        });

        if(enabledExport){
            if(selected.length > 0){
                var selectedModel = new Ext.data.Model({
                    fields:[
                        {name: 'Owner_Id', type: "Int", defaultValue: 0},
                        {name: 'Owner_Name', type: "string", defaultValue: ''},
                        {name: 'Owner_Name2', type: "string", defaultValue: ''},
                        {name: 'Owner_Mailing1', type: "string", defaultValue: ''},
                        {name: 'Owner_Mailing2', type: "string", defaultValue: ''},
                        {name: 'Owner_City', type: "string", defaultValue: ''},
                        {name: 'Owner_ST', type: "string", defaultValue: ''},
                        {name: 'Owner_Zip', type: "string", defaultValue: ''},
                        {name: 'Interest_Type', type: "string", defaultValue: ''},
                        {name: 'Interest_Percent', type: "string", defaultValue: ''},
                    ]
                });
                var selectedStore = new Ext.data.Store({
                    model: selectedModel,
                    autoSave: false,
                    autoLoad: false,
                    proxy: {
                        type: "localstorage"         
                    }
                });
                
                for(var i=0; i<selected.length; i++){
                    var nRecord = new selectedStore.model;
                        nRecord.set('Owner_Id', selected[i].data.ownerId);
                        nRecord.set('Owner_Name', selected[i].data.Owner_Name);
                        nRecord.set('Owner_Name2', selected[i].data.Owner_Name2);
                        nRecord.set('Owner_Mailing1', selected[i].data.Owner_Mailing1);
                        nRecord.set('Owner_Mailing2', selected[i].data.Owner_Mailing2);
                        nRecord.set('Owner_City', selected[i].data.Owner_City);
                        nRecord.set('Owner_ST', selected[i].data.Owner_ST);
                        nRecord.set('Owner_Zip', selected[i].data.Owner_Zip);
                        nRecord.set('Interest_Type', selected[i].data.interestType);
                        nRecord.set('Interest_Percent', selected[i].data.interestPercent);
                        selectedStore.add(nRecord);
                        selectedName = selected[i].leaseName;
                }
                var jsonData = Ext.encode(Ext.pluck(selectedStore.data.items, 'data'));
                var sendJasonData = '{"data":'+jsonData+'}';
                var filename = 'DO_'+leaseId+'_'+leaseName+'_'+Ext.Date.format(new Date(), 'Y-m-d');
                if(selected.length<=1000){
                    Ext.MessageBox.show({
                        title:'Export Division Order',
                        msg: '<p style="line-height:18px; margin:0px">Downloaded File Info <br />'+
                            'File name: '+filename+'<br />'+
                            'Lease name: '+leaseName+' <br />'+
                            'Number of records exported: '+ selected.length +'<br /></p>',
                        buttons: Ext.Msg.OKCANCEL,
                        icon: Ext.Msg.INFO,
                        fn: function(btn){                           
                            if (btn == "ok"){
                                form.submit({
                                    url: '/api/excel/download',
                                    standardSubmit: true,
                                    method: 'POST', 
                                    params: {
                                        data: sendJasonData,
                                        fileName: filename,
                                        exportType: 'ExportDO',
                                    },
                                    success: function(fm, action) {
                                        //console.log(fm)
                                    },
                                    failure: function(form, response) {
                                        Ext.MessageBox.show({
                                            title:'Download Failed',
                                            msg: 'Cannot process your request to your browsers version.',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.Msg.ERROR,
                                        });
                                    }
                                });
                            }
                        }                
                    });
                } else {
                    Ext.MessageBox.show({
                        title:'Internal Error',
                        msg: "<p style='text-align:center;padding:0;margin:0;font-weight:bold;font-size:13px;'>Unable to Process Request.</p></br><p style='color:red;text-align:center;padding:0;margin:0;font-weight:normal;font-size:13px;'> Number of records exceeds limit to</br>1000 maximum records</p>",
                        buttons: Ext.Msg.OK,
                        iconCls: Ext.Msg.INFO
                    });
                }
            } else {
                Ext.MessageBox.show({
                    title:'Alert',
                    msg: "<p style='text-align:center;padding:0;margin:0;font-weight:bold;font-size:13px;'>Please select/check record for Export DO",
                    buttons: Ext.Msg.OK,
                    iconCls: MineralPro.config.Runtime.listSyncMsgErrorIcon
                });
            }
        } else {
            Ext.MessageBox.show({
                title:'Alert',
                msg: "<p style='text-align:center;padding:0;margin:0;font-weight:bold;font-size:13px;'>Please save data before Export DO",
                buttons: Ext.Msg.OK,
                iconCls: MineralPro.config.Runtime.listSyncMsgErrorIcon
            });
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseOwnersGridViewController onTransferMultipleToMultipleGroup.')
    },
    onDeleteSelected: function(button){
        var me = this;
        var grid = me.getView();
        var store = grid.getStore();
        var selected = grid.getSelectionModel().getSelection();
        if(selected.length>0){  
            Ext.each(selected, function(el, index, items){
                el.set('rowDeleteFlag','D')
            })
        }else{
            Ext.MessageBox.show({  
                iconCls: Minerals.config.Runtime.multipleEditAlertIcon,
                title: Minerals.config.Runtime.multipleEditAlertTitle,    
                msg: 'Please select/check record to be delete.',                
                buttons: Ext.Msg.OK
            });
        }
    },
    onOpenSelectedTabView: function (record, rowIndex) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseOwnersGridViewController onOpenSelectedTabView.')
        var me = this;
		var centerView = me.getView().up('centerView');
		var westView = centerView.up().down('westView');
        var favoriteTree = westView.down('#favoriteTreeId');
        var menuTree = westView.down('#menuTreeId');
        var dataTree=true;
        var index = 0;
        
        favoriteTree.getStore().each(function(c, fn){
            if(c.data.functionName == 'Manage Owner'){
                index = fn;
                dataTree=false;
            }
        })            
        menuTree.getStore().each(function(c, fn){
            if(c.data.functionName == 'Manage Owner'){
                index = fn;
                dataTree=true;
            }
        })
        
        if(dataTree == true){
            menuTree.getSelectionModel().select(index);
            setTimeout(function () {
                var tabPanel = centerView.down('ProperRecordsManageOwner');
                var cols = centerView.down('#ProperRecordsManageOwnersId').down('grid').columns;

                tabPanel.setActiveTab(0);
                Ext.each(cols, function (col) {
                    if(col.dataIndex == 'name'){
                        col.filterElement.setValue(record.data.ownerName.trim());
                        col.filterElement.search();
                    }
                });
            },700 )
        }else{
            favoriteTree.getSelectionModel().select(index);
            setTimeout(function () {
                var tabPanel = centerView.down('ProperRecordsManageOwner');
                var cols = centerView.down('#ProperRecordsManageOwnersId').down('grid').columns;

                tabPanel.setActiveTab(0);
                Ext.each(cols, function (col) {
                    if(col.dataIndex == 'name'){
                        col.filterElement.setValue(record.data.ownerName.trim());
                    }
                });
            },700 )
        }
		
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseOwnersGridViewController onOpenSelectedTabView.')
    },
    onOpenAddListForm: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseOwnersGridViewController onOpenAddListForm.')
        var me = this;
        var mainView = me.getView();
        var record = Ext.create(mainView.mainListModel);      
        var addView = Ext.widget(mainView.addFormAliasName);
        var comboStore = Ext.getStore('Minerals.store.ProperRecords.OwnerComboStore');
        comboStore.reload();
        record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
        addView.down('form').loadRecord(record);
        addView.show()
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseOwnersGridViewController onOpenAddListForm.')
    },
});
