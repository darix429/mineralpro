Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersEditFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersAddFormViewController'],
    controller: 'LeaseOwnersAddFormViewController',
    alias: 'widget.LeaseOwnersEditFormView',
    
    
    title: 'Edit Owner',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#ownerNameId',
    layout: 'fit',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'MineralValuationManageLeaseId', //id of the main panel
    listGridStore: 'Minerals.sub.MineralValuationManageLease.store.LeaseOwnerStore', //store for Lease Owner grid
    listGridAlias: 'LeaseOwnersGridView', 
    bodyPadding: 5,
    
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'form',
            defaults:{ 
                maskRe: /[^'\^]/,
                labelWidth: 120,
                width : 400
            },
            layout: 'vbox',
            bodyPadding: 5,
            items:[ {
                    fieldLabel: 'Lease Name',
                    xtype: 'displayfield',
                    labelAlign: 'right',
                    msgTarget: 'side',
                    margin: '0 0 10',
                    itemId: 'leaseName',
                },  {       
                    xtype: 'hidden',     
                    name: 'leaseId',   
                    queryMode:'local',
                    itemId:'leaseId' 
                },  {
                    fieldLabel: 'Owner Name',
                    selectOnFocus: true,
                    tabIndex: 1,
                    name : 'ownerName',
                    xtype:'textfield',
                    itemId:'ownerNameId',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    disabled:true,
                    allowBlank: false,
                    queryMode:'local',
                },  {       
                     xtype: 'hidden',     
                     name: 'ownerId',   
                     queryMode:'local',
                     itemId:'ownerNoItemId' 
                },  {
                    fieldLabel: 'Interest Type',
                    selectOnFocus: true,
                    tabIndex: 2,
                    name : 'interestTypeCd',
                    xtype:'combo',
                    itemId:'interestTypeCdItemId',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    displayField: 'interestType',
                    valueField: 'interestTypeCd',
                    typeAhead: true,
                    forceSelection: true,   
                    store: 'Minerals.sub.MineralValuationManageLease.store.LeaseInterestTypeStore',
                    allowBlank: false,
                    queryMode:'local',
                    listeners:{
                        change:function(record){
                            me.down('#interestTypeItemId').setValue(me.down('#interestTypeCdItemId').getRawValue());
                        },
                    }
                },  {       
                    xtype: 'hidden',     
                    name: 'interestType',   
                    queryMode:'local',
                    itemId:'interestTypeItemId' 
                },  {
                    fieldLabel: 'Interest Percent',
                    xtype: 'textfield',
                    maskRe:/[0-9.]/,
					selectOnFocus: true,
                    tabIndex: 3,
                    name : 'interestPercent',
                    itemId:'interestPercentItemId',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    allowBlank: false,
                    msgTarget: 'side',
                    enableKeyEvents: true,
                    maxLength:10,
                    validator: function(v)  {
                        if (isNaN(v*1)) {
                            return "Must be a decimal or whole number";
                        }
                        return true;
                    },
                    listeners : {
                        keyup: function(data, e, eOpts){
                            var mainView = Ext.getCmp(me.mainPanelId);
                            var listGrid = mainView.down(me.listGridAlias);
                            var totalInterest = 0;
                            var interestTypeCd = me.down('#interestTypeCdItemId').getValue();
                            var royaltyInterestPercent = 0;
                            var workingInterestPercent = 0;
                            var royaltyInterestValues = 0;
                            var workingInterestValues = 0;
                            var ScanStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseOwnerStore');
                            var allRecords = ScanStore.getData().getSource() || ScanStore.getData();
                            allRecords.each( 
                                function(owner){
                                    if(owner.data.rowDeleteFlag == '' && owner.data.interestTypeCd !==0){
                                        totalInterest = parseFloat(totalInterest)+parseFloat(owner.data.interestPercent);
                                        if(owner.data.interestTypeCd !==4){
                                            royaltyInterestPercent = parseFloat(royaltyInterestPercent)+parseFloat(owner.data.interestPercent);
                                            royaltyInterestValues = parseFloat(royaltyInterestValues)+parseFloat(owner.data.OwnerValues);
                                        } else {
                                            workingInterestPercent = parseFloat(workingInterestPercent)+parseFloat(owner.data.interestPercent);
                                            workingInterestValues = parseFloat(workingInterestValues)+parseFloat(owner.data.OwnerValues);
                                        }
                                    }
                                }
                            );
                            
                            var interestPercent = listGrid.selection.get('interestPercent');
                            var ownerPercentRaw = me.down('#interestPercentItemId').getRawValue();
                            var ownerPercent = Ext.util.Format.number(ownerPercentRaw,'0.00000000');         
                            var totalInterestPercent = ((totalInterest-interestPercent) + (ownerPercent*1));
                            var ownerValue = 0;
                            if(interestTypeCd == 4){
                                ownerValue = ((workingInterestValues/workingInterestPercent)*ownerPercent);
                            }
                            if(interestTypeCd !== 4 && interestTypeCd !== 0){
                                ownerValue = ((royaltyInterestValues/royaltyInterestPercent)*ownerPercent);
                            }
                                if(ownerPercent!=0 && !isNaN(ownerPercent*1)){
                                    me.down('#ownerValueItemId').show();
                                    me.down('#ownerValueItemId').setValue(Ext.util.Format.number(ownerValue, '$000,000'));
                                }else{
                                    me.down('#ownerValueItemId').setValue(Ext.util.Format.number(0, '$000,000'));
                                    me.down('#ownerValueItemId').hide();
                                    
                                }
                            me.down('#totalInterestPctItemId').setValue(parseFloat(totalInterestPercent).toFixed(8));
                            
                        },
                        afterrender: function(){
                            var ownerPercent = me.down('#interestPercentItemId').getRawValue();
                            var fixValue = parseFloat(ownerPercent).toFixed(8);
                            me.down('#interestPercentItemId').setValue(fixValue)
                        }
                    }
                },
                {
                    fieldLabel: 'Total Interest (%)',
                    name: 'totalInterestPercent',
                    xtype: 'textfield',
                    labelAlign: 'right',
                    msgTarget: 'side',
                    margin: '0 0 10',
                    readOnly: true,
                    queryMode:'local',
                    itemId: 'totalInterestPctItemId',
                    listeners : {
                        change: function(record) {
                            if((record.getRawValue()*1)==1){
                                 this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_good.png) no-repeat right;');
                            }else if((record.getRawValue()*1)>1){
                                 this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_lower.png) no-repeat right;');
                            }else{
                                 this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_upper.png) no-repeat right;');
                            }
                        }
                    }
                },  {
                    fieldLabel: 'Owner Value',
                    name: 'OwnerValues',   
                    xtype: 'textfield',
                    readOnly: true,
                    margin: '0 0 0',
                    hidden:true,
                    itemId:'ownerValueItemId',
                    labelAlign: 'right',
                    fieldStyle:'background-color:#d8d8d8',
                    listeners : {
                        afterrender: function(){
                            var mainView = Ext.getCmp(me.mainPanelId);
                            var listGrid = mainView.down(me.listGridAlias);
                            var ownerPercent = me.down('#interestPercentItemId').getRawValue();
                            var totalInterest = 0;
                            var interestTypeCd = me.down('#interestTypeCdItemId').getValue();
                            var royaltyInterestPercent = 0;
                            var workingInterestPercent = 0;
                            var royaltyInterestValues = 0;
                            var workingInterestValues = 0;
                            var ScanStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseOwnerStore');
                            var allRecords = ScanStore.getData().getSource() || ScanStore.getData();
                            allRecords.each( 
                                function(owner){
                                    if(owner.data.rowDeleteFlag == '' && owner.data.interestTypeCd !==0){
                                        totalInterest = parseFloat(totalInterest)+parseFloat(owner.data.interestPercent);
                                        if(owner.data.interestTypeCd !==4){
                                            royaltyInterestPercent = parseFloat(royaltyInterestPercent)+parseFloat(owner.data.interestPercent);
                                            royaltyInterestValues = parseFloat(royaltyInterestValues)+parseFloat(owner.data.OwnerValues);
                                        } else {
                                            workingInterestPercent = parseFloat(workingInterestPercent)+parseFloat(owner.data.interestPercent);
                                            workingInterestValues = parseFloat(workingInterestValues)+parseFloat(owner.data.OwnerValues);
                                        }
                                    }
                                }
                            );
                            var ownerPercentRaw = me.down('#interestPercentItemId').getRawValue();
                            var ownerPercent = Ext.util.Format.number(ownerPercentRaw,'0.00000000');
                            var ownerValue = 0;
                            if(interestTypeCd == 4){
                                ownerValue = ((workingInterestValues/workingInterestPercent)*ownerPercent);
                            }
                            if(interestTypeCd !== 4 && interestTypeCd !== 0){
                                ownerValue = ((royaltyInterestValues/royaltyInterestPercent)*ownerPercent);
                            }
                            if(ownerPercent!=0){
                                me.down('#ownerValueItemId').show();
                                me.down('#ownerValueItemId').setValue(Ext.util.Format.number(ownerValue, '$000,000'));
                            }else{
                                me.down('#ownerValueItemId').hide();
                            }
                        }
                    }
                }
            ]
            
        }];

        this.listeners = {
            afterrender: function(){
                var me = this;          
                var mainView = Ext.getCmp(me.mainPanelId);
                var LeaseName = mainView.selectedName;
                var leaseNameId = mainView.selectedId;
                var listGrid = mainView.down(me.listGridAlias);
                me.down('#leaseName').setValue(LeaseName);
                me.down('#leaseId').setValue(leaseNameId);
                var totalInterestPercent = 0;
                var ScanStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseOwnerStore');
                var allRecords = ScanStore.getData().getSource() || ScanStore.getData();
                allRecords.each( 
                    function(owner){
                        if(owner.data.rowDeleteFlag == '' && owner.data.interestTypeCd !==0){
                            totalInterestPercent = parseFloat(totalInterestPercent)+parseFloat(owner.data.interestPercent);
                        }
                    }
                );
                me.down('#totalInterestPctItemId').setValue(parseFloat(totalInterestPercent).toFixed(8));
            },
        }
        this.callParent(arguments);
    },

    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});