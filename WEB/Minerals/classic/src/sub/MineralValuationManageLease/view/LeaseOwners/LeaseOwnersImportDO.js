Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersImportDO', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    alias: 'widget.LeaseOwnersImportDO',
    title: ' Import Division Order',
    defaultFocus: '#fileUploadItemId',
    controller: 'LeaseOwnersImportDOController',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'MineralValuationManageLeaseId', //id of the main panel
    listGridStore: 'Minerals.sub.MineralValuationManageLease.store.LeaseOwnerStore', 
    listGridAlias: 'LeaseOwnersGridView', //alias name for note gridview

    initComponent: function () {
        var me = this;
        me.items = [{
            xtype: 'form',
            padding: 20,
            width:800,
            items: [{
                    items: [{
                        xtype: 'fieldset',
                        title:'Import Data To Grid',
                        html: [
                            '<h3>File Supported</h3>',
                            '<p>Only Excel Documents with extension name (xlsx) and (xls) are allowed to upload on this section.<br/>',
                            '- You can download and use Formatted file below. For safety procedure and avoid data conflicts',
                        ],
                        items:[{
                            xtype: 'filefield',
                            hideLabel: true,
                            width:730,
                            itemId:'fileUploadItemId',
                            reference: 'fileUploadFormat',
                            selectOnFocus: true,
                            msgTarget: 'side',
                            emptyText:'Excel Document required',
                            buttonConfig: {
                                iconCls: 'search',
                                text: 'Browse File'
                            },  
                            validator: function(v) {
                                if(isNaN(v)){
                                    var file = v.split('.');
                                    if (file[file.length-1]=='xlsx' || file[file.length-1]=='xls') {
                                        return true;
                                    } else if (file[file.length-1]=='xml'||file[file.length-1]=='xlt'||file[file.length-1]=='xlm'){
                                        return "<li>This File format need to convert <br>into 'xls' or 'xlsx' Format</li>";
                                    }
                                    return "<li>Invalid File Format.</li>";
                                } else {
                                    return "<li>No input File specified</li>";
                                }
                            },
                            listeners: {
                                change: function(fld, value) {
                                    this.setRawValue(value.replace(/C:\\fakepath\\/g, ''));
                                }
                            }
                        }, {
                            xtype: 'button',
                            width:730,
                            text: 'Upload File',
                            handler: 'uploadFile'
                        }]
                    }]
                }, {
                    items: [{
                        xtype: 'fieldset',
                        title:'Download File Format',
                        html: [
                            '<h3>File Format</h3>',
                            '<p>This File structure is how information is stored (encoded) in Lease Owner Grid<br/> ',
                            '- Field names are based on lease owner grid.<br/> - You can select Owner from the grid to be extracted with the Formatted File.</p>',
                            '<p style="color:red">Note: Must be less than 192 Owners selected.</p>',
                        ],
                        items:[{
                            xtype: 'button',
                            width:730,
                            text: 'Download File',
                            handler: 'downloadExcelXml',
                        }]
                    }]
                } 
            ]
    }];


        me.callParent(arguments);
    },
});