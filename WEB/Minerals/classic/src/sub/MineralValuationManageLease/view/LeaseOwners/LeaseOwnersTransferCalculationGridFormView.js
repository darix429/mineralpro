Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersTransferCalculationGridFormView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.LeaseOwnersTransferCalculationGridFormView',
    defaultFocus: 'ownerNameItemId',
    requires: ['Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersTransferFormViewController'],
    controller: 'LeaseOwnersTransferFormViewController',
  //  saveAction: 'onTransferMultipleGroupToGrid',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'ProperRecordsManageOwnerLeaseHoldingsId', //id of the main panel
    listGridStore: 'Minerals.sub.MineralValuationManageLease.store.LeaseOwnerStore', 
    listGridAlias: 'LeaseOwnersGridView', //alias name for note gridview
    store: 'Minerals.sub.MineralValuationManageLease.store.LeaseOwnersTransferCalculationStore',
    
    loadMask: true,
    height: 300,
    layout: 'fit',

    columns: [{
        header: 'Owner Name',
        dataIndex: 'ownerName',
        flex: 2,
        maxWidth: 240,
        itemid: 'OwnerNameItemId',
        //sortable:false,
       // filterElement: new Ext.form.TextField(),
    }, {
        header: 'Owner ID',
        dataIndex: 'ownerId',
        flex: 0,
        //sortable:false,
       // filterElement: new Ext.form.TextField(),
    }, {
        header: 'Split Among Receivers',
        dataIndex: 'percentReceive',
        flex: 1,
      //  sortable:false,
      //  filterElement: new Ext.form.TextField(),
        editor: {
                allowBlank: false,
                xtype: 'textfield',
                maskRe:/[0-9.]/,
                minValue: 0,
                maxValue: 100
            },
        renderer: function(val, meta) {
            return  Ext.util.Format.number(val, '000.00%');
        },    
    },{
        header: 'Owner Receive Percent',
        dataIndex: 'ownerReceivePct',
        flex: 1,
       // sortable:false,
      //  filterElement: new Ext.form.TextField(),
    },{
        header: 'Value to Receive',
        dataIndex: 'OwnerReceiveValue',
        flex: 1,
        align: 'right',
        minWidth: 120,
        maxWidth: 150,
      //  sortable:false,
        renderer: function(val, meta, record) {
            return Ext.util.Format.number(val, '$000,000');
        },
    }],

    listeners: {
         beforeedit: 'onBeforeEdit',
         validateedit: 'onValidateEditButtom',
    },
    initComponent: function () {
        var me = this;
        me.plugins = [Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2
            })
        ]
        // me.selModel= Ext.create('Ext.selection.CheckboxModel',{
        //     checkOnly: true,
        //     mode: 'SINGLE',
        // })
        me.callParent(arguments);
    },
    viewConfig: {
        getRowClass: function(record) {
            if (record.get('edited') == 'Y') {
                return 'edited-record';
            } 
        },
    }
});