Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersAddFormViewController'],
    controller: 'LeaseOwnersAddFormViewController',
    alias: 'widget.LeaseOwnersAddFormView',

    title: 'Add New Owner',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#ownerNoItemId',
    layout: 'fit',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'MineralValuationManageLeaseId', //id of the main panel
    listGridStore: 'Minerals.sub.MineralValuationManageLease.store.LeaseOwnerStore', //store for Lease Owner grid
    listGridAlias: 'LeaseOwnersGridView',
    width: 450, 
    bodyPadding: 5,
    
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'form',
            defaults:{ 
                maskRe: /[^'\^]/,
                labelWidth: 130,
                padding: 5,
                width : 410
            },
            layout: 'vbox',
            bodyPadding: 10,
            items:[{
                    fieldLabel: 'Lease Name',
                    xtype: 'displayfield',
                    labelAlign: 'right',
                    msgTarget: 'side',
                    margin: '0 0 10',
                    itemId: 'leaseName',
                },{       
                    xtype: 'hidden',     
                    name: 'leaseId',   
                    queryMode:'local',
                    itemId:'leaseId' 
                },
                {
                    fieldLabel: 'Owner Name',
                    selectOnFocus: true,
                    tabIndex: 1,
                    name : 'ownerId',
                    xtype:'combo',
                    itemId:'ownerNoItemId',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    displayField: 'name1',
                    valueField: 'ownerId',   
                    store: 'Minerals.store.ProperRecords.OwnerComboStore',
                    typeAhead: true,
                    allowBlank: false,
                    queryMode:'local',
                    listeners:{
                        change:function(record){
                                me.down('#ownerNameId').setValue(me.down('#ownerNoItemId').getRawValue());
                               me.down('#addrLine1').setValue("");
                               me.down('#addrLine2').setValue("");
                               me.down('#addrLine3').setValue("");
                               me.down('#city').setValue("");
                               me.down('#state').setValue("");
                               me.down('#zipcode').setValue("");
                               if(me.down('#ownerNoItemId').value == null){
                                   me.down('#ownerIdItemId').clearValue();
                                }
                               

                        },

                        select: function (record) {

                               var id = '';
                               var ad1 = '';
                               var ad2 = '';
                               var ad3 = '';
                               var city = '';
                               var state = '';
                               var zip = '';

                               id = record.valueCollection.items[0].data.valueField;
                               ad1 = record.valueCollection.items[0].data.addrLine1;
                               ad2 = record.valueCollection.items[0].data.addrLine2;
                               ad3 = record.valueCollection.items[0].data.addrLine3;
                               city = record.valueCollection.items[0].data.city;
                               state = record.valueCollection.items[0].data.state;
                               zip = record.valueCollection.items[0].data.zipcode;
                          
                               me.down('#ownerIdItemId').setValue(id);
                               me.down('#addrLine1').setValue(ad1);
                               me.down('#addrLine2').setValue(ad2);
                               me.down('#addrLine3').setValue(ad3);
                               me.down('#city').setValue(city);
                               me.down('#state').setValue(state);
                               me.down('#zipcode').setValue(zip);
                   
                        }
                    } 
                },{       
                    xtype: 'hidden',     
                    name: 'ownerName',   
                    queryMode:'local',
                    itemId:'ownerNameId' 
                },
                 {
                    fieldLabel: 'ID',
                    selectOnFocus: false,
                    tabIndex: 2,
                    maskRe:/[0-9]/,
                    name : 'ownerN',
                    xtype:'combo',
                    itemId:'ownerIdItemId',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    displayField: 'valueField',
                    valueField: 'displayField',   
                    store: 'Minerals.store.ProperRecords.OwnerComboStore',
                    typeAhead: true,
                    allowBlank: false,
                    queryMode:'local',
                    listeners: {
                        change:function(record){
                                me.down('#ownerNameId').setValue(me.down('#ownerNoItemId').getRawValue());
                                if(me.down('#ownerIdItemId').value == null){
                                   me.down('#ownerNoItemId').clearValue();
                                }
    
                               me.down('#addrLine1').setValue("");
                               me.down('#addrLine2').setValue("");
                               me.down('#addrLine3').setValue("");
                               me.down('#city').setValue("");
                               me.down('#state').setValue("");
                               me.down('#zipcode').setValue("");
                        },
                            select: function (record) {
                                var name = ''
                                var ad1 = '';
                               var ad2 = '';
                               var ad3 = '';
                               var city = '';
                               var state = '';
                               var zip = '';

                               name = record.valueCollection.items[0].data.displayField;
                               ad1 = record.valueCollection.items[0].data.addrLine1;
                               ad2 = record.valueCollection.items[0].data.addrLine2;
                               ad3 = record.valueCollection.items[0].data.addrLine3;
                               city = record.valueCollection.items[0].data.city;
                               state = record.valueCollection.items[0].data.state;
                               zip = record.valueCollection.items[0].data.zipcode;
   
                               me.down('#ownerNoItemId').setValue(name);
                               me.down('#addrLine1').setValue(ad1);
                               me.down('#addrLine2').setValue(ad2);
                               me.down('#addrLine3').setValue(ad3);
                               me.down('#city').setValue(city);
                               me.down('#state').setValue(state);
                               me.down('#zipcode').setValue(zip);
                            }
                        },
                },
               
                {
                        fieldLabel: 'In Care of',
                        itemId: 'addrLine1',
                        tabIndex: 5,
                        name: 'addrLine1',
                        disabled:true,
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        readOnly: true,
                        msgTarget: 'side',
                        
                    },{
                        fieldLabel: 'Address Line 2',
                        itemId: 'addrLine2',
                        tabIndex: 6,
                        name: 'addrLine2',
                        disabled:true,
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        readOnly: true,
                       
                    },{
                        fieldLabel: 'Address Line 3',
                        itemId: 'addrLine3',
                        tabIndex: 7,
                        name: 'addrLine3',
                        disabled:true,
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        readOnly: true,
                    },{
                        fieldLabel: 'City',
                        tabIndex: 8,
                        itemId: 'city',
                        name: 'city',
                        disabled:true,
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        readOnly: true,
                     
                    },{
                        fieldLabel: 'State',
                        selectOnFocus: true,
                        tabIndex: 9,
                        disabled:true,
                        itemId: 'state',
                        name : 'state',
                        xtype:'textfield',
                        readOnly: true,
                        margin: '0 0 10',
                        labelAlign: 'right',
                    },{
                        fieldLabel: 'Zip Code',
                        tabIndex: 10,
                        itemId: 'zipcode',
                        disabled:true,
                        name: 'zipcode',
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        msgTarget: 'side',
                    },{
                    fieldLabel: 'Interest Type',
                    selectOnFocus: true,
                    tabIndex: 3,
                    name : 'interestTypeCd',
                    xtype:'combo',
                    itemId:'interestTypeCdItemId',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    displayField: 'interestType',
                    valueField: 'interestTypeCd',   
                    store: 'Minerals.sub.MineralValuationManageLease.store.LeaseInterestTypeStore',
                    allowBlank: false,
                    queryMode:'local',
                    listeners:{
                        select: 'onAddCheckLeaseOwnerExisting',
                        change:function(record){
                            me.down('#interestTypeItemId').setValue(me.down('#interestTypeCdItemId').getRawValue());
                        },
                    }
                },{       
                    xtype: 'hidden',     
                    name: 'interestType',   
                    queryMode:'local',
                    itemId:'interestTypeItemId' 
                },{
                    fieldLabel: 'Interest Percent',
                    xtype: 'textfield',
                    maskRe:/[0-9.]/,
                    selectOnFocus: true,
                    tabIndex: 4,
                    name : 'interestPercent',
                    itemId:'interestPercentItemId',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    allowBlank: false,
                    msgTarget: 'side',
                    maxLength:10,
                    enableKeyEvents: true,
                    validator: function(v) {
                            if (isNaN(v*1)) {
                                return "Must be a decimal or whole number";
                            }
                            return true;
                    },
                    listeners : {
                        keyup: function(data, e, eOpts){
                            var mainView = Ext.getCmp(me.mainPanelId);
                            var listGrid = mainView.down(me.listGridAlias);
                            var totalInterest = 0;
                            var interestTypeCd = me.down('#interestTypeCdItemId').getValue();
                            var royaltyInterestPercent = 0;
                            var workingInterestPercent = 0;
                            var royaltyInterestValues = 0;
                            var workingInterestValues = 0;
                            var ScanStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseOwnerStore');
                            var allRecords = ScanStore.getData().getSource() || ScanStore.getData();
                            allRecords.each( 
                                function(owner){
                                    if(owner.data.rowDeleteFlag == '' && owner.data.interestTypeCd !==0){
                                        totalInterest = parseFloat(totalInterest)+parseFloat(owner.data.interestPercent);
                                        if(owner.data.interestTypeCd !==4){
                                            royaltyInterestPercent = parseFloat(royaltyInterestPercent)+parseFloat(owner.data.interestPercent);
                                            royaltyInterestValues = parseFloat(royaltyInterestValues)+parseFloat(owner.data.OwnerValues);
                                        } else {
                                            workingInterestPercent = parseFloat(workingInterestPercent)+parseFloat(owner.data.interestPercent);
                                            workingInterestValues = parseFloat(workingInterestValues)+parseFloat(owner.data.OwnerValues);
                                        }
                                    }
                                }
                            );
                            var ownerPercentRaw = me.down('#interestPercentItemId').getRawValue();
                            var ownerPercent = Ext.util.Format.number(ownerPercentRaw,'0.00000000');            
                            var totalInterestPercent = ((totalInterest*1) + (ownerPercent*1));
                            var ownerValue = 0;
                            if(interestTypeCd == 4 && workingInterestValues){
                                ownerValue = ((workingInterestValues/workingInterestPercent)*ownerPercent);
                            }
                            if(interestTypeCd !== 4 && interestTypeCd !== 0 && royaltyInterestValues){
                                ownerValue = ((royaltyInterestValues/royaltyInterestPercent)*ownerPercent);
                            }
                                if(ownerPercent!==0){
                                    me.down('#ownerValueItemId').show();
                                    me.down('#ownerValueItemId').setValue(Ext.util.Format.number(ownerValue, '$000,000'));
                                }else{
                                    me.down('#ownerValueItemId').setValue(Ext.util.Format.number(0, '$000,000'));
                                    me.down('#ownerValueItemId').hide();
                                }
                            me.down('#totalInterestPctItemId').setValue(parseFloat(totalInterestPercent).toFixed(8));
                        }
                    }
                },
                {
                    fieldLabel: 'Total Interest (%)',
                    name: 'totalInterestPercent',
                    xtype: 'textfield',
                    labelAlign: 'right',
                    msgTarget: 'side',
                    margin: '0 0 10',
                    queryMode:'local',
                    itemId: 'totalInterestPctItemId',
                    readOnly: true,
                    listeners : {
                        change: function(record) {
                            this.setValue(Ext.util.Format.number(record.value,'0.00000000'))
                            if((record.getRawValue()*1)==1){
                                 this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_good.png) no-repeat right;');
                            }else if((record.getRawValue()*1)>1){
                                 this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_lower.png) no-repeat right;');
                            }else{
                                 this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_upper.png) no-repeat right;');
                            }
                        }
                    }
                },{
                    fieldLabel: 'Owner Value',
                    name: 'OwnerValues',   
                    xtype: 'textfield',
                    readOnly: true,
                    margin: '0 0 0',
                    hidden:true,
                    itemId:'ownerValueItemId',
                    labelAlign: 'right',
                    fieldStyle:'background-color:#d8d8d8',
                    listeners : {
                        afterrender: function(){
                            me.down('#ownerValueItemId').hide();
                        }
                    }
                }
            ]
            
        }];

        this.listeners = {
            afterrender: function(){
                var me = this;          
                var mainView = Ext.getCmp(me.mainPanelId);
                var listGrid = mainView.down(me.listGridAlias);
                var LeaseName = mainView.selectedName;
                var leaseNameId = mainView.selectedId;
                me.down('#leaseId').setValue(leaseNameId);
                me.down('#leaseName').setValue(LeaseName);
                var totalInterestPercent = 0;
                var ScanStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseOwnerStore');
                var allRecords = ScanStore.getData().getSource() || ScanStore.getData();
                allRecords.each( 
                    function(owner){
                        if(owner.data.rowDeleteFlag == '' && owner.data.interestTypeCd !==0){
                            totalInterestPercent = parseFloat(totalInterestPercent)+parseFloat(owner.data.interestPercent);
                        }
                    }
                );
                me.down('#totalInterestPctItemId').setValue(parseFloat(totalInterestPercent).toFixed(8));
            },
        }

        this.callParent(arguments);
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
        MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});