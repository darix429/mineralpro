
Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersDetailView', {
    extend: 'MineralPro.config.view.BaseDetailView',
    alias: 'widget.LeaseOwnersDetailView',
    
    title: 'Lease Owner Details',
    defaults: {
        width: 275,
    },   
    items: [{
        xtype:'textfield',
        fieldLabel:'Birth Date',
        name: 'birthDt',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'Address Line 1',
        name: 'addrLine1',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'Address Line 2',
        name: 'addrLine2',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'Address Line 3',
        name: 'addrLine3',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'City',
        name: 'city',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'State',
        name: 'stateName',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'ZIP Code',
        name: 'zipcode',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'Country',
        name: 'countryName',
        readOnly: true 
    },{
        xtype : 'menuseparator',
        width : '100%',
        padding: false
    },{
        xtype:'textfield',
        fieldLabel:'Phone Number', 
        name: 'phoneNum',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'Fax Number',
        name:  'faxNum',
        readOnly: true          
    },{
        xtype:'textfield',
        fieldLabel:'Email Address',
        name:  'emailAddress',
        readOnly: true          
    }]
});