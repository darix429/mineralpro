Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersTransferBaseFormView' ,{
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    alias : 'widget.LeaseOwnersTransferBaseFormView',
   // layout:'fit',
    title: 'Transfer Ownership  ',
    defaultFocus: 'OwnerNameItemId',
    id: 'LeaseOwnersTransferBaseFormViewId',
    requires: ['Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersTransferFormViewController'],
    controller: 'LeaseOwnersTransferFormViewController',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'MineralsLeaseOwnersId', //id of the main panel
    listGridStore: 'Minerals.sub.MineralValuationManageLease.store.LeaseOwnerStore',
    listGridAlias: 'LeaseOwnersGridView', //alias name for note gridview
    closeAction: 'hide',
    width: 1200,
    height: 585, 
    bodyPadding: 1,
      
    items:[{
        xtype: 'panel',
         layout: {
            type: 'table',
            columns: 2,
        // tdAttrs: { style: 'padding: 10px; vertical-align: top;' }
        }, 
        items: [{
                title: 'Selected Owner',
                width: 1195,
                xtype: 'LeaseOwnersTransferSelectedLeaseFormView',
                colspan: 2
              //  region:'south'
            },{
                title: 'Select Receiving Owner',
                width: 378,
                xtype: 'LeaseOwnersTransferMultipleFormView',
               // region:'center'
            },{
                //title: 'center',
                width: 818,
                xtype: 'LeaseOwnersTransferCalculationGridFormView',
              //  region:'west',
            }] 

     }],
     listeners:{
            close: function(){
                var gridleaseStore = this.down('LeaseOwnersTransferSelectedLeaseFormView').getStore();
                var gridcalcStore = this.down('LeaseOwnersTransferCalculationGridFormView').getStore();
                var gridselection = this.down('LeaseOwnersTransferMultipleFormView');
                var gridselectionStore = gridselection.getStore();
                gridselectionStore.rejectChanges();
                gridselection.getSelectionModel().deselectAll();
                gridleaseStore.removeAll();
                gridcalcStore.removeAll();
                //gridleaseStore.rejectChanges();
                gridleaseStore.commitChanges();
                gridcalcStore.commitChanges();
                var cols = gridselection.columns;
                Ext.each(cols, function (col) {
                    if (col.filterElement)
                        col.filterElement.setValue('');
                });
            }
     },
     buttons: [
        {
            xtype: 'button',
            text: 'Transfer All',
            itemId: 'transferAllId',
            disabled: true,
            listeners: {
                click: 'onTransferAll'
            }
        },{
            xtype: 'button',
            text: 'Clear',
            itemId: 'clearId',
            listeners: {
               click: function(){
                    var view = this.up('window');
                    var gridcalcStore = view.down('LeaseOwnersTransferCalculationGridFormView').getStore();
                    var gridselection = view.down('LeaseOwnersTransferMultipleFormView');
                    view.down('#transferAllId').disable();
                    gridcalcStore.removeAll();
                    gridselection.getSelectionModel().deselectAll();
               }
            }
        },
        // {
        //     xtype: 'button',
        //     text: 'Save',
        //     itemId: 'saveId',
        //     disabled: true,
        //     listeners: {
        //         click: 'onTransferMultipleToMultipleGroupToGrid'
        //     }
        // },
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]    
});    