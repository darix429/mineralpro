var Base64 = (function() {
    // Private property
    var keyStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=.";

    // Private method for UTF-8 encoding

    function utf8Encode(string) {
        string = string.replace(/\r\n/g, "\n");
        var utftext = "";
        for (var n = 0; n < string.length; n++) {
            var c = string.charCodeAt(n);
            if (c < 128) {
                utftext += String.fromCharCode(c);
            } else if ((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            } else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }
        }
        return utftext;
    }

    // Public method for encoding
    return {
        encode: (typeof btoa == 'function') ? function(input) {
            return btoa(utf8Encode(input));
        } : function(input) {
            var output = "";
            var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
            var i = 0;
            input = utf8Encode(input);
            while (i < input.length) {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);
                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;
                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }
                output = output +
                    keyStr.charAt(enc1) + keyStr.charAt(enc2) +
                    keyStr.charAt(enc3) + keyStr.charAt(enc4);
            }
            return output;
        }
    };
})();
//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersImportDOController', {
    extend: 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.LeaseOwnersImportDOController', 

    getImportToGrid: function(data, input) {
        var me = this;                     
        var view = me.getView();
        var leaseId = Ext.getCmp(view.mainPanelId).selectedId;
        var leaseName = Ext.getCmp(view.mainPanelId).selectedName;
        var headerStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseHeaderDetailStore');
        var leaseHeaderData = headerStore.findRecord('leaseId', leaseId, 0, false, false, true);
        var listStore = Ext.StoreMgr.lookup(view.listGridStore);
        var ownerStore = Ext.getStore('Minerals.store.ProperRecords.OwnerComboStore');
        var appraisalValue = leaseHeaderData.get('appraisedValue');
        var nCount = 0, eCount = 0, dCount = 0, dNames = '';
        var StoreRecord = []
        var totalInterest = 0;
        if(data[0].Owner_ID || data[0].Owner_Name){
            for (var i = 0; i < data.length; i++) {
                var OwnerNameValues = data[i].Owner_Name+' '+data[i].Owner_Name2;    
                var gridRecord = new listStore.model;
                var interestTypeCd = 0;
                var interestTypeStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseInterestTypeStore');
                
                if(!isNaN(data[i].Owner_ID*1 && data[i].Interest_Percent*1)){
                    var interestType = interestTypeStore.findRecord('interestType', data[i].Interest_Type, 0, false, false, true);
                    if(interestType && data[i].Interest_Type !== 'UNASSIGNED'){
                        interestTypeCd = interestType.get('interestTypeCd');
                        totalInterest = totalInterest + (1*data[i].Interest_Percent);
                    } else {
                        data[i].rowDeleteFlag = 'D';
                        data[i].Interest_Type = 'INVALID TYPE';
                        ++dCount;                       
                    }
                    if (listStore.findRecord('ownerId', data[i].Owner_ID, 0, false, false, true)) {
                        var resultFind = listStore.findRecord('ownerId', data[i].Owner_ID, 0, false, false, true);
                        resultFind.set(data[i]);
                        resultFind.set('leaseId', leaseId);
                        resultFind.set('leaseName', leaseName);
                        resultFind.set('interestType', data[i].Interest_Type);
                        resultFind.set('interestTypeCd', interestTypeCd);
                        resultFind.set('interestPercent', data[i].Interest_Percent);
                        resultFind.set('ownerName', data[i].Owner_Name+' '+data[i].Owner_Name2);
                        resultFind.set('ownerId', data[i].Owner_ID);
                        resultFind.set('OwnerValues', (appraisalValue*(data[i].Interest_Percent*1)));
                        resultFind.set('appraisedValue', appraisalValue);
                        ++eCount;
                    } else {
                        gridRecord.set(data[i]);
                        gridRecord.set('leaseId', leaseId);
                        gridRecord.set('leaseName', leaseName);
                        gridRecord.set('interestType', data[i].Interest_Type);
                        gridRecord.set('interestTypeCd', interestTypeCd);
                        gridRecord.set('interestPercent', data[i].Interest_Percent);
                        gridRecord.set('ownerName', data[i].Owner_Name+' '+data[i].Owner_Name2);
                        gridRecord.set('ownerId', data[i].Owner_ID);
                        gridRecord.set('OwnerValues', (appraisalValue*(data[i].Interest_Percent*1)));
                        gridRecord.set('appraisedValue', appraisalValue);
                        StoreRecord[nCount] = gridRecord;
                        ++nCount;//Array Count for new Records
                    }
                } else {
                    dNames += '[ '+OwnerNameValues+' ]<br>'
                    ++dCount; //Count for Excluded records that has invalid input
                }
            }
            var sMessage = '<p><b>File name: '+ input +'</b></p><fieldset><p>Total Update Record: '+(eCount)+'</p>'+'<p>Total New Record: '+(nCount)+'</p>'+'<p>Total Interest Percent added: ('+Ext.util.Format.number((parseFloat(totalInterest).toFixed(8)*100),'0.0')+' %)</p></fieldset>';
            if(dCount>0){
                sMessage += '<p>Invalid Record:['+ dCount +']<br/>Cant convert input data type</p><p style="color:red">'+dNames+'</p>';
            }
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
                title:'Upload Completed',
                padding:20,
                msg: sMessage,
                buttons: Ext.Msg.OK
            });
        } else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
                title:'Invalid Format',
                msg: "<p>Invalid File Format. Can't Read from this file source <br/>- Please  download File Format on Import DO</p>",
                buttons: Ext.Msg.OK
            });

        }
        return StoreRecord
    },
    uploadFile: function(button) {
        var me = this;
        var view = me.getView();
        var form = view.down('form');
        var listStore = Ext.StoreMgr.lookup(view.listGridStore);
        var fileInfo = view.down('#fileUploadItemId')
        var filename = fileInfo.value
        if(fileInfo.wasValid) {
            form.submit({
                url: '/api/excel/upload',
                waitMsg: 'Uploading your file...',
                success: function(fm, action) {
                    //Get data from Imported data
                    var importData = action.result.data;
                    var record = me.getImportToGrid(importData, filename);

                    for (var i = 0; i < record.length; i++) {
                        listStore.insert(0,record[i]);
                    }
                    me.getView().close();
                },
                failure: function(form, response) {
                    Ext.Msg.alert('Invalid File Format', "<p style='text-align:left;padding:0;margin:0'>This File has invalid format and cannot be opened.</p>");
                }
            });
        } else {
            var message = "<p style='text-align:center;padding:0;margin:0;font-weight:bold;font-size:13px;'>Import data field is invalid<ul style='margin:0'><li style='color:red'>";
                if(!fileInfo.activeError){
                    message += 'No input file specified</li></ul>';
                } else {
                    message += fileInfo.activeError+'</li></ul>';
                }
                
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
                title:'Error',
                msg: message,
                buttons: Ext.Msg.OK
            });
        }
    },
    downloadExcelXml: function(title) {

        var me = this.getView()
        var listStore = Ext.StoreMgr.lookup(me.listGridStore); 
        var listGrid = Ext.getCmp(me.mainPanelId).down(me.listGridAlias);

        //if (!title) title = listGrid.title;

        //var vExportContent = this.getExcelXml(title);
        //var location = 'data:application/vnd.ms-excel;base64,' + Base64.encode(vExportContent);

        /* 
          dynamically create and anchor tag to force download with suggested filename 
          note: download attribute is Google Chrome specific
        */
        var selectedModel = new Ext.data.Model({
            fields:[
                {name: 'Owner_Id', type: "Int", defaultValue: 0},
                {name: 'Owner_Name', type: "string", defaultValue: ''},
                {name: 'Owner_Name2', type: "string", defaultValue: ''},
                {name: 'Owner_Mailing1', type: "string", defaultValue: ''},
                {name: 'Owner_Mailing2', type: "string", defaultValue: ''},
                {name: 'Owner_City', type: "string", defaultValue: ''},
                {name: 'Owner_ST', type: "string", defaultValue: ''},
                {name: 'Owner_Zip', type: "string", defaultValue: ''},
                {name: 'Interest_Type', type: "string", defaultValue: ''},
                {name: 'Interest_Percent', type: "string", defaultValue: ''},
            ]
        });
        var selectedStore = new Ext.data.Store({
            model: selectedModel,
            autoSave: false,
            autoLoad: false,
            proxy: {
                type: "localstorage"         
            }
        });

        var selected = listGrid.getView().getSelectionModel().selected.items;
        
        for(var i=0; i<selected.length; i++){
            var nRecord = new selectedStore.model;
                nRecord.set('Owner_Id', selected[i].data.ownerId);
                nRecord.set('Owner_Name', selected[i].data.Owner_Name);
                nRecord.set('Owner_Name2', selected[i].data.Owner_Name2);
                nRecord.set('Owner_Mailing1', selected[i].data.Owner_Mailing1);
                nRecord.set('Owner_Mailing2', selected[i].data.Owner_Mailing2);
                nRecord.set('Owner_City', selected[i].data.Owner_City);
                nRecord.set('Owner_ST', selected[i].data.Owner_ST);
                nRecord.set('Owner_Zip', selected[i].data.Owner_Zip);
                nRecord.set('Interest_Type', selected[i].data.interestType);
                nRecord.set('Interest_Percent', selected[i].data.interestPercent);
                selectedStore.add(nRecord);
        }
        var jsonData = Ext.encode(Ext.pluck(selectedStore.data.items, 'data'));
        var sendJasonData = '{"data":'+jsonData+'}';
        var filename = listGrid.title+'-'+Ext.Date.format(new Date(), 'Y-m-d H:i');
        if(selected.length<=192){
            Ext.MessageBox.show({
                title:'Save as',
                msg: 'Downloaded File Info <br /><br />'+
                    'File name: '+filename+'<br />'+
                    'Lease name: '+Ext.getCmp(me.mainPanelId).selectedName+' <br />'+
                    'Record: [ '+ selected.length +' ]<br />',
                buttons: Ext.Msg.OKCANCEL,
                icon: Ext.Msg.INFO,
                fn: function(btn){
                        
                    if (btn == "ok"){
                        Ext.Ajax.request({
                            url: '/api/excel/download',
                            method:'GET',
                            params: {
                                data: sendJasonData,
                                fileName: filename
                            },
                            success: function(response){
                                var el = Ext.DomHelper.append(Ext.getBody(), {
                                    tag:          'a',
                                    href:          response.request.requestOptions.url
                                });
                                el.click();
                                me.close();
                            },
                            failed: function(response){
                                Ext.MessageBox.show({
                                    title:'Download Failed',
                                    msg: 'Cannot process your request.',
                                    buttons: Ext.Msg.OK,
                                    icon: Ext.Msg.ERROR,
                                });
                            }
                        });      
                    }
                }                
            });
        } else {
            Ext.MessageBox.show({
                title:'Internal Error',
                msg: "<p style='text-align:center;padding:0;margin:0;font-weight:bold;font-size:13px;'>Unable to Process Request.</p></br><p style='color:red;text-align:center;padding:0;margin:0;font-weight:normal;font-size:13px;'> Number of records exceeds limit to</br>192 maximum records</p>",
                buttons: Ext.Msg.OK,
                iconCls: Ext.Msg.INFO
            });
        }
		
        // if (Ext.isChrome) {
        //     var tpl = new Ext.XTemplate(
        //             'Downloaded File Info <br /><br />',
        //             'File name: '+listGrid.title+'-'+Ext.getCmp(me.mainPanelId).selectedName+'-'+Ext.Date.format(new Date(), 'Y-m-d Hi')+'<br />',
        //             'Lease Owner: '+Ext.getCmp(me.mainPanelId).selectedName+' <br />'
        //     );
        //     var gridEl = listGrid.getEl();
        //     var el = Ext.DomHelper.append(gridEl, {
        //         tag: "a",
        //         download: listGrid.title + "-" + Ext.Date.format(new Date(), 'Y-m-d Hi') + '.xml',
        //         href: location
        //     });
        //     // Start to download File Formatted
        //     var startDownload = Ext.Msg.alert('Save as', tpl.apply(),
        //         function(){el.click();}
        //     );       
        //     Ext.fly(el).destroy();
        //     me.close();
            
        // } else {
        //     var form = me.down('#uploadForm');
        //     if (form) {
        //         form.destroy();
        //     }
        //     form = me.add({
        //         xtype: 'form',
        //         itemId: 'uploadForm',
        //         hidden: true,
        //         standardSubmit: true,
        //         url: 'http://webapps.figleaf.com/dataservices/Excel.cfc?method=echo&mimetype=application/vnd.ms-excel&filename=' + escape(listGrid.title+'-'+ Ext.Date.format(new Date(), 'Y-m-d Hi') + ".xml"),
        //         items: [{
        //             xtype: 'hiddenfield',
        //             name: 'data',
        //             value: vExportContent
        //         }]
        //     });
        //     form.getForm().submit();
        //     Ext.fly(el).destroy();
        //     me.close();
        // }
    },
        // getExcelXml: function(title) {
        //     var me = this.getView()
        //     var listStore = Ext.StoreMgr.lookup(me.listGridStore); 
        //     var listGrid = Ext.getCmp(me.mainPanelId).down(me.listGridAlias);

        //     var theTitle = listGrid.title;

        //     var worksheet = this.createWorksheet(theTitle);
        //     var cm = listGrid.columns;
        //     var totalWidth = listGrid.columns.length;

        //     return ''.concat(
        //         '<?xml version="1.0"?>',
        //         '<Workbook xmlns="urn:schemas-microsoft-com:office:spreadsheet" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns:ss="urn:schemas-microsoft-com:office:spreadsheet" xmlns:html="http://www.w3.org/TR/REC-html40">',
        //         '<DocumentProperties xmlns="urn:schemas-microsoft-com:office:office"><Title>' + theTitle + '</Title>',
        //         '<LastAuthor>Sam Eliseo</LastAuthor>',
        //         '<Created>'+new Date()+'</Created>',
        //         '<LastSaved>'+new Date()+'</LastSaved>',
        //         '<Version>16.00</Version></DocumentProperties>',
        //         '<OfficeDocumentSettings xmlns="urn:schemas-microsoft-com:office:office"><AllowPNG/></OfficeDocumentSettings>',
        //         '<ExcelWorkbook xmlns="urn:schemas-microsoft-com:office:excel">',
        //         '<WindowHeight>' + worksheet.height + '</WindowHeight>',
        //         '<WindowWidth>' + worksheet.width + '</WindowWidth>',
        //         '<ProtectStructure>False</ProtectStructure>',
        //         '<ProtectWindows>False</ProtectWindows>',
        //         '</ExcelWorkbook>',

        //         '<Styles>',

        //         '<Style ss:ID="Default" ss:Name="Normal">',
        //         '<Alignment ss:Vertical="Bottom"/>',
        //         '<Borders/>',
        //         '<Font ss:FontName="Calibri" x:Family="Swiss" ss:Size="12" ss:Color="#000000"/>',
        //         '<Interior/>',
        //         '<NumberFormat/>',
        //         '<Protection/>',
        //         '</Style>',

        //         '<Style ss:ID="headercell">',
        //         '<Font ss:Bold="1" ss:Size="10" />',
        //         '<Alignment ss:Horizontal="Center" ss:WrapText="1" />',
        //         '<Interior ss:Color="#A3C9F1" ss:Pattern="Solid" />',
        //         '</Style>',


        //         '<Style ss:ID="even">',
        //         '<Interior ss:Color="#CCFFFF" ss:Pattern="Solid" />',
        //         '</Style>',


        //         '<Style ss:ID="evendate" ss:Parent="even">',
        //         '<NumberFormat ss:Format="yyyy-mm-dd" />',
        //         '</Style>',


        //         '<Style ss:ID="evenint" ss:Parent="even">',
        //         '<Numberformat ss:Format="0" />',
        //         '</Style>',

        //         '<Style ss:ID="evenfloat" ss:Parent="even">',
        //         '<Numberformat ss:Format="0.00" />',
        //         '</Style>',

        //         '<Style ss:ID="odd">',
        //         '<Interior ss:Color="#CCCCFF" ss:Pattern="Solid" />',
        //         '</Style>',

        //         '<Style ss:ID="groupSeparator">',
        //         '<Interior ss:Color="#D3D3D3" ss:Pattern="Solid" />',
        //         '</Style>',

        //         '<Style ss:ID="odddate" ss:Parent="odd">',
        //         '<NumberFormat ss:Format="yyyy-mm-dd" />',
        //         '</Style>',

        //         '<Style ss:ID="oddint" ss:Parent="odd">',
        //         '<NumberFormat Format="0" />',
        //         '</Style>',

        //         '<Style ss:ID="oddfloat" ss:Parent="odd">',
        //         '<NumberFormat Format="0.00" />',
        //         '</Style>',


        //         '</Styles>',
        //         worksheet.xml,
        //         '</Workbook>'
        //     );
        // },

        // /*  Support function to return field info from store based on fieldname */

        // getModelField: function(fieldName) {
        //      var me = this.getView();
        //      var listStore = Ext.StoreMgr.lookup(me.listGridStore); 
        //      var listGrid = Ext.getCmp(me.mainPanelId).down(me.listGridAlias);

        //      var fields = listGrid.store.model.getFields();
        //      for (var i = 0; i < fields.length; i++) {
        //          if (fields[i].name === fieldName) {
        //              return fields[i];
        //          }
        //      }
        //  },

        // /* Convert store into Excel Worksheet */

        // generateEmptyGroupRow: function(dataIndex, value, cellTypes) {
        //     var me = this.getView();
        //     var listStore = Ext.StoreMgr.lookup(me.listGridStore); 
        //     var listGrid = Ext.getCmp(me.mainPanelId).down(me.listGridAlias);

        //     var cm = listGrid.columns;
        //     var colCount = cm.length;
        //     var rowTpl = '<Row ss:AutoFitHeight="0"><Cell ss:StyleID="groupSeparator" ss:MergeAcross="{0}"><Data ss:Type="String"><html:b>{1}</html:b></Data></Cell></Row>';
        //     var visibleCols = 0;

        //     rowXml += '<Cell ss:StyleID="groupSeparator">'

        //     for (var j = 0; j < colCount; j++) {
        //         if (cm[j].xtype != 'actioncolumn' && (cm[j].dataIndex != '') && cm[j].xtype != 'checkcolumn' && cm[j].config.Ctype != 'none') {
        //              rowXml += '<Cell ss:StyleID="groupSeparator"/>';
        //             visibleCols++;
        //         }
        //     }

        //     rowXml += "</Row>";

        //     return Ext.String.format(rowTpl, visibleCols - 1, value);
        // },


        // createWorksheet: function(theTitle) {
        //     var me = this.getView();
        //     var listStore = Ext.StoreMgr.lookup(me.listGridStore); 
        //     var listGrid = Ext.getCmp(me.mainPanelId).down(me.listGridAlias);
        //     // Calculate cell data types and extra class names which affect formatting
        //     var cellType = [];
        //     var cellTypeClass = [];
        //     var cm = listGrid.columns;
        //     var totalWidthInPixels = 0;
        //     var colXml = '';
        //     var headerXml = '';
        //     var visibleColumnCountReduction = 0;
        //     var colCount = cm.length;

        //     for (var i = 0; i < colCount; i++) {
        //         if (cm[i].xtype != 'actioncolumn' && (cm[i].dataIndex != '') && cm[i].xtype != 'checkcolumn' && cm[i].config.Ctype != 'none') {
        //             var w = (cm[i].getEl().getWidth()==0) ? 100 :cm[i].getEl().getWidth();
        //             totalWidthInPixels += w;

        //             if (cm[i].text === "") {
        //                 cellType.push("None");
        //                 cellTypeClass.push("");
        //                 ++visibleColumnCountReduction;
        //             } else {
        //                 colXml += '<Column ss:AutoFitWidth="1" ss:Width="' + w + '" />';
        //                 headerXml += '<Cell ss:StyleID="headercell">' +
        //                     '<Data ss:Type="String">' + cm[i].text + '</Data>' +
        //                     '<NamedCell ss:Name="Lease_Owner"></NamedCell></Cell>';

        //                 var fld = this.getModelField(cm[i].dataIndex);
        //                 switch (fld.Ctype) {
        //                     case "int":
        //                         cellType.push("Number");
        //                         cellTypeClass.push("int");
        //                         break;
        //                     case "float":
        //                         cellType.push("Number");
        //                         cellTypeClass.push("float");
        //                         break;
        //                     case "bool":
        //                     case "boolean":
        //                         cellType.push("String");
        //                         cellTypeClass.push("");
        //                         break;
        //                     case "date":
        //                         cellType.push("DateTime");
        //                         cellTypeClass.push("date");
        //                         break;
        //                     default:
        //                         cellType.push("String");
        //                         cellTypeClass.push("");
        //                         break;
        //                 }
        //             }
        //         }
        //     }
        //     var visibleColumnCount = cellType.length;
        //     var result = {
        //         height: 9000,
        //         width: Math.floor(totalWidthInPixels * 30) + 50
        //     };

        //     // Generate worksheet header details.
        //     // determine number of rows
        //     var selected = listGrid.getView().getSelectionModel().selected.items
        //     var numGridRows = selected.length+1;
        //      if (!Ext.isEmpty(listGrid.store.groupField)) {
        //          numGridRows = numGridRows + listGrid.store.getGroups().length;
        //      }
        //     // create header for worksheet
        //     var t = ''.concat(
        //         '<Worksheet ss:Name="Lease_Owner">',
        //         '<Table ss:ExpandedColumnCount="' + (visibleColumnCount),
        //         '" ss:ExpandedRowCount="' + numGridRows + '" x:FullColumns="1" x:FullRows="1" ss:DefaultColumnWidth="65" ss:DefaultRowHeight="15">',
        //         colXml,
        //         '<Row ss:AutoFitHeight="1">',
        //         headerXml +
        //         '</Row>'
        //     );

        //     // Generate the data rows from the data in the Store
        //     var groupVal = "";
        //     var groupField = "";
        //     var  storeData = listGrid.store.model

        //     for (var i = 0, it = selected, l = it.length; i < l; i++) {
        //         t += '<Row>';
        //         var cellClass = (i & 1) ? 'odd' : 'even';
        //         r = it[i].data;
        //         var k = 0;
        //         for (var j = 0; j < colCount; j++) {
        //             if (cm[j].xtype != 'actioncolumn' && (cm[j].dataIndex != '') && cm[j].xtype != 'checkcolumn' && cm[j].config.Ctype != 'none') {
        //                 var v = r[cm[j].dataIndex];
        //                 if (cellType[k] !== "None") {
        //                     t += '<Cell ss:StyleID="' + cellClass + cellTypeClass[k] + '"><Data ss:Type="' + cellType[k] + '">';
        //                     if (cellType[k] == 'DateTime') {
        //                         t += Ext.Date.format(v, 'Y-m-d');
        //                     } else {
        //                         t += v;
        //                     }
        //                     t += '</Data></Cell>';
        //                 }
        //                 k++;
        //             }
        //         }
        //         t += '</Row>';
        //     }

        //     result.xml = t.concat(
        //         '</Table>',
        //         '<WorksheetOptions xmlns="urn:schemas-microsoft-com:office:excel">',
        //         '<Unsynced/>',
        //         '<Selected/>',
        //         '<Panes>',
        //         '<Pane>',
        //         '<Number>3</Number>',
        //         '<ActiveRow>1</ActiveRow>',
        //         '</Pane>',
        //         '</Panes>',
        //         '<ProtectObjects>False</ProtectObjects>',
        //         '<ProtectScenarios>False</ProtectScenarios>',
        //         '</WorksheetOptions>',
        //         '</Worksheet>'
        //     );
        //     return result;
        // }

});