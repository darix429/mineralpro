Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersView' , {
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.MineralsLeaseOwners',
    layout: 'border',
    
    id: 'MineralsLeaseOwnersId',
    
    items: [{
           // title: 'LeaseOwners',
            xtype: 'LeaseOwnersGridView',
            html:'center',
            region:'center'
        }] 
});    