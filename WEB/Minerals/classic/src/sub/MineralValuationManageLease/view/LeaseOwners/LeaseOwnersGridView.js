Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.LeaseOwnersGridView',
    store:'Minerals.sub.MineralValuationManageLease.store.LeaseOwnerStore',
    title:'Lease Owner',
    xtype: 'grouped-grid',
    requires: ['Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersGridViewController','Ext.grid.feature.Grouping'],
    controller: 'LeaseOwnersGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'ownerNoId', 
    mainPanelAlias: 'MineralValuationManageLease', //widget alias name of the main panel
    mainListModel: 'Minerals.sub.MineralValuationManageLease.model.LeaseOwnerModel',     

    addFormAliasName: 'LeaseOwnersAddFormView', //widget form to call for add list 
    editFormAliasName: 'LeaseOwnersEditFormView', //widget form to call for edit

    
    selectFirstRow: false,
    firstFocus: 'ownerNoId',  
    
    addButtonTooltip: 'Add Lease Owner',
    addButtonText: 'Add Lease Owner',

    iconCls: 'icon-grid',
    features: [{
        id: 'group',
        ftype: 'groupingsummary',
        groupHeaderTpl: '{columnName}: {name}',
       // hideGroupedHeader: true,
        enableGroupingMenu: true,
        startCollapsed: true 
    }],    
    columns:[{ 
        header: 'Owner_ID',
        flex     : 1,
        maxWidth: 100,
        dataIndex: 'ownerId',
        itemId: 'ownerNoId',
        summaryType: 'count',
        sortable: true,
        summaryRenderer: function(value, summaryData, dataIndex) {
            return ((value === 0 || value > 1) ? '(' + value + ' Owners)' : '(1 Owner)');
        },
        filterElement: new Ext.form.TextField()
    },{ 
        header: 'Owner_Name',
        flex     : 1,
        width: 100,
        dataIndex: 'Owner_Name',
        sortable: true,
        hidden:true
    },{ 
        header: 'Owner_Name2',
        flex     : 1,
        maxWidth: 100,
        dataIndex: 'Owner_Name2',
        sortable: true,
        hidden:true
    },{ 
        header: 'Owner_Mailing1',
        flex: 1,
        width: 100,
        dataIndex: 'Owner_Mailing1',
        sortable: true,
        hidden:true
    },{ 
        header: 'Owner_Mailing2',
        flex: 1,
        width: 100,
        dataIndex: 'Owner_Mailing2',
        sortable: true,
        hidden:true
    },{ 
        header: 'Owner_City',
        flex: 1,
        width: 100,
        dataIndex: 'Owner_City',
        sortable: true,
        hidden:true
    },{ 
        header: 'Owner_ST',
        flex: 1,
        width: 100,
        dataIndex: 'Owner_ST',
        sortable: true,
        hidden:true
    },{ 
        header: 'Owner_Zip',
        flex: 1,
        width: 100,
        dataIndex: 'Owner_Zip',
        sortable: true,
        hidden:true
    },{   
        header: 'Owner Name',
        flex: 1, 
        autoSizeColumn: true, 
        minWidth: 180,
        dataIndex: 'ownerName',
        itemId: 'ownerNameId',
        sortable: true,
        filterElement:new Ext.form.TextField(),
        renderer: function (value) {
                return '<a href="javascript:void(0);" >' + value + '</a>';
        },
        listeners: {
            click: function (cell, td, rowIndex, cellIndex, e, record, value, index) {
                this.up('grid').getController().onOpenSelectedTabView(record, rowIndex);
            }
        }
    },{   
        header: 'Interest_Type',
        flex: 1,
        maxWidth: 200,
        dataIndex: 'interestType',
        Ctype:'string',
        sortable: true,
        filterElement:new Ext.form.TextField(),
        renderer: function(val, meta) {
            if(val == 'UNASSIGNED'){
                return '<p style="color:red;margin:0px;padding:0px 20px;background: url(../resources/icons/tp_lower.png) no-repeat left;">'+val+'</p>'    
            }
            return  val;
        },    
     },{   
        header: 'Interest_Percent',
        flex: 1,
        maxWidth: 180,
        sortable: true,
        dataIndex: 'interestPercent',
        Ctype:'float',
        summaryType: function(records){
            var totalInterestPercent = 0;
            for(var i = 0; records.length>i; i++)
            {   
                if(records[i].get('rowDeleteFlag')=='' && !isNaN(parseFloat(records[i].get('interestPercent')).toFixed(8)*1)){
                    totalInterestPercent = ((1*totalInterestPercent) + (parseFloat(records[i].get('interestPercent')).toFixed(8)*1));
                }
            }
            return '('+Ext.util.Format.number(totalInterestPercent*100, '0.0')+'%) Total Interest';
        },
        renderer: function(val, meta) {
            if(val == '-0' || val > 1){
                return '<p style="color:red;margin:0px;padding:0px 20px;background: url(../resources/icons/tp_lower.png) no-repeat left;">'+val+'</p>'    
            }
            return parseFloat(val).toFixed(8);
        },
        filterElement:new Ext.form.TextField()    
     },{   
        header: 'Owner Value',
        flex: 1,
        maxWidth: 180,
        align: 'right',
        dataIndex: 'OwnerValues',
        sortable: true,
        summaryType: 'float',
        // summaryRenderer: function(val, params, data) {
        //    return  'Total Values '+Ext.util.Format.number(val, '$000,000');
        // },
        summaryType: function(records){
            var totalOwnerValues = 0;
            for(var i = 0; records.length>i; i++)
            {   
                if(records[i].get('rowDeleteFlag')=='' && !isNaN(parseFloat(records[i].get('OwnerValues')))){
                    totalOwnerValues = ((1*totalOwnerValues) + (parseFloat(records[i].get('OwnerValues'))));
                }
            }
            return 'Total Values '+Ext.util.Format.number(totalOwnerValues, '$000,000');
        },
        renderer: function(val, meta) {
            return  Ext.util.Format.number(val, '$000,000');
        },
        filterElement:new Ext.form.TextField()    
     }, {
        xtype: 'actioncolumn',
        sortable: true,
        width: Minerals.config.Runtime.actionColWidth,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls,
        ],
        listeners: {
             afterrender: function(){
                  var me = this       
                  me.setHidden(MineralPro.config.Runtime.access_level)
             }
        }
    }],

        initComponent: function () {  
        var me = this;
        me.selModel= Ext.create('Ext.selection.CheckboxModel',{
            checkOnly: true,
             listeners: {
                selectionchange: function(){
                    var items = me.getSelectionModel().getSelected().length;

                    if(items){
                        me.down('#ExportDOButtonId').enable();
                        me.down('#DeleteSelectedButtonId').enable();
                        me.down('#TransferToMultipleGroupButtonId').enable();
                    }else{
                        me.down('#ExportDOButtonId').disable();
                        me.down('#DeleteSelectedButtonId').disable();
                        me.down('#TransferToMultipleGroupButtonId').disable();
                    }
                }
            } 
        }),  
        
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')]

        var ImportDOButton = {
            xtype: 'form',
            padding: 0,            
            width: 104,
            height: 33,
            itemId:'formUpload',
            align: 'center',
            bodyStyle:{"background-color":"transparent"},   
            items: [{
                xtype: 'filefield',
                width: 104,
                buttonOnly: true,
                buttonText: 'Import DO',
               // hideLabel: true,
               // disabled: true,
                itemId: 'ImportDOButtonId',
				alwaysShow: true,
                buttonConfig: {
                    iconCls: Minerals.config.Runtime.importDOButtonIcon,
                },
                listeners: {
                    change: 'onImportDO',
                    afterrender: function(){
                        // var mainView = Ext.getCmp('MineralValuationManageLeaseId');
                        // var leaseId = mainView.selectedId;

                        // if(leaseId == '' || leaseId == null || leaseId == 0){
                        //     me.down('#ImportDOButtonId').disable();
                        // } else {
                        //     me.down('#ImportDOButtonId').enable();
                        // }
                        me.down('#ImportDOButtonId').setHidden(MineralPro.config.Runtime.access_level)

						
                    }
                }
            }]

        }

        var ExportDOButton = {
            xtype: 'form',
            padding: 0,            
            width: 104,
            height: 33,
            itemId:'formDownload',
            align: 'center',
            bodyStyle:{"background-color":"transparent"},   
            items: [{
                xtype: 'button',
                tooltip: 'Export Division Order',
                text: 'Export DO',
                iconCls: 'export',
                disabled: true,
                itemId: 'ExportDOButtonId',
				alwaysShow: true,
                listeners: {
                    click: 'onExportDO'
                    /*afterrender: function(){
                        var me = this
                        me.setHidden(MineralPro.config.Runtime.access_level)
                    }*/
                }
            }]

        }

        var TransferGroupButton = {
                xtype: 'button',
                tooltip: Minerals.config.Runtime.ownerLeaseHoldingsTransferBtnTooltip,
                text: Minerals.config.Runtime.ownerLeaseHoldingsTransferBtnText,
                iconCls: Minerals.config.Runtime.ownerLeaseHoldingsTransferBtnIcon,
                listeners: {
                    click: 'onTransferMultipleGroup',
                    afterrender: function(){
                        var me = this
                        me.setHidden(MineralPro.config.Runtime.access_level)
                    }
                }
            }
        var TransferToMultipleGroupButton = {
                xtype: 'button',
                disabled: true,
                itemId: 'TransferToMultipleGroupButtonId',
                tooltip: Minerals.config.Runtime.leaseOwnerTransferToMultipleBtnTooltip,
                text: Minerals.config.Runtime.ownerLeaseHoldingsTransferToMultipleBtnText,
                iconCls: Minerals.config.Runtime.ownerLeaseHoldingsTransferToMultipleBtnIcon,
                listeners: {
                    click: 'onTransferMultipleToMultipleGroup',
                    afterrender: function(){
                        var me = this
                        me.setHidden(MineralPro.config.Runtime.access_level)
                    }
                }
            }
        var DeleteSelectedButton = {
                xtype: 'button',
                disabled: true,
                itemId: 'DeleteSelectedButtonId',
                tooltip: 'Delete Selected Lease Owner',
                text: 'Delete Selected',
                iconCls: Minerals.config.Runtime.deleteCls,
                listeners: {
                    click: 'onDeleteSelected',
                    afterrender: function(){
                        var me = this
                        me.setHidden(MineralPro.config.Runtime.access_level)
                    }
                }
            }    
        me.tools= [
        TransferToMultipleGroupButton,
        DeleteSelectedButton,
        ImportDOButton,
        ExportDOButton,
        MineralPro.config.util.MainGridToolCmpt.addNewDataButton,
        MineralPro.config.util.MainGridToolCmpt.saveToDbButton,                   
        MineralPro.config.util.MainGridToolCmpt.clearFilterButton],

        
        me.callParent(arguments);
        
    },
    listeners: {
        selectionchange: 'onLoadSelectionChange',
        
    }    
});
