//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseOwners.LeaseOwnersAddFormViewController', {
    extend: 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.LeaseOwnersAddFormViewController', 
    
    
    onAddAndNew: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseOwnerAddFormViewController onAddAndNew.')
        var me = this;                     
        var view = me.getView();
        var mainView = Ext.getCmp(view.mainPanelId);
        var listStore = Ext.StoreMgr.lookup(view.listGridStore); 
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
                
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();
        var leaseId = values.leaseId;
        var LeaseName = values.leaseName;          
        if(form.isValid()){
            if(!values.interestTypeCd == 0){
                if((values.totalInterestPercent*1) <= 1){
                    var TotalInterestPercent = values.totalInterestPercent;
                    var headerStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseHeaderDetailStore');
                    var leaseHeaderData = headerStore.findRecord('leaseId', leaseId, 0, false, false, true);
                    var appraisedValue = leaseHeaderData.get('appraisedValue');

                    record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                    record.set(values);    
                    listStore.insert(0,record);
                    
                    listGrid.getSelectionModel().select(0, true);  
                    //this part of the code will reset the data of the form window.
                    //form.reset()
                    //var new_record = Ext.create(listStore.model.displayName)//mainListModel
                    var new_record = Ext.create(listGrid.mainListModel)
                    
                    new_record.set('totalInterestPercent', Ext.util.Format.number(TotalInterestPercent,'0.00000000'));
                    new_record.set('leaseId', leaseId);
                    new_record.set('interestPercent', 0);
                    new_record.set('appraisedValue', appraisedValue);
                    view.down('#ownerValueItemId').hide();                    
                    form.loadRecord(new_record);
                    view.focus(view.defaultFocus)
                    //listGrid.fireEvent('afterlayout');
                }else{
                    Ext.Msg.show({
                        iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                        title: 'Lease Interest Pct Exeed',    
                        msg: 'The total Lease Interest Pct have Exeeded 1.00000000',               
                        buttons: Ext.Msg.YESNO,
                        fn: function (btn) {
                            if (btn === 'yes') {
                                var TotalInterestPercent = values.totalInterestPercent;
                                var headerStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseHeaderDetailStore');
                                var leaseHeaderData = headerStore.findRecord('leaseId', leaseId, 0, false, false, true);
                                var appraisedValue = leaseHeaderData.get('appraisedValue');

                                record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                                record.set(values);    
                                listStore.insert(0,record);
                                
                                listGrid.getSelectionModel().select(0, true);  
                                //this part of the code will reset the data of the form window.
                                //form.reset()
                                //var new_record = Ext.create(listStore.model.displayName)//mainListModel
                                var new_record = Ext.create(listGrid.mainListModel)
                                
                                new_record.set('totalInterestPercent', Ext.util.Format.number(TotalInterestPercent,'0.00000000'));
                                new_record.set('leaseId', leaseId);
                                new_record.set('interestPercent', 0);
                                new_record.set('appraisedValue', appraisedValue);
                                view.down('#ownerValueItemId').hide();                    
                                form.loadRecord(new_record);
                                view.focus(view.defaultFocus)
                                //listGrid.fireEvent('afterlayout'); 
                            }   
                        }
                    });
                }
            } else {
                Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: 'Invalid Interest Type',    
                    msg: 'Owner Interest Type not assigned',               
                    buttons: Ext.Msg.OK
                });
            }
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        }  
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseOwnerAddFormViewController onAddAndNew.')
    },

    /**
    * Edit the data from the form into the data grid
    */

    onEditListToGrid: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseOwnerAddFormViewController onAddAndNew.')
        var me = this;        
        var view = me.getView();
        var mainPanel = Ext.getCmp(view.mainPanelId);
        var listGrid = mainPanel.down(view.listGridAlias)
        var LeaseName = mainPanel.selectedName;
        var leaseNameId = mainPanel.selectedId;
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();
        if(form.isValid()){
            if(!values.interestTypeCd==0){
                if((values.totalInterestPercent*1) <= 1){
                    
                    record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                    record.set(values);    
                    
                    if(listGrid.withDetailsView){
                        var detailView = mainPanel.down(listGrid.detailAliasName);
                        detailView.loadRecord(record);
                    }
                    win.close();

                }else{
                    Ext.Msg.show({
                        iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                        title: 'Lease Interest Pct Exeed',    
                        msg: 'The total Lease Interest Pct have Exeeded 1.00000000. <br />Do you want to proceed?',               
                        buttons: Ext.Msg.YESNO,
                        fn: function (btn) {
                            if (btn === 'yes') {
                                record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                                record.set(values);    
                                
                                if(listGrid.withDetailsView){
                                    var detailView = mainPanel.down(listGrid.detailAliasName);
                                    detailView.loadRecord(record);
                                }
                                win.close();
                            }
                        }
                    });
                }
            } else {
                Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: 'Invalid Interest Type',    
                    msg: 'Owner Interest Type not assigned',               
                    buttons: Ext.Msg.OK
                });
            }                         
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        } 
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAddEditFormViewController onEditListToGrid.')
    },
    
    /**
     * Add the data from the form into the data grid
     */
    onAddListToGrid: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseOwnerAddFormViewController onAddListToGrid.')
        var me = this;                     
        var view = me.getView();
        var mainPanel = Ext.getCmp(view.mainPanelId);
        var listStore = Ext.StoreMgr.lookup(view.listGridStore); 
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();
        var LeaseName = mainPanel.selectedName;
        var leaseNameId = mainPanel.selectedId;

        if(form.isValid()){
            if(!values.interestTypeCd==0){
                if((values.totalInterestPercent*1) <= 1){
                    record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                    record.set(values);    
                    win.close();
                    listStore.insert(0,record);
                    
                    listGrid.getSelectionModel().select(0, true);  
                    listGrid.getView().focusRow(0);   
                    //listGrid.fireEvent('afterlayout');
                }else{
                    Ext.Msg.show({
                        iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                        title: 'Lease Interest Pct Exeed',    
                        msg: 'The total Lease Interest Pct have Exeeded 1.00000000. <br />Do you want to proceed?',               
                        buttons: Ext.Msg.YESNO,
                        fn: function (btn) {
                            if (btn === 'yes') {
                                record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                                record.set(values);    
                                win.close();
                                listStore.insert(0,record);
                                
                                listGrid.getSelectionModel().select(0, true);  
                                listGrid.getView().focusRow(0);   
                                //listGrid.fireEvent('afterlayout');
                            }
                        }
                    });
                }
            } else {
                Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: 'Invalid Interest Type',    
                    msg: 'Owner Interest Type not assigned',               
                    buttons: Ext.Msg.OK
                });
            }
        } else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseOwnerAddFormViewController onAddListToGrid.')
    },
    onAddCheckLeaseOwnerExisting: function (e, event, opt) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseOwnerAddFormViewController onAddLeaseOwnerCheckExisting.')

        var me = this;
        var view = me.getView();
        var ownerId = view.down('#ownerNoItemId')
        var interestType = view.down('#interestTypeItemId');
        var showAlert = false;

        setTimeout(function () {
            var listStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseOwnerStore');
            var allRecords = listStore.getData().getSource() || listStore.getData();
            allRecords.each(function (record) {
                if (record.data.ownerId == ownerId.value) {
                    if (record.get(interestType.name) == interestType.value) {
                        showAlert = true;
                        if (view.alias[0] == 'widget.LeaseOwnersEditFormView') {
                           // console.log('Edit Form')
                            if (listStore.getAt(view.editRecordIndex).get(interestType.name) == interestType.value) {
                                 showAlert = false;
                            }
                        }
                    }
                }
            })

            if (showAlert) {
                Ext.Msg.show({
                    iconCls: MineralPro.config.Runtime.addExistingDataWarningCls,
                    title: MineralPro.config.Runtime.addExistingDataWarningTitle,
                    msg: 'The Owner has already existing Interest Type on the list.',
                    buttons: Ext.Msg.OK,
                    fn: function () {
                        e.focus();
                        view.down('#interestTypeCdItemId').select(0);
                    }
                });
            }

        }, 200)
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseOwnerAddFormViewController onAddLeaseOwnerCheckExisting.')
    },
    onAddCheckExisting: function(e, event, opt){
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseOwnerAddFormViewController onAddCheckExisting.')
        
        var me = this;  
        var view = me.getView();
        
         setTimeout(function (){                       
            if (view.id.toLowerCase().indexOf("edit") == -1) {

                var listStore = Ext.StoreMgr.lookup(view.listGridStore);

                if (listStore.findRecord(e.name, e.value, 0, false, false, true)) {
                    Ext.Msg.show({
                        iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
                        title:'Alert',
                        msg: 'Owner is already in your list.',
                        buttons: Ext.Msg.OK,
                        fn: function(){
                            e.focus();
                        }
                    });
                }
            }
        },200)                         
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseOwnerAddFormViewController onAddCheckExisting.')    
    }
});