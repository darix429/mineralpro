
Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseAppraisalNote.LeaseAppraisalNoteGridView', {
    extend: 'Minerals.config.view.Note.BaseNoteGridView',
    alias: 'widget.LeaseAppraisalNoteGridView',
    store:'Minerals.sub.MineralValuationManageLease.store.LeaseNoteAppraisalStore',

    // requires: ['Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteGridViewController'],
    // controller: 'LeaseNoteGridViewController',     
          
    //below are default items needed for grid checking 
    // firstLoadItemId: 'dateItemId', 
    mainPanelAlias: 'MineralValuationManageLease', //widget alias name of the main panel
    mainListModel: 'Minerals.model.MineralValuation.NotesModel',     

    addFormAliasName: 'LeaseAppraisalNoteAddFormView', //widget form to call for add list 
    editFormAliasName: 'LeaseAppraisalNoteEditFormView', //widget form to call for edit
   

    // firstFocus: 'dateItemId',    
    
    addButtonTooltip: 'Add New Note',
    addButtonText: 'Add New Note',
    
    fieldGridViewAlias: 'LeaseFieldsGridView' 
        
    // columns:[{   
    //     header: 'Updated Date',
    //     flex     : 2,
    //     dataIndex: 'createDt',
    //     itemId: 'dateItemId',
    //     filterElement: new Ext.form.TextField({
    //     })
    // },{   
    //     header: 'Updated By',
    //     flex     : 2,
    //     dataIndex: 'appraiserName',
    //     filterElement:new Ext.form.TextField()     
    // },{   
    //     header: 'Note',
    //     flex     : 4,
    //     dataIndex: 'note',
    //     filterElement:new Ext.form.TextField() 
    // },{   
    //     header: 'Confidential',
    //     flex     : 1,
    //     dataIndex: 'securityCd',
    //     renderer: function(value){
    //         if(value == 2){
    //             return 'Yes';
    //         }else if(value == 1){
    //             return 'No';
    //         }
    //     },
    //     filterElement : new Ext.form.ComboBox({                              
    //         showFilterIcon:true,
    //         triggerAction           : 'all',                
    //         typeAhead               : true,                                
    //         mode                    : 'local',
    //         listWidth               : 160,
    //         hideTrigger             : false,
    //         emptyText               : 'Select',
    //         store                   :[['2','Yes'],['1','No']]
    //     })
    // },{
    //  xtype: 'actioncolumn',
    //     width: Minerals.config.Runtime.actionColWidth,
    //     items: [
    //         MineralPro.config.util.MainGridActionColCmpt.editCls,
    //         MineralPro.config.util.MainGridActionColCmpt.deleteCls,
    //     ],
    //     listeners: {
    //         afterrender: function(){
    //             var me = this       
    //             me.setHidden(MineralPro.config.Runtime.access_level)
    //         }
    //     }
    // }],
    // listeners: {
    //     afterrender: function(){
    //         var me = this;
    //         var LeaseNoteViewTypeCombo = me.down('#LeaseNoteViewTypeComboItemId');
    //         var LeaseNoteViewTypeComboStore = LeaseNoteViewTypeCombo.getStore();
    //         LeaseNoteViewTypeComboStore.load({
    //             callback: function(){
    //                 LeaseNoteViewTypeCombo.select(LeaseNoteViewTypeComboStore.getData().getAt(0));
    //             }
    //         })
    //         var LeaseNoteMineralCombo = me.down('#LeaseNoteMineralComboItemId');
    //         var LeaseNoteMineralComboStore = LeaseNoteMineralCombo.getStore();
    //         LeaseNoteMineralComboStore.load({
    //             callback: function(){
    //                 LeaseNoteMineralCombo.select(LeaseNoteMineralComboStore.getData().getAt(0));
    //             }
    //         })
    //     }
    // },
    // initComponent: function () {  
    //     var me = this;

    //     var LeaseNoteViewTypeCombo = {
    //         xtype: 'combo',
    //         width: 350,
    //         labelAlign: 'right',
    //         hidden: true,
    //         typeAhead: true,
    //         displayField: 'displayField',
    //         valueField: 'valueField',
    //         store: 'Minerals.sub.MineralValuationManageLease.store.LeaseStore',
    //         fieldLabel: 'View Type:',
    //         emptyText: 'Select View Type',
    //         itemId: 'LeaseNoteViewTypeComboItemId',
    //         listeners: {
    //             afterrender: function (field) {
    //                 Ext.defer(function () {
    //                     field.focus(true, 100);
    //                 }, 10);                
    //         },
    //             change: function (fieldLabel, newval, oldval) {
    //                 var me = this
    //                 var grid = me.up('grid');
    //                 var listStore = grid.getStore();
    //                 var LeaseNoteFilter = grid.down('#LeaseNoteMineralComboItemId').getValue();
    //                 listStore.getProxy().extraParams = {
    //                     mineralViewType: newval,
    //                     LeaseNoteFilter: LeaseNoteFilter
    //                 }
    //                 listStore.load();                              
    //         },
    //         beforequery: function(queryVV){
    //                 queryVV.combo.expand();
    //                 queryVV.combo.store.load();
    //                 return false;
    //             }
    //         }
    //     }
    
    //     var LeaseNoteMineralCombo = {
    //         xtype: 'combo',
    //         width: 350,
    //         labelAlign: 'right',
    //         typeAhead: true,
    //         hidden: true,
    //         dataIndex: 'mineralReference',
    //         displayField: 'displayField',
    //         valueField: 'valueField',
    //         store: 'Minerals.sub.MineralValuationManageLease.store.LeaseStore',
    //         fieldLabel: 'Lease:',
    //         emptyText: 'Select Lease',
    //         itemId: 'LeaseNoteMineralComboItemId',
    //         listeners: {
    //             change: function (fieldLabel, newval, oldval) {
    //                 var me = this
    //                 var grid = me.up('grid');
    //                 var listStore = grid.getStore();
    //                 var mineralViewType = grid.down('#LeaseNoteViewTypeComboItemId').getValue();
    //                 listStore.getProxy().extraParams = {
    //                     mineralViewType: mineralViewType,
    //                     LeaseNoteFilter: newval
    //                 }
    //                 listStore.load();                              
    //         },
    //             beforequery: function(queryVV){
    //                 queryVV.combo.expand();
    //                 queryVV.combo.store.load();
    //                 return false;
    //             }
    //         }
    //     }
               
    //     me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],       
               
    //     me.tools= [
    //     LeaseNoteViewTypeCombo,    
    //     LeaseNoteMineralCombo,
    //     MineralPro.config.util.MainGridToolCmpt.addNewDataButton,
    //     MineralPro.config.util.MainGridToolCmpt.saveToDbButton,                  
    //     MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
    //     me.callParent(arguments);
        
    // }    
});