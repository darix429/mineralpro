Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseAppraisalNote.LeaseAppraisalNoteEditFormView', {
    extend: 'Minerals.sub.MineralValuationManageLease.view.LeaseAppraisalNote.LeaseAppraisalNoteAddFormView',
    //requires: ['Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteEditFormViewController'],
    //controller: 'LeaseNoteEditFormViewController',
    alias: 'widget.LeaseAppraisalNoteEditFormView',
    
    
    title: 'Update Appraisal Note',
    // defaultFocus: '#noteId',
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});