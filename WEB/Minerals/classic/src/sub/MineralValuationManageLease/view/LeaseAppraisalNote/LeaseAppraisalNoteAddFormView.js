Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseAppraisalNote.LeaseAppraisalNoteAddFormView', {
    extend: 'Minerals.config.view.Note.BaseNoteAddEditFormView',
    // requires: ['Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteAddFormViewController'],
    // controller: 'LeaseNoteAddFormViewController',
    alias: 'widget.LeaseAppraisalNoteAddFormView',

    title: 'Add Appraisal Notes',
    saveAction: 'onAddListToGrid',
//    defaultFocus: '#noteId',
    layout: 'fit',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'MineralValuationManageLeaseId', //id of the main panel
    listGridStore: 'Minerals.sub.MineralValuationManageLease.store.LeaseNoteAppraisalStore', //store for note grid
    listGridAlias: 'LeaseAppraisalNoteGridView', 
    bodyPadding: 5,
    width: 800,
    height: 565,

    subjectType: 'Lease Note',

    subjectTypeLabel: 'Lease Name'

    // initComponent: function() {
    //     var me = this;
    //     me.items = [{
    //         xtype: 'form',
    //         defaults:{ maskRe: /[^'\^]/},
    //         margin:'0 5 0 0',
    //         bodyPadding: 5,
    //         items: [
    //             {
    //                 xtype: 'hidden',
    //                 name: 'mineralId',
    //                 itemId: 'mineralIdItemIdNote'
    //             },
    //             {
    //                 xtype: 'hidden',
    //                 name: 'mineralName',
    //                 itemId: 'LeaseNoteItemId'
    //             },
    //             {
    //                 fieldLabel: 'Subject Type',
    //                 xtype: 'displayfield',
    //                 labelAlign: 'right',
    //                 msgTarget: 'side',
    //                 itemId: 'subjectTypeCd',
    //             },
    //             {
    //                 fieldLabel: 'Lease',
    //                 xtype: 'displayfield',
    //                 labelAlign: 'right',
    //                 msgTarget: 'side',
    //                 itemId: 'leaseName',
                            
    //             },{
    //                 xtype: 'radiogroup',
    //                 fieldLabel: 'Confidential',
    //                 tabIndex: 1,
    //                 margin: '0 0 10',
    //                 labelAlign: 'right',
    //                 width: 150,
    //                 items: [{
    //                     name: 'securityCd',
    //                     boxLabel: 'Yes', 
    //                     inputValue: 2, 
    //                     margin: '0 10 0',
    //                 },{
    //                     name: 'securityCd',
    //                     boxLabel: 'No', 
    //                     inputValue: 1
    //                 }]
    //             },{
    //                 fieldLabel: 'Quick Notes',
    //                 selectOnFocus: true,
    //                 tabIndex: 2,
    //                 width: 750,
    //                 name : 'quickNoteCd',
    //                 xtype:'combobox',
    //                 itemId:'comboQuickNote',
    //                 margin: '10 0 10',
    //                 labelAlign: 'right',
    //                 emptyText : 'Select Quick Note',
    //                 valueField: 'quickNoteCd', 
    //                 displayField: 'quickNoteDesc',
    //                 store: 'Minerals.sub.MineralValuationManageLease.store.QuickNoteStore',
    //                 typeAhead: true,
    //                 queryMode:'local',
    //                 listeners:{
    //                     Select: function (record) {
    //                         me.down('#noteId').setValue(me.down('#comboQuickNote').getRawValue());
    //                         me.down('#noteId').focus();
    //                     },
    //                 }, 
    //                 // forceSelection: true,
    //                 allowBlank: true,
    //                 msgTarget: 'side'  
    //             },{
    //                 fieldLabel: 'Notes',
    //                 xtype: 'textareafield',
    //                 itemId: 'noteId',
    //                 tabIndex: 3,
    //                 width: 750,
    //                 height: 220,

    //                 name : 'note',
    //                 valueField: 'note', 
    //                 displayField: 'noteTypeCd',

    //                 resizable: true,
    //                 margin: '0 0 10',
    //                 labelAlign: 'right',
    //                 allowBlank: false,
    //                 queryMode:'local',
    //                 listeners:{
    //                     Select:function(record){ 
    //                         me.down('#quickNoteCd').setValue(me.down('#noteId').getRawValue());
    //                     },
    //                 },
    //                 msgTarget: 'side'
    //             }]
            
    //     }];
        
    //     this.callParent(arguments);
    // },

    // listeners:{
    //     afterrender: function(){
    //         var me = this;          
    //         var listGrid = Ext.getCmp(me.mainPanelId).down(me.listGridAlias);

    //         var mineralName = listGrid.down('#LeaseNoteMineralComboItemId').rawValue;
    //         me.down('#LeaseNoteItemId').setValue(mineralName);

    //         var mineralId = listGrid.down('#LeaseNoteMineralComboItemId').getValue();
    //         me.down('#mineralIdItemIdNote').setValue(mineralId);
    //     },
    // },
    // buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
    //     MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
    //     MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});