Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseAppraisalNote.LeaseAppraisalNoteView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.AppraisalNoteView',
    layout:'border',
    
    id: 'LeaseAppraisalNoteViewId',
    
    items: [{
            xtype: 'LeaseAppraisalNoteGridView',
            html:'center',
            region:'center'
        },{
        	xtype: 'LeaseAppraisalNoteDetailView',
        	region: 'east'
        }] 
});    