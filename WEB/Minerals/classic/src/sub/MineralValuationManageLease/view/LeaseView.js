Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseView' ,{
    extend: 'MineralPro.config.view.BaseTabPanelView',
    alias : 'widget.MineralValuationManageLease',
    
    requires: 'Minerals.sub.MineralValuationManageLease.view.LeaseViewController',
    controller: 'LeaseViewController',
    
    layout:'border',
    
    //used for tab change
    selectedId: '0',//Need default value '0'
    selectedIdIndex: 'leaseId',
    selectedName: '0',//Need default value '0'
    selectedNameIndex: 'leaseName',
    selectedRRCNumber: '-1',//Need default value '-1', '0' is used for LeaseNote
    selectedRRCNumberIndex: 'rrcNumber',
    
    subjectTypeCd: '2',
    
    noteTabIndex: '6',//this is the corresponding index for noteView. start at 0;
    appraisalTabIndex: '5',//this is the corresponding index for appraisalView. start at 0;
    
    id: 'MineralValuationManageLeaseId',
    listeners: {
        tabchange: 'onTabChangeOverride'
    },
    items: [{
            title: 'Leases',
            xtype: 'MineralsLeases',
        }, {           
            title: 'Tax Units',
            xtype: 'MineralsLeaseTaxUnits',
        }, {           
            title: 'Lease Owners',
            xtype: 'MineralsLeaseOwners',    
        }, {           
            title: 'Lease Notes',
            xtype: 'LeaseNoteView',    
        }, {
            title: 'RRC/Fields',
            xtype: 'LeaseFieldsView',
        }, {           
            title: 'Appraisal',
            xtype: 'LeaseAppraisalView',    
        }, {           
            title: 'Appraisal Notes',
            xtype: 'AppraisalNoteView',    
        }] 
});    