Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.LeaseNoteView',
    layout:'border',
    
    id: 'LeaseNoteViewId',
    
    items: [{
            xtype: 'LeaseNoteGridView',
            html:'center',
            region:'center'
        },{
        	xtype: 'LeaseNoteDetailView',
        	region: 'east'
        }] 
});    