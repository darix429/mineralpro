Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteEditFormView', {
    extend: 'Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteAddFormView',
    //requires: ['Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteEditFormViewController'],
    //controller: 'LeaseNoteEditFormViewController',
    alias: 'widget.LeaseNoteEditFormView',
    
    
    title: 'Update Lease Note',
    // defaultFocus: '#noteId',
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});