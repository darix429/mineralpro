
Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteDetailView', {
    extend: 'MineralPro.config.view.BaseDetailView',
    alias: 'widget.LeaseNoteDetailView',
    
    title: 'Note Details',
    itemId: 'LeaseNoteDetails',
    defaults: {
        width: 275,
        labelWidth: 93
    },   
    items: [{
        xtype: 'textareafield',
        fieldLabel:'Note',
        name: 'note',
        anchor: '100%',
        labelWidth: 80,
        grow: true, 
        readOnly: true 
    }]
});