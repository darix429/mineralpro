Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.config.view.Note.BaseNoteAddEditFormViewController'],
    controller: 'BaseNoteAddEditFormViewController',

    alias: 'widget.LeaseNoteAddFormView',

    title: 'Add Lease Notes',
    saveAction: 'onAddListToGrid',
//    defaultFocus: '#noteId',
    layout: 'fit',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'MineralValuationManageLeaseId', //id of the main panel
    listGridStore: 'Minerals.sub.MineralValuationManageLease.store.LeaseNoteStore', //store for note grid
    listGridAlias: 'LeaseNoteGridView', 
    bodyPadding: 5,
    width: 800,
    height: 520,

    subjectType: 'Lease Note',

    subjectTypeLabel: 'Lease Name',

    saveAction: 'onAddListToGrid',
    defaultFocus: '#noteId',
    layout: 'fit',

    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'form',
            defaults:{ maskRe: /[^'\^]/},
            margin:'0 5 0 0',
            bodyPadding: 5,
            items: [{
                    xtype: 'hidden',
                    name: 'subjectId',
                    itemId: 'subjectIdItemId'
                },{
                    xtype: 'hidden',
                    name: 'subjectTypeCd',
                    itemId: 'subjectTypeCdItemId'
                },{
                    fieldLabel: 'Subject Type',
                    xtype: 'displayfield',
                    labelAlign: 'right',
                    msgTarget: 'side',
                    itemId: 'subjectTypeItemId',
                },{
//                    fieldLabel: 'Lease',
                    xtype: 'displayfield',
                    labelAlign: 'right',
                    msgTarget: 'side',
                    itemId: 'subjectLabelItemId',
                },{
                    xtype: 'radiogroup',
                    fieldLabel: 'Confidential',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    width: 150,
                    items: [{
                        name: 'securityCd',
                        boxLabel: 'No',
                        tabIndex: 1, 
                        inputValue: 1, 
                        margin: '0 10 0',
                    },{
                        name: 'securityCd',
                        boxLabel: 'Yes', 
                        inputValue: 2
                    }]
                },{
                    fieldLabel: 'Notes Type',
                    selectOnFocus: true,
                    tabIndex: 2,
                    width: 750,
                    name : 'noteTypeCd',
                    xtype:'combobox',
                    itemId:'comboNoteType',
                    margin: '10 0 10',
                    labelAlign: 'right',
                    emptyText : 'Select Note Type',
                    valueField: 'noteTypeCd', 
                    displayField: 'noteTypeDesc',
                    store: 'Minerals.store.MineralValuation.NoteTypeStore',
                    typeAhead: true,
                    queryMode:'local',
                    forceSelection: true,
                    listeners:{
                        // Select: function (record) {
                        //     var note = me.down('#noteId');
                        //     note.setValue(note.getValue() + ' ' + this.getRawValue());
                        //     note.focus();
                        // },
                    }, 
                    // forceSelection: true,
                    //allowBlank: false,
                    msgTarget: 'side'  
                },{
                    fieldLabel: 'Quick Notes',
                    selectOnFocus: true,
                    tabIndex: 2,
                    width: 750,
                    name : 'quickNoteCd',
                    xtype:'combobox',
                    itemId:'comboQuickNote',
                    margin: '10 0 10',
                    labelAlign: 'right',
                    emptyText : 'Select Quick Note',
                    valueField: 'quickNoteCd', 
                    displayField: 'quickNoteDesc',
                    store: 'Minerals.store.MineralValuation.QuickNoteStore',
                    typeAhead: true,
                    queryMode:'local',
                    forceSelection: true,
                    listeners:{
                        Select: function (record) {
                            var note = me.down('#noteId');
                            note.setValue(note.getValue() + ' ' + this.getRawValue());
                            note.focus();
                        },
                    }, 
                    // forceSelection: true,
                    allowBlank: true,
                    msgTarget: 'side'  
                },{
                    fieldLabel: 'Notes',
                    xtype: 'textareafield',
                    name : 'note',
                    itemId: 'noteId',
                    tabIndex: 3,
                    width: 750,
                    height: 190, 
                    labelAlign: 'right',
                    allowBlank: false,
                }]
            
        }];
        
        this.callParent(arguments);
    },

    listeners:{
        afterrender: function(){
            var me = this;          
            var mainPanel = Ext.getCmp(me.mainPanelId)
            
            me.down('#noteId').focus();
            me.down('#subjectIdItemId').setValue(mainPanel.selectedId);
            me.down('#subjectTypeItemId').setValue(me.subjectType);
            me.down('#subjectLabelItemId').setFieldLabel(me.subjectTypeLabel);
            me.down('#subjectLabelItemId').setValue(mainPanel.selectedName);         
        },
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
        MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});