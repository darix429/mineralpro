Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseAppraisal.LeaseAppraisalFormView', {
    extend: 'Minerals.config.view.Appraisal.BaseAppraisalFormView',
    alias: 'widget.LeaseAppraisalFormView',
    
    mainPanelAlias: 'MineralValuationManageLease',
    mainPanelId: 'MineralValuationManageLeaseId',
    firstFocus: 'rrcNumberItemId',   
    
    appraisalStore: 'Minerals.sub.MineralValuationManageLease.store.LeaseAppraisalStore',
    
    calcAppraisalStore: 'Minerals.sub.MineralValuationManageLease.store.LeaseCalcAppraisalStore',
    
    fieldGridViewAlias: 'LeaseFieldsGridView',
    // listeners: {
    //     afterRender: function(thisForm, options){
    //         var me = this

    //         setTimeout(function (){
    //             var idx = 1;
    //             Ext.each(me.query('field:not(hiddenfield):not(displayfield)'),function(field){
    //                 if(field.tabIndex > 0 && field.tabIndex !== -1){
    //                 field.inputEl.dom.tabIndex = field.initialConfig.tabIndex;
    //                 }
    //             });
    //         },500)
    //     }
    // }
    
});    