Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseAppraisal.LeaseAppraisalView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.LeaseAppraisalView',
    layout:'border',
    
    id: 'LeaseAppraisalViewId',
    
    items: [{
            xtype: 'LeaseAppraisalFormView',
            region:'center'
        }] 
});    