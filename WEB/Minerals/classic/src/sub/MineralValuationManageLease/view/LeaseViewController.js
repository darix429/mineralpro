Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseViewController' ,{
    extend: 'MineralPro.config.view.BaseTabPanelViewController',
    alias : 'controller.LeaseViewController',
    layout:'border',
    onTabChangeOverride: function(tabPanel, newCard, oldCard, eOpts){
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseViewController onTabChangeOverride.') 
        var me = this;
        me.onTabChange(tabPanel);
        var mineralsLeases = tabPanel.down('MineralsLeases');
        var grid = mineralsLeases.down('grid');
        var selectedIdIndex = tabPanel.selectedIdIndex;
        var gridSelectionModel = grid.getSelectionModel();
        var selected = gridSelectionModel.getSelection();
        selected = selected.length ? selected[0] : '';
        gridSelectionModel.deselectAll();
        gridSelectionModel.select(selected);
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseViewController onTabChangeOverride.') 
    },
      onBeforeTabChange: function(tabPanel, newCard, oldCard, eOpts){       
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseViewController onBeforeTabChange.') 
             
        Ext.get(document.activeElement).blur();    
        var activeTab = oldCard;
        var saveData = false
        var saveStore = []   
        var arrayGrid = activeTab.query('grid');        
        var leaseGrid = tabPanel.down('grid');
        var leaseHeaderStore = Ext.StoreMgr.lookup(leaseGrid.headerInfoStore);
        var leaseRec = leaseHeaderStore.getAt(0) 

        var changeTab = true;
        var totalInterest = 0;
        var newIndex = '[' + tabPanel.items.indexOf(newCard).toString() + ']'        
        var withUnitIndex = '[4],[5],[6]' //index for RRC/Field, Appraisal, Notes
        
        var appraisal = false;
        var index = tabPanel.items.indexOf(activeTab);
               
        if(withUnitIndex.indexOf(newIndex) != -1){
            if(leaseRec){
                if(leaseRec.get('unitName') != null && leaseRec.get('unitName') != ''){
                    changeTab = false;
                    Ext.Msg.show({
                        iconCls: MineralPro.config.Runtime.listSyncMsgErrorIcon,
                        title: 'Alert',
                        msg: '<br>This lease is part of a Unit. Please use the Hyperlink in the Lease Details panel to go to the Unit for RRC/Fields and Appraisal information.<br><br>',
                        buttons: Ext.Msg.OK
                    });
                    return false;
                }
            }
        }
        /******************************************************************
        | New Appraisal Logic Starts Here
        |__________________________________________________________________
        */

        var leasesGrid = tabPanel.down('MineralsLeases grid');
        var leasesSelectionModel = leasesGrid.getSelectionModel();
        var leasesStore = leasesGrid.getStore();

        if(!leasesStore.getCount() || !leasesSelectionModel.getSelection().length){
            Ext.Msg.show({
                title: MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                msg: "Please select a lease.",
                iconCls: MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                buttons: Ext.Msg.OK,
                modal: true,
                scope: this,
                width: 250,
                fn: function (btn) {
                    if (btn === 'ok'){
                        tabPanel.setActiveTab(0);//activate the first tab
                    }
                }
            });
            return false;
        }
        // if(newCard.xtype == 'LeaseAppraisalView'){}
        
        /******************************************************************
        | New Appraisal Logic Ends Here
        |__________________________________________________________________
        */
        if(changeTab){    
            if(parseInt(tabPanel.appraisalTabIndex) == index || parseInt(tabPanel.appraisalDetailsTab) == index){
                if(activeTab.down('form').getRecord()){
                     var record = activeTab.down('form').getRecord();
                     var values = activeTab.down('form').getValues();       
                     record.set(values);
                }
                appraisal = true
            }
           
            if (activeTab.down('form')) {
                if (activeTab.down('form').newCalculation == true && !activeTab.down('#saveToDbButtonItemId').isHidden() && !activeTab.down('#saveToDbButtonItemId').isDisabled()) {
                    saveData = true;
                    //activeTab.down('#saveToDbButtonItemId').disable(true);
                    activeTab.down('form').newCalculation = false;

                } else {

                    for (var x = 0; x < arrayGrid.length; x++) {
                        var store = arrayGrid[x].getStore();
                        if (appraisal) {
                            if (store.getNewRecords().length > 0) {
                                var newRec = store.getNewRecords()[0]
                                var exclude = 'rrcNumber,fieldId,'
                                        + 'wellDescription,wellTypeCd,maxYearLife,comment,productionEquipmentCount,'
                                        + 'productionWellSchedule,productionEquipmentValue,serviceEquipmentCount,'
                                        + 'serviceWellSchedule,serviceEquipmentValue,injectionEquipmentCount,'
                                        + 'injectionWellSchedule,injectionEquipmentValue,'
                                        + 'discountRate,grossOilPrice,grossProductPrice,'
                                        + 'grossWorkingInterestGasPrice,grossRoyaltyInterestGasPrice,operatingExpense,'
                                        + 'dailyAverageOil,dailyAverageGas,dailyAverageProduct'
                                var fieldArray = exclude.split(',')
                                Ext.each(fieldArray, function (field) {
                                    if (newRec.get(field) != 0 && newRec.get(field) != ''
                                            && newRec.get(field) != '0') {
                                        saveStore.push(store)
                                        saveData = true
                                        if (activeTab.down('#saveToDbButtonItemId').isDisabled()
                                                || activeTab.down('#saveToDbButtonItemId').isHidden()) {
                                            saveData = false;
                                        }
                                        return false;
                                    }
                                })

                            } else if (store.getUpdatedRecords().length > 0) {
                                saveStore.push(store)
                                saveData = true
                                if (activeTab.down('#saveToDbButtonItemId').isDisabled()
                                        || activeTab.down('#saveToDbButtonItemId').isHidden()) {
                                    saveData = false;
                                }
                            }
                        } else {
                            if (store.getNewRecords().length > 0
                                    || store.getUpdatedRecords().length > 0
                                    || store.getRemovedRecords().length > 0) {
                                saveStore.push(store)
                                saveData = true
                            }
                        }

                        x = arrayGrid.length
                    }
                }
           }
            
           // for(var x=0; x<arrayGrid.length; x++){
           //     var store = arrayGrid[x].getStore();                 
           //         if(store.getNewRecords().length > 0 
           //             || store.getUpdatedRecords().length > 0 
           //             || store.getRemovedRecords().length > 0){
           //             saveStore.push(store)
           //             saveData = true                                            
           //         }                                
           //     x=arrayGrid.length
           // }
               
            if(saveData){
                var confirmed = false;
                var error = false;
                if(activeTab.xtype == 'MineralsLeaseOwners'){
                    for(var x=0; x<arrayGrid.length; x++){
                        var store = arrayGrid[x].getStore();
                        store.clearFilter(true);
                        Ext.each(store.data.items, function(el, index, items){
                            if(el.data.rowDeleteFlag == ''){
                                if(el.data.interestType == 'UNASSIGNED') {
                                    error = true;    
                                }
                                if(el.data.interestPercent > 1 || el.data.interestPercent == '-0') {
                                    error = true;    
                                }
                                totalInterest = parseFloat(totalInterest)+parseFloat(el.data.interestPercent);
                            }
                        })
                    }
                    if(parseFloat(totalInterest*1).toFixed(8) <= 1){
                        saveData = true;
                    } else {
                        saveData = false;
                    }
                }

                if(error){
                    Ext.Msg.show({
                        title:MineralPro.config.Runtime.tabChangeConfirmationTitle,    
                        msg:'There were Errors in the Grid, Your Data Will Not Be Saved ...<br /> Do you want to Proceed?',
                        iconCls:MineralPro.config.Runtime.tabChangeConfirmationIcon,
                        buttons: Ext.Msg.YESNO,               
                        scope: this,
                        width: 450,
                        fn: function(btn) { 
                            if (btn === 'yes'){
                                Ext.each(saveStore, function(store){                 
                                    store.removeAll();
                                    store.rejectChanges();                                
                                }); 
                                tabPanel.setActiveTab(newCard)
                            }
                        }
                    });

                } else {
                    Ext.Msg.show({
                        title:MineralPro.config.Runtime.tabChangeConfirmationTitle,    
                        msg:MineralPro.config.Runtime.tabChangeConfirmationMsg,
                        iconCls:MineralPro.config.Runtime.tabChangeConfirmationIcon,
                        buttons: Ext.Msg.YESNO,               
                        scope: this,
                        width: 250,
                        fn: function(btn) { 
                            if (btn === 'yes'){
                                if(saveData){
                                    if(parseFloat(totalInterest*1).toFixed(8)<1 && activeTab.xtype == 'MineralsLeaseOwners'){
                                        Ext.Msg.show({
                                            title:'Warning',
                                            msg:'Total interest percent is not equal to 100%. <br />Do you want to proceed?',
                                            iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                                            buttons: Ext.Msg.YESNO,
                                            scope: this,
                                            width: 300,
                                            fn: function (btn) {
                                                if (btn === 'yes') {
                                                    Ext.each(saveStore, function(store){    
                                                        var params = store.getProxy().extraParams
                                                            params['createLength'] = store.getNewRecords().length
                                                            params['updateLength'] = store.getUpdatedRecords().length
                                                        store.getProxy().extraParams = params
                                                        
                                                       // store.getProxy().extraParams = {
                                                       //     createLength: store.getNewRecords().length,
                                                       //     updateLength: store.getUpdatedRecords().length, 
                                                       // };    
                                                       // console.log(store)
                                                        var storeReload = false;
                                                        var reload = 'Owner,Unit,Lease,Appraisal'
                                                        var reloadArr = reload.split(',');
                                                        Ext.each(reloadArr, function(rel){
                                                            if(Ext.util.Format.lowercase(store.proxy.api.read).indexOf(Ext.util.Format.lowercase(rel))){
                                                                storeReload=true;
                                                                return false;
                                                            }
                                                        })
                                                            
                                                        store.sync({
                                                            callback: function(){
                                                                if(storeReload){
                                                                    store.reload();
                                                                }
                                                                if(activeTab.xtype == 'MineralsLeaseOwners'){
                                                                    var mainView = Ext.getCmp('MineralValuationManageLeaseId');
                                                                    var leaseStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseStore');
                                                                    var leaseId = mainView.selectedId;
                                                                    var leaseGridStore = mainView.down('LeasesGridView').getStore();
                                                                    var leaseGridSelect= mainView.down('LeasesGridView').getSelectionModel();
                                                                    var getSelectedLease = leaseStore.find('leaseId', leaseId, 0, false, false, true);
                                                                    leaseGridSelect.deselectAll();
                                                                    leaseGridSelect.select(getSelectedLease, false, false);
                                                                }
                                                            }
                                                        });  
                                                        store.commitChanges();
                                                    })
                                                    tabPanel.setActiveTab(newCard)
                                                }
                                            }
                                        });
                                    }
                                    else
                                    {
                                        Ext.each(saveStore, function(store){    
                                            var params = store.getProxy().extraParams
                                                params['createLength'] = store.getNewRecords().length
                                                params['updateLength'] = store.getUpdatedRecords().length
                                                if(appraisal){                                                                    
                                                    store.getAt(0).set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser); 
                                                    params['origRrcNumber'] = store.getAt(0).get('origRrcNumber');                                    
                                                    params['newRecord'] = activeTab.down('form').newAppraisal
                                                    params['newCalculation'] = activeTab.down('form').newCalculation
                                                    params['updateCalc'] = false;                                                                                                        
                                                }
                                            store.getProxy().extraParams = params
                                            
                                           // store.getProxy().extraParams = {
                                           //     createLength: store.getNewRecords().length,
                                           //     updateLength: store.getUpdatedRecords().length, 
                                           // };    
                                           // console.log(store)
                                            var storeReload = false;
                                            var reload = 'Owner,Unit,Lease,Appraisal'
                                            var reloadArr = reload.split(',');
                                            Ext.each(reloadArr, function(rel){
                                                if(Ext.util.Format.lowercase(store.proxy.api.read).indexOf(Ext.util.Format.lowercase(rel))){
                                                    storeReload=true;
                                                    return false;
                                                }
                                            })
                                                
                                            store.sync({
                                                callback: function(){
                                                    if(storeReload){
                                                        store.reload();
                                                    }
                                                    if(activeTab.xtype == 'MineralsLeaseOwners'){
                                                        var mainView = Ext.getCmp('MineralValuationManageLeaseId');
                                                        var leaseStore = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseStore');
                                                        var leaseId = mainView.selectedId;
                                                        var leaseGridStore = mainView.down('LeasesGridView').getStore();
                                                        var leaseGridSelect= mainView.down('LeasesGridView').getSelectionModel();
                                                        var getSelectedLease = leaseStore.find('leaseId', leaseId, 0, false, false, true);
                                                        leaseGridSelect.deselectAll();
                                                        leaseGridSelect.select(getSelectedLease, false, false);
                                                    }
                                                }
                                            });  
                                            store.commitChanges();
                                        })
                                        tabPanel.setActiveTab(newCard)
                                    }
                                }
                                else 
                                {
                                    Ext.Msg.show({
                                        iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                                        title: 'Lease Interest Pct Exeed',    
                                        msg: 'The total Lease Interest Pct have Exeeded 1.00000000',               
                                        buttons: Ext.Msg.OK
                                    });
                                }
                            }
                            else
                            {  
                                if(saveStore) 
                                {                                               
                                    Ext.each(saveStore, function(store){
                                        store.rejectChanges()
                                        if(parseInt(tabPanel.appraisalTabIndex) == index){
                                            if(store.count() > 0){
                                                activeTab.down('form').loadRecord(store.getAt(0));
                                            }
                                        }

                                        if(parseInt(tabPanel.appraisalDetailsTab) == index){
                                            if(store.count() > 0){
                                                activeTab.down('form').loadRecord(store.getAt(0));
                                            }
                                        }
                                    }); 
                                }
                                tabPanel.setActiveTab(newCard)

                                // Ext.each(saveStore, function(store){                 
                                //     store.removeAll();
                                //     store.rejectChanges();                                
                                // });
                                // // Ext.each(operation.getRecords(), function(record){
                                // //     record.reject();
                                // // }); 
                                // tabPanel.setActiveTab(newCard)
                            }
                        }
                    });
                }       
                return false;
            }
        }             
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseViewController onBeforeTabChange.')    
    },
    
});    