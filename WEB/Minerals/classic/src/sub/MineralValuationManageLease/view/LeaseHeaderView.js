Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseHeaderView' ,{
    extend: 'MineralPro.config.view.BaseHeaderView',
    alias : 'widget.MineralsLeaseHeader',
    layout: 'hbox',
    defaults: {
        style: {borderColor: '#000000', borderStyle: 'solid', borderWidth: '1px'},
        xtype: 'fieldset',
        layout: 'vbox',
        width: '24%',
        height: 95,
        margin: '0, 2, 0, 2',
        cls: 'header_fieldset',
    },
    items: [{
            title: 'Lease Information',
            width: '35%',
            defaults: {
                xtype: 'displayfield',
                fieldStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:5px',
                labelStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px',
                margin: '0 0 0 2',
                padding: '0 0 0 0',
                width: '100%',
            },
            items: [{
                    labelWidth: 85,
                    fieldLabel: 'Lease Name:',
                    name: 'leaseName',
                }, {
                    labelWidth: 85,
                    fieldLabel: 'Lease ID:',
                    name: 'leaseId',
                }, {
                    labelWidth: 85,
                    fieldLabel: 'Operator:',
                    name: 'operator',
                    renderer: function (val, meta) {
                        if(val.length>=38) {
                            return  val.substr(0,36)+'...'
                        } else {
                            return  val
                        }
                    },
                }, {
                    labelWidth: 155,
                    fieldLabel: 'Number of Lease Owners:',
                    name: 'TotalLeaseOwners',
                    // listeners : {
                    //     change : function () {
                    //         if(this.value<=1) {
                    //             this.setFieldLabel('Number of Lease Owner:');
                    //         } else {
                    //             this.setFieldLabel('Number of Lease Owners:')
                    //         }
                    //     }
                    // }
                }]
        }, {
            title: 'Values',
            width: '22%',
            defaults: {
                xtype: 'displayfield',
                fieldStyle: 'text-align:right;font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:5px',
                labelStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px',
                margin: '0 7 0 2',
                padding: '0 0 0 0',
                width: '100%',
                labelWidth: 145,
                renderer: function (val, meta) {
                    return  Ext.util.Format.number(val, '$000,000');
                },
            },
            items: [{
                    fieldLabel: 'Royalty Interest Value:',
                    name: 'totalRoyaltyValue',
                }, {
                    fieldLabel: 'Working Interest Value:',
                    name: 'totalWorkingValue',
                }, {
                    fieldLabel: 'Total Lease Value:',
                    name: 'appraisedValue',
                }]
        }, {
            title: 'Interest',
            width: '42%',
            layout: 'hbox',
            defaults: {
                xtype: 'container',
                layout: 'vbox',
//                width: '50%',
            },
            items: [{
                    width: '47%',
                    defaults: {
                        xtype: 'displayfield',
                        fieldStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:5px',
                        labelStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px',
                        margin: '0 0 0 2',
                        padding: '0 0 0 0',
                        width: '100%',
                        labelWidth: 110,
                    },
                    items: [{
                        fieldLabel: 'Royalty Interest:',                   
                        name: 'royaltyInterest',   
                    },{
                        fieldLabel: 'Overiding Royalty',
                        name: 'overidingRoyalty',  
                    },{
                        fieldLabel: 'Oil Payment',
                        name: 'oilPayment',  
                    }]
                }, {
                    width: '53%',
                    defaults: {
                        xtype: 'displayfield',
                        fieldStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:5px',
                        labelStyle: 'font-weight:normal;margin:0px;padding:0px 0px 0px 20px;font-size:12px',
                        margin: '0 0 0 2',
                        padding: '0 0 0 0',
                        width: '100%',
                        labelWidth: 150,
                    },
                    items: [{
                        fieldLabel: 'Total Royalty Interest',
                        name: 'totalRoyaltyInterest',
                        renderer: function (val, meta) {
                            return  Ext.util.Format.number(val, '0.000000');
                        },    
                    },{
                        fieldLabel: 'Total Working Interest',
                        name: 'totalWorkingInterest',
                        renderer: function (val, meta) {
                            return  Ext.util.Format.number(val, '0.000000');
                        },                              
                    },{
                        fieldLabel: 'Total Interest Percent',
                        name: 'totalInterestPercent',
                        listeners: {
                            change: function(record) {
                                if(record.value>1){
                                     this.labelStyle = 'background:url(../resources/icons/tp_lower.png) no-repeat left top;font-weight:normal;margin:0px;padding:0px 0px 0px 20px;font-size:12px;min-height:5px';
                                }else if(record.value<1){
                                     this.labelStyle = 'background:url(../resources/icons/tp_upper.png) no-repeat left top;font-weight:normal;margin:0px;padding:0px 0px 0px 20px;font-size:12px;min-height:5px';
                                }else{
                                     this.labelStyle = 'background:url(../resources/icons/tp_good.png) no-repeat left top;font-weight:normal;margin:0px;padding:0px 0px 0px 20px;font-size:12px;min-height:5px';
                                }
                            }  
                        }                            
                    }]
                }]
        }]  
//------------------------------------------------------------------------------------  
});    
