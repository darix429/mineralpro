
Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseTaxUnits.LeaseTaxUnitsGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.LeaseTaxUnitsGridView',
    store:'Minerals.sub.MineralValuationManageLease.store.LeaseTaxUnitStore',

    requires: ['Minerals.sub.MineralValuationManageLease.view.LeaseTaxUnits.LeaseTaxUnitsGridViewController'],
    controller: 'LeaseTaxUnitsGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'taxUnitTypeId', 
    mainPanelAlias: 'MineralValuationManageLease', //widget alias name of the main panel
    mainListModel: 'Minerals.sub.MineralValuationManageLease.model.LeaseTaxUnitModel',     

    addFormAliasName: 'LeaseTaxUnitsAddFormView', //widget form to call for add list 
    editFormAliasName: 'LeaseTaxUnitsEditFormView', //widget form to call for edit

    //headerInfo: 'Minerals.sub.MineralValuationManageLease.view.OwnerHeaderView',
 
    firstFocus: 'taxUnitTypeId',
    
    addButtonTooltip: 'Add Tax Unit',
    addButtonText: 'Add Tax Unit',
        
    columns:[{ 
        header: 'Tax Unit Type',
        flex     : 1,
        dataIndex: 'taxUnitType',
        itemId: 'taxUnitTypeId',
        filterElement: new Ext.form.TextField()
    },{   
        header: 'Tax Unit Name',
        flex     : 1,
        dataIndex: 'taxUnitName',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Tax Unit Value (%)',
        flex     : 1,
        dataIndex: 'taxUnitPercent',
        filterElement:new Ext.form.TextField(),
        renderer: function(val, meta) {
            return Ext.util.Format.number(val, '0.000');
        },   
     },{
      xtype: 'actioncolumn',
         width: Minerals.config.Runtime.actionColWidth,
         items: [
             MineralPro.config.util.MainGridActionColCmpt.editCls,
             MineralPro.config.util.MainGridActionColCmpt.deleteCls,
         ],
         listeners: {
             afterrender: function(){
                 var me = this       
                 me.setHidden(MineralPro.config.Runtime.access_level)
             }
         }
     }],
 
    initComponent: function () {  
        var me = this;
               
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],       
               
        me.tools= [
        MineralPro.config.util.MainGridToolCmpt.addNewDataButton
        ,MineralPro.config.util.MainGridToolCmpt.saveToDbButton                   
        ,MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
        me.callParent(arguments);
        
    },
    listeners: {
        selectionchange: 'onLoadSelectionChange',
        // afterlayout: 'onAfterLayout',
    }
});