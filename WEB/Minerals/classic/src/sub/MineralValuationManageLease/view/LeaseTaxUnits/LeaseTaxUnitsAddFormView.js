Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseTaxUnits.LeaseTaxUnitsAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.sub.MineralValuationManageLease.view.LeaseTaxUnits.LeaseTaxUnitsAddFormViewController'],
    controller: 'LeaseTaxUnitsAddFormViewController',
    alias: 'widget.LeaseTaxUnitsAddFormView',

    title: 'Add Tax Unit',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#taxUnitTypeCdId',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'MineralValuationManageLeaseId', //id of the main panel
    listGridStore: 'Minerals.sub.MineralValuationManageLease.store.LeaseTaxUnitStore',
    listGridAlias: 'LeaseTaxUnitsGridView',
    layout: 'fit',
    bodyPadding: 5,
    
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'form',
            defaults:{ 
                labelWidth: 130,
                width: 390,
                maskRe: /[^'\^]/ 
            },
            layout: 'vbox',
            bodyPadding: 5,
            items:[{
                fieldLabel: 'Lease Name',
                xtype: 'displayfield',
                labelAlign: 'right',
                msgTarget: 'side',
                margin: '0 0 0',
                itemId: 'leaseName',
            },{
                fieldLabel: 'leaseId',
                name: 'leaseId',
                xtype: 'hidden',
                itemId:'leaseNameId',
                labelAlign: 'right',    
            },{
                fieldLabel: 'Tax Unit Type',
                selectOnFocus: true,
                tabIndex: 1,
                name : 'taxUnitTypeCd',
                xtype:'combo',
                itemId:'taxUnitTypeCdId',
                margin: '0 0 10',
                labelAlign: 'right',
                displayField: 'taxUnitType',
                valueField: 'taxUnitTypeCd',   
                store: 'Minerals.sub.MineralValuationManageLease.store.LeaseTaxUnitTypeStore',
                typeAhead: true,
                forceSelection: true,
                allowBlank: false,
                queryMode:'local',
                value: 'UNASSIGNED',
                listeners:{
                    change:function(fieldLabel, newval, oldval){
                        var st = Ext.getStore('Minerals.sub.MineralValuationManageLease.store.LeaseTaxUnitCodeStore');
                        if(!isNaN(newval*1)){
                            st.load({
                                params: {
                                    taxUnitTypeCd: newval
                                },
                                callback: function (records, operation, success) {
                                    me.down('#taxUnitCdId').select(0);
                                }
                            });
                            me.down('#idTaxUnitType').setValue(me.down('#taxUnitTypeCdId').getRawValue());
                        }
                    }
                } 
            },{       
                xtype: 'hidden',     
                name: 'taxUnitType',   
                queryMode:'local',
                itemId:'idTaxUnitType' 
            },{
                fieldLabel: 'Tax Unit Name',
                selectOnFocus: true,
                tabIndex: 2,
                name : 'taxUnitCd',
                xtype:'combo',
                itemId:'taxUnitCdId',
                margin: '0 0 10',
                labelAlign: 'right',
                displayField: 'taxUnitName',
                valueField: 'taxUnitCd',   
                store: 'Minerals.sub.MineralValuationManageLease.store.LeaseTaxUnitCodeStore',
                typeAhead: true,
                forceSelection: true,
                allowBlank: false,
                emptyText: 'UNASSIGNED',
                queryMode:'local',
                listeners:{
                    select: 'onAddCheckExisting',
                    change:function(record, a, b){
                         // var preloadValue = record.getStore().data.items[a].data.estimatedTaxRate;
                        //   if(!preloadValue){
                        //         preloadValue = '1.00000000';
                        //   }
                        //  me.down('#taxUnitPercentId').setValue(preloadValue);
                        if(!isNaN(a*1)){
                            me.down('#idTaxUnitName').setValue(me.down('#taxUnitCdId').getRawValue());
                        }
                    }
                } 
            },{       
                xtype: 'hidden',     
                name: 'taxUnitName',   
                queryMode:'local',
                itemId:'idTaxUnitName' 
            },{
                fieldLabel: 'Tax Unit Value(%)',
                itemId: 'taxUnitPercentId',
                selectOnFocus: true,
                tabIndex: 3,
                maskRe:/[0-9.]/,
                name: 'taxUnitPercent',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                msgTarget: 'side',
                maxLength: 5,
                allowBlank: false,
                enableKeyEvents: true,
                validator: function(v) {
                        if (parseFloat(v) >= 10) {                            
                            return "Must be a decimal or whole number and less than 10.";
                        }
                        return true;
                },
                listeners: {
                    initialize: function(){
                        var nfield = Ext.get('weightfield');
                        nfield.down('input').set({pattern:'[0-9]*'});
                    }
                }
                    
            }]
            
        }];
        me.listeners = {
            afterrender: function(){
                var me = this;          
                var mainView = Ext.getCmp(me.mainPanelId);
                var LeaseName = mainView.selectedName;
                var leaseNameId = mainView.selectedId;

                me.down('#leaseName').setValue(LeaseName);
                me.down('#leaseNameId').setValue(leaseNameId);

                setTimeout(function () {
                    me.down('#idTaxUnitType').setValue(me.down('#taxUnitTypeCdId').getRawValue());
                    me.down('#idTaxUnitName').setValue(me.down('#taxUnitCdId').getRawValue()); 
                }, 100);
            },
        }

        me.callParent(arguments);

    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
        MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});
