//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseTaxUnits.LeaseTaxUnitsAddFormViewController', {
    extend: 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.LeaseTaxUnitsAddFormViewController',
    onAddAndNew: function (button) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseTaxUnitsAddFormViewController onAddAndNew.')
        var me = this;
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore);
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);

        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();
        var leaseId = values.leaseId;

        var taxUnitTypeCdId = view.down('#taxUnitTypeCdId');
        var taxUnitCdId = view.down('#taxUnitCdId');

        if (taxUnitTypeCdId.getRawValue() != 'UNASSIGNED') {
            if (taxUnitCdId.getRawValue() != 'UNASSIGNED') {
                if(form.isValid()){
                    record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                    record.set(values);
                    listStore.insert(0, record);

                    //form.reset()
                    var new_record = Ext.create(listGrid.mainListModel)
                    new_record.set('leaseId', leaseId);
                    form.loadRecord(new_record);
                    view.focus(view.defaultFocus);
                    //listGrid.fireEvent('afterlayout');
                }else{
                    Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                    msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                    buttons: Ext.Msg.OK
                    });
                }
            } else {
                taxUnitCdId.focus()
                me.showAddEditAlert()
            }
        } else {
            taxUnitTypeCdId.focus()
            me.showAddEditAlert()
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseTaxUnitsAddFormViewController onAddAndNew.')
    },
    onAddListToGrid: function (button) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseTaxUnitsAddFormViewController onAddListToGrid.')
        var me = this;
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore);
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();

        var taxUnitTypeCdId = view.down('#taxUnitTypeCdId');
        var taxUnitCdId = view.down('#taxUnitCdId');

        if (taxUnitTypeCdId.getRawValue() != 'UNASSIGNED') {
            if (taxUnitCdId.getRawValue() != 'UNASSIGNED') {
                if(form.isValid()){
                    record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                    record.set(values);
                    win.close();
                    listStore.insert(0, record);

                    listGrid.getSelectionModel().select(0, true);
                    listGrid.getView().focusRow(0);
                }else{
                    Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                    msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                    buttons: Ext.Msg.OK
                    });
                }
            } else {
                taxUnitCdId.focus()
                me.showAddEditAlert()
            }
        } else {
            taxUnitTypeCdId.focus()
            me.showAddEditAlert()
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseTaxUnitsAddFormViewController onAddListToGrid.')
    },
    onEditListToGrid: function (button) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAddEditFormViewController onEditListToGrid.')

        var me = this;
        var view = me.getView();
        var mainPanel = Ext.getCmp(view.mainPanelId);
        var listGrid = mainPanel.down(view.listGridAlias)

        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();

        var taxUnitTypeCdId = view.down('#taxUnitTypeCdId');
        var taxUnitCdId = view.down('#taxUnitCdId');

        if (taxUnitTypeCdId.getRawValue() != 'UNASSIGNED') {
            if (taxUnitCdId.getRawValue() != 'UNASSIGNED') {
                if(form.isValid()){
                record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                    record.set(values);

                    if (listGrid.withDetailsView) {
                        var detailView = mainPanel.down(listGrid.detailAliasName);
                        detailView.loadRecord(record);
                    }
                    win.close();
                }else{
                    Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                    msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                    buttons: Ext.Msg.OK
                    });
                }
                
            } else {
                taxUnitCdId.focus()
                me.showAddEditAlert()
            }
        } else {
            taxUnitTypeCdId.focus()
            me.showAddEditAlert()
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAddEditFormViewController onEditListToGrid.')
    },
    onAddCheckExisting: function (e, event, opt) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering LeaseTaxUnitsAddFormViewController onAddCheckExisting.')

        var me = this;
        var view = me.getView();
        var interestType = view.down('#taxUnitTypeCdId')
        var taxUnitCd = view.down('#taxUnitCdId');
        var showAlert = false;
        setTimeout(function () {
            var listStore = Ext.StoreMgr.lookup(view.listGridStore);
            listStore.each(function (record) {
                if (record.get(interestType.name) == interestType.value) {
                    if (record.get(taxUnitCd.name) == taxUnitCd.value) {
                        showAlert = true;
                        if (view.alias[0].indexOf('Edit') != -1) {
                            if (listStore.getAt(view.editRecordIndex).get(taxUnitCd.name) == taxUnitCd.value) {
                                showAlert = false;
                            }
                        }
                    }
                }
            })

            if (showAlert) {
                Ext.Msg.show({
                    iconCls: MineralPro.config.Runtime.addExistingDataWarningCls,
                    title: MineralPro.config.Runtime.addExistingDataWarningTitle,
                    msg: 'Tax Unit name is already in used.',
                    buttons: Ext.Msg.OK,
                    fn: function () {
                        e.focus();
                        view.down('#taxUnitCdId').select(0);
                    }
                });
            }

        }, 200)
        MineralPro.config.RuntimeUtility.DebugLog('Leaving LeaseTaxUnitsAddFormViewController onAddCheckExisting.')
    },
    
    showAddEditAlert: function (){
        Ext.Msg.show({
            iconCls: Minerals.config.Runtime.addEditTaxUnitMsgErrorIcon,
            title: Minerals.config.Runtime.addEditTaxUnitMsgErrorTitle,
            msg: Minerals.config.Runtime.addEditTaxUnitMsgErrorMsg,
            buttons: Ext.Msg.OK
        });
    },
});