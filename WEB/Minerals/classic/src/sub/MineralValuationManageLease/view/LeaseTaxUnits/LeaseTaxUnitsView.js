Ext.define('Minerals.sub.MineralValuationManageLease.view.LeaseTaxUnits.LeaseTaxUnitsView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.MineralsLeaseTaxUnits',
    layout:'border',
    
    id: 'MineralsLeaseTaxUnitsId',
    
    items: [{
            //title: 'LeaseTaxUnits',
            xtype: 'LeaseTaxUnitsGridView',
            html:'center',
            region:'center'
        }] 
});    