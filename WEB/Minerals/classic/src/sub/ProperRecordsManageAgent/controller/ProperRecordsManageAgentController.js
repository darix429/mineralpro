///**
// * Handles the controller processing for ProperRecordsManageAgent
// */

Ext.define('Minerals.sub.ProperRecordsManageAgent.controller.ProperRecordsManageAgentController', {
    extend: 'Ext.app.Controller',
  
    models: [
        'Minerals.sub.ProperRecordsManageAgent.model.ManageAgentModel'
    ],
    
    stores: [
        'Minerals.sub.ProperRecordsManageAgent.store.ManageAgentStore',
        'Minerals.sub.ProperRecordsManageAgent.store.StatusCdStore'
    ],  
    
    views: [
        'Minerals.sub.ProperRecordsManageAgent.view.ManageAgentView',
        'Minerals.sub.ProperRecordsManageAgent.view.ManageAgentGridView',
        'Minerals.sub.ProperRecordsManageAgent.view.ManageAgentDetailView',
        'Minerals.sub.ProperRecordsManageAgent.view.ManageAgentAddFormView',
        'Minerals.sub.ProperRecordsManageAgent.view.ManageAgentEditFormView',
    ],          
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden']
});


