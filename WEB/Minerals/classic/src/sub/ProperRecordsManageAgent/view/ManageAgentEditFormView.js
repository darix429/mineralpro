Ext.define('Minerals.sub.ProperRecordsManageAgent.view.ManageAgentEditFormView', {
    extend: 'Minerals.sub.ProperRecordsManageAgent.view.ManageAgentAddFormView',
    //requires: ['Minerals.sub.ProperRecordsManageAgent.view.ManageAgentEditFormViewController'],
    //controller: 'ManageAgentEditFormViewController',
    alias: 'widget.ManageAgentEditFormView',
    
    title: 'Edit Agent',
    defaultFocus: '#agentNameId',
    listeners: {
        afterrender: function(form){
            var me = form;
            var codeId = form.down('#agencyCodeId');
            if(codeId){
                codeId.setVisible(true);
            }
        }
    },
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});
