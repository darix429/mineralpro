Ext.define('Minerals.sub.ProperRecordsManageAgent.view.ManageAgentView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.ProperRecordsManageAgent',
    layout:'border',
    
    id: 'ProperRecordsManageAgentId',
    
    items: [{
                region: 'center',  
                xtype:'ManageAgentGridView'
            },{
                region: 'east', 
                xtype:'ManageAgentDetailView'
    },] 
});    