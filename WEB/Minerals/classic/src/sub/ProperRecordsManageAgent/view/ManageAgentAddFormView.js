Ext.define('Minerals.sub.ProperRecordsManageAgent.view.ManageAgentAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.sub.ProperRecordsManageAgent.view.ManageAgentAddFormViewController'],
    controller: 'ManageAgentAddFormViewController',
    alias: 'widget.ManageAgentAddFormView',

    title: 'Add New Agent',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#agentNameId',
    layout: 'fit',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'ProperRecordsManageAgentId', //id of the main panel
    listGridStore: 'Minerals.sub.ProperRecordsManageAgent.store.ManageAgentStore', //store for note grid
    listGridAlias: 'ManageAgentGridView', 
    bodyPadding: 5,
    
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'form',
            defaults:{ maskRe: /[^'\^]/ },
            layout: 'hbox',
            bodyPadding: 5,
            items:[{
                xtype: 'fieldset',
                defaults:{ maskRe: /[^'\^]/ },
                flex: 1,
                margin:'0 5 0 0',
                title: 'Agent Information',
                autoheight: true, 
                items: [{
                    fieldLabel: 'Agent Code',
                    xtype: 'numberfield',
                    itemId: 'agencyCodeId',
                    hidden: true,
                    //selectOnFocus: true,
                    // tabIndex: 1,
                    // minValue: '0',
                    // maxLength: 3,
                    editable: false,
                    disabled: true,
                    name : 'agencyCdx',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    allowBlank: false,
                    msgTarget: 'side',
                    listeners: {
                        blur: 'onAddCheckExisting'                    
                    }
                },
                {
                    fieldLabel: 'Agent Name',
                    xtype: 'textfield',
                    itemId: 'agentNameId',
                    selectOnFocus: true,
                    tabIndex: 1,
                    name : 'agencyName',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    allowBlank: false,
                    msgTarget: 'side',
                    maxLength: 50,
                    
                },{
                    fieldLabel: 'Status',
                    xtype: 'combo',
                    selectOnFocus: true,
                    tabIndex: 2,
                    itemId: 'StatusItemId',
                    displayField: 'description',
                    valueField: 'code',
                    name: 'statusCd',
                    typeAhead: true,
                    store: 'Minerals.sub.ProperRecordsManageAgent.store.StatusCdStore',
                    forceSelection: true,
                    allowBlank: false,
                    labelAlign: 'right',
                    msgTarget: 'side',
                    queryMode: 'local',
                    listeners:{
                        select:function(record){
                            me.down('#hiddenStatusItemId').setValue(me.down('#StatusItemId').getRawValue());
                        },   
                        // beforequery: function(queryVV){
                        //     queryVV.combo.expand();
                        //     queryVV.combo.store.load();
                        //     return false;
                        // }
                    },        
                },{
                fieldLabel: 'Hidden Status',
                queryMode:'local',
                name: 'description',
                xtype: 'hidden',
                readOnly:true,
                margin: '0 0 10',
                itemId:'hiddenStatusItemId',
                labelAlign: 'right',   
                    
//                },{
//                    xtype: 'fieldcontainer',
//                    fieldLabel: 'Status',
//                    defaultType: 'checkboxfield',
//                    margin: '0 0 10',
//                    labelAlign: 'right',
//                    items: [{
//                            boxLabel  : 'Active',
//                            name      : 'statusCd',
//                            inputValue: '0',
//                            uncheckedValue: '14',
//                            id        : 'checkbox1'
//                        }]
                }]
            },{
                xtype: 'fieldset',
                flex: 1,
                margin:'0 0 5 0',
                title: 'Agent Address',
                autoheight: true,
                defaults:{
                    width:350,
                    maskRe: /[^'\^]/ 
                }, 
                items: [{
                    fieldLabel: 'Address Line 1',
                    itemId: 'addrLine1',
					selectOnFocus: true,
                    tabIndex: 3,
                    name: 'addrLine1',
                    xtype: 'textfield',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    //validateBlank: true,
                    //allowBlank: false,
                    msgTarget: 'side',
                    enableKeyEvents: true,
                    maxLength: 50,
                    listeners: {
                        blur: 'onCheckAddress'                    
                    }
                },{
                    fieldLabel: 'Address Line 2',
                    itemId: 'addrLine2',
					//selectOnFocus: true,
                    tabIndex: 4,
                    name: 'addrLine2',
                    xtype: 'textfield',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    msgTarget: 'side',
                    enableKeyEvents: true,
                    maxLength: 50,
                    listeners: {
                        blur: 'onCheckAddress'                    
                    }
                },{
                    fieldLabel: 'Address Line 3',
                    itemId: 'addrLine3',
					//selectOnFocus: true,
                    tabIndex: 5,
                    name: 'addrLine3',
                    xtype: 'textfield',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    msgTarget: 'side',
                    enableKeyEvents: true,
                    maxLength: 50,
                    //listeners: {
                    //    blur: 'onCheckAddress'                    
                    //}
                },{
                    fieldLabel: 'City',
					selectOnFocus: true,
                    tabIndex: 6,
                    name: 'city',
                    xtype: 'textfield',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    //validateBlank: true,
                    //allowBlank: false,
                    msgTarget: 'side',
                    maxLength: 35,
                },{
                    fieldLabel: 'State',
					selectOnFocus: true,
                    tabIndex: 7,
                    name : 'stateCd',
                    xtype:'combo',
                    itemId:'comboState',
                    margin: '0 0 10',
                    selectOnTab:false,
                    labelAlign: 'right',
                    displayField: 'stateName',
                    valueField: 'stateCd',   
                    store: 'MineralPro.store.StateStore',
                    typeAhead: true,
                    forceSelection: true,
                    queryMode:'local', 
                    enableKeyEvents: true,
                    listeners:{
                        Select:function(record){ 
                            me.down('#idstate').setValue(me.down('#comboState').getRawValue());
                        } ,
                        keyup: function(data, e, eOpts){
                               if(data.getRawValue() == ''){
                                  me.down('#idstate').setValue(''); 
                               }
                        },
                    }, 
                    //forceSelection: true,
                    allowBlank: true,
                    msgTarget: 'side'     
                },{       
                    xtype: 'hidden',     
                    name: 'stateName',   
                    queryMode:'local',
                    itemId:'idstate' 
                },{
                    fieldLabel: 'Zip Code',
					selectOnFocus: true,
                    tabIndex: 8,
                    name: 'zipcode',
                    xtype: 'textfield',
                    margin: '0 0 10',
                    labelAlign: 'right',
					maxLength:9,
                    minLength:5,
                    maskRe:/[0-9.]/,
                    msgTarget: 'side',
                    enableKeyEvents: true,
                },{
                    fieldLabel: 'Country',
					selectOnFocus: true,
                    tabIndex: 9,
                    name : 'countryCd',
                    xtype:'combo',
                    itemId:'comboCountry',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    displayField: 'countryName',
                    valueField: 'countryCd',   
                    store: 'MineralPro.store.CountryStore',
                    typeAhead: true,
                    queryMode:'local', 
                    listeners:{
                        Select:function(record){ 
                            me.down('#idcountry').setValue(me.down('#comboCountry').getRawValue());
                        },                       
                    }, 
                    forceSelection: true,
                    allowBlank: false,
                    msgTarget: 'side'     
                },{      
                    xtype: 'hidden',      
                    name: 'countryName',   
                    queryMode:'local',
                    itemId:'idcountry' 
                },]  
            },{
                xtype: 'fieldset',
                defaults:{ maskRe: /[^'\^]/ },
                flex: 1,
                margin:'0 0 0 5',
                title: 'Email & Phone',
                autoheight: true, 
                items: [{
                    fieldLabel: 'Email Address',
					selectOnFocus: true,
                    tabIndex: 10,					
                    name: 'emailAddress',
                    xtype: 'textfield',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    //validateBlank: true,
                    //allowBlank: false,
                    msgTarget: 'side',
                    vtype:'email',
                    maxLength: 50,
                },{
                    fieldLabel: 'Phone Number',
                    selectOnFocus: true,
                    tabIndex: 11,
                    name: 'phoneNum',
                    xtype: 'textfield',
                    margin: '0 0 10',
                    minValue: '0',
                    labelAlign: 'right',
                    maxLength: 10,
                    maskRe:/[0-9.]/,
                    //validateBlank: true,
                    //allowBlank: false,
                    msgTarget: 'side',
//                    vtype:'phone',
//                    enableKeyEvents: true,
//                    listeners: {
//                        keypress: 'onCheckNumber'                  
//                    }
                },{
                    fieldLabel: 'Fax Number',
                    selectOnFocus: true,
                    tabIndex: 12,
                    name: 'faxNum',
                    xtype: 'textfield',
                    margin: '0 0 10',
                    minValue: '0',
                    labelAlign: 'right',
                    maskRe:/[0-9.]/,
                    //validateBlank: true,
                    //allowBlank: false,
                    msgTarget: 'side',
//                    vtype:'phone',
//                    enableKeyEvents: true,
                    maxLength: 10,
//                    listeners: {
//                        keypress: 'onCheckNumber'                  
//                    }
                }]  
            }]
            
        }];

        me.listeners = {
            afterrender: function(){
                setTimeout(function () {
                    me.down('#hiddenStatusItemId').setValue(me.down('#StatusItemId').getRawValue());
                }, 100);               
            }
        }

        this.callParent(arguments);
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
        MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});