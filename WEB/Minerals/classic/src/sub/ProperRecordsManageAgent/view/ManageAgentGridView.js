
Ext.define('Minerals.sub.ProperRecordsManageAgent.view.ManageAgentGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.ManageAgentGridView',
    store:'Minerals.sub.ProperRecordsManageAgent.store.ManageAgentStore',

    requires: ['Minerals.sub.ProperRecordsManageAgent.view.ManageAgentGridViewController'],
    controller: 'ManageAgentGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'agencyCdxId', 
    mainPanelAlias: 'ProperRecordsManageAgent', //widget alias name of the main panel
    mainListModel: 'Minerals.sub.ProperRecordsManageAgent.model.ManageAgentModel',   
      
    detailAliasName: 'ManageAgentDetailView',
    withDetailsView: true, //true if it has details view

    addFormAliasName: 'ManageAgentAddFormView', //widget form to call for add list 
    editFormAliasName: 'ManageAgentEditFormView', //widget form to call for edit
    
    affectedComboStore: ['Minerals.store.ProperRecords.AgencyStore'],//combos store for agent

    firstFocus: 'agencyCdxId',    
    
    addButtonTooltip: 'Add New Agent',
    addButtonText: 'Add New Agent',
        
    initComponent: function () {  
        var me = this;
        var deleteCls= {
                getClass: function (colConfig, cellData, record, rowIndex, colIndex, dataStore) {
                    if (record.get('rowDeleteFlag') == 'D') {
                        return(MineralPro.config.Runtime.undoDeleteCls);
                    } else {
                        return(MineralPro.config.Runtime.deleteCls);
                    }
                },
                getTip: function (colConfig, cellData, record, rowIndex, colIndex, dataStore) {
                    if (record.get('rowDeleteFlag') == 'D') {
                        return (MineralPro.config.Runtime.undoDeleteTooltip);
                    } else {
                        return (MineralPro.config.Runtime.deleteTooltip);
                    }
                },
                handler: function (gridView, rowIndex, colIndex, clickedItem, eventObj, record, row) {
                    this.up('grid').getSelectionModel().select(rowIndex)                   
                    if (record.get('rowDeleteFlag') == 'D') {
                        record.set('rowDeleteFlag', record.previousValues.rowDeleteFlag);                                      
                    } else {
                        if(record.get('enableDeleteCount') != 0 && record.get('enableDeleteCount') != undefined){
                            var owner = 'Owners'
                            if(record.get('enableDeleteCount') == 1){
                                owner = 'Owner'
                            }
                            Ext.Msg.show({
                                title: 'Alert',
                                msg:'Agent is assigned to '+record.get('enableDeleteCount')+' '+owner+', are you sure you want to delete the Agent',
//                                iconCls:'',
                                buttons: Ext.Msg.YESNO,
                                scope: this,
                                width: 250,
                                fn: function (btn) {
                                    if (btn === 'yes') {
                                        record.set('rowDeleteFlag', 'D');
                                        record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser );
                                    }
                                }
                            });
                        }else{
                            record.set('rowDeleteFlag', 'D'); 
                            record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser );
                        }                                  
                    }
                },            
            }   
        
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],       
                            
        me.columns=[{   
            header: 'Agent Code',
            flex     : 1,
            dataIndex: 'agencyCdx',
            itemId: 'agencyCdxId',
            filterElement: new Ext.form.TextField()   
         },{   
            header: 'Agent Name',
            flex     : 2,
            dataIndex: 'agencyName',
            filterElement:new Ext.form.TextField()     
        },{   
            header: 'Status',
            flex     : 1,
            dataIndex: 'description',
            filterElement : new Ext.form.ComboBox({                              
                showFilterIcon:true,
                triggerAction           : 'all',                
                typeAhead               : true,                                
                mode                    : 'local',
                listWidth               : 160,
                hideTrigger             : false,
                emptyText               : 'Select',
                store                   : Ext.create('Minerals.sub.ProperRecordsManageAgent.store.StatusCdStore'),
                listeners: {
                    beforerender: function(combo) {
                        combo.store.load({
                            callback: function(records, operation, success) {
                                var type = [];
                                for(i = 0;i < records.length;i++){
                                    var a = [];
                                    a.push(records[i].data.description)
                                    a.push(records[i].data.description);
                                    type.push(a);
                                }
                                combo.bindStore(type);
                                var recordSelected = combo.getStore().getAt(0);  
                                combo.setValue(recordSelected.get('field1'));
                            }
                        });
                    }
                }
            })
        },{
         xtype: 'actioncolumn',
            width: Minerals.config.Runtime.actionColWidth,
            items: [
                MineralPro.config.util.MainGridActionColCmpt.editCls,
                deleteCls,
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    }],
        
               
        me.tools= [
        MineralPro.config.util.MainGridToolCmpt.addNewDataButton
        ,MineralPro.config.util.MainGridToolCmpt.saveToDbButton                   
        ,MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
        me.callParent(arguments);
        
    },
    listeners: {
        selectionchange: 'onLoadSelectionChange',
        afterlayout: 'onAfterLayout',
    }    
});
