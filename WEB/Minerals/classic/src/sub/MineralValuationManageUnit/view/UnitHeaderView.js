Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitHeaderView', {
    extend: 'MineralPro.config.view.BaseHeaderView',
    alias: 'widget.UnitHeaderView',
    layout: 'hbox',
    defaults: {
        style: {borderColor: '#000000', borderStyle: 'solid', borderWidth: '1px'},
        xtype: 'fieldset',
        layout: 'vbox',
        width: '24%',
        height: 95,
        margin: '0, 2, 0, 2',
        cls: 'header_fieldset',
    },
    items: [{
            title: 'Unit Information',
            width: '42%',
            defaults: {
                xtype: 'displayfield',
                fieldStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
                labelStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px',
                margin: '0 0 0 2',
                padding: '0 0 0 0',
                width: '100%',
                labelWidth: 105,
            },
            items: [{
                    fieldLabel: 'Unit Name:',
                    name: 'unitName',
                }, {
                    fieldLabel: 'Unit ID:',
                    name: 'unitId',
                }, {
                    fieldLabel: 'Operator:',
                    name: 'operatorName'
                },{
                    fieldLabel: 'Number of  Leases:',
                    name: 'numberofleases'
                }]
        }, {
            title: 'Values',
            width: '27%',
            defaults: {
                xtype: 'displayfield',
                fieldStyle: 'text-align:right;font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
                labelStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px',
                margin: '0 7 0 2',
                padding: '0 0 0 0',
                width: '100%',
                labelWidth: 150,
                renderer: function (val, meta) {
                    return  Ext.util.Format.number(val, '$000,000');
                },
            },
            items: [{
                    fieldLabel: 'Unit Working Interest Value:',
                    name: 'workingInterestValue',
                }, {
                    fieldLabel: 'Unit Royalty Interest Value:',
                    name: 'royaltyInterestValue',
                }, {
                    fieldLabel: 'Total Unit Value:',
                    name: 'totalUnitValue',
                }]
        }, {
            title: 'Interest',
            width: '30%',
            defaults: {
                xtype: 'displayfield',
                fieldStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
                labelStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px',
                margin: '0 0 0 2',
                padding: '0 0 0 0',
                width: '100%',
                labelWidth: 175,
                renderer: function (val, meta) {
                    return  Ext.util.Format.number(val, '0.00000000');
                },
            },
            items: [{           
                    fieldLabel: 'Weighted Working Interest:',
                    name: 'weightedWorkingInterest',
                }, {
                    fieldLabel: 'Weighted Royalty Interest:',
                    name: 'weightedRoyaltyInterest',
                }, {
                    fieldLabel: 'Total Unit WI Weight:',
                    name: 'totalWorkingInterest',
                    listeners: {
                            change: function(record) {
                                if(record.value>1){
                                     this.labelStyle = 'background:url(../resources/icons/tp_lower.png) no-repeat left top;font-weight:normal;margin:0px;padding:0px 0px 0px 20px;font-size:12px;min-height:5px';
                                }else if(record.value<1){
                                     this.labelStyle = 'background:url(../resources/icons/tp_upper.png) no-repeat left top;font-weight:normal;margin:0px;padding:0px 0px 0px 20px;font-size:12px;min-height:5px';
                                }else{
                                     this.labelStyle = 'background:url(../resources/icons/tp_good.png) no-repeat left top;font-weight:normal;margin:0px;padding:0px 0px 0px 20px;font-size:12px;min-height:5px';
                                }
                            }  
                        } 
                }, {
                    fieldLabel: 'Total Unit RI Weight:',
                    name: 'totalRoyaltyInterest',
                    listeners: {
                            change: function(record) {
                                if(record.value>1){
                                     this.labelStyle = 'background:url(../resources/icons/tp_lower.png) no-repeat left top;font-weight:normal;margin:0px;padding:0px 0px 0px 20px;font-size:12px;min-height:5px';
                                }else if(record.value<1){
                                     this.labelStyle = 'background:url(../resources/icons/tp_upper.png) no-repeat left top;font-weight:normal;margin:0px;padding:0px 0px 0px 20px;font-size:12px;min-height:5px';
                                }else{
                                     this.labelStyle = 'background:url(../resources/icons/tp_good.png) no-repeat left top;font-weight:normal;margin:0px;padding:0px 0px 0px 20px;font-size:12px;min-height:5px';
                                }
                            }  
                        }                
                }]
        }]
});    
