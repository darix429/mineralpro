Ext.define('Minerals.sub.MineralValuationManageUnit.view.Units.UnitsAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.sub.MineralValuationManageUnit.view.Units.UnitsAddFormViewController'],
    controller: 'UnitsAddFormViewController',
    alias: 'widget.UnitsAddFormView',
    title: 'Add New Unit',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#unitNameItemId',
    width: 450,
    mainPanelId: 'MineralValuationManageUnitId', //id of the main panel
    listGridStore: 'Minerals.sub.MineralValuationManageUnit.store.UnitsStore', //store for unit grid
    listGridAlias: 'UnitsGridView', //alias name for unit gridview

    initComponent: function () {
        var me = this;
        me.items = [{
                xtype: 'form',
                defaults: {
                    maskRe: /[^'\^]/,
                    labelWidth: 130,
                    width: 400
                },
                bodyPadding: 5,
                items: [{
                        fieldLabel: 'Units No.',
                        selectOnFocus: true,
                        tabIndex: 1,
                        itemId: 'unitNoItemId',
                        name: 'unitId',
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        validateBlank: true,
                        hidden: true
                    }, {
                        fieldLabel: 'Unit Name',
                        selectOnFocus: true,
                        tabIndex: 2,
                        itemId: 'unitNameItemId',
                        name: 'unitName',
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        validateBlank: true,
                        allowBlank: false,
//                listeners: {
//                   blur: 'onAddCheckExisting'
//                }
                    },{
                        fieldLabel: 'Operator ID',
                        selectOnFocus: true,
                        maskRe:/[0-9]/,
                        tabIndex: 3,
                        name: 'operatorName',
                        xtype: 'combo',
                        itemId: 'operatorNameItemId',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        displayField: 'operatorId',
                        valueField: 'operatorName',
                        store: 'Minerals.store.MineralValuation.OperatorComboStore',
                        typeAhead: true,
                        //forceSelection: true,
                        allowBlank: false,
                        queryMode: 'local',
                        listeners: {
                            select: function (record) {
                               me.down('#operatorItemId').setValue(me.down('#operatorNameItemId').getRawValue());
                                //console.log(me.down('#operatorNameItemId').getRawValue());
                            }
                        },
                        validator : function(value) {
                            var store = this.getStore()
                            var result = store.findRecord('operatorId', value)
                            if(result){
                                return true
                            } else {
                                return 'must be selected on the list'
                            }
                        }
                    },{
                        fieldLabel: 'Operator Name',
                        selectOnFocus: true,
                        tabIndex: 4,
                        name: 'operatorId',
                        xtype: 'combo',
                        itemId: 'operatorItemId',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        displayField: 'operatorName',
                        valueField: 'operatorId',
                        store: 'Minerals.store.MineralValuation.OperatorComboStore',
                        typeAhead: true,
                        forceSelection: true,
                        allowBlank: false,
                        queryMode: 'local',
                        listeners: {
                            select: function (record) {
                                me.down('#operatorNameItemId').setValue(me.down('#operatorItemId').value);
                            }
                        },
//                        beforequery: function (queryVV) {
//                            queryVV.combo.expand();
//                            queryVV.combo.store.load();
//                            return false;
//                        }
                     },
                    // {
                    //     xtype: 'hidden',
                    //     name: 'operatorName',
                    //     queryMode: 'local',
                    //     itemId: 'operatorNameItemId'
                    // }

                ],
            }];


        me.listeners = {
            afterrender: function () {
                setTimeout(function () {
                    //me.down('#operatorNameItemId').setValue(me.down('#operatorItemId').getRawValue());
                    //me.down('#operatorItemId').setValue(me.down('#operatorNameItemId').getRawValue());
                }, 100);
            }}

        me.callParent(arguments);
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
        MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});