
Ext.define('Minerals.sub.MineralValuationManageUnit.view.Units.UnitsGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.UnitsGridView',
    store:'Minerals.sub.MineralValuationManageUnit.store.UnitsStore',

    requires: ['Minerals.sub.MineralValuationManageUnit.view.Units.UnitsGridViewController'],
    controller: 'UnitsGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'unitId', 
    mainPanelAlias: 'MineralValuationManageUnit', //widget alias name of the main panel
    mainListModel: 'Minerals.sub.MineralValuationManageUnit.model.UnitsModel',     

    addFormAliasName: 'UnitsAddFormView', //widget form to call for add list 
    editFormAliasName: 'UnitsEditFormView', //widget form to call for edit
   
    withChildTab: true,
    headerInfo: 'Minerals.sub.MineralValuationManageUnit.view.UnitHeaderView',
    headerInfoAlias: 'UnitHeaderView',
    headerInfoStore: 'Minerals.sub.MineralValuationManageUnit.store.UnitsHeaderDetailStore',

    firstFocus: 'unitId',    
    
    addButtonTooltip: 'Add New Unit',
    addButtonText: 'Add New Unit',
         
    columns:[{ 
        header: 'Unit No.',
        flex     : 0,
		width: 130,
        dataIndex: 'unitId',
        itemId: 'unitId',
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        })
    },{   
        header: 'Unit Name',
        flex     : 1,
        dataIndex: 'unitName',
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        })
    },{   
        header: 'Operator ID',
        flex     : 0,
		width: 130,
        dataIndex: 'operatorId',
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        })
    },{   
        header: 'Operator Name',
        flex     : 1,
        dataIndex: 'operatorName',
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        })
    },{
     xtype: 'actioncolumn',
        width: Minerals.config.Runtime.actionColWidth,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls,
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    }],
   
    initComponent: function () {  
        var me = this;
        var mainPanel = me.up(me.mainPanelAlias);

        // me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],       
        me.plugins = [Ext.create('Ext.ux.grid.FilterAllRows',{
            pluginId: 'filterAllRows'
        })];
        
        me.tools= [
            MineralPro.config.util.MainGridToolCmpt.addNewDataButton,
            MineralPro.config.util.MainGridToolCmpt.saveToDbButton,
            // MineralPro.config.util.MainGridToolCmpt.clearFilterButton
        ];
        // var store = Ext.getStore(me.store);
        // store.setRemoteFilter(true);
        // store.on({
        //     beforeload: {
        //         fn: function(store, operation, eOpts){
        //             MineralPro.config.RuntimeUtility.DebugLog('Entering LeasesGridView initComponent attach load event beforeload.')
        //             Ext.getBody().mask('Loading Units...');

        //             // console.log('-------------------------------BEFORE STORE IS LOADED START -------------------------------');
        //             // console.log('selectedId : '+mainPanel.selectedId);
        //             // console.log('selectedIdIndex : '+mainPanel.selectedIdIndex);
        //             // console.log('selectedName : '+mainPanel.selectedName);
        //             // console.log('selectedNameIndex : '+mainPanel.selectedNameIndex);
        //             // console.log('selectedRRCNumber : '+mainPanel.selectedRRCNumber);
        //             // console.log('selectedRRCNumberIndex : '+mainPanel.selectedRRCNumberIndex);
        //             // console.log('subjectTypeCd : '+mainPanel.subjectTypeCd);
        //             // console.log('-------------------------------- END --------------------------------');

        //             var extraParams = store.getProxy().extraParams;
        //             var filterData = me.getFilterData();//getFilterData is bound on FilterAllRows Plugin
        //             // var filterCount = Ext.Object.getValues(filterData).filter(function(val){return val.trim();}).length;
        //             var filterCount = Object.values(filterData).filter(function(val){return val.trim();}).length;
        //             if(filterCount == 0){
        //                 Ext.getBody().unmask();
        //                 MineralPro.config.RuntimeUtility.DebugLog('Entering LeasesGridView initComponent attach load event canceled.')
        //                 return false;
        //             }
        //             MineralPro.config.RuntimeUtility.DebugLog('Entering LeasesGridView initComponent attach load event continue to load.')
        //             extraParams['searchBy'] = me.down('cycle#searchCategory').getActiveItem().getValue();
        //         }
        //     },
        //     load: {
        //         fn: function(store, records, successful, operation, eOpts){
        //             MineralPro.config.RuntimeUtility.DebugLog('Entering LeasesGridView initComponent attach load event.')
        //             Ext.getBody().unmask();

        //             // console.log('-------------------------------AFTER STORE IS LOADED START -------------------------------');
        //             // console.log('selectedId : '+mainPanel.selectedId);
        //             // console.log('selectedIdIndex : '+mainPanel.selectedIdIndex);
        //             // console.log('selectedName : '+mainPanel.selectedName);
        //             // console.log('selectedNameIndex : '+mainPanel.selectedNameIndex);
        //             // console.log('selectedRRCNumber : '+mainPanel.selectedRRCNumber);
        //             // console.log('selectedRRCNumberIndex : '+mainPanel.selectedRRCNumberIndex);
        //             // console.log('subjectTypeCd : '+mainPanel.subjectTypeCd);
        //             // console.log('-------------------------------- END --------------------------------');

        //             var selectedIndex = store.findBy(function(rec){
        //                 return rec.get(mainPanel.selectedIdIndex) == mainPanel.selectedId;
        //             });
        //             if(selectedIndex == -1){
        //                 selectedIndex = 0;
        //                 /*------------------------------ START ------------------------------
        //                 | Clear out selected Owner on mainPanel
        //                 */
        //                 me.getSelectionModel().deselectAll(true);
        //                 mainPanel.selectedId =  '0'; //Need default value '0'
        //                 mainPanel.selectedIdIndex =  'unitId';
        //                 mainPanel.selectedName =  '0'; //Need default value '0'
        //                 mainPanel.selectedNameIndex =  'unitName';
        //                 /*
        //                 | Clear out selected Owner on mainPanel
        //                 | ------------------------------ END ------------------------------
        //                 */
        //             }

        //             if(successful){
        //                 // var filterData = me.getFilterData();//getFilterData is bound on FilterAllRows Plugin
        //                 // // var filterCount = Ext.Object.getValues(filterData).filter(function(val){return val.trim();}).length;
        //                 // var filterCount = Object.values(filterData).filter(function(val){return val.trim();}).length;
        //                 // if(filterCount==0){
        //                 //     me.store.clearFilter(true);
        //                 // }
        //                 // if(records.length > 0 && me.selModel){
        //                 //     if(me.selModel.selectionMode == 'SINGLE'){
        //                 //         me.getSelectionModel().select(0);
        //                 //         me.fireEvent('selectionchange',me.getSelectionModel(), me.getSelectionModel().getSelection());
        //                 //         MineralPro.config.RuntimeUtility.DebugLog('END firing selectionchange');
        //                 //     }
        //                 // }
        //             }
        //             else{
        //                 var descriptiveErrorMsg = '';
        //                 if(operation.getError() && operation.getError().statusText){
        //                     descriptiveErrorMsg += '<p><span style="font-weight:bold;">Error Details: </span><span style="color:red;">'+operation.getError().statusText+'.</span></p>'
        //                 }
        //                 Ext.Msg.show({
        //                     title: 'Oops!',
        //                     msg: "Something went wrong while retrieving Owner's info."+descriptiveErrorMsg,
        //                     buttons: Ext.Msg.OK
        //                 });
        //             }
        //             MineralPro.config.RuntimeUtility.DebugLog('Leaving LeasesGridView initComponent attach load event.')
        //         }
        //     }
        // })
        me.callParent(arguments);
        
    },
    listeners: {
        selectionchange: 'onLoadSelectionChange',
        // afterlayout: 'onAfterLayout',
    }
});