Ext.define('Minerals.sub.MineralValuationManageUnit.view.Units.UnitsView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.UnitsView',
    layout:'border',
    
    id: 'UnitsViewId',
    
    items: [{
            xtype: 'UnitsGridView',
            html:'center',
            region:'center'
        }] 
});    