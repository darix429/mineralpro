Ext.define('Minerals.sub.MineralValuationManageUnit.view.Units.UnitsEditFormView', {
    extend: 'Minerals.sub.MineralValuationManageUnit.view.Units.UnitsAddFormView',
    alias: 'widget.UnitsEditFormView',
    
    
    title: 'Edit Unit',
    defaultFocus: '#unitNameItemId',

    listeners: {
        afterrender: function(){
            var me = this;
            var unitNoItemId = me.down('#unitNoItemId');
                unitNoItemId.disable(true);
                unitNoItemId.show();
            if(isNaN(me.down('#operatorNameItemId'))){
                me.down('#operatorNameItemId').setValue(me.down('#operatorItemId').value);
            }
        }
    },

    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});