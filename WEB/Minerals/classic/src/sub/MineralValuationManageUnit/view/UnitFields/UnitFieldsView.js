Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitFields.UnitFieldsView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.UnitFieldsView',
    layout:'border',
    
    id: 'UnitFieldViewId',
    
    items: [{
            xtype: 'UnitFieldsGridView',
            html:'center',
            region:'center'
        }] 
});    