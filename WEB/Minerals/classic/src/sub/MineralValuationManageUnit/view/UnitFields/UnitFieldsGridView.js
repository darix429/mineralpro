
Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitFields.UnitFieldsGridView', {
    extend: 'Minerals.config.view.Field.BaseFieldGridView',
    alias: 'widget.UnitFieldsGridView', 
    
    store:'Minerals.sub.MineralValuationManageUnit.store.UnitFieldsStore',           
              
    //below are default items needed for grid checking 
//    firstLoadItemId: 'dateItemId', 
    mainPanelAlias: 'MineralValuationManageUnit', //widget alias name of the main panel
    mainPanelId: 'MineralValuationManageUnitId',
//    mainListModel: 'Minerals.model.MineralValuation.NotesModel',     
//
//    addFormAliasName: 'UnitNotesAddFormView', //widget form to call for add list 
//    editFormAliasName: 'UnitNotesEditFormView', //widget form to call for edit
   

//    firstFocus: 'dateItemId',    
    
//    addButtonTooltip: 'Add New Note',
//    addButtonText: 'Add New Note',
        
      
});