Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitLeases.UnitLeasesGridViewController', {
    extend: 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.UnitLeasesGridViewController',
    onOpenSelectedTabView: function (record, rowIndex) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering UnitLeasesGridViewController onOpenSelectedTabView.')
        var me = this;
        var centerView = me.getView().up('centerView');
        var westView = centerView.up().down('westView');
        var favoriteTree = westView.down('#favoriteTreeId');
        var menuTree = westView.down('#menuTreeId');
        var dataTree = true;
        var index = 0;

        favoriteTree.getStore().each(function (c, fn) {
            if (c.data.functionName == 'Manage Lease') {
                index = fn;
                dataTree = false;
            }
        })
        menuTree.getStore().each(function (c, fn) {
            if (c.data.functionName == 'Manage Lease') {
                index = fn;
                dataTree = true;
            }
        })

        if (dataTree == true) {
            menuTree.getSelectionModel().select(index);
            setTimeout(function () {
                var tabPanel = centerView.down('MineralValuationManageLease');
                var grid = centerView.down('#MineralsLeasesId').down('grid');
                var cols = grid.columns;

                tabPanel.setActiveTab(0);
                Ext.each(cols, function (col) {
                    if (col.dataIndex == 'leaseName') {
                        col.filterElement.setValue(record.data.leaseName.trim());
                    }
                });
                var hasFilterAllRowsPlugin = grid.getPlugin('filterAllRows');
                if(hasFilterAllRowsPlugin){
                    grid.fireEvent('runFilter');
                }
            }, 800)
        } else {
            favoriteTree.getSelectionModel().select(index);
            setTimeout(function () {
                var tabPanel = centerView.down('MineralValuationManageLease');
                var grid = centerView.down('#MineralsLeasesId').down('grid');
                var cols = grid.columns;

                tabPanel.setActiveTab(0);
                Ext.each(cols, function (col) {
                    if (col.dataIndex == 'leaseName') {
                        col.filterElement.setValue(record.data.leaseName.trim());
                    }
                });
                var hasFilterAllRowsPlugin = grid.getPlugin('filterAllRows');
                if(hasFilterAllRowsPlugin){
                    grid.fireEvent('runFilter');
                }
            }, 800)
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving UnitLeasesGridViewController onOpenSelectedTabView.')
    },
    onOpenAddListForm: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering UnitLeasesGridViewController onOpenAddListForm.')
        var me = this;
        var mainView = me.getView();
        var record = Ext.create(mainView.mainListModel);
        var addView = Ext.widget(mainView.addFormAliasName);
        var store = addView.down('#leaseItemId').getStore();

        store.load();
        record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
        addView.down('form').loadRecord(record);
        addView.show()
        MineralPro.config.RuntimeUtility.DebugLog('Leaving UnitLeasesGridViewController onOpenAddListForm.')
    },
    onSyncListGridData: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering UnitLeasesGridViewController onSyncListGridData.')
        var me = this;
        var view = me.getView();
        var listStore = view.getStore();
        var totalRI = 0;
        var totalWI = 0;
        listStore.each(function (c) {
            if (c.data.rowDeleteFlag == '') {
                totalWI = +totalWI + +c.data.unitWorkingInterestPercent;
                totalRI = +totalRI + +c.data.unitRoyaltyInterestPercent;
            }
        });

        
            var dirty = listStore.getNewRecords().length > 0
                    || listStore.getUpdatedRecords().length > 0
                    || listStore.getRemovedRecords().length > 0;
            if (dirty) {
            if (totalWI <= 1 && totalRI <= 1) {    
                Ext.Msg.show({
                    title: MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                    msg: MineralPro.config.Runtime.syncGridMsgConfimationMsg,
                    iconCls: MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                    buttons: Ext.Msg.YESNO,
                    scope: this,
                    width: 250,
                    fn: function (btn) {
                        if (btn === 'yes') {
                            view.getSelectionModel().deselectAll(true);
                            listStore.getProxy().extraParams = {
                                createLength: listStore.getNewRecords().length,
                                updateLength: listStore.getUpdatedRecords().length
                            };
                            listStore.sync({
                                callback: function (records, operation, success) {
                                    var mainPanel = view.up(view.mainPanelAlias)
                                    var mainGrid = mainPanel.down('UnitsGridView');
                                    var mainStore = mainGrid.getStore();
                                    var record = mainGrid.getSelectionModel().getSelected();
                                    var mainSelectedId = mainPanel.selectedId;
                                    var getSelectedIndex = mainStore.find('unitId', mainSelectedId, 0, false, false, true);
                                    mainGrid.getSelectionModel().deselectAll();
                                    mainGrid.getSelectionModel().select(getSelectedIndex, false, false);
                                    if (mainPanel.selectedId) {
                                        listStore.getProxy().extraParams = {
                                            selectedId: mainPanel.selectedId
                                        };
                                    }
                                    listStore.reload({
                                        callback: function () {
                                            if (view.getStore().getAt(0)) {
                                                view.getSelectionModel().select(0, false, false);
                                            }
                                        }
                                    });
                                }
                            });
                            listStore.commitChanges();
                        }
                    }
                });
            } else {
            Ext.Msg.show({
                    title: 'Unit Pct Exceed',
                    msg: 'The total Unit Percent have Exceeded 1.00000000. <br />Do you want to proceed?',
                    iconCls: MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                    buttons: Ext.Msg.YESNO,
                    scope: this,
                    fn: function (btn) {
                        if (btn === 'yes') {
                            view.getSelectionModel().deselectAll(true);
                            listStore.getProxy().extraParams = {
                                createLength: listStore.getNewRecords().length,
                                updateLength: listStore.getUpdatedRecords().length
                            };
                            listStore.sync({
                                callback: function (records, operation, success) {
                                    var mainPanel = view.up(view.mainPanelAlias)
                                    var mainGrid = mainPanel.down('UnitsGridView');
                                    var mainStore = mainGrid.getStore();
                                    var record = mainGrid.getSelectionModel().getSelected();
                                    var mainSelectedId = mainPanel.selectedId;
                                    var getSelectedIndex = mainStore.find('unitId', mainSelectedId, 0, false, false, true);
                                    mainGrid.getSelectionModel().deselectAll();
                                    mainGrid.getSelectionModel().select(getSelectedIndex, false, false);
                                    if (mainPanel.selectedId) {
                                        listStore.getProxy().extraParams = {
                                            selectedId: mainPanel.selectedId
                                        };
                                    }
                                    listStore.reload({
                                        callback: function () {
                                            if (view.getStore().getAt(0)) {
                                                view.getSelectionModel().select(0, false, false);
                                            }
                                        }
                                    });
                                }
                            });
                            listStore.commitChanges();
                        }
                    }
                });
            }
        } else {
            Ext.Msg.show({
                    iconCls: MineralPro.config.Runtime.listSyncMsgErrorIcon,
                    title: MineralPro.config.Runtime.listSyncMsgErrorTitle,
                    msg: MineralPro.config.Runtime.listSyncMsgErrorMsg,
                    buttons: Ext.Msg.OK
                });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving UnitLeasesGridViewController onSyncListGridData.')
    },
    onValidateEdit: function (editor, context) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering UnitLeasesGridViewController onValidateEdit.')
        var me = this;
        var view = me.getView();
        var listStore = view.getStore();
        var showAlert = false;
        var cRec = context.record
        if (context.field == 'tractNum') {
            listStore.each(function (rec) {
                if (rec != cRec) {
                    if (rec.get('tractNum') == context.value) {
                        showAlert = true;
                        return false;
                    }
                }
            })
            if (showAlert) {
                Ext.Msg.show({
                    iconCls: MineralPro.config.Runtime.addExistingDataWarningCls,
                    title: 'Alert',
                    msg: 'Tract Number is already assigned. Changes will not reflect.',
                    buttons: Ext.Msg.OK,
                    fn: function () {
                        cRec.set('tractNum', context.originalValue);
                    }
                });
            }
        } else {
            var total = 0;
            var totalWIPWeight = 0;
            var totalRIPWeight = 0;

            listStore.each(function (rec) {
                if(rec.get('rowDeleteFlag') == ''){
                    if (rec == cRec) {
                        total = parseFloat(total) + parseFloat(context.value);
                        if (context.field == 'unitWorkingInterestPercent') {
                            totalWIPWeight = parseFloat(totalWIPWeight) + parseFloat(rec.get('workingInterestPercent')) * parseFloat(context.value);
                            totalRIPWeight = parseFloat(totalRIPWeight) + parseFloat(rec.get('royaltyInterestPercentWeight'));
                        } else {
                            totalRIPWeight = parseFloat(totalRIPWeight) + parseFloat(rec.get('royaltyInterestPercent')) * parseFloat(context.value);
                            totalWIPWeight = parseFloat(totalWIPWeight) + parseFloat(rec.get('workingInterestPercentWeight'));
                        }
                    } else {
                        total = parseFloat(total) + parseFloat(rec.get(context.field));
                        totalWIPWeight = parseFloat(totalWIPWeight) + parseFloat(rec.get('workingInterestPercentWeight'));
                        totalRIPWeight = parseFloat(totalRIPWeight) + parseFloat(rec.get('royaltyInterestPercentWeight'));
                    }
                }
            })
            
            if (total <= 1) {

                var workingInterestPercentWeight;
                var royaltyInterestPercentWeight;
                var workingInterestValue;
                var royaltyInterestValue;
                var totalWorkingInterestValue = cRec.get('totalWorkingInterestValue');
                var totalRoyaltyInterestValue = cRec.get('totalRoyaltyInterestValue');

                listStore.each(function (rec) {
                    if(rec.get('rowDeleteFlag') == ''){
                        if (rec == cRec) {
                            if(context.field == 'unitWorkingInterestPercent'){
                                workingInterestPercentWeight = parseFloat(rec.get('workingInterestPercent')) * parseFloat(context.value)
                                royaltyInterestPercentWeight = parseFloat(rec.get('royaltyInterestPercent')) * parseFloat(rec.get('unitRoyaltyInterestPercent')) 
                            }else{
                                workingInterestPercentWeight = parseFloat(rec.get('workingInterestPercent')) * parseFloat(rec.get('unitWorkingInterestPercent'))
                                royaltyInterestPercentWeight = parseFloat(rec.get('royaltyInterestPercent')) * parseFloat(context.value)                  
                            }
                        }else{
                             workingInterestPercentWeight = parseFloat(rec.get('workingInterestPercent')) * parseFloat(rec.get('unitWorkingInterestPercent'))
                             royaltyInterestPercentWeight = parseFloat(rec.get('royaltyInterestPercent')) * parseFloat(rec.get('unitRoyaltyInterestPercent'))                    
                        }

    //                    workingInterestPercentWeight = parseFloat(rec.get('unitWorkingInterestPercent')) * parseFloat(rec.get('workingInterestPercent'))
                        workingInterestValue = parseFloat(totalWorkingInterestValue) / parseFloat(totalWIPWeight) * parseFloat(workingInterestPercentWeight)
                        rec.set('workingInterestPercentWeight', Ext.util.Format.number(workingInterestPercentWeight, '0.00000000'));
                        rec.set('workingInterestValue', Ext.util.Format.number(workingInterestValue, '$000,000'));

    //                    royaltyInterestPercentWeight = parseFloat(rec.get('unitRoyaltyInterestPercent')) * parseFloat(rec.get('royaltyInterestPercent'))
                        royaltyInterestValue = parseFloat(totalRoyaltyInterestValue) / parseFloat(totalRIPWeight) * parseFloat(royaltyInterestPercentWeight)
                        rec.set('royaltyInterestPercentWeight', Ext.util.Format.number(royaltyInterestPercentWeight, '0.00000000'));
                        rec.set('royaltyInterestValue', Ext.util.Format.number(royaltyInterestValue, '$000,000'));
                    }
                })

            } else {
                Ext.Msg.show({
                    iconCls: MineralPro.config.Runtime.addExistingDataWarningCls,
                    title: context.column.text + ' exceeded',
                    msg: 'The total ' + context.column.text + ' have Exceeded 1.00000000. Changes will not reflect.',
                    buttons: Ext.Msg.OK,
                    fn: function () {
                        cRec.set(context.field, context.originalValue);
                    }
                });
            }
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving UnitLeasesGridViewController onValidateEdit.')
    },
    reCalcValues: function(grid){
        MineralPro.config.RuntimeUtility.DebugLog('Entering UnitLeasesGridViewController reCalcValues.')
                                                               
            var listStore = grid.getStore();
            var totalWIPWeight = 0;
            var totalRIPWeight = 0;
            var workingInterestPercentWeight;
            var royaltyInterestPercentWeight;
            var workingInterestValue;
            var royaltyInterestValue;
            var totalWorkingInterestValue;
            var totalRoyaltyInterestValue;            

            // listStore.each(function (rec) {
            //     if(rec.get('rowDeleteFlag') == ''){
            //         totalWIPWeight = parseFloat(totalWIPWeight) + parseFloat(rec.get('workingInterestPercentWeight'));
            //         totalRIPWeight = parseFloat(totalRIPWeight) + parseFloat(rec.get('royaltyInterestPercentWeight'));                  
            //     }
            // });
            var data = listStore.getData().items.map(function(rec,i){ return rec.getData(); })
            Ext.getBody().mask('calculating...');
            Ext.Ajax.request({
                method: 'POST',
                url: '/api/Minerals/MineralValuation/MineralValuationManageUnit/CalculateUnitLeases',
                params: {
                    rowUpdateUserid: MineralPro.config.Runtime.idAppUser,
                    appraisalYear: MineralPro.config.Runtime.appraisalYear,
                    unitId: data[0].unitId
                },
                jsonData: data,
                success: function(response){
                    Ext.getBody().unmask();
                    var resp = JSON.parse(response.responseText);
                    var data = resp.data || [];
                    listStore.each(function(rec){
                        var leaseId = rec.get('leaseId');
                        var calcLease = data.filter(function(l){
                            return l.leaseId == leaseId;
                        });
                        if(calcLease.length){
                            var fields = [
                                'workingInterestValue','royaltyInterestValue',
                                'workingInterestPercentWeight','royaltyInterestPercentWeight'
                            ];
                            fields.forEach(function(field){
                                var val = calcLease[0][field];
                                rec.set(field, val);
                            });
                        }
                    })
                },
                failure: function(response){
                    Ext.getBody().unmask();
                    var resp = JSON.parse(response.responseText);
                    Ext.Msg.show({
                        title: 'Error',
                        msg: 'Something went wrong while trying to calculate lease values. You may click on save button to recalculate and save to database.',
                        buttons: Ext.Msg.OK
                    });
                }
            });
            // listStore.each(function (rec) {
            //     if(rec.get('rowDeleteFlag') == ''){
            //         totalWorkingInterestValue = rec.get('totalWorkingInterestValue');
            //         totalRoyaltyInterestValue = rec.get('totalRoyaltyInterestValue');

            //         workingInterestPercentWeight = parseFloat(rec.get('workingInterestPercent')) * parseFloat(rec.get('unitWorkingInterestPercent'))
            //         royaltyInterestPercentWeight = parseFloat(rec.get('royaltyInterestPercent')) * parseFloat(rec.get('unitRoyaltyInterestPercent'))                    
                    
            //         // workingInterestValue = parseFloat(totalWorkingInterestValue) / parseFloat(totalWIPWeight) * parseFloat(workingInterestPercentWeight)
            //         workingInterestValue = parseFloat(totalWorkingInterestValue) * parseFloat(rec.get('unitWorkingInterestPercent')||0)
            //         rec.set('workingInterestPercentWeight', Ext.util.Format.number(workingInterestPercentWeight, '0.00000000'));
            //         rec.set('workingInterestValue', Ext.util.Format.number(workingInterestValue, '$000,000'));

            //         // royaltyInterestValue = parseFloat(totalRoyaltyInterestValue) / parseFloat(totalRIPWeight) * parseFloat(royaltyInterestPercentWeight)
            //         royaltyInterestValue = parseFloat(totalRoyaltyInterestValue) * parseFloat(rec.get('unitRoyaltyInterestPercent')||0)
            //         rec.set('royaltyInterestPercentWeight', Ext.util.Format.number(royaltyInterestPercentWeight, '0.00000000'));
            //         rec.set('royaltyInterestValue', Ext.util.Format.number(royaltyInterestValue, '$000,000'));
            //     }
            // })
                      
        MineralPro.config.RuntimeUtility.DebugLog('Leaving UnitLeasesGridViewController reCalcValues.')
    }
    
});

                     