Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitLeases.UnitLeasesView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.UnitLeasesView',
    layout:'border',
    
    id: 'UnitLeasesViewId',
    
    items: [{
            xtype: 'UnitLeasesGridView',
            html:'center',
            region:'center'
        }] 
});    