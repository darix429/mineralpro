Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitLeases.UnitLeasesAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.sub.MineralValuationManageUnit.view.UnitLeases.UnitLeasesAddFormViewController'],
    controller: 'UnitLeasesAddFormViewController',
    alias: 'widget.UnitLeasesAddFormView',
    title: 'Add New Lease',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#leaseItemId',
    width: 450,
    mainPanelId: 'MineralValuationManageUnitId', //id of the main panel
    listGridStore: 'Minerals.sub.MineralValuationManageUnit.store.UnitLeasesStore', //store for unit grid
    listGridAlias: 'UnitLeasesGridView', //alias name for unit gridview

    initComponent: function () {
        var me = this;
        me.items = [{
                xtype: 'form',
                defaults: {
                    maskRe: /[^'\^]/,
                    labelWidth: 130,
                    width: 400
                },
                bodyPadding: 5,
                items: [{
                        fieldLabel: 'Unit Name',
                        xtype: 'displayfield',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        margin: '0 0 10',
                        name: 'unitName',
                        itemId: 'unitName',
                    }, {
                        xtype: 'hidden',
                        name: 'unitId',
                        queryMode: 'local',
                        itemId: 'unitNameId'
                    }, {
                        fieldLabel: 'Lease Name',
                        selectOnFocus: true,
                        tabIndex: 1,
                        name: 'leaseId',
                        xtype: 'combo',
                        itemId: 'leaseItemId',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        displayField: 'leaseName',
                        valueField: 'leaseId',
                        typeAhead: true,
                        forceSelection: true,
                        store: 'Minerals.sub.MineralValuationManageUnit.store.UnitLeasesLeaseStore',
                        allowBlank: false,
                        queryMode: 'local',
                        listeners: {
                            select: function (combo) {
                                me.down('#leaseNameItemId').setValue(this.getRawValue());
                                var comboStore = combo.getStore();
                                var rec = comboStore.getAt(comboStore.find(combo.name, combo.value, false, false, true));
                                me.down('#workingInterestPercentItemId').setValue(rec.get('workingInterestPercent'));
                                me.down('#royaltyInterestPercentItemId').setValue(rec.get('royaltyInterestPercent'));
                            },
                            change: function (combo) {
                                if (me.id.toLowerCase().indexOf("edit") != -1) {
                                    me.down('#leaseNameItemId').setValue(this.getRawValue());
                                    var comboStore = combo.getStore();
                                    var rec = comboStore.getAt(comboStore.find(combo.name, combo.value, false, false, true));
                                    me.down('#workingInterestPercentItemId').setValue(rec.get('workingInterestPercent'));
                                    me.down('#royaltyInterestPercentItemId').setValue(rec.get('royaltyInterestPercent'));
                                }
                            },
                            blur: 'onAddCheckExisting'
                        },
                        // beforequery: function(queryVV){
                        // 	queryVV.combo.expand();
                        // 	queryVV.combo.store.load();
                        // 	return false;
                        // } 
                    }, {
                        xtype: 'hidden',
                        name: 'leaseName',
                        queryMode: 'local',
                        itemId: 'leaseNameItemId'
                    }, {
                        fieldLabel: 'Tract No.',
                        //selectOnFocus: true,
                        tabIndex: 2,
                        // maskRe: /[0-9.]/,
                        itemId: 'tractNoItemId',
                        name: 'tractNum',
                        msgTarget: 'side',
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        validateBlank: true,
                        maxLength: 10,
                        listeners: {
                            blur: 'onAddCheckExistingNumber'
                        }
                    }, {
                        fieldLabel: 'Unit WI Weight',
                        //selectOnFocus: true,
                        tabIndex: 3,
                        itemId: 'unitWIItemId',
                        name: 'unitWorkingInterestPercent',
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        validateBlank: true,
                        allowBlank: false,
                        maxValue: 999999,
                        msgTarget: 'side',
                        decimalPrecision: 8,
                        decimalSeparator: '.',
                        maskRe: /[0-9.]/,
                        minValue: 0.00000000,
                        enableKeyEvents: true,
                        listeners: {
                            initialize: function () {
                                var nfield = Ext.get('weightfield');
                                nfield.down('input').set({pattern: '[0-9]*'});
                            },
                            keyup: function (data, e, eOpts) {
                                var leaseUnitWI = this.getRawValue();
                                var mainView = Ext.getCmp(me.mainPanelId);
                                var listGrid = mainView.down(me.listGridAlias);
                                var baseValueWI = me.down('#oldValueWIItemId').getValue();
                                var WIinterestPct = leaseUnitWI;
                                var selected = listGrid.getSelectionModel().getSelection()[0];
                                
                                listGrid.getStore().each(function (c) {
                                    if (me.id.toLowerCase().indexOf("edit") > -1 && c.data.rowDeleteFlag == '' && c.get('leaseId') != selected.get('leaseId')){
                                        WIinterestPct = +WIinterestPct + +c.data.unitWorkingInterestPercent;
                                    }
                                    else if(me.id.toLowerCase().indexOf("edit") == -1){
                                        WIinterestPct = +WIinterestPct + +c.data.unitWorkingInterestPercent;
                                    }
                                });

                                var totalWIPct = WIinterestPct;
                                // if(me.id.toLowerCase().indexOf("edit") != -1){
                                //     totalWIPct = totalWIPct - baseValueWI
                                // }
                                var leaseWIWeight = me.down('#workingInterestPercentWeightItemId')
                                
                                if (leaseUnitWI != '' && leaseUnitWI > 0) {
                                    me.down('#workingInterestValueItemId').show();
                                    me.down('#totalWorkingInterestPercentItemId').setValue(parseFloat(totalWIPct).toFixed(8));
                                    var totalValueWI = me.down('#totalWorkingInterestValueId').getRawValue();
                                                                                                                                           
                                    // var leaseWIWeightPercent = parseFloat(leaseUnitWI * me.down('#workingInterestPercentItemId').getRawValue());
                                    // leaseWIWeight.setValue(Ext.util.Format.number(leaseWIWeightPercent, '0.00000000'));
                                    
                                    // var WIValue = parseFloat(leaseWIWeightPercent * totalValueWI);
                                    // me.down('#workingInterestValueItemId').setValue(Ext.util.Format.number(WIValue, '$000,000'));
                                    if(me.id.toLowerCase().indexOf("edit") == -1){
                                        var combo = me.down('combobox[name=leaseId]');
                                        var comboStore = combo.getStore();
                                        var selectedRec = comboStore.getAt(comboStore.find(combo.name, combo.value, false, false, true));
                                        var leaseWIV = selectedRec.get('workingInterestValue') || 0;
                                        
                                        var WIValue = parseInt(leaseWIV);
                                        me.down('#workingInterestValueItemId').setValue(Ext.util.Format.number(WIValue, '$000,000'));
                                    }
                                    else{
                                        var WIValue = parseFloat(leaseUnitWI * totalValueWI);
                                        me.down('#workingInterestValueItemId').setValue(Ext.util.Format.number(WIValue, '$000,000'));    
                                    }
                                    
                                    
                                    
                                } else {
                                    me.down('#workingInterestValueItemId').hide();
                                    // me.down('#workingInterestPercentItemId').setValue(Ext.util.Format.number(WIinterestPct, '0.00000000'));   
                                    me.down('#totalWorkingInterestPercentItemId').setValue(parseFloat(totalWIPct).toFixed(8));
                                    leaseWIWeight.setValue(Ext.util.Format.number(WIinterestPct, '0.00000000')); 
                                }                                                                                                                         
                            },
                        },
                        validator: function (val) {
                            if (val <= 1) {
                                return true;
                            } else if (val == '.') {
                                return "Value must contain a number.";
                            } else {
                                return "Value cannot be greater than 1.00000000.";
                            }
                        }
                    }, {
                        fieldLabel: 'Lease WI',
                        name: 'workingInterestPercent',
                        xtype: 'textfield',
                        readOnly: true,
                        margin: '0 0 10',
                        itemId: 'workingInterestPercentItemId',
                        labelAlign: 'right',
//                        hidden: true,
                        fieldStyle: 'background-color: 	#D8D8D8;'
                        //disabled: true         
                            
                        
                    }, {
                        fieldLabel: 'Lease WI Value',
                        name: 'workingInterestValue',
                        xtype: 'textfield',
                        readOnly: true,
                        margin: '0 0 10',
                        itemId: 'workingInterestValueItemId',
                        labelAlign: 'right',
                        hidden: true,
                        fieldStyle: 'background-color: 	#D8D8D8;'
                                //disabled: true     
                    }, {
                        fieldLabel: 'Total Unit WI Weight',
                        name: 'totalWorkingInterestPercent',
                        xtype: 'textfield',
                        readOnly: true,
                        margin: '0 0 10',
                        itemId: 'totalWorkingInterestPercentItemId',
                        labelAlign: 'right',
                        listeners: {
                            change: function (record) {
                                if (record.getRawValue() == 1) {
                                    this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_good.png) no-repeat right;');
                                } else if (record.getRawValue() < 1) {
                                    this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_upper.png) no-repeat right;');
                                } else {
                                    this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_lower.png) no-repeat right;');
                                }
                            }
                        }
                        //hidden: true,
                        //disabled: true     
                    }, {
                        fieldLabel: 'Unit RI Weight',
                        //selectOnFocus: true,
                        tabIndex: 4,
                        msgTarget: 'side',
                        itemId: 'unitRIItemId',
                        name: 'unitRoyaltyInterestPercent',
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        validateBlank: true,
                        allowBlank: false,
                        maxValue: 999999,
                        decimalPrecision: 8,
                        decimalSeparator: '.',
                        maskRe: /[0-9.]/,
                        minValue: 0.00000000,
                        enableKeyEvents: true,
                        listeners: {
                            initialize: function () {
                                var nfield = Ext.get('weightfield');
                                nfield.down('input').set({pattern: '[0-9]*'});
                            },
                            keyup: function (data, e, eOpts) {
                                var leaseUnitRI = this.getRawValue();
                                var mainView = Ext.getCmp(me.mainPanelId);
                                var listGrid = mainView.down(me.listGridAlias);
                                var baseValueRI = me.down('#oldValueRIItemId').getValue();
                                var RIinterestPct = leaseUnitRI;
                                var selected = listGrid.getSelectionModel().getSelection()[0];
                                    
                                listGrid.getStore().each(function (c) {
                                    if (me.id.toLowerCase().indexOf("edit") > -1 && c.data.rowDeleteFlag == '' && c.get('leaseId') != selected.get('leaseId')){
                                        RIinterestPct = +RIinterestPct + +c.data.unitRoyaltyInterestPercent;
                                    }
                                    else if(me.id.toLowerCase().indexOf("edit") == -1){
                                        RIinterestPct = +RIinterestPct + +c.data.unitRoyaltyInterestPercent;
                                    }
                                });

                                var totalRIPct = RIinterestPct;
                                // if(me.id.toLowerCase().indexOf("edit") != -1){
                                //     totalRIPct = totalRIPct - baseValueRI
                                // }
                                
                                var leaseRIWeight = me.down('#royaltyInterestPercentWeightItemId')

                                if (leaseUnitRI != '' && leaseUnitRI > 0) {
                                    me.down('#royaltyInterestValueItemId').show();
                                    me.down('#totalRoyaltyInterestPercentItemId').setValue(parseFloat(totalRIPct).toFixed(8));
                                    var totalValueRI = me.down('#totalRoyaltyInterestValueId').getRawValue();
                                                                        
                                    // var leaseRIWeightPercent = parseFloat(leaseUnitRI * me.down('#royaltyInterestPercentItemId').getRawValue());
                                    // leaseRIWeight.setValue(Ext.util.Format.number(leaseRIWeightPercent, '0.00000000'));
                                       
                                    // var RIValue = parseFloat(leaseRIWeightPercent * totalValueRI);
                                    // me.down('#royaltyInterestValueItemId').setValue(Ext.util.Format.number(RIValue, '$000,000'));
                                    if(me.id.toLowerCase().indexOf("edit") == -1){
                                        var combo = me.down('combobox[name=leaseId]');
                                        var comboStore = combo.getStore();
                                        var selectedRec = comboStore.getAt(comboStore.find(combo.name, combo.value, false, false, true));
                                        var leaseRIV = selectedRec.get('royaltyInterestValue') || 0;
                                            
                                        var RIValue = parseInt(leaseRIV);
                                        me.down('#royaltyInterestValueItemId').setValue(Ext.util.Format.number(RIValue, '$000,000'));
                                    }
                                    else{
                                        var RIValue = parseFloat(leaseUnitRI * totalValueRI);
                                        me.down('#royaltyInterestValueItemId').setValue(Ext.util.Format.number(RIValue, '$000,000'));
                                    } 
                                } else {
                                    me.down('#royaltyInterestValueItemId').hide();
                                    // me.down('#royaltyInterestPercentItemId').setValue(Ext.util.Format.number(RIinterestPct, '0.00000000'));
                                    me.down('#totalRoyaltyInterestPercentItemId').setValue(parseFloat(totalRIPct).toFixed(8));
                                    leaseRIWeight.setValue(Ext.util.Format.number(RIinterestPct, '0.00000000')); 
                                }
                            },
                        },
                        validator: function (val) {
                            if (val <= 1) {
                                return true;
                            } else if (val == '.') {
                                return "Value must contain a number.";
                            } else {
                                return "Value cannot be greater than 1.00000000.";
                            }
                        }
                     }, {
                        fieldLabel: 'Lease RI',
                        name: 'royaltyInterestPercent',
                        xtype: 'textfield',
                        readOnly: true,
                        margin: '0 0 10',
                        itemId: 'royaltyInterestPercentItemId',
                        labelAlign: 'right',
//                        hidden: true,
                        fieldStyle: 'background-color: 	#D8D8D8;'
                        //disabled: true         
                        
                        
                    }, {
                        fieldLabel: 'Lease RI Value',
                        name: 'royaltyInterestValue',
                        xtype: 'textfield',
                        readOnly: true,
                        margin: '0 0 10',
                        itemId: 'royaltyInterestValueItemId',
                        labelAlign: 'right',
                        hidden: true,
                        fieldStyle: 'background-color: 	#D8D8D8;'
                        //disabled: true                             
                    }, {
                        fieldLabel: 'Total Unit RI Weight',
                        name: 'totalRoyaltyInterestPercent',
                        xtype: 'textfield',
                        readOnly: true,
                        margin: '0 0 10',
                        itemId: 'totalRoyaltyInterestPercentItemId',
                        labelAlign: 'right',
                        listeners: {
                            change: function (record) {
                                if (record.getRawValue() == 1) {
                                    this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_good.png) no-repeat right;');
                                } else if (record.getRawValue() < 1) {
                                    this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_upper.png) no-repeat right;');
                                } else {
                                    this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_lower.png) no-repeat right;');
                                }
                            }
                        }
                        //disabled: true     
                    }, {
                        xtype: 'hidden',
                        name: 'totalWorkingInterestValue',
                        queryMode: 'local',
                        itemId: 'totalWorkingInterestValueId'
                    }, {
                        xtype: 'hidden',
                        name: 'totalRoyaltyInterestValue',
                        queryMode: 'local',
                        itemId: 'totalRoyaltyInterestValueId'
                    }, {
                        xtype: 'hidden',
                        name: 'workingInterestPercentWeight',
                        queryMode: 'local',
                        itemId: 'workingInterestPercentWeightItemId'
                    }, {
                        xtype: 'hidden',
                        name: 'royaltyInterestPercentWeight',
                        queryMode: 'local',
                        itemId: 'royaltyInterestPercentWeightItemId'    
                    },{
                        name: 'oldValueRI',   
                        xtype: 'hidden',
                        itemId:'oldValueRIItemId',
                        queryMode:'local', 
                    },{
                        name: 'oldValueWI',   
                        xtype: 'hidden',
                        itemId:'oldValueWIItemId',
                        queryMode:'local',                                            
                    }],
            }];


        me.listeners = {
            afterrender: function () {
                var me = this;
                var mainView = Ext.getCmp(me.mainPanelId);
                var unitName = mainView.selectedName;
                me.down('#unitName').setValue(unitName);

                var unitNameId = mainView.selectedId;
                me.down('#unitNameId').setValue(unitNameId);

                var listGrid = mainView.down(me.listGridAlias);
                var totalWI = 0;
                var totalRI = 0;

                listGrid.getStore().each(function (c) {
                    if (c.data.rowDeleteFlag == '') {
                        totalWI = +totalWI + +c.data.unitWorkingInterestPercent;
                        totalRI = +totalRI + +c.data.unitRoyaltyInterestPercent;
                    }
                });

                me.down('#totalWorkingInterestPercentItemId').setValue(Ext.util.Format.number(totalWI, '0.00000000'));
                me.down('#totalRoyaltyInterestPercentItemId').setValue(Ext.util.Format.number(totalRI, '0.00000000'));

                var totalRIValue = 0;
                var totalWIValue = 0;
                if (listGrid.getStore().getCount() > 0) {
                    totalRIValue = listGrid.getStore().getAt(0).get('totalRoyaltyInterestValue');
                    totalWIValue = listGrid.getStore().getAt(0).get('totalWorkingInterestValue');
                }
                me.down('#totalWorkingInterestValueId').setValue(totalWIValue);
                me.down('#totalRoyaltyInterestValueId').setValue(totalRIValue);
                
                
                if(!me.down('#oldValueWIItemId').getValue()){
                        me.down('#oldValueWIItemId').setValue(me.down('#unitWIItemId').getRawValue());    
                }   
                if(!me.down('#oldValueRIItemId').getValue()){
                        me.down('#oldValueRIItemId').setValue(me.down('#unitRIItemId').getRawValue());    
                }  


            }}

        me.callParent(arguments);
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
        MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});