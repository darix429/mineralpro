Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitLeases.UnitLeasesEditFormView', {
//    extend: 'MineralPro.config.view.BaseAddEditFormView',
    extend: 'Minerals.sub.MineralValuationManageUnit.view.UnitLeases.UnitLeasesAddFormView',
    requires: ['Minerals.sub.MineralValuationManageUnit.view.UnitLeases.UnitLeasesAddFormViewController'],
    controller: 'UnitLeasesAddFormViewController',
    alias: 'widget.UnitLeasesEditFormView',
    title: 'Edit Lease',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#leaseItemId',
    width: 450,

    mainPanelId: 'MineralValuationManageUnitId', //id of the main panel
    listGridStore: 'Minerals.sub.MineralValuationManageUnit.store.UnitLeasesStore', //store for unit grid
    listGridAlias: 'UnitLeasesGridView', //alias name for unit gridview


    initComponent: function () {
        var me = this; 
//            me.items = [{
//            xtype: 'form',
//            defaults:{ 
//                maskRe: /[^'\^]/,
//                labelWidth: 130,
//                width: 400
//            },
//            bodyPadding:5,
//            items: [{
//                fieldLabel: 'Unit Name',
//                xtype: 'displayfield',
//                labelAlign: 'right',
//                msgTarget: 'side',
//                margin: '0 0 10',
//                name: 'unitName',
//                itemId: 'unitName',
//            },{       
//                xtype: 'hidden',     
//                name: 'unitId',   
//                queryMode:'local',
//                itemId:'unitNameId' 
//            },{
//                fieldLabel: 'Lease Name',
//                xtype: 'combo',
//                selectOnFocus: true,
//                tabIndex: 1,
//                disabled: true,
//                itemId: 'leaseItemId',
//                displayField: 'leaseName',
//                valueField: 'leaseId',
//                queryMode: 'local',
//                store: 'Minerals.sub.MineralValuationManageUnit.store.UnitLeasesLeaseStore',
//                listeners:{
//                    select:function(record){
//                        me.down('#leaseNameItemId').setValue(this.getRawValue());
//                    }, 
//                     //blur: 'onAddCheckExisting' 
//                },
//                name: 'leaseId',
//                typeAhead: true,
//                forceSelection: true,
//                allowBlank: false,
//                labelAlign: 'right', 
//                   
//             },{       
//				xtype: 'hidden',     
//				name: 'leaseName',   
//				queryMode:'local',
//				itemId:'leaseNameItemId' 
//			},{          
//                fieldLabel: 'Tract No.',               
//                //selectOnFocus: true,
//                tabIndex: 2,
//                maskRe:/[0-9.]/,
//                itemId: 'tractNoItemId',
//                name: 'tractNum',
//                msgTarget: 'side',
//                xtype: 'numberfield',
//                margin: '0 0 10',
//                labelAlign: 'right',
//                validateBlank: true,
//                minValue: 0,
//                listeners: {
//                    blur: 'onAddCheckExistingNumber'
//                 }
//            },{
//                fieldLabel: 'Lease WI',
//              //  selectOnFocus: true,
//                tabIndex: 3,
//                itemId: 'unitWIItemId',
//                name: 'unitWorkingInterestPercent',
//                xtype: 'textfield',
//                margin: '0 0 10',
//                labelAlign: 'right',
//                validateBlank: true,
//                allowBlank: false,
//                maxValue: 999999,
//                msgTarget: 'side',
//                decimalPrecision : 8,
//                decimalSeparator: '.',
//                maskRe:/[0-9.]/,
//                minValue: 0.00000000,
//                enableKeyEvents: true,
//                listeners:{
//                     initialize: function(){
//                            var nfield = Ext.get('weightfield');
//                            nfield.down('input').set({pattern:'[0-9]*'});
//                     },
//                      keyup: function(data, e, eOpts){
//                            var leaseUnitWI = this.getRawValue();
//                            var mainView = Ext.getCmp(me.mainPanelId);
//                            var listGrid = mainView.down(me.listGridAlias);
//                            var baseValueWI = me.down('#oldValueWIItemId').getValue();
//                            var WIinterestPct = 0;
//                            listGrid.getStore().each(function(c){
//                                if(c.data.rowDeleteFlag == ''){
//                                    WIinterestPct = +WIinterestPct + +c.data.unitWorkingInterestPercent;
//                                }
//                            });
//
//                            var totalbaseWIPct = parseFloat(WIinterestPct) - parseFloat(baseValueWI);
//                            var totalWIPct = parseFloat(leaseUnitWI) + parseFloat(totalbaseWIPct);
//
//                            if(leaseUnitWI != ''){
//                                me.down('#workingInterestValueItemId').show();
//                                me.down('#workingInterestPercentItemId').setValue(parseFloat(totalWIPct).toFixed(8));
//                                var totalValueWI = me.down('#totalWorkingInterestValueId').getRawValue();
//                                var WIValue = parseFloat(leaseUnitWI * totalValueWI);
//                                me.down('#workingInterestValueItemId').setValue(Ext.util.Format.number(WIValue, '$000,000'));
//                            }else{
//                                me.down('#workingInterestValueItemId').hide();
//                                me.down('#workingInterestPercentItemId').setValue(Ext.util.Format.number(totalbaseWIPct,'0.00000000'));
//                            }
//                      },     
//                },
//                validator: function(val) {
//                    if (val <= 1) {
//                        return true;
//                    }else if(val == '.'){
//                        return "Value must contain a number.";
//                    }else {
//                        return "Value cannot be greater than 1.00000000.";
//                    }
//                }
//            },{
//                fieldLabel: 'WI Value',
//                name: 'workingInterestValue',   
//                xtype: 'textfield',
//                readOnly: true,
//                margin: '0 0 10',
//                itemId:'workingInterestValueItemId',
//                labelAlign: 'right',
//                fieldStyle: 'background-color: #D8D8D8;',
//                listeners : {
//                    afterrender: function(me){
//                            me.setValue(Ext.util.Format.number(me.getRawValue(), '$000,000'));
//                    }
//                }
//               // hidden: true,
//                //disabled: true     
//            },{
//                fieldLabel: 'Unit WI Weight',
//                name: 'workingInterestPercent',   
//                xtype: 'textfield',
//                readOnly: true,
//                margin: '0 0 10',
//                itemId:'workingInterestPercentItemId',
//                labelAlign: 'right',
//                listeners : {
//                    change: function(record) {
//                        if(record.getRawValue() == 1){
//                                this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_good.png) no-repeat right;');
//                        }else if(record.getRawValue() < 1){
//                                this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_upper.png) no-repeat right;');
//                        }else{
//                                this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_lower.png) no-repeat right;');
//                        }
//                    }
//                }
//                //hidden: true,
//                //disabled: true     
//            },{
//                fieldLabel: 'Lease RI',
//               // selectOnFocus: true,
//                tabIndex: 4,
//                msgTarget: 'side',
//                itemId: 'unitRIItemId',
//                name: 'unitRoyaltyInterestPercent',
//                xtype: 'textfield',
//                margin: '0 0 10',
//                labelAlign: 'right',
//                validateBlank: true,
//                allowBlank: false,
//                maxValue: 999999,
//                decimalPrecision : 8,
//                decimalSeparator: '.',
//                maskRe:/[0-9.]/,
//                minValue: 0.00000000,
//                enableKeyEvents: true,
//                listeners:{
//                     initialize: function(){
//                            var nfield = Ext.get('weightfield');
//                            nfield.down('input').set({pattern:'[0-9]*'});
//                     },
//                     keyup: function(data, e, eOpts){
//                            var leaseUnitRI = this.getRawValue();
//                            var mainView = Ext.getCmp(me.mainPanelId);
//                            var listGrid = mainView.down(me.listGridAlias);
//                            var baseValueRI = me.down('#oldValueRIItemId').getValue();
//                            var RIinterestPct = 0;
//                            listGrid.getStore().each(function(c){
//                                if(c.data.rowDeleteFlag == ''){
//                                    RIinterestPct = +RIinterestPct + +c.data.unitRoyaltyInterestPercent;
//                                }
//                            });
//                            
//                            var totalbaseRIPct = parseFloat(RIinterestPct) - parseFloat(baseValueRI);
//                            var totalRIPct = parseFloat(leaseUnitRI) + parseFloat(totalbaseRIPct);
//                           
//                            if(leaseUnitRI != ''){
//                               // me.down('#royaltyInterestValueItemId').show();
//                                me.down('#royaltyInterestPercentItemId').setValue(parseFloat(totalRIPct).toFixed(8));
//                                var totalValueRI = me.down('#totalRoyaltyInterestValueId').getRawValue();
//                                var RIValue = parseFloat(leaseUnitRI * totalValueRI);
//                                me.down('#royaltyInterestValueItemId').setValue(Ext.util.Format.number(RIValue, '$000,000'));
//                            }else{
//                               // me.down('#royaltyInterestValueItemId').hide();
//                                me.down('#royaltyInterestPercentItemId').setValue(Ext.util.Format.number(totalbaseRIPct,'0.00000000'));
//                            }
//                      }, 
//                },
//                validator: function(val) {
//                    if (val <= 1) {
//                        return true;
//                    }else if(val == '.'){
//                        return "Value must contain a number.";
//                    }else {
//                        return "Value cannot be greater than 1.00000000.";
//                    }
//                }
//            },{
//                fieldLabel: 'RI Value',
//                name: 'royaltyInterestValue',   
//                xtype: 'textfield',
//                readOnly: true,
//                margin: '0 0 10',
//                itemId:'royaltyInterestValueItemId',
//                labelAlign: 'right',
//                fieldStyle: 'background-color: #D8D8D8;',
//                listeners : {
//                    afterrender: function(me){
//                            me.setValue(Ext.util.Format.number(me.getRawValue(), '$000,000'));
//                    }
//                }
//               // hidden: true,
//                //disabled: true     
//            },{
//                fieldLabel: 'Unit RI Weight',
//                name: 'royaltyInterestPercent',   
//                xtype: 'textfield',
//                readOnly: true,
//                margin: '0 0 10',
//                itemId:'royaltyInterestPercentItemId',
//                labelAlign: 'right',
//                listeners : {
//                    change: function(record) {
//                        if(record.getRawValue() == 1){
//                                this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_good.png) no-repeat right;');
//                        }else if(record.getRawValue() < 1){
//                                this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_upper.png) no-repeat right;');
//                        }else{
//                                this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_lower.png) no-repeat right;');
//                        }
//                    }
//                }
//                //disabled: true     
//            },{       
//                xtype: 'hidden',     
//                name: 'totalWorkingInterestValue',   
//                queryMode:'local',
//                itemId:'totalWorkingInterestValueId' 
//            },{       
//                xtype: 'hidden',     
//                name: 'totalRoyaltyInterestValue',   
//                queryMode:'local',
//                itemId:'totalRoyaltyInterestValueId' 
//            },{
//                name: 'oldValueRI',   
//                xtype: 'hidden',
//                itemId:'oldValueRIItemId',
//                queryMode:'local', 
//             },{
//                name: 'oldValueWI',   
//                xtype: 'hidden',
//                itemId:'oldValueWIItemId',
//                queryMode:'local', 
//             }],
//        }];
//
//        
//        me.listeners = {
//        afterrender: function () {  
//                var me = this;      
//                var mainView = Ext.getCmp(me.mainPanelId);
//                var unitName = mainView.selectedName;
//                me.down('#unitName').setValue(unitName);
//
//                var unitNameId = mainView.selectedId;
//                me.down('#unitNameId').setValue(unitNameId);  
//
//                var listGrid = mainView.down(me.listGridAlias);
//                var totalWI = 0;
//                var totalRI = 0;
//
//                listGrid.getStore().each(function(c){
//                    if(c.data.rowDeleteFlag == ''){
//                        totalWI = +totalWI + +c.data.unitWorkingInterestPercent;
//                        totalRI = +totalRI + +c.data.unitRoyaltyInterestPercent;
//                    }
//                });
//
//                me.down('#workingInterestPercentItemId').setValue(Ext.util.Format.number(totalWI,'0.00000000'));
//                me.down('#royaltyInterestPercentItemId').setValue(Ext.util.Format.number(totalRI,'0.00000000'));
//
//                var totalRIValue = listGrid.getStore().getAt(0).get('totalRoyaltyInterestValue');
//                var totalWIValue = listGrid.getStore().getAt(0).get('totalWorkingInterestValue');
//            
//                me.down('#totalWorkingInterestValueId').setValue(totalWIValue);  
//                me.down('#totalRoyaltyInterestValueId').setValue(totalRIValue);      
//
//                if(!me.down('#oldValueWIItemId').getValue()){
//                        me.down('#oldValueWIItemId').setValue(me.down('#unitWIItemId').getRawValue());    
//                }   
//                if(!me.down('#oldValueRIItemId').getValue()){
//                        me.down('#oldValueRIItemId').setValue(me.down('#unitRIItemId').getRawValue());    
//                }                                                                                      
//        }}
//
        me.callParent(arguments);
    },
    listeners: {
        render: function(me){
            me.down('#leaseItemId').disable();
        }
    },
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});