
Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitLeases.UnitLeasesGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.UnitLeasesGridView',
    store:'Minerals.sub.MineralValuationManageUnit.store.UnitLeasesStore',

    requires: ['Minerals.sub.MineralValuationManageUnit.view.UnitLeases.UnitLeasesGridViewController'],
    controller: 'UnitLeasesGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'leaseId', 
    mainPanelAlias: 'MineralValuationManageUnit', //widget alias name of the main panel
    mainListModel: 'Minerals.sub.MineralValuationManageUnit.model.UnitLeasesModel',     

    addFormAliasName: 'UnitLeasesAddFormView', //widget form to call for add list 
    editFormAliasName: 'UnitLeasesEditFormView', //widget form to call for edit
   

    firstFocus: 'leaseId',    
    
    addButtonTooltip: 'Add New Lease',
    addButtonText: 'Add New Lease',
    features: [{
          id: 'group',
          ftype: 'groupingsummary',
          groupHeaderTpl: '{columnName}: {name}',
//          hideGroupedHeader: true,
          enableGroupingMenu: true,
          startCollapsed: false  
      }],     
         
         
    columns:[{       
        header: 'Lease No.',
        flex     : 1,
        dataIndex: 'leaseId',
        itemId: 'leaseId',
        filterElement: new Ext.form.TextField()
    },{   
        header: 'Lease Name',
        flex     : 2,
        dataIndex: 'leaseName',
        filterElement:new Ext.form.TextField(),
        renderer: function (value) {
                        return '<a href="javascript:void(0);" >' + value + '</a>';
                    },
        listeners: {
            click: function (cell, td, rowIndex, cellIndex, e, record, value, index) {
                   this.up('grid').getController().onOpenSelectedTabView(record, rowIndex);
            }
        }
    },{   
        header: 'Tract No.',
        flex     : 1,
        dataIndex: 'tractNum',
        filterElement:new Ext.form.TextField(),
        editor: {
            allowBlank: false,
            xtype: 'textfield',
            maskRe:/[0-9.]/,
            minValue: 0,
            maxValue: 100
        }
    },{   
        header: 'Unit WI Weight',
        flex     : 1,
        dataIndex: 'unitWorkingInterestPercent',
        filterElement:new Ext.form.TextField(),
        editor: {
            allowBlank: false,
            xtype: 'textfield',
            maskRe:/[0-9.]/,
            minValue: 0,
            maxValue: 1
        },
        summaryType: 'sum',
        summaryRenderer: function(val, params, data) {
           var me = this;
           var gridStore = me.up('grid').getStore();
           var dataIndex = me.dataIndex;
           var ret = 0;
           gridStore.each(function(rec){
               if(rec.get('rowDeleteFlag') == ''){
                   ret=parseFloat(ret) + parseFloat(rec.get(dataIndex))
               }                 
           })
           return  Ext.util.Format.number(ret, '0.00000000');
        },
        renderer: function(val, meta) {
            return Ext.util.Format.number(val, '0.00000000');
        },    
    },{   
        header: 'Unit RI Weight',
        flex     : 1,
        dataIndex: 'unitRoyaltyInterestPercent',
        filterElement:new Ext.form.TextField(),
        editor: {
            allowBlank: false,
            xtype: 'textfield',
            maskRe:/[0-9.]/,
            minValue: 0,
            maxValue: 1
        },
        summaryType: 'sum',
        summaryRenderer: function(val, params, data) {
            var me = this;
            var gridStore = me.up('grid').getStore();
            var dataIndex = me.dataIndex;
            var ret = 0;
            gridStore.each(function(rec){
                if(rec.get('rowDeleteFlag') == ''){
                    ret=parseFloat(ret) + parseFloat(rec.get(dataIndex))
                }                   
            })
            return  Ext.util.Format.number(ret, '0.00000000');
        },
        renderer: function(val, meta) {
            return Ext.util.Format.number(val, '0.00000000');
        },    
    },{   
        header: 'Lease WI',
        flex     : 1,
        dataIndex: 'workingInterestPercent',
        filterElement:new Ext.form.TextField(),
        // summaryType: 'sum',
        // summaryRenderer: function(val, params, data) {
        //     var me = this;
        //     var gridStore = me.up('grid').getStore();
        //     var dataIndex = me.dataIndex;
        //     var ret = 0;
        //     gridStore.each(function(rec){
        //         if(rec.get('rowDeleteFlag') == ''){
        //             ret=parseFloat(ret) + parseFloat(rec.get(dataIndex))
        //         }                   
        //     })
        //     return  Ext.util.Format.number(ret, '0.00000000');
        // },
        renderer: function(val, meta) {
            return Ext.util.Format.number(val, '0.00000000');
        },    
    },{   
        header: 'Lease RI',
        flex     : 1,
        dataIndex: 'royaltyInterestPercent',
        filterElement:new Ext.form.TextField(),
        // summaryType: 'sum',
        // summaryRenderer: function(val, params, data) {
        //     var me = this;
        //     var gridStore = me.up('grid').getStore();
        //     var dataIndex = me.dataIndex;
        //     var ret = 0;
        //     gridStore.each(function(rec){
        //         if(rec.get('rowDeleteFlag') == ''){
        //             ret=parseFloat(ret) + parseFloat(rec.get(dataIndex))
        //         }                   
        //     })
        //     return  Ext.util.Format.number(ret, '0.00000000');
        // },
        renderer: function(val, meta) {
            return Ext.util.Format.number(val, '0.00000000');
        },       
    },{   
         header: 'Weighted Lease WI',
        flex     : 1,
        dataIndex: 'workingInterestPercentWeight',
        filterElement:new Ext.form.TextField(),
        // summaryType: 'sum',
        // summaryRenderer: function(val, params, data) {
        //     var me = this;
        //     var gridStore = me.up('grid').getStore();
        //     var dataIndex = me.dataIndex;
        //     var ret = 0;
        //     gridStore.each(function(rec){
        //         if(rec.get('rowDeleteFlag') == ''){
        //             ret=parseFloat(ret) + parseFloat(rec.get(dataIndex))
        //         }                   
        //     })
        //     return  Ext.util.Format.number(ret, '0.00000000');
        // },
        renderer: function(val, meta) {
            return Ext.util.Format.number(val, '0.00000000');
        },    
    },{   
        header: 'Weighted Lease RI',
        flex     : 1,
        dataIndex: 'royaltyInterestPercentWeight',
        filterElement:new Ext.form.TextField(),
        // summaryType: 'sum',
        // summaryRenderer: function(val, params, data) {
        //     var me = this;
        //     var gridStore = me.up('grid').getStore();
        //     var dataIndex = me.dataIndex;
        //     var ret = 0;
        //     gridStore.each(function(rec){
        //         if(rec.get('rowDeleteFlag') == ''){
        //             ret=parseFloat(ret) + parseFloat(rec.get(dataIndex))
        //         }                   
        //     })
        //     return  Ext.util.Format.number(ret, '0.00000000');
        // },
        renderer: function(val, meta) {
            return Ext.util.Format.number(val, '0.00000000');
        },       
    },{                    
        header: 'Lease WI Value',
        flex     : 1,
        align: 'right',
        dataIndex: 'workingInterestValue',
        filterElement:new Ext.form.TextField(),
        summaryType: 'sum',
        summaryRenderer: function(val, params, data) {
            var me = this;
            var gridStore = me.up('grid').getStore();
            var dataIndex = me.dataIndex;
            var ret = 0;
            gridStore.each(function(rec){
                if(rec.get('rowDeleteFlag') == ''){
                    ret=parseFloat(ret) + parseFloat(rec.get(dataIndex))
                }                   
            })
            return  Ext.util.Format.number(ret, '$000,000');
        },
        renderer: function(val, meta) {
            return  Ext.util.Format.number(val, '$000,000');
        },    
    },{   
        header: 'Lease RI Value',
        flex     : 1,
        align: 'right',
        dataIndex: 'royaltyInterestValue',
        filterElement:new Ext.form.TextField(),
        summaryType: 'sum',
        summaryRenderer: function(val, params, data) {
            var me = this;
            var gridStore = me.up('grid').getStore();
            var dataIndex = me.dataIndex;
            var ret = 0;
            gridStore.each(function(rec){
                if(rec.get('rowDeleteFlag') == ''){
                    ret=parseFloat(ret) + parseFloat(rec.get(dataIndex))
                }                   
            })
            return  Ext.util.Format.number(ret, '$000,000');
        },
        renderer: function(val, meta) {
            return  Ext.util.Format.number(val, '$000,000');
        },   
    },{
     xtype: 'actioncolumn',
        width: Minerals.config.Runtime.actionColWidth,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            {
                getClass: function (colConfig, cellData, record, rowIndex, colIndex, dataStore) {
                    if (record.get('rowDeleteFlag') == 'D') {
                        return(MineralPro.config.Runtime.undoDeleteCls);
                    } else {
                        return(MineralPro.config.Runtime.deleteCls);
                    }
                },
                getTip: function (colConfig, cellData, record, rowIndex, colIndex, dataStore) {
                    if (record.get('rowDeleteFlag') == 'D') {
                        return (MineralPro.config.Runtime.undoDeleteTooltip);
                    } else {
                        return (MineralPro.config.Runtime.deleteTooltip);
                    }
                },
                handler: function (gridView, rowIndex, colIndex, clickedItem, eventObj, record, row) {
                    this.up('grid').getSelectionModel().select(rowIndex)                    
                    if (record.get('rowDeleteFlag') == 'D') {
                        record.set('rowDeleteFlag', record.previousValues.rowDeleteFlag);                                      
                    } else {
                        record.set('rowDeleteFlag', 'D');            
                    }
                    this.up('grid').getController().reCalcValues(this.up('grid'))
                }
            }                    
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    }],
    listeners: {
         validateedit: 'onValidateEdit',
    },
   
    initComponent: function () {  
        var me = this;
               
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow'),
            Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2
            })],     

               
        me.tools= [
        MineralPro.config.util.MainGridToolCmpt.addNewDataButton
        ,MineralPro.config.util.MainGridToolCmpt.saveToDbButton                   
        ,MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
        me.callParent(arguments);
        
    }  
});
