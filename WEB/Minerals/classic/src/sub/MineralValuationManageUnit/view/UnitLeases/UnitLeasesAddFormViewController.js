//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitLeases.UnitLeasesAddFormViewController', {
    extend: 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.UnitLeasesAddFormViewController', 

    onAddAndNew: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering UnitLeasesAddFormViewController onAddAndNew.')
        var me = this;                     
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore); 
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
        var mainView = Ext.getCmp(view.mainPanelId);
        var unitNameId = mainView.selectedId; 

        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();  
        if(form.isValid()){
            if(values.royaltyInterestPercent <= 1 && values.workingInterestPercent <= 1){
                record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
                record.set(values);    
                record.set('unitId', unitNameId);
                record.set('Unit Name', mainView.selectedName);
                record.set('new', true);
                listStore.insert(0,record);

                listGrid.getController().reCalcValues(listGrid);
                
                view.down('#workingInterestValueItemId').hide();
                view.down('#royaltyInterestValueItemId').hide();
                var totalWI = 0;
                var totalRI = 0;

                listGrid.getStore().each(function(rec){
                    if(rec.data.rowDeleteFlag == ''){
                        totalWI = +totalWI + +rec.data.unitWorkingInterestPercent;
                        totalRI = +totalRI + +rec.data.unitRoyaltyInterestPercent;
                    }
                });

                var totalRIValue = listGrid.getStore().getAt(0).get('totalRoyaltyInterestValue');
                var totalWIValue = listGrid.getStore().getAt(0).get('totalWorkingInterestValue');
                listGrid.getSelectionModel().select(0, true);  
                var new_record = Ext.create(listGrid.mainListModel)                    
                form.loadRecord(new_record);
                view.focus(view.defaultFocus)
                view.down('#workingInterestPercentItemId').setValue(Ext.util.Format.number(totalWI,'0.00000000'));
                view.down('#royaltyInterestPercentItemId').setValue(Ext.util.Format.number(totalRI,'0.00000000'));
                view.down('#totalWorkingInterestValueId').setValue(totalWIValue);  
                view.down('#totalRoyaltyInterestValueId').setValue(totalRIValue);

            }else if(values.royaltyInterestPercent <= 1 && values.workingInterestPercent > 1){
                Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: 'Unit WI Pct Exceed',    
                    msg: 'The total Unit WI Percent have Exceeded 1.00000000',              
                    buttons: Ext.Msg.OK
                });
            }else if(values.workingInterestPercent <= 1 && values.royaltyInterestPercent > 1){
                Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: 'Unit RI Pct Exceed',    
                    msg: 'The total Unit RI Percent have Exceeded 1.00000000',              
                    buttons: Ext.Msg.OK
                });
            }else{
                 Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: 'Unit WI and RI Pct Exceed',    
                    msg: 'The total Unit WI and RI Percent </br> have Exceeded 1.00000000',              
                    buttons: Ext.Msg.OK
                });
            }
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        }  
        MineralPro.config.RuntimeUtility.DebugLog('Leaving UnitLeasesAddFormViewController onAddAndNew.')
    },
    
    /**
     * Add the data from the form into the data grid
     */
    onAddListToGrid: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering UnitLeasesAddFormViewController onAddListToGrid.')
        var me = this;                     
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore); 
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
        var mainView = Ext.getCmp(view.mainPanelId);
        var unitNameId = mainView.selectedId; 
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();  

        if(form.isValid()){
            if(values.royaltyInterestPercent <= 1 && values.workingInterestPercent <= 1){

                record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
                record.set(values);    
                record.set('unitId', unitNameId);
                record.set('Unit Name', mainView.selectedName);
                record.set('new', true);

                win.close();
                listStore.insert(0,record);

                listGrid.getController().reCalcValues(listGrid);

                listGrid.getSelectionModel().select(0, true);  
                listGrid.getView().focusRow(0);   
                
                
            }else if(values.royaltyInterestPercent <= 1 && values.workingInterestPercent > 1){
                Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: 'Unit WI Pct Exceed',    
                    msg: 'The total Unit WI Percent have Exceeded 1.00000000',              
                    buttons: Ext.Msg.OK
                });
            }else if(values.workingInterestPercent <= 1 && values.royaltyInterestPercent > 1){
                Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: 'Unit RI Pct Exceed',    
                    msg: 'The total Unit RI Percent have Exceeded 1.00000000',              
                    buttons: Ext.Msg.OK
                });
            }else{
                 Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: 'Unit WI and RI Pct Exceed',    
                    msg: 'The total Unit WI and RI Percent </br> have Exceeded 1.00000000',              
                    buttons: Ext.Msg.OK
                });
            }
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving UnitLeasesAddFormViewController onAddListToGrid.')
    },

    onAddCheckExisting: function(e, event, opt){
        MineralPro.config.RuntimeUtility.DebugLog('Entering UnitLeasesAddFormViewController onAddCheckExisting.')
                    
        var me = this;  
        var view = me.getView();
        var tractNo = view.down('#tractNoItemId');
        var combo = view.down('#leaseItemId')
        var comboStore = combo.getStore();
        var record = combo.getSelection()
        if(record != null){
            var rec = comboStore.getAt(comboStore.find(combo.name, combo.value, false, false, true));
            var unitId = e.displayTplData[0].unitId;
            setTimeout(function (){                       
                var showAlert = false;
                var showAlertNumber = false;
                
                if(!e.destroyed){
                    var listStore = Ext.StoreMgr.lookup(view.listGridStore);

                    if (listStore.findRecord(e.name, e.value, 0, false, false, true)) {
                        showAlert = true;
                        if (view.id.toLowerCase().indexOf("edit") != -1) {
                            if (listStore.getAt(view.editRecordIndex).get(e.name) == e.value) {
                                showAlert = false;
                            }
                        }                                     
                    }
                    
                    if (listStore.findRecord(tractNo.name, tractNo.value, 0, false, false, true)) {
                        showAlertNumber = true;
                        if (view.id.toLowerCase().indexOf("edit") != -1) {
                            if (listStore.getAt(view.editRecordIndex).get(tractNo.name) == tractNo.value) {
                                showAlertNumber = false;
                            }
                        }                                     
                    }
                    
                    if(showAlert){
                        Ext.Msg.show({
                            iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
                            title:'Alert',
                            msg: 'Lease is already in your list.',
                            buttons: Ext.Msg.OK,
                            fn: function(){
                                e.focus();
                            }
                        });
                    }else if(unitId != 0){
                        Ext.Msg.show({
                            iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
                            title:'Alert',
                            msg: 'This lease is already in another unit, please remove it from the other unit before adding to this unit.',
                            buttons: Ext.Msg.OK,
                            width: 250,
                            fn: function(){
                                e.focus();
                            }
                        });
                    }else if(rec.get('subjectId')!= null && rec.get('subjectId')!=''){
                        Ext.Msg.show({
                            title: 'Alert',
                            msg: 'Lease already has an existing appraisal. Do you want to delete existing lease appraisal? ',
                            iconCls: MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                            buttons: Ext.Msg.YESNO,
                            scope: this,
                            width: 250,
                            fn: function (btn) {
                                if (btn === 'no') { 
                                combo.setValue('');
                                combo.focus();
                                }
                            }
                        });
                    }else if(showAlertNumber){
                        Ext.Msg.show({
                            iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
                            title:'Alert',
                            msg: 'Tract Number is already assigned.',
                            buttons: Ext.Msg.OK,
                            fn: function(){
                                tractNo.focus();
                            }
                        });
                    }  
                }          
            },200)
        }
//        
//         setTimeout(function (){                       
//            if (view.id.toLowerCase().indexOf("edit") == -1) {
//
//                var listStore = Ext.StoreMgr.lookup(view.listGridStore);
//
//                if (listStore.findRecord(e.name, e.value, 0, false, false, true)) {
//                    Ext.Msg.show({
//                        iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
//                        title:'Alert',
//                        msg: 'Lease is already in your list.',
//                        buttons: Ext.Msg.OK,
//                        fn: function(){
//                            e.focus();
//                        }
//                    });
//                }
//            }
//        },100)
        
                             
        MineralPro.config.RuntimeUtility.DebugLog('Leaving UnitLeasesAddFormViewController onAddCheckExisting.')    
    },
     onAddCheckExistingNumber: function(e, event, opt){
        MineralPro.config.RuntimeUtility.DebugLog('Entering UnitLeasesAddFormViewController onAddCheckExistingNumber.')
        
        var me = this;  
        var view = me.getView();
        setTimeout(function (){                       
            var showAlert = false;

            if(!e.destroyed){
                var listStore = Ext.StoreMgr.lookup(view.listGridStore);

                if (listStore.findRecord(e.name, e.value, 0, false, false, true)) {
                    showAlert = true;
                    if (view.id.toLowerCase().indexOf("edit") != -1) {
                        if (listStore.getAt(view.editRecordIndex).get(e.name) == e.value) {
                            showAlert = false;
                        }
                    }                                     
                }
                if(showAlert){
                     Ext.Msg.show({
                        iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
                        title:'Alert',
                        msg: 'Tract Number is already assigned.',
                        buttons: Ext.Msg.OK,
                        fn: function(){
                            e.focus();
                        }
                    });
                }     
            }       
        },200)
        
//         setTimeout(function (){                       
//            if (view.id.toLowerCase().indexOf("edit") == -1) {
//
//                var listStore = Ext.StoreMgr.lookup(view.listGridStore);
//
//                if (listStore.findRecord(e.name, e.value, 0, false, false, true)) {
//                    Ext.Msg.show({
//                        iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
//                        title:'Alert',
//                        msg: 'Tract Number is already assigned.',
//                        buttons: Ext.Msg.OK,
//                        fn: function(){
//                            e.focus();
//                        }
//                    });
//                }
//            }
//        },100)
        
                             
        MineralPro.config.RuntimeUtility.DebugLog('Leaving UnitLeasesAddFormViewController onAddCheckExistingNumber.')    
    },
    onEditListToGrid: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering UnitLeasesAddFormViewController onEditListToGrid.')

        var me = this;        
        var view = me.getView();
        var mainPanel = Ext.getCmp(view.mainPanelId);
        var listGrid = mainPanel.down(view.listGridAlias)
        
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();
        
        if(form.isValid()){
            if(values.royaltyInterestPercent <= 1 && values.workingInterestPercent <= 1){
                record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                record.set(values);                                
                win.close(); 
                
                listGrid.getController().reCalcValues(listGrid);
             }else if(values.royaltyInterestPercent <= 1 && values.workingInterestPercent > 1){
                Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: 'Unit WI Pct Exceed',    
                    msg: 'The total Unit WI Percent have Exceeded 1.00000000',              
                    buttons: Ext.Msg.OK
                });
            }else if(values.workingInterestPercent <= 1 && values.royaltyInterestPercent > 1){
                Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: 'Unit RI Pct Exceed',    
                    msg: 'The total Unit RI Percent have Exceeded 1.00000000',              
                    buttons: Ext.Msg.OK
                });
            }else{
                 Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: 'Unit WI and RI Pct Exceed',    
                    msg: 'The total Unit WI and RI Percent </br> have Exceeded 1.00000000',              
                    buttons: Ext.Msg.OK
                });  
            }                             
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        } 
        MineralPro.config.RuntimeUtility.DebugLog('Leaving UnitLeasesAddFormViewController onEditListToGrid.')
    },
    
    
});