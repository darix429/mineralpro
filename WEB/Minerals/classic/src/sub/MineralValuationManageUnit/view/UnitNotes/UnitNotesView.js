Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitNotes.UnitNotesView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.UnitNotesView',
    layout:'border',
    
    id: 'UnitNotesViewId',
    
    items: [{
            xtype: 'UnitNotesGridView',
            html:'center',
            region:'center'
        },{
        	xtype: 'UnitNoteDetailView',
        	region: 'east'
        }] 
});    