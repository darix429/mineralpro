Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitNotes.UnitNoteDetailView', {
    extend: 'MineralPro.config.view.BaseDetailView',
    alias: 'widget.UnitNoteDetailView',
    
    title: 'Note Details',
    itemId: 'UnitNoteDetails',
    defaults: {
        width: 275,
        labelWidth: 93
    },   
    items: [{
        xtype: 'textareafield',
        fieldLabel:'Note',
        name: 'note',
        anchor: '100%',
        labelWidth: 80,
        grow: true, 
        readOnly: true 
    }]
});