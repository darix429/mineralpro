
Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitNotes.UnitNotesGridView', {  
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.UnitNotesGridView',
    store:'Minerals.sub.MineralValuationManageUnit.store.UnitNotesStore',

    requires: ['Minerals.sub.MineralValuationManageUnit.view.UnitNotes.UnitNotesGridViewController'],
    controller: 'UnitNotesGridViewController',    
          
    //below are default items needed for grid checking 
    // firstLoadItemId: 'dateItemId', 
    mainPanelAlias: 'MineralValuationManageUnit', //widget alias name of the main panel
    mainListModel: 'Minerals.model.MineralValuation.NotesModel',     

    addFormAliasName: 'UnitNotesAddFormView', //widget form to call for add list 
    editFormAliasName: 'UnitNotesEditFormView', //widget form to call for edit
   

    // firstFocus: 'dateItemId',    
    
    addButtonTooltip: 'Add New Note',
    addButtonText: 'Add New Note',
    
//    fieldGridViewAlias: 'UnitFieldsGridView', //this is only for appraisal notes
        
    columns:[{   
        header: 'Ref Year',
        flex     : 2,
        dataIndex: 'referenceAppraisalYear',
        itemId: 'referenceAppraisalYearItemId',
        filterElement: new Ext.form.TextField({
        })
    },{   
        header: 'Create Date',
        flex     : 2,
        dataIndex: 'createDt',
        itemId: 'dateItemId',
        filterElement: new Ext.form.TextField({
        })
    },{   
        header: 'User',
        flex     : 2,
        dataIndex: 'noteAppraiserName',
        filterElement:new Ext.form.TextField()     
    },{   
        header: 'Note',
        flex     : 4,
        cls: 'noteCls',
        tdCls: 'noteTdCls',
        dataIndex: 'note',
        filterElement:new Ext.form.TextField() 
    },{   
        header: 'Confidential',
        flex     : 2,
        dataIndex: 'securityCd',
        renderer: function(value){
            if(value == 2){
                return 'Yes';
            }else if(value == 1){
                return 'No';
            }
        },
        filterElement : new Ext.form.ComboBox({                              
            showFilterIcon:true,
            triggerAction           : 'all',                
            typeAhead               : true,                                
            mode                    : 'local',
            listWidth               : 160,
            hideTrigger             : false,
            emptyText               : 'Select',
            store                   :[['2','Yes'],['1','No']]
        })
    },{      
        header: 'Sequence No.',
        flex     : 2,
        dataIndex: 'seqNumber',
        filterElement:new Ext.form.TextField()
    },{
        xtype: 'actioncolumn',
        width: Minerals.config.Runtime.actionColWidth,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls,
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        },
        renderer : function (v, cellValues, record, rowIdx, colIdx, store, view){
            var me = this
            var selectedYear = MineralPro.config.Runtime.appraisalYear
            var refAppraisalYear = record.data.referenceAppraisalYear
            if(refAppraisalYear*1 == selectedYear*1 || record.dirty){
                record.data.editRecord = true
                record.data.deleteRecord = true
            } else {
                record.data.editRecord = false
                record.data.deleteRecord = false
            }
        }
    }],
    listeners: {
        selectionchange: 'onLoadSelectionChange',
        viewready: function (grid) {
        var view = grid.view;
        this.toolTip = Ext.create('Ext.tip.ToolTip', {
            target: view.el,
            delegate: view.itemSelector + ' .noteTdCls',
            //delegate: view.cellSelector, //all columns
            trackMouse: true,
            renderTo: Ext.getBody(),
            listeners: {
                beforeshow: function(tip) {
                    var trigger = tip.triggerElement,
                        parent = tip.triggerElement.parentElement,
                        columnTitle = view.getHeaderByCell(trigger).text,
                        columnDataIndex = view.getHeaderByCell(trigger).dataIndex,
                        columnText = view.getRecord(parent).get(columnDataIndex).toString();
                    if (columnText){
                        tip.update("<b>" + columnTitle + ":</b> " + columnText);
                    } else {
                        return false;
                    }
                }
            }
        });
           
        }
    },
    // listeners: {
    //     afterrender: function(){
    //         var me = this;
    //         var LeaseNoteViewTypeCombo = me.down('#LeaseNoteViewTypeComboItemId');
    //         var LeaseNoteViewTypeComboStore = LeaseNoteViewTypeCombo.getStore();
    //         LeaseNoteViewTypeComboStore.load({
    //             callback: function(){
    //                 LeaseNoteViewTypeCombo.select(LeaseNoteViewTypeComboStore.getData().getAt(0));
    //             }
    //         })
    //         var LeaseNoteMineralCombo = me.down('#LeaseNoteMineralComboItemId');
    //         var LeaseNoteMineralComboStore = LeaseNoteMineralCombo.getStore();
    //         LeaseNoteMineralComboStore.load({
    //             callback: function(){
    //                 LeaseNoteMineralCombo.select(LeaseNoteMineralComboStore.getData().getAt(0));
    //             }
    //         })
    //     }
    // },
    initComponent: function () {  
        var me = this;

        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],       
               
        me.tools= [{
            xtype: 'container',
            width: '75%',
            layout: {
                type: 'hbox',    
                pack: 'end'
            },
            items: [
                MineralPro.config.util.MainGridToolCmpt.addNewDataButton,
                MineralPro.config.util.MainGridToolCmpt.saveToDbButton,                  
                MineralPro.config.util.MainGridToolCmpt.clearFilterButton
            ]
        }],   
        
        me.callParent(arguments);
        
    }
});