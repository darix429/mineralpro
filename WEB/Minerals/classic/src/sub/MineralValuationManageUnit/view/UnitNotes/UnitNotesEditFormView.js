Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitNotes.UnitNotesEditFormView', {
    extend: 'Minerals.sub.MineralValuationManageUnit.view.UnitNotes.UnitNotesAddFormView',
    //requires: ['Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteEditFormViewController'],
    //controller: 'LeaseNoteEditFormViewController',
    alias: 'widget.UnitNotesEditFormView',
    
    
    title: 'Update Unit Note',
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});