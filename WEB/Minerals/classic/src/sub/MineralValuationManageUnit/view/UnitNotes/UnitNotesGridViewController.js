Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitNotes.UnitNotesGridViewController', {
    extend: 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.UnitNotesGridViewController',

    onSyncListGridData: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering UnitNotesGridViewController onSyncListGridData.')
        var me = this;       
        var view = me.getView();
        var listStore = view.getStore();
                                  
        var dirty = listStore.getNewRecords().length > 0
        || listStore.getUpdatedRecords().length > 0
        || listStore.getRemovedRecords().length > 0;
        if (dirty) {
            Ext.Msg.show({
                title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                msg:MineralPro.config.Runtime.syncGridMsgConfimationMsg,
                iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                buttons: Ext.Msg.YESNO,
                scope: this,
                width: 250,
                fn: function (btn) {
                    if (btn === 'yes') {
                        view.getSelectionModel().deselectAll(true);
                        listStore.getProxy().extraParams = {
                            createLength: listStore.getNewRecords().length,
                            updateLength: listStore.getUpdatedRecords().length
                        };

                        listStore.sync({
                            callback: function (records, operation, success) { 
                                var mainPanel = view.up(view.mainPanelAlias)
                                if(mainPanel.selectedId){
                                    listStore.getProxy().extraParams = {
                                        selectedId: mainPanel.selectedId,
                                        selectedRRCNumber: mainPanel.selectedRRCNumber
                                    };
                                }
                                listStore.reload();
                            }
                        });
                        listStore.commitChanges();
                    }
                }
            });
        } else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                title:MineralPro.config.Runtime.listSyncMsgErrorTitle,
                msg:MineralPro.config.Runtime.listSyncMsgErrorMsg,
                buttons: Ext.Msg.OK
            });                      
        }                         
        MineralPro.config.RuntimeUtility.DebugLog('Leaving UnitNotesGridViewController onSyncListGridData.')
    },
    onLoadSelectionChange: function(model, record){
        MineralPro.config.RuntimeUtility.DebugLog('Entering UnitNotesGridViewController onLoadSelectionChange.')
        record = record[0];
        var me = this;
        var view = me.getView();
        var detailsView = view.nextSibling('UnitNoteDetailView');
        if(record){
            detailsView.loadRecord(record);
        }
        else{
            detailsView.reset();
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving UnitNotesGridViewController onLoadSelectionChange.')
    }
});