Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitAppraisalNote.UnitAppraisalNoteEditFormView', {
    extend: 'Minerals.sub.MineralValuationManageUnit.view.UnitAppraisalNote.UnitAppraisalNoteAddFormView',
    //requires: ['Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteEditFormViewController'],
    //controller: 'LeaseNoteEditFormViewController',
    alias: 'widget.UnitAppraisalNoteEditFormView',
    
    
    title: 'Update Appraisal Note',
    // defaultFocus: '#noteId',
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});