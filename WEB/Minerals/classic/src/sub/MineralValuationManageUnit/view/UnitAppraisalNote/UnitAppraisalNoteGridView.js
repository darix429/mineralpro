
Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitAppraisalNote.UnitAppraisalNoteGridView', {
    extend: 'Minerals.config.view.Note.BaseNoteGridView',
    alias: 'widget.UnitAppraisalNoteGridView',
    store:'Minerals.sub.MineralValuationManageUnit.store.UnitNotesAppraisalStore',  
          
    //below are default items needed for grid checking 
//    firstLoadItemId: 'dateItemId', 
    mainPanelAlias: 'MineralValuationManageUnit', //widget alias name of the main panel
    mainListModel: 'Minerals.model.MineralValuation.NotesModel',     

    addFormAliasName: 'UnitAppraisalNoteAddFormView', //widget form to call for add list 
    editFormAliasName: 'UnitAppraisalNoteEditFormView', //widget form to call for edit
   

//    firstFocus: 'dateItemId',    
    
    addButtonTooltip: 'Add New Note',
    addButtonText: 'Add New Note',
        
    fieldGridViewAlias: 'UnitFieldsGridView'    
        
      
});