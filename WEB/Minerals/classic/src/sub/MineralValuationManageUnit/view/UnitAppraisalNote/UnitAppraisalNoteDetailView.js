Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitAppraisalNote.UnitAppraisalNoteDetailView', {
    extend: 'MineralPro.config.view.BaseDetailView',
    alias: 'widget.UnitAppraisalNoteDetailView',
    
    title: 'Note Details',
    itemId: 'UnitAppraisalNoteDetail',
    defaults: {
        width: 275,
        labelWidth: 93
    },   
    items: [{
        xtype: 'textareafield',
        fieldLabel:'Note',
        name: 'note',
        anchor: '100%',
        labelWidth: 80,
        grow: true, 
        readOnly: true 
    }]
});