Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitAppraisalNote.UnitAppraisalNoteView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.UnitAppraisalNoteView',
    layout:'border',
    
    id: 'UnitAppraisalNoteViewId',
    
    items: [{
            xtype: 'UnitAppraisalNoteGridView',
            html:'center',
            region:'center'
        },{
        	xtype:'UnitAppraisalNoteDetailView',
        	region: 'east'
        }] 
});    