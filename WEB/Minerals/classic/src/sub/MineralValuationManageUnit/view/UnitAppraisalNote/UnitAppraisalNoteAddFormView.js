Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitAppraisalNote.UnitAppraisalNoteAddFormView', {
    extend: 'Minerals.config.view.Note.BaseNoteAddEditFormView',

    alias: 'widget.UnitAppraisalNoteAddFormView',

    title: 'Add Appraisal Notes',
    saveAction: 'onAddListToGrid',
//    defaultFocus: '#noteId',
    layout: 'fit',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'MineralValuationManageUnitId', //id of the main panel
    listGridStore: 'Minerals.sub.MineralValuationManageUnit.store.UnitNotesAppraisalStore', //store for note grid
    listGridAlias: 'UnitAppraisalNoteGridView', 
    bodyPadding: 5,
    width: 800,
    height: 565,

    subjectType: 'Unit Note',
    
    subjectTypeLabel: 'Unit Name'

});