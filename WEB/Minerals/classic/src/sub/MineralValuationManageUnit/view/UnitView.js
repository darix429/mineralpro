Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitView' ,{
    extend: 'MineralPro.config.view.BaseTabPanelView',
    alias : 'widget.MineralValuationManageUnit',
    layout:'border',
    
    //used for tab change 
    selectedId: '0',//Need default value '0'
    selectedIdIndex: 'unitId',
    selectedName: '0',//Need default value '0'
    selectedNameIndex: 'unitName',
     selectedRRCNumber: '-1',//Need default value '-1', '0' is used for UnitNote
    selectedRRCNumberIndex: 'rrcNumber',
    
    subjectTypeCd: '1',
    
    noteTabIndex: '5',//this is the corresponding index for noteView. start at 0;
    appraisalTabIndex: '4',//this is the corresponding index for appraisalView. start at 0;
    
    id: 'MineralValuationManageUnitId',
    
    items: [{
            title: 'Units',
            xtype: 'UnitsView',
        }, {           
            title: 'Unit Leases',
            xtype: 'UnitLeasesView',
        }, {           
            title: 'Unit Notes',
            xtype: 'UnitNotesView',
        }, {
            title: 'RRC/Fields',
            xtype: 'UnitFieldsView',
        }, {           
            title: 'Appraisal',
            xtype: 'UnitAppraisalView',    
        }, {           
            title: 'Appraisal Notes',
            xtype: 'UnitAppraisalNoteView',
        }],
    listeners: {
        beforetabchange: function(tabPanel, newCard, oldCard, eOpts){
            /******************************************************************
            | New Appraisal Logic Starts Here
            |__________________________________________________________________
            */

            var unitsGrid = tabPanel.down('UnitsView grid');
            var unitsSelectionModel = unitsGrid.getSelectionModel();
            var unitsStore = unitsGrid.getStore();

            if(!unitsStore.getCount() || !unitsSelectionModel.getSelection().length){
                Ext.Msg.show({
                    title: MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                    msg: "Please select a unit.",
                    iconCls: MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                    buttons: Ext.Msg.OK,
                    modal: true,
                    scope: this,
                    width: 250,
                    fn: function (btn) {
                        if (btn === 'ok'){
                            tabPanel.setActiveTab(0);//activate the first tab
                        }
                    }
                });
                return false;
            }
            // if(newCard.xtype == 'UnitAppraisalView'){}
            
            /******************************************************************
            | New Appraisal Logic Ends Here
            |__________________________________________________________________
            */
        }
    }
});    