Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitAppraisal.UnitAppraisalFormView', {
    extend: 'Minerals.config.view.Appraisal.BaseAppraisalFormView',
    alias: 'widget.UnitAppraisalFormView',
    
    mainPanelAlias: 'MineralValuationManageUnit',
    mainPanelId: 'MineralValuationManageUnitId',
    firstFocus: 'rrcNumberItemId',
    appraisalStore: 'Minerals.sub.MineralValuationManageUnit.store.UnitAppraisalStore',
    
    calcAppraisalStore: 'Minerals.sub.MineralValuationManageUnit.store.UnitCalcAppraisalStore',
    
    fieldGridViewAlias: 'UnitFieldsGridView',
    // listeners: {
    //     afterRender: function(thisForm, options){
    //         var me = this

    //         setTimeout(function (){
    //             var idx = 1;
    //             Ext.each(me.query('field:not(hiddenfield):not(displayfield)'),function(field){
    //                 if(field.tabIndex > 0 && field.tabIndex !== -1){
    //                 field.inputEl.dom.tabIndex = field.initialConfig.tabIndex;
    //                 }
    //             });
    //         },500)
    //     }
    // }
    
});    