Ext.define('Minerals.sub.MineralValuationManageUnit.view.UnitAppraisal.UnitAppraisalView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.UnitAppraisalView',
    layout:'border',
    
    id: 'UnitAppraisalViewId',
    
    items: [{
            xtype: 'UnitAppraisalFormView',
            region:'center'
        }] 
});    