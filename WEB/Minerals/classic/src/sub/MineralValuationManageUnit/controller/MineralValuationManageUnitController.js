///**
// * Handles the controller processing for MineralValuationManageLease
// */

Ext.define('Minerals.sub.MineralValuationManageUnit.controller.MineralValuationManageUnitController', {
    extend: 'Ext.app.Controller',
    
    models: [
        'Minerals.sub.MineralValuationManageUnit.model.UnitLeasesModel',
        'Minerals.sub.MineralValuationManageUnit.model.UnitLeasesLeaseModel'
    ],
    
    stores: [
        'Minerals.sub.MineralValuationManageUnit.store.UnitNotesStore',
        'Minerals.sub.MineralValuationManageUnit.store.UnitAppraisalStore',
        'Minerals.sub.MineralValuationManageUnit.store.UnitCalcAppraisalStore',
        'Minerals.sub.MineralValuationManageUnit.store.UnitFieldsStore',
        'Minerals.sub.MineralValuationManageUnit.store.UnitsStore',
        'Minerals.sub.MineralValuationManageUnit.store.UnitLeasesStore',
        'Minerals.sub.MineralValuationManageUnit.store.UnitLeasesLeaseStore',
        'Minerals.sub.MineralValuationManageUnit.store.UnitsHeaderDetailStore',
        
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalOperatingExpStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalOilDailyAvgStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalGasDailyAvgStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalProdDailyAvgStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedProdStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedSvrcStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedInjStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSubjectIdStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSubjectNameStore',
        'Minerals.sub.MineralValuationManageUnit.store.UnitNotesAppraisalStore'
        
    ],  
    
    views: [
        'Minerals.sub.MineralValuationManageUnit.view.UnitView',
        'Minerals.sub.MineralValuationManageUnit.view.UnitHeaderView',
		
        'Minerals.sub.MineralValuationManageUnit.view.Units.UnitsView',
        'Minerals.sub.MineralValuationManageUnit.view.Units.UnitsGridView',
        'Minerals.sub.MineralValuationManageUnit.view.Units.UnitsAddFormView',
        'Minerals.sub.MineralValuationManageUnit.view.Units.UnitsEditFormView',

        'Minerals.sub.MineralValuationManageUnit.view.UnitLeases.UnitLeasesView',
        'Minerals.sub.MineralValuationManageUnit.view.UnitLeases.UnitLeasesGridView',
        'Minerals.sub.MineralValuationManageUnit.view.UnitLeases.UnitLeasesAddFormView',
        'Minerals.sub.MineralValuationManageUnit.view.UnitLeases.UnitLeasesEditFormView',
        
        'Minerals.sub.MineralValuationManageUnit.view.UnitNotes.UnitNotesView',
        'Minerals.sub.MineralValuationManageUnit.view.UnitNotes.UnitNoteDetailView',
        'Minerals.sub.MineralValuationManageUnit.view.UnitNotes.UnitNotesGridView',
        'Minerals.sub.MineralValuationManageUnit.view.UnitNotes.UnitNotesGridViewController',
        'Minerals.sub.MineralValuationManageUnit.view.UnitNotes.UnitNotesAddFormView',
        'Minerals.sub.MineralValuationManageUnit.view.UnitNotes.UnitNotesEditFormView',
        
        'Minerals.sub.MineralValuationManageUnit.view.UnitAppraisal.UnitAppraisalView',
        'Minerals.sub.MineralValuationManageUnit.view.UnitAppraisal.UnitAppraisalFormView',
        
        'Minerals.sub.MineralValuationManageUnit.view.UnitFields.UnitFieldsView',
        'Minerals.sub.MineralValuationManageUnit.view.UnitFields.UnitFieldsGridView',

        'Minerals.sub.MineralValuationManageUnit.view.UnitAppraisalNote.UnitAppraisalNoteView',
        'Minerals.sub.MineralValuationManageUnit.view.UnitAppraisalNote.UnitAppraisalNoteDetailView',
        'Minerals.sub.MineralValuationManageUnit.view.UnitAppraisalNote.UnitAppraisalNoteGridView',
        'Minerals.sub.MineralValuationManageUnit.view.UnitAppraisalNote.UnitAppraisalNoteAddFormView',
        'Minerals.sub.MineralValuationManageUnit.view.UnitAppraisalNote.UnitAppraisalNoteEditFormView'
    ],  
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden'],

    init: function(){
        var me = this;
        
        setTimeout(function () {
            me.getStore('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedProdStore').load();
            me.getStore('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedSvrcStore').load();
            me.getStore('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedInjStore').load();
        }, 200);
    }

});


    
