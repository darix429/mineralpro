///**
// * Handles the controller processing for ProperRecordsManageOwner
// */

Ext.define('Minerals.sub.ProperRecordsManageOwner.controller.ProperRecordsManageOwnerController', {
    extend: 'Ext.app.Controller',
  
    models: [
        'Minerals.sub.ProperRecordsManageOwner.model.OwnerModel',
        'Minerals.sub.ProperRecordsManageOwner.model.OwnerExemptionModel',
        'Minerals.sub.ProperRecordsManageOwner.model.OwnerExemptionTypeModel',
        'Minerals.sub.ProperRecordsManageOwner.model.OwnerLeaseHoldingsModel',
        'Minerals.sub.ProperRecordsManageOwner.model.OwnerTaxUnitModel',
        'Minerals.sub.ProperRecordsManageOwner.model.OwnerDisableVetModel',
        'Minerals.sub.ProperRecordsManageOwner.model.OwnerTotalExemptionModel',
    ],
    
    stores: [
        'Minerals.sub.ProperRecordsManageOwner.store.ChangeReasonStore',
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerStore',
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerExemptionStore',
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerExemptionTypeStore',
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerExemptionTypeCodeStore',
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerNoteStore',
        //'Minerals.sub.ProperRecordsManageOwner.store.OwnerNoteViewTypeStore', //Not being used
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsStore',
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsLeaseStore',
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsInterestTypeStore',
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsTransferStore',
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsTransferCalculationStore',
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsOwnerTransferStore',
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerTaxUnitStore',
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerDisableVetStore',
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerTotalExemptionStore',
        'Minerals.sub.ProperRecordsManageOwner.store.OwnerHeaderDetailStore'
    ],  
    
    views: [
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerView',
        'Minerals.sub.ProperRecordsManageOwner.view.Owners.OwnersView',
        'Minerals.sub.ProperRecordsManageOwner.view.Owners.OwnersGridView',
        'Minerals.sub.ProperRecordsManageOwner.view.Owners.OwnersDetailView',
        'Minerals.sub.ProperRecordsManageOwner.view.Owners.OwnersAddFormView',
        'Minerals.sub.ProperRecordsManageOwner.view.Owners.OwnersEditFormView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteDetailView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteGridView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteEditFormView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteAddFormView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsGridView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsAddFormView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsEditFormView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsTransferMultipleFormView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsTransferBaseFormView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsTransferCalculationGridFormView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsTransferSelectedOwnerFormView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerTaxUnit.OwnerTaxUnitView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerTaxUnit.OwnerTaxUnitGridView',
        'Minerals.sub.ProperRecordsManageOwner.view.OwnerHeaderView',
    ],          
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden'],

    init: function(){              
//        var me=this;
//            me.getStore('Minerals.sub.ProperRecordsManageOwner.store.ChangeReasonStore').load();
//            me.getStore('Minerals.sub.ProperRecordsManageOwner.store.OwnerTypeStore').load(); 
//            me.getStore('Minerals.sub.ProperRecordsManageOwner.store.OwnerDisableVetStore').load();
//            me.getStore('Minerals.sub.ProperRecordsManageOwner.store.OwnerTotalExemptionStore').load();                
    },
});


    
