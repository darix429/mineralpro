Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerTaxUnit.OwnerTaxUnitView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.ProperRecordsManageOwnerTaxUnit',
    layout:'border',
    
    id: 'ProperRecordsManageOwnerTaxUnitId',
    
    items: [{
            xtype: 'OwnerTaxUnitGridView',
            html:'center',
            region:'center'
        }] 
});    