
Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerTaxUnit.OwnerTaxUnitGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.OwnerTaxUnitGridView',
    store:'Minerals.sub.ProperRecordsManageOwner.store.OwnerTaxUnitStore',
    xtype: 'grouped-grid',
    requires: ['Minerals.sub.ProperRecordsManageOwner.view.OwnerTaxUnit.OwnerTaxUnitGridViewController',
    'Ext.grid.feature.Grouping'],
    controller: 'OwnerTaxUnitGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'leaseIditemId', 
    mainPanelAlias: 'ProperRecordsManageOwner', //widget alias name of the main panel
    mainListModel: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerTaxUnitModel',     

   
    firstFocus: 'leaseIditemId',    

    
    iconCls: 'icon-grid',
    features: [{
            id: 'group',
            ftype: 'groupingsummary',
            groupHeaderTpl: '{columnName}:{name}',
           // hideGroupedHeader: true,
            startCollapsed: true  
        }],
        columns: [{
            header: 'Lease No.',
            flex     : 0,
            sortable: true,
            dataIndex: 'leaseId',
            itemId: 'leaseIditemId',
            tdCls: 'leaseName',
            hideable: false,
            summaryType: 'count',
            summaryRenderer: function(value, summaryData, dataIndex) {
                return ((value === 0 || value > 1) ? '(' + value + ' Leases)' : '(1 Lease)');
            },
            filterElement: new Ext.form.TextField()
        },{
            header: 'Lease Name',
            flex     : 2,
            sortable: true,
            dataIndex: 'leaseName',
            filterElement: new Ext.form.TextField()
        },{   
            header: 'Owner Value',
            flex     : 1,
            sortable: true,
            align: 'right',
            dataIndex: 'ownerValue',
            filterElement:new Ext.form.TextField(),
            renderer: function(val, meta) {
                return  Ext.util.Format.number(val, '$000,000');
            },      
        },{   
            header: 'Tax Unit Name',
            flex     : 1,
            sortable: true,
            dataIndex: 'taxUnitName',
            filterElement:new Ext.form.TextField()     
         },{
            header: 'Tax Unit Pct',
            flex     : 0,
            sortable: true,
            dataIndex: 'taxUnitPercent',
            filterElement: new Ext.form.TextField()
        },{
            header: 'Tax Unit Value',
            flex     : 1,
            sortable: true,
            align: 'right',
            dataIndex: 'taxUnitValue',
            filterElement: new Ext.form.TextField(),
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
                return Ext.util.Format.number(record.get('ownerValue') * record.get('taxUnitPercent'), '$000,000');
            }, 
        },{
            header: 'Estimated Tax Rate',
            flex     : 1,
            sortable: true,
            dataIndex: 'estimatedTaxRate',
            filterElement: new Ext.form.TextField()
        },{
            header: 'Tax Amount',
            flex     : 1,
            sortable: true,
            align: 'right',
            dataIndex: 'taxValue',
            renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
                return Ext.util.Format.number((record.get('ownerValue') * record.get('taxUnitPercent')) * record.get('estimatedTaxRate'), '$000,000.00');
            }, 
            summaryType: function(records, values) {
                var i = 0,
                    length = records.length,
                    total = 0,
                    record;

                for (; i < length; ++i) {
                    record = records[i];
                    total += (record.get('ownerValue') * record.get('taxUnitPercent')) * record.get('estimatedTaxRate');
                }
                return total;
            },
            summaryRenderer: function(value, summaryData, dataIndex) {
                return 'Total '+Ext.util.Format.number(value, '$000,000.00');
            },
            filterElement: new Ext.form.TextField()
        }],

   
    initComponent: function () {  
        var me = this;
               
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],       
               
        me.tools= [
        // MineralPro.config.util.MainGridToolCmpt.addNewDataButton,
        // MineralPro.config.util.MainGridToolCmpt.saveToDbButton, 
       // MineralPro.config.util.MainGridToolCmpt.displayField,                   
        MineralPro.config.util.MainGridToolCmpt.clearFilterButton
        ],   
        
        me.callParent(arguments);
        
    }    
});