//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsTransferFormViewController', {
    extend : 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.OwnerLeaseHoldingsTransferFormViewController',

    onTransferMultipleToMultipleGroupToGrid: function(button){
         MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsTransferFormViewController onTransferMultipleToMultipleGroupToGrid.')
        var me = this;
        var win = button.up('window');
        var view = me.getView();
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
        var listStore = listGrid.getStore();
        var leaseGrid = win.down('OwnerLeaseHoldingsTransferSelectedOwnerFormView');
        var ownerCalcGrid = win.down('OwnerLeaseHoldingsTransferCalculationGridFormView');
        var leaseStore = leaseGrid.getStore();
        var ownerCalcStore = ownerCalcGrid.getStore();
        var record = leaseGrid.getSelectionModel().getSelection()[0];
        if(record){
            var ownerTransferPct = record.data.ownerTransferPct;
            if(!ownerTransferPct){
                    ownerTransferPct = record.data.interestPercent;
            }
            var interestTypeCd = record.data.interestTypeCd;
            var ownerId = record.data.ownerId;
            var leaseId = record.data.leaseId;
            var PctTransfer = record.data.PctTransfer;
            var total = 0;
            for (var a=0; a<ownerCalcStore.data.length; a++) {  
                total = +ownerCalcStore.data.items[a].data.percentReceive + +total; 
            }
            if(leaseStore.data.length){
                if(total >= 100){
//                    var dirty = ownerCalcStore.getNewRecords().length > 0
//                    || ownerCalcStore.getUpdatedRecords().length > 0
//                    || ownerCalcStore.getRemovedRecords().length > 0;
                    var dirty = ownerCalcStore.getCount() > 0
                    if(dirty){
                        Ext.Msg.show({
                            title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                            msg:'Are you sure you want to save changes to the database?',
                            iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                            buttons: Ext.Msg.YESNO,
                            scope: this,
                            width: 250,
                            fn: function (btn) {
                                if (btn === 'yes') {
//                                    Ext.getBody().mask('Loading...')
                                    view.mask('Loading...')
                                    //win.hide();
                                    var d = new Date();
                                    ownerCalcStore.each(function(rec){
                                        rec.set('', d.getTime())
                                    })
                                    ownerCalcStore.getProxy().extraParams = {
                                        ownerId: ownerId,
                                        leaseId: leaseId,
                                        PctTransfer: PctTransfer,
                                        interestTypeCd: interestTypeCd,
                                        ownerTransferPct: ownerTransferPct,
                                        updateLength: ownerCalcStore.getUpdatedRecords().length,
                                        rowUpdateUserid: MineralPro.config.Runtime.idAppUser
                                    };
                                    ownerCalcStore.sync({
                                        callback: function (records, operation, success) { 
                                            var mainPanel = listGrid.up(listGrid.mainPanelAlias);
                                            var mainGrid = mainPanel.down('OwnersGridView');
                                            var mainStore = mainGrid.getStore();
                                            var record = mainGrid.getSelectionModel().getSelected();
                                            var mainSelectedId = mainPanel.selectedId;
                                            var getSelectedIndex = mainStore.find('ownerId', mainSelectedId, 0, false, false, true);
                                            mainGrid.getSelectionModel().deselectAll();
                                            mainGrid.getSelectionModel().select(getSelectedIndex, false, false);
                                            if(mainPanel.selectedId){
                                                listStore.getProxy().extraParams = {
                                                    selectedId: mainPanel.selectedId
                                                };
                                            }
                                            listStore.reload({
                                                callback: function () {
                                                   
                                                }
                                            });
                                            view.unmask()
                                        }
                                    });
                                    leaseStore.remove(record);
                                    leaseGrid.getSelectionModel().select(0, false, false);
                                }
                            }
                        });
                    } else {
                        Ext.Msg.show({
                            iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                            title:MineralPro.config.Runtime.listSyncMsgErrorTitle,
                            msg:MineralPro.config.Runtime.listSyncMsgErrorMsg,
                            buttons: Ext.Msg.OK
                        });                      
                    } 
                } else {
                    Ext.Msg.show({
                        iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                        title: 'Warning',    
                        msg: 'Percent is lesser than 100%.',  
                        buttons: Ext.Msg.OK
                    });
                }
            } else {
                Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                    title: 'Warning',    
                    msg: 'Please select lease.',  
                    buttons: Ext.Msg.OK
                });
            }
        }else{
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                title: 'Warning',    
                msg: 'There is no more Lease-Owner to be Transfered.',  
                buttons: Ext.Msg.OK
            });
        }      
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsTransferFormViewController onTransferMultipleToMultipleGroupToGrid.')
    },


    transferAll: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsTransferFormViewController transferAll.')
        var me = this;
        var win = button.up('window');
        var view = me.getView();
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
        var listStore = listGrid.getStore();
        var leaseGrid = win.down('OwnerLeaseHoldingsTransferSelectedOwnerFormView');
        var ownerCalcGrid = win.down('OwnerLeaseHoldingsTransferCalculationGridFormView');
        var leaseStore = leaseGrid.getStore();
        var ownerCalcStore = ownerCalcGrid.getStore();
        leaseGrid.getSelectionModel().select(0, false, false);
        var record = leaseGrid.getSelectionModel().getSelection()[0];

        var d = new Date();
        var promises = [];
        leaseStore.each(function(rec) {
            var promise = new Ext.Promise(function (resolve, reject) {
                var ownerTransferPct = rec.data.ownerTransferPct;
                if(!ownerTransferPct){
                    ownerTransferPct = rec.data.interestPercent;
                }
                var interestTypeCd = rec.data.interestTypeCd;
                var ownerId = rec.data.ownerId;
                var leaseId = rec.data.leaseId;
                var PctTransfer = rec.data.PctTransfer;

                ownerCalcStore.each(function(rec){
                    rec.set('', d.getTime())
                })
                ownerCalcStore.getProxy().extraParams = {
                    ownerId: ownerId,
                    leaseId: leaseId,
                    PctTransfer: PctTransfer,
                    interestTypeCd: interestTypeCd,
                    ownerTransferPct: ownerTransferPct,
                    updateLength: ownerCalcStore.getUpdatedRecords().length,
                    rowUpdateUserid: MineralPro.config.Runtime.idAppUser
                };
                ownerCalcStore.sync({
                    callback: function (records, operation, success) { 
                        // var mainPanel = listGrid.up(listGrid.mainPanelAlias);
                        // var mainGrid = mainPanel.down('OwnersGridView');
                        // var mainStore = mainGrid.getStore();
                        // var record = mainGrid.getSelectionModel().getSelected();
                        // var mainSelectedId = mainPanel.selectedId;
                        // var getSelectedIndex = mainStore.find('ownerId', mainSelectedId, 0, false, false, true);
                        // mainGrid.getSelectionModel().deselectAll();
                        // mainGrid.getSelectionModel().select(getSelectedIndex, false, false);
                        // if(mainPanel.selectedId){
                        //     listStore.getProxy().extraParams = {
                        //         selectedId: mainPanel.selectedId
                        //     };
                        // }

                    },
                    success: function(){
                        resolve('success');
                    },
                    failure: function(){
                        reject('');
                    }
                });
                leaseStore.remove(rec);
                leaseGrid.getSelectionModel().select(0, false, false);
                me.onLoadSelectionChange(0, rec);
            });
            promises.push(promise);
            // leaseStore.remove(rec);;
            // leaseGrid.getSelectionModel().select(0, false, false);
            // me.onLoadSelectionChange(0, rec)
        });
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsTransferFormViewController transferAll.')
        return promises;
    },
    onTransferAll: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsTransferFormViewController onTransferAll.')
        var me = this;
        var win = button.up('window');
        var view = me.getView();
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
        var listStore = listGrid.getStore();
        var leaseGrid = win.down('OwnerLeaseHoldingsTransferSelectedOwnerFormView');
        var ownerCalcGrid = win.down('OwnerLeaseHoldingsTransferCalculationGridFormView');
        var leaseStore = leaseGrid.getStore();
        var ownerCalcStore = ownerCalcGrid.getStore();
        leaseGrid.getSelectionModel().select(0, false, false);
        var record = leaseGrid.getSelectionModel().getSelection()[0];
        var selectedOwners = leaseGrid.getStore().getData().items.map(function(rec){
            return {
                leaseId: rec.get('leaseId'),
                ownerId: rec.get('ownerId'),
                interestType: rec.get('interestType')
            }
        });
        var selectOwnerOnMainTab = function(){
            var mainPanel = listGrid.up(listGrid.mainPanelAlias);
            var mainGrid = mainPanel.down('OwnersGridView');
            var mainStore = mainGrid.getStore();
            var record = mainGrid.getSelectionModel().getSelected();
            var mainSelectedId = mainPanel.selectedId;
            var getSelectedIndex = mainStore.find('ownerId', mainSelectedId, 0, false, false, true);
            mainGrid.getSelectionModel().deselectAll();
            mainGrid.getSelectionModel().select(getSelectedIndex, false, false);
            if(mainPanel.selectedId){
                listStore.getProxy().extraParams = {
                    selectedId: mainPanel.selectedId
                };
            }
        };
        if(record){
                    var dirty = ownerCalcStore.getCount() > 0
                    if(dirty){
                        Ext.Msg.show({
                            title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                            msg:'Are you sure you want to save changes to the database?',
                            iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                            buttons: Ext.Msg.YESNO,
                            scope: this,
                            width: 250,
                            fn: function (btn) {
                                if (btn === 'yes') {
                                    view.mask('Loading...')
                                    Ext.Promise.all(me.transferAll(button))
                                    .then(function(records){
                                        // all promises is resolved
                                        selectOwnerOnMainTab();
                                        listStore.on({
                                            load: {
                                                fn: function(){
                                                    Ext.MessageBox.alert('Success', 'Transfered Successfully.', function(){
                                                        win.close();
                                                    }, this);
                                                    view.unmask();
                                                },
                                                scope: this,
                                                single: true
                                            }
                                        });
                                        listStore.reload();
                                    },
                                    function(){
                                        // at least one promise was rejected
                                        selectOwnerOnMainTab();
                                        listStore.on({
                                            load: {
                                                fn: function(){

                                                    var failedCount = [];
                                                    var isOwnersExists = selectedOwners.some(function(owner,i){
                                                        var lowner = listStore.findRecord('ownerId',owner.ownerId,0,false,false,true);
                                                        var exists = lowner ? lowner.get('leaseId') == owner.leaseId && lowner.get('interestType') == owner.interestType ? true : false : false;
                                                        if(!exists){
                                                            failedCount.push(owner.ownerName);
                                                            leaseGrid.getStore().removeAt(i);
                                                            leaseGrid.getSelectionModel().select(0, false, false);
                                                        }
                                                        return exists;
                                                    });
                                                    if(!isOwnersExists){
                                                        Ext.MessageBox.alert('Failed', 'Transfer failed to complete.', function(){
                                                            win.close();
                                                        }, this);
                                                    }
                                                    else{
                                                        if(failedCount.length > 5){
                                                            failedCount = failedCount.length +' transfer failed.';   
                                                        }
                                                        else{
                                                            var lst = failedCount.pop();
                                                            failedCount.join(', ') + ' and '+lst+' failed to transfer ownership.' ;
                                                        }

                                                        Ext.Msg.show({
                                                            title: 'Failed to transfer all owner values',
                                                            msg: failedCount+' Try again?',
                                                            buttons: Ext.Msg.YESNO,
                                                            scope: this,
                                                            fn: function(btn){
                                                                if(btn=='no'){
                                                                    win.close();
                                                                }
                                                            }
                                                        });
                                                    }
                                                    view.unmask();
                                                },
                                                scope: this,
                                                single: true
                                            }
                                        });
                                        listStore.reload();
                                        
                                    });

                                }
                            }
                        });
                    } else {
                        Ext.Msg.show({
                            iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                            title:MineralPro.config.Runtime.listSyncMsgErrorTitle,
                            msg:MineralPro.config.Runtime.listSyncMsgErrorMsg,
                            buttons: Ext.Msg.OK
                        });                      
                    } 
        }else{
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                title: 'Warning',    
                msg: 'There is no more Lease-Owner to be Transfered.',  
                buttons: Ext.Msg.OK
            });
       }      
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsTransferFormViewController onTransferAll.')
    },
     onLoadSelectionChange: function (model, records) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsTransferFormViewController onLoadSelectionChange.')
             var me = this;
             var view = me.getView();
             if(view.basePanelId){
                var calcGrid = Ext.getCmp(view.basePanelId).down(view.gridCalcAlias);
                var calcStore = calcGrid.getStore();
                if(records[0]){
                    var PctTransfer = records[0].data.PctTransfer;
                    var interestPct = records[0].data.interestPercent;
                    var ownerValue = records[0].data.ownerValue;
                    var calVal = ((PctTransfer/100)*interestPct)
                    var length = calcStore.data.length;
                    var percent = 100/length;
                    var curTotalOwnerTransferPct = 0;
                    var totalPct = 0;
                        calcStore.each(function(c, fn){
                            var percentReceive = c.data.percentReceive;
                            var valueEdit = (calVal*percentReceive)/100;
                            var OwnerReceiveCalcVal =(ownerValue*PctTransfer)/100
                            var OwnerReceiveValue = (OwnerReceiveCalcVal*percentReceive)/100;
                            c.set('OwnerReceiveValue', OwnerReceiveValue);
                            c.set('ownerReceivePct', Ext.util.Format.number(valueEdit, '0.00000000'));
                            totalPct = valueEdit
                            curTotalOwnerTransferPct=parseFloat(curTotalOwnerTransferPct)+parseFloat(Ext.util.Format.number(totalPct, '0.00000000'));
                        });
                }
                if(calcStore.getAt(0)){
                        var balanceVal = parseFloat(Ext.util.Format.number(calVal, '0.00000000')) - parseFloat(curTotalOwnerTransferPct);
                        if(balanceVal != 0){  
                        var toCompute = parseFloat(balanceVal) + parseFloat(calcStore.getAt(0).data.ownerReceivePct);
                        calcStore.getAt(0).set('ownerReceivePct', Ext.util.Format.number(toCompute, '0.00000000'));
                        }
                }
                  
            }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsTransferFormViewController onLoadSelectionChange.')
    },
    onValidateEditTop: function (editor, context, eOpts) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsTransferFormViewController onValidateEditTop.')
        var me = this;
        var record = context.record
        var view = me.getView();
        var store = me.getView().getStore();
        var interestPct = record.data.interestPercent;
        var ownerValue = record.data.ownerValue;
        var value = context.value;
        var column = context.column.dataIndex;
        var curTotalOwnerTransferPct = 0;
        if(column == 'PctTransfer'){
            if(value <= 100){
                var calVal = (value/100)*interestPct;

                if(record.data.edited == ''){
                    record.set('oldValue', context.originalValue);
                }
                record.set('ownerTransferPct', calVal); 
                var calcGrid = Ext.getCmp(view.basePanelId).down(view.gridCalcAlias);
                var calcStore = calcGrid.getStore();
                var length = calcStore.data.length;
                var percent = 100/length;

                calcStore.each(function(c, fn){
                    var percentReceive = c.data.percentReceive;
                    var valueEdit = (calVal*percentReceive)/100;
                    var OwnerReceiveCalcVal =(ownerValue*value)/100
                    var OwnerReceiveValue = (OwnerReceiveCalcVal*percentReceive)/100;
                    c.set('OwnerReceiveValue', OwnerReceiveValue);
                    c.set('ownerReceivePct', Ext.util.Format.number(valueEdit, '0.00000000'));
                    curTotalOwnerTransferPct=parseFloat(curTotalOwnerTransferPct)+parseFloat(Ext.util.Format.number(valueEdit, '0.00000000'));
                });
            }
        }else{
            if(value <= interestPct){
                
                var calVal = (value*100)/interestPct;

                if(record.data.edited == ''){
                    record.set('oldValue', context.originalValue);
                }
                record.set('PctTransfer', calVal); 
                var calcGrid = Ext.getCmp(view.basePanelId).down(view.gridCalcAlias);
                var calcStore = calcGrid.getStore();
                var length = calcStore.data.length;
                var percent = 100/length;

                calcStore.each(function(c, fn){
                    var percentReceive = c.data.percentReceive;
                    var valueEdit = (value*percentReceive)/100;
                    var OwnerReceiveCalcVal =(ownerValue*calVal)/100;
                    var OwnerReceiveValue = (OwnerReceiveCalcVal*percentReceive)/100;
                    c.set('OwnerReceiveValue', OwnerReceiveValue);
                    c.set('ownerReceivePct', Ext.util.Format.number(valueEdit, '0.00000000'));
                    curTotalOwnerTransferPct=parseFloat(curTotalOwnerTransferPct)+parseFloat(Ext.util.Format.number(valueEdit, '0.00000000'));
                });
            }
        }
        if(calcStore.getAt(0)){
            var balanceVal = parseFloat(Ext.util.Format.number(calVal, '0.00000000')) - parseFloat(curTotalOwnerTransferPct);
            if(balanceVal != 0){   
                var toCompute = parseFloat(balanceVal) + parseFloat(calcStore.getAt(0).data.ownerReceivePct);
                calcStore.getAt(0).set('ownerReceivePct', Ext.util.Format.number(toCompute, '0.00000000'));
            }
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsTransferFormViewController onValidateEditTop.')
    },
  
     onValidateEditButtom: function (editor, context, eOpts) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsTransferFormViewController onValidateEditButtom.')
        var me = this;
        var record = context.record
        var view = me.getView();
        var store = me.getView().getStore();
        var topGrid = view.up().down('OwnerLeaseHoldingsTransferSelectedOwnerFormView');
        var ownerTransferPct = topGrid.getSelectionModel().getSelection()[0].data.ownerTransferPct;
        var percent = 100;
        var PctTransfer = topGrid.getSelectionModel().getSelection()[0].data.PctTransfer
        var interestPercent = topGrid.getSelectionModel().getSelection()[0].data.interestPercent
        var ownerValue = topGrid.getSelectionModel().getSelection()[0].data.ownerValue;
        var calVal = (PctTransfer/100)*interestPercent;
        Ext.suspendLayouts();
        if(record.data.edited == ''){
            record.set('oldValue', context.originalValue);
        }
        
        var value = (context.value*calVal)/100;
        var OwnerReceiveCalcVal =(ownerValue*PctTransfer)/100;
        var OwnerReceiveValue = (OwnerReceiveCalcVal*context.value)/100;
        record.set('OwnerReceiveValue', OwnerReceiveValue);
        record.set('percentReceive', context.value);
        record.set('ownerReceivePct', Ext.util.Format.number(value, '0.00000000'));
        var totalsum = ''; 
        var length = 0;
        var curTotalOwnerTransferPct=0
        
        store.each(function(c, fn){
            if(c.data.edited == 'Y'){
                totalsum = +c.data.percentReceive + +totalsum;
            }else{
                length = length+1;
            }
        });
        if(totalsum <= percent){
            var percentEdit = (percent - totalsum)/length;
            var values = (percentEdit*calVal)/100;
            var OwnerReceiveCalcVal =(ownerValue*PctTransfer)/100
            var OwnerReceiveValues = (OwnerReceiveCalcVal*percentEdit)/100;
           
            store.each(function(c, fn){
                if(c.data.edited == ''){
                    c.set('OwnerReceiveValue', OwnerReceiveValues);    
                    c.set('percentReceive', percentEdit);
                    c.set('ownerReceivePct', Ext.util.Format.number(values, '0.00000000'));
                    
                }
               curTotalOwnerTransferPct=parseFloat(curTotalOwnerTransferPct)+parseFloat(Ext.util.Format.number(c.data.ownerReceivePct, '0.00000000'));
           });

           var balanceVal = parseFloat(Ext.util.Format.number(calVal, '0.00000000')) - parseFloat(curTotalOwnerTransferPct)
            if(balanceVal != 0){
                var balValue = record.get('ownerReceivePct');
                balValue = parseFloat(balanceVal) + parseFloat(store.getAt(0).data.ownerReceivePct);
                store.getAt(0).set('ownerReceivePct', Ext.util.Format.number(balValue, '0.00000000'));
            }
            Ext.resumeLayouts(true);

        }else{
            Ext.resumeLayouts(true);
            setTimeout(function (){   
                Ext.Msg.show({
                    iconCls: Minerals.config.Runtime.listSyncMsgErrorIcon,
                    title: 'Percent exceeded',    
                    msg: 'Split Percent exceeded 100%.',               
                    buttons: Ext.Msg.OK,
                     fn: function(btn){                        
                        var value = (context.originalValue*calVal)/100;
                        var OwnerReceiveCalcVal =(ownerValue*PctTransfer)/100;
                        var OwnerReceiveValue = (OwnerReceiveCalcVal*context.originalValue)/100;
                        record.set('OwnerReceiveValue', OwnerReceiveValue);
                        record.set('percentReceive', context.originalValue);
                        record.set('ownerReceivePct', Ext.util.Format.number(value, '0.00000000'));                        
                    }
                }); 
            },100)
        }  
        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsTransferFormViewController onValidateEditButtom.')
    },
    onBeforeEdit: function (editor, context, eOpts){
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsTransferFormViewController onBeforeEdit.')
        var me = this;
        var record = context.record
        var view = me.getView();
        var store = me.getView().getStore();
        var length = 0;

        store.each(function(c, fn){
            if(c.data.edited == ''){
                 length = length+1;
            }
        });

        if(length == 1 && record.data.edited == ''){
            setTimeout(function (){   
                Ext.Msg.show({
                    iconCls: Minerals.config.Runtime.listSyncMsgErrorIcon,
                    title: 'Warning!',    
                    msg: 'Cannot edit Last/Single record.',               
                    buttons: Ext.Msg.OK
                }); 
            },50)
        }else{
            record.set('edited', 'Y');
        }


        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsTransferFormViewController onBeforeEdit.')
    },
    onBeforeEditTop: function (editor, context, eOpts){
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsTransferFormViewController onBeforeEditTop.')
        var me = this;
        var record = context.record
        var view = me.getView();
        var store = me.getView().getStore();
        var interestPct = record.data.interestPercent;
        var length = 0;
        var value = context.value;
        var originalValue = context.originalValue;
        var column = context.column.dataIndex;

        if(column == 'PctTransfer'){
            if(value > 100){
                    setTimeout(function (){   
                    Ext.Msg.show({
                        iconCls: Minerals.config.Runtime.listSyncMsgErrorIcon,
                        title: 'Warning!',    
                        msg: 'Split percent cannot be greater than 100%.',               
                        buttons: Ext.Msg.OK
                    }); 
                },50)
                record.set('PctTransfer', originalValue);
            }
        }else{
           if(value>interestPct){
                    setTimeout(function (){   
                    Ext.Msg.show({
                        iconCls: Minerals.config.Runtime.listSyncMsgErrorIcon,
                        title: 'Warning!',    
                        msg: 'Owner transfer percent cannot be greater than owner percent.',               
                        buttons: Ext.Msg.OK
                    }); 
                },50)
                record.set('ownerTransferPct', originalValue);
            } 
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsTransferFormViewController onBeforeEditTop.')
    }
  
});
