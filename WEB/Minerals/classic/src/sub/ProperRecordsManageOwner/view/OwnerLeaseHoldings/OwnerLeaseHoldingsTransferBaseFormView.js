Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsTransferBaseFormView' ,{
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    alias : 'widget.OwnerLeaseHoldingsTransferBaseFormView',
   // layout:'fit',
    title: 'Transfer Ownership  ',
    defaultFocus: 'OwnerNameItemId',
    id: 'OwnerLeaseHoldingsTransferBaseFormViewId',
    requires: ['Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsTransferFormViewController'],
    controller: 'OwnerLeaseHoldingsTransferFormViewController',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'ProperRecordsManageOwnerLeaseHoldingsId', //id of the main panel
    listGridStore: 'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsStore', 
    listGridAlias: 'OwnerLeaseHoldingsGridView', //alias name for note gridview
    closeAction: 'hide',
    width: 1200,
    height: 585, 
    bodyPadding: 1,
      
    items:[{
        xtype: 'panel',
         layout: {
            type: 'table',
            columns: 2,
        // tdAttrs: { style: 'padding: 10px; vertical-align: top;' }
        }, 
        items: [{
                title: 'Selected Lease',
                width: 1195,
                xtype: 'OwnerLeaseHoldingsTransferSelectedOwnerFormView',
                colspan: 2
              //  region:'south'
            },{
                title: 'Select Receiving Owner',
                width: 378,
                xtype: 'OwnerLeaseHoldingsTransferMultipleFormView',
               // region:'center'
            },{
                //title: 'center',
                width: 818,
                xtype: 'OwnerLeaseHoldingsTransferCalculationGridFormView',
              //  region:'west',
            }] 

     }],
     listeners:{
            close: function(){
                var gridleaseStore = this.down('OwnerLeaseHoldingsTransferSelectedOwnerFormView').getStore();;
                var gridcalcStore = this.down('OwnerLeaseHoldingsTransferCalculationGridFormView').getStore();
                var gridselection = this.down('OwnerLeaseHoldingsTransferMultipleFormView');
                var gridselectionStore = gridselection.getStore();
                gridselectionStore.rejectChanges();
                gridselection.getSelectionModel().deselectAll();
                gridleaseStore.removeAll();
                gridcalcStore.removeAll();
                //gridleaseStore.rejectChanges();
                gridleaseStore.commitChanges();
                gridcalcStore.commitChanges();
                var cols = gridselection.columns;
                Ext.each(cols, function (col) {
                    if (col.filterElement)
                        col.filterElement.setValue('');
                });
            }
     },
     buttons: [
        {
            xtype: 'button',
            text: 'Transfer All',
            itemId: 'transferAllId',
            disabled: true,
            listeners: {
                click: 'onTransferAll'
            }
        },{
            xtype: 'button',
            text: 'Clear',
            itemId: 'clearId',
            listeners: {
               click: function(){
                    var view = this.up('window');
                    var gridcalcStore = view.down('OwnerLeaseHoldingsTransferCalculationGridFormView').getStore();
                    var gridselection = view.down('OwnerLeaseHoldingsTransferMultipleFormView');
                    view.down('#transferAllId').disable();
                    gridcalcStore.removeAll();
                    gridselection.getSelectionModel().deselectAll();
               }
            }
        },
        // {
        //     xtype: 'button',
        //     text: 'Save',
        //     itemId: 'saveId',
        //     disabled: true,
        //     listeners: {
        //         click: 'onTransferMultipleToMultipleGroupToGrid'
        //     }
        // },
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]    
});    