Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.ProperRecordsManageOwnerLeaseHoldings',
    layout:'border',
    
    id: 'ProperRecordsManageOwnerLeaseHoldingsId',
    
    items: [{
            xtype: 'OwnerLeaseHoldingsGridView',
            html:'center',
            region:'center'
        }] 
});    