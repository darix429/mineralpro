Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsAddFormViewController'],
    controller: 'OwnerLeaseHoldingsAddFormViewController',
    alias: 'widget.OwnerLeaseHoldingsAddFormView',

    title: 'Add New Lease',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#leaseIdItemId',
    layout: 'fit',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'ProperRecordsManageOwnerId', //id of the main panel
    listGridStore: 'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsStore', //store for note grid
    listGridAlias: 'OwnerLeaseHoldingsGridView',
    width: 450, 
    bodyPadding: 5,
    
    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'form',
            defaults:{ 
                maskRe: /[^'\^]/ ,
                labelWidth: 130,
                padding: 5,
                width: 410
            },
                items:[{
                fieldLabel: 'Owner Name',
                xtype: 'displayfield',
                labelAlign: 'right',
                msgTarget: 'side',
                margin: '0 0 10',
                itemId: 'ownerName',
            }, {       
                xtype: 'hidden',     
                name: 'ownerId',   
                queryMode:'local',
                itemId:'ownerNameId' 
            },{
                fieldLabel: 'Lease Name',
                xtype: 'combo',
                selectOnFocus: true,
                tabIndex:1,
                itemId: 'leaseIdItemId',
                displayField: 'leaseName',
                valueField: 'leaseId',
                queryMode: 'local',
                store: 'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsLeaseStore',
                listeners:{
                    select:function(record){
                        me.down('#leaseNameItemId').setValue(this.getRawValue());
                        me.down('#operatorNameItemId').setValue(this.displayTplData[0].operatorName);
                        me.down('#totalInterestPctItemId').setValue(this.displayTplData[0].totalInterestPct);
                        me.down('#appraisedValueItemId').setValue(this.displayTplData[0].appraisedValue);
                        me.down('#interestPercentItemId').setValue(0);
                        me.down('#ownerValueItemId').hide();
                    }, 
                   
                     blur: 'onAddCheckExisting'                    
                     
                    // beforequery: function(queryVV){
                    //     queryVV.combo.expand();
                    //     queryVV.combo.store.load();
                    //     return false;
                    // }
                },
                name: 'leaseId',
                typeAhead: true,
                forceSelection: true,
                allowBlank: false,
                labelAlign: 'right', 
                   
             },{
                fieldLabel: 'lease Name',
                name: 'leaseName',
                xtype: 'hidden',
                margin: '0 0 10',
                itemId:'leaseNameItemId',
                labelAlign: 'right',     
             },{
                fieldLabel: 'Interest Type',
                xtype: 'combo',
                selectOnFocus: true,
                tabIndex: 2,
                itemId: 'interestTypeCdItemId',
                displayField: 'interestTypeDesc',
                valueField: 'interestTypeCd',
                queryMode:'local',
                store: 'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsInterestTypeStore',
                listeners:{
                    select:function(record){
                        me.down('#interestTypeDescItemId').setValue(this.getRawValue());
                    }
                },
                name: 'interestTypeCd',
                typeAhead: true,
                forceSelection: true,
                allowBlank: false,
                labelAlign: 'right',    
             },{
                fieldLabel: 'Interest Type Name',
                name: 'interestTypeDesc',
                xtype: 'hidden',
                margin: '0 0 10',
                itemId:'interestTypeDescItemId',
                labelAlign: 'right',     
             },{
                fieldLabel: 'Interest Percent',
                selectOnFocus: true,
                name: 'interestPercent',
                xtype: 'textfield',
                itemId: 'interestPercentItemId',
                maskRe:/[0-9.]/,
                margin: '0 0 10',
                tabIndex: 2,
                labelAlign: 'right',
                decimalPrecision : 8,
                decimalSeparator: '.',
                validateBlank: true,
                allowBlank: false,
                maxValue: 999999,
                enableKeyEvents: true, 
                minValue: 0.00000000,
                listeners:{
                     initialize: function(){
                            var nfield = Ext.get('weightfield');
                            nfield.down('input').set({pattern:'[0-9]*'});
                     },
                     keyup: function(field, e, eOpts){
                        var value = field.getValue();
                        var appraisedValue = me.down('#appraisedValueItemId').getRawValue();
                        //var interestPct = rec.getRawValue();
                        var ownerValue = (value * appraisedValue);

                        if(me.down('#leaseIdItemId').getValue()){
                            var baseValue = me.down('#leaseIdItemId').displayTplData[0].totalInterestPct;
                            var total = parseFloat(value) + parseFloat(baseValue);
                                if(value != 0){
                                    me.down('#ownerValueItemId').show();
                                    me.down('#ownerValueItemId').setValue(Ext.util.Format.number(ownerValue, '$000,000'));
                                }else{
                                    me.down('#ownerValueItemId').setValue(Ext.util.Format.number(0, '$000,000'));
                                    me.down('#ownerValueItemId').hide();
                                }
                                if(total){
                                    me.down('#totalInterestPctItemId').setValue(parseFloat(total).toFixed(8))
                                }else{
                                    me.down('#totalInterestPctItemId').setValue(baseValue)
                                }
                        }else{
                            Ext.Msg.show({
                                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                                title: 'Alert',    
                                msg: 'Please choose lease first.',               
                                buttons: Ext.Msg.OK,
                                fn: function (btn) {
                                    if (btn === 'ok') {
                                         me.down('#leaseIdItemId').focus();
                                         field.setValue('');
                                    }
                                }
                            });
                        }
                    }, 
                }
            },{
                fieldLabel: 'Lease Total Interest Pct',
                name: 'totalInterestPct',   
                xtype: 'textfield',
                readOnly: true,
                margin: '0 0 10',
                itemId:'totalInterestPctItemId',
                labelAlign: 'right',
                listeners : {
                    change: function(record) {
                        if(record.getRawValue() == 1){
                                this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_good.png) no-repeat right;');
                        }else if(record.getRawValue() < 1){
                                this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_upper.png) no-repeat right;');
                        }else{
                                this.setFieldStyle('background:#d8d8d8 url(../resources/icons/tp_lower.png) no-repeat right;');
                        }
                    }
                }
                //disabled: true     
             },{
                fieldLabel: 'Value',
                name: 'ownerValue',   
                xtype: 'textfield',
                readOnly: true,
                margin: '0 0 0',
                hidden:true,
                itemId:'ownerValueItemId',
                labelAlign: 'right',
                listeners : {
                        afterrender: function(me){
                            me.setValue(Ext.util.Format.number(me.getRawValue(), '$000,000'));
                        }
                },
                fieldStyle: 'background-color: #D8D8D8;'
             },{
                fieldLabel: 'Operator',
                name: 'operatorName',   
                xtype: 'hidden',
                margin: '0 0 10',
                itemId:'operatorNameItemId',
                labelAlign: 'right',     
             },{       
                xtype: 'hidden',     
                name: 'appraisedValue',   
                queryMode:'local',
                itemId:'appraisedValueItemId' 
            }
            ]
        }];

        me.listeners = {
            afterrender: function(){
                var me = this;      
                var mainView = Ext.getCmp(me.mainPanelId);
                var ownerName = mainView.selectedName;
                me.down('#ownerName').setValue(ownerName);

                var ownerNameId = mainView.selectedId;
                me.down('#ownerNameId').setValue(ownerNameId);

                setTimeout(function () {
                    me.down('#leaseNameItemId').setValue(me.down('#leaseIdItemId').getRawValue());
                    me.down('#interestTypeDescItemId').setValue(me.down('#interestTypeCdItemId').getRawValue()); 
                }, 100);
            },
        }

        me.callParent(arguments);
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
        MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});
