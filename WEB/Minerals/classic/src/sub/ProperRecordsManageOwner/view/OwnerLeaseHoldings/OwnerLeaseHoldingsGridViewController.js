
Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsGridViewController', {
    extend: 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.OwnerLeaseHoldingsGridViewController',

    onTransferMultipleToMultipleGroup: function(button, rowIndex){
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsGridViewController onTransferMultipleToMultipleGroup.')
        var me = this;
        var grid = me.getView();
        var store = grid.getStore();
        var selected = grid.getSelectionModel().getSelection();
        var dirty = store.getNewRecords().length > 0
                || store.getUpdatedRecords().length > 0
                || store.getRemovedRecords().length > 0;
        if(!dirty){
            if(selected.length>0){       
                var view = Ext.getCmp('OwnerLeaseHoldingsTransferBaseFormViewId');
                if(view){
                    Ext.getCmp('OwnerLeaseHoldingsTransferBaseFormViewId').show();
                   var gridownerowner = view.down('OwnerLeaseHoldingsTransferMultipleFormView');
                    gridownerowner.getSelectionModel().deselectAll();
                }else{
                    view = new Ext.widget('OwnerLeaseHoldingsTransferBaseFormView');
                 }
                var gridlease = view.down('OwnerLeaseHoldingsTransferSelectedOwnerFormView');
                var leasestore = gridlease.getStore();
                var gridlOwners = view.down('OwnerLeaseHoldingsTransferMultipleFormView');
                var gridlOwnersStore = gridlOwners.getStore();
                gridlOwnersStore.rejectChanges();
                gridlOwnersStore.reload({
                    callback: function () {
                        var getSelectedIndex = gridlOwnersStore.find('ownerId', selected[0].data.ownerId, 0, false, false, true);
                        gridlOwnersStore.removeAt(getSelectedIndex);
                    }
                })
                leasestore.insert(0, selected); 
                leasestore.each(function(c, fn){
                    c.set('ownerTransferPct', c.data.interestPercent);
                })
                gridlease.getSelectionModel().select(0, true);
                gridlease.getView().focusRow(0); 
                store.reload();  
                view.show();
            }else{
                Ext.Msg.show({
                    iconCls: Minerals.config.Runtime.multipleEditAlertIcon,
                    title: Minerals.config.Runtime.multipleEditAlertTitle,    
                    msg: Minerals.config.Runtime.multipleEditAlertMsg,                
                    buttons: Ext.Msg.OK
                });
            }
        }else{
            Ext.Msg.show({
                    iconCls: Minerals.config.Runtime.multipleEditAlertIcon,
                    title: 'Warning',    
                    msg: 'Please save changes first.',                
                    buttons: Ext.Msg.OK
                });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsGridViewController onTransferMultipleToMultipleGroup.')
    },
    onOpenSelectedTabView: function (record, rowIndex) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsGridViewController onOpenSelectedTabView.')
        var me = this;
		var centerView = me.getView().up('centerView');
		var westView = centerView.up().down('westView');
        var favoriteTree = westView.down('#favoriteTreeId');
        var menuTree = westView.down('#menuTreeId');
        var dataTree=true;
        var index = 0;
        
        favoriteTree.getStore().each(function(c, fn){
            if(c.data.functionName == 'Manage Lease'){
                index = fn;
                dataTree=false;
            }
        })            
        menuTree.getStore().each(function(c, fn){
            if(c.data.functionName == 'Manage Lease'){
                index = fn;
                dataTree=true;
            }
        })
        
        if(dataTree == true){
            menuTree.getSelectionModel().select(index);
            setTimeout(function () {
                var tabPanel = centerView.down('MineralValuationManageLease');
                var cols = centerView.down('#MineralsLeasesId').down('grid').columns;
                
                tabPanel.setActiveTab(0);
                Ext.each(cols, function (col) {
                    if(col.dataIndex == 'leaseName'){
                        col.filterElement.setValue(record.data.leaseName.trim());
                    }
                });
            },800 )
        }else{
            favoriteTree.getSelectionModel().select(index);
            setTimeout(function () {
                var tabPanel = centerView.down('MineralValuationManageLease');
                var cols = centerView.down('#MineralsLeasesId').down('grid').columns;

                tabPanel.setActiveTab(0);
                Ext.each(cols, function (col) {
                    if(col.dataIndex == 'leaseName'){
                        col.filterElement.setValue(record.data.leaseName.trim());
                    }
                });
            },800 )
        }
		
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsGridViewController onOpenSelectedTabView.')
    },

    onOpenAddListForm: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsGridViewController onOpenAddListForm.')
        var me = this;
        var mainView = me.getView();
        var record = Ext.create(mainView.mainListModel);      
        var addView = Ext.widget(mainView.addFormAliasName);
        var store = addView.down('#leaseIdItemId').getStore();
        
        store.load();
        record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
        addView.down('form').loadRecord(record);
        addView.show()
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsGridViewController onOpenAddListForm.')
    },
    
    onSyncListGridData: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsGridViewController onSyncListGridData.')
        var me = this;       
        var view = me.getView();
        var listStore = view.getStore();
                                  
        var dirty = listStore.getNewRecords().length > 0
        || listStore.getUpdatedRecords().length > 0
        || listStore.getRemovedRecords().length > 0;
        if (dirty) {
            Ext.Msg.show({
                title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                msg:MineralPro.config.Runtime.syncGridMsgConfimationMsg,
                iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                buttons: Ext.Msg.YESNO,
                scope: this,
                width: 250,
                fn: function (btn) {
                    if (btn === 'yes') {
                        view.getSelectionModel().deselectAll(true);
                        listStore.getProxy().extraParams = {
                            createLength: listStore.getNewRecords().length,
                            updateLength: listStore.getUpdatedRecords().length
                        };
                        listStore.sync({
                            callback: function (records, operation, success) { 
                                var mainPanel = view.up(view.mainPanelAlias);
                                var mainGrid = mainPanel.down('OwnersGridView');
                                var mainStore = mainGrid.getStore();
                                var record = mainGrid.getSelectionModel().getSelected();
                                var mainSelectedId = mainPanel.selectedId;
                                var getSelectedIndex = mainStore.find('ownerId', mainSelectedId, 0, false, false, true);
                                mainGrid.getSelectionModel().deselectAll();
                                mainGrid.getSelectionModel().select(getSelectedIndex, false, false);
                                if(mainPanel.selectedId){
                                    listStore.getProxy().extraParams = {
                                        selectedId: mainPanel.selectedId
                                    };
                                }
                                listStore.reload({
                                    callback: function () {
                                        if (view.getStore().getAt(0)) { 
                                                view.getSelectionModel().select(0, false, false);
                                        }                                     
                                    }
                                });
                            }
                        });
                        listStore.commitChanges();
                    }
                }
            });
        } else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                title:MineralPro.config.Runtime.listSyncMsgErrorTitle,
                msg:MineralPro.config.Runtime.listSyncMsgErrorMsg,
                buttons: Ext.Msg.OK
            });                      
        }                         
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsGridViewController onSyncListGridData.')
    },
    onExportLeaseHoldings: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsGridViewController onExportLeaseHoldings.')
        
        var me = this.getView();
        var main = this;
        var grid = me.getView();
        var store = grid.getStore();
        var selected = grid.getSelectionModel().getSelection();
        var listStore = grid.getStore(); 
        var listGrid = me.getView();
        var ownerId = Ext.getCmp('ProperRecordsManageOwnerId').selectedId;
        var ownerName = Ext.getCmp('ProperRecordsManageOwnerId').selectedName;
        var appraisalYear = MineralPro.config.Runtime.appraisalYear;
        var selectedName = '';
        var form = button.up();
        var enabledExport = true;

        Ext.each(store.getNewRecords(), function(el, index, items){
                for(var i=0; i<selected.length; i++){
                    if(selected[i].data.ownerId == el.data.ownerId){
                        enabledExport = false;
                    }
                }
        });

        if(enabledExport){
            if(selected.length > 0){
                var selectedModel = new Ext.data.Model({
                    fields:[
                        {name: 'ownerId', type: "int", defaultValue: 0},
                        {name: 'leaseId', type: "int", defaultValue: 1},
                        {name: 'idLeaseOwner', type: "int", defaultValue: 0},
                        {name: 'operatorId', type: "int", defaultValue: 0},
                        {name: 'interestPercent', type: "string", defaultValue: ''},
                        {name: 'interestTypeCd', type: "int", defaultValue: 0},
                        {name: 'ownerValue', type: "float", defaultValue: 0},
                        {name: 'operatorName', type: "string", defaultValue: ''},
                        {name: 'leaseName', type: "string", defaultValue: ''},
                        {name: 'interestTypeDesc', type: "string", defaultValue: 'UNASSIGNED'}
                    ]
                });
                var selectedStore = new Ext.data.Store({
                    model: selectedModel,
                    autoSave: false,
                    autoLoad: false,
                    proxy: {
                        type: "localstorage"         
                    }
                });
                
                for(var i=0; i<selected.length; i++){
                    var nRecord = new selectedStore.model;
                        nRecord.set('Lease_Id', selected[i].data.leaseId);
                        nRecord.set('Lease_Name', selected[i].data.leaseName);
                        nRecord.set('Operator_ID', selected[i].data.operatorId);
                        nRecord.set('Operator', selected[i].data.operatorName);
                        nRecord.set('Interest_Type', selected[i].data.interestTypeDesc);
                        nRecord.set('Interest_Percent', selected[i].data.interestPercent);
                        nRecord.set('Value', selected[i].data.ownerValue);
                        selectedStore.add(nRecord);
                        selectedName = selected[i].leaseName;
                }
                var jsonData = Ext.encode(Ext.pluck(selectedStore.data.items, 'data'));
                var sendJasonData = '{"data":'+jsonData+'}';
                var filename = ownerId+'_'+ownerName+'_'+Ext.Date.format(new Date(), 'Y-m-d')+'_AY'+appraisalYear;
                var headerName = ownerId+'_'+ownerName+'_'+appraisalYear;
                if(selected.length<=250){
                    Ext.MessageBox.show({
                        title:'Export Lease Holdings',
                        msg: '<p style="line-height:18px; margin:0px">Downloaded File Info <br />'+
                            'File name: '+filename+'<br />'+
                            'Owner name: '+ownerName+' <br />'+
                            'Number of records exported: '+ selected.length +'<br /></p>',
                        buttons: Ext.Msg.OKCANCEL,
                        icon: Ext.Msg.INFO,
                        fn: function(btn){                           
                            if (btn == "ok"){
                                form.submit({
                                    url: '/api/excel/download',
                                    standardSubmit: true,
                                    method: 'POST', 
                                    params: {
                                        data: sendJasonData,
                                        fileName: filename,
                                        exportType: 'ExportLeaseHoldings',
                                        headerName: headerName
                                    },
                                    success: function(fm, action) {
                                        //console.log(fm)
                                    },
                                    failure: function(form, response) {
                                        Ext.MessageBox.show({
                                            title:'Download Failed',
                                            msg: 'Cannot process your request to your browsers version.',
                                            buttons: Ext.Msg.OK,
                                            icon: Ext.Msg.ERROR,
                                        });
                                    }
                                });
                            }
                        }                
                    });
                } else {
                    Ext.MessageBox.show({
                        title:'Internal Error',
                        msg: "<p style='text-align:center;padding:0;margin:0;font-weight:bold;font-size:13px;'>Unable to Process Request.</p></br><p style='color:red;text-align:center;padding:0;margin:0;font-weight:normal;font-size:13px;'> Number of records exceeds limit to</br>250 maximum records</p>",
                        buttons: Ext.Msg.OK,
                        iconCls: Ext.Msg.INFO
                    });
                }
            } else {
                Ext.MessageBox.show({
                    title:'Alert',
                    msg: "<p style='text-align:center;padding:0;margin:0;font-weight:bold;font-size:13px;'>Please select/check record for Export DO",
                    buttons: Ext.Msg.OK,
                    iconCls: MineralPro.config.Runtime.listSyncMsgErrorIcon
                });
            }
        } else {
            Ext.MessageBox.show({
                title:'Alert',
                msg: "<p style='text-align:center;padding:0;margin:0;font-weight:bold;font-size:13px;'>Please save data before Export DO",
                buttons: Ext.Msg.OK,
                iconCls: MineralPro.config.Runtime.listSyncMsgErrorIcon
            });
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsGridViewController onExportLeaseHoldings.')
    },
});