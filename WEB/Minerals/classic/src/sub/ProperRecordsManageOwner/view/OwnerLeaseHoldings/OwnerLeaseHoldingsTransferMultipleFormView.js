Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsTransferMultipleFormView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.OwnerLeaseHoldingsTransferMultipleFormView',
    defaultFocus: 'OwnerNameItemId',
    requires: ['Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsTransferFormViewController'],
    controller: 'OwnerLeaseHoldingsTransferFormViewController',
   // saveAction: 'onTransferMultipleToMultipleGroupToGrid',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'ProperRecordsManageOwnerLeaseHoldingsId', //id of the main panel
    listGridStore: 'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsStore', 
    listGridAlias: 'OwnerLeaseHoldingsGridView', //alias name for note gridview
    store: 'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsOwnerTransferStore',
    loadMask: true,
    height: 300,
    layout: 'fit',
    viewConfig: {
        markDirty: false
    },
            
            
    columns: [{
        header: 'Owner Name',
        dataIndex: 'ownerName',
        itemId: 'OwnerNameItemId',
        flex: 2,
        filterElement: new Ext.form.TextField(),
    }, {
        header: 'Owner Address',
        dataIndex: 'ownerAddress',
        flex:2,
        filterElement: new Ext.form.TextField(),
    }, {
        header: 'Owner ID',
        dataIndex: 'ownerId',
        flex:1,
        filterElement: new Ext.form.TextField(),
    }],


    initComponent: function () {
        var me = this;
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')]
        me.selModel= Ext.create('Ext.selection.CheckboxModel',{
            checkOnly: true,
            showHeaderCheckbox: false, 
            listeners: {
                selectionchange:function(){},
                select:function(sel ,record, index){
                    var grid = me.getView();
                    var selectedOwner = this.getCount();
                    if(selectedOwner == 51){
                        Ext.Msg.show({
                            iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                            title:'Warning!',
                            msg:'Receving Owner must not be greater than 50.',
                            buttons: Ext.Msg.OK
                        }); 
                        this.deselect(index) 
                    }else{
                        var gridCalculation = me.up().down('OwnerLeaseHoldingsTransferCalculationGridFormView');
                        var gridlease = me.up().down('OwnerLeaseHoldingsTransferSelectedOwnerFormView');
                        var calcStore = gridCalculation.getStore();
                    // var ownerTransferPct = gridlease.getSelectionModel().getSelection()[0].data.ownerTransferPct;
                        var percent = 100;
                        var recordLease = gridlease.getSelectionModel().getSelection()[0];
                       // var button = me.up('OwnerLeaseHoldingsTransferBaseFormView').down('#saveId');
                        var buttonTransAll = me.up('OwnerLeaseHoldingsTransferBaseFormView').down('#transferAllId');
                        if(buttonTransAll.disabled == true){
                          //  button.enable();
                            buttonTransAll.enable();
                        }
                        if(recordLease){
                            var PctTransfer = recordLease.data.PctTransfer;
                            var interestPercent = recordLease.data.interestPercent;
                            var ownerValue = recordLease.data.ownerValue;
                            var calVal = (PctTransfer/100)*interestPercent;
                            record.set('PctTransfer', percent);
                            var totalsum = ''; 
                            var length = 0;
                            
                            calcStore.each(function(c, fn){
                                if(c.data.edited == 'Y'){
                                    totalsum = +c.data.percentReceive + +totalsum;
                                }else{
                                    length = length+1;
                                }
                            });
                            var curTotalOwnerTransferPct = 0;
                            var items = calcStore.data.length;
                            if(items == 0){
                            // var percent = 100;
                                var calcValue = (percent*calVal)/100;
                                var OwnerReceiveCalcVal =(ownerValue*PctTransfer)/100
                                var OwnerReceiveValue = (OwnerReceiveCalcVal*percent)/100;
                                record.set('edited', '');
                                record.set('percentReceive', percent);
                                record.set('OwnerReceiveValue', OwnerReceiveValue);
                                record.set('ownerReceivePct', Ext.util.Format.number(calcValue, '0.00000000'));
                                calcStore.insert(0, record); 

                            }else{
                                var percentEdit = (percent - totalsum)/(length+1);
                                var calcValue = (percentEdit*calVal)/100;
                                var OwnerReceiveCalcVal =(ownerValue*PctTransfer)/100
                                var OwnerReceiveValue = (OwnerReceiveCalcVal*percentEdit)/100;
                                record.set('edited', '');
                                record.set('OwnerReceiveValue', OwnerReceiveValue);
                                record.set('percentReceive', percentEdit);
                                record.set('ownerReceivePct', Ext.util.Format.number(calcValue, '0.00000000'));
                                calcStore.insert(0, record); 

                                calcStore.each(function(c, fn){
                                    var value = (percentEdit*calVal)/100;
                                    var OwnerReceiveCalcVal =(ownerValue*PctTransfer)/100
                                    var OwnerReceiveValue = (OwnerReceiveCalcVal*percentEdit)/100;
                                    if(c.data.edited == ''){
                                        c.set('OwnerReceiveValue', OwnerReceiveValue);
                                        c.set('percentReceive', percentEdit);
                                        c.set('ownerReceivePct', Ext.util.Format.number(value, '0.00000000'));
                                        calcStore.insert(0, c); 
                                    }
                                       curTotalOwnerTransferPct=parseFloat(curTotalOwnerTransferPct)+parseFloat(Ext.util.Format.number(c.data.ownerReceivePct, '0.00000000'));
                                    
                                });
                                var balanceVal = parseFloat(Ext.util.Format.number(calVal, '0.00000000')) - parseFloat(curTotalOwnerTransferPct);
                                if(balanceVal != 0){   
                                    var toCompute = parseFloat(balanceVal) + parseFloat(calcStore.getAt(0).data.ownerReceivePct);
                                    calcStore.getAt(0).set('ownerReceivePct', Ext.util.Format.number(toCompute, '0.00000000'));
                                }
                            }
                        }
                    }
                    
                },
                deselect: function(sel, record, index, eOpts ) {
                    var gridCalculation = me.up().down('OwnerLeaseHoldingsTransferCalculationGridFormView');
                    var gridlease = me.up().down('OwnerLeaseHoldingsTransferSelectedOwnerFormView');
                    var calcStore = gridCalculation.getStore();
                    //var ownerTransferPct = gridlease.getSelectionModel().getSelection()[0].data.ownerTransferPct;
                    var percent = 100;
                    var recordLease = gridlease.getSelectionModel().getSelection()[0];
                    var items = calcStore.data.length;
                    var curTotalOwnerTransferPct = 0;
                    //var button = me.up('OwnerLeaseHoldingsTransferBaseFormView').down('#saveId');
                    var buttonTransAll = me.up('OwnerLeaseHoldingsTransferBaseFormView').down('#transferAllId');
                    if(items == 1){
                      //  button.disable();
                        buttonTransAll.disable();
                    }
                    if(recordLease){
                        var PctTransfer = recordLease.data.PctTransfer;
                        var interestPercent = recordLease.data.interestPercent;
                        var ownerValue = recordLease.data.ownerValue;
                        var calVal = (PctTransfer/100)*interestPercent;
                        calcStore.remove(record, true);

                        var totalsum = ''; 
                        var length = 0;

                        calcStore.each(function(c, fn){
                            if(c.data.edited == 'Y'){
                                totalsum = +c.data.percentReceive + +totalsum;
                            }else{
                                length = length+1;
                            }
                        });
                        var items = calcStore.data.length;
                        calcStore.each(function(c, fn){
                            percentEdit = (percent - totalsum)/(length);
                            
                            if(items == 1){
                                percentEdit = percent; 
                            }
                            if(c.data.edited == ''){
                                var value = (percentEdit*calVal)/100;
                                var OwnerReceiveCalcVal =(ownerValue*PctTransfer)/100
                                var OwnerReceiveValue = (OwnerReceiveCalcVal*percentEdit)/100;
                                c.set('edited', '');
                                c.set('OwnerReceiveValue', OwnerReceiveValue);
                                c.set('percentReceive', percentEdit);
                                c.set('ownerReceivePct', Ext.util.Format.number(value, '0.00000000'));
                                calcStore.insert(0, c); 
                            }
                                curTotalOwnerTransferPct=parseFloat(curTotalOwnerTransferPct)+parseFloat(Ext.util.Format.number(c.data.ownerReceivePct, '0.00000000')); 
                            
                        });
                        var balanceVal = parseFloat(Ext.util.Format.number(calVal, '0.00000000')) - parseFloat(curTotalOwnerTransferPct);
                        if(balanceVal != 0){   
                            if(calcStore.getAt(0)){ 
                                var toCompute = parseFloat(balanceVal) + parseFloat(calcStore.getAt(0).data.ownerReceivePct);
                                calcStore.getAt(0).set('ownerReceivePct', Ext.util.Format.number(toCompute, '0.00000000'));
                            }
                        }
                    }
                }
            }
        })
        me.callParent(arguments);
    }
});