Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsTransferSelectedOwnerFormView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.OwnerLeaseHoldingsTransferSelectedOwnerFormView',
    defaultFocus: 'leaseNameitemId',
    requires: ['Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsTransferFormViewController'],
    controller: 'OwnerLeaseHoldingsTransferFormViewController',
    saveAction: 'onTransferMultipleGroupToGrid',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelAlias: 'ProperRecordsManageOwner',
    mainPanelId: 'ProperRecordsManageOwnerLeaseHoldingsId', //id of the main panel
    listGridStore: 'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsStore', 
    listGridAlias: 'OwnerLeaseHoldingsGridView', //alias name for note gridview
    basePanelId: 'OwnerLeaseHoldingsTransferBaseFormViewId',
    gridCalcAlias: 'OwnerLeaseHoldingsTransferCalculationGridFormView',
    store: 'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsTransferStore',
    loadMask: true,
    height: 215,
    layout: 'fit',

    features: [{
        id: 'group',
        ftype: 'groupingsummary',
        groupHeaderTpl: '{columnName}: {name}',
        hideGroupedHeader: true,
        enableGroupingMenu: false,
    }],
    columns: [{
        header: 'Lease ID',
        dataIndex: 'leaseId',
        flex: 1,
        minWidth: 85,
        filterElement: new Ext.form.TextField()
    },{
        header: 'Lease Name',
        dataIndex: 'leaseName',
        flex: 3,
        itemid: 'leaseNameitemId',
        filterElement: new Ext.form.TextField(),
        summaryType: 'count',
        summaryRenderer: function(value, summaryData, dataIndex) {
           return ((value === 0 || value > 1) ? '(' + value + ' Leases to transfer )' : ' 1 Lease to transfer ');
        }
    },{
        header: 'Owner ID',
        dataIndex: 'ownerId',
        flex: 1,
        minWidth: 85,
        filterElement: new Ext.form.TextField(),
    },{
        header: 'Owner Name',
        dataIndex: 'ownerName',
        flex: 3,
        filterElement: new Ext.form.TextField(),
    },{
        header: 'Owner Percent',
        dataIndex: 'interestPercent',
        flex: 1,
        minWidth: 120,
        filterElement: new Ext.form.TextField(),
    },{
        header: 'Owner Value',
        dataIndex: 'ownerValue',
        flex: 1,
        minWidth: 120,
        align: 'right',
        filterElement: new Ext.form.TextField(),
        renderer: function(val, meta) {
            return  Ext.util.Format.number(val, '$000,000');
        },
    },{
        header: 'Interest Type',
        dataIndex: 'interestTypeDesc',
        flex: 2,
        filterElement: new Ext.form.TextField(),
    },{
        header: 'Share to Transfer',
        dataIndex: 'PctTransfer',
        flex: 2,
        filterElement: new Ext.form.TextField(),
        editor: {
                allowBlank: false,
                xtype: 'textfield',
                maskRe:/[0-9.]/,
                minValue: 0,
                maxValue: 100
            },
        renderer: function(val, meta) {
            return  Ext.util.Format.number(val, '000.00%');
        },    
    },{
        header: 'Owner Transfer Percent ',
        dataIndex: 'ownerTransferPct',
        flex: 2,
        editor: {
                allowBlank: false,
                xtype: 'textfield',
                maskRe:/[0-9.]/,
                minValue: 0.00000000,
                maxValue: 1.00000000    
            },
        filterElement: new Ext.form.TextField(),
        renderer: function(value, metaData, record, rowIdx, colIdx, store, view) {
          // return Ext.util.Format.number((record.get('interestPercent') * record.get('PctTransfer')/100), '0.00000000');
          return Ext.util.Format.number(value, '0.00000000');
        }, 
    },{
        header: 'Value to Transfer',
        dataIndex: 'OwnerTransferValue',
        flex: 2,
        align: 'right',
        minWidth: 120,
        sortable:false,
        renderer: function(val, meta, record) {
            var ownerValue = record.get('ownerValue');
            var PctTransfer = record.get('PctTransfer');
            return Ext.util.Format.number((ownerValue * PctTransfer)/100, '$000,000');
        },
      //  filterElement: new Ext.form.TextField(),
    }
    ],
    listeners: {
         edit: 'onBeforeEditTop',
         validateedit: 'onValidateEditTop',
    },

    initComponent: function () {
        var me = this;
        me.plugins = [Ext.create('Ext.grid.plugin.CellEditing', {
            clicksToEdit: 2
            })
        ]
       // me.plugins = [Ext.create('Ext.ux.grid.FilterRow')]
        // me.selModel= Ext.create('Ext.selection.CheckboxModel',{
        //     checkOnly: true,
        //     mode: 'SINGLE',
        // })
        me.callParent(arguments);
    },
     viewConfig: {
        getRowClass: function(record) {
            // if (record.get('edited') == 'Y') {
            //     return 'edited-record';
            // } 
        },
    }
});