//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsAddFormViewController', {
    extend: 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.OwnerLeaseHoldingsAddFormViewController',

    onAddAndNew: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsAddFormViewController onAddAndNew.')
        var me = this;                     
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore); 
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
        var mainView = Ext.getCmp(view.mainPanelId);
        var ownerNameId = mainView.selectedId;
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();             
        if(form.isValid()){
            if(values.totalInterestPct <= 1){

                record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                record.set(values); 
                record.set('ownerId', ownerNameId)
                listStore.insert(0,record);
                
                listGrid.getSelectionModel().select(0, true);  
                //this part of the code will reset the data of the form window.
                //form.reset()
                //var new_record = Ext.create(listStore.model.displayName)//mainListModel
                var new_record = Ext.create(listGrid.mainListModel)                    
                form.loadRecord(new_record);
                view.focus(view.defaultFocus)
                //listGrid.fireEvent('afterlayout');
            }else{
                 Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: 'Lease Interest Pct Exceed',    
                    msg: 'The total Lease Interest Percent <br> have Exceeded 1.00000000',              
                    buttons: Ext.Msg.OK
                });
            }
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        }  
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsAddFormViewController onAddAndNew.')
    },
    
    /**
     * Add the data from the form into the data grid
     */
    onAddListToGrid: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsAddFormViewController onAddListToGrid.')
        var me = this;                     
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore); 
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();  
        var mainView = Ext.getCmp(view.mainPanelId);
        var ownerNameId = mainView.selectedId;

        if(form.isValid()){
            if(values.totalInterestPct <= 1){
                record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                record.set(values);   
                record.set('ownerId', ownerNameId)
                 
                win.close();
                listStore.insert(0,record);
                
                listGrid.getSelectionModel().select(0, true);  
                listGrid.getView().focusRow(0);   
                //listGrid.fireEvent('afterlayout');
            }else{
                 Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: 'Lease Interest Pct Exceed',    
                    msg: 'The total Lease Interest Percent <br> have Exceeded 1.00000000',               
                    buttons: Ext.Msg.OK
                });
            }
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsAddFormViewController onAddListToGrid.')
    },
     onAddCheckExisting: function(e, event, opt){
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsAddFormViewController onAddCheckExisting.')
        
        var me = this;  
        var view = me.getView();
        
         setTimeout(function (){                       
            if (view.id.toLowerCase().indexOf("edit") == -1) {

                var listStore = Ext.StoreMgr.lookup(view.listGridStore);

                if (listStore.findRecord(e.name, e.value, 0, false, false, true)) {
                    Ext.Msg.show({
                        iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
                        title:'Alert',
                        msg: 'Lease is already in your list.',
                        buttons: Ext.Msg.OK,
                        fn: function(){
                            e.focus();
                        }
                    });
                }
            }
        },200)
        
                             
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsAddFormViewController onAddCheckExisting.')    
    },
      onEditListToGrid: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerLeaseHoldingsAddFormViewController onEditListToGrid.')

        var me = this;        
        var view = me.getView();
        var mainPanel = Ext.getCmp(view.mainPanelId);
        var listGrid = mainPanel.down(view.listGridAlias)
        
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();
        
        if(form.isValid()){
            if(values.totalInterestPct <= 1){
                record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                record.set(values);                

                if(listGrid.withDetailsView){
                    var detailView = mainPanel.down(listGrid.detailAliasName);
                    detailView.loadRecord(record);
                }
                
                win.close(); 
            }else{
                 Ext.Msg.show({
                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                    title: 'Lease Interest Pct Exceed',    
                    msg: 'The total Lease Interest Percent <br> have Exceeded 1.00000000',               
                    buttons: Ext.Msg.OK
                });
            }                            
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        } 
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerLeaseHoldingsAddFormViewController onEditListToGrid.')
    },
});