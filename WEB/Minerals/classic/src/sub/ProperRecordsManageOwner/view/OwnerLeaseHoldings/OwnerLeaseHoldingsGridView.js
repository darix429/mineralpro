
Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.OwnerLeaseHoldingsGridView',
    store:'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsStore',
    xtype: 'grouped-grid',
    requires: ['Minerals.sub.ProperRecordsManageOwner.view.OwnerLeaseHoldings.OwnerLeaseHoldingsGridViewController',
    'Ext.grid.feature.Grouping'],
    controller: 'OwnerLeaseHoldingsGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'leaseNoItemId', 
    mainPanelAlias: 'ProperRecordsManageOwner', //widget alias name of the main panel
    mainListModel: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerLeaseHoldingsModel',     

    addFormAliasName: 'OwnerLeaseHoldingsAddFormView', //widget form to call for add list 
    editFormAliasName: 'OwnerLeaseHoldingsEditFormView', //widget form to call for edit
    
    firstFocus: 'leaseNoItemId',    
    
    addButtonTooltip: 'Add New Lease',
    addButtonText: 'Add New Lease',
    iconCls: 'icon-grid',
    selectFirstRow: false,

    features: [{
            id: 'group',
            ftype: 'groupingsummary',
            groupHeaderTpl: '{columnName}: {name}',
           // hideGroupedHeader: true,
            enableGroupingMenu: true,
            startCollapsed: true  
        }],
    columns:[{ 
        header: 'Lease ID',
        flex     : 0,
        sortable: true,
        dataIndex: 'leaseId',
        itemId: 'leaseNoItemId',
        //dataIndex: 'leaseId',
        summaryType: 'count',
        summaryRenderer: function(value, summaryData, dataIndex) {
           return ((value === 0 || value > 1) ? '(' + value + ' Leases)' : '(1 Lease)');
        },
        filterElement: new Ext.form.TextField()
    },{   
        header: 'Lease Name',
        flex     : 1,
        sortable: true,
        dataIndex: 'leaseName',
        filterElement:new Ext.form.TextField(),
        renderer: function (value) {
                        return '<a href="javascript:void(0);" >' + value + '</a>';
                    },
        listeners: {
            click: function (cell, td, rowIndex, cellIndex, e, record, value, index) {
                   this.up('grid').getController().onOpenSelectedTabView(record, rowIndex);
            }
        }
    },{   
        header: 'Operator Name',
        flex     : 1,
        sortable: true,
        dataIndex: 'operatorName',
        filterElement:new Ext.form.TextField()    
     },{   
        header: 'Interest Type',
        flex     : 1,
        sortable: true,
        dataIndex: 'interestTypeDesc',
        filterElement:new Ext.form.TextField()     
    },{   
        header: 'Interest Pct',
        flex     : 0,
        sortable: true,
        dataIndex: 'interestPercent',
        filterElement:new Ext.form.TextField() 
    },{   
        header: 'Value',
        flex     : 1,
        maxWidth: 180,
        sortable: true,
        align: 'right',
        dataIndex: 'ownerValue',
        summaryType: 'sum',
        summaryRenderer: function(val, params, data) {
           return  'Total Values '+Ext.util.Format.number(val, '$000,000');
        },
        renderer: function(val, meta) {
            return  Ext.util.Format.number(val, '$000,000');
        },
        filterElement:new Ext.form.TextField()      
    },
    // {
    //  xtype: 'actioncolumn',
    //     width: Minerals.config.Runtime.actionColWidth,
    //     items: [
    //         MineralPro.config.util.MainGridActionColCmpt.editCls,
    //         MineralPro.config.util.MainGridActionColCmpt.deleteCls,
    //     ],
    //     listeners: {
    //         afterrender: function(){
    //             var me = this       
    //             me.setHidden(MineralPro.config.Runtime.access_level)
    //         }
    //     }
    // }
    ],
      
    initComponent: function () {  
        var me = this;
        me.selModel= Ext.create('Ext.selection.CheckboxModel',{
            checkOnly: true,
            listeners: {
                selectionchange: function(){
                    var items = me.getSelectionModel().getSelected().length;
                    if(items){
                        me.down('#TransferToMultipleGroupButtonId').enable();
                        me.down('#exportLeaseHoldingsId').enable();
                    }else{
                        me.down('#TransferToMultipleGroupButtonId').disable();
                        me.down('#exportLeaseHoldingsId').disable();
                    }
                },
            }  
        }),  
        
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')]

        var TransferToMultipleGroupButton = {
                xtype: 'button',
                disabled: true,
                itemId: 'TransferToMultipleGroupButtonId',
                tooltip: Minerals.config.Runtime.ownerLeaseHoldingsTransferToMultipleBtnTooltip,
                text: Minerals.config.Runtime.ownerLeaseHoldingsTransferToMultipleBtnText,
                iconCls: Minerals.config.Runtime.ownerLeaseHoldingsTransferToMultipleBtnIcon,
                listeners: {
                    click: 'onTransferMultipleToMultipleGroup',
                    afterrender: function(){
                        var me = this
                        me.setHidden(MineralPro.config.Runtime.access_level)
                    }
                }
            }
            
			   var ExportLeaseHoldingsButton = {
				   xtype: 'form',
				   padding: 0,            
				   width: 170,
				   height: 33,
				   itemId:'formDownload',
				   align: 'center',
				   bodyStyle:{"background-color":"transparent"},  
				   items: [{
					   xtype: 'button',
					   tooltip: 'Export Lease Holdings',
					   text: 'Export Lease Holdings',
					   iconCls: 'export',
					   disabled: true,
					   itemId: 'exportLeaseHoldingsId',
					   alwaysShow: true,
					   listeners: {
						   click: 'onExportLeaseHoldings'
	//                        afterrender: function(){
	//                            var me = this
	//                            me.setHidden(MineralPro.config.Runtime.access_level)
	//                        }
					   }
				   }]
	   
			   }
           
        me.tools= [
            ExportLeaseHoldingsButton,
       // TransferGroupButton,    
        TransferToMultipleGroupButton,
        //MineralPro.config.util.MainGridToolCmpt.addNewDataButton,
        //MineralPro.config.util.MainGridToolCmpt.saveToDbButton,                   
        MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
        me.callParent(arguments);
        
    }  
});
