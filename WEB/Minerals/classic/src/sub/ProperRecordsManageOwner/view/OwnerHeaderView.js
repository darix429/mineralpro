Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerHeaderView' ,{
    extend: 'MineralPro.config.view.BaseHeaderView',
    alias : 'widget.ProperRecordsManageOwnerHeader',  
    layout: 'hbox',
    defaults: {
        style: {borderColor: '#000000', borderStyle: 'solid', borderWidth: '1px'},
        xtype: 'fieldset',
        layout: 'vbox',
        width: '24%',
        height: 95,
        margin: '0, 2, 0, 2',
        cls: 'header_fieldset',
    },
    items: [{
        title: 'Owner Information',
        width: '30%',
        layout: 'hbox',
        defaults: {
            xtype: 'container',
            layout: 'vbox',
            width: '100%',
            height: 95
        },
            items: [{
                defaults: {
                    xtype: 'displayfield',
                    fieldStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
                    labelStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px',
                    margin: '0 0 0 2',
                    padding: '0 0 0 0',
                    width: '100%',
                    labelWidth: 80,
                },               
                items:[{
                    fieldLabel: 'Owner ID',
                    name: 'ownerId',  
                },{  
                    fieldLabel: 'Owner Name',
                    name: 'name',
                },{
                    fieldLabel: 'In Care of',
                    name: 'inCareOf',  
                },{
                    fieldLabel: 'Agent',
                    name: 'agencyName',
                }]
            }]
    },{
        itemId: 'addressInfo',
        title: 'Address',
        width: '30%',
        layout: 'vbox',
        defaults: {
            xtype: 'container',
            layout: 'vbox',
            width: '100%',
        },
            items: [{
                itemId: 'address',
                defaults: {
                    xtype: 'displayfield',
                    fieldStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
                    labelStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px',
                    margin: '0 0 0 2',
                    padding: '0 0 0 0',
                    width: '100%',
                    labelWidth: 100,
                },               
                items:[{
                        fieldLabel: 'Address Line2',
                        name: 'addrLine2',  
                    },{
                        fieldLabel: 'Address Line3',
                        name: 'addrLine3',  
                    },{  
                        fieldLabel: 'City',
                        name: 'city',
                    },{
                        itemId: 'stateName',
                        fieldLabel: 'State/Zip',
                        name: 'stateName',  
                    },{
                        name: 'zipcode',
                        hidden: true,
                        listeners: {
                            render: function(status){
                                var me = this;
                                var stateName = me.up('#addressInfo').down('#address').down('#stateName');
                                if(stateName.getValue() != '' && status.value != ''){
                                    stateName.setValue(stateName.getValue() +", "+ status.value);
                                }else if(stateName.getValue() == '' && status.value){
                                    stateName.setValue("UNASSIGNED, "+ status.value);
                                }else if(status.value == '' && stateName.getValue()){
                                    stateName.setValue(stateName.getValue() +", UNASSIGNED");
                                }else{
                                    stateName.setValue("UNASSIGNED, UNASSIGNED");
                                }
                            }
                        } 
                    }]
            }]
    },{
        title: 'Holdings',
        width: '40%',
        layout: 'hbox',
        itemId: 'holdings',
        defaults: {
            xtype: 'container',
            layout: 'vbox',
        },
        items: [{
            itemId: 'col1',
            width: '55%',
            defaults: {
                xtype: 'displayfield',
                fieldStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
                labelStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px',
                margin: '0 0 0 2',
                padding: '0 0 0 0',
                width: '100%',
                labelWidth: 120,
            },               
            items:[{
                itemId: 'totalLeaseHolding',
                name: 'totalLeaseHolding',
                fieldLabel: 'Total Lease Holdings',
                margin: '0 10 0 2',
                valueAlign: 'left',
                fieldStyle: 'text-align:right;font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
                listeners: {
                    render: function(status){
                        this.setValue(status.value + ';');
                    }
                }
            },{  
                fieldLabel: 'Total Exempt',
                itemId: 'totalExemptionDesc',
                name: 'totalExemptionDesc',
                margin: '0 10 0 2',
                labelWidth: 75,
                fieldStyle: 'word-wrap:normal;text-align:right;font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
            },{
                itemId: 'totalExemptionExpirationDt',
                name: 'totalExemptionExpirationDt',
                hidden: true,
                listeners: {
                    render: function(status){
                        var me = this;
                        var col1 = me.up('#col1');
                        var totalExemptionDesc = col1.down('#totalExemptionDesc').getValue();
                        var totalExemptionStartYear = col1.up('#holdings').down('#col2').down('#totalExemptionStartYear');
                           
                        var today = new Date();
                        var dd = today.getDate();
                        var mm = today.getMonth()+1;

                        var yyyy = today.getFullYear();
                            if(dd<10){
                                dd='0'+dd;
                            } 
                            if(mm<10){
                                mm='0'+mm;
                            } 
                            var today = mm+'/'+dd+'/'+yyyy;
                            var current = new Date();
                            var expirationDate = new Date(status.value);
                            // console.log(totalExemptionDesc);
                            // console.log(current > expirationDate || totalExemptionDesc == '' || totalExemptionDesc == 'UNASSIGNED');
                            if(current > expirationDate || totalExemptionDesc == '' || totalExemptionDesc == 'UNASSIGNED'){
                                totalExemptionStartYear.hide();
                                col1.down('#totalExemptionDesc').hide();
                                var disableVetDesc = col1.down('#disableVetDesc').getValue();
                                var disableVetExpirationDt = new Date(col1.down('#disableVetExpirationDt').getValue());
                                if(disableVetDesc == 'UNASSIGNED' || disableVetDesc == '' || current > disableVetExpirationDt){
                                    col1.down('#noExemption').show();
                                }
                            }
                        }
                    } 
            },{  
                fieldLabel: 'Disable Vet',
                itemId: 'disableVetDesc',
                name: 'disableVetDesc',
                margin: '0 10 0 2',
                labelWidth: 80,
                fieldStyle: 'text-align:right;font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
                listeners: {
                    render: function(status){
                        var desc = status.value;
                        this.setValue(desc.replace("DISABLED VET ", ""));
                    }
                }    
            },{
                itemId: 'disableVetExpirationDt',
                name: 'disableVetExpirationDt',
                hidden: true,
                listeners: {
                    render: function(status){
                            var me = this;
                            var col1 = me.up('#col1');
                            var disableVetDesc = col1.down('#disableVetDesc').getValue();
                            var disableVetStartYear = col1.up('#holdings').down('#col2').down('#disableVetStartYear');
                        
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth()+1;

                            var yyyy = today.getFullYear();
                            if(dd<10){
                                dd='0'+dd;
                            } 
                            if(mm<10){
                                mm='0'+mm;
                            } 
                            var today = mm+'/'+dd+'/'+yyyy;
                            var current = new Date();
                            var expirationDate = new Date(status.value);
                            if(current > expirationDate || disableVetDesc == 'UNASSIGNED' || disableVetDesc == ''){
                                disableVetStartYear.hide();
                                disableVetStartYear.up('#col2').down('#amount').hide();
                                col1.down('#disableVetDesc').hide();
                                var totalExemptionDesc = col1.down('#totalExemptionDesc').getValue();
                                var totalExemptionExpirationDt = new Date(col1.down('#totalExemptionExpirationDt').getValue());
                                if(totalExemptionDesc == 'UNASSIGNED' || totalExemptionDesc == '' || current > totalExemptionExpirationDt){
                                    col1.down('#noExemption').show();
                                }
                            }
                        }
                    } 
            },{
                itemId: 'noExemption',
                fieldLabel: 'No Exemption',
                labelSeparator: '',
                hidden: true,
            }]
        },{
            itemId: 'col2',
            width: '45%',
            defaults: {
                xtype: 'displayfield',
                fieldStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
                labelStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px',
                margin: '0 0 0 2',
                padding: '0 0 0 0',
                width: '100%',
                labelWidth: 60,
            },               
            items:[{
                itemId: 'totalLeaseHoldingValue',
                name: 'totalLeaseHoldingValue',
                fieldLabel: 'Value',
                labelAlign: 'right',
                margin: '0 10 0 2',
                fieldStyle: 'text-align:right;font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
                listeners: {
                    render: function(status){
                        if(status.value == ''){
                            this.setValue("$ 0");
                        }else{
                            this.setValue(Ext.util.Format.number(status.value, '$ 000,000'));   
                        }
                    }
                }        
            },{  
                fieldLabel: 'Start Year',
                itemId: 'totalExemptionStartYear',
                name: 'totalExemptionEffectiveDt',
                margin: '0 10 0 2',
                labelAlign: 'right',
                fieldStyle: 'text-align:right;font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
                listeners: {
                    render: function(status){
                        var date = new Date(status.value);
                        this.setValue(date.getFullYear());
                    }
                 }
            },{  
                fieldLabel: 'Start Year',
                itemId: 'disableVetStartYear',
                name: 'disableVetEffectiveDt',
                margin: '0 10 0 2',
                labelAlign: 'right',
                fieldStyle: 'text-align:right;font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
                listeners: {
                    render: function(status){
                        var date = new Date(status.value);
                        this.setValue(date.getFullYear());
                    }
                 }
            },{
                name: 'amount',
                itemId: 'amount',
                fieldLabel: 'Amount',
                margin: '0 10 0 2',
                labelAlign: 'right',
                fieldStyle: 'text-align:right;font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:2px',
            }]
        }]
    }],
//    listeners: {
//        beforerender: function(){
//            var me = this;
//            me.up('northView').setHeight(140)
//            me.up('northView').down('#headerForm').down('#headerInfoItemId').setHeight(95)
//        },
//        beforedestroy: function(){
//            Ext.getCmp('northViewId').setHeight(100)
//            Ext.getCmp('northViewId').down('#headerForm').down('#headerInfoItemId').setHeight(60)
//        }
//  }      
});    
