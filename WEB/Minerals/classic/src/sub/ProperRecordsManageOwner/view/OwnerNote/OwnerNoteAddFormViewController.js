//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteAddFormViewController', {
    extend: 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.ownerNoteAddFormViewController', 

    onAddAndNew: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAddEditFormViewController onAddAndNew.')
        var me = this;                     
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore); 
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
                
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();                        
        if(form.isValid()){
            record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
            record.set(values);    
            listStore.insert(0,record);
            
            listGrid.getSelectionModel().select(0, true);  
			
            
            //this part of the code will reset the data of the form window.
            //form.reset()
            //var new_record = Ext.create(listStore.model.displayName)//mainListModel
            var new_record = Ext.create(listGrid.mainListModel)                    
            form.loadRecord(new_record);
            view.focus(view.defaultFocus)

            var ownerName = listGrid.down('#ownerNoteOwnerComboItemId').rawValue;
            view.down('#ownerNoteItemId').setValue(ownerName);

            var ownerId = listGrid.down('#ownerNoteOwnerComboItemId').getValue();
            view.down('#ownerIdItemIdNote').setValue(ownerId);
            //listGrid.fireEvent('afterlayout');
            
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        }  
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAddEditFormViewController onAddAndNew.')
    },
    
});