
Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteGridView', {
    extend: 'Minerals.config.view.Note.BaseNoteGridView',
    alias: 'widget.OwnerNoteGridView',
    store:'Minerals.sub.ProperRecordsManageOwner.store.OwnerNoteStore',
    viewConfig: {
        getRowClass: function(record, index, rowParams, store) {
            return record.dirty ? 'dirty-record' : '';
        }
    },
    requires: ['Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteGridViewController'],
    controller: 'OwnerNoteGridViewController',     
          
    //below are default items needed for grid checking 
    // firstLoadItemId: 'dateItemId', 
    mainPanelAlias: 'ProperRecordsManageOwner', //widget alias name of the main panel
    mainListModel: 'Minerals.model.MineralValuation.NotesModel',     

    addFormAliasName: 'OwnerNoteAddFormView', //widget form to call for add list 
    editFormAliasName: 'OwnerNoteEditFormView', //widget form to call for edit
   

    // firstFocus: 'dateItemId',    
    
    addButtonTooltip: 'Add New Note',
    addButtonText: 'Add New Note',
    
    columns:[{   
        header: 'Ref Year',
        flex     : 2,
        dataIndex: 'referenceAppraisalYear',
        itemId: 'referenceAppraisalYearItemId',
        filterElement: new Ext.form.TextField({
        })
    },{   
        header: 'Create Date',
        flex     : 2,
        dataIndex: 'createDt',
        itemId: 'dateItemId',
        filterElement: new Ext.form.TextField()
    },{   
        header: 'User',
        flex     : 2,
        dataIndex: 'noteAppraiserName',
        filterElement:new Ext.form.TextField()
    },{        
        header: 'Note',
        flex     : 4,
        dataIndex: 'note',
        cls: 'noteCls',
        tdCls: 'noteTdCls',
        filterElement:new Ext.form.TextField() 
    },{      
        header: 'Sequence No.',
        flex     : 2,
        dataIndex: 'seqNumber',
        filterElement:new Ext.form.TextField()     
    },{   
        header: 'Confidential',
        flex     : 2,
        dataIndex: 'securityCd',
        renderer: function(value){
            if(value == 2){
                return 'Yes';
            }else if(value == 1){
                return 'No';
            }
        },
        filterElement : new Ext.form.ComboBox({                              
            showFilterIcon:true,
            triggerAction           : 'all',                
            typeAhead               : true,                                
            mode                    : 'local',
            listWidth               : 160,
            hideTrigger             : false,
            emptyText               : 'Select',
            store                   :[['2','Yes'],['1','No']]
        })
    },{
     xtype: 'actioncolumn',
        width: Minerals.config.Runtime.actionColWidth,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls,
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        },
        renderer : function (v, cellValues, record, rowIdx, colIdx, store, view){
            var me = this
            var selectedYear = MineralPro.config.Runtime.appraisalYear
            var refAppraisalYear = record.data.referenceAppraisalYear
            if(refAppraisalYear*1 == selectedYear*1 || record.dirty){
                record.data.editRecord = true
                record.data.deleteRecord = true
            } else {
                record.data.editRecord = false
                record.data.deleteRecord = false
            }
        }
    }],
    listeners: {
        selectionchange: 'onLoadSelectionChange'
    }
    // listeners: {
    //     viewready: function (grid) {
    //     var view = grid.view;
    //     this.toolTip = Ext.create('Ext.tip.ToolTip', {
    //         target: view.el,
    //         delegate: view.itemSelector + ' .noteTdCls',
    //         //delegate: view.cellSelector, //all columns
    //         trackMouse: true,
    //         renderTo: Ext.getBody(),
    //         listeners: {
    //             beforeshow: function(tip) {
    //                 var trigger = tip.triggerElement,
    //                     parent = tip.triggerElement.parentElement,
    //                     columnTitle = view.getHeaderByCell(trigger).text,
    //                     columnDataIndex = view.getHeaderByCell(trigger).dataIndex,
    //                     columnText = view.getRecord(parent).get(columnDataIndex).toString();
    //                 if (columnText){
    //                     tip.update("<b>" + columnTitle + ":</b> " + columnText);
    //                 } else {
    //                     return false;
    //                 }
    //             }
    //         }
    //     });
    //     }
    // }
        
});