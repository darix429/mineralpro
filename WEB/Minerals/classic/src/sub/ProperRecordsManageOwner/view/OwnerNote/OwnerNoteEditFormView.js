Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteEditFormView', {
    extend: 'Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteAddFormView',
    //requires: ['Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteEditFormViewController'],
    //controller: 'OwnerNoteEditFormViewController',
    alias: 'widget.OwnerNoteEditFormView',
    
    
    title: 'Update Note',
    defaultFocus: '#noteId',
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});