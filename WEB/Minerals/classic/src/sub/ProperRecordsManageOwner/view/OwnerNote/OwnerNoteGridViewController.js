//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteGridViewController', {
    extend: 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.OwnerNoteGridViewController',

    // onSyncListGridData: function () {
    //     MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerNoteGridViewController onSyncListGridData.')
        
    //     var me = this;       
    //     var view = me.getView();
    //     var listStore = view.getStore();
                                  
    //     var dirty = listStore.getNewRecords().length > 0
    //     || listStore.getUpdatedRecords().length > 0
    //     || listStore.getRemovedRecords().length > 0;
    //     if (dirty) {
    //         Ext.Msg.show({
    //             title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
    //             msg:MineralPro.config.Runtime.syncGridMsgConfimationMsg,
    //             iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
    //             buttons: Ext.Msg.YESNO,
    //             scope: this,
    //             width: 250,
    //             fn: function (btn) {
    //                 if (btn === 'yes') {
    //                     view.getSelectionModel().deselectAll(true);
    //                     listStore.getProxy().extraParams = {
    //                         createLength: listStore.getNewRecords().length,
    //                         updateLength: listStore.getUpdatedRecords().length
    //                     };

    //                     listStore.sync({
    //                         callback: function (records, operation, success) { 
    //                             var ownerViewType = view.down('#ownerNoteViewTypeComboItemId').getValue();
    //                             var ownerNoteFilter = view.down('#ownerNoteOwnerComboItemId').getValue();
    //                             listStore.getProxy().extraParams = {
    //                                 ownerViewType: ownerViewType,
    //                                 ownerNoteFilter: ownerNoteFilter
    //                             }                               
    //                             listStore.reload();
    //                         }
    //                     });
    //                     listStore.commitChanges();
    //                 }
    //             }
    //         });
    //     } else {
    //         Ext.Msg.show({
    //             iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
    //             title:MineralPro.config.Runtime.listSyncMsgErrorTitle,
    //             msg:MineralPro.config.Runtime.listSyncMsgErrorMsg,
    //             buttons: Ext.Msg.OK
    //         });                      
    //     }                         
    //     MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerNoteGridViewController onSyncListGridData.')
    // },
    onLoadSelectionChange: function(model, record){
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerNoteGridViewController onLoadSelectionChange.')
        record = record[0];
        var me = this;
        var view = me.getView();
        var detailsView = view.nextSibling('OwnerNoteDetailView');

        if(record){
            detailsView.loadRecord(record);    
        }
        else{
            detailsView.reset();
        }
        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerNoteGridViewController onLoadSelectionChange.')
    }
});