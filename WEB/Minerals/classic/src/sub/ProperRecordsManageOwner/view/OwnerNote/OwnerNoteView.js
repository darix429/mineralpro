Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.ProperRecordsManageOwnerNote',
    layout:'border',
    
    id: 'ProperRecordsManageOwnerNoteId',
    
    items: [{
            // title: 'OwnerNote',
            xtype: 'OwnerNoteGridView',
            html:'center',
            region:'center'
        },{
            xtype: 'OwnerNoteDetailView',
            region:'east'
        }] 
});    