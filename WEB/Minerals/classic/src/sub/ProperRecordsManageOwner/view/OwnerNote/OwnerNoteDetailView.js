
Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteDetailView', {
    extend: 'MineralPro.config.view.BaseDetailView',
    alias: 'widget.OwnerNoteDetailView',
    
    title: 'Note Details',
    itemId: 'ownerNoteDetails',
    defaults: {
        width: 275,
        labelWidth: 93
    },   
    items: [{
        xtype: 'textareafield',
        fieldLabel:'Note',
        name: 'note',
        anchor: '100%',
        labelWidth: 80,
        grow: true, 
        readOnly: true 
    }]
});