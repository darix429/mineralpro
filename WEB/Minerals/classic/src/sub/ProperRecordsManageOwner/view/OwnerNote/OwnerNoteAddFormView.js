Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteAddFormView', {
    extend: 'Minerals.config.view.Note.BaseNoteAddEditFormView',
    // requires: ['Minerals.sub.ProperRecordsManageOwner.view.OwnerNote.OwnerNoteAddFormViewController'],
    // controller: 'ownerNoteAddFormViewController',
    alias: 'widget.OwnerNoteAddFormView',

    title: 'Add Notes',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#noteId',
    layout: 'fit',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'ProperRecordsManageOwnerId', //id of the main panel
    listGridStore: 'Minerals.sub.ProperRecordsManageOwner.store.OwnerNoteStore', //store for note grid
    listGridAlias: 'OwnerNoteGridView', 
    bodyPadding: 5,
    width: 800,
    height: 520,

    subjectType: 'Owner',
    
    subjectTypeLabel: 'Owner Name'
});