Ext.define('Minerals.sub.ProperRecordsManageOwner.view.Owners.OwnersEditFormView', {
    extend: 'Minerals.sub.ProperRecordsManageOwner.view.Owners.OwnersAddFormView',
    //requires: ['Minerals.sub.ProperRecordsManageOwner.view.Owners.OwnersEditFormViewController'],
    //controller: 'ownersEditFormViewController',
    alias: 'widget.OwnersEditFormView',
    
    
    title: 'Edit Owner',
    defaultFocus: '#comboAgency',
    
    listeners: {
        afterrender: function(){
            var me = this;
            // me.down('#ownerNoItemId').disable(true);
        }
    },

    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});