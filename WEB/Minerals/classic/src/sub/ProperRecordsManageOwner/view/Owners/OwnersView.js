Ext.define('Minerals.sub.ProperRecordsManageOwner.view.Owners.OwnersView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.ProperRecordsManageOwners',
    layout:'border',
    
    id: 'ProperRecordsManageOwnersId',
    
    items: [{
           // title: 'Owners',
            xtype: 'OwnersGridView',
            html:'center',
            region:'center'
        },{
            xtype: 'OwnersDetailView',
            region:'east'
        }] 
});    