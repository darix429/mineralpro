//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.ProperRecordsManageOwner.view.Owners.OwnersGridViewController', {
    extend:'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.OwnersGridViewController',

    onLoadSelectionChange: function (model, records) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnersGridViewController onLoadSelectionChange.')
        var me = this;

        if (!me.xtype) {
            me = me.getView();
        }

        var mainPanel = me.up(me.mainPanelAlias);
        var parentTab = false;
        var showHeader = false;
        if(mainPanel.selectedId != null){
            parentTab = true;
        }
        if(parentTab){
           if(me.withChildTab){
                if(records.length > 0){
                    mainPanel.selectedId = records[0].get(mainPanel.selectedIdIndex);
                    mainPanel.selectedName = records[0].get(mainPanel.selectedNameIndex);
                }
                var headerDetailStore = Ext.StoreMgr.lookup(me.headerInfoStore);
                headerDetailStore.getProxy().extraParams = {
                    selectedId: mainPanel.selectedId
                };

                headerDetailStore.on({
                    beforeload: {
                        fn: function(store, operation, eOpts){
                            MineralPro.config.RuntimeUtility.DebugLog('headerDetailStore beforeload');
                            MineralPro.config.RuntimeUtility.DebugLog(mainPanel.selectedId);
                            if(!mainPanel.selectedId || parseInt(mainPanel.selectedId) <= 0){
                                MineralPro.config.RuntimeUtility.DebugLog('Entering OwnersGridViewController onLoadSelectionChange headerDetailStore canceled.')
                                Ext.getBody().unmask();
                                return false;// Don't reload header
                            }
                            else{

                                MineralPro.config.RuntimeUtility.DebugLog('Entering OwnersGridViewController onLoadSelectionChange headerDetailStore continue to load. selectedId is '+mainPanel.selectedId);
                                Ext.getBody().mask("Loading Owner's Info...");
                            }
                        },
                        single: true
                    },
                    load: {
                        fn: function(store, rec, successful, operation, eOpts){
                            Ext.getBody().unmask();
                            if(successful){
                                if(rec.length>0){
                                    showHeader = true;
                                    me.headerDetail = rec[0]
                                }
                                updateHeader(me, rec, showHeader);
                                if (me.withDetailsView) {
                                    var detailView = me.up(me.mainPanelId).down(me.detailAliasName);

                                    if(me.detailAliasName == 'OwnersDetailView'){
                                        if(records[0] && records[0].dirty){
                                            evaluateExemption(detailView,records[0]);
                                        } else if (rec.length > 0){
                                            evaluateExemption(detailView, rec[0]);
                                        }
                                    }

                                    if(records[0] && records[0].dirty){
                                        detailView.loadRecord(records[0]);
                                    } else if (rec.length > 0){
                                        detailView.loadRecord(rec[0]);
                                    }
                                }
                                if(me.showEdit){
                                    me.getController().showEditViewViaHeader();
                                }
                            }
                            else{
                                Ext.getBody().unmask();
                                var descriptiveErrorMsg = '';
                                if(operation.getError() && operation.getError().statusText){
                                    descriptiveErrorMsg += '<p><span style="font-weight:bold;">Error Details: </span><span style="color:red;">'+operation.getError().statusText+'.</span></p>'
                                }
                                Ext.Msg.show({
                                    title: 'Oops!',
                                    msg: 'Something went wrong while retrieving header info.'+descriptiveErrorMsg,
                                    buttons: Ext.Msg.OK
                                });
                            }
                        },
                        single: true
                    }
                });
                headerDetailStore.load();
            }
        }else if (me.withDetailsView) {
            var detailView = me.up(me.mainPanelId).down(me.detailAliasName);

            if(records.length > 0){
                if(me.detailAliasName == 'OwnersDetailView'){
                    evaluateExemption(detailView,records[0]);
                }
                detailView.loadRecord(records[0]);
            }
            updateHeader(me, records, showHeader);
        }else{
            updateHeader(me, records, showHeader);
        }
        function updateHeader (me, records, showHeader){
            MineralPro.config.Runtime.headerInfoXtype='';
            if(me.headerInfo && me.headerInfo.length>0 && records.length > 0 && showHeader){
                var headerInfo = new Ext.create(me.headerInfo);
                headerInfo.loadRecord(records[0])
                MineralPro.config.Runtime.headerInfoXtype=headerInfo
            }
            Ext.globalEvents.fireEvent('updateHeader');
        }

        function evaluateExemption (view, record){
            var totalExemptionView = view.items.items[18];
            var disableVetView = view.items.items[19];
            var noExemption = view.items.items[20];
            totalExemptionView.hide();
            disableVetView.hide();
            noExemption.hide();

            const totalExemptionCd = record.data.totalExemptionCd;
            const totalExemptionExpirationDt = new Date(record.data.totalExemptionExpirationDt);
            const disableVetCd = record.data.disableVetCd;
            const disableVetExpirationDt = new Date(record.data.disableVetExpirationDt);
            const currentDate = new Date();
            var hasTotalExemption = false, hasDisableVet = false;

            if(totalExemptionCd != 0 && currentDate < totalExemptionExpirationDt){
                totalExemptionView.show();
                hasTotalExemption = true;
            }

            if(disableVetCd != 0 && currentDate < disableVetExpirationDt){
                disableVetView.show();
                hasDisableVet = true;
            }

            if(!hasTotalExemption && !hasDisableVet){
                noExemption.show();
            }
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnersGridViewController onLoadSelectionChange.')
    },
    // onAfterStateRestore: function(grid, state){
    //     console.log('--------------------START--------------------');
    //     if(state && state.storeState && state.storeState.filters.length){
    //         var grid = this.getView();
    //         var cols = grid.getColumns();
    //         var filters = state.storeState.filters;
    //         filters.forEach(function(filter){
    //             cols.forEach(function(col){
    //                 if(col.dataIndex && col.dataIndex.toLowerCase() == filter.property.toLowerCase()){
    //                     col.filterElement.suspendEvents();
    //                     col.filterElement.setValue(filter.value);
    //                     // col.filterElement.resumeEvents();
    //                 }
    //             });
    //         });
    //     }
    //     console.log('--------------------END--------------------');
    // },
    generateCrystalReprot: function(grid, rowIndex, colIndex, item, e, record){
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnersGridViewController generateCrystalReprot.')
         var me = this.getView();
            var mainPanel = me.up(me.mainPanelAlias);
            var reportID = record.get('reportId');
            var idOwner = record.get('ownerId');
            grid.getSelectionModel().select(rowIndex)
            // var url = window.location.origin + '/CrystalReport/'
            //                     +reportID+'/'
            //                     +mainPanel.selectedId+'/'
            //                     +idJournalEntry+'/'

            //            window.open(url);
                    Ext.Ajax.request({
                            waitMsg: 'Generating Report Preview...',
                            url: '/CrystalReport/'+reportID+'',
                            method:'GET',
                            params: {
                               year: MineralPro.config.Runtime.appraisalYear,
                               owner_id: idOwner
                            },
                            success: function(response){
                                var el = Ext.DomHelper.append(Ext.getBody(), {
                                    tag:          'a',
                                    href:          response.request.requestOptions.url,
                                    modal:true,
                                    target: '_blank'
                                });
                                el.click();
                            },
                            // failed: function(response){
                            //     Ext.MessageBox.show({
                            //         title:'Export Failed',
                            //         msg: 'Cannot process your request.',
                            //         buttons: Ext.Msg.OK,
                            //         icon: Ext.Msg.ERROR,
                            //     });
                            // }
                        });
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnersGridViewController generateCrystalReprot.')
    },
    onSyncListGridData: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnersGridViewController onSyncListGridData.')
        var me = this;
        var view = me.getView();
        var listStore = view.getStore();

        var dirty = listStore.getNewRecords().length > 0
        || listStore.getUpdatedRecords().length > 0
        || listStore.getRemovedRecords().length > 0;
        if (dirty) {
            Ext.Msg.show({
                title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                msg:MineralPro.config.Runtime.syncGridMsgConfimationMsg,
                iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                buttons: Ext.Msg.YESNO,
                scope: this,
                width: 250,
                fn: function (btn) {
                    if (btn === 'yes') {
                        view.getSelectionModel().deselectAll(true);
                        listStore.getProxy().extraParams = {
                            createLength: listStore.getNewRecords().length,
                            updateLength: listStore.getUpdatedRecords().length
                        };

                        listStore.sync({
                            success: function(batch, options){
                                
                                var filterData = view.getFilterData();//getFilterData is bound on FilterAllRows Plugin
                                var filterCount = Ext.Object.getValues(filterData).filter(function(val){return val.trim();}).length;

                                if(filterCount == 0){
                                    var deletedRecords = listStore.queryRecords('rowDeleteFlag','D');
                                    listStore.remove(deletedRecords);
                                    if(listStore.getCount()==0 && view.clearGridFilters){
                                        view.clearGridFilters();
                                    }
                                    else{
                                        view.getSelectionModel().select(0, false, false);
                                    }
                                }
                                else{
                                    listStore.reload({
                                        callback: function(records, operation, success){
                                            if(view.getStore().getAt(0)){
                                                view.getSelectionModel().select(0, false, false);
                                            }
                                        }
                                    })
                                }
                                var comboStore = Ext.getStore('Minerals.store.ProperRecords.OwnerComboStore');
                                comboStore.reload();
                            },
                            failure: function(batch, options){
                                Ext.Msg.show({
                                    iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                                    title:MineralPro.config.Runtime.listSyncMsgErrorTitle,
                                    msg:MineralPro.config.Runtime.listSyncMsgErrorMsg,
                                    buttons: Ext.Msg.OK
                                });
                            }
                        });
                        listStore.commitChanges();
                    }
                }
            });
        } else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                title:MineralPro.config.Runtime.listSyncMsgErrorTitle,
                msg:MineralPro.config.Runtime.listSyncMsgErrorMsg,
                buttons: Ext.Msg.OK
            });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnersGridViewController onSyncListGridData.')
    }
});
