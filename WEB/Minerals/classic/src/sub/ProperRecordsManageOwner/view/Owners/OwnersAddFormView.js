Ext.define('Minerals.sub.ProperRecordsManageOwner.view.Owners.OwnersAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.sub.ProperRecordsManageOwner.view.Owners.OwnersAddFormViewController'],
    controller: 'ownersAddFormViewController',
    alias: 'widget.OwnersAddFormView',

    title: 'Add New Owner',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#comboAgency',
    layout: 'fit',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'ProperRecordsManageOwnersId', //id of the main panel
    listGridStore: 'Minerals.sub.ProperRecordsManageOwner.store.OwnerStore', //store for note grid
    listGridAlias: 'OwnersGridView', 
    bodyPadding: 5,
    
    initComponent: function() {
        var me = this;
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!

        var yyyy = today.getFullYear();
        if(dd<10){
           dd='0'+dd;
        } 
        if(mm<10){
           mm='0'+mm;
        } 
        var today = mm+'/'+dd+'/'+yyyy;

        me.items = [{
            xtype: 'form',
            defaults:{ maskRe: /[^'\^]/ },
            layout: 'vbox',
            bodyPadding: 5,
            items:[{
                xtype: 'form',
                defaults:{ maskRe: /[^'\^]/ },
                layout: 'hbox',
                // bodyPadding: 5,
                items:[{
                    xtype: 'fieldset',
                    flex: 1,
                    margin:'0 5 0 0',
                    title: 'Owner Information',
                    autoheight: true,
                    defaults:{
                        width:320,
                        maskRe: /[^'\^]/ 
                    },  
                    items: [{
                        fieldLabel: 'Agent',
                        selectOnFocus: true,
                        tabIndex: 1,
                        labelWidth: 70,
                        name : 'agencyCdx',
                        xtype:'combo',
                        itemId:'comboAgency',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        displayField: 'agencyName',
                        valueField: 'agencyCdx',   
                        store: 'Minerals.store.ProperRecords.AgencyStore',
                        typeAhead: true,
                        // allowBlank: false,
                        queryMode:'local',
                        listeners:{
                            Select:function(record){ 
                                me.down('#idagency').setValue(me.down('#comboAgency').getRawValue());
                            }
                        }, 
                        forceSelection: true,
                        msgTarget: 'side'  
                    },{       
                        xtype: 'hidden',     
                        name: 'agencyName',   
                        queryMode:'local',
                        itemId:'idagency' 
                    },{
                        fieldLabel: 'Name',
                        xtype: 'textfield',
                        selectOnFocus: true,
                        tabIndex: 2,
                        labelWidth: 70,
                        name : 'name',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        allowBlank: false,
                        msgTarget: 'side',
                        maxLength: 50,
                    },{
                        fieldLabel: 'Name 2',
                        xtype: 'textfield',
                        selectOnFocus: true,
                        tabIndex: 3,
                        labelWidth: 70,
                        name : 'name2',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        // allowBlank: false,
                        msgTarget: 'side',
                        maxLength: 50,
                    },{
                        fieldLabel: 'Birth Date',
                        xtype: 'datefield',
                        selectOnFocus: true,
                        tabIndex: 4,
                        labelWidth: 70,
                        minValue: new Date(1800, 1, 1),
                        name : 'birthDt',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        msgTarget: 'side'
                    }]
                },{
                    xtype: 'fieldset',
                    flex: 1,
                    margin:'0 0 5 0',
                    title: 'Owner Address',
                    autoheight: true,
                    defaults:{
                        width:350,
                        maskRe: /[^'\^]/ 
                    }, 
                    items: [{
                        fieldLabel: 'In Care of',
                        itemId: 'inCareOf',
                        selectOnFocus: true,
                        tabIndex: 5,
                        name: 'inCareOf',
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        enableKeyEvents: true,
                        // listeners: {
                        //     blur: 'onCheckAddress'                    
                        // },
                        maxLength: 50,
                        validator: function(v) {
                            var me = this.up();
                            if (me.down('#inCareOf').getValue()!=''||me.down('#addrLine2').getValue()!=''||me.down('#addrLine3').getValue()!='') {
                                return true;
                            }
                            return 'This field is required';
                        }
                    },{
                        fieldLabel: 'Address Line 2',
                        itemId: 'addrLine2',
                        selectOnFocus: true,
                        tabIndex: 6,
                        name: 'addrLine2',
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        enableKeyEvents: true,
                        maxLength: 50,
                        validator: function(v) {
                            var me = this.up();
                            if (me.down('#inCareOf')!=''||me.down('#addrLine2')!=''||me.down('#addrLine3')!='') {
                                return true;
                            }
                            return 'This field is required';
                        }
                        
                    },{
                        fieldLabel: 'Address Line 3',
                        itemId: 'addrLine3',
                        selectOnFocus: true,
                        tabIndex: 7,
                        name: 'addrLine3',
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        enableKeyEvents: true,
                        maxLength: 50,
                        validator: function(v) {
                            var me = this.up();
                            if (me.down('#inCareOf')!=''||me.down('#addrLine2')!=''||me.down('#addrLine3')!='') {
                                return true;
                            }
                            return 'This field is required';
                        }
                        //listeners: {
                        //    blur: 'onCheckAddress'                    
                        //}
                    },{
                        fieldLabel: 'City',
                        selectOnFocus: true,
                        tabIndex: 8,
                        name: 'city',
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        //validateBlank: true,
                        allowBlank: false,
                        msgTarget: 'side',
                        maxLength: 35,
                    },{
                        fieldLabel: 'State',
                        selectOnFocus: true,
                        tabIndex: 9,
                        name : 'stateCd',
                        xtype:'combo',
                        itemId:'comboState',
                        margin: '0 0 10',
                        selectOnTab:false,
                        labelAlign: 'right',
                        displayField: 'stateName',
                        valueField: 'stateCd',   
                        store: 'MineralPro.store.StateStore',
                        typeAhead: true,
                        forceSelection: true,
                        queryMode:'local', 
                        enableKeyEvents: true,
                        listeners:{
                            Select:function(record){ 
                                me.down('#idstate').setValue(me.down('#comboState').getRawValue());
                            } ,
                            keyup: function(data, e, eOpts){
                                if(data.getRawValue() == ''){
                                    me.down('#idstate').setValue(''); 
                                }
                            },
                        }, 
                        allowBlank: false,
                        msgTarget: 'side'     
                    },{       
                        xtype: 'hidden',     
                        name: 'stateName',   
                        queryMode:'local',
                        itemId:'idstate' 
                    },{
                        fieldLabel: 'Zip Code',
                        selectOnFocus: true,
                        tabIndex: 10,
                        name: 'zipcode',
                        xtype: 'textfield',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        msgTarget: 'side',
                        allowBlank: false,
                        maxLength:9,
                        minLength:5,
                        maskRe:/[0-9.]/,
                        // vtype:'zipCode',
                        enableKeyEvents: true,
                        // listeners: {
                        //     keypress: 'onCheckZipCode',
                            // afterrender: function(record) {
                            //     var value = record.getValue();
                            //     if(value.length>5){
                            //         record.setValue(value.slice(0,5)+'-'+value.slice(5,value.length));
                            //     }
                            // }                
                        // }
                    },{
                        fieldLabel: 'Country',
                        selectOnFocus: true,
                        tabIndex: 11,
                        name : 'countryCd',
                        xtype:'combo',
                        itemId:'comboCountry',
                        margin: '0 0 10',
                        labelAlign: 'right',
                        displayField: 'countryName',
                        valueField: 'countryCd',   
                        store: 'MineralPro.store.CountryStore',
                        forceSelection: true,
                        typeAhead: true,
                        queryMode:'local', 
                        listeners:{
                            Select:function(record){ 
                                me.down('#idcountry').setValue(me.down('#comboCountry').getRawValue());
                            },
                        }, 
                        allowBlank: false,
                        msgTarget: 'side'     
                    },{      
                        xtype: 'hidden',      
                        name: 'countryName',   
                        queryMode:'local',
                        itemId:'idcountry' 
                    }]  
                },{
                    xtype: 'form',
                    defaults:{ maskRe: /[^'\^]/ },
                    layout: 'vbox',
                    items:[{
                        xtype: 'fieldset',
                        defaults:{
                            width:320,
                            maskRe: /[^'\^]/ 
                        }, 
                        flex: 1,
                        margin:'0 0 0 5',
                        title: 'Email & Phone',
                        autoheight: true, 
                        items: [{
                            fieldLabel: 'Email Address',
                            selectOnFocus: true,
                            tabIndex: 12,					
                            name: 'emailAddress',
                            xtype: 'textfield',
                            margin: '0 0 10',
                            labelAlign: 'right',
                            //validateBlank: true,
                            //allowBlank: false,
                            msgTarget: 'side',
                            vtype:'email',
                            maxLength: 50,
                        },{
                            fieldLabel: 'Phone Number',
                            selectOnFocus: true,
                            tabIndex: 13,
                            name: 'phoneNum',
                            xtype: 'textfield',
                            margin: '0 0 10',
                            labelAlign: 'right',
                            //validateBlank: true,
                            //allowBlank: false,
                            msgTarget: 'side',
                            // vtype:'phone',
                            enableKeyEvents: true,
                            maxLength: 10,
                            maxValue: 9999999999,
                            minValue: 0,
                            maskRe:/[0-9.]/,
                            // listeners: {
                            //     keypress: 'onCheckNumber'                  
                            // }
                        },{
                            fieldLabel: 'Fax Number',
                            selectOnFocus: true,
                            tabIndex: 14,
                            name: 'faxNum',
                            xtype: 'textfield',
                            margin: '0 0 7',
                            labelAlign: 'right',
                            msgTarget: 'side',
                            maskRe:/[0-9.]/,
                            // vtype:'phone',
                            enableKeyEvents: true,
                            maxLength: 10,
                            maxValue: 9999999999,
                            minValue: 0,
                        },
                        ]  
                    },{
                        xtype: 'fieldset',
                        defaults:{
                            width:320,
                            maskRe: /[^'\^]/ 
                        }, 
                        flex: 1,
                        margin:'5 0 0 5',
                        autoheight: true, 
                        items: [{
                            fieldLabel: 'Change Reason',
                            selectOnFocus: true,
                            tabIndex: 16,
                            name : 'ownerChangeCd',
                            xtype:'combo',
                            itemId:'comboChangeReason',
                            margin: '10 0 15',
                            labelAlign: 'right',
                            displayField: 'changeReason',
                            valueField: 'ownerChangeCd',   
                            store: 'Minerals.sub.ProperRecordsManageOwner.store.ChangeReasonStore',
                            typeAhead: true,
                            queryMode:'local',
                            listeners:{
                                Select:function(record){ 
                                    me.down('#idChangeReason').setValue(me.down('#comboChangeReason').getRawValue());
                                },
                            }, 
                            forceSelection: true,
                            allowBlank: true,
                            msgTarget: 'side'  
                        },{       
                            xtype: 'hidden',     
                            name: 'changeReason',   
                            queryMode:'local',
                            itemId:'idChangeReason' 
                        },{
                            text: 'Do you want to hide owner information?',
                            tabIndex: 18,
                            xtype: 'label',
                            margin: '15 0 10',
                            labelAlign: 'right'
                        },{
                            xtype: 'radiogroup',
                            fieldLabel: 'Confidential',
                            margin: '10 0 10',
                            labelAlign: 'right',
                            name: 'confidential',
                            items: [{
                                xtype: 'radiofield',
                                boxLabel: 'Yes', 
                                inputValue: 'Yes',
                                name: 'confidential',
                                margin: '0 10 0',
                            },{
                                xtype: 'radiofield',
                                boxLabel: 'No', 
                                name: 'confidential',
                                inputValue: 'No'
                            }],
                            setValue:function(value) {
                                var val = Ext.isObject(value) ? value : {confidential:value};
                                Ext.form.RadioGroup.prototype.setValue.call(this, val);
                            }
                        }]
                    }]
                }]  
            },{
                xtype: 'form',
                defaults:{ maskRe: /[^'\^]/ },
                layout: 'vbox',
                // bodyPadding: 5,
                items:[{
                    xtype: 'fieldset',
                    flex: 1,
                    // margin:'0 5 0 0',
                    title: 'Owner Exemption',
                    defaults: {anchor: '100%'},
                    autoheight: true,
                    layout: 'anchor',
                    defaults:{
                        maskRe: /[^'\^]/ 
                    },  
                    items :[{
                        layout: 'vbox',
                        xtype:"container",
                        items: [{
                            layout: 'hbox',
                            xtype:"container",
                            margin: '5 0',
                            items: [{
                                margin: '0 0 0',
                                xtype:'combo',
                                labelWidth: 100,
                                fieldLabel: 'Total Exemption',
                                width: 400,
                                labelAlign: 'right',
                                itemId: 'comboTotalExemptionDescription',
                                name : 'totalExemptionCd',
                                displayField: 'totalExemptionDesc',
                                valueField: 'totalExemptionCd',
                                store: 'Minerals.sub.ProperRecordsManageOwner.store.OwnerTotalExemptionStore',
                                typeAhead: true,
                                queryMode: 'local',
                                forceSelection: true,
                                allowBlank: true,
                                msgTarget: 'side',
                                tabIndex: 19,
                                listeners:{
                                    select:function(record){ 
                                        me.down('#idTotalExemptionDesc').setValue(me.down('#comboTotalExemptionDescription').getRawValue());
            
                                        if(me.down('#comboTotalExemptionDescription').getRawValue() == 'UNASSIGNED'){
                                            me.down('#totalExemptionEffectDate').setValue('');
                                            me.down('#totalExemptionExpirationDate').setValue('');
                                        }else{
                                            if(!me.down('#totalExemptionEffectDate').getValue()){
                                                me.down('#totalExemptionEffectDate').setValue(today);
                                            }
                                        }
                                    }
                                }
                            },{       
                                xtype: 'hidden',     
                                name: 'totalExemptionDesc',   
                                queryMode:'local',
                                itemId:'idTotalExemptionDesc' 
                            },{       
                                xtype: 'hidden',     
                                name: 'totalIdOwnerExemption',   
                            },{
                                fieldLabel: 'Effective Date',
                                selectOnFocus: true,
                                tabIndex: 20,
                                width: 332,
                                itemId: 'totalExemptionEffectDate',
                                xtype: 'datefield',
                                format: 'Y-m-d',
                                name : 'totalExemptionEffectiveDt',
                                labelAlign: 'right',
                                allowBlank: true,
                                msgTarget: 'side'
                            },{
                                fieldLabel: 'Expiration Date',
                                selectOnFocus: true,
                                width: 332,
                                tabIndex: 21,
                                itemId: 'totalExemptionExpirationDate',
                                xtype: 'datefield',
                                format: 'Y-m-d',
                                name : 'totalExemptionExpirationDt',
                                labelAlign: 'right',
                                allowBlank: true,
                                msgTarget: 'side'
                            }]
                        },{
                            layout: 'hbox',
                            xtype:"container",
                            items: [{
                                margin: '0 0 0',
                                xtype:'combo',
                                labelWidth: 100,
                                fieldLabel: 'Disable Vet',
                                width: 400,
                                labelAlign: 'right',
                                itemId: 'comboDisableVetDescription',
                                name : 'disableVetCd',
                                displayField: 'disableVetDesc',
                                valueField: 'disableVetCd',
                                store: 'Minerals.sub.ProperRecordsManageOwner.store.OwnerDisableVetStore',
                                typeAhead: true,
                                queryMode: 'local',
                                forceSelection: true,
                                allowBlank: true,
                                msgTarget: 'side',
                                tabIndex: 22,
                                listeners:{
                                    select:function(record){ 
                                        me.down('#idDisableVetDesc').setValue(me.down('#comboDisableVetDescription').getRawValue());
                                        if(me.down('#comboDisableVetDescription').getRawValue() == 'UNASSIGNED'){
                                            me.down('#disableVetEffectDate').setValue('');
                                            me.down('#disableVetExpirationDate').setValue('');
                                        }else{
                                            if(!me.down('#disableVetEffectDate').getValue()){
                                                me.down('#disableVetEffectDate').setValue(today);
                                            }
                                            try{
                                                me.down('#amountTextfield').setValue(record.displayTplData[0].amount);
                                            }catch(err){
                                            }
                                        }
                                    }
                                }
                            },{       
                                xtype: 'hidden',     
                                name: 'disableVetDesc',   
                                queryMode:'local',
                                itemId:'idDisableVetDesc' 
                            },{       
                                xtype: 'hidden',     
                                name: 'disableIdOwnerExemption',   
                            },{       
                                fieldLabel: 'Amount',
                                tabIndex: 23,
                                itemId: 'amountTextfield',
                                xtype: 'textfield',
                                name : 'amount',
                                labelAlign: 'right',
                                readOnly: true,
                                hidden:true,
                            },{
                                fieldLabel: 'Effective Date',
                                selectOnFocus: true,
                                tabIndex: 24,
                                width: 332,
                                itemId: 'disableVetEffectDate',
                                xtype: 'datefield',
                                format: 'Y-m-d',
                                name : 'disableVetEffectiveDt',
                                labelAlign: 'right',
                                allowBlank: true,
                                msgTarget: 'side'
                            },{
                                fieldLabel: 'Expiration Date',
                                selectOnFocus: true,
                                width: 332,
                                tabIndex: 25,
                                itemId: 'disableVetExpirationDate',
                                xtype: 'datefield',
                                format: 'Y-m-d',
                                name : 'disableVetExpirationDt',
                                labelAlign: 'right',
                                allowBlank: true,
                                msgTarget: 'side'
                            }]
                        }]
                    }]
                }]
            }]
        }];
        this.callParent(arguments);
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
        MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});