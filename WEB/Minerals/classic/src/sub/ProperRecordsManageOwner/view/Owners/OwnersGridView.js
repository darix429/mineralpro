
Ext.define('Minerals.sub.ProperRecordsManageOwner.view.Owners.OwnersGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.OwnersGridView',
    store:'Minerals.sub.ProperRecordsManageOwner.store.OwnerStore',

    requires: ['Minerals.sub.ProperRecordsManageOwner.view.Owners.OwnersGridViewController'],
    controller: 'OwnersGridViewController',

    //below are default items needed for grid checking
    firstLoadItemId: 'ownerNoItemId',
    mainPanelAlias: 'ProperRecordsManageOwner', //widget alias name of the main panel
    mainListModel: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerModel',
    exemptionModel: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerExemptionModel',
    exemptionStore: 'Minerals.sub.ProperRecordsManageOwner.store.OwnerExemptionStore',

    detailAliasName: 'OwnersDetailView',
    exemptionAliasName: 'OwnersExemptionGridView',
    withDetailsView: true, //true if it has details view
    withChildTab: true,
    withDependentGridView: true,
    withDependentGridViewStore: 'Minerals.sub.ProperRecordsManageOwner.store.OwnerExemptionStore',

    addFormAliasName: 'OwnersAddFormView', //widget form to call for add list
    editFormAliasName: 'OwnersEditFormView', //widget form to call for edit
    addExemptionAliasName: 'OwnersAddExemptionView',
    headerInfo: 'Minerals.sub.ProperRecordsManageOwner.view.OwnerHeaderView',
    headerInfoAlias: 'ProperRecordsManageOwnerHeader',
    headerInfoStore: 'Minerals.sub.ProperRecordsManageOwner.store.OwnerHeaderDetailStore',

    affectedComboStore: ['Minerals.store.ProperRecords.OwnerComboStore',
                'Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsOwnerTransferStore'],//combos store for agent

    firstFocus: 'ownerNoItemId',

    addButtonTooltip: 'Add New Owner',
    addButtonText: 'Add New Owner',
    bodyCls: 'darker-grid',
    // stateful: true,
    // stateId: 'OwnersGridView',
    columns:[{
        header: 'Owner ID',
        flex     : 0,
       // maxWidth: 75,
        dataIndex: 'ownerId',
        itemId: 'ownerNoItemId',
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true,
            // inputType: 'number'
        })
    },{
        header: 'Owner Name',
        flex     : 2,
        autoSizeColumn: true,
      //  minWidth: 160,
        dataIndex: 'name',
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        })
    },{
        header: 'In Care Of',
        flex     : 1,
      //  minWidth: 100,
        dataIndex: 'inCareOf',
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        })
    },{
        header: 'Agent Name',
        flex     : 1,
      //  minWidth: 100,
        dataIndex: 'agencyName',
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        })
    },{
        header: 'Address',
        flex     : 2,
      //  minWidth: 100,
        dataIndex: 'addrLine3',
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        })
    },{
    //     header: 'Change Reason',
    //     flex     : 1,
    //  //   minWidth: 100,
    //     dataIndex: 'changeReason',
    //     filterElement:new Ext.form.field.Text()
    // },{
    //     header: 'Confidential',
    //     flex     : 0,
    //   //  maxWidth: 80,
    //     dataIndex: 'confidential',
    //     filterElement : new Ext.form.ComboBox({
    //         showFilterIcon:true,
    //         triggerAction           : 'all',
    //         typeAhead               : true,
    //         mode                    : 'local',
    //         listWidth               : 160,
    //         hideTrigger             : false,
    //         emptyText               :    'Select',
    //         store                   :[['Yes','Yes'],['No','No']]
    //     })
    // },{
     xtype: 'actioncolumn',
        width: 70,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls,{
                icon:MineralPro.config.Runtime.crystalIconCls,
                tooltip:'Notice Of Determination',
                handler: function (grid, rowIndex, colIndex, item, e, record) {
                        this.up('grid').getController().generateCrystalReprot(grid, rowIndex, colIndex, item, e, record)
                },
            },
        ],
        listeners: {
            afterrender: function(){
                var me = this
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    }],

    initComponent: function () {
        var me = this;
        // me.plugins = [Ext.create('Ext.ux.grid.FilterRow')];
        me.plugins = [Ext.create('Ext.ux.grid.FilterAllRows',{
            pluginId: 'filterAllRows'
        })];
        me.tools = [
            MineralPro.config.util.MainGridToolCmpt.addNewDataButton,
            MineralPro.config.util.MainGridToolCmpt.saveToDbButton,
            // MineralPro.config.util.MainGridToolCmpt.clearFilterButton
        ];
        // me.tools = [{
        //     xtype: 'container',
        //     width: '100%',
        //     layout: {
        //         type: 'hbox',
        //         pack: 'end'
        //     },
        //     items: [
        //         MineralPro.config.util.MainGridToolCmpt.addNewDataButton,
        //         MineralPro.config.util.MainGridToolCmpt.saveToDbButton,
        //         // MineralPro.config.util.MainGridToolCmpt.clearFilterButton
        //     ]
        // }];

        var store = Ext.getStore(me.store);
        store.setRemoteFilter(true);
        store.on({
            beforeload: {
                fn: function(store, operation, eOpts){
                    MineralPro.config.RuntimeUtility.DebugLog('Entering OwnersGridView initComponent attach load event beforeload.')
                    Ext.getBody().mask('Loading Owners...');
                    var extraParams = store.getProxy().extraParams;
                    var filterData = me.getFilterData();//getFilterData is bound on FilterAllRows Plugin
                    // var filterCount = Ext.Object.getValues(filterData).filter(function(val){return val.trim();}).length;
                    var filterCount = Object.values(filterData).filter(function(val){return val.trim();}).length;
                    if(filterCount == 0){
                        Ext.getBody().unmask();
                        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnersGridView initComponent attach load event canceled.')
                        return false;
                    }
                    MineralPro.config.RuntimeUtility.DebugLog('Entering OwnersGridView initComponent attach load event continue to load.')
                    extraParams['searchBy'] = me.down('cycle#searchCategory').getActiveItem().getValue();


                    var headerInfo = Ext.ComponentQuery.query(me.headerInfoAlias)[0];
                    var detailView = me.up(me.mainPanelAlias).down(me.detailAliasName);

                    if(headerInfo && typeof headerInfo.getForm == 'function' && headerInfo.getForm()){
                        headerInfo.getForm().reset(true);
                    }
                    if(detailView && typeof detailView.getForm == 'function' && detailView.getForm()){
                        detailView.getForm().reset(true);
                    }
                }
            },
            load: {
                fn: function(store, records, successful, operation, eOpts){
                    MineralPro.config.RuntimeUtility.DebugLog('Entering OwnersGridView initComponent attach load event.')
                    Ext.getBody().unmask();
                    /*------------------------------ START ------------------------------
                    | Clear out selected Owner on mainPanel
                    */
                    me.getSelectionModel().deselectAll(true);
                    var mainPanel = me.up(me.mainPanelAlias);
                    mainPanel.selectedId =  '0'; //Need default value '0'
                    mainPanel.selectedIdIndex =  'ownerId';
                    mainPanel.selectedName =  '0'; //Need default value '0'
                    mainPanel.selectedNameIndex =  'name';
                    /*
                    | Clear out selected Owner on mainPanel
                    | ------------------------------ END ------------------------------
                    */

                    if(successful){
                        var filterData = me.getFilterData();//getFilterData is bound on FilterAllRows Plugin
                        // var filterCount = Ext.Object.getValues(filterData).filter(function(val){return val.trim();}).length;
                        var filterCount = Object.values(filterData).filter(function(val){return val.trim();}).length;
                        if(filterCount==0){
                            me.store.clearFilter(true);
                        }
                        if(records.length > 0 && me.selModel){
                            if(me.selModel.selectionMode == 'SINGLE'){
                                me.getSelectionModel().select(0);
                                me.fireEvent('selectionchange',me.getSelectionModel(), me.getSelectionModel().getSelection());
                                MineralPro.config.RuntimeUtility.DebugLog('END firing selectionchange');
                            }
                        }
                    }
                    else{
                        var descriptiveErrorMsg = '';
                        if(operation.getError() && operation.getError().statusText){
                            descriptiveErrorMsg += '<p><span style="font-weight:bold;">Error Details: </span><span style="color:red;">'+operation.getError().statusText+'.</span></p>'
                        }
                        Ext.Msg.show({
                            title: 'Oops!',
                            msg: "Something went wrong while retrieving Owner's info."+descriptiveErrorMsg,
                            buttons: Ext.Msg.OK
                        });
                    }
                    MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnersGridView initComponent attach load event.')
                }
            }
        })
        me.callParent(arguments);

    },
    listeners: {
        selectionchange: 'onLoadSelectionChange',
        // afterlayout: 'onAfterLayout',
        // staterestore: 'onAfterStateRestore'
    }
});
