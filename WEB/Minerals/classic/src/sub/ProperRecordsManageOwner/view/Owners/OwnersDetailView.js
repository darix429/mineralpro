
Ext.define('Minerals.sub.ProperRecordsManageOwner.view.Owners.OwnersDetailView', {
    extend: 'MineralPro.config.view.BaseDetailView',
    alias: 'widget.OwnersDetailView',
    
    title: 'Owner Details',
    itemId: 'ownerDetails',
    defaults: {
        width: 275,
        labelWidth: 93
    },   
    items: [{
        xtype:'textfield',
        fieldLabel:'Owner Id',
        name: 'ownerId',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'Name',
        name: 'name',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'Name 2',
        name: 'name2',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'Agent',
        name: 'agencyName',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'In Care Of',
        name: 'inCareOf',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'Address Line 2',
        name: 'addrLine2',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'Address Line 3',
        name: 'addrLine3',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'City',
        name: 'city',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'State',
        name: 'stateName',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'ZIP Code',
        name: 'zipcode',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'Country',
        name: 'countryName',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'Birth Date',
        name: 'birthDt',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'Change Reason',
        name: 'changeReason',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'Confidential',
        name: 'confidential',
        readOnly: true 
    },{
        xtype : 'menuseparator',
        width : '100%',
        padding: false
    },{
        xtype:'textfield',
        fieldLabel:'Phone Number', 
        name: 'phoneNum',
        readOnly: true 
    },{
        xtype:'textfield',
        fieldLabel:'Fax Number',
        name:  'faxNum',
        readOnly: true          
    },{
        xtype:'textfield',
        fieldLabel:'Email Address',
        name:  'emailAddress',
        readOnly: true          
    },{
        xtype: 'container',
        hidden: true,
        itemId: 'totalExemption',
        items: [{
            xtype : 'menuseparator',
            width : '100%',
            padding: false
        },{
            xtype:'textfield',
            fieldLabel:'Total Exemption',
            itemId: 'totalExemptionDesc', 
            name: 'totalExemptionDesc',
            readOnly: true,
        },{
            xtype:'textfield',
            fieldLabel:'Effective Date',
            name:  'totalExemptionEffectiveDt',
            readOnly: true          
        },{
            xtype:'textfield',
            fieldLabel:'Expiration Date',
            readOnly: true,
            itemId: 'totalExemptionExpirationDt',
            name:  'totalExemptionExpirationDt',        
        }]
    },{
        xtype: 'container',
        hidden: true,
        itemId: 'disableVet',
        items: [{
            xtype : 'menuseparator',
            width : '100%',
            padding: false
        },{
            xtype:'textfield',
            fieldLabel:'Disable Vet', 
            name: 'disableVetDesc',
            itemId: 'disableVetDesc',
            readOnly: true, 
        },{
            xtype:'textfield',
            fieldLabel:'Effective Date',
            name:  'disableVetEffectiveDt',
            readOnly: true          
        },{
            xtype:'textfield',
            fieldLabel:'Expiration Date',
            itemId: 'disableVetExpirationDt',
            name:  'disableVetExpirationDt',
            readOnly: true,        
        },{
            xtype:'textfield',
            fieldLabel:'Amount',
            itemId:  'amount',
            name:  'amount',
            readOnly: true,        
        }]
    },{
        xtype: 'container',
        itemId: 'noExemption',
        items: [{
            xtype : 'menuseparator',
            width : '100%',
            padding: false
        },{
            xtype: 'container',
            itemId: 'noExemption',
            style:'padding:10px 0px 10px 0px',
            items: [{
                xtype:'label',
                text: 'No Exemption',       
            }]       
        }]
    }]
});