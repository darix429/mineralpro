Ext.define('Minerals.sub.ProperRecordsManageOwner.view.OwnerView' ,{
    extend: 'MineralPro.config.view.BaseTabPanelView',
    alias : 'widget.ProperRecordsManageOwner',
    layout:'border',
    
    
    selectedId: '0',//Need default value '0'
    selectedIdIndex: 'ownerId',
    selectedName: '0',//Need default value '0'
    selectedNameIndex: 'name',

    id: 'ProperRecordsManageOwnerId',
    listeners: {
        activate: function(tab){ 
            var grid = tab.down('OwnersGridView');
            var store = grid.getStore();
            
            if( (!store.isLoading()) && (!store || !store.getData().getCount()) ){
                MineralPro.config.Runtime.headerInfoXtype=''
                Ext.globalEvents.fireEvent('updateHeader');
            }
            else if(store.getData().getCount()){
                var selectionModel = grid.getSelectionModel();
                var selected = selectionModel ? selectionModel.getSelection():[];
                if(selected.length>0){
                    grid.fireEvent('selectionchange', selectionModel, selected);
                }
                else{
                    selectionModel.select(0, false, false);
                }
            }
        },
        beforetabchange: function(tabPanel, newCard, oldCard, eOpts){
            MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerView beforetabchange.')
            var me = this;
            var origFn = me.getController().onBeforeTabChange(tabPanel, newCard, oldCard, eOpts);

            var ownersView = me.down('ProperRecordsManageOwners');
            var selected = ownersView.down('grid').getSelectionModel().getSelection();
            var ownersStore = ownersView.down('grid').getStore();
            if(selected.length == 0){
                Ext.Msg.show({
                    title: MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                    msg: "You have not selected an owner, would you like to create one instead?",
                    iconCls: MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                    buttons: Ext.Msg.YESNO,
                    modal: true,
                    scope: this,
                    width: 250,
                    fn: function (btn) {
                        if (btn === 'yes'){
                            ownersView.down('grid #addNewDataButtonItemId').fireEvent('click');
                        }
                    }
                });
                return false;
            }
            MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerView beforetabchange.')
        }
    },
    items: [{
            title: 'Owners',
            xtype: 'ProperRecordsManageOwners'
        },{
            title: 'Lease Holdings',
            xtype: 'ProperRecordsManageOwnerLeaseHoldings'
        },{    
            title: 'Sum by Tax Units',
            xtype: 'ProperRecordsManageOwnerTaxUnit'
        },{
            title: 'Owner Notes',
            xtype: 'ProperRecordsManageOwnerNote'
        },
        // {           
        //     title: 'Owner Exemptions',
        //     xtype: 'ProperRecordsManageOwnerExemptions'
        // },
         ] 

});    