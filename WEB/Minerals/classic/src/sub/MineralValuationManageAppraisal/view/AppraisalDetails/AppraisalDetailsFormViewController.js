//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.config.view.Appraisal.AppraisalDetailsFormViewController', {
    extend: 'Minerals.config.view.Appraisal.BaseAppraisalFormViewController',
    alias: 'controller.AppraisalDetailsFormViewController',
    counter: 0,
    storeFailureCallback: function(operation){
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewController storeFailureCallback.')
        var response = operation && typeof operation.getError == 'function' ? operation.getError() : {};
        var msg = 'Request Status: '+response.status+'<br/>Request Status Text: '+response.statusText+'<br/>Response Text: '+response.responseText;
        Ext.Msg.show({
            iconCls: Minerals.config.Runtime.appraisalCalcAlertIcon,
            title: 'Error!',
            closeAction: 'hide',
            msg: 'Something went wrong while processing your request.<br/><span style="font-weight:bold;line-height:25px;">Error Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p><p style="font-weight:bold;font-style:italic;color:red;">*** If error persist, please check you\'re server.</p>',
            buttons: Ext.Msg.OK,
        });
        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController storeFailureCallback.')
    },
    onNewAppraisalButton: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewController onNewAppraisalButton.')
        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var appraisalListView = Ext.ComponentQuery.query(view.appraisalListViewAlias)[0];
        Ext.getBody().mask('Loading...')
        view.newAppraisal = true;

        view.down('#subjectIdItemId').enable(true);
        view.down('#subjectTypeItemId').enable(true);
        view.down('#nameItemId').enable(true);

        view.down('#subjectTypeItemId').focus();
        view.down('#calcButtonItemId').disable(true);
        me.resetFormValues(true)

        // console.log('------------------------------- START -------------------------------');
        // console.log(appraisalListView);
        // console.log('-------------------------------- END --------------------------------');
        
        var header = Ext.ComponentQuery.query(appraisalListView.headerInfoAlias)[0];
        if(appraisalListView && header){
            header.reset(true);
        }
            

        // var defaultField = view.down('#fieldItemId').getStore().getAt(0)
        // view.down('#fieldItemId').select(defaultField)
        // view.down('#fieldItemId').fireEvent('select', view.down('#fieldItemId'), defaultField)

        var declineGridArr = view.down('#declineTypeItemId').query('grid');

        Ext.each(declineGridArr, function(grid){
            var store = grid.getStore();
                store.getProxy().extraParams = {
                    selectedId: '0',
                    rrcNumber: '0',
                    subjectTypeCd: '0'
                }
                store.load({
                    callback: function(record, operation, success){
                        if(success){

                        }
                        else{
                            me.storeFailureCallback(operation);
                        }
                    }
                });
        })
        Ext.getBody().unmask()
        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController onNewAppraisalButton.')
    },
    onNextButton: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewController onNextButton.')
        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var appraisalDetailStore = view.down('grid').getStore();
        var appraisalGrid = mainPanel.down(view.appraisalListViewAlias);
        var appraisalStore = appraisalGrid.getStore();
//        view.newAppraisal = false;
        view.down('#subjectIdItemId').disable(true);
        view.down('#subjectTypeItemId').disable(true);
        view.down('#nameItemId').disable(true);

        Ext.getBody().mask('Loading...');

//        var saveData = false
        var saveData = true

        var record = view.getRecord();
        var values = view.getValues();
            record.set(values);

        if(view.down('#saveToDbButtonItemId').isDisabled() || view.down('#saveToDbButtonItemId').isHidden()){
            saveData = false;
        }


//        if(appraisalDetailStore.getNewRecords().length > 0){
//            var newRec = appraisalDetailStore.getNewRecords()[0]
//                var exclude = 'rrcNumber,fieldId,'
//                    +'wellDescription,wellTypeCd,maxYearLife,comment,productionEquipmentCount,'
//                    +'productionWellSchedule,productionEquipmentValue,serviceEquipmentCount,'
//                    +'serviceWellSchedule,serviceEquipmentValue,injectionEquipmentCount,'
//                    +'injectionWellSchedule,injectionEquipmentValue,'
//                    +'discountRate,grossOilPrice,grossProductPrice,'
//                    +'grossWorkingInterestGasPrice,grossRoyaltyInterestGasPrice,operatingExpense,'
//                    +'dailyAverageOil,dailyAverageGas,dailyAverageProduct'
//                var fieldArray = exclude.split(',')
//                Ext.each(fieldArray, function(field){
//                    if(newRec.get(field) != 0 && newRec.get(field) != ''
//                            && newRec.get(field) != '0'){
//                        saveData = true
//                        return false;
//                    }
//                })
//        }else if(appraisalDetailStore.getUpdatedRecords().length > 0){
//            saveData = true
//        }

        if (saveData){
            Ext.Msg.show({
            title:MineralPro.config.Runtime.tabChangeConfirmationTitle,
                    msg:MineralPro.config.Runtime.tabChangeConfirmationMsg,
                    iconCls:MineralPro.config.Runtime.tabChangeConfirmationIcon,
                    buttons: Ext.Msg.YESNO,
                    scope: this,
                    width: 250,
                    fn: function(btn) {
                        if (btn === 'yes'){
                            Ext.getBody().mask('Loading...');
                            var params = appraisalDetailStore.getProxy().extraParams
                                  params['createLength'] = appraisalDetailStore.getNewRecords().length
                                  params['updateLength'] = appraisalDetailStore.getUpdatedRecords().length
                                  appraisalDetailStore.getAt(0).set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
                                  params['origRrcNumber'] = appraisalDetailStore.getAt(0).get('origRrcNumber');
                                  params['newRecord'] = view.newAppraisal
                                  params['newCalculation'] = view.newCalculation
                                  params['updateCalc'] = false;

                                appraisalDetailStore.getProxy().extraParams = params

                            appraisalDetailStore.sync({
                                success: function(){
                                    loadNext();
                                },
                                failure: function(batch, options){
                                    Ext.getBody().unmask();
                                    appraisalDetailStore.rejectChanges();
                                    var msg = batch.getExceptions().map(function(operation){
                                        return 'Status Text: '+operation.getError().statusText;
                                    });
                                    msg = msg.join('<br/> ');

                                    Ext.Msg.show({
                                        title: 'Oops!',
                                        msg: 'Failed to save your data <br/><span style="font-weight:bold;line-height:25px;">Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p>',
                                        icon: Ext.Msg.ERROR,
                                        buttons: Ext.Msg.OK
                                    });
                                },
                                callback: function(){
                                    
                                }
                            });
                        } else{
                            loadNext();
                        }
                    }
               });
           }else{
               loadNext();
           }


        function loadNext(){
            appraisalGrid.withChildTab = true;
            var appraisalGridSelection = appraisalGrid.getSelectionModel();
            var appraisalStoreCount = appraisalStore.getCount();
            var selected = appraisalGridSelection.getSelection();
            var currentIndex = 0;
            if(selected.length){
                currentIndex = appraisalStore.indexOf(selected[0]);
            }
            else if(mainPanel.selectedId){
                currentIndex = appraisalStore.findBy(function(rec,id){
                                    return rec.get(mainPanel.selectedIdIndex) == mainPanel.selectedId && rec.get(mainPanel.selectedRRCNumberIndex) == mainPanel.selectedRRCNumber;
                                });
            }
            // appraisalStore.each(function (rec) {
            //     if (rec.get(mainPanel.selectedIdIndex) == mainPanel.selectedId) {
            //         if (rec.get(mainPanel.selectedRRCNumberIndex) == mainPanel.selectedRRCNumber) {
            //             return false;
            //         }
            //     }
            //     currentIndex++;
            // })
            // if (appraisalStoreCount > currentIndex + 1) {
            //     currentIndex = currentIndex + 1;
            // } else {
            //     currentIndex = 0;
            // }
            var nextIndex = currentIndex+1 ;
            if(nextIndex >= appraisalStoreCount){
                // reset to first record
                nextIndex = 0;
            }
            
            var newselected = appraisalGridSelection.getSelection();
            newselected = newselected.data || newselected[0] ? newselected[0].data : newselected;
            mainPanel.selectedId = newselected.subjectId || mainPanel.selectedId;
            mainPanel.selectedRRCNumber = newselected.rrcNumber || mainPanel.selectedRRCNumber;
                
            appraisalGridSelection.select(nextIndex, false, false)
            view.down('#appraisalIndexItemId').setText((nextIndex+1)+ ' of ' + appraisalStoreCount);

            appraisalDetailStore.getProxy().extraParams = {
                selectedId: mainPanel.selectedId,
                selectedRRCNumber: mainPanel.selectedRRCNumber,
                idAppUser: MineralPro.config.Runtime.idAppUser,
                subjectTypeCd: appraisalStore.getAt(currentIndex).get('subjectTypeCd')
            };

            var params = view.down('#subjectIdItemId').getStore().getProxy().extraParams

            if (params.subjectType) {
                view.down('#subjectIdItemId').getStore().getProxy().extraParams = {}
                view.down('#nameItemId').getStore().getProxy().extraParams = {}

                view.down('#subjectIdItemId').getStore().reload({
                    callback: function (record, operation, success) {
                        if(success){
                            view.down('#nameItemId').getStore().reload({
                                callback: function (record, operation, success) {
                                    if(success){
                                        appraisalDetailStore.load({
                                            callback: function (record, operation, success) {
                                                if(success){
                                                    view.down('#calcButtonItemId').enable(true);
                                                }
                                                else{
                                                    me.storeFailureCallback(operation);
                                                }
                                            }
                                        })
                                    }
                                    else{
                                        me.storeFailureCallback(operation);
                                    }
                                }
                            })
                        }
                        else{
                            me.storeFailureCallback(operation);
                        }
                    }
                })

            } else {
                appraisalDetailStore.load({
                    callback: function (record, operation, success) {
                        if(success){
                            view.down('#calcButtonItemId').enable(true);
                        }
                        else{
                            me.storeFailureCallback(operation);
                        }
                    }
                })
            }
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController onNextButton.')
    },
    onPreviousButton: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewController onPreviousButton.')
        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var appraisalDetailStore = view.down('grid').getStore();
        var appraisalGrid = mainPanel.down(view.appraisalListViewAlias);
        var appraisalStore = appraisalGrid.getStore();
//        view.newAppraisal = false;
        view.down('#subjectIdItemId').disable(true);
        view.down('#subjectTypeItemId').disable(true);
        view.down('#nameItemId').disable(true);

        Ext.getBody().mask('Loading...');

//        var saveData = false
        var saveData = true
        var record = view.getRecord();
        var values = view.getValues();
            record.set(values);

        if(view.down('#saveToDbButtonItemId').isDisabled() || view.down('#saveToDbButtonItemId').isHidden()){
            saveData = false;
        }

//        if(appraisalDetailStore.getNewRecords().length > 0){
//            var newRec = appraisalDetailStore.getNewRecords()[0]
//                var exclude = 'rrcNumber,fieldId,'
//                    +'wellDescription,wellTypeCd,maxYearLife,comment,productionEquipmentCount,'
//                    +'productionWellSchedule,productionEquipmentValue,serviceEquipmentCount,'
//                    +'serviceWellSchedule,serviceEquipmentValue,injectionEquipmentCount,'
//                    +'injectionWellSchedule,injectionEquipmentValue,'
//                    +'discountRate,grossOilPrice,grossProductPrice,'
//                    +'grossWorkingInterestGasPrice,grossRoyaltyInterestGasPrice,operatingExpense,'
//                    +'dailyAverageOil,dailyAverageGas,dailyAverageProduct'
//                var fieldArray = exclude.split(',')
//                Ext.each(fieldArray, function(field){
//                    if(newRec.get(field) != 0 && newRec.get(field) != ''
//                            && newRec.get(field) != '0'){
//                        saveData = true
//                        return false;
//                    }
//                })
//        }else if(appraisalDetailStore.getUpdatedRecords().length > 0){
//            saveData = true
//        }

        if (saveData){
            Ext.Msg.show({
            title:MineralPro.config.Runtime.tabChangeConfirmationTitle,
                    msg:MineralPro.config.Runtime.tabChangeConfirmationMsg,
                    iconCls:MineralPro.config.Runtime.tabChangeConfirmationIcon,
                    buttons: Ext.Msg.YESNO,
                    scope: this,
                    width: 250,
                    fn: function(btn) {
                        if (btn === 'yes'){
                            Ext.getBody().mask('Loading...');
                            var params = appraisalDetailStore.getProxy().extraParams
                                  params['createLength'] = appraisalDetailStore.getNewRecords().length
                                  params['updateLength'] = appraisalDetailStore.getUpdatedRecords().length
                                  appraisalDetailStore.getAt(0).set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
                                  params['origRrcNumber'] = appraisalDetailStore.getAt(0).get('origRrcNumber');
                                  params['newRecord'] = view.newAppraisal
                                  params['newCalculation'] = view.newCalculation
                                  params['updateCalc'] = false;

                                appraisalDetailStore.getProxy().extraParams = params

                            appraisalDetailStore.sync({
                                success: function(){
                                    loadPrevious();
                                },
                                failure: function(batch, options){
                                    Ext.getBody().unmask();
                                    appraisalDetailStore.rejectChanges();
                                    var recs = batch.getOperations().map(function(op){
                                        return op.getRecords();
                                    });
                                    if(recs.length && typeof recs[0].getData == 'function'){
                                        appraisalDetailStore.getAt(0).set(recs[0].getData());
                                    }
                                    var msg = batch.getExceptions().map(function(operation){
                                        return 'Status Text: '+operation.getError().statusText;
                                    });
                                    msg = msg.join('<br/> ');

                                    Ext.Msg.show({
                                        title: 'Oops!',
                                        msg: 'Failed to save your data <br/><span style="font-weight:bold;line-height:25px;">Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p>',
                                        icon: Ext.Msg.ERROR,
                                        buttons: Ext.Msg.OK
                                    });
                                },
                                callback: function(){
                                    
                                }
                            });
                        } else{
                            loadPrevious();
                        }
                    }
               });
           }else{
               loadPrevious();
           }

        function loadPrevious() {
            var appraisalGridSelection = appraisalGrid.getSelectionModel();
            var appraisalStoreCount = appraisalStore.getCount();
            var selected = appraisalGridSelection.getSelection();

            var currentIndex = 0;
            if(selected.length){
                currentIndex = appraisalGrid.store.indexOf(selected[0]);
            }
            else if(mainpanel.selectedId){
                currentIndex = appraisalStore.findBy(function(rec,id){
                                    return rec.get(mainPanel.selectedIdIndex) == mainPanel.selectedId && rec.get(mainPanel.selectedRRCNumberIndex) == mainPanel.selectedRRCNumber;
                                });
            }

            // appraisalStore.each(function (rec) {
            //     if (rec.get(mainPanel.selectedIdIndex) == mainPanel.selectedId) {
            //         if (rec.get(mainPanel.selectedRRCNumberIndex) == mainPanel.selectedRRCNumber) {
            //             return false;
            //         }
            //     }
            //     currentIndex++;
            // })

            // if (currentIndex - 1 >= 0) {
            //     currentIndex = currentIndex - 1;
            // } else {
            //     currentIndex = appraisalStoreCount - 1;
            // }
            var prevIndex = currentIndex - 1;
            prevIndex = prevIndex < 0 ? appraisalStoreCount-1 : prevIndex; //if less than 1, set it to last row
            appraisalGridSelection.select(prevIndex, false, false)
                
            view.down('#appraisalIndexItemId').setText((prevIndex+1) + ' of ' + appraisalStoreCount);
          
            appraisalDetailStore.getProxy().extraParams = {
                selectedId: mainPanel.selectedId,
                selectedRRCNumber: mainPanel.selectedRRCNumber,
                idAppUser: MineralPro.config.Runtime.idAppUser,
                subjectTypeCd: appraisalStore.getAt(currentIndex).get('subjectTypeCd')
            };

            var params = view.down('#subjectIdItemId').getStore().getProxy().extraParams

            if (params.subjectType) {
                view.down('#subjectIdItemId').getStore().getProxy().extraParams = {}
                view.down('#nameItemId').getStore().getProxy().extraParams = {}

                view.down('#subjectIdItemId').getStore().reload({
                    callback: function (record, operation, success) {
                        if(success){
                            view.down('#nameItemId').getStore().reload({
                                callback: function (record, operation, success) {
                                    if(success){
                                        appraisalDetailStore.load({
                                            callback: function (record, operation, success) {
                                                if(success){
                                                    view.down('#calcButtonItemId').enable(true);
                                                }
                                                else{
                                                    me.storeFailureCallback(operation)
                                                }
                                            }
                                        })
                                    }
                                    else{
                                        me.storeFailureCallback(operation)
                                    }
                                }
                            })
                        }
                        else{
                            me.storeFailureCallback(operation);
                        }
                    }
                })

            } else {
                appraisalDetailStore.load({
                    callback: function (record, operation, success) {
                        if(success){
                            view.down('#calcButtonItemId').enable(true);
                        }
                        else{
                            me.storeFailureCallback(operation)
                        }
                    }
                })
            }
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController onPreviousButton.')
    },
    isFormValid: function(){
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewControllerisFormValid.')
        var me = this;
        var view = me.getView();
        var response = {};
        var mainPanel = view.up(view.mainPanel);
        if(view.isValid()){
            response.valid = true;
        }
        else{
            response.valid = false;
            var errorsTpl = new Ext.XTemplate(
                '<tpl if="">',
                '<ul class="field-errors-list"><tpl for="."><li><span class="error-field">{field}</span> - <span class="field-error">{error}</span></li></tpl></ul>',
                '</tpl>'
            );
            var errors = [];
            var form = view.getForm();
            var fields = form.getFields();
            fields = fields.items || [];
            Ext.each(fields,function (field) {
                var fieldName = field.getName();
                fieldName = fieldName.split(/(?=[A-Z])/).map(function(word){return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();}).join(' ');
                errors = errors.concat(
                    Ext.Array.map(field.getErrors(), function (error) {
                        return { field: fieldName, error: error }
                    })
                );
            });
            response.errMsg = errorsTpl.apply(errors);
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController isFormValid.')
        return response;
    },
    onSyncListGridData: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewController onSyncListGridData.')
        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var appraisalDetailStore = view.down('grid').getStore();
        var model = appraisalDetailStore.getModel();
        var isNewAppraisal = view.newAppraisal;
        var record = view.getRecord();
        var values = view.getValues();
        var recordCopy = record ? record.copy(null): model.create(values); // used to compare, instead of updating store values.
        if(!record){
            var newRecord = model.create(values);
            newRecord.phantom = true;
            appraisalDetailStore.add(newRecord);
            view.loadRecord(newRecord);

            // recordCopy.loadRecord(newRecord);
        }
        else{
            recordCopy.set(values);
        }
        // console.log('------------------------------- START -------------------------------');
        // console.log('Original record');
        // console.log(record);
        // console.log('New record');
        // console.log(recordCopy);
        var formFields = Ext.Object.getKeys(values);
        var equal = Ext.Array.every(formFields, function(field){
            return record && record.get(field) == recordCopy.get(field);
        })
        // console.log('fields');
        // console.log(formFields);
        // console.log('equal '+equal);
        // console.log('-------------------------------- END --------------------------------');

        var isFormValid = me.isFormValid()
        // var dirty = appraisalDetailStore.getNewRecords().length > 0
        //         || appraisalDetailStore.getUpdatedRecords().length > 0
        //         || appraisalDetailStore.getRemovedRecords().length > 0;

       // Ext.each(declineGridArr, function(grid){
       //     var store = grid.getStore();
       //     dirtyDecline = store.getNewRecords().length > 1
       //         || store.getUpdatedRecords().length > 0
       //         || store.getRemovedRecords().length > 0;
       //     if(dirtyDecline){
       //         return false;
       //     }
       // })

        if (!equal && isFormValid.valid) {
            var origRrcNumber = appraisalDetailStore.getAt(0).get('origRrcNumber');
            if(view.newAppraisal == true){
                origRrcNumber = view.down('#rrcNumberItemId').getValue();
                if(record){
                    record.set('origRrcNumber', origRrcNumber)//Must be equal
                    record.set('rrcNumber', origRrcNumber)
                }
                mainPanel.origRrcNumber = origRrcNumber
                mainPanel.selectedRRCNumber = origRrcNumber
            }
            Ext.Msg.show({
                title: MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                msg: MineralPro.config.Runtime.syncGridMsgConfimationMsg,
                iconCls: MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                buttons: Ext.Msg.YESNO,
                scope: this,
                width: 250,
                fn: function (btn) {
                    if (btn === 'yes') {
                        Ext.getBody().mask('Loading...')
                        var subjectTypeCd = appraisalDetailStore.getAt(0).get('subjectTypeCd')
                        if(appraisalDetailStore.getCount()){
                            appraisalDetailStore.getAt(0).set(values);
                        }
                        var dirty = appraisalDetailStore.getNewRecords().length > 0 || appraisalDetailStore.getUpdatedRecords().length > 0 || appraisalDetailStore.getRemovedRecords().length > 0;
                        if(dirty){
                            appraisalDetailStore.getAt(0).set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
                            appraisalDetailStore.getProxy().extraParams = {
                                origRrcNumber: origRrcNumber,
                                subjectTypeCd: subjectTypeCd,
                                updateLength: appraisalDetailStore.getUpdatedRecords().length,
                                newRecord: isNewAppraisal,
                                newCalculation: view.newCalculation,
                                updateCalc: false
                            };
                            appraisalDetailStore.sync({
                                success: function(batch, options){
                                    Ext.getBody().unmask();
                                    var appraisalGrid = mainPanel.down(view.appraisalListViewAlias);
                                    var appraisalStore = appraisalGrid.getStore();
                                    var data = batch.operations[0].config.records[0];
                                    var newRRC = data.get('rrcNumber');
                                    var fiedlGridSelection = appraisalGrid.getSelectionModel();
                                    var appraisalStoreCount = appraisalStore.getCount();
                                    
                                    var filterData = appraisalGrid.getFilterData();//getFilterData is bound on FilterAllRows Plugin
                                    var filterCount = Ext.Object.getValues(filterData).filter(function(val){return val.trim();}).length;
                                    var records = batch.getOperations().filter(function(op){
                                        return op.wasSuccessful();
                                    }).map(function(op){
                                        return op.getRecords();
                                    }).reduce(function(prev, cur){
                                        return prev.concat(cur);
                                    });
                                    var calcAppraisalStore = Ext.StoreMgr.lookup(view.calcAppraisalStore);
                                        
                                    Ext.Ajax.request({
                                        url: calcAppraisalStore.getProxy().getApi().read,
                                        method: 'GET',
                                        params: {
                                            rowUpdateUserid: MineralPro.config.Runtime.idAppUser,
                                            idAppUser: MineralPro.config.Runtime.idAppUser,
                                            appraisalYear: MineralPro.config.Runtime.appraisalYear,
                                            selectedId: records[0].get('subjectId'),
                                            selectedRRCNumber: records[0].get('rrcNumber'),
                                            subjectTypeCd: records[0].get('subjectTypeCd'),
                                            newAppraisal: view.newAppraisal,
                                            maxYear: records[0].get('maxYearLife')
                                        },
                                        // url: appraisalStore.getProxy().getApi().read,
                                        // method: 'GET',
                                        // params: {
                                        //     idAppUser: MineralPro.config.Runtime.idAppUser,
                                        //     appraisalYear: MineralPro.config.Runtime.appraisalYear,
                                        //     selectedId: records[0].get('subjectId'),
                                        //     selectedRRCNumber: records[0].get('rrcNumber'),
                                        //     subjectTypeCd: records[0].get('subjectTypeCd')
                                        // },
                                        success: function(response){
                                            var resp = Ext.decode(response.responseText);
                                            var addedRecords = [];
                                            
                                            var data  = resp.success ? resp.data : [];
                                            var isRecExists = appraisalStore.findBy(function(rec){
                                                // return rec.get('subjectId') == records[0].get('subjectId') && rec.get('rrcNumber') == records[0].get('rrcNumber');
                                                return rec.get('subjectId') == mainPanel.selectedId && rec.get('rrcNumber') == mainPanel.selectedRRCNumber;
                                            });
                                            // console.log('------------------------------- START -------------------------------');
                                            // console.log('isRecExists '+isRecExists);
                                            // console.log('appraisalStore')
                                            // console.log(appraisalStore);
                                            // console.log('records[0]')
                                            // console.log(records[0]);
                                            // console.log('subjectId: '+records[0].get('subjectId')+' rrcNumber: '+records[0].get('rrcNumber'));
                                            // console.log('-------------------------------- END --------------------------------');
                                            var currentIndex = 0;
                                            if(isRecExists >= 0){
                                                currentIndex = isRecExists;
                                                appraisalStore.getAt(currentIndex).set(data[0]);
                                                addedRecords = [appraisalStore.getAt(currentIndex)];
                                            }
                                            else{
                                                // addedRecords = appraisalStore.add(data);
                                                // currentIndex = appraisalStore.findBy(function(rec){
                                                //     return rec.get('subjectId') == records[0].get('subjectId') && rec.get('rrcNumber') == records[0].get('rrcNumber');
                                                // });
                                                addedRecords = appraisalStore.insert(currentIndex, data);
                                            }
                                            addedRecords.forEach(function(rec,i){
                                                rec.dirty = false;
                                                rec.phantom = false;
                                            });

                                            appraisalStoreCount = appraisalStore.getCount(); //update appraisalStoreCount
                                            currentIndex = currentIndex >= 0 && currentIndex < appraisalStoreCount ? currentIndex : 0;
                                            view.down('#appraisalIndexItemId').setText((currentIndex+1)+ ' of ' + appraisalStoreCount);

                                            fiedlGridSelection.deselectAll(true);
                                            fiedlGridSelection.select(currentIndex);
                                            
                                            var newlyCreatedAppraisal = fiedlGridSelection.getSelection()[0];
                                            var textArray = view.query('textfield');
                                            var numberArray = view.query('numberfield');
                                            var comboArray = view.query('combobox');

                                            var fields = [].concat(textArray, numberArray, comboArray);
                                            
                                            fields.forEach(function(field){
                                                field.suspendEvents();
                                            });
                                            
                                            view.loadRecord(newlyCreatedAppraisal);
                                            
                                            fields.forEach(function(field){
                                                field.resumeEvents();
                                                field.setFieldStyle('background-color: white; color: #000');
                                            });
                                            
                                            
                                            appraisalGrid.getView().refresh();
                                            view.newAppraisal = false;
                                            
                                            var declineStores = [
                                                'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalOperatingExpStore',
                                                'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalOilDailyAvgStore',
                                                'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalGasDailyAvgStore',
                                                'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalProdDailyAvgStore'
                                            ];
                                            Ext.each(declineStores,function(name){
                                                var store = Ext.getStore(name);
                                                store.getProxy().extraParams.selectedId = records[0].get('subjectId');
                                                store.getProxy().extraParams.rrcNumber = newRRC;
                                                store.reload({
                                                    callback: function(record, operation, success){
                                                        if(success){

                                                        }
                                                        else{
                                                            me.storeFailureCallback(operation);
                                                        }
                                                    }
                                                });
                                            });
                                            view.down('#saveToDbButtonItemId').disable(true);
                                        },
                                        failure: function(response){
                                            var msg = 'Status Text: '+response.statusText;
                                            Ext.Msg.show({
                                                iconCls: Minerals.config.Runtime.appraisalCalcAlertIcon,
                                                title: 'Error!',
                                                closeAction: 'hide',
                                                msg: 'Something went wrong while processing your request.<br/><span style="font-weight:bold;line-height:25px;">Error Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p><p style="font-weight:bold;font-style:italic;color:red;">*** If error persist, please check you\'re server.</p>',
                                                buttons: Ext.Msg.OK,
                                            });
                                            MineralPro.config.RuntimeUtility.DebugLog('Failure AppraisalDetailsFormViewController onSyncListGridData AjaxRequest.')
                                        }
                                    });
                                    // if(filterCount == 0){

                                    //     var calcAppraisalStore = Ext.StoreMgr.lookup(view.calcAppraisalStore);
                                        
                                    //     Ext.Ajax.request({
                                    //         url: calcAppraisalStore.getProxy().getApi().read,
                                    //         method: 'GET',
                                    //         params: {
                                    //             rowUpdateUserid: MineralPro.config.Runtime.idAppUser,
                                    //             appraisalYear: MineralPro.config.Runtime.appraisalYear,
                                    //             selectedId: records[0].get('subjectId'),
                                    //             selectedRRCNumber: records[0].get('rrcNumber'),
                                    //             subjectTypeCd: records[0].get('subjectTypeCd'),
                                    //             newAppraisal: view.newAppraisal,
                                    //             maxYear: records[0].get('maxYearLife')
                                    //         },
                                    //         // url: appraisalStore.getProxy().getApi().read,
                                    //         // method: 'GET',
                                    //         // params: {
                                    //         //     idAppUser: MineralPro.config.Runtime.idAppUser,
                                    //         //     appraisalYear: MineralPro.config.Runtime.appraisalYear,
                                    //         //     selectedId: records[0].get('subjectId'),
                                    //         //     selectedRRCNumber: records[0].get('rrcNumber'),
                                    //         //     subjectTypeCd: records[0].get('subjectTypeCd')
                                    //         // },
                                    //         success: function(response){
                                    //             var resp = Ext.decode(response.responseText);
                                    //             var addedRecords = [];
                                    //             var currentIndex;
                                    //             var data  = resp.success ? resp.data : [];
                                    //             var isRecExists = appraisalStore.findBy(function(rec){
                                    //                 return rec.get('subjectId') == records[0].get('subjectId') && rec.get('rrcNumber') == records[0].get('rrcNumber');
                                    //             });
                                    //             if(isRecExists >= 0){
                                    //                 appraisalStore.getAt(isRecExists).set(data[0]);
                                    //             }
                                    //             else{
                                    //                 addedRecords = appraisalStore.add(data);
                                    //                 currentIndex = appraisalStore.findBy(function(rec){
                                    //                     return rec.get('subjectId') == records[0].get('subjectId') && rec.get('rrcNumber') == records[0].get('rrcNumber');
                                    //                 });

                                    //             }
                                    //             appraisalStoreCount = appraisalStore.getCount(); //update appraisalStoreCount
                                    //             currentIndex = currentIndex >= 0 && currentIndex <appraisalStoreCount ? currentIndex : appraisalStoreCount-1;
                                    //             view.down('#appraisalIndexItemId').setText((currentIndex+1)+ ' of ' + appraisalStoreCount);

                                    //             fiedlGridSelection.deselectAll(true);
                                    //             fiedlGridSelection.select(currentIndex);
                                                
                                    //             var newlyCreatedAppraisal = fiedlGridSelection.getSelection()[0];
                                    //             var textArray = view.query('textfield');
                                    //             var numberArray = view.query('numberfield');
                                    //             var comboArray = view.query('combobox');

                                    //             var fields = [].concat(textArray, numberArray, comboArray);
                                                
                                    //             fields.forEach(function(field){
                                    //                 field.suspendEvents();
                                    //             });

                                    //             view.loadRecord(newlyCreatedAppraisal);
                                                
                                    //             fields.forEach(function(field){
                                    //                 field.resumeEvents();
                                    //                 field.setFieldStyle('background-color: white; color: #000');
                                    //             });
                                    //             addedRecords.forEach(function(rec,i){
                                    //                 rec.dirty = false;
                                    //             });
                                                
                                    //             appraisalGrid.getView().refresh();
                                    //             view.newAppraisal = false;
                                    //             view.down('#saveToDbButtonItemId').disable(true);
                                    //         },
                                    //         failure: function(response){
                                    //             MineralPro.config.RuntimeUtility.DebugLog('Failure AppraisalDetailsFormViewController onSyncListGridData AjaxRequest.')
                                    //         }
                                    //     });

                                    // }
                                    // else{
                                    //     appraisalStore.load({
                                    //         callback: function (items, operation, success) {
                                                
                                    //             var currentIndex = appraisalStore.findBy(function(rec){
                                    //                 return rec.get(mainPanel.selectedIdIndex) == mainPanel.selectedId && rec.get(mainPanel.selectedRRCNumberIndex) == mainPanel.selectedRRCNumber;
                                    //             });
                                    //             if(currentIndex == -1 && view.newAppraisal){
                                    //                 appraisalStore.insert(0, records);
                                    //                 appraisalStore.getAt(0).dirty = false;
                                    //                 appraisalGrid.getView().refresh();
                                                    
                                    //                 currentIndex = 0;
                                    //                 fiedlGridSelection.select(currentIndex)
                                    //             }
                                    //             else{
                                    //                 currentIndex = currentIndex >= 0 && currentIndex <appraisalStoreCount ? currentIndex : appraisalStoreCount-1;
                                    //                 fiedlGridSelection.select(currentIndex)
                                    //                 me.resetFormValues(false)
                                    //             }
                                    //             view.down('#appraisalIndexItemId').setText((currentIndex+1) + ' of ' + appraisalStoreCount);
                                    //             view.down('#calcButtonItemId').enable(true);

                                    //             if(view.newAppraisal == true){
                                    //                 view.down('#calcButtonItemId').click();
                                    //             } else {
                                    //                 appraisalDetailStore.reload({
                                    //                     callback: function () {
                                    //                         if (appraisalDetailStore.getAt(0)) {
                                    //                             view.down('grid').getSelectionModel().select(currentIndex, false, false);
                                    //                         }
                                    //                     }
                                    //                 });
                                    //             }
                                    //        }
                                    //     });
                                    // }
                                    appraisalDetailStore.getProxy().extraParams = {
                                        selectedId: mainPanel.selectedId,
                                        selectedRRCNumber: newRRC,
                                        subjectTypeCd: appraisalDetailStore.getAt(0).get('subjectTypeCd')
                                    };
                                    appraisalDetailStore.getAt(0).set({'origRrcNumber': newRRC});
                                    view.down('#saveToDbButtonItemId').disable(true);
                                },
                                failure: function(batch, options){
                                    Ext.getBody().unmask();
                                    appraisalDetailStore.rejectChanges();
                                    appraisalDetailStore.getAt(0).set(values);
                                    appraisalDetailStore.getAt(0).dirty = true;
                                    var msg = batch.getExceptions().map(function(operation){
                                        return 'Status Text: '+operation.getError().statusText;
                                    });
                                    msg = msg.join('<br/> ');
                                    
                                    Ext.Msg.show({
                                        title: 'Oops!',
                                        msg: 'Failed to save your data <br/><span style="font-weight:bold;line-height:25px;">Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p>',
                                        icon: Ext.Msg.ERROR,
                                        buttons: Ext.Msg.OK
                                    });
                                }
                            });
                            
                            
                            view.down('#subjectIdItemId').disable(true);
                            view.down('#subjectTypeItemId').disable(true);
                            view.down('#nameItemId').disable(true);
                        }
                        else{
                            Ext.getBody().unmask();
                            Ext.Msg.show({
                                title: 'Oops!',
                                msg: 'Form has not been modified',
                                icon: Ext.Msg.ERROR,
                                buttons: Ext.Msg.OK
                            });
                        }
                    }
                }
            });
        }
        else if(!isFormValid.valid){
            Ext.Msg.show({
                title: 'Please check your input data',
                icon: Ext.Msg.ERROR,
                buttons: Ext.Msg.OK,
                msg: isFormValid.errMsg
            });
        }
        else {
            Ext.Msg.show({
                iconCls: MineralPro.config.Runtime.listSyncMsgErrorIcon,
                title: MineralPro.config.Runtime.listSyncMsgErrorTitle,
                msg: MineralPro.config.Runtime.listSyncMsgErrorMsg,
                buttons: Ext.Msg.OK
            });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController onSyncListGridData.')
    },
    onRRCNumberCheckExisting: function (e, event, opt) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewController onRRCNumberCheckExisting.')

        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var appraisalDetailStore = view.down('grid').getStore();
        var appraisalGrid = mainPanel.down(view.appraisalListViewAlias);
        var appraisalStore = appraisalGrid.getStore();
        // var showAlert = false;
        var isEditAndSameValue = !view.newAppraisal && appraisalDetailStore.getAt(0) && appraisalDetailStore.getAt(0).get(mainPanel.selectedRRCNumberIndex) == e.value ? true : false;
        
        var subjectId = e.previousSibling('combobox[name=subjectId]').getValue();
        var rrcNumber = e.getValue();
        var subjectTypeCd = e.previousSibling('#subjectTypeCdItemId').getValue();
        if(subjectId && rrcNumber && subjectTypeCd && !isEditAndSameValue){
            Ext.getBody().mask('Checking RRC Number...');
            Ext.Ajax.request({
                url: appraisalStore.getProxy().getApi().read,
                method: 'GET',
                async: false,
                params: {
                    idAppUser: MineralPro.config.Runtime.idAppUser,
                    appraisalYear: MineralPro.config.Runtime.appraisalYear,
                    selectedId: subjectId,
                    selectedRRCNumber: rrcNumber,
                    subjectTypeCd: subjectTypeCd
                },
                success: function(response){
                    Ext.getBody().unmask();
                    var resp = Ext.decode(response.responseText);
                    if(resp.data.length){
                        Ext.Msg.show({
                            iconCls: Minerals.config.Runtime.appraisalExistRRCNumberAlertIcon,
                            title: Minerals.config.Runtime.appraisalExistRRCNumberAlertTitle,
                            msg: Minerals.config.Runtime.appraisalExistRRCNumberAlertMsg,
                            buttons: Ext.Msg.OK,
                            fn: function () {
                                e.focus();
                            }
                        });
                    }
                },
                failure: function(response){
                    Ext.getBody().unmask();
                    var msg = 'Status Text: '+response.statusText;
                    Ext.Msg.show({
                        iconCls: Minerals.config.Runtime.appraisalCalcAlertIcon,
                        title: 'Error!',
                        closeAction: 'hide',
                        msg: 'Something went wrong while processing your request.<br/><span style="font-weight:bold;line-height:25px;">Error Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p><p style="font-weight:bold;font-style:italic;color:red;">*** If error persist, please check you\'re server.</p>',
                        buttons: Ext.Msg.OK,
                    });
                }
            });
        }

        // setTimeout(function () {
        //     appraisalStore.each(function (rec) {
        //         if (rec.get(mainPanel.selectedIdIndex) == mainPanel.selectedId) {
        //             if (rec.get(mainPanel.selectedRRCNumberIndex) == e.value) {
        //                 showAlert = true;
        //                 if (!view.newAppraisal && appraisalDetailStore.getAt(0).get(mainPanel.selectedRRCNumberIndex) == e.value) {
        //                     showAlert = false;
        //                 }
        //                 return false;
        //             }
        //         }
        //     })

        //     if (showAlert) {
        //         Ext.Msg.show({
        //             iconCls: Minerals.config.Runtime.appraisalExistRRCNumberAlertIcon,
        //             title: Minerals.config.Runtime.appraisalExistRRCNumberAlertTitle,
        //             msg: Minerals.config.Runtime.appraisalExistRRCNumberAlertMsg,
        //             buttons: Ext.Msg.OK,
        //             fn: function () {
        //                 e.focus();
        //             }
        //         });
        //     }
        // }, 200)

        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController onRRCNumberCheckExisting.')
    },
    onAfterLoadAppraisal: function (e, event, opt) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewController onAfterLoadAppraisal.')

        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var appraisalGrid = mainPanel.down(view.appraisalListViewAlias);
        var appraisalStore = appraisalGrid.getStore();
       
        Ext.each(view.query('field:not(hiddenfield):not(displayfield)'),function(field){
            if(field.tabIndex > 0 && field.tabIndex !== -1){
            field.inputEl.dom.tabIndex = field.initialConfig.tabIndex;
            }
        });


        var appraisalGridSelection = appraisalGrid.getSelectionModel();
        var appraisalStoreCount = appraisalStore.getCount();
        var selected = appraisalGridSelection.getSelection();
        
        var currentIndex = 0;
        me.resetFormValues(false)

        if(selected.length){
            currentIndex = appraisalStore.indexOf(selected[0]);
        }
        else if(mainPanel.selectedId && mainPanel.selectedRRCNumber){
            currentIndex = appraisalStore.findBy(function(rec,id){
                return rec.get(mainPanel.selectedIdIndex) == mainPanel.selectedId && rec.get(mainPanel.selectedRRCNumberIndex) == mainPanel.selectedRRCNumber;
            });
        }
        currentIndex = currentIndex > -1 ? currentIndex : 0;
        view.down('#appraisalIndexItemId').setText( (currentIndex+1) + ' of ' + appraisalStoreCount);

        // var subjectCombo = view.down('combobox[name=subjectType]');
        // subjectCombo.fireEvent('change',subjectCombo, subjectCombo.getValue(), '');
        var declineGridArr = view.down('#declineTypeItemId').query('grid');

        Ext.each(declineGridArr, function(grid){
            if(mainPanel.selectedId && mainPanel.selectedRRCNumber){
                var store = grid.getStore();
                store.getProxy().extraParams = {
                    selectedId: mainPanel.selectedId,
                    rrcNumber: mainPanel.selectedRRCNumber,
                    subjectTypeCd: mainPanel.subjectTypeCd
                }
                store.load({
                    callback: function(records, operation, success){
                        if(success){

                        }
                        else{
                            me.storeFailureCallback(operation);
                        }
                    }
                });
            }
        })
        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController onAfterLoadAppraisal.')
    },
    onCalcButton: function(resetRecord){
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewController onCalcButton.')
        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var calcAppraisalStore = Ext.StoreMgr.lookup(view.calcAppraisalStore);
        var gridStore = view.down('grid').getStore();
        var record = gridStore.getAt(0);
        var values = view.getValues();
        var recordCopy = record.copy(null);
        var resetToOrigValues = function(){
            MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewController onCalcButton resetToOrigValues.')
            // var fieldsReverted = record.copyFrom(recordCopy);
            gridStore.rejectChanges();
            record.set(recordCopy.getData()); // for some reason record.copyfrom does not copy all data from the passed data model.
            record.dirty = false;
            MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController onCalcButton resetToOrigValues.')
        };
        var formFields = Ext.Object.getKeys(values);
        var equal = Ext.Array.every(formFields, function(field){
            return record && record.get(field) == values[field];
        })
        record.set(values);

        if(calcAppraisalStore.proxy.api.read.indexOf('subjectTypeCd') != -1){
            calcAppraisalStore.getProxy().extraParams = {
                selectedId: mainPanel.selectedId,
                selectedRRCNumber: mainPanel.selectedRRCNumber,
                rowUpdateUserid: MineralPro.config.Runtime.idAppUser,
                newAppraisal: view.newAppraisal,
                maxYear: view.down('#maxYearItemId').getValue()
            };
        }else{
            calcAppraisalStore.getProxy().extraParams = {
                subjectTypeCd: gridStore.getAt(0).get('subjectTypeCd'),
                selectedId: mainPanel.selectedId,
                selectedRRCNumber: mainPanel.selectedRRCNumber,
                rowUpdateUserid: MineralPro.config.Runtime.idAppUser,
                newAppraisal: view.newAppraisal,
                maxYear: view.down('#maxYearItemId').getValue()
            };
        }
            
        var dirty = gridStore.getNewRecords().length > 0
                || gridStore.getUpdatedRecords().length > 0
                || gridStore.getRemovedRecords().length > 0
                || gridStore.getModifiedRecords().length > 0;

        if(dirty){
            var isFormValid = me.isFormValid();
            
            if(isFormValid.valid){
                var origRrcNumber = mainPanel.selectedRRCNumber;
                gridStore.getAt(0).set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
                gridStore.getProxy().extraParams = {
                    origRrcNumber: origRrcNumber,
                    newRecord: false,
                    newCalculation: false,
                    updateCalc: true
                };
                gridStore.sync({
                    success: function(){
                        resetToOrigValues();
                        loadCalc();
                    },
                    failure: function(batch, options){
                        /*This will be called if request was able to communicate with the server but failed to sync. Connection problems like server isn't responding will not be thrown to this block of code. add exception on store proxy instead*/
                        resetToOrigValues();
                        var records = batch.getOperations().map(function(op){
                            return op.getRecords();
                        });
                        if(records.length && typeof records[0].getData == 'function'){
                            gridStore.getAt(0).set(records[0].getData());
                        }
                        Ext.getBody().unmask();
                        var msg = batch.getExceptions().map(function(operation){
                            return 'Status Text: '+operation.getError().statusText;
                        });
                        msg = msg.join('<br/> ');
                        
                        Ext.Msg.show({
                            title: 'Oops!',
                            msg: 'Failed to save your data <br/><span style="font-weight:bold;line-height:25px;">Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p>',
                            icon: Ext.Msg.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    }
                })
                //record.set(record.previousValues);
            }
            else{
                resetToOrigValues();
                Ext.Msg.show({
                    title: 'Please check your input data',
                    icon: Ext.Msg.ERROR,
                    msg: isFormValid.errMsg,
                    buttons: Ext.Msg.OK
                });
            }
        }
        else{
            resetToOrigValues();
            loadCalc()
        }

        function loadCalc(){
            calcAppraisalStore.getProxy().setTimeout(120000);// 2 min timeout
            calcAppraisalStore.load({
                callback: function(record, operation, success){
                    if(success){
                        var updateCalcFieldValue = function (fieldArray,  record){
                            var exclude = [
                                'subjectId','subjectName','rrcNumber','fieldId','wellDepth','wellDepth','x','legalDescription',
                                'wellDescription', 'wellTypeCd', 'maxYearLife','note','productionEquipmentCount',
                                'productionWellSchedule', 'productionEquipmentValue', 'serviceEquipmentCount',
                                'serviceWellSchedule', 'serviceEquipmentValue', 'injectionEquipmentCount',
                                'injectionWellSchedule', 'injectionEquipmentValue', 'maxYearLife',
                                'discountRate', 'grossOilPrice', 'grossProductPrice',
                                'grossWorkingInterestGasPrice', 'grossRoyaltyInterestGasPrice', 'operatingExpense',
                                'dailyAverageOil', 'dailyAverageGas', 'dailyAverageProduct'
                            ]
                            view.calcToFalse = true
                            Ext.each(fieldArray, function(field){
                                if(exclude.indexOf(field.name) == -1){
                                    var recordval = record.get(field.name) || ''
                                    field.setValue(recordval)
                                    var fieldval = field.getValue();
                                    fieldval = fieldval ? fieldval.toString() : '';
                                    view.calcToFalse = fieldval == recordval;
                                }
                            })
                        };
                        var viewModelRecord = view.getRecord();
                            viewModelRecord.dirty=true;
                        if(record[0]){
                           // record[0].set('rrcNumber', mainPanel.selectedRRCNumber)
                            var textArray = view.query('textfield');
                            var numberArray = view.query('numberfield');
                            var comboArray = view.query('combobox');
                            updateCalcFieldValue(textArray, record[0]);
                            updateCalcFieldValue(numberArray, record[0]);
                            updateCalcFieldValue(comboArray, record[0]);
                            view.newCalculation = true;
                            if(view.calcToFalse){
                                view.newCalculation = false;
                            }
                            if(view.newCalculation == true && view.down('#saveToDbButtonItemId').isDisable()){
                                view.down('#saveToDbButtonItemId').enable(true);
                            }

                            if(view.newAppraisal == true){
                                me.resetFormValues(false);
                                view.newAppraisal = false;
                                view.newCalculation = false;
                                view.down('#saveToDbButtonItemId').disable(true);
                                gridStore.getProxy().extraParams = {
                                    selectedId: mainPanel.selectedId,
                                    selectedRRCNumber: mainPanel.selectedRRCNumber,
                                    subjectTypeCd: gridStore.getAt(0).get('subjectTypeCd')
                                };

                                gridStore.reload({
                                    callback: function(record, operation, success){
                                        if(success){
                                            if (gridStore.getAt(0)) {
                                                view.down('grid').getSelectionModel().select(0, false, false);
                                            }
                                        }
                                        else{
                                            me.storeFailureCallback(operation);
                                        }
                                    }
                                });
                            }
                        }else{
                            Ext.Msg.show({
                                iconCls: Minerals.config.Runtime.appraisalCalcAlertIcon,
                                title: Minerals.config.Runtime.appraisalCalcAlertTitle,
                                msg: Minerals.config.Runtime.appraisalCalcAlertMsg,
                                buttons: Ext.Msg.OK,
                            });
                            view.newCalculation= false
                            view.down('#saveToDbButtonItemId').disable(true);
                        }
                    }
                    else{
                        me.storeFailureCallback(operation)
                    }
                }
            })
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController onCalcButton.')

    },
//
//    onChangeData: function (key, newValue, oldValue, eOpts) {
//        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewController onChangeData.')
//
//        var me = this;
//        var view = me.getView();
//
//        var appraisalStore = view.down('grid').getStore();
//        var keyValue = ''
//        var storeValue = ''
//
//        if (key.value) {
//            keyValue = key.value
//        }
//        if (appraisalStore.count() > 0) {
//            if (appraisalStore.getAt(0).get(key.name)) {
//                storeValue = appraisalStore.getAt(0).get(key.name)
//            }
//        }
//
//        if (keyValue.toString() != storeValue.toString()) {
//            key.setFieldStyle('background-color: coral; color: white');
//        } else {
//            if(key.readOnly){
//                key.setFieldStyle('background-color: white; color: #999966');
//            }else{
//                key.setFieldStyle('background-color: white; color:black;');
//            }
//        }
//        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController onChangeData.')
//    },
//
//    resetFormValues: function (resetRecord) {
//        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewController resetFormValues.')
//        var me = this;
//        var view = me.getView();
//
//        var appraisalStore = view.down('grid').getStore();
//
//        var record = appraisalStore.getAt(0);
//        var textArray = view.query('textfield');
//        var numberArray = view.query('numberfield');
//        var comboArray = view.query('combobox');
//
//        resetField(textArray, resetRecord, '0');
//        resetField(numberArray, resetRecord, '0');
//        resetField(comboArray, resetRecord, '');
//
//        function resetField(fieldArray, resetRecord, value) {
//            var exclude = '[operatorName],[agencyName]'
//            Ext.each(fieldArray, function (field) {
//                if (resetRecord && exclude.indexOf('[' + field.name + ']') == -1) {
//                    record.set(field.name, value)
//                    field.setValue(value)
////                     console.log(field.name + '; ' + value)
//                }
//                if(field.readOnly){
//                    field.setFieldStyle('background-color: white; color: #999966');
//                }else{
//                    field.setFieldStyle('background-color: white; color:black;');
//                }
////                field.setFieldStyle('background-color: white;');
//            })
//        }
//
//        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController resetFormValues.')
//
//    },
//
//    onSelectFieldName: function (combo, rec, eOpts) {
//        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewController onSelectFieldName.')
//        var me = this;
//        var view = me.getView();
//        view.down('#wellDepthItemId').setValue(rec.get('wellDepth'));
//        view.down('#gravityCdItemId').setValue(rec.get('gravityCd'));
//        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController onSelectFieldName.')
//    },
//
    onValidateDeclineBeforeEdit: function(editor, context, eOpts){
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewController onValidateDeclineBeforeEdit.')
        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var appraisalStore = view.down('grid').getStore();
        var selectedId = appraisalStore.getCount() ? appraisalStore.getAt(0).get('subjectId') : '';
        if(mainPanel.newAppraisal){
            selectedId = view.down('combobox[name=subjectId]').getValue();
        }
        selectedId = selectedId && selectedId != '0' ? selectedId : '';
        var rrcNumber = view.down('#rrcNumberItemId').getValue();
        rrcNumber = rrcNumber && rrcNumber != '0' ? rrcNumber : ''
        var canEdit = selectedId && rrcNumber;
        if(!canEdit){
            var required = [{key: 'Selected ID', value: selectedId}, {key: 'RRC Number', value: rrcNumber}].filter(function(field){
                return !field.value;
            }).map(function(field){
                return field.key;
            });
            var fields = required.length > 1 ? required.join(' and ') + ' are ' : required[0] + ' is ';
            Ext.Msg.show({
                title: 'Oops!',
                buttons: Ext.Msg.OK,
                msg: fields+'required before you can edit this field.'
            });
            return false;
        }
        view.down('#calcButtonItemId').disable()
        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController onValidateDeclineBeforeEdit.')
        return true;
    },
    onValidateDecline: function (editor, context) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewController onValidateDecline.')
        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var appraisalStore = view.down('grid').getStore();
        var store = context.grid.getStore();
        var selectedId = appraisalStore.getCount() ? appraisalStore.getAt(0).get('subjectId') : '';
        var subjectTypeCd = appraisalStore.getCount() ? appraisalStore.getAt(0).get('subjectTypeCd') : '';
        
        if(mainPanel.newAppraisal){
            selectedId = view.down('combobox[name=subjectId]').getValue();
            subjectTypeCd = view.down('[name=subjectTypeCd]').getValue();
        }
        selectedId = selectedId && selectedId != '0' ? selectedId : '';
        var rrcNumber = view.down('#rrcNumberItemId').getValue();
        rrcNumber = rrcNumber && rrcNumber != '0' ? rrcNumber : ''
        if(rrcNumber && selectedId){
            store.getProxy().extraParams = {
                selectedId: selectedId, // mainPanel.selectedId,
                subjectTypeCd: subjectTypeCd, // appraisalStore.getAt(0).get('subjectTypeCd'),
                rrcNumber: rrcNumber, //view.getRecord().get('rrcNumber'),
                createLength: store.getNewRecords().length,
                updateLength: store.getUpdatedRecords().length,
                queueUserid: MineralPro.config.Runtime.idAppUser,
                rowUpdateUserId: MineralPro.config.Runtime.idAppUser,
                appraisalYear: MineralPro.config.Runtime.appraisalYear
            }

            store.getModifiedRecords().forEach(function(rec){
                var decline = rec.get('decline') || 0;
                var declineYears = rec.get('declineYears') || 0;
                if(parseInt(decline) != 0 || parseInt(declineYears) != 0){
                    if(rec.getField('subjectId') && (rec.get('subjectId') != selectedId)){
                        rec.set('subjectId', selectedId)
                    }
                    if(rec.get('rrcNumber') != rrcNumber){
                        rec.set('rrcNumber', rrcNumber);
                    }
                    rec.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
                }
            });
            if(store.getModifiedRecords().length || store.getRemovedRecords().length){
                /* Increment count only if store has dirty data to sync to the database*/
                view.down('#calcButtonItemId').disable();
                me.counter += 1;
            }
            else if(!editor.editing){
                view.down('#calcButtonItemId').enable();
            }
            store.sync({
                success: function(batch, options){
                    Ext.getBody().unmask();
                    store.commitChanges();
                },
                failure: function(batch, options){
                    Ext.getBody().unmask();
                    store.rejectChanges();
                    var msg = batch.getExceptions().map(function(operation){
                        return 'Status Text: '+operation.getError().statusText;
                    });
                    msg = msg.join('<br/> ');
                    
                    Ext.Msg.show({
                        title: 'Oops!',
                        msg: 'Failed to save your data <br/><span style="font-weight:bold;line-height:25px;">Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p>',
                        icon: Ext.Msg.ERROR,
                        buttons: Ext.Msg.OK
                    });
                },
                callback: function(){
                    /*allways called regardless if successful or not*/
                    me.counter -= 1;
                    if(me.counter == 0 && !view.newAppraisal && !editor.editing){
                        view.down('#calcButtonItemId').enable();
                    }
                    Ext.getBody().unmask();
                }
            });

            var lastRec = store.getAt(store.getCount()-1)
            var curRec = context.record
            if(lastRec.get('decline') != '0' || lastRec.get('declineYears') != '0'){
                var recDecline = Ext.create('Minerals.model.MineralValuation.AppraisalModel')
                recDecline.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                store.insert(store.getCount()+1, recDecline)
            }

            if(curRec.get('decline') == '0' && curRec.get('declineYears') == '0'){
                if(context.rowIdx < store.getCount()-1){
                    store.remove(context.record);
                }
            }else{
                curRec.set('oriDecline', curRec.get('decline'));
                curRec.set('oriDeclineYears', curRec.get('declineYears'));
            }
            
        } else {
            Ext.Msg.show({
                title: 'Please check your input data',
                buttons: Ext.Msg.OK,
                msg: 'Selected ID and RRCNUmber are required before you can edit this field.'
            });
            store.reload({
                callback: function(record, operation, success){
                    if(success){
                        /*Do something when successful*/
                    }
                    else{
                        me.storeFailureCallback(operation)
                    }
                }
            });
            view.body.dom.scrollTop = 100;
            // view.down('#rrcNumberItemId').focus();
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController onValidateDecline.')
    },



});
