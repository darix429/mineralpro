Ext.define('Minerals.sub.MineralValuationManageAppraisal.view.AppraisalDetails.AppraisalDetailsView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.AppraisalDetailsView',
    layout:'border',
    
    id: 'AppraisalDetailsViewId',
    
    items: [{
            xtype: 'AppraisalDetailsFormView',
            region:'center'
        }] 
});    