Ext.define('Minerals.sub.MineralValuationManageAppraisal.view.AppraisalDetails.AppraisalDetailsFormView', {
    extend: 'MineralPro.config.view.BaseView',
    alias: 'widget.AppraisalDetailsFormView',
    requires: ['Ext.app.ViewModel','Minerals.config.view.Appraisal.AppraisalDetailsFormViewController'],
    controller: 'AppraisalDetailsFormViewController',
    id: 'AppraisalDetailsFormViewId',
    mainPanelAlias: 'MineralValuationManageAppraisal',
    mainPanelId: 'MineralValuationManageAppraisalId',
    appraisalStore: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalDetailStore',
    calcAppraisalStore: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalCalcStore',
    appraisalListViewAlias: 'AppraisalListGridView',
    layout: 'hbox',
    firstFocus: 'rrcNumberItemId',
    newAppraisal: false,
    newCalculation: false,
    appraisalModel: 'Minerals.model.MineralValuation.AppraisalModel',
    scrollable: true,
    width: 600,
    defaults: {
        margin: '2 2 2 2',
    },
    viewModel: {
        data: {
            isLease: ''
        }
    },
    initComponent: function () {
        var me = this;
        var appraisalStore = Ext.StoreMgr.lookup(me.appraisalStore);
        
        appraisalStore.on({
            load: function (store, records, successful, operation, eOpts) {
                Ext.getBody().mask('Loading...')
                var comboSubjectId = Ext.StoreMgr.lookup('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSubjectIdStore');
                var comboSubjectName = Ext.StoreMgr.lookup('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSubjectNameStore');

                comboSubjectId.reload({
                    callback: function () {
                        Ext.getBody().unmask()
                        var form = Ext.getCmp('AppraisalDetailsFormViewId');
                        var mainPanel = Ext.getCmp(form.mainPanelId)
                        var newRec = false;
                        if (records) {
                            if (records[0]) {
                                form.loadRecord(records[0]);
                                form.down('#calcButtonItemId').enable(true);                               
                                if (mainPanel.newAppraisal == true) {
                                    Ext.getBody().mask('Loading...')
                                    mainPanel.newAppraisal = false;
                                    form.down('#calcButtonItemId').disable(true);
                                    setTimeout(function(){
                                    mainPanel.getActiveTab().down('#newAppraisalButton').click();
                                    },200)                                   
                                }else{
                                    me.newAppraisal = false
                                    me.newCalculation = false
                                }
                            } else {
                                newRec = true;
                            }
                        } else {
                            newRec = true;
                        }
                        if (newRec) {
                            console.log('New Record Created')
                            var record = Ext.create(form.appraisalModel);
                            record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
                            record.set('subjectId', mainPanel.selectedId)
                            form.loadRecord(record);
                            appraisalStore.insert(0, record);
                            form.down('#calcButtonItemId').disable(true);
                            mainPanel.newAppraisal = false;
                        }
                        form.getController().onAfterLoadAppraisal();
                    }
                });
            },
        });

        appraisalStore.on({
            beforeload: function () {
                var form = Ext.getCmp('AppraisalDetailsFormViewId');
                var mainPanel = Ext.getCmp(me.mainPanelId)
                var params = this.getProxy().extraParams
                params['newRecord'] = me.newAppraisal;
                params['idAppUser'] = MineralPro.config.Runtime.idAppUser;
                params['subjectTypeCd'] = mainPanel.subjectTypeCd
                this.getProxy().extraParams = params;
                if(mainPanel.newAppraisal){
                    Ext.getBody().unmask();
                    me.down('#newAppraisalButton').fireEvent('click');
                    return false;
                }
            },
        })

        appraisalStore.on({
            beforesync: function () {
                var form = Ext.getCmp('AppraisalDetailsFormViewId');
                var mainPanel = Ext.getCmp(form.mainPanelId)
                var params = this.getProxy().extraParams
                params['newRecord'] = me.newAppraisal;
                params['idAppUser'] = MineralPro.config.Runtime.idAppUser;
                params['subjectTypeCd'] = mainPanel.subjectTypeCd
                this.getProxy().extraParams = params;
            },
        })
                
        me.items = [{
//                xtype: 'BaseGridView',
                xtype: 'grid',
                store: me.appraisalStore,
                hidden: true,
//                controller: '',
            }, {
                xtype: 'fieldset',
                padding: '0 5 5 5',
                margin: '-5 0 0 0',
                minWidth: 330,
                maxWidth: 340,
                flex: 1,
                title: 'Basic Information',
                defaults: {
                    maskRe: /[^'\^]/,
                    labelAlign: 'right',
                    margin: '0 0 1 0',
                    width: '100%',
                    height: 25,
                    enableKeyEvents: true,
                    listeners: {
                        change: 'onChangeData',
                    }
                },
                items: [{
                        fieldLabel: 'Subject Type',
                        xtype: 'combobox',
                        itemId: 'subjectTypeItemId',
                        name: 'subjectType',
                        store: [['Unit', 'Unit'], ['Lease', 'Lease']],
                        style: {
                            opacity: 1
                        },
                        bind: {
                            value: '{isLease}'
                        },
                        fieldStyle: 'font-weight:bold;',
                        disabled: true,
                        tabIndex: 1,
                        listeners: {
                            change: function(combo, newValue, oldValue, eOpts){
                                MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsForm SubjectType change Event.')
                                me.getController().onChangeData(combo, newValue, oldValue, eOpts);
                                var subjectTypeCd = newValue == 'Unit' ? 1 : 2;
                                me.down('#subjectTypeCdItemId').setValue(subjectTypeCd);
                                var subjectIdStore = me.down('#subjectIdItemId').getStore();
                                var subjectIdInitialVal = me.down('#subjectIdItemId').getValue()
                                var subjectNameStore = me.down('#nameItemId').getStore();
                                var subjectNameInitialVal = me.down('#nameItemId').getValue();
                                subjectIdStore.getProxy().extraParams = {
                                    subjectType: combo.value
                                };

                                subjectNameStore.getProxy().extraParams = {
                                    subjectType: combo.value
                                };
                                Ext.getBody().mask('loading...');
                                subjectIdStore.reload({
                                    callback: function (records, operation, success) {
                                        if(success){
                                            subjectNameStore.reload({
                                                callback: function (records, operation, success) {
                                                    Ext.getBody().unmask();
                                                    var mainPanel = Ext.getCmp(me.mainPanelId)
                                                    if(success){
                                                        var selectedId = mainPanel.selectedId && mainPanel.selectedId != '0' ? mainPanel.selectedId : '';
                                                        var rrcNumber = mainPanel.selectedRRCNumber && mainPanel.selectedRRCNumber != '0' ? mainPanel.selectedRRCNumber : '';
                                                        var selected = selectedId ? selectedId : (subjectIdInitialVal && subjectIdInitialVal != '0') ? subjectIdInitialVal : '';
                                                        var rec = subjectIdStore.findRecord('subjectId', selected);
                                                        var subjectName = rec ? rec.get('subjectName') : '';

                                                        combo.nextSibling('#subjectIdItemId').suspendEvent('change')
                                                        combo.nextSibling('#subjectIdItemId').setValue('');//just to clear out the previously set value. Just to be sure that change event will be triggered
                                                        combo.nextSibling('#subjectIdItemId').resumeEvent('change')
                                                        
                                                        if(selectedId){
                                                            combo.nextSibling('#subjectIdItemId').suspendEvent('change');
                                                            combo.nextSibling('#subjectIdItemId').setValue(selectedId);
                                                            combo.nextSibling('#subjectIdItemId').resumeEvent('change');
                                                        }
                                                        else if(subjectIdInitialVal && subjectIdInitialVal != '0'){
                                                            combo.nextSibling('#subjectIdItemId').suspendEvent('change');
                                                            combo.nextSibling('#subjectIdItemId').setValue(subjectIdInitialVal);    
                                                            combo.nextSibling('#subjectIdItemId').resumeEvent('change');
                                                        }
                                                        if(me.down('#nameItemId').getValue() != subjectName){
                                                            me.down('#nameItemId').suspendEvent('change')
                                                            me.down('#nameItemId').setValue(subjectName)
                                                            me.down('#nameItemId').resumeEvent('change')
                                                        }
                                                    }
                                                    else{
                                                        Ext.Msg.show({
                                                            title: 'Oops!',
                                                            iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                                                            msg: 'Something went wrong while loading subjectNames. Would you like to try again?',
                                                            buttons: Ext.Msg.YESNO,
                                                            fn: function(btn){
                                                                if(btn=='yes'){
                                                                    subjectNameStore.reload();
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                        else{
                                            Ext.getBody().unmask();
                                            Ext.Msg.show({
                                                title: 'Oops!',
                                                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                                                msg: 'Something went wrong while loading subjectIDs. Would you like to try again?',
                                                buttons: Ext.Msg.YESNO,
                                                fn: function(btn){
                                                    if(btn=='yes'){
                                                        subjectIdStore.reload();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });
                                MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsForm SubjectType change Event.')  
                            }
                        }                        
                    }, {
                        fieldLabel: 'Subject Id',
                        xtype: 'combobox',
                        selectOnFocus: true,
                        itemId: 'subjectIdItemId',
                        name: 'subjectId',
                        displayField: 'subjectId',
                        valueField: 'subjectId',
                        store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSubjectIdStore',
                        queryMode: 'local',
                        tabIndex: 2,
                        style: {
                            opacity: 1
                        },
                        fieldStyle: 'font-weight:bold;',
                        forceSelection: true,
                        disabled: true,
                        listeners: {
                            // select: function (combo, record) {
                            //     var comboStore = combo.getStore();
                            //     var rec = comboStore.getAt(comboStore.find('subjectId', combo.getValue(), false, false, true));
                            //     if(rec && rec.get('subjectType') && rec.get('subjectName') && rec.get('subjectTypeCd')){
                            //         me.down('#subjectTypeItemId').setValue(rec.get('subjectType'))
                            //         me.down('#nameItemId').setValue(rec.get('subjectName'))
                            //         me.down('#subjectTypeCdItemId').setValue(rec.get('subjectTypeCd'))
                            //         me.down('#rrcNumberItemId').focus()
                            //     }
                            // },
                            change: function(combo, newValue, oldValue, eOpts){
                                MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsForm SubjectId change Event.')
                                me.getController().onChangeData(combo, newValue, oldValue, eOpts);

                                var comboStore = combo.getStore();
                                    
                                var rec = comboStore.getAt(comboStore.find('subjectId', newValue, false, false, true));
                                var rrcNumber = combo.nextSibling('numberfield[name=rrcNumber]').getValue();
                                var subjectName = '';
                                var subjectTypeCd = '';
                                var subjectType = '';
                                if(rec && rec.get('subjectType') && rec.get('subjectName') && rec.get('subjectTypeCd')){
                                    var mainPanel = me.up(me.mainPanelAlias);
                                    var appraisalList = mainPanel.down(me.appraisalListViewAlias);
                                    var appraisalListStore = appraisalList.getStore();
                                    var appraisalStoreCount = appraisalListStore.getCount();
                                    var recIndex = appraisalListStore.findBy(function(record){
                                        return record.get('subjectId') == rec.get('subjectId') && record.get('rrcNumber') == rrcNumber;
                                    });
                                    appraisalList.getSelectionModel().select(recIndex);
                                    me.down('#appraisalIndexItemId').setText( (recIndex+1) + ' of ' + appraisalStoreCount);
                                    if(rec.get('subjectType').toLowerCase() == 'lease'){
                                        var newSelected = appraisalList.getSelectionModel().getSelection();
                                        if(newSelected.length){
                                            me.down('textarea[name=legalDescription]').setValue(newSelected[0].get('legalDescription'))
                                        }
                                    }
                                    subjectName = rec.get('subjectName');
                                    subjectTypeCd = rec.get('subjectTypeCd');
                                    subjectType = rec.get('subjectType');
                                }
                                if(me.down('#nameItemId').getValue() != subjectName){
                                    me.down('#nameItemId').suspendEvent('change')
                                    me.down('#nameItemId').setValue(subjectName)
                                    me.down('#nameItemId').resumeEvent('change')
                                }
                                if(me.down('#subjectTypeItemId').getValue() != subjectType){
                                    me.down('#subjectTypeItemId').suspendEvent('change');
                                    me.down('#subjectTypeItemId').setValue(subjectType)
                                    me.down('#subjectTypeItemId').resumeEvent('change');
                                }
                                if(me.down('#subjectTypeCdItemId').getValue() != subjectTypeCd){
                                    me.down('#subjectTypeCdItemId').suspendEvent('change')
                                    me.down('#subjectTypeCdItemId').setValue(subjectTypeCd)
                                    me.down('#subjectTypeCdItemId').resumeEvent('change')
                                }
                                // me.down('#rrcNumberItemId').focus()
                                
                                MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsForm SubjectId change Event.')
                            }
                        }
                    }, {
                        fieldLabel: 'Subject Name',
                        xtype: 'combobox',
                        name: 'name',
                        displayField: 'subjectName',
                        valueField: 'subjectName',
                        store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSubjectNameStore',
                        itemId: 'nameItemId',
                        disabled: true,
                        queryMode: 'local',
                        tabIndex: 3,
                        style: {
                            opacity: 1
                        },
                        fieldStyle: 'font-weight:bold;',
                        forceSelection: true,
                        selectOnFocus: true,
                        listeners: {
                            // select: function (combo, record) {
                            //     var comboStore = combo.getStore();
                            //     var rec = comboStore.getAt(comboStore.find('subjectId', combo.getValue(), false, false, true));
                            //     if(rec && rec.get('subjectType') && rec.get('subjectName') && rec.get('subjectTypeCd')){
                            //         me.down('#subjectTypeItemId').setValue(rec.get('subjectType'))
                            //         me.down('#nameItemId').setValue(rec.get('subjectName'))
                            //         me.down('#subjectTypeCdItemId').setValue(rec.get('subjectTypeCd'))
                            //         me.down('#rrcNumberItemId').focus()
                            //     }
                            // },
                            change: function(combo, newValue, oldValue, eOpts){
                                MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormView SubjectName change listener.')
                                me.getController().onChangeData(combo, newValue, oldValue, eOpts);

                                var comboStore = combo.getStore();
                                var rec = comboStore.findRecord('subjectName', newValue);
                                if(rec && rec.get('subjectType') && rec.get('subjectName') && rec.get('subjectTypeCd')){
                                    if(rec.get('subjectType').toLowerCase() == 'lease'){
                                        var mainPanel = me.up(me.mainPanelAlias);
                                        var appraisalList = mainPanel.down(me.appraisalListViewAlias);
                                        var appraisalListStore = appraisalList.getStore();
                                        var findRec = appraisalListStore.findRecord('subjectId',rec.get('subjectId'))
                                        appraisalList.getSelectionModel().select(findRec);
                                        if(findRec){
                                            me.down('textarea[name=legalDescription]').setValue(findRec.get('legalDescription'))
                                        }
                                    }
                                    if(me.down('#subjectIdItemId').getValue() != rec.get('subjectId')){
                                        me.down('#subjectIdItemId').suspendEvent('change');
                                        me.down('#subjectIdItemId').setValue(rec.get('subjectId'));
                                        me.down('#subjectIdItemId').resumeEvent('change');    
                                    }
                                    if(me.down('#subjectTypeItemId').getValue() != rec.get('subjectType')){
                                        me.down('#subjectTypeItemId').suspendEvent('change');
                                        me.down('#subjectTypeItemId').setValue(rec.get('subjectType'))
                                        me.down('#subjectTypeItemId').resumeEvent('change');
                                    }
                                    if(me.down('#subjectTypeCdItemId').getValue() != rec.get('subjectTypeCd')){
                                        me.down('#subjectTypeCdItemId').suspendEvent('change')
                                        me.down('#subjectTypeCdItemId').setValue(rec.get('subjectTypeCd'))
                                        me.down('#subjectTypeCdItemId').resumeEvent('change')
                                    }
                                    me.down('#rrcNumberItemId').focus()
                                }
                                MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormView SubjectName change listener.')
                            }
                        }
                    }, {
                        xtype: 'hidden',
                        name: 'subjectTypeCd',
                        itemId: 'subjectTypeCdItemId'
                    }, {
                        fieldLabel: 'RRC Number',
                        xtype: 'numberfield',
                        minValue: 0,
                        maxValue: 2147483647, //max value for mssql int
                        tabIndex: 4,
                        name: 'rrcNumber',
                        fieldStyle: 'font-weight:bold;',
                        itemId: 'rrcNumberItemId',
                        listeners: {
                            blur: 'onRRCNumberCheckExisting',
                            change: 'onChangeData',
                        }
                    }, {
                        xtype: 'hidden',
                        name: 'origRrcNumber',
                        itemId: 'origRrcNumberItemId'
                    }, {
                        fieldLabel: 'Field',
                        xtype: 'combobox',
                        itemId: 'fieldItemId',
                        selectOnFocus: true,
                        tabIndex: 5,
                        name: 'fieldId',
                        displayField: 'fieldIdName',
                        valueField: 'fieldId',
                        store: 'Minerals.store.MineralValuation.FieldComboStore',
                        queryMode: 'local',
                        //forceSelection: true,
                        listeners: {
                            select: 'onSelectFieldName',
                            change: 'onChangeData',
                        }
                    }, {
                        fieldLabel: 'Well Depth',
                        xtype: 'numberfield',
                        minValue: '0',
                        selectOnFocus: true,
                        name: 'wellDepth',
                        readOnly: true,
                        itemId: 'wellDepthItemId',
                    }, {
                        fieldLabel: 'Gravity',
                        xtype: 'combobox',
                        selectOnFocus: true,
                        name: 'gravityCd',
                        displayField: 'gravityDesc',
                        valueField: 'gravityCd',
                        store: 'Minerals.store.ProperRecords.GravityCdStore',
                        queryMode: 'local',
                        forceSelection: true,
                        readOnly: true,
                        itemId: 'gravityCdItemId',
                    }, {
                        fieldLabel: 'Well Description',
                        xtype: 'textfield',
                        selectOnFocus: true,
                        tabIndex: 6,
                        name: 'wellDescription',
                    }, {
                        fieldLabel: 'WellType',
                        xtype: 'combobox',
                        selectOnFocus: true,
                        tabIndex: 7,
                        name: 'wellTypeCd',
                        displayField: 'wellTypeDesc',
                        valueField: 'wellTypeCd',
                        store: 'Minerals.store.MineralValuation.WellTypeComboStore',
                    }, {
                        fieldLabel: 'Year Life',
                        xtype: 'textfield',
                        readOnly: true,
                        fieldStyle: 'color: #000; font-weight:bold; ',
                        name: 'yearLife',    
                    }, {
                        fieldLabel: 'Comment',
                        xtype: 'textarea',
                        tabIndex: 8,
                        name: 'comment',
                        maxLength: 50,
                        width: '100%',
                    },
                    {
                        fieldLabel: 'Legal Description',
                        xtype: 'textarea',
                        bind: {
                            hidden: '{isLease=="Unit"}'
                        },
                        disabled: true,
                        tabIndex: 10,
                        name: 'legalDescription',
                        maxLength: 50,
                        width: '100%',
                        style: {
                            opacity: 1
                        }
                    }]
            }, {
                xtype: 'container',
                layout: 'vbox',
                minWidth: 660,
//            maxWidth: 375,
                flex: 1,
                defaults: {width: '100%'},
                items: [{
                        xtype: 'fieldset',
//                        padding: '0 5 5 5',
                        margin: '8 0 0 0',
                        title: '',
                        defaults: {
                            maskRe: /[^'\^]/,
                            xtype: 'container',
                            layout: 'hbox',
                            height: 32,
                            style: {
                                marginBottom: '0px'
                            }
                        },
                        items: [{
                            defaults: {
                                xtype: 'displayfield',
                                fieldStyle: 'text-align: center;',
                                margin: '0 2 0 2',                               
                            },
                            items: [{
                                value: '',
                                width: '20%'
                            }, {
                                value: '#',
                                width: '15%'
                            }, {
                                value: 'Schedule',
                                width: '49%'
                            }, {
                                value: 'Equip Value', 
                                width: '15%'
                            }]
                        },{
                            defaults: {
                                xtype: 'textfield',
                                enableKeyEvents: true,
                                margin: '0 2 0 2',
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },
                            items: [{
                                        xtype: 'displayfield',
                                        fieldStyle: 'text-align: right;',
                                        value: 'Production Wells:',
                                        width: '20%'
                                    }, {
                                        name: 'productionEquipmentCount',
                                        itemId: 'productionEquipmentCountItemId',
                                        maskRe: /[0-9.]/,
                                        tabIndex: 9,
                                        width: '15%',
                                        listeners: {
                                            change: function(key, newValue, oldValue, eOpts){
                                                var view = key.up('form');
                                                var count = view.down('#productionEquipmentCountItemId')
                                                var combo = view.down('#productionWellScheduleItemId');
                                                var comboStore = combo.getStore();
                                                var value = 0;
                                                var index = comboStore.find('valueField', combo.getValue());
                                                if(index != -1){
                                                    value = comboStore.getAt(index).get('salvageValue')
                                                }
                                                view.down('#productionEquipmentValueItemId').setValue(Ext.util.Format.number(count.getValue() * value, '000,000'))
                                                
                                                view.getController().onChangeData(key, newValue, oldValue, eOpts)
                                            }
                                        }
                                    }, {
                                        xtype: 'combobox',
                                        store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedProdStore',
                                        itemId: 'productionWellScheduleItemId',
                                        displayField: 'displayField',
                                        valueField: 'valueField',
                                        name: 'productionWellSchedule',
                                        queryMode: 'local',  
                                        allowBlank: true,
                                        tabIndex: 10,
                                        width: '49%',
                                        listeners:{
                                            focus: function(){
                                                this.expand();
                                            },
                                            change: function(key, newValue, oldValue, eOpts){
                                                var view = key.up('form');
                                                var count = view.down('#productionEquipmentCountItemId')
                                                var combo = view.down('#productionWellScheduleItemId');
                                                var comboStore = combo.getStore();
                                                var value = 0;
                                                var index = comboStore.find('valueField', combo.getValue());
                                                if(index != -1){
                                                    value = comboStore.getAt(index).get('salvageValue')
                                                }
                                                view.down('#productionEquipmentValueItemId').setValue(Ext.util.Format.number(count.getValue() * value, '000,000'))
                                                
                                                view.getController().onChangeData(key, newValue, oldValue, eOpts)
                                            }
                                        }
                                    }, {
                                        name: 'productionEquipmentValue',
                                        itemId: 'productionEquipmentValueItemId',
                                        maskRe: /[0-9.]/,
                                        readOnly: true,
                                        fieldStyle: 'color: #000;',
                                        width: '15%'
                                    }]
                        },{
                            defaults: {
                                xtype: 'textfield',
                                enableKeyEvents: true,
                                width: '24.5%',
                                margin: '0 2 0 2',
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },
                            items: [{
                                        xtype: 'displayfield',
                                        fieldStyle: 'text-align: right;',
                                        value: 'Service Wells:',
                                        width: '20%'
                                    }, {
                                        name: 'serviceEquipmentCount',
                                        itemId: 'serviceEquipmentCountItemId',
                                        maskRe: /[0-9.]/,
                                        tabIndex: 11,
                                        width: '15%',
                                        listeners: {
                                            change: function(key, newValue, oldValue, eOpts){
                                                var view = key.up('form');
                                                var count = view.down('#serviceEquipmentCountItemId')
                                                var combo = view.down('#serviceWellScheduleItemId');
                                                var comboStore = combo.getStore();
                                                var value = 0;
                                                var index = comboStore.find('valueField', combo.getValue());
                                                if(index != -1){
                                                    value = comboStore.getAt(index).get('salvageValue')
                                                }
                                                view.down('#serviceEquipmentValueItemId').setValue(Ext.util.Format.number(count.getValue() * value, '000,000'))
                                                
                                                view.getController().onChangeData(key, newValue, oldValue, eOpts)
                                            }
                                        }
                                    }, {
                                        xtype: 'combobox',
                                        store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedSvrcStore',
                                        itemId: 'serviceWellScheduleItemId',
                                        displayField: 'displayField',
                                        valueField: 'valueField',                                        
                                        name: 'serviceWellSchedule',
                                        queryMode: 'local',
                                        allowBlank: true,
                                        tabIndex: 12,
                                        width: '49%',
                                        listeners:{
                                            focus: function(){
                                                this.expand();
                                            },
                                            change: function(key, newValue, oldValue, eOpts){
                                                var view = key.up('form');
                                                var count = view.down('#serviceEquipmentCountItemId')
                                                var combo = view.down('#serviceWellScheduleItemId');
                                                var comboStore = combo.getStore();
                                                var value = 0;
                                                var index = comboStore.find('valueField', combo.getValue());
                                                if(index != -1){
                                                    value = comboStore.getAt(index).get('salvageValue')
                                                }
                                                view.down('#serviceEquipmentValueItemId').setValue(Ext.util.Format.number(count.getValue() * value, '000,000'))
                                                
                                                view.getController().onChangeData(key, newValue, oldValue, eOpts)
                                            }
                                        }
                                    }, {
                                        name: 'serviceEquipmentValue',
                                        itemId: 'serviceEquipmentValueItemId',
                                        maskRe: /[0-9.]/,
                                        readOnly: true,
                                        fieldStyle: 'color: #000;',
                                        width: '15%'
                                    }]
                        },{
                            defaults: {
                                xtype: 'textfield',
                                enableKeyEvents: true,
                                width: '24.5%',
                                margin: '0 2 0 2',
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },
                            items: [{
                                        xtype: 'displayfield',
                                        fieldStyle: 'text-align: right;',
                                        value: 'Injection Wells:',
                                        width: '20%'
                                    }, {
                                        name: 'injectionEquipmentCount',
                                        itemId: 'injectionEquipmentCountItemId',
                                        maskRe: /[0-9.]/,
                                        tabIndex: 13,
                                        width: '15%',
                                        listeners: {
                                            change: function(key, newValue, oldValue, eOpts){
                                                var view = key.up('form');
                                                var count = view.down('#injectionEquipmentCountItemId')
                                                var combo = view.down('#injectionWellScheduleItemId');
                                                var comboStore = combo.getStore();
                                                var value = 0;
                                                var index = comboStore.find('valueField', combo.getValue());
                                                if(index != -1){
                                                    value = comboStore.getAt(index).get('salvageValue')
                                                }
                                                view.down('#injectionEquipmentValueItemId').setValue(Ext.util.Format.number(count.getValue() * value, '000,000'))
                                                
                                                view.getController().onChangeData(key, newValue, oldValue, eOpts)
                                            }
                                        }
                                    }, {
                                        xtype: 'combobox',
                                        store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedInjStore',
                                        itemId: 'injectionWellScheduleItemId',
                                        displayField: 'displayField',
                                        valueField: 'valueField',                                                                               
                                        name: 'injectionWellSchedule',
                                        queryMode: 'local',
                                        allowBlank: true,
                                        tabIndex: 14,
                                        width: '49%',
                                        listeners:{
                                            focus: function(){
                                                this.expand();
                                            },
                                            change: function(key, newValue, oldValue, eOpts){
                                                var view = key.up('form');
                                                var count = view.down('#injectionEquipmentCountItemId')
                                                var combo = view.down('#injectionWellScheduleItemId');
                                                var comboStore = combo.getStore();
                                                var value = 0;
                                                var index = comboStore.find('valueField', combo.getValue());
                                                if(index != -1){
                                                    value = comboStore.getAt(index).get('salvageValue')
                                                }
                                                view.down('#injectionEquipmentValueItemId').setValue(Ext.util.Format.number(count.getValue() * value, '000,000'))
                                                
                                                view.getController().onChangeData(key, newValue, oldValue, eOpts)
                                            }
                                        }
                                    }, {
                                        name: 'injectionEquipmentValue',
                                        itemId: 'injectionEquipmentValueItemId',
                                        maskRe: /[0-9.]/,
                                        readOnly: true,
                                        fieldStyle: 'color: #000;',
                                        width: '15%'
                                    }]
                        },{
                            defaults: {
                                xtype: 'textfield',
                                enableKeyEvents: true,
                                margin: '0 2 0 2',
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },
                            items: [{
                                    xtype: 'displayfield',
                                    fieldStyle: 'text-align: right;',
                                    value: 'Max Years Life:',
                                    width: '20%',
                                }, {
                                    name: 'maxYearLife',
                                    maskRe: /[0-9.]/,
                                    tabIndex: 15,
                                    itemId: 'maxYearItemId',
                                    width: '15%'
                                }, {
                                    xtype: 'displayfield',
                                    fieldStyle: 'text-align: right;',
                                    value: 'Discount:',
                                    width: '49%'
                                }, {
                                    name: 'discountRate',
                                    xtype: 'numberfield',
                                    minValue: 0,
                                    maxValue: 100,
                                    // maskRe: /[0-9.]/,
                                    tabIndex: 16,
                                    width: '15%',
                                    listeners: {
                                        validitychange: function(field, isValid, eOpts){
                                            var label = field.previousSibling().el.dom;
                                            var cls = 'appraisalform-invalid-cont';
                                            if(!isValid && label){
                                                if(!label.classList.contains(cls)){
                                                    label.classList.add(cls);
                                                }
                                            }
                                            else if(label){
                                                if(label.classList.contains(cls)){
                                                    label.classList.remove(cls);
                                                }
                                            }
                                        },
                                        change: 'onChangeData'
                                    }
                            }]
                        },{
                            defaults: {
                                xtype: 'textfield',
                                enableKeyEvents: true,
                                margin: '0 2 0 2',
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },
                            items: [{
                                    xtype: 'displayfield',
                                    fieldStyle: 'text-align: right;',
                                    value: 'Oil W-H Price:',
                                    width: '20%'
                                }, {
                                    name: 'grossOilPrice',
                                    maskRe: /[0-9.]/,
                                    tabIndex: 17,
                                    width: '15%'    
                                }, {
                                    xtype: 'displayfield',
                                    fieldStyle: 'text-align: right;',
                                    value: 'Product Price:',
                                    width: '49%'
                                }, {
                                    name: 'grossProductPrice',
                                    maskRe: /[0-9.]/,
                                    tabIndex: 18,
                                    width: '15%'
                            }]
                        },{
                            defaults: {
                                xtype: 'textfield',
                                enableKeyEvents: true,
                                width: '24.5%',
                                margin: '0 2 0 2',
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },
                            items: [{
                                    xtype: 'displayfield',
                                    fieldStyle: 'text-align: right;',
                                    value: 'Gas WI Price:',
                                    width: '20%'
                                }, {
                                    name: 'grossWorkingInterestGasPrice',
                                    maskRe: /[0-9.]/,
                                    tabIndex: 19,
                                    width: '15%'
                                }, {
                                    xtype: 'displayfield',
                                    fieldStyle: 'text-align: right;',
                                    value: 'Gas RI Price:',
                                    width: '49%'
                                }, {
                                    name: 'grossRoyaltyInterestGasPrice',
                                    maskRe: /[0-9.]/,
                                    tabIndex: 20,
                                    width: '15%'
                            }]
                        }]                                               
                    },{
                        xtype: 'fieldset',
                        padding: '0 5 5 5',
                        margin: '8 0 0 0',
                        title: '',
                        layout: 'hbox',
                        flex: 1,
                        itemId: 'declineTypeItemId',
                        defaults: {
                            xtype: 'container',     
                            layout: 'vbox',
                            widht: '24.4%',
                            margin: '0 1 0 1',
                            flex: 1,
                        },
                        items: [{
                            defaults:{
                                margin: '0 2 0 2',    
                                flex: 1,
                            },
                            items:[{
                                xtype: 'displayfield',
                                width: '100%',
                                fieldStyle: 'text-align: center;',                                
                                value: 'Operating Exp',
                            },{
                                xtype: 'textfield',
                                name: 'operatingExpense',
                                itemId: 'operatingExpenseItemId',
                                enableKeyEvents: true,
                                width: '100%',
                                margin: '-10 2 0 2',
                                maskRe:/[0-9.]/,
                                tabIndex: 21,
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },{
                                xtype: 'grid',
                                width: '100%',
                                store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalOperatingExpStore',
                                plugins: [Ext.create('Ext.grid.plugin.CellEditing', {
                                    clicksToEdit: 1
                                })],
                                listeners: {
                                    beforeedit: 'onValidateDeclineBeforeEdit',
                                    edit: 'onValidateDecline'
                                },
                                columns: {
                                    defaults: {
                                        editor: {
                                            allowBlank: false,
                                            xtype: 'numberfield',
                                            // maskRe:/[0-9.]/,
                                            minValue: 0,
                                            maxValue: 100,                                        
                                        },
                                        sortable: false 
                                    },    
                                    items: [{
                                        header: 'Decline',
                                        flex: 1,
                                        dataIndex: 'decline',
                                    },{
                                        header: 'Year',
                                        flex: 1,
                                        dataIndex: 'declineYears',
                                    }]
                                },
                            }]    
                        },{
                            defaults:{
                                margin: '0 2 0 2',
                                flex: 1,
                            },
                            items:[{
                                xtype: 'displayfield',
                                width: '100%',
                                fieldStyle: 'text-align: center;',
                                value: 'Oil Daily Avg',                               
                            },{
                                xtype: 'textfield',
                                name: 'dailyAverageOil',
                                itemId: 'dailyAverageOilItemId',
                                enableKeyEvents: true,
                                width: '100%',
                                margin: '-10 2 0 2',
                                maskRe:/[0-9.]/,
                                tabIndex: 22,
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },{
                                xtype: 'grid',
                                width: '100%',
                                store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalOilDailyAvgStore',
                                plugins: [Ext.create('Ext.grid.plugin.CellEditing', {
                                    clicksToEdit: 1
                                })],
                                listeners: {
                                    beforeedit: 'onValidateDeclineBeforeEdit',
                                    edit: 'onValidateDecline'
                                },
                                columns: {
                                    defaults: {
                                        editor: {
                                            allowBlank: false,
                                            xtype: 'numberfield',
                                            // maskRe:/[0-9.]/,
                                            minValue: 0,
                                            maxValue: 100
                                        }, 
                                        sortable: false
                                    },    
                                    items: [{
                                        header: 'Decline',
                                        flex: 1,
                                        dataIndex: 'decline',
                                    },{
                                        header: 'Year',
                                        flex: 1,
                                        dataIndex: 'declineYears',
                                    }]
                                },
                            }]    
                        },{
                            defaults:{
                                margin: '0 2 0 2',
                                flex: 1,
                            },
                            items:[{
                                xtype: 'displayfield',
                                width: '100%',
                                fieldStyle: 'text-align: center;',
                                value: 'Gas Daily Avg',
                            },{
                                xtype: 'textfield',
                                name: 'dailyAverageGas',
                                itemId: 'dailyAverageGasItemId',
                                enableKeyEvents: true,
                                width: '100%',
                                margin: '-10 2 0 2',
                                maskRe:/[0-9.]/,
                                tabIndex: 23,
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },{
                                xtype: 'grid',
                                width: '100%',
                                store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalGasDailyAvgStore',                                
                                plugins: [Ext.create('Ext.grid.plugin.CellEditing', {
                                    clicksToEdit: 1
                                })],      
                                listeners: {
                                    beforeedit: 'onValidateDeclineBeforeEdit',
                                    edit: 'onValidateDecline'
                                },
                                columns: {
                                    defaults: {
                                        editor: {
                                            xtype: 'numberfield',
                                            // maskRe:/[0-9.]/,
                                            minValue: 0,
                                            maxValue: 100
                                        }, 
                                        sortable: false
                                    },    
                                    items: [{
                                        header: 'Decline',
                                        flex: 1,
                                        dataIndex: 'decline',
                                    },{
                                        header: 'Year',
                                        flex: 1,
                                        dataIndex: 'declineYears',
                                    }]
                                },
                            }]    
                        },{
                            defaults:{
                                margin: '0 2 0 2',
                                flex: 1,
                            },
                            items:[{
                                xtype: 'displayfield',
                                width: '100%',
                                fieldStyle: 'text-align: center;',
                                value: 'Prod Daily Avg',
                            },{
                                xtype: 'textfield',
                                enableKeyEvents: true,
                                itemId: 'dailyAverageProductItemId',
                                name: 'dailyAverageProduct',
                                width: '100%',
                                margin: '-10 2 0 2',
                                maskRe:/[0-9.]/,
                                tabIndex: 24,
                                listeners: {
                                    change: 'onChangeData'
                                }
                            },{
                                xtype: 'grid',
                                width: '100%',
                                store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalProdDailyAvgStore',
                                plugins: [Ext.create('Ext.grid.plugin.CellEditing', {
                                    clicksToEdit: 1
                                })],
                                listeners: {
                                    beforeedit: 'onValidateDeclineBeforeEdit',
                                    edit: 'onValidateDecline'
                                },
                                columns: {
                                    defaults: {
                                        editor: {
                                            allowBlank: false,
                                            xtype: 'numberfield',
                                            // maskRe:/[0-9.]/,
                                            minValue: 0,
                                            maxValue: 100
                                        },
                                        sortable: false 
                                    },    
                                    items: [{
                                        header: 'Decline',
                                        flex: 1,
                                        dataIndex: 'decline',
                                    },{
                                        header: 'Year',
                                        flex: 1,
                                        dataIndex: 'declineYears',
                                    }]
                                },                                
                            }]    
                        }]
                                                   
                    },{
                        xtype: 'fieldset',
//                        padding: '0 5 5 5',
                        margin: '14 0 0 0',
                        title: '',
                        defaults: {
                            maskRe: /[^'\^]/,
                            xtype: 'container',
                            layout: 'hbox',
                            height: 32
                        },
                        items: [{
                                defaults: {
                                    xtype: 'displayfield',
                                    fieldStyle: 'text-align: center;',
                                    width: '12.85%'
                                },
                                items: [{
                                        value: '',
                                        width: '9%'   
                                    }, {
                                        value: 'Equipment',
                                    }, {
                                        value: 'Oil',
                                    }, {
                                        value: 'Gas',
                                    }, {
                                        value: 'Product',
                                    }, {
                                        value: 'Total',  
                                    }, {
                                        value: 'Prior Yr',
                                    }, {
                                        value: 'Wgt Pct',
                                    }]
                            }, {
                                defaults: {
                                    xtype: 'textfield',
                                    readOnly: true,
                                    fieldStyle: 'color: #000; text-align: right; padding-right: 3px;padding-left: 3px;',
                                    width: '12.85%',
                                    listeners: {
                                        change: 'onChangeData',
                                    }
                                },
                                items: [{
                                        xtype: 'displayfield',
                                        fieldStyle: 'text-align: right;',
                                        value: 'WI Value:',
                                        width: '9%' 
                                    }, {
                                        name: 'equipmentValue',
                                        maskRe: /[0-9.]/,                                       
                                    }, {
                                        name: 'workingInterestOilValue',
                                        itemId: 'workingInterestOilValueItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'workingInterestGasValue',
                                        itemId: 'workingInterestGasValueItemId',
                                        maskRe: /[0-9.]/,                                   
                                    }, {
                                        name: 'workingInterestProductValue',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'workingInterestTotalValue',
                                        itemId: 'workingInterestTotalValueItemId',
                                        fieldStyle: 'color: #000; font-weight: bold; text-align: right; padding-right: 3px;padding-left: 3px;',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'priorYrWorkingInterestTotalValue',
                                        maskRe: /[0-9.]/,    
                                    }, {
                                        name: 'workingInterestPercent',
                                        fieldStyle: 'text-align: left; padding-right: 3px;padding-left: 3px;',
                                        maskRe: /[0-9.]/, 
                                    }]
                            }, {
                                defaults: {
                                    xtype: 'textfield',
                                    readOnly: true,
                                    fieldStyle: 'color: #000; text-align: right; padding-right: 3px;padding-left: 3px;',
                                    width: '12.85%',
                                    listeners: {
                                        change: 'onChangeData',
                                    }
                                },
                                items: [{
                                        xtype: 'displayfield',
                                        fieldStyle: 'text-align: right;',
                                        value: 'RI Value:',
                                        width: '9%' 
                                    }, {
                                        name: 'equipmentRiValue',
                                        maskRe: /[0-9.]/,
                                        
                                    }, {
                                        name: 'royaltyInterestOilValue',
                                        itemId: 'royaltyInterestOilValueItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'royaltyInterestGasValue',
                                        itemId: 'royaltyInterestGasValueItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'royaltyInterestProductValue',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'royaltyInterestTotalValue',
                                        fieldStyle: 'color: #000; font-weight: bold; text-align: right; padding-right: 3px;padding-left: 3px;',
                                        itemId: 'royaltyInterestTotalValueItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'priorYrRoyaltyInterestTotalValue',
                                        maskRe: /[0-9.]/, 
                                    }, {
                                        name: 'royaltyInterestPercent',
                                        fieldStyle: 'text-align: left; padding-right: 3px;padding-left: 3px;',
                                        maskRe: /[0-9.]/,
                                    }]
                            },{
                                defaults: {
                                    xtype: 'textfield',
                                    readOnly: true,
                                    fieldStyle: 'color: #000; text-align: right; padding-right: 3px;padding-left: 3px;',
                                    width: '12.85%',
                                    listeners: {
                                        change: 'onChangeData',
                                    }
                                },
                                items: [{
                                        xtype: 'displayfield',
                                        fieldStyle: 'text-align: right;',
                                        value: 'Total:',
                                        width: '9%' 
                                    }, {
                                        name: 'equipmentTotal',
                                        maskRe: /[0-9.]/,
                                        
                                    }, {
                                        name: 'totalInterestOilValue',
                                        itemId: 'totalInterestOilValueItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'totalInterestGasValue',
                                        itemId: 'totalInterestGasValueItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'totalProductValue',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'totalInterestValue',
                                        itemId: 'totalInterestValueItemId',
                                        fieldStyle: 'color: #000; font-weight: bold; text-align: right; padding-right: 3px;padding-left: 3px;',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'priorYrTotalInterestValue',
                                        maskRe: /[0-9.]/,     
                                    }, {
                                        name: 'totalInterestPercent',
                                        fieldStyle: 'text-align: left; padding-right: 3px;padding-left: 3px;',
                                        maskRe: /[0-9.]/,   
                                    }]
                            },{
                                margin: '10 0 5 0',
                                defaults: {
                                    xtype: 'textfield',
                                    readOnly: true,
                                    fieldStyle: 'color: #999966; text-align: right; padding-right: 3px;padding-left: 3px;',
                                    width: '12.85%',
                                    listeners: {
                                        change: 'onChangeData',
                                    }
                                },
                                items: [{
                                        xtype: 'displayfield',
                                        fieldStyle: 'text-align: right;',
                                        value: 'Reserves:',
                                        width: '9%' 
                                    }, {
                                        name: 'equipmentReserve',
                                        maskRe: /[0-9.]/,
                                        
                                    }, {
                                        name: 'accumOilProduction',
                                        itemId: 'accumOilProductionItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'accumGasProduction',
                                        itemId: 'accumGasProductionItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'accumProductProduction',
                                        maskRe: /[0-9.]/, 
//                                    }, {
//                                        name: 'accumTotalProduction',
//                                        itemId: 'accumTotalProductionItemId',
//                                        maskRe: /[0-9.]/,
//                                    }, {
//                                        name: 'priorYearReserve',
//                                        maskRe: /[0-9.]/,  
//                                    }, {
//                                        name: 'weightPctReserve',
//                                        maskRe: /[0-9.]/,   
                                    }]
                            }]
                    }]
            }];

        var appraisalIndex = {
            xtype: 'label',
            style: 'color:white;',
            itemId: 'appraisalIndexItemId',
            text: '0 of 0',
            margin: '10 10 0 0'
        }

        var newAppraisalButton = {
            xtype: 'button',
            tooltip: Minerals.config.Runtime.appraisalNewButtonTooltip,
            text: Minerals.config.Runtime.appraisalNewButtonText,
            iconCls: Minerals.config.Runtime.appraisalNewButtonIcon,
            itemId: 'newAppraisalButton',
            //disabled: true,
            listeners: {
                click: 'onNewAppraisalButton',
                afterrender: function () {
                    var me = this
                    me.setHidden(MineralPro.config.Runtime.access_level)
                }
            }
        }

        var makeValue0 = {
            xtype: 'button',
            tooltip: 'Make Value $0',
            text: 'MakeValue $0',
            //iconCls: '',
            itemId: 'makeValue0',
            //disabled: true,
            listeners: {
                click: 'onMakeValue0',
                afterrender: function () {
                    var me = this
                    me.setHidden(MineralPro.config.Runtime.access_level)
                }
            }
        }

        var nextButton = {
            xtype: 'button',
            tooltip: Minerals.config.Runtime.appraisalNextButtonTooltip,
            text: Minerals.config.Runtime.appraisalNextButtonText,
            iconCls: Minerals.config.Runtime.appraisalNextButtonIcon,
            alwaysShow: true,
            listeners: {
                click: 'onNextButton',
            }
        }

        var previousButton = {
            xtype: 'button',
            tooltip: Minerals.config.Runtime.appraisalPreviousButtonTooltip,
            text: Minerals.config.Runtime.appraisalPreviousButtonText,
            iconCls: Minerals.config.Runtime.appraisalPreviousButtonIcon,
            alwaysShow: true,
            listeners: {
                click: 'onPreviousButton',
            }
        }

        var calcButton = {
            xtype: 'button',
            itemId: 'calcButtonItemId',
            tooltip: Minerals.config.Runtime.appraisalCalcButtonTooltip,
            text: Minerals.config.Runtime.appraisalCalcButtonText,
            iconCls: Minerals.config.Runtime.appraisalCalcButtonIcon,
            alwaysShow: true,
            listeners: {
                click: 'onCalcButton',
            }
        }

        var yearByYearAnalysisButton = {
            xtype: 'button',
            itemId: 'yearByYearAnalysisButtonItemId',
            tooltip: Minerals.config.Runtime.yearByYearAnalysisButtonTooltip,
            text: Minerals.config.Runtime.yearByYearAnalysisButtonText,
//            iconCls: Minerals.config.Runtime.yearByYearAnalysisButtonIcon,
//            disabled: true,
            alwaysShow: true,
            listeners: {
                click: 'onYearByYearAnalysisButton',
            }
        }

        me.tools = [{
                xtype: 'container',
                width: '50%',
                layout: {
                    type: 'hbox',
                },
                items: [
                    appraisalIndex,
                    previousButton,
                    nextButton,
                    newAppraisalButton,
                    makeValue0
                    
                ]
            }, {
                xtype: 'container',
                width: '50%',
                layout: {
                    type: 'hbox',
                    pack: 'end'
                },
                items: [
                    yearByYearAnalysisButton,
                    calcButton,
                    MineralPro.config.util.MainGridToolCmpt.saveToDbButton,
                ]
            }];

        me.callParent(arguments);
    },
    listeners : {
        afterRender: function(thisForm, options){
            var me = this;
            me.keyNav = Ext.create('Ext.util.KeyNav', me.el, {
                'tab': {
                   fn: function (e) {
                        setTimeout(function (){
                                       
                            var activeElement = Ext.get(Ext.Element.getActiveElement());
                            var tabCur = activeElement.dom.tabIndex;
                            if(tabCur > 0){
                                
                            } else { 
                                var mb = Ext.Msg.show({
                                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                                    title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                                    msg:"You're not allowed to exit the form",              
                                    buttons: Ext.Msg.OK,
                                    fn: function(){
                                        me.down("#rrcNumberItemId").focus();
                                    }
                                });
                            }
                                       
                        },200)
                    }
                },
                scope: me
            });
        }
    }
});    