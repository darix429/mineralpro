Ext.define('Minerals.sub.MineralValuationManageAppraisal.view.AppraisalHeaderView', {
    extend: 'MineralPro.config.view.BaseHeaderView',
    alias: 'widget.AppraisalHeaderView',
    layout: 'hbox',
    defaults: {
        style: {borderColor: '#000000', borderStyle: 'solid', borderWidth: '1px'},
        xtype: 'fieldset',
        layout: 'vbox',
        width: '24%',
        height: 95,
        margin: '0, 2, 0, 2',
        cls: 'header_fieldset',
    },
    items: [{
            title: 'Appraisal Information',
            width: '100%',
            layout: 'hbox',
            defaults: {
                xtype: 'container',
                layout: 'vbox',
//                width: '50%',
            },
            items: [{
                    width: '49%',
                    defaults: {
                        xtype: 'displayfield',
                        fieldStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:5px',
                        labelStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px',
                        margin: '0 0 0 2',
                        padding: '0 0 0 0',
                        width: '100%',
                        labelWidth: 110,
                    },
                    items: [{
                            fieldLabel: 'Lease/Unit Name:',
                            name: 'leaseUnitIdName',
                        }, {
                            fieldLabel: 'RRC Number:',
                            name: 'rrcNumber',                    
                        }]
                }, {
                    width: '49%',
                    defaults: {
                        xtype: 'displayfield',
                        fieldStyle: 'font-weight:normal;margin:0px;padding:0px;font-size:12px;min-height:5px',
                        labelStyle: 'font-weight:normal;margin:0px;padding:0px 0px 0px 20px;font-size:12px',
                        margin: '0 0 0 2',
                        padding: '0 0 0 0',
                        width: '100%',
                        labelWidth: 100,
                    },
                    items: [{
                            fieldLabel: 'Operator:',
                            name: 'operatorIdName'
                        }, {
                            fieldLabel: 'Agent:',
                            name: 'agencyName'
                        }]
                }]
        }]
});    