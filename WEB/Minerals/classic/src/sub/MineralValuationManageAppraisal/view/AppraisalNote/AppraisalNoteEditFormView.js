Ext.define('Minerals.sub.MineralValuationManageAppraisal.view.AppraisalNote.AppraisalNoteEditFormView', {
    extend: 'Minerals.sub.MineralValuationManageAppraisal.view.AppraisalNote.AppraisalNoteAddFormView',
    //requires: ['Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteEditFormViewController'],
    //controller: 'LeaseNoteEditFormViewController',
    alias: 'widget.AppraisalNoteEditFormView',
    
    
    title: 'Update Appraisal Note',
    // defaultFocus: '#noteId',
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});