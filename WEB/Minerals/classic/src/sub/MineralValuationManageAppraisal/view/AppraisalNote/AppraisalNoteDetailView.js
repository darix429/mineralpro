Ext.define('Minerals.sub.MineralValuationManageAppraisal.view.AppraisalNote.AppraisalNoteDetailView', {
    extend: 'MineralPro.config.view.BaseDetailView',
    alias: 'widget.AppraisalNoteDetailView',
    
    title: 'Note Details',
    itemId: 'AppraisalNoteDetail',
    defaults: {
        width: 275,
        labelWidth: 93
    },   
    items: [{
        xtype: 'textareafield',
        fieldLabel:'Note',
        name: 'note',
        anchor: '100%',
        labelWidth: 80,
        grow: true, 
        readOnly: true 
    }]
});