Ext.define('Minerals.sub.MineralValuationManageAppraisal.view.AppraisalNote.AppraisalNoteView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.AppraisalNotesView',
    layout:'border',
    
    id: 'AppraisalNotesViewId',
    
    items: [{
            xtype: 'AppraisalNoteGridView',
            html:'center',
            region:'center'
        },{
        	xtype: 'AppraisalNoteDetailView',
        	region: 'east'
        }] 
});    