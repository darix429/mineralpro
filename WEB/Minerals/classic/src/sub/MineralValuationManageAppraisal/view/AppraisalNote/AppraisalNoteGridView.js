
Ext.define('Minerals.sub.MineralValuationManageAppraisal.view.AppraisalNote.AppraisalNoteGridView', {
//    extend: 'Minerals.config.view.Note.BaseNoteGridView',
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.AppraisalNoteGridView',
    store:'Minerals.sub.MineralValuationManageAppraisal.store.AppraisalNoteStore',

    // requires: ['Minerals.sub.MineralValuationManageLease.view.LeaseNote.LeaseNoteGridViewController'],
    // controller: 'LeaseNoteGridViewController',     
          
    //below are default items needed for grid checking 
    // firstLoadItemId: 'dateItemId', 
    mainPanelAlias: 'MineralValuationManageAppraisal', //widget alias name of the main panel
    mainListModel: 'Minerals.model.MineralValuation.NotesModel',     

    addFormAliasName: 'AppraisalNoteAddFormView', //widget form to call for add list 
    editFormAliasName: 'AppraisalNoteEditFormView', //widget form to call for edit
   

    // firstFocus: 'dateItemId',    
    
    addButtonTooltip: 'Add New Note',
    addButtonText: 'Add New Note',
    
    fieldGridViewAlias: 'AppraisalListGridView', 
        
     columns:[{   
        header: 'Ref Year ',
        flex     : 2,
        dataIndex: 'referenceAppraisalYear',
        itemId: 'referenceAppraisalYearItemId',
        filterElement: new Ext.form.TextField({
        })
    },{   
        header: 'Create Date',
        flex     : 2,
        dataIndex: 'createDt',
        itemId: 'dateItemId',
        filterElement: new Ext.form.TextField({
        })
    },{   
        header: 'User',
        flex     : 2,
        dataIndex: 'noteAppraiserName',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'RRC Number',
        flex     : 2,
        dataIndex: 'rrcNumber',
        itemId:'rrcNumberItemId',
        filterElement:new Ext.form.TextField() 
    },{      
        header: 'Note',
        flex     : 6,
        dataIndex: 'note',
        cls: 'noteCls',
        tdCls: 'noteTdCls',
        filterElement:new Ext.form.TextField() 
    },{   
        header: 'Confidential',
        flex     : 2,
        dataIndex: 'securityCd',
        renderer: function(value){
            if(value == 2){
                return 'Yes';
            }else if(value == 1){
                return 'No';
            }
        },
        filterElement : new Ext.form.ComboBox({                              
            showFilterIcon:true,
            triggerAction           : 'all',                
            typeAhead               : true,                                
            mode                    : 'local',
            listWidth               : 160,
            hideTrigger             : false,
            emptyText               : 'Select',
            store                   :[['2','Yes'],['1','No']]
        })
    },{      
        header: 'Sequence No.',
        flex     : 2,
        dataIndex: 'seqNumber',
        filterElement:new Ext.form.TextField()     
    },{
     xtype: 'actioncolumn',
        width: Minerals.config.Runtime.actionColWidth,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls,
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        },
        renderer : function (v, cellValues, record, rowIdx, colIdx, store, view){
            var me = this
            var selectedYear = MineralPro.config.Runtime.appraisalYear
            var refAppraisalYear = record.data.referenceAppraisalYear
            if(refAppraisalYear*1 == selectedYear*1 || record.dirty){
                record.data.editRecord = true
                record.data.deleteRecord = true
            } else {
                record.data.editRecord = false
                record.data.deleteRecord = false
            }
        }
    }],
    
    initComponent: function () {  
        var me = this;
      
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')];      
        
        if(me.up('tabpanel').selectedRRCNumber != null){
           
                var noteStore = Ext.StoreMgr.lookup(me.store);                             
                noteStore.on({
                    load: function(store, records, successful, operation, eOpts ){
                            var mainPanel = me.up(me.mainPanelAlias);
                            me.down('#rrcNumberItemId').setText('RRC Number: ' + mainPanel.selectedRRCNumber);                            
                    },
                });
                
                noteStore.on({
                    beforeload: function (s) {
                        var mainPanel = Ext.getCmp('MineralValuationManageAppraisalId')
                        var params = this.getProxy().extraParams
                        params['subjectTypeCd'] = mainPanel.subjectTypeCd
                        params['selectedRRCNumber'] = mainPanel.selectedRRCNumber
                        this.getProxy().extraParams = params;
                    }
                });

                noteStore.on({
                    beforesync: function () {
                        var mainPanel = Ext.getCmp('MineralValuationManageAppraisalId')
                        var params = this.getProxy().extraParams
                        params['idAppUser'] = MineralPro.config.Runtime.idAppUser;
                        params['subjectTypeCd'] = mainPanel.subjectTypeCd
                        this.getProxy().extraParams = params;
                    },
                })
        }
        
        var rrcNumber = {
            xtype: 'label',
            style: 'color: white;',
            itemId: 'rrcNumberItemId',
            text: 'RRC Number: None',
            margin: '10 10 0 0'
        }
                     
        
        me.tools = [{
            xtype: 'container',
            width: '25%',
            layout: {
                type: 'hbox',                   
            },
            items: [ 
                rrcNumber
            ]
        },{
            xtype: 'container',
            width: '75%',
            layout: {
                type: 'hbox',    
                pack: 'end'
            },
            items: [
                MineralPro.config.util.MainGridToolCmpt.addNewDataButton,
                MineralPro.config.util.MainGridToolCmpt.saveToDbButton,                  
                MineralPro.config.util.MainGridToolCmpt.clearFilterButton
            ]
        }];
                 
        me.callParent(arguments);
        
    },
    
    listeners: {
        selectionchange: function(model, record){
            record = record[0];
            var view = this;
            var detailsView = view.nextSibling('AppraisalNoteDetailView');
            if(record){
                detailsView.loadRecord(record);
            }
            else{
                detailsView.reset();
            }
        },
        afterrender: function(){
            var me = this
            if(me.up('tabpanel').selectedRRCNumber == null){
                me.down('#rrcNumberItemId').hide();
            }else{
                me.down('#rrcNumberItemId').show();              
            }
        },
        viewready: function (grid) {
        var view = grid.view;
        this.toolTip = Ext.create('Ext.tip.ToolTip', {
            target: view.el,
            delegate: view.itemSelector + ' .noteTdCls',
            //delegate: view.cellSelector, //all columns
            trackMouse: true,
            renderTo: Ext.getBody(),
            listeners: {
                beforeshow: function(tip) {
                    var trigger = tip.triggerElement,
                        parent = tip.triggerElement.parentElement,
                        columnTitle = view.getHeaderByCell(trigger).text,
                        columnDataIndex = view.getHeaderByCell(trigger).dataIndex;
                
                    if(view.getRecord(parent)){
                        var columnText = view.getRecord(parent).get(columnDataIndex).toString();                   
                        if (columnText){
                            tip.update("<b>" + columnTitle + ":</b> " + columnText);
                        } else {
                            return false;
                        }
                    }else{
                        return false;
                    }                                                         
                }
            }
        });
        }
    }
});