//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.MineralValuationManageAppraisal.view.AppraisalList.AppraisalListGridViewController', {
    extend: 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.AppraisalListGridViewController',
    
    
     onOpenAddListForm: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalListGridViewController onOpenAddListForm.')
        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        mainPanel.newAppraisal = true;     
        mainPanel.setActiveTab(parseInt(mainPanel.appraisalDetailsTab));

        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalListGridViewController onOpenAddListForm.')
    },
    /**
     * Open up the Edit form for editing
     */
    onOpenEditListForm: function (grid, rowIndex, colIndex) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalListGridViewController onOpenEditListForm.')
        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        mainPanel.setActiveTab(parseInt(mainPanel.appraisalDetailsTab));
          
        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalListGridViewController onOpenEditListForm.')
    },
    
    onLoadSelectionChange: function (model, records) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalListGridViewController onLoadSelectionChange.')
        var me = this;

        if (!me.xtype) {
            me = me.getView();
        }

        var mainPanel = me.up(me.mainPanelAlias);
        var parentTab = false;
        var showHeader = false;
        if (mainPanel.selectedId != null) {
            parentTab = true;
        }
        if (parentTab) {
            if (me.withChildTab) {
                if (records.length > 0) {
                    mainPanel.selectedId = records[0].get(mainPanel.selectedIdIndex);
                    mainPanel.selectedRRCNumber = records[0].get(mainPanel.selectedRRCNumberIndex);
                    mainPanel.selectedName = records[0].get(mainPanel.selectedNameIndex);
                    mainPanel.subjectTypeCd = records[0].get('subjectTypeCd')
                    var headerDetailStore = Ext.StoreMgr.lookup(me.headerInfoStore);
                        headerDetailStore.getProxy().extraParams = {
                            selectedId: mainPanel.selectedId,
                            selectedRRCNumber: mainPanel.selectedRRCNumber,
                            idAppUser: MineralPro.config.Runtime.idAppUser,
                            subjectTypeCd: mainPanel.subjectTypeCd
                        };
                    headerDetailStore.load({
                          callback: function(rec){
                            if(rec.length>0){
                                showHeader = true; 
                                me.headerDetail = rec[0]
                            }
                            updateHeader(me, rec, showHeader);
                            if(me.showEdit){
                                me.getController().showEditViewViaHeader();
                            }
                        }
                    }); 
                }      
                  
                
            }
        }
        
        function updateHeader (me, records, showHeader){    
            MineralPro.config.Runtime.headerInfoXtype='';
            if(me.headerInfo && me.headerInfo.length>0 && records.length > 0 && showHeader){
                var headerInfo = new Ext.create(me.headerInfo);
                headerInfo.loadRecord(records[0])          
               MineralPro.config.Runtime.headerInfoXtype=headerInfo                
          }              
           Ext.globalEvents.fireEvent('updateHeader');
        } 
        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalListGridViewController onLoadSelectionChange.')
    },

    onOpenSelectedTabView: function (record, rowIndex) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalListGridViewController onOpenSelectedTabView.')
        var me = this;
        var centerView = me.getView().up('centerView');
        var westView = centerView.up().down('westView');
        var favoriteTree = westView.down('#favoriteTreeId');
        var menuTree = westView.down('#menuTreeId');
        var dataTree = true;
        var index = 0;
        var subjectType = record.data.subjectType;

        if(subjectType == 'Unit'){
            favoriteTree.getStore().each(function(c, fn){
                if(c.data.functionName == 'Manage Unit'){
                    index = fn;
                    dataTree=false;
                }
            })            
            menuTree.getStore().each(function(c, fn){
                if(c.data.functionName == 'Manage Unit'){
                    index = fn;
                    dataTree=true;
                }
            })
            
            if(dataTree == true){
                menuTree.getSelectionModel().select(index);
                setTimeout(function () {
                    var tabPanel = centerView.down('MineralValuationManageUnit');
                    var grid = centerView.down('#UnitsViewId').down('grid');
                    var hasFilterAllRowsPlugin = grid.getPlugin('filterAllRows');
                    var cols = grid.columns;
                    
                    tabPanel.setActiveTab(0);
                    Ext.each(cols, function (col) {
                        if(col.dataIndex == 'unitName'){
                            col.filterElement.setValue(record.data.name.trim());
                        }
                    });
                    var hasFilterAllRowsPlugin = grid.getPlugin('filterAllRows');
                    if(hasFilterAllRowsPlugin){
                        grid.fireEvent('runFilter');
                    }
                },800 )
            }else{
                favoriteTree.getSelectionModel().select(index);
                setTimeout(function () {
                    var tabPanel = centerView.down('MineralValuationManageUnit');
                    var grid = centerView.down('#UnitsViewId').down('grid');
                    var cols = grid.columns;

                    tabPanel.setActiveTab(0);
                    Ext.each(cols, function (col) {
                        if(col.dataIndex == 'unitName'){
                            col.filterElement.setValue(record.data.name.trim());
                        }
                    });
                    var hasFilterAllRowsPlugin = grid.getPlugin('filterAllRows');
                    if(hasFilterAllRowsPlugin){
                        grid.fireEvent('runFilter');
                    }
                },800 )
            }
        }else{
            favoriteTree.getStore().each(function (c, fn) {
                if (c.data.functionName == 'Manage Lease') {
                    index = fn;
                    dataTree = false;
                }
            })
            menuTree.getStore().each(function (c, fn) {
                if (c.data.functionName == 'Manage Lease') {
                    index = fn;
                    dataTree = true;
                }
            })

            if (dataTree == true) {
                menuTree.getSelectionModel().select(index);
                setTimeout(function () {
                    var tabPanel = centerView.down('MineralValuationManageLease');
                    var grid = centerView.down('#MineralsLeasesId').down('grid');
                    var cols = grid.columns;

                    tabPanel.setActiveTab(0);
                    Ext.each(cols, function (col) {
                        if (col.dataIndex == 'leaseName') {
                            col.filterElement.setValue(record.data.name.trim());
                        }
                    });
                    var hasFilterAllRowsPlugin = grid.getPlugin('filterAllRows');
                    if(hasFilterAllRowsPlugin){
                        grid.fireEvent('runFilter');
                    }
                }, 800)
            } else {
                favoriteTree.getSelectionModel().select(index);
                setTimeout(function () {
                    var tabPanel = centerView.down('MineralValuationManageLease');
                    var grid = centerView.down('#MineralsLeasesId').down('grid');
                    var cols = grid.columns;

                    tabPanel.setActiveTab(0);
                    Ext.each(cols, function (col) {
                        if (col.dataIndex == 'leaseName') {
                            col.filterElement.setValue(record.data.name.trim());
                        }
                    });
                    var hasFilterAllRowsPlugin = grid.getPlugin('filterAllRows');
                    if(hasFilterAllRowsPlugin){
                        grid.fireEvent('runFilter');
                    }
                }, 800)
            }
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalListGridViewController onOpenSelectedTabView.')
    },
    
    onOpenMassCalculation: function (grid, rowIndex, colIndex) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalListGridViewController onOpenMassCalculation.')
        
        var me = this;
        var mainView = me.getView();  
        var massCalcView = Ext.widget(mainView.massCalcAliasName);     
        var cols = mainView.columns;

        Ext.each(cols, function (col) {
        if (col.filterElement)
            col.filterElement.setValue('');
        });
            
        massCalcView.show()
        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalListGridViewController onOpenMassCalculation.')
    },
});