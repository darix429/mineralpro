
Ext.define('Minerals.sub.MineralValuationManageAppraisal.view.AppraisalList.AppraisalListGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.AppraisalListGridView',
    store:'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalStore',

    requires: ['Minerals.sub.MineralValuationManageAppraisal.view.AppraisalList.AppraisalListGridViewController'],
    controller: 'AppraisalListGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'rrcNumberItemId', 
    mainPanelAlias: 'MineralValuationManageAppraisal', //widget alias name of the main panel
    mainListModel: 'Minerals.model.MineralValuation.AppraisalModel',   
      
    headerInfo: 'Minerals.sub.MineralValuationManageAppraisal.view.AppraisalHeaderView',
    headerInfoAlias: 'AppraisalHeaderView',
    headerInfoStore: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalHeaderStore',
  
    calcAppraisalStore: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalCalcStore',
      
    massCalcAliasName: 'AppraisalListMassCalcView', 
    
    withChildTab: true,

    firstFocus: 'rrcNumberItemId',    
    
    addButtonTooltip: 'Add New Appraisal',
    addButtonText: 'Add New Appraisal',
        
    columns:[{   
        header: 'RRC Number',
        flex     : 1,
        dataIndex: 'rrcNumber',
        itemId: 'rrcNumberItemId',   
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        }),
    },{   
        header: 'Appraisal Year',
        flex     : 1,
        dataIndex: 'appraisalYear',
        itemId: 'appraisalYearItemId',
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        })
     },{   
        header: 'Subject Type',
        flex     : 1,
        dataIndex: 'subjectType',
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        }),
    },{   
        header: 'Subject Id',
        flex     : 1,
        dataIndex: 'subjectId',    
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        }),   
    },{   
        header: 'Subject Name',
        flex     : 2,
        dataIndex: 'name', 
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        }),
        renderer: function (value) {
                        return '<a href="javascript:void(0);" >' + value + '</a>';
                    },
        listeners: {
            click: function (cell, td, rowIndex, cellIndex, e, record, value, index) {
                   this.up('grid').getController().onOpenSelectedTabView(record, rowIndex);
            }
        }
    },{   
        header: 'Field Name',
        flex     : 2,
        dataIndex: 'fieldName',  
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        }),
    },{   
        header: 'Operator Name',
        flex     : 2,
        dataIndex: 'operatorName',  
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        }),   
//    },{   
//        header: 'Gravity Level',
//        flex     : 1,
//        dataIndex: 'gravityDesc',  
//        filterElement: new Ext.form.TextField(),
    },{   
        header: 'Total Appraised Value',
        flex     : 2,
         align: 'right',
        dataIndex: 'totalValue',   
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        }),
        renderer: function(val, meta) {
            return  Ext.util.Format.number(val, '$000,000');
        },
    },{   
        header: 'Last Appraised Year',
        flex     : 1,
        dataIndex: 'lastAppraisalYear',   
        filterElement: Ext.create('Ext.form.field.Text',{
            enableKeyEvents: true
        })
    },{
     xtype: 'actioncolumn',
        width: Minerals.config.Runtime.actionColWidth,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls,
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    }],
 
    initComponent: function () {  
        var me = this;            
        var mainPanel = me.up(me.mainPanelAlias);
       // me.selModel= Ext.create('Ext.selection.CheckboxModel',{
       //     checkOnly: true,
       // }), 
        
        // me.plugins = [Ext.create('Ext.ux.grid.FilterRowMatch')]    
        me.plugins = [Ext.create('Ext.ux.grid.FilterAllRows',{
            pluginId: 'filterAllRows'
        })];

        var massCalculation = {
            xtype: 'button',
            itemId: 'massCalculationItemId',
            tooltip: 'Mass Calculation',
            text: 'Mass Calculation',
            iconCls: Minerals.config.Runtime.appraisalCalcButtonIcon,
            listeners: {
                click: 'onOpenMassCalculation',
                afterrender: function () {
                    var me = this
                    me.setHidden(MineralPro.config.Runtime.access_level)
                }
            }
        }       
        me.tools = [
            massCalculation,   
            MineralPro.config.util.MainGridToolCmpt.addNewDataButton,
            MineralPro.config.util.MainGridToolCmpt.saveToDbButton,                  
            // MineralPro.config.util.MainGridToolCmpt.clearFilterButton
        ];
        // var store = Ext.getStore(me.store);
        // store.setRemoteFilter(true);
        // store.on({
        //     beforeload: {
        //         fn: function(store, operation, eOpts){
        //             MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalListGridView initComponent attach load event beforeload.')
        //             Ext.getBody().mask('Loading Appraisals...');
        //             // console.log('-------------------------------BEFORE STORE IS LOADED START -------------------------------');
        //             // console.log('selectedId : '+mainPanel.selectedId);
        //             // console.log('selectedIdIndex : '+mainPanel.selectedIdIndex);
        //             // console.log('selectedName : '+mainPanel.selectedName);
        //             // console.log('selectedNameIndex : '+mainPanel.selectedNameIndex);
        //             // console.log('selectedRRCNumber : '+mainPanel.selectedRRCNumber);
        //             // console.log('selectedRRCNumberIndex : '+mainPanel.selectedRRCNumberIndex);
        //             // console.log('subjectTypeCd : '+mainPanel.subjectTypeCd);
        //             // console.log('-------------------------------- END --------------------------------');
                    
        //             var extraParams = store.getProxy().extraParams;
        //             var filterData = me.getFilterData();//getFilterData is bound on FilterAllRows Plugin
        //             var filterCount = Ext.Object.getValues(filterData).filter(function(val){return val.trim();}).length;
        //             // var filterCount = Object.values(filterData).filter(function(val){return val.trim();}).length;
        //             if(filterCount == 0){
        //                 Ext.getBody().unmask();
        //                 MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalListGridView initComponent attach load event canceled.')
        //                 if(store.getCount()>0){
        //                     console.log('commit changes')
        //                     store.commitChanges();
        //                 }
        //                 return false;
        //             }
        //             MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalListGridView initComponent attach load event continue to load.')
        //             extraParams['searchBy'] = me.down('cycle#searchCategory').getActiveItem().getValue();
        //         }
        //     },
        //     load: {
        //         fn: function(store, records, successful, operation, eOpts){
        //             MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalListGridView initComponent attach load event.')
        //             Ext.getBody().unmask();
                    
        //             // console.log('-------------------------------AFTER STORE IS LOADED START -------------------------------');
        //             // console.log('selectedId : '+mainPanel.selectedId);
        //             // console.log('selectedIdIndex : '+mainPanel.selectedIdIndex);
        //             // console.log('selectedName : '+mainPanel.selectedName);
        //             // console.log('selectedNameIndex : '+mainPanel.selectedNameIndex);
        //             // console.log('selectedRRCNumber : '+mainPanel.selectedRRCNumber);
        //             // console.log('selectedRRCNumberIndex : '+mainPanel.selectedRRCNumberIndex);
        //             // console.log('subjectTypeCd : '+mainPanel.subjectTypeCd);
        //             // console.log('-------------------------------- END --------------------------------');
                        
        //             var selectedIndex = store.findBy(function(rec){
        //                 return rec.get(mainPanel.selectedIdIndex) == mainPanel.selectedId && rec.get(mainPanel.selectedRRCNumberIndex) == mainPanel.selectedRRCNumber;
        //             });
        //             if(selectedIndex == -1){
        //                 console.log('------------------------------- START -------------------------------');
        //                 console.log('selected row does not exists. reset to default values.');
        //                 console.log('-------------------------------- END --------------------------------');
                            
        //                 selectedIndex = 0;
        //                 /*------------------------------ START ------------------------------
        //                 | Clear out selected Owner on mainPanel
        //                 */
        //                 me.getSelectionModel().deselectAll(true);
        //                 mainPanel.selectedId =  '0'; //Need default value '0'
        //                 mainPanel.selectedIdIndex =  'subjectId';
        //                 mainPanel.selectedName =  '0'; //Need default value '0'
        //                 mainPanel.selectedNameIndex =  'name';
        //                 /*
        //                 | Clear out selected Owner on mainPanel
        //                 | ------------------------------ END ------------------------------
        //                 */
        //             }
                    
        //             if(successful){
        //                 // var filterData = me.getFilterData();//getFilterData is bound on FilterAllRows Plugin
        //                 // // var filterCount = Ext.Object.getValues(filterData).filter(function(val){return val.trim();}).length;
        //                 // var filterCount = Object.values(filterData).filter(function(val){return val.trim();}).length;
        //                 // if(filterCount==0){
        //                 //     me.store.clearFilter(true);
        //                 // }
        //                 // if(records.length > 0 && me.selModel){
        //                 //     console.log(' ---- start selecting '+selectedIndex+' -----------------------------------');
        //                 //     me.getSelectionModel().deselectAll(true);
        //                 //     me.getSelectionModel().select(selectedIndex);
        //                 //     MineralPro.config.RuntimeUtility.DebugLog('END firing selectionchange');
        //                 // }
        //             }
        //             else{
        //                 var descriptiveErrorMsg = '';
        //                 if(operation.getError() && operation.getError().statusText){
        //                     descriptiveErrorMsg += '<p><span style="font-weight:bold;">Error Details: </span><span style="color:red;">'+operation.getError().statusText+'.</span></p>'
        //                 }
        //                 Ext.Msg.show({
        //                     title: 'Oops!',
        //                     msg: "Something went wrong while retrieving Appraisal's info."+descriptiveErrorMsg,
        //                     buttons: Ext.Msg.OK
        //                 });
        //             }
        //             MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalListGridView initComponent attach load event.')
        //         }
        //     }
        // })


        // -------------------------------------------------- END ------------------------------------------------------------------
        // var filterByExact = {
        //     xtype: 'checkbox',
        //     itemId: 'filterByExactItemId',
        //     listeners: {
        //         change: function(){
        //             me.fireEvent('runFilter');
        //         }
        //     }
        // } 
        // var filterLabel = {
        //     xtype: 'label',
        //     style: 'color:white;',
        //     text: 'Filter By Exact Match',
        //     margin: '10 10 0 0'
        // }

        // var addNewDataButton = Object.assign({},MineralPro.config.util.MainGridToolCmpt.addNewDataButton);//deep clone object instead of referencing the object
        // addNewDataButton.disabled = true;
        // me.tools = [{
        //         xtype: 'container',
        //         width: '25%',
        //         layout: {
        //             type: 'hbox',
        //         },
        //         items: [
        //             filterByExact,
        //             filterLabel
        //         ]
        //     }, {
        //         xtype: 'container',
        //         width: '75%',
        //         layout: {
        //             type: 'hbox',
        //             pack: 'end'
        //         },
        //         items: [
        //             massCalculation,   
        //             MineralPro.config.util.MainGridToolCmpt.addNewDataButton,
        //            // addNewDataButton,
        //             MineralPro.config.util.MainGridToolCmpt.saveToDbButton,                  
        //             MineralPro.config.util.MainGridToolCmpt.clearFilterButton
        //         ]
        // }];
                                          
        me.callParent(arguments);       
    },
});
