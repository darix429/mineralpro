Ext.define('Minerals.sub.MineralValuationManageAppraisal.view.AppraisalList.AppraisalListView', {
    extend: 'MineralPro.config.view.BaseView',
    alias: 'widget.AppraisalListView',
    layout: 'border',
    id: 'AppraisalListViewId',
    items: [{
            region: 'center',
            xtype: 'AppraisalListGridView'
        }]
});    