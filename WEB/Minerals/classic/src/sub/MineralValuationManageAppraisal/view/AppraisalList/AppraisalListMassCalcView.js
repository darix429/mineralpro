Ext.define('Minerals.sub.MineralValuationManageAppraisal.view.AppraisalList.AppraisalListMassCalcView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.sub.MineralValuationManageAppraisal.view.AppraisalList.AppraisalListMassCalcViewController'],
    controller: 'AppraisalListMassCalcViewController',
    alias: 'widget.AppraisalListMassCalcView',

    title: 'Mass Calculate Appraisal',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#appraisalYearItemId',
    width: 1000,
    height: 585,
    
    
    calcAppraisalStore: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalCalcStore',
    
    mainPanelId: 'MineralValuationManageAppraisalId', //id of the main panel


    initComponent: function () {
        var me = this; 
        me.items = [{
            xtype: 'grid',
            store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalStore',
            columns:[{   
                header: 'Appraisal Year',
                flex     : 1,
                dataIndex: 'appraisalYear',
                itemId: 'appraisalYearItemId',
                filterElement: new Ext.form.TextField()   
             },{   
                header: 'Subject Type',
                flex     : 1,
                dataIndex: 'subjectType',
                filterElement:new Ext.form.TextField(),
            },{   
                header: 'Subject Id',
                flex     : 1,
                dataIndex: 'subjectId',    
                filterElement: new Ext.form.TextField(),   
            },{   
                header: 'Name',
                flex     : 2,
                dataIndex: 'name', 
                filterElement: new Ext.form.TextField(),
            },{   
                header: 'RRC Number',
                flex     : 1,
                dataIndex: 'rrcNumber',   
                filterElement: new Ext.form.TextField(),
            },{   
                header: 'Field Name',
                flex     : 2,
                dataIndex: 'fieldName',  
                filterElement: new Ext.form.TextField(),
            },{   
                header: 'Operator Name',
                flex     : 2,
                dataIndex: 'operatorName',  
                filterElement: new Ext.form.TextField(),   
            },{   
                header: 'Total Appraised Value',
                flex     : 2,
                align: 'right',
                dataIndex: 'totalValue',   
                filterElement: new Ext.form.TextField(),
                renderer: function(val, meta) {
                    return  Ext.util.Format.number(val, '$000,000');
                },
            },{   
                header: 'Last Appraised Year',
                flex     : 1,
                dataIndex: 'lastAppraisalYear',   
                filterElement: new Ext.form.TextField(),
            }],
            
            selModel: Ext.create('Ext.selection.CheckboxModel',{
                checkOnly: true,
            }), 
            
            plugins: [Ext.create('Ext.ux.grid.FilterRow')]                             
        }];
    
    
         var massCalculation = {
            xtype: 'button',
            itemId: 'massCalculationItemId',
            tooltip: 'Mass Calculate',
            text: 'Mass Calculate',
            listeners: {
                click: 'onMassCalculation',
            }
        } 
        
        me.buttons= [massCalculation,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]

        me.callParent(arguments);
    },
    
    listeners:{
        close: function(){
            var gridselection = this.down('grid');
            var cols = gridselection.columns;

            Ext.each(cols, function (col) {
            if (col.filterElement)
                col.filterElement.setValue('');
            });
        }

    },  
    
});