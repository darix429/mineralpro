//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.MineralValuationManageAppraisal.view.AppraisalList.AppraisalListMassCalcViewController', {
    extend: 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.AppraisalListMassCalcViewController',
          
    onMassCalculation: function (grid, rowIndex, colIndex) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalListMassCalcViewController onMassCalculation.')
        
        var me = this;
        var view = me.getView();
        var selected = view.down('grid').getSelectionModel().selected
        var calcAppraisalStore = Ext.StoreMgr.lookup(view.calcAppraisalStore);
       
         if(selected.length>0){   
             
            view.mask('Loading...')
             
            var subjectTypeCdList='', subjectIdList='';
            selected.each(function(data){
                if(subjectIdList.indexOf('['+data.get('subjectId') + ']') == -1){
                    subjectTypeCdList += '['+data.get('subjectTypeCd') + '],';
                    subjectIdList += '['+data.get('subjectId') + '],';
                }          
            })
                Ext.Ajax.request({
                    url: window.location.origin + calcAppraisalStore.proxy.api.read,
                    method: 'POST',
                    params: {
                        massCalculation: 'true',
                        subjectTypeCdList: subjectTypeCdList,
                        subjectIdList: subjectIdList,
                        idAppUser: MineralPro.config.Runtime.idAppUser,
                        appraisalYear: MineralPro.config.Runtime.appraisalYear
                    },
                    success: function (form, data) {
                        view.unmask()
                        Ext.Msg.show({
                            iconCls: '',
                            title: '',    
                            msg: 'Mass calculation has been competed.',                
                            buttons: Ext.Msg.OK
                        });
                    }
                });              
        }else{
            Ext.Msg.show({
                iconCls: 'wrn',
                title: 'Error',    
                msg: 'Please select appraisal for calculation.',                
                buttons: Ext.Msg.OK
            });
        }
          
        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalListMassCalcViewController onMassCalculation.')
    },
});