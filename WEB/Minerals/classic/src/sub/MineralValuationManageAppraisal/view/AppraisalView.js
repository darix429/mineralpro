Ext.define('Minerals.sub.MineralValuationManageAppraisal.view.AppraisalView' ,{
    extend: 'MineralPro.config.view.BaseTabPanelView',
    alias : 'widget.MineralValuationManageAppraisal',
    layout:'border',
    
//    requires: 'Minerals.sub.MineralValuationManageAppraisal.view.AppraisalViewController',
//    controller: 'AppraisalViewController',
    
    //used for tab change 
    selectedId: '0',//Need default value '0'
    selectedIdIndex: 'subjectId',
    selectedName: '0',
    selectedNameIndex: 'name',        
    selectedRRCNumber: '0',//Need default value '0'
    selectedRRCNumberIndex: 'rrcNumber',
    subjectTypeCd: '', 
     
    
    appraisalDetailsTab: '1',
    
    id: 'MineralValuationManageAppraisalId',
    
    items: [
        {
            title: 'Appraisal List',
            xtype: 'AppraisalListView',
        },
        {
            title: 'Appraisal Details',
            xtype: 'AppraisalDetailsView',

        },
        {
            title: 'Appraisal Notes',
            xtype: 'AppraisalNotesView',
        }
    ],
    listeners: {
        beforetabchange: function(tabPanel, newCard, oldCard, eOpts){
            MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalView beforetabchange.')
            var me = this;
            var origFn = me.getController().onBeforeTabChange(tabPanel, newCard, oldCard, eOpts);

            var appraisalListView = me.down('AppraisalListView');
            var selected = appraisalListView.down('grid').getSelectionModel().getSelection();
            var appraisalList = appraisalListView.down('grid');
            var appraisalListStore = appraisalList.getStore();
            if(newCard.xtype == 'AppraisalDetailsView' && !selected.length && !me.newAppraisal){
                Ext.Msg.show({
                    title: MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                    msg: "You have not selected an appraisal, would you like to create one instead?",
                    iconCls: MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                    buttons: Ext.Msg.YESNO,
                    modal: true,
                    scope: this,
                    width: 250,
                    fn: function (btn) {
                        if (btn === 'yes'){
                            me.newAppraisal = true;
                            tabPanel.setActiveTab(newCard)
                        }
                    }
                });
                if(newCard.down('#appraisalIndexItemId')){
                    var appraisalListStoreCount = appraisalListStore.getCount();
                    var selected = appraisalListStoreCount ? appraisalList.getSelectionModel().getSelection() : [];
                    selected = selected.length ? selected[0] : false;
                    var currentIndex = selected ? appraisalListStore.indexOf(selected)+1 : 0;
                    newCard.down('#appraisalIndexItemId').setText( currentIndex + ' of ' + appraisalListStoreCount);
                }
                MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalView beforetabchange.')
                return false;
            }
            else if(newCard.xtype == 'AppraisalNotesView' && !selected.length && !appraisalListStore.getCount()){
                Ext.Msg.show({
                    title: MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                    msg: "You have not selected an appraisal, would you like to create one instead?",
                    iconCls: MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                    buttons: Ext.Msg.YESNO,
                    modal: true,
                    scope: this,
                    width: 250,
                    fn: function (btn) {
                        if (btn === 'yes'){
                            me.newAppraisal = true;
                            tabPanel.setActiveTab(tabPanel.down('AppraisalDetailsView'));
                        }
                    }
                });
                MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalView beforetabchange.')
                return false;
            }
            if(oldCard.xtype == 'AppraisalDetailsView' && me.newAppraisal && !oldCard.down('#saveToDbButtonItemId').isDisabled()){
                Ext.Msg.show({
                    title:MineralPro.config.Runtime.tabChangeConfirmationTitle,    
                    msg:MineralPro.config.Runtime.tabChangeConfirmationMsg,
                    iconCls:MineralPro.config.Runtime.tabChangeConfirmationIcon,
                    buttons: Ext.Msg.YESNO,               
                    scope: this,
                    width: 250,
                    fn: function(btn) { 
                        if (btn === 'yes'){
                            oldCard.down('#saveToDbButtonItemId').fireEvent('click');
                        }
                        else{
                            var form = oldCard.down('AppraisalDetailsFormView')
                            form.getController().resetFormValues(true)
                            var comboBoxes = ['subjectType', 'subjectId', 'name'];
                            comboBoxes.forEach(function(combo){
                                form.down('combobox[name='+combo+']').suspendEvent('change');
                                form.down('combobox[name='+combo+']').setValue('');
                                form.down('combobox[name='+combo+']').resumeEvent('change');
                            });
                            form.down('[name=subjectTypeCd]').setValue('');
                            form.down('[name=origRrcNumber]').setValue('');
                            me.newAppraisal = false;
                            tabPanel.setActiveTab(newCard);
                        }
                    }
                });
                MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalView beforetabchange.')
                return false;
            }
            else if(oldCard.xtype == 'AppraisalDetailsView' && me.newAppraisal && oldCard.down('#saveToDbButtonItemId').isDisabled()){
                me.newAppraisal = false;
            }
            MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalView beforetabchange.')
            return origFn;
        }
    }
    
});    