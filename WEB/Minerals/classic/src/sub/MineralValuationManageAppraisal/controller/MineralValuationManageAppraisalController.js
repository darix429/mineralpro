///**
// * Handles the controller processing for ProperRecordsManageAgent
// */

Ext.define('Minerals.sub.MineralValuationManageAppraisal.controller.MineralValuationManageAppraisalController', {
    extend: 'Ext.app.Controller',
  
//    models: [
//    ],
    
    stores: [
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalDetailStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalCalcStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSubjectIdStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSubjectNameStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalHeaderStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalOperatingExpStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalOilDailyAvgStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalGasDailyAvgStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalProdDailyAvgStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedProdStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedSvrcStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedInjStore',
        'Minerals.sub.MineralValuationManageAppraisal.store.AppraisalNoteStore'
    ],  
    
    views: [
        'Minerals.sub.MineralValuationManageAppraisal.view.AppraisalView',
        'Minerals.sub.MineralValuationManageAppraisal.view.AppraisalList.AppraisalListView',
        'Minerals.sub.MineralValuationManageAppraisal.view.AppraisalList.AppraisalListGridView',
        'Minerals.sub.MineralValuationManageAppraisal.view.AppraisalDetails.AppraisalDetailsFormView',
        'Minerals.sub.MineralValuationManageAppraisal.view.AppraisalDetails.AppraisalDetailsView',
        'Minerals.sub.MineralValuationManageAppraisal.view.AppraisalHeaderView',
        'Minerals.sub.MineralValuationManageAppraisal.view.AppraisalList.AppraisalListMassCalcView',
        'Minerals.sub.MineralValuationManageAppraisal.view.AppraisalNote.AppraisalNoteView',
        'Minerals.sub.MineralValuationManageAppraisal.view.AppraisalNote.AppraisalNoteDetailView',
        'Minerals.sub.MineralValuationManageAppraisal.view.AppraisalNote.AppraisalNoteGridView',
        'Minerals.sub.MineralValuationManageAppraisal.view.AppraisalNote.AppraisalNoteAddFormView',
        'Minerals.sub.MineralValuationManageAppraisal.view.AppraisalNote.AppraisalNoteEditFormView',
//        'Minerals.sub.MineralValuationManageAppraisal.view.ManageAppraisalAddFormView',
//        'Minerals.sub.MineralValuationManageAppraisal.view.ManageAppraisalEditFormView',
//        'Minerals.sub.MineralValuationManageAppraisal.view.ManageAppraisalDetailView',
    ],          
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden'],

    init: function(){
        var me = this;
        
        setTimeout(function () {
            me.getStore('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedProdStore').load();
            me.getStore('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedSvrcStore').load();
            me.getStore('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedInjStore').load();
        }, 200);
    }
});


