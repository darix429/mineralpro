///**
// * Handles the controller processing for Client List
// */

Ext.define('Minerals.sub.ProperRecordsManageOperator.controller.ProperRecordsManageOperatorController', {
    extend: 'Ext.app.Controller',
  
    models: [
        'Minerals.sub.ProperRecordsManageOperator.model.ManageOperatorModel'
    ],
    
    stores: [
        'Minerals.sub.ProperRecordsManageOperator.store.ManageOperatorStore',
    ],  
    
    views: [
        'Minerals.sub.ProperRecordsManageOperator.view.ManageOperatorView',
        'Minerals.sub.ProperRecordsManageOperator.view.ManageOperatorGridView',
        'Minerals.sub.ProperRecordsManageOperator.view.ManageOperatorAddFormView',
        'Minerals.sub.ProperRecordsManageOperator.view.ManageOperatorEditFormView',
    ],          
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden']
});


