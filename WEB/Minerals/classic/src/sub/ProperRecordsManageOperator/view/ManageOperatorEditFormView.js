Ext.define('Minerals.sub.ProperRecordsManageOperator.view.ManageOperatorEditFormView', {
    extend: 'Minerals.sub.ProperRecordsManageOperator.view.ManageOperatorAddFormView',
    //requires: ['Minerals.sub.ProperRecordsManageOperator.view.ManageOperatorEditFormViewController'],
    //controller: 'ManageOperatorEditFormViewController',
    alias: 'widget.ManageOperatorEditFormView',
    
    
    title: 'Edit Operator',
    defaultFocus: '#operatorNameItemId',

    listeners: {
        afterrender: function(){
            var me = this;
            var operatorNoItemId = me.down('#operatorNoItemId');
                operatorNoItemId.disable(true);
                //operatorNoItemId.show();
        }
    },

    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});