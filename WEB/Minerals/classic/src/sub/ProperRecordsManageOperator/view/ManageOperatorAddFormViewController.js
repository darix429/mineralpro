//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.ProperRecordsManageOperator.view.ManageOperatorAddFormViewController', {
    extend: 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.ManageOperatorAddFormViewController', 

    onAddAndNew: function (button) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering SecurityUserListAddFormViewController onAddAndNew.')
        var me = this;
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore);
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);

        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();
        if (form.isValid()) {
            record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
            record.set(values);
            listStore.insert(0, record);

            listGrid.getSelectionModel().select(0, false, true);

            //form.reset()
            var new_record = Ext.create(listGrid.mainListModel)
            form.loadRecord(new_record);
            view.focus(view.defaultFocus)

             setTimeout(function () {
                view.down('#ownerNameItemId').setValue(view.down('#ownerIdItemId').getRawValue());
                // view.down('#agentNameItemId').setValue(view.down('#AgentCdxItemId').getRawValue()); 
            }, 100);  

            //listGrid.fireEvent('afterlayout');

        } else {
            Ext.Msg.show({
                iconCls: MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title: MineralPro.config.Runtime.addEditToGridMsgErrorTitle,
                msg: MineralPro.config.Runtime.addEditToGridMsgErrorMsg,
                buttons: Ext.Msg.OK
            });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving SecurityUserListAddFormViewController onAddAndNew.')
    }

});