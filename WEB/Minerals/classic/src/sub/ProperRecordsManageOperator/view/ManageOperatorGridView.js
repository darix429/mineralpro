
Ext.define('Minerals.sub.ProperRecordsManageOperator.view.ManageOperatorGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.ManageOperatorGridView',
    store:'Minerals.sub.ProperRecordsManageOperator.store.ManageOperatorStore',

    requires: ['Minerals.sub.ProperRecordsManageOperator.view.ManageOperatorGridViewController'],
    controller: 'ManageOperatorGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'operatorNoItemId', 
    mainPanelAlias: 'ProperRecordsManageOperator', //widget alias name of the main panel
    mainListModel: 'Minerals.sub.ProperRecordsManageOperator.model.ManageOperatorModel',     

//    detailAliasName: 'clientListDetails',//widget aliasName for detailView
    addFormAliasName: 'ManageOperatorAddFormView', //widget form to call for add list 
    editFormAliasName: 'ManageOperatorEditFormView', //widget form to call for edit
    
    affectedComboStore: ['Minerals.store.MineralValuation.OperatorComboStore'],//combos store for agent
    
    firstFocus: 'operatorNoItemId',    
    
    addButtonTooltip: 'Add New Operator',
    addButtonText: 'Add New Operator',
        
    columns:[{ 
        header: 'Operator No.',
        flex     : 1,
        dataIndex: 'operatorId',
        itemId: 'operatorNoItemId',
        filterElement: new Ext.form.TextField(),
        // toggleSortState: function () {
        //     var grid = this.up().grid,
        //         store = grid.getStore(),
        //         direction = this.sortState,
        //         columnName = this.dataIndex;
            
        //         console.log(direction+' - '+columnName)
        //     store.sort([{
        //         direction : 'DESC',
        //         sorterFn : 'recordDivision'
        //     },{
        //         direction : direction,
        //         sorterFn : columnName
        //     }])
        //     grid.multiColumnSort = originalMultiColumnSort;
        // }
    },{   
        header: 'Operator Name',
        flex     : 3,
        dataIndex: 'operatorName',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Owner No.',
        flex     : 1,
        dataIndex: 'ownerId',
        filterElement:new Ext.form.TextField()    
     },{   
        header: 'Owner Name',
        flex     : 3,
        dataIndex: 'ownerName1',
        filterElement:new Ext.form.TextField()     
    },
    // {   
    //     header: 'Record Division',
    //     flex     : 0,
    //     width : 300,
    //     dataIndex: 'recordDivision',
    //     filterElement:new Ext.form.TextField(),
    //     renderer : function(val, column, record) {
    //         if(record.dirty){
    //             record.data.recordDivision = 'true'
    //         }
    //         return record.dirty
    //     }     
    //  },
    // {   
    //     header: 'Agent No.',
    //     flex     : 1,
    //     dataIndex: 'agencyCdx',
    //     filterElement:new Ext.form.TextField() 
    // },{   
    //     header: 'Agent Name',
    //     flex     : 1,
    //     dataIndex: 'agencyName',
    //     filterElement:new Ext.form.TextField()      
    // },
    {
     xtype: 'actioncolumn',
        width: Minerals.config.Runtime.actionColWidth,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls,
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    }],
 
    initComponent: function () {  
        var me = this;
               
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],       
               
        me.tools= [
        MineralPro.config.util.MainGridToolCmpt.addNewDataButton
        ,MineralPro.config.util.MainGridToolCmpt.saveToDbButton                   
        ,MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
        me.callParent(arguments);
        
    }    
});