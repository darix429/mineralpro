Ext.define('Minerals.sub.ProperRecordsManageOperator.view.ManageOperatorView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.ProperRecordsManageOperator',
    layout:'border',
    
    id: 'ProperRecordsManageOperatorId',
    
    items: [{
        xtype:'ManageOperatorGridView',
        html:'center',
        region:'center'
    }]
});    