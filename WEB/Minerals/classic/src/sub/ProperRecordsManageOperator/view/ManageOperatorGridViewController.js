//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.sub.ProperRecordsManageOperator.view.ManageOperatorGridViewController', {
    extend: 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.ManageOperatorGridViewController',

    onOpenAddListForm: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering ManageOperatorGridViewController onOpenAddListForm.')
        var me = this;
        var mainView = me.getView();
        var record = Ext.create(mainView.mainListModel);      
        var addView = Ext.widget(mainView.addFormAliasName);
        var comboStore = Ext.getStore('Minerals.store.ProperRecords.OwnerComboStore');
        comboStore.reload();
        record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
        addView.down('form').loadRecord(record);
        addView.show()
        MineralPro.config.RuntimeUtility.DebugLog('Leaving ManageOperatorGridViewController onOpenAddListForm.')
    },

});
