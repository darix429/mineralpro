Ext.define('Minerals.sub.ProperRecordsManageOperator.view.ManageOperatorAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.sub.ProperRecordsManageOperator.view.ManageOperatorAddFormViewController'],
    controller: 'ManageOperatorAddFormViewController',
    alias: 'widget.ManageOperatorAddFormView',
    title: 'Add New Operator',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#operatorNoItemId',
    width: 450,
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'ProperRecordsManageOperatorId', //id of the main panel
    listGridStore: 'Minerals.sub.ProperRecordsManageOperator.store.ManageOperatorStore', //store for note grid
    listGridAlias: 'ManageOperatorGridView', //alias name for note gridview

    initComponent: function () {
        var me = this; 
            me.items = [{
            xtype: 'form',
            defaults:{ 
                maskRe: /[^'\^]/,
                labelWidth: 130,
                width: 400
            },
            bodyPadding:5,
            items: [{          
                fieldLabel: 'Operator No.',               
                selectOnFocus: true,
                tabIndex: 1,
                itemId: 'operatorNoItemId',
                name: 'operatorId',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
                maxLength: 9,
                maskRe:/[0-9]/,
                //hidden: true,
                listeners: {
                    blur: 'onAddCheckExisting',
                    change: function( field, newval, oldval ) {
                        if(field.getRawValue != ''){
                            if(newval > 0){
                                field.setValue(newval*1);
                            } else {
                                field.setValue('');
                            }
                        }
                    }
                }
            },{
                fieldLabel: 'Operator Name',
                selectOnFocus: true,
                tabIndex: 2,
                itemId: 'operatorNameItemId',
                name: 'operatorName',
                xtype: 'textfield',
                maxLength: 50,
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
                regex: /[a-zA-Z0-9]+/
            },{
                fieldLabel: 'Owner',
                xtype: 'combo',
                selectOnFocus: true,
                tabIndex: 3,
                itemId: 'ownerIdItemId',
                displayField: 'fieldName',
                valueField: 'valueField',
                store: 'Minerals.store.ProperRecords.OwnerComboStore',
                listeners:{
                    select:function(record){
                        me.down('#ownerNameItemId').setValue(this.getRawValue());
                    },      
                    // afterrender: function(){
                    //     var comboOwner = this;
                    //     var comboOwnerStore = comboOwner.getStore();                          
                    //     comboOwnerStore.load({
                    //         callback: function(){
                    //             comboOwner.select(comboOwnerStore.getData().getAt(0));
                    //         }
                    //     })
                    // },
//                    beforequery: function(queryVV){
//                        queryVV.combo.expand();
//                        queryVV.combo.store.load();
//                        return false;
//                    }
                },                
                name: 'ownerId',
                typeAhead: true,
                queryMode: 'local',
                forceSelection: true,
                allowBlank: false,
                labelAlign: 'right',    
             },
            //  {
            //     fieldLabel: 'Agent',
            //     xtype: 'combo',
            //     selectOnFocus: true,
            //     tabIndex: 4,
            //     itemId: 'AgentCdxItemId',
            //     displayField: 'agencyName',
            //     valueField: 'agencyCdx',
            //     store: 'Minerals.store.ProperRecords.AgencyStore',
            //     listeners:{
            //         select:function(record){
            //             me.down('#agentNameItemId').setValue(this.getRawValue());
            //         },    
            //         // afterrender: function(){
            //         //     var comboAgent = this;
            //         //     var comboAgentStore = comboAgent.getStore();                          
            //         //     comboAgentStore.load({
            //         //         callback: function(){
            //         //             comboAgent.select(comboAgentStore.getData().getAt(0));
            //         //         }
            //         //     })
            //         // },
            //         beforequery: function(queryVV){
            //             queryVV.combo.expand();
            //             queryVV.combo.store.load();
            //             return false;
            //         }
            //     },
            //     name: 'agencyCdx',
            //     typeAhead: true,
            //     forceSelection: true,
            //     allowBlank: false,
            //     labelAlign: 'right', 

            //  },
             {
                fieldLabel: 'Owner Name',
                name: 'ownerName1',
                xtype: 'hidden',
                margin: '0 0 10',
                itemId:'ownerNameItemId',
                labelAlign: 'right',     
             }
            //  {
            //     fieldLabel: 'Agent Name',
            //     name: 'agencyName',
            //     xtype: 'hidden',
            //     margin: '0 0 10',
            //     itemId:'agentNameItemId',
            //     labelAlign: 'right',             
            // }
            ],
        }];

        
        me.listeners = {
        afterrender: function () {                
                setTimeout(function () {
                    me.down('#ownerNameItemId').setValue(me.down('#ownerIdItemId').getRawValue());
                    // me.down('#agentNameItemId').setValue(me.down('#AgentCdxItemId').getRawValue()); 
            }, 100);                                                                                                    
        }}

        me.callParent(arguments);
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
        MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});
