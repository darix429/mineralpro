
Ext.define('Minerals.config.view.Field.BaseFieldGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    requires: ['Minerals.config.view.Note.BaseFieldGridViewController'],
    controller: 'BaseFieldGridViewController',
    autoRender: true,
    //below are default items needed for grid checking 
    firstLoadItemId: 'rrcNumberItemId',
    firstFocus: 'rrcNumberItemId',
    
    initComponent: function () {
        var me = this;

        var fieldStore = Ext.StoreMgr.lookup(me.store);  
                           
        fieldStore.on({
            load: function(store, records, successful, operation, eOpts ){
                if(records.length == 0){                      
//                    me.up(me.mainPanelAlias).selectedRRCNumber = '0'
                    Ext.getCmp(me.mainPanelId).selectedRRCNumber = '0'
                }                     
            },
        });   

        var appraisal = {
            icon: Minerals.config.Runtime.appraisalCls,
            tooltip: Minerals.config.Runtime.appraisalTooltip,
            handler: function (grid, rowIndex, colIndex, item, e, record) {
                this.up('grid').getSelectionModel().select(rowIndex)
                this.up('grid').getController().onGoToAppraisal(grid, rowIndex, colIndex, item)
            },
        }

        var note = {
            icon: Minerals.config.Runtime.noteCls,
            tooltip: Minerals.config.Runtime.noteTooltip,
            handler: function (grid, rowIndex, colIndex, item, e, record) {
                this.up('grid').getSelectionModel().select(rowIndex)
                this.up('grid').getController().onGoToNote(grid, rowIndex, colIndex, item)
            },
        }
                

        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')]
        me.columns = [{
                header: 'RRC #',
                flex: 1,
                dataIndex: 'rrcNumber',
                itemId: 'rrcNumberItemId',
                filterElement: new Ext.form.TextField({
                })
            }, {
                header: 'Field #',
                flex: 1,
                dataIndex: 'fieldId',
                filterElement: new Ext.form.TextField()
            }, {
                header: 'Field Name',
                flex: 1,
                dataIndex: 'fieldName',
                filterElement: new Ext.form.TextField()
            }, {
                header: 'Depth',
                flex: 1,
                dataIndex: 'wellDepth',
                filterElement: new Ext.form.TextField()
            }, {
                header: 'Gravity',
                flex: 1,
                dataIndex: 'gravityDesc',
                filterElement: new Ext.form.TextField()
            },{
                header: 'Last Appraisal Year',
                flex: 1,
                dataIndex: 'lastAppraisalYear',
                filterElement: new Ext.form.TextField()
            }, {
                xtype: 'actioncolumn',
                width: 50,
                alwaysShow: true,
                items: [
                    appraisal,
                    note
                ],
                listeners: {
                    afterrender: function () {
                        var me = this
                        me.setHidden(MineralPro.config.Runtime.access_level)
            }         }
            }],
        me.tools = [],
        me.callParent(arguments);

    }
});