//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.config.view.Note.BaseFieldGridViewController', {
    extend: 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.BaseFieldGridViewController',
                                   
    onGoToAppraisal: function(){    
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseFieldGridViewController onGoToAppraisal.')
        
        var me = this;
        var view = me.getView()
        var mainPanel = view.up(view.mainPanelAlias); 
        mainPanel.setActiveTab(parseInt(mainPanel.appraisalTabIndex));
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseFieldGridViewController onGoToAppraisal.')
    },      
    
    onGoToNote: function(){    
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseFieldGridViewController onGoToNote.')
        
        var me = this;
        var view = me.getView()
        var mainPanel = view.up(view.mainPanelAlias); 
        mainPanel.setActiveTab(parseInt(mainPanel.noteTabIndex));
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseFieldGridViewController onGoToNote.')
    }, 
                  
    onLoadSelectionChange: function (model, records) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseFieldGridViewController onLoadSelectionChange.')
            var me = this;

            if (!me.xtype) {
                me = me.getView();
            }

            var mainPanel = me.up(me.mainPanelAlias); 
                                      
            if(records.length > 0){
                mainPanel.selectedRRCNumber = records[0].get(mainPanel.selectedRRCNumberIndex); 
            }
                          
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseFieldGridViewController onLoadSelectionChange.')
    },              
                  
                  
});

