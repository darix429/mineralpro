//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.config.view.Note.BaseNoteGridViewController', {
    extend: 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.BaseNoteGridViewController',
                  
     /**
     * Sync the client grid data to database (add, edit, delete)
     */
    onSyncListGridData: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseNoteGridViewController onSyncListGridData.')
        var me = this;       
        var view = me.getView();
        var listStore = view.getStore();
        var dirty = listStore.getNewRecords().length > 0
        || listStore.getUpdatedRecords().length > 0
        || listStore.getRemovedRecords().length > 0;
        if (dirty) {
            Ext.Msg.show({
                title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                msg:MineralPro.config.Runtime.syncGridMsgConfimationMsg,
                iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                buttons: Ext.Msg.YESNO,
                scope: this,
                width: 250,
                fn: function (btn) {
                    if (btn === 'yes') {
                        view.getSelectionModel().deselectAll(true);
                        listStore.getProxy().extraParams = {
                            createLength: listStore.getNewRecords().length,
                            updateLength: listStore.getUpdatedRecords().length
                        };

                        listStore.sync({
                            success: function(batch, options){
                                var mainPanel = view.up(view.mainPanelAlias)
                                if(mainPanel.selectedId){
                                    listStore.getProxy().extraParams = {
                                        selectedId: mainPanel.selectedId,
                                        selectedRRCNumber: mainPanel.selectedRRCNumber
                                    };
                                }
                                listStore.commitChanges();
                                listStore.reload();
                            },
                            failure: function(batch, options){
                                MineralPro.config.RuntimeUtility.DebugLog('Entering BaseNoteGridViewController onSyncListGridData. listStore failure')
                                Ext.Msg.show({
                                    iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                                    title:MineralPro.config.Runtime.listSyncMsgErrorTitle,
                                    msg:'Failed to save your data.',
                                    buttons: Ext.Msg.OK
                                });
                                listStore.queryRecordsBy(function(rec){
                                    return !rec.get('idNote') || rec.get('rowDeleteFlag') != '';
                                }).forEach(function(rec){
                                    rec.dirty = true;
                                })
                                view.getView().refresh();
                                
                                    
                                MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseNoteGridViewController onSyncListGridData. listStore failure')
                            },
                            callback: function(batch, options){
                                // this will be executed regardless if sync is successful or not
                                // console.log('------------------------------- START callback -------------------------------');
                                // console.log(options);
                                // console.log(listStore);
                                // console.log(view)
                                // console.log('-------------------------------- END --------------------------------');
                                    
                            }
                        });
                    }
                }
            });
        } else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                title:MineralPro.config.Runtime.listSyncMsgErrorTitle,
                msg:MineralPro.config.Runtime.listSyncMsgErrorMsg,
                buttons: Ext.Msg.OK
            });                      
        }                         
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseNoteGridViewController onSyncListGridData.')
    },              
                  
    onAfterLoadNote: function (e, event, opt) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseNoteGridViewController onAfterLoadNote.')

        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var noteStore = view.getStore();

        if(view.fieldGridViewAlias){
            var fieldGrid = mainPanel.down(view.fieldGridViewAlias);
            var fieldStore = fieldGrid.getStore();

            fieldStore.getProxy().extraParams = {
                selectedId: mainPanel.selectedId,
            };

            fieldStore.load({
                callback: function () {   
                    if (fieldStore.getCount() > 0) {
                        view.down('#addNewDataButtonItemId').enable(true)
                        view.down('#rrcNumberItemId').setText('RRC Number: ' + mainPanel.selectedRRCNumber);                   
                        if(fieldStore.find('rrcNumber', mainPanel.selectedRRCNumber, false, false, true) == -1){
                            var fiedlGridSelection = fieldGrid.getSelectionModel();
                            fiedlGridSelection.select(0, false, false);
                        }

                        noteStore.getProxy().extraParams = {
                            selectedId: mainPanel.selectedId,
                            selectedRRCNumber: mainPanel.selectedRRCNumber
                        };              
                        noteStore.load();
                    }else{
                        view.down('#addNewDataButtonItemId').disable(true);
                        view.down('#rrcNumberItemId').setText('RRC Number: None');  
                        Ext.Msg.show({
                            iconCls:Minerals.config.Runtime.noNoteMsgErrorIcon,
                            title:Minerals.config.Runtime.noNoteMsgErrorTitle,    
                            msg:Minerals.config.Runtime.noNoteMsgErrorMsg,               
                            buttons: Ext.Msg.OK
                        });
                    }
                }           
            })
        }
        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseNoteGridViewController onAfterLoadNote.')
    },              
    onLoadSelectionChange: function(model, record){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseNoteGridViewController onLoadSelectionChange.')
        record = record[0];
        var me = this;
        var view = me.getView();
        var detailsView = view.nextSibling('[region=east]');
        if(record){
            detailsView.loadRecord(record);
        }
        else{
            detailsView.reset();
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseNoteGridViewController onLoadSelectionChange.')
    }
});

