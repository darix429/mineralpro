Ext.define('Minerals.config.view.Note.BaseNoteAddEditFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['Minerals.config.view.Note.BaseNoteAddEditFormViewController'],
    controller: 'BaseNoteAddEditFormViewController',

    saveAction: 'onAddListToGrid',
    defaultFocus: '#noteId',
    layout: 'fit',

    bodyPadding: 2,
    width: 800,
    height: 600,

    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'form',
            defaults:{ maskRe: /[^'\^]/},
            margin:'0 4 0 0',
            bodyPadding: 2,
            items: [{
                    xtype: 'hidden',
                    name: 'subjectId',
                    itemId: 'subjectIdItemId'
                },{
                    xtype: 'hidden',
                    name: 'subjectTypeCd',
                    itemId: 'subjectTypeCdItemId'
                },{
                    fieldLabel: 'Subject Type',
                    xtype: 'displayfield',
                    labelAlign: 'right',
                    msgTarget: 'side',
                    itemId: 'subjectTypeItemId',
                },{
//                    fieldLabel: 'Lease',
                    xtype: 'displayfield',
                    labelAlign: 'right',
                    msgTarget: 'side',
                    itemId: 'subjectLabelItemId',
                            
                },{                   
                    fieldLabel: 'RRC Number',
                    //tabIndex: 1,
                    itemId: 'rrcNumberItemId',                  
                    xtype: 'displayfield',
                    //margin: '0 0 10',
                    labelAlign: 'right', 
                    readOnly: true,
                },{    
                    xtype: 'hidden',
                    name: 'rrcNumber',
                    itemId: 'rrcNumberHiddenItemId', 
                },{
                    xtype: 'radiogroup',
                    fieldLabel: 'Confidential',
                    margin: '0 0 10',
                    labelAlign: 'right',
                    width: 150,
                    items: [{
                        name: 'securityCd',
                        tabIndex: 1,
                        boxLabel: 'No', 
                        inputValue: 1, 
                        margin: '0 10 0',
                    },{
                        name: 'securityCd',
                        boxLabel: 'Yes', 
                        inputValue: 2
                    }]
                },{
                    fieldLabel: 'Notes Type',
                    selectOnFocus: true,
                    tabIndex: 2,
                    width: 750,
                    name : 'noteTypeCd',
                    xtype:'combobox',
                    itemId:'comboNoteType',
                    margin: '10 0 10',
                    labelAlign: 'right',
                    emptyText : 'Select Note Type',
                    valueField: 'noteTypeCd', 
                    displayField: 'noteTypeDesc',
                    store: 'Minerals.store.MineralValuation.NoteTypeStore',
                    typeAhead: true,
                    queryMode:'local',
                    forceSelection: true,
                    listeners:{
                        // Select: function (record) {
                        //     var note = me.down('#noteId');
                        //     note.setValue(note.getValue() + ' ' + this.getRawValue());
                        //     note.focus();
                        // },
                    }, 
                    // forceSelection: true,
                    //allowBlank: false,
                    msgTarget: 'side'  
                },{
                    fieldLabel: 'Quick Notes',
                    selectOnFocus: true,
                    tabIndex: 2,
                    width: 750,
                    name : 'quickNoteCd',
                    xtype:'combobox',
                    itemId:'comboQuickNote',
                    margin: '10 0 10',
                    labelAlign: 'right',
                    emptyText : 'Select Quick Note',
                    valueField: 'quickNoteCd', 
                    displayField: 'quickNoteDesc',
                    store: 'Minerals.store.MineralValuation.QuickNoteStore',
                    typeAhead: true,
                    queryMode:'local',
                    forceSelection: true,
                    listeners:{
                        Select: function (record) {
                            var note = me.down('#noteId');
                            note.setValue(note.getValue() + ' ' + this.getRawValue());
                            note.focus();
                        },
                    }, 
                    // forceSelection: true,
                    allowBlank: true,
                    msgTarget: 'side'  
                },{
                    fieldLabel: 'Notes',
                    xtype: 'textareafield',
                    name : 'note',
                    itemId: 'noteId',
                    tabIndex: 3,
                    width: 750,
                    height: 190, 
                    labelAlign: 'right',
                    allowBlank: false,
                }]
            
        }];
        
        this.callParent(arguments);
    },

    listeners:{
        afterrender: function(){
            var me = this;          
            var mainPanel = Ext.getCmp(me.mainPanelId)
            
            me.down('#noteId').focus();
            me.down('#subjectIdItemId').setValue(mainPanel.selectedId);
            me.down('#subjectTypeItemId').setValue(me.subjectType);
            me.down('#subjectLabelItemId').setFieldLabel(me.subjectTypeLabel);
            me.down('#subjectLabelItemId').setValue(mainPanel.selectedName);         
            
           if(mainPanel.selectedRRCNumber == null){
                me.down('#rrcNumberItemId').hide()
                me.down('#noteId').setHeight(220)
            }else{
                me.down('#rrcNumberItemId').setValue(mainPanel.selectedRRCNumber);
                me.down('#rrcNumberHiddenItemId').setValue(mainPanel.selectedRRCNumber);               
            }
           
        },
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
        MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});