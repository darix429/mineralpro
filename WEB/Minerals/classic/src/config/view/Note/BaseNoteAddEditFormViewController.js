//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.config.view.Note.BaseNoteAddEditFormViewController', {
    extend: 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.BaseNoteAddEditFormViewController', 
    
    onAddAndNew: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseNoteAddEditFormViewController onAddAndNew.')
        var me = this;                     
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore); 
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
        var mainPanel = Ext.getCmp(view.mainPanelId)        
                
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();  
                                     
        if(form.isValid()){
            record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
            record.set(values);    
            listStore.insert(0,record);
            
            listGrid.getSelectionModel().select(0, true);  
            
            var new_record = Ext.create(listGrid.mainListModel)                    
            form.loadRecord(new_record);
                                    
            view.down(view.defaultFocus).focus();
            view.down('#subjectIdItemId').setValue(mainPanel.selectedId);
            view.down('#subjectTypeItemId').setValue(view.subjectType);
            view.down('#subjectLabelItemId').setFieldLabel(view.subjectType);
            view.down('#subjectLabelItemId').setValue(mainPanel.selectedName);
            if(view.down('#rrcNumberHiddenItemId')){
                view.down('#rrcNumberHiddenItemId').setValue(mainPanel.selectedRRCNumber);
            }                 
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        }  
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseNoteAddEditFormViewController onAddAndNew.')
    },
    
});