//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('Minerals.config.view.Appraisal.BaseAppraisalFormViewController', {
    extend: 'MineralPro.config.view.BaseViewController',
    alias: 'controller.BaseAppraisalFormViewController',
    counter: 0,
    storeFailureCallback: function(operation){
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalDetailsFormViewController storeFailureCallback.')
        var response = operation && typeof operation.getError == 'function' ? operation.getError() : {};
        var msg = 'Request Status: '+response.status+'<br/>Request Status Text: '+response.statusText+'<br/>Response Text: '+response.responseText;
        Ext.Msg.show({
            iconCls: Minerals.config.Runtime.appraisalCalcAlertIcon,
            title: 'Error!',
            closeAction: 'hide',
            msg: 'Something went wrong while processing your request.<br/><span style="font-weight:bold;line-height:25px;">Error Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p><p style="font-weight:bold;font-style:italic;color:red;">*** If error persist, please check you\'re server.</p>',
            buttons: Ext.Msg.OK,
        });
        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalDetailsFormViewController storeFailureCallback.')
    },
    onNewAppraisalButton: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController onNewAppraisalButton.')
        var me = this;
        var view = me.getView();
        view.newAppraisal = true;
        view.down('#rrcNumberItemId').focus();

        view.down('#calcButtonItemId').disable(true);
        me.resetFormValues(true)

        // var defaultField = view.down('#fieldItemId').getStore().getAt(0)
        // view.down('#fieldItemId').select(defaultField)
        // view.down('#fieldItemId').fireEvent('select', view.down('#fieldItemId'), defaultField)

        var declineGridArr = view.down('#declineTypeItemId').query('grid');

        Ext.each(declineGridArr, function(grid){
            var store = grid.getStore();
                store.getProxy().extraParams = {
                    selectedId: '0',
                    rrcNumber: '0',
                    subjectTypeCd: '0'
                }
                store.load({
                    callback: function(record, operation, success){
                        if(success){

                        }
                        else{
                            me.storeFailureCallback(operation);
                        }
                    }
                });
        })
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController onNewAppraisalButton.')
    },
    onNextButton: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController onNextButton.')

        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var appraisalStore = view.down('grid').getStore();
        var fieldGrid = mainPanel.down(view.fieldGridViewAlias);
        var fieldStore = fieldGrid.getStore();
//        var saveData = false
        var saveData = true;
        var record = view.getRecord();
        var values = view.getValues();
            record.set(values);

        if(view.down('#saveToDbButtonItemId').isDisabled() || view.down('#saveToDbButtonItemId').isHidden()){
            saveData = false;
        }

//        if(appraisalStore.getNewRecords().length > 0){
//            var newRec = appraisalStore.getNewRecords()[0]
//                var exclude = 'rrcNumber,fieldId,'
//                    +'wellDescription,wellTypeCd,maxYearLife,comment,productionEquipmentCount,'
//                    +'productionWellSchedule,productionEquipmentValue,serviceEquipmentCount,'
//                    +'serviceWellSchedule,serviceEquipmentValue,injectionEquipmentCount,'
//                    +'injectionWellSchedule,injectionEquipmentValue,'
//                    +'discountRate,grossOilPrice,grossProductPrice,'
//                    +'grossWorkingInterestGasPrice,grossRoyaltyInterestGasPrice,operatingExpense,'
//                    +'dailyAverageOil,dailyAverageGas,dailyAverageProduct'
//                var fieldArray = exclude.split(',')
//                Ext.each(fieldArray, function(field){
//                    if(newRec.get(field) != 0 && newRec.get(field) != ''
//                            && newRec.get(field) != '0'){
//                        saveData = true
//                        return false;
//                    }
//                })
//        }else if(appraisalStore.getUpdatedRecords().length > 0){
//            saveData = true
//        }

        if (saveData){
            Ext.Msg.show({
            title:MineralPro.config.Runtime.tabChangeConfirmationTitle,
                    msg:MineralPro.config.Runtime.tabChangeConfirmationMsg,
                    iconCls:MineralPro.config.Runtime.tabChangeConfirmationIcon,
                    buttons: Ext.Msg.YESNO,
                    scope: this,
                    width: 250,
                    fn: function(btn) {
                        if (btn === 'yes'){
                            Ext.getBody().mask('Loading...');
                            var params = appraisalStore.getProxy().extraParams
                                  params['createLength'] = appraisalStore.getNewRecords().length
                                  params['updateLength'] = appraisalStore.getUpdatedRecords().length
                                  appraisalStore.getAt(0).set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
                                  params['origRrcNumber'] = appraisalStore.getAt(0).get('origRrcNumber');
                                  params['newRecord'] = view.newAppraisal
                                  params['newCalculation'] = view.newCalculation
                                  params['updateCalc'] = false;

                                appraisalStore.getProxy().extraParams = params

                            appraisalStore.sync({
                                success: function(){
                                    loadNext();
                                },
                                failure: function(batch, options){
                                    Ext.getBody().unmask();
                                    appraisalStore.rejectChanges();
                                    var msg = batch.getExceptions().map(function(operation){
                                        return 'Status Text: '+operation.getError().statusText;
                                    });
                                    msg = msg.join('<br/> ');

                                    Ext.Msg.show({
                                        title: 'Oops!',
                                        msg: 'Failed to save your data <br/><span style="font-weight:bold;line-height:25px;">Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p>',
                                        icon: Ext.Msg.ERROR,
                                        buttons: Ext.Msg.OK
                                    });
                                },
                                callback: function(){
                                    
                                }
                            });
                        } else{
                            loadNext();
                        }
                    }
               });
           }else{
               loadNext();
           }

        function loadNext(){
            fieldStore.getProxy().extraParams = {
                selectedId: mainPanel.selectedId,
            };


            fieldStore.load({
                callback: function (records, operation, success) {
                    if(success){
                        Ext.getBody().mask('Loading...');
                        var fiedlGridSelection = fieldGrid.getSelectionModel();

                        var fieldStoreCount = fieldStore.getCount();
                        var currentIndex = fieldStore.find('rrcNumber', view.down('#rrcNumberItemId').getValue(), false, false, true);

                        if (fieldStoreCount > currentIndex + 1) {
                            currentIndex = currentIndex + 1;
                        } else {
                            currentIndex = 0;
                        }

                        fiedlGridSelection.select(currentIndex, false, false)
                        view.down('#appraisalIndexItemId').setText(currentIndex + 1 + ' of ' + fieldStoreCount);
                        appraisalStore.getProxy().extraParams = {
                            selectedId: mainPanel.selectedId,
                            selectedRRCNumber: mainPanel.selectedRRCNumber
                        };

                        appraisalStore.load({
                            callback: function(records, operation, success){
                                if(success){
                                    Ext.getBody().mask('Loading...');
                                    // view.down('#saveToDbButtonItemId').disable(true);
                                    view.down('#calcButtonItemId').enable(true);
                                }
                                else{
                                    me.storeFailureCallback(operation);
                                }
                            }
                        });
                    }
                    else{
                        me.storeFailureCallback(operation);
                    }
                    
                }
            })
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController onNextButton.')
    },
    onPreviousButton: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController onPreviousButton.')

        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var appraisalStore = view.down('grid').getStore();
        var fieldGrid = mainPanel.down(view.fieldGridViewAlias);
        var fieldStore = fieldGrid.getStore();
//        var saveData = false;
        var saveData = true;

        var record = view.getRecord();
        var values = view.getValues();
            record.set(values);
        if(view.down('#saveToDbButtonItemId').isDisabled() || view.down('#saveToDbButtonItemId').isHidden()){
            saveData = false;
        }

//        if(appraisalStore.getNewRecords().length > 0){
//            var newRec = appraisalStore.getNewRecords()[0]
//                var exclude = 'rrcNumber,fieldId,'
//                    +'wellDescription,wellTypeCd,maxYearLife,comment,productionEquipmentCount,'
//                    +'productionWellSchedule,productionEquipmentValue,serviceEquipmentCount,'
//                    +'serviceWellSchedule,serviceEquipmentValue,injectionEquipmentCount,'
//                    +'injectionWellSchedule,injectionEquipmentValue,'
//                    +'discountRate,grossOilPrice,grossProductPrice,'
//                    +'grossWorkingInterestGasPrice,grossRoyaltyInterestGasPrice,operatingExpense,'
//                    +'dailyAverageOil,dailyAverageGas,dailyAverageProduct'
//                var fieldArray = exclude.split(',')
//                Ext.each(fieldArray, function(field){
//                    if(newRec.get(field) != 0 && newRec.get(field) != ''
//                            && newRec.get(field) != '0'){
//                        saveData = true
//                        return false;
//                    }
//                })
//        }else if(appraisalStore.getUpdatedRecords().length > 0){
//            saveData = true
//        }

        if (saveData){
            Ext.Msg.show({
            title:MineralPro.config.Runtime.tabChangeConfirmationTitle,
                    msg:MineralPro.config.Runtime.tabChangeConfirmationMsg,
                    iconCls:MineralPro.config.Runtime.tabChangeConfirmationIcon,
                    buttons: Ext.Msg.YESNO,
                    scope: this,
                    width: 250,
                    fn: function(btn) {
                        if (btn === 'yes'){
                            Ext.getBody().mask('Loading...');
                            var params = appraisalStore.getProxy().extraParams
                                  params['createLength'] = appraisalStore.getNewRecords().length
                                  params['updateLength'] = appraisalStore.getUpdatedRecords().length
                                  appraisalStore.getAt(0).set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
                                  params['origRrcNumber'] = appraisalStore.getAt(0).get('origRrcNumber');
                                  params['newRecord'] = view.newAppraisal
                                  params['newCalculation'] = view.newCalculation
                                  params['updateCalc'] = false;

                                appraisalStore.getProxy().extraParams = params

                            appraisalStore.sync({
                                success: function(){
                                    loadPrevious();
                                },
                                failure: function(batch, options){
                                    Ext.getBody().unmask();
                                    appraisalStore.rejectChanges();
                                    var msg = batch.getExceptions().map(function(operation){
                                        return 'Status Text: '+operation.getError().statusText;
                                    });
                                    msg = msg.join('<br/> ');

                                    Ext.Msg.show({
                                        title: 'Oops!',
                                        msg: 'Failed to save your data <br/><span style="font-weight:bold;line-height:25px;">Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p>',
                                        icon: Ext.Msg.ERROR,
                                        buttons: Ext.Msg.OK
                                    });
                                },
                                callback: function(){
                                    
                                }
                            });
                        } else{
                            loadPrevious();
                        }
                    }
               });
           }else{
               loadPrevious();
           }

        function loadPrevious(){
            fieldStore.getProxy().extraParams = {
                selectedId: mainPanel.selectedId,
            };

            fieldStore.load({
                callback: function (records, operation, success) {
                    if(success){
                        Ext.getBody().mask('Loading...');
                        var fiedlGridSelection = fieldGrid.getSelectionModel();

                        var fieldStoreCount = fieldStore.getCount();
                        var currentIndex = fieldStore.find('rrcNumber', view.down('#rrcNumberItemId').getValue(), false, false, true);

                        if (currentIndex - 1 >= 0) {
                            currentIndex = currentIndex - 1;
                        } else {
                            currentIndex = fieldStoreCount - 1;
                        }

                        fiedlGridSelection.select(currentIndex, false, false)
                        view.down('#appraisalIndexItemId').setText(currentIndex + 1 + ' of ' + fieldStoreCount);
                        appraisalStore.getProxy().extraParams = {
                            selectedId: mainPanel.selectedId,
                            selectedRRCNumber: mainPanel.selectedRRCNumber
                        };

                        appraisalStore.load({
                            callback: function(records, operation, success){
                                if(success){
                                    Ext.getBody().mask('Loading...');
                                    // view.down('#saveToDbButtonItemId').disable(true);
                                    view.down('#calcButtonItemId').enable(true);
                                }
                                else{
                                    me.storeFailureCallback(operation)
                                }
                            }
                        });
                    }
                    else{
                        me.storeFailureCallback(operation);
                    }
                    
                }
            })
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController onPreviousButton.')
    },
    isFormValid: function(){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController isFormValid.')
        var me = this;
        var view = me.getView();
        var response = {};
        var mainPanel = view.up(view.mainPanel);
        if(view.isValid()){
            response.valid = true;
        }
        else{
            response.valid = false;
            var errorsTpl = new Ext.XTemplate(
                '<tpl if="">',
                '<ul class="field-errors-list"><tpl for="."><li><span class="error-field">{field}</span> - <span class="field-error">{error}</span></li></tpl></ul>',
                '</tpl>'
            );
            var errors = [];
            var form = view.getForm();
            var fields = form.getFields();
            fields = fields.items || [];
            Ext.each(fields,function (field) {
                var fieldName = field.getName();
                fieldName = fieldName.split(/(?=[A-Z])/).map(function(word){return word.charAt(0).toUpperCase() + word.substr(1).toLowerCase();}).join(' ');
                errors = errors.concat(
                    Ext.Array.map(field.getErrors(), function (error) {
                        return { field: fieldName, error: error }
                    })
                );
            });
            response.errMsg = errorsTpl.apply(errors);
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController isFormValid.')
        return response;
    },
    onSyncListGridData: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController onSyncListGridData.')
        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var listStore = view.down('grid').getStore();
        var record = view.getRecord();
        var isFormValid = me.isFormValid()
        var values = view.getValues();
        var recordCopy = record ? record.copy(null): model.create(values); // used to compare, instead of updating store values.
        if(!record){
            var newRecord = model.create(values);
            newRecord.phantom = true;
            appraisalDetailStore.add(newRecord);
            view.loadRecord(newRecord);   
        }
        else{
            recordCopy.set(values);
        }
        var formFields = Ext.Object.getKeys(values);
        var equal = Ext.Array.every(formFields, function(field){
            return record && record.get(field) == recordCopy.get(field);
        })
        // var dirty = listStore.getNewRecords().length > 0
        //         || listStore.getUpdatedRecords().length > 0
        //         || listStore.getRemovedRecords().length > 0;

        if (!equal && isFormValid.valid) {
            var origRrcNumber = listStore.getAt(0).get('origRrcNumber');
            if(view.newAppraisal == true){
                origRrcNumber = view.down('#rrcNumberItemId').getValue()
            }

            Ext.Msg.show({
                title: MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                msg: MineralPro.config.Runtime.syncGridMsgConfimationMsg,
                iconCls: MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                buttons: Ext.Msg.YESNO,
                scope: this,
                width: 250,
                fn: function (btn) {
                    if (btn === 'yes') {
                        Ext.getBody().mask('Loading...')
                        record = record ? record.set(values) : record;
                        listStore.getAt(0).set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
                        listStore.getProxy().extraParams = {
                            origRrcNumber: origRrcNumber,
                            updateLength: listStore.getUpdatedRecords().length,
                            newRecord: view.newAppraisal,
                            newCalculation:  view.newCalculation,
                            updateCalc: false
                        };
                        listStore.sync({
                            success: function(batch, options){
                                var fieldGrid = mainPanel.down(view.fieldGridViewAlias);
                                var fieldStore = fieldGrid.getStore();

                                var data = batch.operations[0].config.records[0];
                                var newRRC = data.get('rrcNumber');
                                var declineStores = [
                                    'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalOperatingExpStore',
                                    'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalOilDailyAvgStore',
                                    'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalGasDailyAvgStore',
                                    'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalProdDailyAvgStore'
                                ];
                                Ext.each(declineStores,function(name){
                                    var store = Ext.getStore(name);
                                    store.getProxy().extraParams.rrcNumber = newRRC;
                                    store.reload();
                                });
                                fieldStore.load({
                                    callback: function (records, operation, success) {
                                        if(success){
                                            var fiedlGridSelection = fieldGrid.getSelectionModel();

                                            var fieldStoreCount = fieldStore.getCount();
                                            var currentIndex = fieldStore.find('rrcNumber', view.down('#rrcNumberItemId').getValue(), false, false, true);

                                            fiedlGridSelection.select(currentIndex, false, false)
                                            view.down('#appraisalIndexItemId').setText(currentIndex + 1 + ' of ' + fieldStoreCount);
                                            me.resetFormValues(false)
                                            view.down('#calcButtonItemId').enable(true);
                                            
                                            if(view.newAppraisal == true){
                                                view.down('#calcButtonItemId').click();
                                                // view.down('grid').getStore().reload();
                                            }
                                            else{
                                                // var mainGrid = mainPanel.down('grid');
                                                // var mainGridCurrentIndex = mainGrid.getStore().find(mainPanel.selectedIdIndex, mainPanel.selectedId, false, false, true);
                                                // mainGrid.getSelectionModel().deselectAll();
                                                // mainGrid.getSelectionModel().select(mainGridCurrentIndex);
                                                listStore.load({
                                                    callback: function (records, operation, success) {
                                                        if(success){
                                                            view.down('#saveToDbButtonItemId').disable(true);
                                                            var mainGrid = mainPanel.down('grid');
                                                                
                                                            var mainGridCurrentIndex = mainGrid.getStore().find(mainPanel.selectedIdIndex, mainPanel.selectedId, false, false, true);
                                                            mainGrid.getSelectionModel().deselectAll();
                                                            mainGrid.getSelectionModel().select(mainGridCurrentIndex);
                                                            view.down('grid').getView().refresh();
                                                        }
                                                        else{
                                                            me.storeFailureCallback(operation);
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                        else{
                                            me.storeFailureCallback(operation);
                                        }
                                        
                                   }
                                });
                            },
                            failure: function(batch, options){
                                Ext.getBody().unmask();
                                listStore.rejectChanges();
                                listStore.getAt(0).set(values);
                                listStore.getAt(0).dirty = true;
                                var msg = batch.getExceptions().map(function(operation){
                                    return 'Status Text: '+operation.getError().statusText;
                                });
                                msg = msg.join('<br/> ');
                                
                                Ext.Msg.show({
                                    title: 'Oops!',
                                    msg: 'Failed to save your data <br/><span style="font-weight:bold;line-height:25px;">Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p>',
                                    icon: Ext.Msg.ERROR,
                                    buttons: Ext.Msg.OK
                                });
                            }
                        });
                        
                    }
                }
            });
        }
        else if(!isFormValid.valid){
            Ext.Msg.show({
                title: 'Please check your input data',
                icon: Ext.Msg.ERROR,
                msg: isFormValid.errMsg,
                buttons: Ext.Msg.OK
            });
        }
        else {
            Ext.Msg.show({
                iconCls: MineralPro.config.Runtime.listSyncMsgErrorIcon,
                title: MineralPro.config.Runtime.listSyncMsgErrorTitle,
                msg: MineralPro.config.Runtime.listSyncMsgErrorMsg,
                buttons: Ext.Msg.OK
            });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController onSyncListGridData.')
    },
    onRRCNumberCheckExisting: function (e, event, opt) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController onRRCNumberCheckExisting.')

        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var fieldStore = mainPanel.down(view.fieldGridViewAlias).getStore();
        var appraisalStore = view.down('grid').getStore();

        setTimeout(function () {
            if (view.id.toLowerCase().indexOf("edit") == -1) {

                fieldStore.getProxy().extraParams = {
                    selectedId: mainPanel.selectedId,
                };

//                fieldStore.load({
//                    callback: function () {
                        if(fieldStore.count()>0){
                            if(!(!view.newAppraisal && appraisalStore.getAt(0).get('rrcNumber') == e.value)){
                                if (fieldStore.findRecord(e.name, e.value, 0, false, false, true)) {
                                    Ext.Msg.show({
                                        iconCls: Minerals.config.Runtime.appraisalExistRRCNumberAlertIcon,
                                        title: Minerals.config.Runtime.appraisalExistRRCNumberAlertTitle,
                                        msg: Minerals.config.Runtime.appraisalExistRRCNumberAlertMsg,
                                        buttons: Ext.Msg.OK,
                                        fn: function () {
                                            e.focus();
                                        }
                                    });
                                }
                            }
                        }
//                    }
//                })
            }
        }, 200)

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController onRRCNumberCheckExisting.')
    },
    onAfterLoadAppraisal: function (e, event, opt) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController onAfterLoadAppraisal.')

        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var appraisalStore = view.down('grid').getStore();
        var fieldGrid = mainPanel.down(view.fieldGridViewAlias);
        var fieldStore = fieldGrid.getStore();

        var subjectTypeCmp = view.down('#subjectTypeItemId');
        var subjectType = mainPanel.itemId=="ManageLease" ? "Lease" : "Unit";
        if(subjectTypeCmp){subjectTypeCmp.fireEvent('change', subjectTypeCmp, subjectType, '',{});}
        

        Ext.each(view.query('field:not(hiddenfield):not(displayfield)'),function(field){
            if(field.tabIndex > 0 && field.tabIndex !== -1){
            field.inputEl.dom.tabIndex = field.initialConfig.tabIndex;
            }
        });

        Ext.getBody().mask('Loading...')
        me.resetFormValues(false)
        fieldStore.getProxy().extraParams = {
            selectedId: mainPanel.selectedId,
        };

        fieldStore.load({
            callback: function (records, operation, success) {
                if(success){
                    Ext.getBody().mask('Loading...')
                    var fiedlGridSelection = fieldGrid.getSelectionModel();
                    var fieldStoreCount = fieldStore.getCount();
                    var currentIndex = 0;

                    if (fieldStoreCount > 0) {

                        var currentIndex = fieldStore.find('rrcNumber', view.down('#rrcNumberItemId').getValue(), false, false, true);

                        if (currentIndex == -1) {
                            currentIndex = 0;
                            fiedlGridSelection.select(currentIndex, false, false);

                           // appraisalStore.getProxy().extraParams = {
                           //     selectedId: mainPanel.selectedId,
                           //     selectedRRCNumber: mainPanel.selectedRRCNumber
                           // };
                            appraisalStore.load();
                        }else{
                            Ext.getBody().unmask()
                        }
                        view.down('#appraisalIndexItemId').setText(currentIndex + 1 + ' of ' + fieldStoreCount);
                    } else {
                        Ext.getBody().unmask()
                        view.down('#appraisalIndexItemId').setText(currentIndex + ' of ' + fieldStoreCount);
                    }
                }
                else{
                    me.storeFailureCallback(operation);
                }
            }
        })

        var declineGridArr = view.down('#declineTypeItemId').query('grid');

        Ext.each(declineGridArr, function(grid){
            var store = grid.getStore();
                store.getProxy().extraParams = {
                    selectedId: mainPanel.selectedId,
                    rrcNumber: mainPanel.selectedRRCNumber,
                    subjectTypeCd: mainPanel.subjectTypeCd
                }
                store.load({
                    callback: function(records, operation, success){
                        if(success){

                        }
                        else{
                            me.storeFailureCallback(operation);
                        }
                    }
                });
        })

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController onAfterLoadAppraisal.')
    },

    onChangeData: function (key, newValue, oldValue, eOpts) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController onChangeData.')
        var me = this;
        var view = me.getView();

        var appraisalStore = view.down('grid').getStore();
        var appraisalData = appraisalStore.getAt(0)
        var keyValue = ''
        var storeValue=''

        // var fieldCalc = 'maxYearLife,productionEquipmentCount,'
        //     +'productionWellSchedule,productionEquipmentValue,serviceEquipmentCount,'
        //     +'serviceWellSchedule,serviceEquipmentValue,injectionEquipmentCount,'
        //     +'injectionWellSchedule,injectionEquipmentValue,'
        //     +'discountRate,grossOilPrice,grossProductPrice,'
        //     +'grossWorkingInterestGasPrice,grossRoyaltyInterestGasPrice,operatingExpense,'
        //     +'dailyAverageOil,dailyAverageGas,dailyAverageProduct';

        // var fieldCalc = 'productionEquipmentCount,'
        //     +'productionWellSchedule,productionEquipmentValue,serviceEquipmentCount,'
        //     +'serviceWellSchedule,serviceEquipmentValue,injectionEquipmentCount,'
        //     +'injectionWellSchedule,injectionEquipmentValue,'
        //     +'discountRate,grossOilPrice,grossProductPrice,'
        //     +'grossWorkingInterestGasPrice,grossRoyaltyInterestGasPrice,operatingExpense,'
        //     +'dailyAverageOil,dailyAverageGas,dailyAverageProduct';
        // var fieldCalcArray = fieldCalc.split(',')
        var values = view.getValues();

        if(key.value){
            keyValue = key.value
        }
        if(appraisalStore.count() > 0){
            if(appraisalStore.getAt(0).get(key.name)){
                storeValue = appraisalData.get(key.name)
            }
        }
        if (keyValue.toString() != storeValue.toString()) {
            key.setFieldStyle('background-color: coral; color: white');

            var enableSave = true;
            // if(appraisalData){
            //      Ext.each(fieldCalcArray, function(field){
            //              if(appraisalData.get(field).toString() != values[field].toString()){
            //                      enableSave = false;
            //                      return false;
            //                  }
            //              })
            //          }
            if(view.newCalculation != true){
                if(enableSave){
                    view.down('#saveToDbButtonItemId').enable(true);
                    if(!view.newAppraisal){
                        view.down('#calcButtonItemId').enable(true);
                    }
                }else{
                    view.down('#saveToDbButtonItemId').disable(true)
                }
            }else{
                view.down('#saveToDbButtonItemId').enable(true)
            }

        } else {
            if(key.readOnly){
                key.setFieldStyle('background-color: white; color: #000');

            }else{
                key.setFieldStyle('background-color: white; color:black;');
            }
            // var field = 'rrcNumber,fieldId,'
            // +'wellDescription,wellTypeCd,comment';

            var field = 'rrcNumber,fieldId,wellDescription,wellTypeCd,comment,maxYearLife,productionEquipmentCount,'
            +'productionWellSchedule,productionEquipmentValue,serviceEquipmentCount,'
            +'serviceWellSchedule,serviceEquipmentValue,injectionEquipmentCount,'
            +'injectionWellSchedule,injectionEquipmentValue,'
            +'discountRate,grossOilPrice,grossProductPrice,'
            +'grossWorkingInterestGasPrice,grossRoyaltyInterestGasPrice,operatingExpense,'
            +'dailyAverageOil,dailyAverageGas,dailyAverageProduct';

            var fieldArray = field.split(',');
            var enableSave = false;

            if(appraisalData){
                Ext.each(fieldArray, function(field){
                    if(appraisalData.get(field).toString() != values[field].toString()){
                        enableSave = true;
                        return false;
                    }
                })

                // Ext.each(fieldCalcArray, function(field){
                //      if(appraisalData.get(field).toString() != values[field].toString()){
                //          enableSave = false;
                //          return false;
                //      }
                //  })
            }

            if(view.newCalculation != true){
                if(enableSave){
                    view.down('#saveToDbButtonItemId').enable(true)
                }else{
                    view.down('#saveToDbButtonItemId').disable(true)
                }
            }else{
                view.down('#saveToDbButtonItemId').enable(true)
            }
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController onChangeData.')
    },

    onCalcButton: function(resetRecord){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController onCalcButton.')
        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var calcAppraisalStore = Ext.StoreMgr.lookup(view.calcAppraisalStore);
        var gridStore = view.down('grid').getStore();
        var record = view.getRecord();
        var values = view.getValues();
        var recordCopy = record.copy(null);
        var resetToOrigValues = function(){
            MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController onCalcButton resetToOrigValues.')
            // var fieldsReverted = record.copyFrom(recordCopy);
            record.set(recordCopy.getData()); // for some reason record.copyfrom does not copy all data from the passed data model.
            MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController onCalcButton resetToOrigValues.')
        };
        record.set(values);
        if(calcAppraisalStore.proxy.api.read.indexOf('subjectTypeCd') != -1){
            calcAppraisalStore.getProxy().extraParams = {
                selectedId: mainPanel.selectedId,
                selectedRRCNumber: mainPanel.selectedRRCNumber,
                rowUpdateUserid: MineralPro.config.Runtime.idAppUser,
                newAppraisal: view.newAppraisal,
                maxYear: view.down('#maxYearItemId').getValue()
            };
        }else{
            calcAppraisalStore.getProxy().extraParams = {
                subjectTypeCd: gridStore.getAt(0).get('subjectTypeCd'),
                selectedId: mainPanel.selectedId,
                selectedRRCNumber: mainPanel.selectedRRCNumber,
                rowUpdateUserid: MineralPro.config.Runtime.idAppUser,
                newAppraisal: view.newAppraisal,
                maxYear: view.down('#maxYearItemId').getValue()
            };
        }

        var dirty = gridStore.getNewRecords().length > 0
                || gridStore.getUpdatedRecords().length > 0
                || gridStore.getRemovedRecords().length > 0;

        if(dirty){
            var isFormValid = me.isFormValid();
            if(isFormValid.valid){
                var origRrcNumber = gridStore.getAt(0).get('origRrcNumber');
                gridStore.getAt(0).set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
                gridStore.getProxy().extraParams = {
                    origRrcNumber: origRrcNumber,
                    newRecord: false,
                    newCalculation: false,
                    updateCalc: true
                };

                gridStore.sync({
                    success: function(){
                        resetToOrigValues();
                        loadCalc()
                    },
                    failure: function(batch, options){
                        resetToOrigValues();
                        var records = batch.getOperations().map(function(op){
                            return op.getRecords();
                        });
                        if(records.length && typeof records[0].getData == 'function'){
                            gridStore.getAt(0).set(records[0].getData());
                        }
                        Ext.getBody().unmask();
                        var msg = batch.getExceptions().map(function(operation){
                            return 'Status Text: '+operation.getError().statusText;
                        });
                        msg = msg.join('<br/> ');
                        
                        Ext.Msg.show({
                            title: 'Oops!',
                            msg: 'Failed to save your data <br/><span style="font-weight:bold;line-height:25px;">Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p>',
                            icon: Ext.Msg.ERROR,
                            buttons: Ext.Msg.OK
                        });
                    }
                })
                // record.set(record.previousValues);
            }
            else{
                resetToOrigValues();
                Ext.Msg.show({
                    title: 'Please check your input data',
                    icon: Ext.Msg.ERROR,
                    msg: isFormValid.errMsg,
                    buttons: Ext.Msg.OK
                });
            }
        }else{
            resetToOrigValues();
            loadCalc()
        }

        function loadCalc(){
            calcAppraisalStore.getProxy().setTimeout(120000);// 2 min timeout
            calcAppraisalStore.load({
                callback: function(record, operation, success){
                    if(success){
                        var updateCalcFieldValue = function (fieldArray,  record){
                            var exclude = [
                                'subjectId','subjectName','rrcNumber','fieldId','wellDepth','wellDepth','x','legalDescription',
                                'wellDescription', 'wellTypeCd', 'maxYearLife','note','productionEquipmentCount',
                                'productionWellSchedule', 'productionEquipmentValue', 'serviceEquipmentCount',
                                'serviceWellSchedule', 'serviceEquipmentValue', 'injectionEquipmentCount',
                                'injectionWellSchedule', 'injectionEquipmentValue', 'maxYearLife',
                                'discountRate', 'grossOilPrice', 'grossProductPrice',
                                'grossWorkingInterestGasPrice', 'grossRoyaltyInterestGasPrice', 'operatingExpense',
                                'dailyAverageOil', 'dailyAverageGas', 'dailyAverageProduct'
                            ]
                            view.calcToFalse = true
                            Ext.each(fieldArray, function(field){
                                if(exclude.indexOf(field.name) == -1){
                                    var recordval = record.get(field.name) || ''
                                    field.setValue(recordval)
                                    var fieldval = field.getValue();
                                    fieldval = fieldval ? fieldval.toString() : '';
                                    view.calcToFalse = fieldval == recordval;
                                }
                            })
                            // var exclude = '[subjectId],[subjectName],[rrcNumber],[fieldId],[wellDepth],[gravityCd],[legalDescription],'
                            //     +'[wellDescription],[wellTypeCd],[maxYearLife],[note],[productionEquipmentCount],'
                            //     +'[productionWellSchedule],[productionEquipmentValue],[serviceEquipmentCount],'
                            //     +'[serviceWellSchedule],[serviceEquipmentValue],[injectionEquipmentCount],'
                            //     +'[injectionWellSchedule],[injectionEquipmentValue],[maxYearLife],'
                            //     +'[discountRate],[grossOilPrice],[grossProductPrice],'
                            //     +'[grossWorkingInterestGasPrice],[grossRoyaltyInterestGasPrice],[operatingExpense],'
                            //     +'[dailyAverageOil],[dailyAverageGas],[dailyAverageProduct],'
                            // Ext.each(fieldArray, function(field){
                            //     if(exclude.indexOf('['+field.name+']') == -1){
                            //         field.setValue(record.get(field.name))
                            //         view.calcToFalse = true;
                            //         if (field.getValue().toString() != record.get(field.name).toString()){
                            //             view.calcToFalse = false;
                            //         }
                            //     }
                            // })
                        };
                        var viewModelRecord = view.getRecord();
                        // viewModelRecord.dirty=true;
                        if(record[0]){
                            var textArray = view.query('textfield');
                            var numberArray = view.query('numberfield');
                            var comboArray = view.query('combobox');

                            updateCalcFieldValue(textArray, record[0]);
                            updateCalcFieldValue(numberArray, record[0]);
                            updateCalcFieldValue(comboArray, record[0]);
                            view.newCalculation = true;
                            if(view.calcToFalse){
                                view.newCalculation = false;
                            }
                            if(view.newCalculation == true && view.down('#saveToDbButtonItemId').isDisabled()){
                                view.down('#saveToDbButtonItemId').enable(true);
                            }
                            // if(view.newCalculation == true && view.down('#saveToDbButtonItemId')){
                            //     if(typeof view.down('#saveToDbButtonItemId').enable == 'function'){
                            //         view.down('#saveToDbButtonItemId').enable(true);
                            //     }
                            // }

                            if(view.newAppraisal == true){
                                me.resetFormValues(false);
                                view.newAppraisal = false;
                                view.newCalculation = false;
                                view.down('#saveToDbButtonItemId').disable(true);
                                gridStore.reload({
                                    callback: function(record, operation, success){
                                        if(success){
                                            
                                        }
                                        else{
                                            me.storeFailureCallback(operation);
                                        }
                                    }
                                });
                                //need this part to reset header values
                                var mainGrid = mainPanel.down('grid');
                                var mainGridCurrentIndex = mainGrid.getStore().find(mainPanel.selectedIdIndex, mainPanel.selectedId, false, false, true);
                                mainGrid.getSelectionModel().deselectAll();
                                mainGrid.getSelectionModel().select(mainGridCurrentIndex);
                                mainGrid.getView().refresh();
                            }
                        }else{
                            Ext.Msg.show({
                                iconCls: Minerals.config.Runtime.appraisalCalcAlertIcon,
                                title: Minerals.config.Runtime.appraisalCalcAlertTitle,
                                msg: Minerals.config.Runtime.appraisalCalcAlertMsg,
                                buttons: Ext.Msg.OK,
                            });
                            view.newCalculation= false
                            view.down('#saveToDbButtonItemId').disable(true);
                        }
                    }
                    else{
                        Ext.getBody().unmask();
                        var descriptiveErrorMsg = operation.getError().statusText;
                        Ext.Msg.show({
                            title: 'Oops!',
                            msg: 'Something went wrong while processing your request.<p><span style="font-weight:bold;">Error Details:</span> <span style="color:red;">'+descriptiveErrorMsg+'</span><p>',
                            buttons: Ext.Msg.OK
                        });
                    }
                }
            })
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController onCalcButton.')

    },

    resetFormValues: function(resetRecord){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController resetFormValues.')
        var me = this;
        var view = me.getView();

        var appraisalStore = view.down('grid').getStore();

        var record = appraisalStore.getAt(0);
        var textArray = view.query('textfield');
        var numberArray = view.query('numberfield');
        var comboArray = view.query('combobox');

        resetField(textArray, resetRecord, '0');
        resetField(numberArray, resetRecord, '0');
        resetField(comboArray, resetRecord, '');

        function resetField(fieldArray, resetRecord, value) {
            var exclude = '[operatorName],[agencyName],[subjectType],[subjectId],[name],[legalDescription]'
            var withDecimal = '[grossOilPrice],[grossProductPrice],[grossWorkingInterestGasPrice],'
                            +'[grossRoyaltyInterestGasPrice],[dailyAverageOil]'

            Ext.each(fieldArray, function (field) {
                if (resetRecord && exclude.indexOf('[' + field.name + ']') == -1) {
                    if (resetRecord && withDecimal.indexOf('[' + field.name + ']') != -1) {
                        value = '0.00'
                    }else{
                        value = '0'
                    }
                    if(record){
                        record.set(field.name, value)
                    }
                    field.setValue(value)
//                     console.log(field.name + '; ' + value)
                }
                if(field.readOnly){
                    field.setFieldStyle('background-color: white; color: #000');
                }else{
                    field.setFieldStyle('background-color: white; color:black;');
                }
//                field.setFieldStyle('background-color: white;');
            })
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController resetFormValues.')

    },

     onSelectFieldName: function (combo, rec, eOpts) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController onSelectFieldName.')
        var me = this;
        var view = me.getView();
        view.down('#wellDepthItemId').setValue(rec.get('wellDepth'));
        view.down('#gravityCdItemId').setValue(rec.get('gravityCd'));
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController onSelectFieldName.')
    },
    onValidateDeclineBeforeEdit: function(editor, context, eOpts){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController onValidateDeclineBeforeEdit.')
        var me = this;
        var view = me.getView();
        view.down('#calcButtonItemId').disable()
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController onValidateDeclineBeforeEdit.')
        return true;
    },
    onValidateDecline: function (editor, context) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController onValidateDecline.')

        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);
        var appraisalStore = view.down('grid').getStore();
        var store = context.grid.getStore();

        store.getProxy().extraParams = {
            selectedId: mainPanel.selectedId,
            subjectTypeCd: appraisalStore.getAt(0).get('subjectTypeCd'),
            rrcNumber: view.down('#rrcNumberItemId').getValue(), //view.getRecord().get('rrcNumber'),
            createLength: store.getNewRecords().length,
            updateLength: store.getUpdatedRecords().length,
            queueUserid: MineralPro.config.Runtime.idAppUser
        }
        if(store.getModifiedRecords().length || store.getRemovedRecords().length){
            /* Increment count only if store has dirty data to sync to the database*/
            view.down('#calcButtonItemId').disable();
            me.counter += 1;
        }
        else if(!editor.editing){
            view.down('#calcButtonItemId').enable();
        }

        store.sync({
            success: function(batch, options){
                Ext.getBody().unmask();
                store.commitChanges();
            },
            failure: function(batch, options){
                Ext.getBody().unmask();
                store.rejectChanges();
                var msg = batch.getExceptions().map(function(operation){
                    return 'Status Text: '+operation.getError().statusText;
                });
                msg = msg.join('<br/> ');

                Ext.Msg.show({
                    title: 'Oops!',
                    msg: 'Failed to save your data <br/><span style="font-weight:bold;line-height:25px;">Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p>',
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK
                });
            },
            callback: function(){
                /*allways called regardless if successful or not*/
                me.counter -= 1;
                if(me.counter == 0 && !view.newAppraisal && !editor.editing){
                    view.down('#calcButtonItemId').enable();
                }
                Ext.getBody().unmask();
            }
        });

        var lastRec = store.getAt(store.getCount()-1)
        var curRec = context.record
        if(lastRec.get('decline') != '0' || lastRec.get('declineYears') != '0'){
            var recDecline = Ext.create('Minerals.model.MineralValuation.AppraisalModel')
            recDecline.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
            store.insert(store.getCount()+1, recDecline)
        }

        if(curRec.get('decline') == '0' && curRec.get('declineYears') == '0'){
             if(context.rowIdx < store.getCount()-1){
                 store.remove(context.record);
             }
        }else{
            curRec.set('oriDecline', curRec.get('decline'));
            curRec.set('oriDeclineYears', curRec.get('declineYears'));
        }
        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController onValidateDecline.')
    },
    onYearByYearAnalysisButton: function (editor, context) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController onYearByYearAnalysisButton.')

                var me = this;
                var view = me.getView();
                var mainPanel = view.up(view.mainPanelAlias);

                Ext.Ajax.request({
                    url: '/api/Minerals/MineralValuation/MineralValuationAppraisal/ReadYearAnalysisLink',
                    method: 'GET',
                    params: {
                        appraisalYear: MineralPro.config.Runtime.appraisalYear
                    },
                    success: function (response, e) {
                        var jsonData = Ext.util.JSON.decode(response.responseText);
                        var reportID = jsonData.data[0].reportID;

                        // var url = window.location.origin + '/CrystalReport/'
                        //         +reportID+
                        //         '/?year='+MineralPro.config.Runtime.appraisalYear+
                        //         '&account_num_from='+mainPanel.selectedId+
                        //         '&account_num_to='+mainPanel.selectedId+
                        //         '&operator_num_from=0&operator_num_to=9999999&rrc_num_from='+mainPanel.selectedRRCNumber+
                        //         '&rrc_num_to'+mainPanel.selectedRRCNumber

                        // window.open(url);

                         Ext.Ajax.request({
                            waitMsg: 'Generating Year by Year Analysis Preview...',
                            url: '/CrystalReport/'+reportID+'',
                            method:'GET',
                            params: {
                               year: MineralPro.config.Runtime.appraisalYear,
                               account_num_from: mainPanel.selectedId,
                               account_num_to: mainPanel.selectedId,
                               operator_num_from: '0',
                               operator_num_to: '9999999',
                               rrc_num_from: mainPanel.selectedRRCNumber,
                               rrc_num_to: mainPanel.selectedRRCNumber,
                            },
                            success: function(response){
                                var el = Ext.DomHelper.append(Ext.getBody(), {
                                    tag:          'a',
                                    href:          response.request.requestOptions.url,
                                    modal:true,
                                    target: '_blank'
                                });
                                el.click();
                            },
                            failure: function(response){
                                var msg = 'Status Text: '+response.statusText;
                                Ext.Msg.show({
                                    iconCls: Minerals.config.Runtime.appraisalCalcAlertIcon,
                                    title: 'Error!',
                                    closeAction: 'hide',
                                    msg: 'Something went wrong while processing your request.<br/><span style="font-weight:bold;line-height:25px;">Error Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p><p style="font-weight:bold;font-style:italic;color:red;">*** If error persist, please check you\'re server.</p>',
                                    buttons: Ext.Msg.OK,
                                });
                            }
                        });
                    },
                    failure: function(response){
                        var msg = 'Status Text: '+response.statusText;
                        Ext.Msg.show({
                            iconCls: Minerals.config.Runtime.appraisalCalcAlertIcon,
                            title: 'Error!',
                            closeAction: 'hide',
                            msg: 'Something went wrong while processing your request.<br/><span style="font-weight:bold;line-height:25px;">Error Details:</span><p style="margin-top:5px;margin-left: 15px;color: red;">'+msg+'</p><p style="font-weight:bold;font-style:italic;color:red;">*** If error persist, please check you\'re server.</p>',
                            buttons: Ext.Msg.OK,
                        });
                    }

                });

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController onYearByYearAnalysisButton.')
    },

    onMakeValue0: function (editor, context) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppraisalFormViewController onMakeValue0.')

        var me = this;
        var view = me.getView();
        var mainPanel = view.up(view.mainPanelAlias);

        view.down('#productionEquipmentCountItemId').setValue(0);
        view.down('#serviceEquipmentCountItemId').setValue(0);
        view.down('#injectionEquipmentCountItemId').setValue(0);
        view.down('#operatingExpenseItemId').setValue(0);
        view.down('#dailyAverageOilItemId').setValue(0);
        view.down('#dailyAverageGasItemId').setValue(0);
        view.down('#dailyAverageProductItemId').setValue(0);

        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppraisalFormViewController onMakeValue0.')
    },
});
