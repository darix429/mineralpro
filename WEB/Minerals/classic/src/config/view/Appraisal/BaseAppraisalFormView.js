Ext.define('Minerals.config.view.Appraisal.BaseAppraisalFormView', {
    extend: 'MineralPro.config.view.BaseView',
    requires: ['Ext.app.ViewModel','Minerals.config.view.Appraisal.BaseAppraisalFormViewController'],
    controller: 'BaseAppraisalFormViewController',

    layout: 'hbox',
    firstFocus: 'rrcNumberItemId',
    newAppraisal: false,
    newCalculation: false,

    appraisalModel: 'Minerals.model.MineralValuation.AppraisalModel',
    scrollable: true,
    width: 600,
    defaults: {
        margin: '2 2 2 2',
    },
    viewModel: {
        data: {
            isLease: ''
        }
    },
    initComponent: function () {
        var me = this;
        var appraisalStore = Ext.StoreMgr.lookup(me.appraisalStore);
        var mainPanel = me.up(me.mainPanelAlias);
        appraisalStore.on({
            load: function (store, records, successful, operation, eOpts) {
                Ext.getBody().mask('Loading...')

                var comboSubjectId = Ext.StoreMgr.lookup('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSubjectIdStore');
                var comboSubjectName = Ext.StoreMgr.lookup('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSubjectNameStore');

                comboSubjectId.reload({
                    callback: function(){
                        comboSubjectName.reload({
                            callback: function(){
                            var mainPanel = Ext.getCmp(me.mainPanelId)
                            var newRec=false;
                            if (records){
                                if (records[0]){
                                    me.loadRecord(records[0]);
                                    me.down('#calcButtonItemId').enable(true);
                                    me.newAppraisal = false
                                    me.newCalculation = false
                                }else{
                                   newRec = true;
                                   me.newCalculation = true;
                                }
                            } else {
                                newRec = true;
                                me.newCalculation = true;
                            }
                            if(newRec){
                                var record = Ext.create(me.appraisalModel);

            //                    console.log(MineralPro.config.Runtime.idAppUser)
                                var selectedRecord = mainPanel.down('grid').getSelectionModel().getSelection()[0]
                                record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser);
                                record.set('operatorName', selectedRecord.get('operatorName'))
                                record.set('operatorId', selectedRecord.get('operatorId'))
                                record.set('subjectId', mainPanel.selectedId)
                                record.set('name', mainPanel.selectedName)
                                var subjectType = 'Unit'
                                if(mainPanel.subjectTypeCd == '2'){
                                    var subjectType = 'Lease'
                                }
                                record.set('subjectType', subjectType)

                                me.loadRecord(record);
                                appraisalStore.insert(0,record);
            //                    me.down('#saveToDbButtonItemId').enable(true);
                                me.down('#calcButtonItemId').disable(true);

                                setTimeout(function(){
                                    me.down('#newAppraisalButton').click();
                                },200)
                            }
                            me.getController().onAfterLoadAppraisal();
                            }
                        })
                    }
                })
            },
        });

        appraisalStore.on({
            beforeload: function () {
                var params = this.getProxy().extraParams
                params['newRecord'] = me.newAppraisal;
                params['idAppUser'] = MineralPro.config.Runtime.idAppUser;
                params['selectedId'] = mainPanel.selectedId;
                params['selectedRRCNumber'] = mainPanel.selectedRRCNumber;
                this.getProxy().extraParams = params;
            },
        })

        appraisalStore.on({
            beforesync: function () {
                var params = this.getProxy().extraParams
                params['newRecord'] = me.newAppraisal;
                params['idAppUser'] = MineralPro.config.Runtime.idAppUser;
                params['selectedId'] = mainPanel.selectedId;
                params['selectedRRCNumber'] = mainPanel.selectedRRCNumber;
                this.getProxy().extraParams = params;
            },
        })

        me.items = [{
//                xtype: 'BaseGridView',
                xtype: 'grid',
                store: me.appraisalStore,
                hidden: true,
//                controller: '',
            }, {
                xtype: 'fieldset',
                padding: '0 5 5 5',
                margin: '-5 0 0 0',
                minWidth: 330,
                maxWidth: 340,
                flex: 1,
                title: 'Basic Information',
                defaults: {
                    maskRe: /[^'\^]/,
                    labelAlign: 'right',
                    margin: '0 0 1 0',
                    width: '100%',
                    height: 25,
                    enableKeyEvents: true,
                    listeners: {
                        change: 'onChangeData',
                    }
                },
                items: [{
                        tabIndex: 1,
                        fieldLabel: 'Subject Type',
                        xtype: 'combobox',
                        itemId: 'subjectTypeItemId',
                        name: 'subjectType',
                        store: [['Unit', 'Unit'], ['Lease', 'Lease']],
                        style: {
                            opacity: 1
                        },
                        fieldStyle: 'font-weight:bold;',
                        bind: {
                            value: '{isLease}'
                        },
                        disabled: true,
                        listeners: {
                            change: function(combo, newValue, oldValue, eOpts){
                                me.getController().onChangeData(combo, newValue, oldValue, eOpts);
                                var subjectIdStore = me.down('#subjectIdItemId').getStore();
                                var subjectNameStore = me.down('#nameItemId').getStore();

                                subjectIdStore.getProxy().extraParams = {
                                    subjectType: combo.value
                                };

                                subjectNameStore.getProxy().extraParams = {
                                    subjectType: combo.value
                                };
                                subjectIdStore.reload({
                                    callback: function (records, operation, success) {
                                        if(success){
                                            subjectNameStore.reload({
                                                callback: function (records, operation, success) {
                                                    var mainPanel = Ext.getCmp(me.mainPanelId)
                                                    if(success){
                                                        var selectedId = mainPanel.selectedId || '';
                                                        combo.nextSibling('#subjectIdItemId').setValue('');//just to clear out the previously set value. Just to be sure that change event will be triggered
                                                        combo.nextSibling('#subjectIdItemId').setValue(selectedId);
                                                    }
                                                    else{
                                                        Ext.Msg.show({
                                                            title: 'Oops!',
                                                            iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                                                            msg: 'Something went wrong while loading subjectNames. Would you like to try again?',
                                                            buttons: Ext.Msg.YESNO,
                                                            fn: function(btn){
                                                                if(btn=='yes'){
                                                                    subjectNameStore.reload();
                                                                }
                                                            }
                                                        });
                                                    }
                                                }
                                            });
                                        }
                                        else{
                                            Ext.Msg.show({
                                                title: 'Oops!',
                                                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                                                msg: 'Something went wrong while loading subjectIDs. Would you like to try again?',
                                                buttons: Ext.Msg.YESNO,
                                                fn: function(btn){
                                                    if(btn=='yes'){
                                                        subjectIdStore.reload();
                                                    }
                                                }
                                            });
                                        }
                                    }
                                });  
                            }
                        }
                    }, {
                        tabIndex: 2,
                        fieldLabel: 'Subject Id',
                        xtype: 'combobox',
                        selectOnFocus: true,
                        itemId: 'subjectIdItemId',
                        name: 'subjectId',
                        displayField: 'subjectId',
                        valueField: 'subjectId',
                        store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSubjectIdStore',
                        queryMode: 'local',
                        style: {
                            opacity: 1
                        },
                        fieldStyle: 'font-weight:bold;',
                        forceSelection: true,
                        disabled: true,
                        listeners: {
                            // select: function (combo, record) {
                            //     var comboStore = combo.getStore();
                            //     var rec = comboStore.getAt(comboStore.find('subjectId', combo.getValue(), false, false, true));
                            //     if(rec && rec.get('subjectType') && rec.get('subjectName') && rec.get('subjectTypeCd')){
                            //         me.down('#subjectTypeItemId').setValue(rec.get('subjectType'))
                            //         me.down('#nameItemId').setValue(rec.get('subjectName'))
                            //         me.down('#subjectTypeCdItemId').setValue(rec.get('subjectTypeCd'))
                            //         me.down('#rrcNumberItemId').focus()
                            //     }
                            // },
                            change: function(combo, newValue, oldValue, eOpts){
                                me.getController().onChangeData(combo, newValue, oldValue, eOpts);

                                var comboStore = combo.getStore();
                                var rec = comboStore.getAt(comboStore.find('subjectId', combo.getValue(), false, false, true));
                                if(rec && rec.get('subjectType') && rec.get('subjectName') && rec.get('subjectTypeCd')){
                                    me.down('#subjectTypeItemId').setValue(rec.get('subjectType'))
                                    // if(rec.get('subjectType').toLowerCase() == 'lease'){
                                    //     var mainPanel = me.up(me.mainPanelAlias);
                                    //     var appraisalList = mainPanel.down(me.appraisalListViewAlias);
                                    //     var appraisalListStore = appraisalList.getStore();
                                    //     var findRec = appraisalListStore.findRecord('subjectId',rec.get('subjectId'))
                                    //     appraisalList.getSelectionModel().select(findRec);
                                    //     if(findRec){
                                    //         me.down('textarea[name=legalDescription]').setValue(findRec.get('legalDescription'))
                                    //     }
                                    // }
                                    me.down('#nameItemId').suspendEvent('change')
                                    me.down('#nameItemId').setValue(rec.get('subjectName'))
                                    me.down('#nameItemId').resumeEvent('change')
                                    me.down('#subjectTypeCdItemId').setValue(rec.get('subjectTypeCd'))
                                    me.down('#rrcNumberItemId').focus()
                                }
                            }
                        }
                    }, {
                        tabIndex: 3,
                        fieldLabel: 'Subject Name',
                        xtype: 'combobox',
                        name: 'name',
                        displayField: 'subjectName',
                        valueField: 'subjectName',
                        store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSubjectNameStore',
                        itemId: 'nameItemId',
                        disabled: true,
                        queryMode: 'local',
                        style: {
                            opacity: 1
                        },
                        fieldStyle: 'font-weight:bold;',
                        forceSelection: true,
                        selectOnFocus: true,
                        listeners: {
                            // select: function (combo, record) {
                            //     var comboStore = combo.getStore();
                            //     var rec = comboStore.getAt(comboStore.find('subjectId', combo.getValue(), false, false, true));
                            //     if(rec && rec.get('subjectType') && rec.get('subjectName') && rec.get('subjectTypeCd')){
                            //         me.down('#subjectTypeItemId').setValue(rec.get('subjectType'))
                            //         me.down('#nameItemId').setValue(rec.get('subjectName'))
                            //         me.down('#subjectTypeCdItemId').setValue(rec.get('subjectTypeCd'))
                            //         me.down('#rrcNumberItemId').focus()
                            //     }
                            // },
                            change: function(combo, newValue, oldValue, eOpts){
                                    
                                me.getController().onChangeData(combo, newValue, oldValue, eOpts);

                                var comboStore = combo.getStore();
                                var rec = comboStore.findRecord('subjectName', newValue);
                                if(rec && rec.get('subjectType') && rec.get('subjectName') && rec.get('subjectTypeCd')){
                                    me.down('#subjectTypeItemId').setValue(rec.get('subjectType'))
                                    // if(rec.get('subjectType').toLowerCase() == 'lease'){
                                    //     var mainPanel = me.up(me.mainPanelAlias);
                                    //     var appraisalList = mainPanel.down(me.appraisalListViewAlias);
                                    //     var appraisalListStore = appraisalList.getStore();
                                    //     var findRec = appraisalListStore.findRecord('subjectId',rec.get('subjectId'))
                                    //     appraisalList.getSelectionModel().select(findRec);
                                    //     if(findRec){
                                    //         me.down('textarea[name=legalDescription]').setValue(findRec.get('legalDescription'))
                                    //     }
                                    // }
                                    me.down('#subjectIdItemId').suspendEvent('change');
                                    me.down('#subjectIdItemId').setValue(rec.get('subjectId'));
                                    me.down('#subjectIdItemId').resumeEvent('change');
                                    me.down('#subjectTypeCdItemId').setValue(rec.get('subjectTypeCd'))
                                    me.down('#rrcNumberItemId').focus()
                                }
                            }
                        }
                    }, {
                        xtype: 'hidden',
                        name: 'subjectTypeCd',
                        itemId: 'subjectTypeCdItemId'
                    }, {
                        fieldLabel: 'RRC Number',
                        xtype: 'numberfield',
                        minValue: 0,
                        maxValue: 2147483647, //max value for mssql int
                        tabIndex: 4,
                        name: 'rrcNumber',
                        fieldStyle: 'font-weight:bold;',
                        itemId: 'rrcNumberItemId',
                        listeners: {
                            blur: 'onRRCNumberCheckExisting',
                            change: 'onChangeData',
                        }
                    }, {
                        xtype: 'hidden',
                        name: 'origRrcNumber',
                        itemId: 'origRrcNumberItemId'
                    }, {
                        fieldLabel: 'Field',
                        xtype: 'combobox',
                        itemId: 'fieldItemId',
                        selectOnFocus: true,
                        tabIndex: 5,
                        name: 'fieldId',
                        displayField: 'fieldIdName',
                        valueField: 'fieldId',
                        store: 'Minerals.store.MineralValuation.FieldComboStore',
                        queryMode: 'local',
                        //forceSelection: true,
                        listeners: {
                            select: 'onSelectFieldName',
                            change: 'onChangeData',
                        }
                    }, {
                        fieldLabel: 'Well Depth',
                        xtype: 'numberfield',
                        minValue: '0',
                        selectOnFocus: true,
                        tabIndex: 6,
                        name: 'wellDepth',
                        readOnly: true,
                        itemId: 'wellDepthItemId',
                    }, {
                        fieldLabel: 'Gravity',
                        xtype: 'combobox',
                        selectOnFocus: true,
                        tabIndex: 7,
                        name: 'gravityCd',
                        displayField: 'gravityDesc',
                        valueField: 'gravityCd',
                        store: 'Minerals.store.ProperRecords.GravityCdStore',
                        queryMode: 'local',
                        forceSelection: true,
                        readOnly: true,
                        itemId: 'gravityCdItemId',
                    }, {
                        fieldLabel: 'Well Description',
                        xtype: 'textfield',
                        selectOnFocus: true,
                        tabIndex: 8,
                        name: 'wellDescription',
                    }, {
                        fieldLabel: 'WellType',
                        xtype: 'combobox',
                        selectOnFocus: true,
                        tabIndex: 9,
                        name: 'wellTypeCd',
                        displayField: 'wellTypeDesc',
                        valueField: 'wellTypeCd',
                        store: 'Minerals.store.MineralValuation.WellTypeComboStore',
                    }, {
                        fieldLabel: 'Year Life',
                        xtype: 'textfield',
                        readOnly: true,
                        fieldStyle: 'color: #000; font-weight:bold; ',
                        name: 'yearLife',
                    }, {
                        fieldLabel: 'Comment',
                        xtype: 'textarea',
                        tabIndex: 10,
                        name: 'comment',
                        maxLength: 50,
                        width: '100%',
                    },
                    {
                        fieldLabel: 'Legal Description',
                        xtype: 'textarea',
                        bind: {
                            hidden: '{isLease=="Unit"}'
                        },
                        disabled: true,
                        tabIndex: 10,
                        name: 'legalDescription',
                        maxLength: 50,
                        width: '100%',
                        style: {
                            opacity: 1
                        }
                    }
                    ]
            }, {
                xtype: 'container',
                layout: 'vbox',
                minWidth: 660,
//            maxWidth: 375,
                flex: 1,
                defaults: {width: '100%'},
                items: [{
                        xtype: 'fieldset',
//                        padding: '0 5 5 5',
                        margin: '8 0 0 0',
                        title: '',
                        defaults: {
                            maskRe: /[^'\^]/,
                            xtype: 'container',
                            layout: 'hbox',
                            height: 32,
                            style: {
                                marginBottom: '0px'
                            }
                        },
                        items: [{
                            defaults: {
                                xtype: 'displayfield',
                                fieldStyle: 'text-align: center;',
                                margin: '0 2 0 2',
                            },
                            items: [{
                                value: '',
                                width: '20%'
                            }, {
                                value: '#',
                                width: '15%'
                            }, {
                                value: 'Schedule',
                                width: '49%'
                            }, {
                                value: 'Equip Value',
                                width: '15%'
                            }]
                        },{
                            defaults: {
                                xtype: 'textfield',
                                enableKeyEvents: true,
                                margin: '0 2 0 2',
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },
                            items: [{
                                        xtype: 'displayfield',
                                        fieldStyle: 'text-align: right;',
                                        value: 'Production Wells:',
                                        width: '20%'
                                    }, {
                                        name: 'productionEquipmentCount',
                                        itemId: 'productionEquipmentCountItemId',
                                        maskRe: /[0-9.]/,
                                        tabIndex: 11,
                                        width: '15%',
                                        listeners: {
                                            change: function(key, newValue, oldValue, eOpts){
                                                var view = key.up('form');
                                                var count = view.down('#productionEquipmentCountItemId')
                                                var combo = view.down('#productionWellScheduleItemId');
                                                var comboStore = combo.getStore();
                                                var value = 0;
                                                var index = comboStore.find('valueField', combo.getValue());
                                                if(index != -1){
                                                    value = comboStore.getAt(index).get('salvageValue')
                                                }
                                                view.down('#productionEquipmentValueItemId').setValue(Ext.util.Format.number(count.getValue() * value, '000,000'))

                                                view.getController().onChangeData(key, newValue, oldValue, eOpts)
                                            }
                                        }
                                    }, {
                                        xtype: 'combobox',
                                        store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedProdStore',
                                        itemId: 'productionWellScheduleItemId',
                                        displayField: 'displayField',
                                        valueField: 'valueField',
                                        name: 'productionWellSchedule',
                                        queryMode: 'local',
                                        allowBlank: true,
                                        tabIndex: 12,
                                        width: '49%',
                                        listeners:{
                                            focus: function(){
                                                this.expand();
                                            },
                                            change: function(key, newValue, oldValue, eOpts){
                                                var view = key.up('form');
                                                var count = view.down('#productionEquipmentCountItemId')
                                                var combo = view.down('#productionWellScheduleItemId');
                                                var comboStore = combo.getStore();
                                                var value = 0;
                                                var index = comboStore.find('valueField', combo.getValue());
                                                if(index != -1){
                                                    value = comboStore.getAt(index).get('salvageValue')
                                                }
                                                 view.down('#productionEquipmentValueItemId').setValue(Ext.util.Format.number(count.getValue() * value, '000,000'))

                                                view.getController().onChangeData(key, newValue, oldValue, eOpts)
                                            }
                                        }
                                    }, {
                                        name: 'productionEquipmentValue',
                                        itemId: 'productionEquipmentValueItemId',
                                        maskRe: /[0-9.]/,
                                        readOnly: true,
                                        fieldStyle: 'color: #000;',
                                        width: '15%'
                                    }]
                        },{
                            defaults: {
                                xtype: 'textfield',
                                enableKeyEvents: true,
                                width: '24.5%',
                                margin: '0 2 0 2',
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },
                            items: [{
                                        xtype: 'displayfield',
                                        fieldStyle: 'text-align: right;',
                                        value: 'Service Wells:',
                                        width: '20%'
                                    }, {
                                        name: 'serviceEquipmentCount',
                                        itemId: 'serviceEquipmentCountItemId',
                                        maskRe: /[0-9.]/,
                                        tabIndex: 13,
                                        width: '15%',
                                        listeners: {
                                            change: function(key, newValue, oldValue, eOpts){
                                                var view = key.up('form');
                                                var count = view.down('#serviceEquipmentCountItemId')
                                                var combo = view.down('#serviceWellScheduleItemId');
                                                var comboStore = combo.getStore();
                                                var value = 0;
                                                var index = comboStore.find('valueField', combo.getValue());
                                                if(index != -1){
                                                    value = comboStore.getAt(index).get('salvageValue')
                                                }
                                                view.down('#serviceEquipmentValueItemId').setValue(Ext.util.Format.number(count.getValue() * value, '000,000'))

                                                view.getController().onChangeData(key, newValue, oldValue, eOpts)
                                            }
                                        }
                                    }, {
                                        xtype: 'combobox',
                                        store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedSvrcStore',
                                        itemId: 'serviceWellScheduleItemId',
                                        displayField: 'displayField',
                                        valueField: 'valueField',
                                        name: 'serviceWellSchedule',
                                        queryMode: 'local',
                                        allowBlank: true,
                                        tabIndex: 14,
                                        width: '49%',
                                        listeners:{
                                            focus: function(){
                                                this.expand();
                                            },
                                            change: function(key, newValue, oldValue, eOpts){
                                                var view = key.up('form');
                                                var count = view.down('#serviceEquipmentCountItemId')
                                                var combo = view.down('#serviceWellScheduleItemId');
                                                var comboStore = combo.getStore();
                                                var value = 0;
                                                var index = comboStore.find('valueField', combo.getValue());
                                                if(index != -1){
                                                    value = comboStore.getAt(index).get('salvageValue')
                                                }
                                                view.down('#serviceEquipmentValueItemId').setValue(Ext.util.Format.number(count.getValue() * value, '000,000'))

                                                view.getController().onChangeData(key, newValue, oldValue, eOpts)
                                            }
                                        }
                                    }, {
                                        name: 'serviceEquipmentValue',
                                        itemId: 'serviceEquipmentValueItemId',
                                        maskRe: /[0-9.]/,
                                        readOnly: true,
                                        fieldStyle: 'color: #000;',
                                        width: '15%'
                                    }]
                        },{
                            defaults: {
                                xtype: 'textfield',
                                enableKeyEvents: true,
                                width: '24.5%',
                                margin: '0 2 0 2',
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },
                            items: [{
                                        xtype: 'displayfield',
                                        fieldStyle: 'text-align: right;',
                                        value: 'Injection Wells:',
                                        width: '20%'
                                    }, {
                                        name: 'injectionEquipmentCount',
                                        itemId: 'injectionEquipmentCountItemId',
                                        maskRe: /[0-9.]/,
                                        tabIndex: 15,
                                        width: '15%',
                                        listeners: {
                                            change: function(key, newValue, oldValue, eOpts){
                                                var view = key.up('form');
                                                var count = view.down('#injectionEquipmentCountItemId')
                                                var combo = view.down('#injectionWellScheduleItemId');
                                                var comboStore = combo.getStore();
                                                var value = 0;
                                                var index = comboStore.find('valueField', combo.getValue());
                                                if(index != -1){
                                                    value = comboStore.getAt(index).get('salvageValue')
                                                }
                                                view.down('#injectionEquipmentValueItemId').setValue(Ext.util.Format.number(count.getValue() * value, '000,000'))

                                                view.getController().onChangeData(key, newValue, oldValue, eOpts)
                                            }
                                        }
                                    }, {
                                        xtype: 'combobox',
                                        store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedInjStore',
                                        itemId: 'injectionWellScheduleItemId',
                                        displayField: 'displayField',
                                        valueField: 'valueField',
                                        name: 'injectionWellSchedule',
                                        queryMode: 'local',
                                        allowBlank: true,
                                        tabIndex: 16,
                                        width: '49%',
                                        listeners:{
                                            focus: function(){
                                                this.expand();
                                            },
                                            change: function(key, newValue, oldValue, eOpts){
                                                var view = key.up('form');
                                                var count = view.down('#injectionEquipmentCountItemId')
                                                var combo = view.down('#injectionWellScheduleItemId');
                                                var comboStore = combo.getStore();
                                                var value = 0;
                                                var index = comboStore.find('valueField', combo.getValue());
                                                if(index != -1){
                                                    value = comboStore.getAt(index).get('salvageValue')
                                                }
                                                view.down('#injectionEquipmentValueItemId').setValue(Ext.util.Format.number(count.getValue() * value, '000,000'))

                                                view.getController().onChangeData(key, newValue, oldValue, eOpts)
                                            }
                                        }
                                    }, {
                                        name: 'injectionEquipmentValue',
                                        itemId: 'injectionEquipmentValueItemId',
                                        maskRe: /[0-9.]/,
                                        readOnly: true,
                                        fieldStyle: 'color: #000;',
                                        width: '15%'
                                    }]
                        },{
                            defaults: {
                                xtype: 'textfield',
                                enableKeyEvents: true,
                                margin: '0 2 0 2',
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },
                            items: [{
                                    xtype: 'displayfield',
                                    fieldStyle: 'text-align: right;',
                                    value: 'Max Years Life:',
                                    width: '20%',

                                }, {
                                    name: 'maxYearLife',
                                    maskRe: /[0-9.]/,
                                    tabIndex: 17,
                                    width: '15%',
                                    itemId: 'maxYearItemId',
                                }, {
                                    xtype: 'displayfield',
                                    fieldStyle: 'text-align: right;',
                                    value: 'Discount:',
                                    width: '49%'
                                }, {
                                    xtype: 'numberfield',
                                    name: 'discountRate',
                                    minValue: 0,
                                    maxValue: 100,
                                    // maskRe: /[0-9.]/,
                                    tabIndex: 18,
                                    width: '15%',
                                    listeners: {
                                        validitychange: function(field, isValid, eOpts){
                                            var label = field.previousSibling().el.dom;
                                            var cls = 'appraisalform-invalid-cont';
                                            if(!isValid && label){
                                                if(!label.classList.contains(cls)){
                                                    label.classList.add(cls);
                                                }
                                            }
                                            else if(label){
                                                if(label.classList.contains(cls)){
                                                    label.classList.remove(cls);
                                                }
                                            }
                                        },
                                        change: 'onChangeData'
                                    }
                            }]
                        },{
                            defaults: {
                                xtype: 'textfield',
                                enableKeyEvents: true,
                                margin: '0 2 0 2',
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },
                            items: [{
                                    xtype: 'displayfield',
                                    fieldStyle: 'text-align: right;',
                                    value: 'Oil W-H Price:',
                                    width: '20%'
                                }, {
                                    name: 'grossOilPrice',
                                    maskRe: /[0-9.]/,
                                    tabIndex: 19,
                                    width: '15%'
                                }, {
                                    xtype: 'displayfield',
                                    fieldStyle: 'text-align: right;',
                                    value: 'Product Price:',
                                    width: '49%'
                                }, {
                                    name: 'grossProductPrice',
                                    maskRe: /[0-9.]/,
                                    tabIndex: 20,
                                    width: '15%'
                            }]
                        },{
                            defaults: {
                                xtype: 'textfield',
                                enableKeyEvents: true,
                                width: '24.5%',
                                margin: '0 2 0 2',
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },
                            items: [{
                                    xtype: 'displayfield',
                                    fieldStyle: 'text-align: right;',
                                    value: 'Gas WI Price:',
                                    width: '20%'
                                }, {
                                    name: 'grossWorkingInterestGasPrice',
                                    maskRe: /[0-9.]/,
                                    tabIndex: 21,
                                    width: '15%'
                                }, {
                                    xtype: 'displayfield',
                                    fieldStyle: 'text-align: right;',
                                    value: 'Gas RI Price:',
                                    width: '49%'
                                }, {
                                    name: 'grossRoyaltyInterestGasPrice',
                                    maskRe: /[0-9.]/,
                                    tabIndex: 22,
                                    width: '15%'
                            }]
                        }]
                    },{
                        xtype: 'fieldset',
                        padding: '0 5 5 5',
                        margin: '8 0 0 0',
                        title: '',
                        layout: 'hbox',
                        flex: 1,
                        itemId: 'declineTypeItemId',
                        defaults: {
                            xtype: 'container',
                            layout: 'vbox',
                            widht: '24.4%',
                            margin: '0 1 0 1',
                            flex: 1,
                        },
                        items: [{
                            defaults:{
                                margin: '0 2 0 2',
                                flex: 1,
                            },
                            items:[{
                                xtype: 'displayfield',
                                width: '100%',
                                fieldStyle: 'text-align: center;',
                                value: 'Operating Exp',
                            },{
                                xtype: 'textfield',
                                name: 'operatingExpense',
                                itemId: 'operatingExpenseItemId',
                                enableKeyEvents: true,
                                width: '100%',
                                margin: '-10 2 0 2',
                                tabIndex: 23,
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },{
                                xtype: 'grid',
                                width: '100%',
                                store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalOperatingExpStore',
                                plugins: [Ext.create('Ext.grid.plugin.CellEditing', {
                                    clicksToEdit: 1
                                })],
                                listeners: {
                                    beforeedit: 'onValidateDeclineBeforeEdit',
                                    edit: 'onValidateDecline'
                                },
                                columns: {
                                    defaults: {
                                        editor: {
                                            xtype: 'numberfield',
                                            allowBlank: false,
                                            // maskRe:/[0-9.]/,
                                            minValue: 0,
                                            maxValue: 100,
                                        },
                                        sortable: false
                                    },
                                    items: [{
                                        header: 'Decline',
                                        flex: 1,
                                        dataIndex: 'decline',
                                    },{
                                        header: 'Year',
                                        flex: 1,
                                        dataIndex: 'declineYears',
                                    }]
                                },
                            }]
                        },{
                            defaults:{
                                margin: '0 2 0 2',
                                flex: 1,
                            },
                            items:[{
                                xtype: 'displayfield',
                                width: '100%',
                                fieldStyle: 'text-align: center;',
                                value: 'Oil Daily Avg',
                            },{
                                xtype: 'textfield',
                                name: 'dailyAverageOil',
                                itemId: 'dailyAverageOilItemId',
                                enableKeyEvents: true,
                                width: '100%',
                                margin: '-10 2 0 2',
                                tabIndex: 24,
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },{
                                xtype: 'grid',
                                width: '100%',
                                store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalOilDailyAvgStore',
                                plugins: [Ext.create('Ext.grid.plugin.CellEditing', {
                                    clicksToEdit: 1
                                })],
                                listeners: {
                                    beforeedit: 'onValidateDeclineBeforeEdit',
                                    edit: 'onValidateDecline'
                                },
                                columns: {
                                    defaults: {
                                        editor: {
                                            xtype: 'numberfield',
                                            allowBlank: false,
                                            // maskRe:/[0-9.]/,
                                            minValue: 0,
                                            maxValue: 100
                                        },
                                        sortable: false
                                    },
                                    items: [{
                                        header: 'Decline',
                                        flex: 1,
                                        dataIndex: 'decline',
                                    },{
                                        header: 'Year',
                                        flex: 1,
                                        dataIndex: 'declineYears',
                                    }]
                                },
                            }]
                        },{
                            defaults:{
                                margin: '0 2 0 2',
                                flex: 1,
                            },
                            items:[{
                                xtype: 'displayfield',
                                width: '100%',
                                fieldStyle: 'text-align: center;',
                                value: 'Gas Daily Avg',
                            },{
                                xtype: 'textfield',
                                name: 'dailyAverageGas',
                                itemId: 'dailyAverageGasItemId',
                                enableKeyEvents: true,
                                width: '100%',
                                margin: '-10 2 0 2',
                                tabIndex: 25,
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },{
                                xtype: 'grid',
                                width: '100%',
                                store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalGasDailyAvgStore',
                                plugins: [Ext.create('Ext.grid.plugin.CellEditing', {
                                    clicksToEdit: 1
                                })],
                                listeners: {
                                    beforeedit: 'onValidateDeclineBeforeEdit',
                                    edit: 'onValidateDecline'
                                },
                                columns: {
                                    defaults: {
                                        editor: {
                                            xtype: 'numberfield',
                                            allowBlank: false,
                                            // maskRe:/[0-9.]/,
                                            minValue: 0,
                                            maxValue: 100
                                        },
                                        sortable: false
                                    },
                                    items: [{
                                        header: 'Decline',
                                        flex: 1,
                                        dataIndex: 'decline',
                                    },{
                                        header: 'Year',
                                        flex: 1,
                                        dataIndex: 'declineYears',
                                    }]
                                },
                            }]
                        },{
                            defaults:{
                                margin: '0 2 0 2',
                                flex: 1,
                            },
                            items:[{
                                xtype: 'displayfield',
                                width: '100%',
                                fieldStyle: 'text-align: center;',
                                value: 'Prod Daily Avg',
                            },{
                                xtype: 'textfield',
                                enableKeyEvents: true,
                                name: 'dailyAverageProduct',
                                itemId: 'dailyAverageProductItemId',
                                width: '100%',
                                margin: '-10 2 0 2',
                                tabIndex: 26,
                                listeners: {
                                    change: 'onChangeData',
                                }
                            },{
                                xtype: 'grid',
                                width: '100%',
                                store: 'Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalProdDailyAvgStore',
                                plugins: [Ext.create('Ext.grid.plugin.CellEditing', {
                                    clicksToEdit: 1
                                })],
                                listeners: {
                                    beforeedit: 'onValidateDeclineBeforeEdit',
                                    edit: 'onValidateDecline'
                                },
                                columns: {
                                    defaults: {
                                        editor: {
                                            xtype: 'numberfield',
                                            allowBlank: false,
                                            // maskRe:/[0-9.]/,
                                            minValue: 0,
                                            maxValue: 100
                                        },
                                        sortable: false
                                    },
                                    items: [{
                                        header: 'Decline',
                                        flex: 1,
                                        dataIndex: 'decline',
                                    },{
                                        header: 'Year',
                                        flex: 1,
                                        dataIndex: 'declineYears',
                                    }]
                                },
                            }]
                        }]

                    },{
                        xtype: 'fieldset',
//                        padding: '0 5 5 5',
                        margin: '14 0 0 0',
                        title: '',
                        defaults: {
                            maskRe: /[^'\^]/,
                            xtype: 'container',
                            layout: 'hbox',
                            height: 32
                        },
                        items: [{
                                defaults: {
                                    xtype: 'displayfield',
                                    fieldStyle: 'text-align: center;',
                                    width: '12.85%'
                                },
                                items: [{
                                        value: '',
                                        width: '9%'
                                    }, {
                                        value: 'Equipment',
                                    }, {
                                        value: 'Oil',
                                    }, {
                                        value: 'Gas',
                                    }, {
                                        value: 'Product',
                                    }, {
                                        value: 'Total',
                                    }, {
                                        value: 'Prior Yr',
                                    }, {
                                        value: 'Wgt Pct',
                                    }]
                            }, {
                                defaults: {
                                    xtype: 'textfield',
                                    readOnly: true,
                                    fieldStyle: 'color: #000; text-align: right; padding-right: 3px;padding-left: 3px;',
                                    width: '12.85%',
                                    listeners: {
                                        change: 'onChangeData',
                                    }
                                },
                                items: [{
                                        xtype: 'displayfield',
                                        fieldStyle: 'text-align: right;',
                                        value: 'WI Value:',
                                        width: '9%'
                                    }, {
                                        name: 'equipmentValue',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'workingInterestOilValue',
                                        itemId: 'workingInterestOilValueItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'workingInterestGasValue',
                                        itemId: 'workingInterestGasValueItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'workingInterestProductValue',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'workingInterestTotalValue',
                                        itemId: 'workingInterestTotalValueItemId',
                                        fieldStyle: 'color: #000; font-weight: bold; text-align: right; padding-right: 3px;padding-left: 3px;',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'priorYrWorkingInterestTotalValue',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'workingInterestPercent',
                                        fieldStyle: 'text-align: left; padding-right: 3px;padding-left: 3px;',
                                        maskRe: /[0-9.]/,
                                    }]
                            }, {
                                defaults: {
                                    xtype: 'textfield',
                                    readOnly: true,
                                     fieldStyle: 'color: #000; text-align: right; padding-right: 3px;padding-left: 3px;',
                                    width: '12.85%',
                                    listeners: {
                                        change: 'onChangeData',
                                    }
                                },
                                items: [{
                                        xtype: 'displayfield',
                                        fieldStyle: 'text-align: right;',
                                        value: 'RI Value:',
                                        width: '9%'
                                    }, {
                                        name: 'equipmentRiValue',
                                        maskRe: /[0-9.]/,

                                    }, {
                                        name: 'royaltyInterestOilValue',
                                        itemId: 'royaltyInterestOilValueItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'royaltyInterestGasValue',
                                        itemId: 'royaltyInterestGasValueItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'royaltyInterestProductValue',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'royaltyInterestTotalValue',
                                        itemId: 'royaltyInterestTotalValueItemId',
                                        fieldStyle: 'color: #000; font-weight: bold; text-align: right; padding-right: 3px;padding-left: 3px;',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'priorYrRoyaltyInterestTotalValue',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'royaltyInterestPercent',
                                        fieldStyle: 'text-align: left; padding-right: 3px;padding-left: 3px;',
                                        maskRe: /[0-9.]/,
                                    }]
                            },{
                                defaults: {
                                    xtype: 'textfield',
                                    readOnly: true,
                                     fieldStyle: 'color: #000; text-align: right; padding-right: 3px;padding-left: 3px;',
                                    width: '12.85%',
                                    listeners: {
                                        change: 'onChangeData',
                                    }
                                },
                                items: [{
                                        xtype: 'displayfield',
                                        fieldStyle: 'text-align: right;',
                                        value: 'Total:',
                                        width: '9%'
                                    }, {
                                        name: 'equipmentTotal',
                                        maskRe: /[0-9.]/,

                                    }, {
                                        name: 'totalInterestOilValue',
                                        itemId: 'totalInterestOilValueItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'totalInterestGasValue',
                                        itemId: 'totalInterestGasValueItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'totalProductValue',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'totalInterestValue',
                                        itemId: 'totalInterestValueItemId',
                                        fieldStyle: 'color: #000; font-weight: bold; text-align: right; padding-right: 3px;padding-left: 3px;',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'priorYrTotalInterestValue',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'totalInterestPercent',
                                        fieldStyle: 'text-align: left; padding-right: 3px;padding-left: 3px;',
                                        maskRe: /[0-9.]/,
                                    }]
                            },{
                                margin: '10 0 5 0',
                                defaults: {
                                    xtype: 'textfield',
                                    readOnly: true,
                                    fieldStyle: 'color: #000; text-align: right; padding-right: 3px;padding-left: 3px;',
                                    width: '12.85%',
                                    listeners: {
                                        change: 'onChangeData',
                                    }
                                },
                                items: [{
                                        xtype: 'displayfield',
                                        fieldStyle: 'text-align: right;',
                                        value: 'Reserves:',
                                        width: '9%'
                                    }, {
                                        name: 'equipmentReserve',
                                        maskRe: /[0-9.]/,

                                    }, {
                                        name: 'accumOilProduction',
                                        itemId: 'accumOilProductionItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'accumGasProduction',
                                        itemId: 'accumGasProductionItemId',
                                        maskRe: /[0-9.]/,
                                    }, {
                                        name: 'accumProductProduction',
                                        maskRe: /[0-9.]/,
//                                    }, {
//                                        name: 'accumTotalProduction',
//                                        itemId: 'accumTotalProductionItemId',
//                                        maskRe: /[0-9.]/,
//                                    }, {
//                                        name: 'priorYearReserve',
//                                        maskRe: /[0-9.]/,
//                                    }, {
//                                        name: 'weightPctReserve',
//                                        maskRe: /[0-9.]/,
                                    }]
                            }]
                    }]
            }];

        var appraisalIndex = {
            xtype: 'label',
            style: 'color: white;',
            itemId: 'appraisalIndexItemId',
            text: '0 of 0',
            margin: '10 10 0 0'
        }

        var newAppraisalButton = {
            xtype: 'button',
            tooltip: Minerals.config.Runtime.appraisalNewButtonTooltip,
            text: Minerals.config.Runtime.appraisalNewButtonText,
            iconCls: Minerals.config.Runtime.appraisalNewButtonIcon,
            itemId: 'newAppraisalButton',
            listeners: {
                click: 'onNewAppraisalButton',
                afterrender: function () {
                    var me = this
                    me.setHidden(MineralPro.config.Runtime.access_level)
                }
            }
        }

        var makeValue0 = {
            xtype: 'button',
            tooltip: 'Make Value $0',
            text: 'MakeValue $0',
            //iconCls: '',
            itemId: 'makeValue0',
            //disabled: true,
            listeners: {
                click: 'onMakeValue0',
                afterrender: function () {
                    var me = this
                    me.setHidden(MineralPro.config.Runtime.access_level)
                }
            }
        }

        var nextButton = {
            xtype: 'button',
            tooltip: Minerals.config.Runtime.appraisalNextButtonTooltip,
            text: Minerals.config.Runtime.appraisalNextButtonText,
            iconCls: Minerals.config.Runtime.appraisalNextButtonIcon,
            alwaysShow: true,
            listeners: {
                click: 'onNextButton',
            }
        }

        var previousButton = {
            xtype: 'button',
            tooltip: Minerals.config.Runtime.appraisalPreviousButtonTooltip,
            text: Minerals.config.Runtime.appraisalPreviousButtonText,
            iconCls: Minerals.config.Runtime.appraisalPreviousButtonIcon,
            alwaysShow: true,
            listeners: {
                click: 'onPreviousButton',
            }
        }

        var calcButton = {
            xtype: 'button',
            itemId: 'calcButtonItemId',
            tooltip: Minerals.config.Runtime.appraisalCalcButtonTooltip,
            text: Minerals.config.Runtime.appraisalCalcButtonText,
            iconCls: Minerals.config.Runtime.appraisalCalcButtonIcon,
            alwaysShow: true,
            listeners: {
                click: 'onCalcButton',
            }
        }

        var yearByYearAnalysisButton = {
            xtype: 'button',
            itemId: 'yearByYearAnalysisButtonItemId',
            tooltip: Minerals.config.Runtime.yearByYearAnalysisButtonTooltip,
            text: Minerals.config.Runtime.yearByYearAnalysisButtonText,
//            iconCls: Minerals.config.Runtime.yearByYearAnalysisButtonIcon,
//            disabled: true,
            alwaysShow: true,
            listeners: {
                click: 'onYearByYearAnalysisButton',
            }
        }

        me.tools = [{
                xtype: 'container',
                width: '50%',
                layout: {
                    type: 'hbox',
                },
                items: [
                    appraisalIndex,
                    previousButton,
                    nextButton,
                    newAppraisalButton,
                    makeValue0
                ]
            }, {
                xtype: 'container',
                width: '50%',
                layout: {
                    type: 'hbox',
                    pack: 'end'
                },
                items: [
                    yearByYearAnalysisButton,
                    calcButton,
                    MineralPro.config.util.MainGridToolCmpt.saveToDbButton,
                ]
            }];

        me.callParent(arguments);
    },
    listeners : {
        afterRender: function(thisForm, options){
            var me = this;
            me.keyNav = Ext.create('Ext.util.KeyNav', me.el, {
                'tab': {
                   fn: function (e) {
                        setTimeout(function (){

                            var activeElement = Ext.get(Ext.Element.getActiveElement());
                            var tabCur = activeElement.dom.tabIndex;
                            if(tabCur > 0){

                            } else {
                                var mb = Ext.Msg.show({
                                    iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                                    title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,
                                    msg:"You're not allowed to exit the form",
                                    buttons: Ext.Msg.OK,
                                    fn: function(){
                                        me.down("#rrcNumberItemId").focus();
                                    }
                                });
                            }

                        },200)
                    }
                },
                scope: me
            });
        }
    }
});
