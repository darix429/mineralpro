/**
 * Contains the global configuration during runtime.
 */

Ext.define('Minerals.config.Runtime', {
    singleton: true, //default configuration for global variable

//**********************************************
//**shared configuration for all sub functions**
//**********************************************   
    //Action Column icons
    editCls: '../resources/icons/pencil.png',//'edit',
    editTooltip: 'Click to edit',
    
    //Action column cls
    deleteCls: 'delete',
    deleteTooltip: 'Delete record',
    undoDeleteCls: 'undo',
    undoDeleteTooltip: 'Undo Delete',
    
    //save button cls
    saveToDbBtnIconCls: 'save',
    addNewBtnIconCls: 'add',
    
    //save grid btn to database configuration
    saveGridBtnText: 'Save to Database',
    saveGridBtnTooltip: 'Save to Database',
    
    //refresh button filter configuration
    refreshBtnIconCls: 'refreshFilter',
    refreshBtnTooltip: 'Refresh Filter',
    refreshBtnText: 'Undo Filter',
    
    //List sync to grid messages and alerts no data
    listSyncMsgErrorIcon: 'wrn',
    listSyncMsgErrorTitle: 'Error',
    listSyncMsgErrorMsg: 'There is no new data to be saved.',
    
    //add data to grid error
    addEditToGridMsgErrorIcon: 'wrn',
    addEditToGridMsgErrorTitle: 'Error',
    addEditToGridMsgErrorMsg: 'Please check data on the highlighted fields.',
    
    //Note sync to grid messages and alerts with data
    syncGridMsgConfimationIcon: 'wrn',
    syncGridMsgConfimationTitle: 'Confirm',
    syncGridMsgConfimationMsg: 'Are you sure you want to commit changes?',
    	
    //list seletion unsaved data
    addExistingDataWarningCls: 'wrn',
    addExistingDataWarningTitle: 'Alert',
    addExistingDataWarningMsg: 'Data is already in the database.',
    
    //list seletion unsaved data
    editDeletedDataWarningCls: 'wrn',
    editDeletedDataWarningTitle: 'Alert',
    editDeletedDataWarningMsg: 'Please undelete data before editing.',


    //tab close confirmation
    tabCloseConfirmationIcon: 'wrn',
    tabCloseConfirmationTitle: 'Confirm',
    tabCloseConfirmationMsg: 'Do you want to save changes before closing?',
    
    actionColWidth: 50,  
   
    multipleEditAlertIcon: 'wrn',
    multipleEditAlertTitle: 'Alert',
    multipleEditAlertMsg: 'Please select/check record for Transfer Ownership.',
    
    duplicateAddressAlertIcon: 'wrn',
    duplicateAddressAlertTitle: 'Alert',
    duplicateAddressAlertMsg: 'Please check addess is duplicated.',
    
    

//*************************************************
//**individual configuration for each subfunction**
//*************************************************
    //Security Group configuration
    
    securityGroupEditSecurityCodeIcon: '../resources/icons/col_drop.png',
    securityGroupEditSecurityCodeTitle: 'Click to edit',
    
    securityGroupDeleteBtnToolTip: 'Delete Security Group',
    securityGroupDeleteBtnText: 'Delete Security Group',
    
    securityGroupDeleteAlertIcon: 'wrn',
    securityGroupDeleteAlertTitle: 'Alert',
    securityGroupDeleteAlertMsg: 'Please select security group to delete.',
    
    securityGroupDeleteConfirmationIcon: 'wrn',
    securityGroupDeleteConfirmationTitle: 'Confirm',
    securityGroupDeleteConfirmationMsg: 'Are you sure you want to delete security group ',
    
    securityGroupEditBtnIcon: 'edit',
    securityGroupEditBtnText: 'Edit Checked',
    securityGroupEditBtnTooltip: 'Edit Checked',
  

    //Security UserList configuration
    securityUserListNoContactAlertIcon: 'wrn',
    securityUserListNoContactAlertTitle: 'Alert',
    securityUserListNoContactAlertMsg: 'No contact created for this Organization name.',
     
    //refresh button filter configuration
    cancelGridIconCls: 'delete',
    cancelGridTooltip: 'Cancel Changes',
    cancelGridText: 'Cancel',
    
    //tooltip configuration
    tooltipImageWidth: 200,
    tooltipImageHeight: 200,
    
    //global
    inlineEditIcon: '../resources/icons/b_inline_edit.png',
    inlineEditComboIcon: '../resources/icons/col_drop.png',
    inlineEditComboTitle: 'Click to edit',

    //OwnerLeaseHoldings and leaseOwner
    ownerLeaseHoldingsTransferToMultipleBtnIcon: 'transfer',
    ownerLeaseHoldingsTransferToMultipleBtnText: 'Transfer Ownership',
    leaseOwnerTransferToMultipleBtnTooltip: 'Check the box in the first column to select owner',
    ownerLeaseHoldingsTransferToMultipleBtnTooltip: 'Check the box in the first column to select lease',
    
    //Unit/Lease Appraisal
    appraisalNextButtonIcon: 'next',
    appraisalNextButtonText: 'Next',
    appraisalNextButtonTooltip: 'Next',
    
    //Unit/Lease Appraisal
    appraisalPreviousButtonIcon: 'prev',
    appraisalPreviousButtonText: 'Previous',
    appraisalPreviousButtonTooltip: 'Previous',
    
    //Unit/Lease Appraisal
    appraisalNewButtonIcon: 'add_appraisal',
    appraisalNewButtonText: 'New Appraisal',
    appraisalNewButtonTooltip: 'New Appraisal',
    
    //Unit/Lease Appraisal
    appraisalCalcButtonIcon: 'calculator',
    appraisalCalcButtonText: 'Calculate',
    appraisalCalcButtonTooltip: 'Calculate',
    
    //Unit/Lease Appraisal
    yearByYearAnalysisButtonIcon: 'yearByYearAnalysis',
    yearByYearAnalysisButtonText: 'Show Year by Year Analysis',
    yearByYearAnalysisButtonTooltip: 'Show Year by Year Analysis',
    
    //Unit/Lease Field
    appraisalCls: '../resources/icons/view.png',//'edit',
    appraisalTooltip: 'Click to Appraise',

    //Unit/Lease Field
    noteCls: '../resources/icons/note.png',//'edit',
    noteTooltip: 'Click to change Notes',

    //Lease/Lease LeaseOwner Import
    importDOButtonIcon: 'import',//'Grid button',
    importDOButtonText: 'Import DO',
    importDOButtonTooltip: 'Download and Import data to grid',
    exportDOButtonIcon: 'export',
    //Appraisal Existing RRC Number
    appraisalExistRRCNumberAlertIcon: 'wrn',
    appraisalExistRRCNumberAlertTitle: 'Alert',
    appraisalExistRRCNumberAlertMsg: 'RRC Number is already in use. Please change RRC number.',
    
    //Appraisal Existing Calculation
    appraisalCalcAlertIcon: 'wrn',
    appraisalCalcAlertTitle: 'Alert',
    appraisalCalcAlertMsg: 'Calculation have no result. Please check verify that you have the correct values.',

    //Notes for Lease/Unit
    noNoteMsgErrorIcon: 'wrn',
    noNoteMsgErrorTitle: 'Alert',
    noNoteMsgErrorMsg: 'Please add appraisal before adding note.',
    
    //Lease Tax Unit
    addEditTaxUnitMsgErrorIcon: 'wrn',
    addEditTaxUnitMsgErrorTitle: 'Error',
    addEditTaxUnitMsgErrorMsg: 'Please change UNASSIGNED to valid selection.',

})