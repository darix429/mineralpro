Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.model.LeaseOwnersModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'idJournalEntry', type: "int", defaultValue: 0},
        {name: 'leaseId', type: "int", defaultValue: 0},
        {name: 'ownerId', type: "int", defaultValue: 0},
        {name: 'interestTypeCd', type: "int", defaultValue: 0},
        {name: 'appraisalYear', type: "int", defaultValue: 0},
        {name: 'beforeAfter', type: "string", defaultValue: ''},
        {name: 'ownerValue', type: "int", defaultValue: 0},
        {name: 'jeReasonCd', type: "int", defaultValue: 0},
        {name: 'jeHoldCd', type: "int", defaultValue: 0},
        {name: 'reportId', type: "int", defaultValue: 0},
        {name: 'taxOfficeNotifyCd', type: "int", defaultValue: 0},

        {name: 'ownerName', type: "string", defaultValue: ''},
        {name: 'agentName', type: "string", defaultValue: ''},
        {name: 'interestPercent', type: "string", defaultValue: ''},
        {name: 'interestType', type: "string", defaultValue: ''},
        {name: 'jeReason', type: "string", defaultValue: ''},
        {name: 'userName', type: "string", defaultValue: ''},
        {name: 'jeHold', type: "string", defaultValue: ''},
        {name: 'taxOfficeNotify ', type: "string", defaultValue: ''}
    ]
});