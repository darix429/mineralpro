Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.model.JEBatchesModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'idJournalEntryBatch', type: "int", defaultValue: 0},
        {name: 'batchId', type: "int", defaultValue: ''},
        {name: 'jeCount', type: "int", defaultValue: 0},
        {name: 'leaseOwnerCount', type: "int", defaultValue: 0},
        {name: 'nonValueChangeCount', type: "int", defaultValue: 0},
        {name: 'valueChangeCount', type: "string", defaultValue: 0},
        {name: 'beforeOwnerValue', type: "string", defaultValue: 0},
        {name: 'afterOwnerValue', type: "int", defaultValue: 0},
        {name: 'valueChangeCount', type: "int", defaultValue: 0},
        {name: 'reportId', type: "int", defaultValue: 0},
        {name: 'rowUpdateUserid', type: "int", defaultValue: ''},
        
        {name: 'rowUpdateDt', type: "string", defaultValue: ''},
        {name: 'createDt', type: "string", defaultValue: ''},
        {name: 'processDt', type: "string", defaultValue: ''},
        {name: 'description', type: "string", defaultValue: ''},
        {name: 'rowUpdateDt', type: "string", defaultValue: ''},
        {name: 'rowDeleteFlag', type: "string", defaultValue: ''}
    ]
});