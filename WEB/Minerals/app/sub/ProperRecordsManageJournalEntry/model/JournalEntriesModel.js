Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.model.JournalEntriesModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'idJournalEntry', type: "int", defaultValue: 0},
        {name: 'appUser', type: "string", defaultValue: 0},
        {name: 'selectedAppraisalYear', type: "int", defaultValue: 0},
        {name: 'batchId', type: "int", defaultValue: 0},
        {name: 'jeReasonCd', type: "int", defaultValue: 0},
        {name: 'taxOfficeNotifyCd', type: "int", defaultValue: 0},
        {name: 'jeHoldCd', type: "int", defaultValue: 0},
        {name: 'reportId', type: "int", defaultValue: 0},
        {name: 'rowUpdateUserid', type: "int", defaultValue: 0},

        {name: 'openDt', type: "string", defaultValue: ''},
        {name: 'closeDt', type: "string", defaultValue: ''},
        {name: 'description', type: "string", defaultValue: ''},
        {name: 'collectionExtractDt', type: "string", defaultValue: ''},
        {name: 'rowUpdateDt', type: "string", defaultValue: ''},
        {name: 'rowDeleteFlag', type: "string", defaultValue: ''}
    ]
});