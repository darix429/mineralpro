Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.store.JournalEntriesStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: false,
    showMask: true,

    model: 'Minerals.sub.ProperRecordsManageJournalEntry.model.JournalEntriesModel',

    proxy: {
        api: {
            read: '/api/Minerals/ProperRecords/ProperRecordsManageJournalEntry/ReadJournalEntries',
            create: '/api/Minerals/ProperRecords/ProperRecordsManageJournalEntry/Create',
            update: '/api/Minerals/ProperRecords/ProperRecordsManageJournalEntry/UpdateJournalEntries',
        }      
    }
    
}); 
