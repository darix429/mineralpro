Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.store.LeaseOwnersStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: false,
    showMask: true,
    
    model: 'Minerals.sub.ProperRecordsManageJournalEntry.model.LeaseOwnersModel',

    proxy: {
        api: {
            read: '/api/Minerals/ProperRecords/ProperRecordsManageJournalEntry/ReadLeaseOwners',
        }      
    }
    
}); 
