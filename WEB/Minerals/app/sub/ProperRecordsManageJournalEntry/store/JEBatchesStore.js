Ext.define('Minerals.sub.ProperRecordsManageJournalEntry.store.JEBatchesStore', {
    extend: 'MineralPro.store.BaseStore',
    //autoLoad: true,
    showMask: true,
    model: 'Minerals.sub.ProperRecordsManageJournalEntry.model.JEBatchesModel',

    proxy: {
        api: {
            read: '/api/Minerals/ProperRecords/ProperRecordsManageJournalEntry/ReadJEBatches',
             update: '/api/Minerals/ProperRecords/ProperRecordsManageJournalEntry/UpdateJEBatches',
        }      
    }
    
}); 
