Ext.define('Minerals.sub.MineralValuationManageUnit.model.UnitLeasesLeaseModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'leaseId', type: "int", defaultValue: 1},
        {name: 'leaseName', type: "string", defaultValue: ''},
        {name: 'unitId', type: "string", defaultValue: ''},
    ]
});
