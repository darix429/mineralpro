Ext.define('Minerals.sub.MineralValuationManageUnit.model.UnitLeasesModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'leaseId', type: "int", defaultValue: 1},
        {name: 'leaseName', type: "string", defaultValue: ''},
        {name: 'unitId', type: "int", defaultValue: 1},
        {name: 'name', type: "string", defaultValue: ''},
        {name: 'royaltyInterestPercent', type: "float", defaultValue: ''},
        {name: 'workingInterestPercent', type: "float", defaultValue: ''},
        {name: 'weightedRoyaltyPercent', type: "float", defaultValue: ''},
        {name: 'weightedWorkingPercent', type: "float", defaultValue: ''},
        {name: 'royaltyInterestValue', type: "float", defaultValue: ''},
        {name: 'workingInterestValue', type: "float", defaultValue: ''},
        {name: 'unitWorkingInterestPercent', type: "float", defaultValue: '0.0000'},
        {name: 'unitRoyaltyInterestPercent', type: "float", defaultValue: '0.0000'},
        {name: 'tractNum', type: "string", defaultValue: ''},
        {name: 'totalWorkingInterestValue', type: "float", defaultValue: ''},
        {name: 'totalRoyaltyInterestValue', type: "float", defaultValue: ''},
        {name: 'totalRoyaltyInterestPercent', type: "float", defaultValue: ''},
        {name: 'totalWorkingInterestPercent', type: "float", defaultValue: ''},
        {name: 'royaltyInterestPercentWeight', type: "float", defaultValue: ''},
        {name: 'workingInterestPercentWeight', type: "float", defaultValue: ''},
    ]
});
