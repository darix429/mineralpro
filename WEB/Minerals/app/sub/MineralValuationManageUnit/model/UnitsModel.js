Ext.define('Minerals.sub.MineralValuationManageUnit.model.UnitsModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'unitId', type: 'string', defaultValue: ''},
        {name: 'unitName', type: 'string', defaultValue: ''},
        {name: 'operatorId', type: 'string', defaultValue: ''},
        {name: 'operatorName', type: 'string', defaultValue: ''},
        {name: 'numberofleases', type: 'int', defaultValue: 0},

        {name: 'totalRoyaltyInterest', type: 'string', defaultValue: ''},
        {name: 'totalWorkingInterest', type: 'string', defaultValue: ''},
        {name: 'weightedRoyaltyInterest', type: 'string', defaultValue: ''},
        {name: 'weightedWorkingInterest', type: 'string', defaultValue: ''},
        {name: 'royaltyInterestValue', type: 'string', defaultValue: ''},
        {name: 'workingInterestValue', type: 'string', defaultValue: ''},
        {name: 'totalUnitValue', type: 'string', defaultValue: ''},       
    ]
});