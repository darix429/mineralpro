Ext.define('Minerals.sub.MineralValuationManageUnit.store.UnitNotesAppraisalStore', {
    extend: 'MineralPro.store.BaseStore',
    // autoLoad: true,
    showMask: true,
    model: 'Minerals.model.MineralValuation.NotesModel',

    proxy: {
        api: {
            create: '/api/Minerals/ProperRecords/ProperRecordsManageNote/CreateManageNote?subjectTypeCd=1&appraisalNote=true',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageNote/ReadManageNote?subjectTypeCd=1&appraisalNote=true',
            update: '/api/Minerals/ProperRecords/ProperRecordsManageNote/UpdateManageNote?subjectTypeCd=1&appraisalNote=true'
        }      
    }
}); 
