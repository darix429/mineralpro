Ext.define('Minerals.sub.MineralValuationManageUnit.store.UnitCalcAppraisalStore', {
    extend: 'MineralPro.store.BaseStore',
    // autoLoad: true,
    showMask: true,
    model: 'Minerals.model.MineralValuation.AppraisalModel',

    proxy: {
        api: {
//            create: '/api/Minerals/MineralValuation/MineralValuationAppraisal/CreateAppraisal?subjectTypeCd=1',
            read: '/api/Minerals/MineralValuation/MineralValuationAppraisal/CalculateAppraisal?subjectTypeCd=1',
//            update: '/api/Minerals/MineralValuation/MineralValuationAppraisal/UpdateAppraisal?subjectTypeCd=1'
        }      
    },
}); 
