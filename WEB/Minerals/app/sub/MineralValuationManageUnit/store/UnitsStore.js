Ext.define('Minerals.sub.MineralValuationManageUnit.store.UnitsStore', {
    extend: 'MineralPro.store.BaseStore',
    // autoLoad: true,
//    tabChangeReload: false, //make sure autoload=true if this is false;
    // showMask: true,
    model: 'Minerals.sub.MineralValuationManageUnit.model.UnitsModel',

    proxy: {
        api: {
            create: '/api/Minerals/MineralValuation/MineralValuationManageUnit/CreateUnits',
            read: '/api/Minerals/MineralValuation/MineralValuationManageUnit/ReadUnits',
            update: '/api/Minerals/MineralValuation/MineralValuationManageUnit/UpdateUnits'
        }      
    }
    
}); 
