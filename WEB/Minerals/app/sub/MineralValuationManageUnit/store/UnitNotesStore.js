Ext.define('Minerals.sub.MineralValuationManageUnit.store.UnitNotesStore', {
    extend: 'MineralPro.store.BaseStore',
    // autoLoad: true,
    showMask: true,
    model: 'Minerals.model.MineralValuation.NotesModel',

    proxy: {
        api: {
            create: '/api/Minerals/ProperRecords/ProperRecordsManageNote/CreateManageNote?subjectTypeCd=1',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageNote/ReadManageNote?subjectTypeCd=1',
            update: '/api/Minerals/ProperRecords/ProperRecordsManageNote/UpdateManageNote?subjectTypeCd=1'
        }      
    }
}); 
