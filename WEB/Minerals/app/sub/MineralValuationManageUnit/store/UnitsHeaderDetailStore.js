Ext.define('Minerals.sub.MineralValuationManageUnit.store.UnitsHeaderDetailStore', {
    extend: 'MineralPro.store.BaseStore',
//    autoLoad: true,
//    tabChangeReload: false, //make sure autoload=true if this is false;
    showMask: true,
    model: 'Minerals.sub.MineralValuationManageUnit.model.UnitsModel',

    proxy: {
        api: {
//            create: '/api/Minerals/MineralValuation/MineralValuationManageUnit/CreateUnits',
            read: '/api/Minerals/MineralValuation/MineralValuationManageUnit/ReadUnitHeaderDetail',
//            update: '/api/Minerals/MineralValuation/MineralValuationManageUnit/UpdateUnits'
        }      
    }
    
}); 
