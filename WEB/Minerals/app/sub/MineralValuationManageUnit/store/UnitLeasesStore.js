Ext.define('Minerals.sub.MineralValuationManageUnit.store.UnitLeasesStore', {
    extend: 'MineralPro.store.BaseStore',
    //autoLoad: true,
    groupField: 'Unit Name',
    showMask: true,
    model: 'Minerals.sub.MineralValuationManageUnit.model.UnitLeasesModel',

    proxy: {
        api: {
            create: '/api/Minerals/MineralValuation/MineralValuationManageUnit/CreateUnitLeases',
            read: '/api/Minerals/MineralValuation/MineralValuationManageUnit/ReadUnitLeases',
            update: '/api/Minerals/MineralValuation/MineralValuationManageUnit/UpdateUnitLeases'
        }      
    }
    
}); 
