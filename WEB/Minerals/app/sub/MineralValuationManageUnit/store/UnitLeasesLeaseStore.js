Ext.define('Minerals.sub.MineralValuationManageUnit.store.UnitLeasesLeaseStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
//    showMask: true,
    model: 'Minerals.sub.MineralValuationManageUnit.model.UnitLeasesLeaseModel',
    
    proxy: {
        api: {
            read: '/api/Minerals/MineralValuation/MineralValuationManageUnit/ReadUnitLeasesLease',
        }      
    }
    
}); 
