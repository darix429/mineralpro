Ext.define('Minerals.sub.MineralValuationManageUnit.store.UnitFieldsStore', {
    extend: 'MineralPro.store.BaseStore',
//  autoLoad: true,
    showMask: true,
    model: 'Minerals.model.MineralValuation.FieldsModel',

    proxy: {
        api: {
//            create: '/api/Minerals/ProperRecords/ProperRecordsManageNote/CreateManageNote?subjectTypeCd=1',
            read: '/api/Minerals/MineralValuation/MineralValuationFields/ReadFields?subjectTypeCd=1',
//            update: '/api/Minerals/ProperRecords/ProperRecordsManageNote/UpdateManageNote?subjectTypeCd=1'
        }      
    }
}); 
