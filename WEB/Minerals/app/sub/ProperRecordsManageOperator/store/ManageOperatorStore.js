Ext.define('Minerals.sub.ProperRecordsManageOperator.store.ManageOperatorStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
//    tabChangeReload: false, //make sure autoload=true if this is false;
    showMask: true,
    model: 'Minerals.sub.ProperRecordsManageOperator.model.ManageOperatorModel',

    proxy: {
        api: {
            create: '/api/Minerals/ProperRecords/ProperRecordsManageOperator/CreateManageOperator',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageOperator/ReadManageOperator',
            update: '/api/Minerals/ProperRecords/ProperRecordsManageOperator/UpdateManageOperator'
        }      
    },
    multiColumnSort : true,
    autoSort : true,
    sorters : [{
        direction : 'DESC',
        property : 'recordDivision'

    }, {
        direction : 'ASC',
        property : 'operatorId'
    }]
    
}); 
