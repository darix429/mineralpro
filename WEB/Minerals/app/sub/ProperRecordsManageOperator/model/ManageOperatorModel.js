Ext.define('Minerals.sub.ProperRecordsManageOperator.model.ManageOperatorModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'idOperator', type: "int", defaultValue: 1},
        {name: 'appraisalYear', type: "string", defaultValue: ''},
        {name: 'operatorId', type: "string", defaultValue: ''},
        {name: 'operatorName', type: "string", defaultValue: ''},
        {name: 'ownerId', type: "int", defaultValue: ''},
        {name: 'ownerName1', type: "string", defaultValue: ''},
        // {name: 'agencyCdx', type: "int", defaultValue: 1},
        // {name: 'agencyName', type: "string", defaultValue: ''},
        {name: 'recordDivision', type: "string", defaultValue: 'false'},
    ]
});