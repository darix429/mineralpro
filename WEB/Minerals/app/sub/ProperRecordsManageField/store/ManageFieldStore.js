Ext.define('Minerals.sub.ProperRecordsManageField.store.ManageFieldStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
//    tabChangeReload: false, //make sure autoload=true if this is false;
    showMask: true,
    model: 'Minerals.sub.ProperRecordsManageField.model.ManageFieldModel',

    proxy: {
        api: {
            create: '/api/Minerals/ProperRecords/ProperRecordsManageField/CreateManageField',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageField/ReadManageField',
            update: '/api/Minerals/ProperRecords/ProperRecordsManageField/UpdateManageField'
        }      
    }
    
}); 
