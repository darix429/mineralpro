Ext.define('Minerals.sub.ProperRecordsManageField.model.ManageFieldModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'fieldId', type: "string", defaultValue: ''},
        {name: 'fieldName', type: "string", defaultValue: ''},
        {name: 'fieldDepth', type: "int", defaultValue: 1},
        {name: 'gravityDesc', type: "string", defaultValue: 'UNASSIGNED'},
        {name: 'gravityCd', type: "int", defaultValue: 0},
        {name: 'lastAppraisalYear', type: "int", defaultValue: 0},
                
    ]
});