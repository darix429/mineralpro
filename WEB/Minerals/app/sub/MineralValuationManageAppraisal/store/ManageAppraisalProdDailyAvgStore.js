Ext.define('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalProdDailyAvgStore', {
    extend: 'MineralPro.store.BaseStore',
//    autoLoad: true,
//    tabChangeReload: false, //make sure autoload=true if this is false;
//    showMask: true,
    model: 'Minerals.model.MineralValuation.AppraisalModel',

    proxy: {
        api: {
            create: '/api/Minerals/MineralValuation/MineralValuationManageAppraisal/CreateManageAppraisalDecline?declineTypeCd=4',
            read: '/api/Minerals/MineralValuation/MineralValuationManageAppraisal/ReadManageAppraisalDecline?declineTypeCd=4',
            update: '/api/Minerals/MineralValuation/MineralValuationManageAppraisal/UpdateManageAppraisalDecline?declineTypeCd=4',
            destroy: '/api/Minerals/MineralValuation/MineralValuationManageAppraisal/DeleteManageAppraisalDecline?declineTypeCd=4'
        }      
    },
    listeners: {
         load: function(store){    
            var recDecline = Ext.create('Minerals.model.MineralValuation.AppraisalModel')
            recDecline.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
            store.insert(store.getCount()+1, recDecline)
        } 
    }
    
}); 
