Ext.define('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalDetailStore', {
    extend: 'MineralPro.store.BaseStore',
//    autoLoad: true,
//    tabChangeReload: false, //make sure autoload=true if this is false;
    showMask: true,
    model: 'Minerals.model.MineralValuation.AppraisalModel',

    proxy: {
        api: {
            create: '/api/Minerals/MineralValuation/MineralValuationAppraisal/CreateAppraisal',
            read: '/api/Minerals/MineralValuation/MineralValuationManageAppraisal/ReadManageAppraisal',
            update: '/api/Minerals/MineralValuation/MineralValuationAppraisal/UpdateAppraisal'
        }      
    }
    
}); 
