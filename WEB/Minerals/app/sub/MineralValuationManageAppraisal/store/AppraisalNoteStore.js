Ext.define('Minerals.sub.MineralValuationManageAppraisal.store.AppraisalNoteStore', {
    extend: 'MineralPro.store.BaseStore',
    // autoLoad: true,
    showMask: true,
    model: 'Minerals.model.MineralValuation.NotesModel',

    proxy: {
        api: {
            create: '/api/Minerals/ProperRecords/ProperRecordsManageNote/CreateManageNote?appraisalNote=true',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageNote/ReadManageNote?appraisalNote=true',
            update: '/api/Minerals/ProperRecords/ProperRecordsManageNote/UpdateManageNote?appraisalNote=true'
        }      
    }
}); 
