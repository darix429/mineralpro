Ext.define('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSchedInjStore', {
    extend: 'MineralPro.store.BaseStore',
//    autoLoad: true,
//    tabChangeReload: false, //make sure autoload=true if this is false;
//    showMask: true,
    model: 'Minerals.model.MineralValuation.AppraisalModel',

    proxy: {
        api: {
//            create: '/api/Minerals/MineralValuation/MineralValuationManageAppraisal/CreateManageAppraisal',
            read: '/api/Minerals/MineralValuation/MineralValuationManageAppraisal/ReadManageAppraisalSchedule?valueTypeCd=3',
//            update: '/api/Minerals/MineralValuation/MineralValuationManageAppraisal/UpdateManageAppraisal'
        }      
    }
    
}); 
