Ext.define('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalOperatingExpStore', {
    extend: 'MineralPro.store.BaseStore',
//    autoLoad: true,
//    tabChangeReload: false, //make sure autoload=true if this is false;
//    showMask: true,
    model: 'Minerals.model.MineralValuation.AppraisalModel',

    proxy: {
        api: {
            create: '/api/Minerals/MineralValuation/MineralValuationManageAppraisal/CreateManageAppraisalDecline?declineTypeCd=1',
            read: '/api/Minerals/MineralValuation/MineralValuationManageAppraisal/ReadManageAppraisalDecline?declineTypeCd=1',
            update: '/api/Minerals/MineralValuation/MineralValuationManageAppraisal/UpdateManageAppraisalDecline?declineTypeCd=1',
            destroy: '/api/Minerals/MineralValuation/MineralValuationManageAppraisal/DeleteManageAppraisalDecline?declineTypeCd=1'
        }      
    },
    listeners: {
         load: function(store){    
            var recDecline = Ext.create('Minerals.model.MineralValuation.AppraisalModel')
            recDecline.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
            store.insert(store.getCount()+1, recDecline)
        } 
    }
    
}); 
