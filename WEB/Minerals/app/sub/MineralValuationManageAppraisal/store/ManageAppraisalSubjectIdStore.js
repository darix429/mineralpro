Ext.define('Minerals.sub.MineralValuationManageAppraisal.store.ManageAppraisalSubjectIdStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    tabChangeReload: false, //make sure autoload=true if this is false;
//    showMask: true,
    model: 'Minerals.model.MineralValuation.AppraisalModel',

    proxy: {
        api: {
//            create: '/api/Minerals/MineralValuation/MineralValuationManageAppraisal/CreateManageAppraisal',
            read: '/api/Minerals/MineralValuation/MineralValuationManageAppraisal/ReadManageAppraisalSubjectId',
//            update: '/api/Minerals/MineralValuation/MineralValuationManageAppraisal/UpdateManageAppraisal'
        }      
    }
    
}); 
