Ext.define('Minerals.sub.MineralValuationManageLease.model.OperatorComboModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'operatorId', type: "int", defaultValue: ''},
        {name: 'operatorName', type: "string", defaultValue: ''},
    ]
});