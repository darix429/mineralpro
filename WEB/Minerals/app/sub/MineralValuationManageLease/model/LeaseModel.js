Ext.define('Minerals.sub.MineralValuationManageLease.model.LeaseModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'leaseId', type: "int", defaultValue: ''},
        {name: 'leaseName', type: "string", defaultValue: ''},
        {name: 'divisionOrderDescription', type: "string", defaultValue: ''},
        {name: 'divisionOrderDt', type: 'string', defaultValue: Ext.Date.format(new Date(), 'Y-m-d')},
        {name: 'description', type: "string", defaultValue: ''},
        {name: 'comment', type: "string", defaultValue: ''},
        {name: 'changeReasonCd', type: "int", defaultValue: 0},
        {name: 'changeReason', type: "string", defaultValue: 'UNASSIGNED'},
        {name: 'cadCategoryCd', type: "int", defaultValue: 0},
        {name: 'cadCategory', type: "string", defaultValue: 'UNASSIGNED'},
        {name: 'operatorId', type: "int", defaultValue: 0},
        {name: 'operatorName', type: "string", defaultValue: 'UNASSIGNED'},
        {name: 'acres', type: "int", defaultValue: 0},
        {name: 'unitId', type: "int", defaultValue: 0},
        {name: 'unitName', type: "string", defaultValue: ''},
        {name: 'leaseYear', type: "int", defaultValue: 0},
        {name: 'totalInterestPercent', type: "float", defaultValue: 0},
        {name: 'TotalLeaseOwners', type: "string", defaultValue: 0},
        {name: 'totalRoyaltyValue', type: "string", defaultValue: '0'},
        {name: 'totalWoekingValue', type: "string", defaultValue: '0'},


    ]
});