Ext.define('Minerals.sub.MineralValuationManageLease.model.LeaseNoteModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'mineralId', type: "int", defaultValue: 1},

        {name: 'leaseId', type: "int", defaultValue: 1},

        {name: 'leaseName', type: "int", defaultValue: 1},

        {name: 'idLeaseNote', type: "int", defaultValue: 1},

        {name: 'subjectTypeCd', type: "string", defaultValue: ''},
        
        {name: 'seqNumber', type: "string", defaultValue: ''},

        {name: 'createDt', type: "string", defaultValue: ''},

        {name: 'securityCd', type: "int", defaultValue: 2},

        {name: 'quickNoteCd', type: "string", defaultValue: ''},

        {name: 'quickNoteDesc', type: "string", defaultValue: ''},

        {name: 'groupCd', type: "string", defaultValue: ''},

        {name: 'referenceAppraisalYear', type: "string", defaultValue: ''},

        {name: 'noteTypeCd', type: "string", defaultValue: ''},

        {name: 'note', type: "string", defaultValue: ''},

    ]
});