Ext.define('Minerals.sub.MineralValuationManageLease.model.LeaseOwnerModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'leaseId', type: "int", defaultValue: 0},
        {name: 'leaseName', type: "string", defaultValue: ''},
        {name: 'OwnerValues', type: 'float', defaultValue: ''},
        {name: 'ownerId', type: 'int', defaultValue: 0},
        {name: 'ownerName', type: "string", defaultValue: ''},
        {name: 'ownerAddress', type: "string", defaultValue: ''},
        {name: 'interestTypeCd', type: "int", defaultValue: 0},
        {name: 'interestType', type: "string", defaultValue: 'UNASSIGNED'},
        {name: 'interestPercent', type: "string", defaultValue: '0'},
        {name: 'totalInterestPercent', type: "float", defaultValue: 0},
        {name: 'rotaltyInterestPercent', type: "float", defaultValue: 0},
        {name: 'overridingInterestPercent', type: "float", defaultValue: 0},
        {name: 'oilPayment', type: "float", defaultValue: 0},
        {name: 'rowDeleteFlag', type: "string", defaultValue: ''},
        //Excel Format for Download
        {name: 'Owner_Name', type: "string", defaultValue: 0},
        {name: 'Owner_Name2', type: "string", defaultValue: 0},
        {name: 'Owner_Mailing1', type: "string", defaultValue: 0},
        {name: 'Owner_Mailing2', type: "string", defaultValue: 0},
        {name: 'Owner_City', type: "string", defaultValue: 0},
        {name: 'Owner_ST', type: "string", defaultValue: 0},
        {name: 'Owner_Zip', type: "string", defaultValue: 0},
        //trensfer
        {name: 'PctTransfer', type: "string", defaultValue: '100'},
        {name: 'percentReceive', type: "string", defaultValue: ''}, 
        {name: 'appraisedValue', type: "float", defaultValue: ''},
        {name: 'edited', type: "string", defaultValue: ''},
        {name: 'oldValue', type: "string", defaultValue: ''},
        {name: 'calcValue', type: "float", defaultValue: ''},
        {name: 'ownerTransferPct', type: "string", defaultValue: ''},
        {name: 'ownerReceivePct', type: "string", defaultValue: ''},
        {name: 'OwnerTransferValue', type: "string", defaultValue: ''},
        {name: 'OwnerReceiveValue', type: "string", defaultValue: ''}
    ]
});