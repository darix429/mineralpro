Ext.define('Minerals.sub.MineralValuationManageLease.model.LeaseTaxUnitModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'idLeaseTaxUnit', type: "int", defaultValue: ''},
        {name: 'leaseId', type: "int", defaultValue: ''},
        {name: 'leaseName', type: "string", defaultValue: ''},
        {name: 'taxUnitTypeCd', type: "int", defaultValue: 0},
        {name: 'taxUnitType', type: "string", defaultValue: 'UNASSIGNED'},
        {name: 'taxUnitCd', type: "int", defaultValue: 0},
        {name: 'taxUnit', type: "int", defaultValue: 'UNASSIGNED'},
        {name: 'taxUnitName', type: "string", defaultValue: 'UNASSIGNED'},
        {name: 'taxUnitPercent', type: "string", defaultValue: '1.000'},
        {name: 'estimatedTaxRate', type: "string", defaultValue: ''}
    ]
});