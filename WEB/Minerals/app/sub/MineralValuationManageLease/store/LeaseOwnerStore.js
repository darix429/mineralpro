Ext.define('Minerals.sub.MineralValuationManageLease.store.LeaseOwnerStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: false,
    showMask: true,
    model: 'Minerals.sub.MineralValuationManageLease.model.LeaseOwnerModel',
    proxy: {
        api: {
            create: '/api/Minerals/ProperRecords/ProperRecordsLeaseOwner/CreateLeaseOwner?subjectType=lease',
            read: '/api/Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseOwner',
            update:  '/api/Minerals/ProperRecords/ProperRecordsLeaseOwner/UpdateLeaseOwner?subjectType=lease'
        }      
    },
    //groupField: 'interestType'
    
}); 
