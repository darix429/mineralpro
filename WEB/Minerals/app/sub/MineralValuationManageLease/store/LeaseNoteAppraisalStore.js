Ext.define('Minerals.sub.MineralValuationManageLease.store.LeaseNoteAppraisalStore', {
    extend: 'MineralPro.store.BaseStore',
    // autoLoad: true,
    showMask: true,
    model: 'Minerals.model.MineralValuation.NotesModel',

    proxy: {
        api: {
            create: '/api/Minerals/ProperRecords/ProperRecordsManageNote/CreateManageNote?subjectTypeCd=2&appraisalNote=true',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageNote/ReadManageNote?subjectTypeCd=2&appraisalNote=true',
            update: '/api/Minerals/ProperRecords/ProperRecordsManageNote/UpdateManageNote?subjectTypeCd=2&appraisalNote=true'
        }      
    }
}); 
