Ext.define('Minerals.sub.MineralValuationManageLease.store.LeaseOwnersTransferCalculationStore', {
    extend: 'MineralPro.store.BaseStore',
    //autoLoad: true,
    model: 'Minerals.sub.MineralValuationManageLease.model.LeaseOwnerModel',
    showMask: true,
    proxy: {
        api: {
            //create: '/api/Minerals/createTransfer',
            //read: '/api/TransferLeaseOwner',
            update: '/api/Minerals/ProperRecords/ProperRecordsLeaseOwner/TransferLeaseOwner',
           // destroy: '/api/TransferLeaseOwnerDELETE'
        }      
    }
    
}); 
