Ext.define('Minerals.sub.MineralValuationManageLease.store.LeaseNoteStore', {
    extend: 'MineralPro.store.BaseStore',
    // autoLoad: true,
    showMask: true,
    model: 'Minerals.model.MineralValuation.NotesModel',

    proxy: {
        api: {
            create: '/api/Minerals/ProperRecords/ProperRecordsManageNote/CreateManageNote?subjectTypeCd=2',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageNote/ReadManageNote?subjectTypeCd=2',
            update: '/api/Minerals/ProperRecords/ProperRecordsManageNote/UpdateManageNote?subjectTypeCd=2'
        }      
    }
}); 
