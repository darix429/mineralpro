Ext.define('Minerals.sub.MineralValuationManageLease.store.LeaseHeaderDetailStore', {
    extend: 'MineralPro.store.BaseStore',
//    autoLoad: true,
//    tabChangeReload: false, //make sure autoload=true if this is false;
    showMask: true,
    model: 'Minerals.sub.MineralValuationManageLease.model.LeaseModel',

    proxy: {
        api: {
//            create: '/api/MineralPro/MineralValuation/MineralValuationManageLease/CreateLease',
            read: '/api/MineralPro/MineralValuation/MineralValuationManageLease/ReadLeaseHeaderDetail',
//            update: '/api/MineralPro/MineralValuation/MineralValuationManageLease/UpdateLease'
        }      
    }
    
}); 
