Ext.define('Minerals.sub.MineralValuationManageLease.store.LeaseOwnersTransferLeaseStore', {
    extend: 'MineralPro.store.BaseStore',
   // autoLoad: true,
    model: 'Minerals.sub.MineralValuationManageLease.model.LeaseOwnerModel',

    proxy: {
        api: {
          //  create: '/api/Minerals/ProperRecords/ProperRecordsLeaseOwner/CreateLeaseOwner?subjectType=owner',
            read: '/api/Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseOwnersTransferLease',
         //   update: '/api/Minerals/ProperRecords/ProperRecordsLeaseOwner/UpdateLeaseOwner?subjectType=owner'
        }      
    }
    
}); 
