Ext.define('Minerals.sub.MineralValuationManageLease.store.LeaseTaxUnitStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: false,
    showMask: true,
    model: 'Minerals.sub.MineralValuationManageLease.model.LeaseTaxUnitModel',

    proxy: {
        api: {
            create: '/api/Minerals/MineralValuation/MineralValuationManageLease/CreateLeaseTaxUnit',
            read: '/api/Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseTaxUnit',
            update: '/api/Minerals/MineralValuation/MineralValuationManageLease/UpdateLeaseTaxUnit'
        }      
    }
    
}); 
