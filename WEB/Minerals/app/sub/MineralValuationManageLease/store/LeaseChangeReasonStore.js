Ext.define('Minerals.sub.MineralValuationManageLease.store.LeaseChangeReasonStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'Minerals.sub.MineralValuationManageLease.model.LeaseModel',

    proxy: {
        api: {
            create: '',
            read: '/api/Minerals/MineralValuation/MineralValuationManageLease/ReadChangeReason',
            update: ''
        }      
    }
    
}); 
