Ext.define('Minerals.sub.MineralValuationManageLease.store.LeaseCalcAppraisalStore', {
    extend: 'MineralPro.store.BaseStore',
    // autoLoad: true,
    showMask: true,
    model: 'Minerals.model.MineralValuation.AppraisalModel',

    proxy: {
        api: {
//            create: '/api/Minerals/MineralValuation/MineralValuationAppraisal/CreateAppraisal?subjectTypeCd=2',
            read: '/api/Minerals/MineralValuation/MineralValuationAppraisal/CalculateAppraisal?subjectTypeCd=2',
//            update: '/api/Minerals/MineralValuation/MineralValuationAppraisal/UpdateAppraisal?subjectTypeCd=2'
        }      
    },
}); 
