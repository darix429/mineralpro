Ext.define('Minerals.sub.MineralValuationManageLease.store.LeaseTaxUnitTypeStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'Minerals.sub.MineralValuationManageLease.model.LeaseTaxUnitModel',

    proxy: {
        api: {
            create: '',
            read: '/api/Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseTaxUnitType',
            update: ''
        }      
    }
    
}); 
