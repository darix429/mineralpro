Ext.define('Minerals.sub.MineralValuationManageLease.store.LeaseTaxUnitCodeStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: false,
    
    model: 'Minerals.sub.MineralValuationManageLease.model.LeaseTaxUnitModel',

    proxy: {
        api: {
            create: '',
            read: '/api/Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseTaxUnitCode',
            update: ''
        }      
    }
    
}); 
