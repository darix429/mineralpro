Ext.define('Minerals.sub.MineralValuationManageLease.store.LeaseStatusStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'Minerals.sub.MineralValuationManageLease.model.LeaseModel',

    proxy: {
        api: {
            create: '',
            read: '/api/Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseStatus',
            update: ''
        }      
    }
    
}); 
