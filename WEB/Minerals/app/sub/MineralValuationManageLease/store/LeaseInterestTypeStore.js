Ext.define('Minerals.sub.MineralValuationManageLease.store.LeaseInterestTypeStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'Minerals.sub.MineralValuationManageLease.model.LeaseOwnerModel',

    proxy: {
        api: {
            create: '',
            read: '/api/Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseOwnerInterestType',
            update: ''
        }      
    }
    
}); 
