Ext.define('Minerals.sub.ProperRecordsManageAgent.store.ManageAgentStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
//    tabChangeReload: false, //make sure autoload=true if this is false;
    showMask: true,
    model: 'Minerals.sub.ProperRecordsManageAgent.model.ManageAgentModel',

    proxy: {
        api: {
            create: '/api/Minerals/ProperRecords/ProperRecordsManageAgent/CreateManageAgent',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageAgent/ReadManageAgent',
            update: '/api/Minerals/ProperRecords/ProperRecordsManageAgent/UpdateManageAgent'
        }      
    }
    
}); 
