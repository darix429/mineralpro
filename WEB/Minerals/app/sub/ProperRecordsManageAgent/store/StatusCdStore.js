Ext.define('Minerals.sub.ProperRecordsManageAgent.store.StatusCdStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    tabChangeReload: false, //make sure autoload=true if this is false;
    showMask: true,
    model: 'Minerals.sub.ProperRecordsManageAgent.model.ManageAgentModel',

    proxy: {
        api: {
            read: '/api/InformationServices/OrganizationOrganizationContact/ReadStatusCodeType',
        }
       
    }
    
}); 
