Ext.define('Minerals.sub.ProperRecordsManageAgent.model.ManageAgentModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'idAgency', type: "int", defaultValue: 1},
        {name: 'appraisalYear', type: "int"},
        {name: 'phoneNum', type: "string", defaultValue: ''},
        {name: 'stateCd', type: "int", defaultValue: 45},
        {name: 'countryCd', type: "int", defaultValue: 1},
        {name: 'faxNum', type: "string" ,defaultValue: ''},
        {name: 'statusCd', type: "int", defaultValue: 0},
        {name: 'description', type: "string", defaultValue: ''},
        {name: 'agencyCdx', type: "string", defaultValue: ''},
    
        {name: 'agencyName', type: "string", defaultValue: ''},
        {name: 'addrLine1', type: "string", defaultValue: ''},
        {name: 'addrLine2', type: "string", defaultValue: ''},
        {name: 'addrLine3', type: "string", defaultValue: ''},
        {name: 'city', type: "string", defaultValue: ''},
        {name: 'emailAddress', type: "string", defaultValue: ''},
        {name: 'stateName', type: "string", defaultValue: 'TX'},
        {name: 'countryName', type: "string", defaultValue: 'UNITED STATES OF AMERICA'},
        {name: 'zipcode', type: "string", defaultValue: ''},
    ]
});