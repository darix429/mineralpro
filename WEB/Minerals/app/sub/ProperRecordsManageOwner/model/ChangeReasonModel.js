Ext.define('Minerals.sub.ProperRecordsManageOwner.model.ChangeReasonModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'ownerChangeCd', type: "int", defaultValue: 0},
        {name: 'changeReason', type: "string"},
    ]
});