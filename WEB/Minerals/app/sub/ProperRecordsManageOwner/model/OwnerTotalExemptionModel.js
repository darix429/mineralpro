Ext.define('Minerals.sub.ProperRecordsManageOwner.model.OwnerTotalExemptionModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'totalExemptionCd', type: "int", defaultValue: 0},
        {name: 'totalExemptionDesc', type: "string", defaultValue: 'UNASSIGNED'}
    ]
});