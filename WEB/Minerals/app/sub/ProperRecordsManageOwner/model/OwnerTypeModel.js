Ext.define('Minerals.sub.ProperRecordsManageOwner.model.OwnerTypeModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'ownerTypeCdx', type: "int", defaultValue: 0},
        {name: 'ownerType', type: "string"},
    ]
});