Ext.define('Minerals.sub.ProperRecordsManageOwner.model.OwnerExemptionTypeModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'exemptionCd', type: "int", defaultValue: 0},
        {name: 'exemptionType', type: "int", defaultValue: 3440},
        {name: 'exemptionTypeDesc', type: "string", defaultValue: 'TOTAL EXEMPTION'},
        {name: 'exemptionCdDesc', type: "string", defaultValue: 'UNASSIGNED'}
    ]
});