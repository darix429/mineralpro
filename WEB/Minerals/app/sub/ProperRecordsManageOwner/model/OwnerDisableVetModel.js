Ext.define('Minerals.sub.ProperRecordsManageOwner.model.OwnerDisableVetModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'disableVetCd', type: "int", defaultValue: 0},
        {name: 'disableVetDesc', type: "string", defaultValue: 'UNASSIGNED'},
        {name: 'amount', type: "string", defaultValue: '0'}
    ]
});