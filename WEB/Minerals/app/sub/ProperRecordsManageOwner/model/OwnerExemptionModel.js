Ext.define('Minerals.sub.ProperRecordsManageOwner.model.OwnerExemptionModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'ownerId', type: "int", defaultValue: ''},
        {name: 'idOwnerExemption', type: "int", defaultValue: 1},
        {name: 'ownerName', type: "string", defaultValue: ''},
        {name: 'effectiveDt', type: "string", defaultValue: '2017-03-10T08:30:00'},
        {name: 'expirationDt', type: "string", defaultValue: '2018-03-10T08:30:00'},
        {name: 'effectiveYear', type: "string", defaultValue: '2017'},
        {name: 'expirationYear', type: "string", defaultValue: '2018'},
        {name: 'amount', type: "string", defaultValue: '0'},
        {name: 'exemptionCd', type: "int", defaultValue: 0},
        {name: 'exemptionType', type: "int", defaultValue: 3430},
        {name: 'exemptionTypeDesc', type: "string", defaultValue: 'TOTAL EXEMPTION'},
        {name: 'exemptionCdDesc', type: "string", defaultValue: 'UNASSIGNED'}
    ]
});