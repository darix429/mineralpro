Ext.define('Minerals.sub.ProperRecordsManageOwner.model.OwnerTaxUnitModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'ownerId', type: "int", defaultValue: 0},
        {name: 'leaseId', type: "int", defaultValue: 0},
        {name: 'operatorId', type: "int", defaultValue: 0},
        {name: 'interestPercent', type: "string", defaultValue: 0},
        {name: 'interestTypeCd', type: "int", defaultValue: 0},
        {name: 'ownerValue', type: "int", defaultValue: 0},
        {name: 'taxUnitCd', type: "int", defaultValue: 0},
        {name: 'taxUnitTypeCd', type: "int", defaultValue: 0},
        {name: 'taxUnitPercent', type: "string", defaultValue: 0},
        {name: 'estimatedTaxRate', type: "string", defaultValue: 0},
        {name: 'taxUnitAbbr', type: "int", defaultValue: 0},
        {name: 'taxValue', type: "string", defaultValue: 0},
        {name: 'taxUnitName', type: "string", defaultValue: ''},
        {name: 'operatorName', type: "string", defaultValue: ''},
        {name: 'leaseName', type: "string", defaultValue: ''},
        {name: 'interestTypeDesc', type: "string", defaultValue: ''},
    ]
});
