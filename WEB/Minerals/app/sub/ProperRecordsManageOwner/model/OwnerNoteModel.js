Ext.define('Minerals.sub.ProperRecordsManageOwner.model.OwnerNoteModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'ownerId', type: "int", defaultValue: 1},

        {name: 'idOwnerNote', type: "int", defaultValue: 1},
        
        {name: 'seqNumber', type: "string", defaultValue: ''},

        {name: 'createDt', type: "string", defaultValue: ''},

        {name: 'ownerSecurityDesc', type: "int", defaultValue: 2},

        {name: 'quickNoteCd', type: "string", defaultValue: ''},

        {name: 'securityCd', type: "string", defaultValue: ''},

        {name: 'groupCd', type: "string", defaultValue: ''},

        {name: 'referenceAppraisalYear', type: "string", defaultValue: ''},

        {name: 'ownerNote', type: "string", defaultValue: ''},

    ]
});