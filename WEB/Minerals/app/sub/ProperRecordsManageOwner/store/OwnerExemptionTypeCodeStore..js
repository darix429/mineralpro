Ext.define('Minerals.sub.ProperRecordsManageOwner.store.OwnerExemptionTypeCodeStore', {
    extend: 'MineralPro.store.BaseStore',
    //autoLoad: true,

    model: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerExemptionTypeModel',

    proxy: {
        api: {
          //  create: '/api/ProperRecords/ProperRecordsManageOwner/CreateOwner',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageOwnerExemption/ReadExemptionTypeCode',
          //  update: '/api/ProperRecords/ProperRecordsManageOwner/UpdateOwner'
        }      
    }
    
}); 
