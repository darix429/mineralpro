Ext.define('Minerals.sub.ProperRecordsManageOwner.store.ChangeReasonStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'Minerals.sub.ProperRecordsManageOwner.model.ChangeReasonModel',

    proxy: {
        api: {
            read: '/api/Minerals/ProperRecords/ProperRecordsManageOwner/ReadChangeReason',
        }      
    }
    
});