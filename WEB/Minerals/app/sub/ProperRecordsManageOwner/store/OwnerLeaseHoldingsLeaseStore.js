Ext.define('Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsLeaseStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    model: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerLeaseHoldingsModel',

    proxy: {
        api: {
            read: '/api/Minerals/ProperRecords/ProperRecordsManageOwnerLeaseHoldings/ReadOwnerLeaseHoldingsLease',
        }      
    }
    
}); 
