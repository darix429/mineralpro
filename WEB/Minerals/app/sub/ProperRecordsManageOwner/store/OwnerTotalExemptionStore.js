Ext.define('Minerals.sub.ProperRecordsManageOwner.store.OwnerTotalExemptionStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerTotalExemptionModel',

    proxy: {
        api: {
            read: '/api/Minerals/ProperRecords/ProperRecordsManageOwnerExemption/ReadTotalExemption',
        }      
    }
    
}); 
