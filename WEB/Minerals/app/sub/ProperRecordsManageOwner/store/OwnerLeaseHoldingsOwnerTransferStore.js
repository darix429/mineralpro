Ext.define('Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsOwnerTransferStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    model: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerLeaseHoldingsModel',

    proxy: {
        api: {
          //  create: '/api/Minerals/ProperRecords/ProperRecordsLeaseOwner/CreateLeaseOwner?subjectType=owner',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageOwnerLeaseHoldings/ReadOwnerLeaseHoldingsOwnerTransfer',
         //   update: '/api/Minerals/ProperRecords/ProperRecordsLeaseOwner/UpdateLeaseOwner?subjectType=owner'
        }      
    }
    
}); 
