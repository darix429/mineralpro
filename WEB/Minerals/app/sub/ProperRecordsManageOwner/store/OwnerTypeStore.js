Ext.define('Minerals.sub.ProperRecordsManageOwner.store.OwnerTypeStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerTypeModel',

    proxy: {
        api: {
            read: '/api/Minerals/ProperRecords/ProperRecordsManageOwner/ReadOwnerType',
        }      
    }
    
});