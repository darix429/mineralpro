Ext.define('Minerals.sub.ProperRecordsManageOwner.store.OwnerHeaderDetailStore', {
    extend: 'MineralPro.store.BaseStore',
//    autoLoad: true,
//    tabChangeReload: false, //make sure autoload=true if this is false;
    showMask: true,
    model: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerModel',

    proxy: {
        api: {
//            create: '/api/Minerals/ProperRecords/ProperRecordsManageOwner/CreateOwner',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageOwner/ReadManageOwnerHeaderDetail',
//            update: '/api/Minerals/ProperRecords/ProperRecordsManageOwner/UpdateOwner'
        }      
    }
    
}); 
