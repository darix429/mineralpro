Ext.define('Minerals.sub.ProperRecordsManageOwner.store.OwnerExemptionStore', {
    extend: 'MineralPro.store.BaseStore',
    //autoLoad: true,
    showMask: true,
    model: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerExemptionModel',

    proxy: {
        api: {
            create: '/api/Minerals/ProperRecords/ProperRecordsManageOwnerExemption/CreateOwnerExemption',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageOwnerExemption/ReadOwnerExemption',
            update: '/api/Minerals/ProperRecords/ProperRecordsManageOwnerExemption/UpdateOwnerExemption'
        }      
    }
    
}); 
