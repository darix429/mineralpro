Ext.define('Minerals.sub.ProperRecordsManageOwner.store.OwnerTaxUnitStore', {
    extend: 'MineralPro.store.BaseStore',
    //autoLoad: true,
    showMask: true,
    groupField: 'taxUnitName',
    model: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerTaxUnitModel',
    proxy: {
        api: {
           // create: '',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageOwnerTaxUnit/ReadOwnerTaxUnit',
          //  update: ''
        }      
    }
    
}); 
