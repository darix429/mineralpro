Ext.define('Minerals.sub.ProperRecordsManageOwner.store.OwnerStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: false,
    tabChangeReload: false,
//    tabChangeReload: false, //make sure autoload=true if this is false;
    showMask: false,
    // statefulFilters: true,
    model: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerModel',
    listeners: {
        beforeload: function(store, operation, eOpts){
            if(!store.getRemoteFilter()){
                return false;
            }
        },
        load: function (store, records, successful, operation, eOpts) {
            var me = this;
        },
    },
    proxy: {
        type: 'ajax',
        timeout: 150000,//2min 30sec
        api: {
            create: '/api/Minerals/ProperRecords/ProperRecordsManageOwner/CreateOwner',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageOwner/ReadManageOwner',
            update: '/api/Minerals/ProperRecords/ProperRecordsManageOwner/UpdateOwner'
        }      
    }
    
}); 
