Ext.define('Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsStore', {
    extend: 'MineralPro.store.BaseStore',
    //autoLoad: true,
    //groupField: 'interestTypeDesc',
    showMask: true,
    model: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerLeaseHoldingsModel',

    proxy: {
        api: {
            create: '/api/Minerals/ProperRecords/ProperRecordsLeaseOwner/CreateLeaseOwner?subjectType=owner',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageOwnerLeaseHoldings/ReadOwnerLeaseHoldings',
            update: '/api/Minerals/ProperRecords/ProperRecordsLeaseOwner/UpdateLeaseOwner?subjectType=owner'
        }      
    }
    
}); 
