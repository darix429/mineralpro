Ext.define('Minerals.sub.ProperRecordsManageOwner.store.OwnerDisableVetStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerDisableVetModel',

    proxy: {
        api: {
            read: '/api/Minerals/ProperRecords/ProperRecordsManageOwnerExemption/ReadDisableVet',
        }      
    }
    
}); 
