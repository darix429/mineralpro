Ext.define('Minerals.sub.ProperRecordsManageOwner.store.OwnerLeaseHoldingsTransferCalculationStore', {
    extend: 'MineralPro.store.BaseStore',
    //autoLoad: true,
    showMask: true,
    model: 'Minerals.sub.ProperRecordsManageOwner.model.OwnerLeaseHoldingsModel',

    proxy: {
        api: {
          //  create: '/api/Minerals/createTransfer',
         //   read: '/api/Minerals/ProperRecords/ProperRecordsManageOwnerLeaseHoldings/ReadOwnerLeaseHoldings',
            update: '/api/Minerals/ProperRecords/ProperRecordsLeaseOwner/TransferLeaseOwner'
        }      
    }
    
}); 
