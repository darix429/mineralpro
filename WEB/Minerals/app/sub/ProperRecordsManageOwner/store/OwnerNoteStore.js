Ext.define('Minerals.sub.ProperRecordsManageOwner.store.OwnerNoteStore', {
    extend: 'MineralPro.store.BaseStore',
    // autoLoad: true,
    constructor : function(config) {
        this.callParent([config]);
        this.proxy.on('exception', this.onProxyException, this);
    },
    operationRecords: {},
    onProxyException: function(proxy, response, operation, eOpts) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering OwnerNoteStore onProxyException.')
        var store = this;
        var storeId = store.getStoreId();
        var msgBoxItemId = storeId.replace(/\./g,'')+'MessageBox';
        var request = operation.getRequest();
        var action = request.getAction();
        Ext.getBody().unmask();
        var errorText = '<p style="font-weight:bold;">Something went wrong while connecting to the server.<p/><span style="font-weight:bold;line-height:25px;">Exception Details:</span>';
        var msgbox = Ext.ComponentQuery.query('#'+msgBoxItemId);
        var details = 'Request Action: '+action+'<br/>Request Status: '+response.status+'<br/>Request Status Text: '+response.statusText+'<br/>Response Text: '+response.responseText;
        details = '<p style="margin-top:5px;margin-left: 15px;color: red;">'+details;
        var additionalNote = '</p><p style="font-weight:bold;">If this error persists, please check you\'re server.</p>';
        var errCnfg = {
            message: errorText+details+additionalNote,
            icon: Ext.Msg.ERROR,
            buttons: Ext.Msg.OK,
            fn: function(btn){
                if(action == 'create' || action == 'update'){
                    store.rejectChanges();
                }
            }
        };
        
        store.operationRecords[action] = store.operationRecords[action] || [];
        store.operationRecords[action] = Ext.Array.merge(store.operationRecords[action], operation.getRecords());

        if(msgbox.length){
            msgbox[0].errorDetails.push(details);
            var allDetails = msgbox[0].errorDetails.join('');
            errCnfg.message = errorText+allDetails+additionalNote;
            msgbox[0].show(errCnfg);
        }
        else{
            Ext.create('Ext.window.MessageBox', {
                // set closeAction to 'destroy' if this instance is not
                // intended to be reused by the application
                closeAction: 'hide',
                title: 'Error!',
                itemId: msgBoxItemId,
                errorDetails: [details]
            }).show(errCnfg);
        }
        Ext.MessageBox.close();//close active message box
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OwnerNoteStore onProxyException.')
    },
    rejectChanges: function(){
        var store = this;
        var storeId = store.getStoreId();
        var msgBoxItemId = storeId.replace(/\./g,'')+'MessageBox';
        var msgbox = Ext.ComponentQuery.query('#'+msgBoxItemId);
        msgbox.forEach(function(msgbox){
            msgbox.destroy();
        })
        var refreshGrids = function(callback){
            /*------------------------------ START ------------------------------
            | Query all grids that uses this store and refresh its view to reapply the css class record-dirty class so it would be colored to red
            */
            var grids = Ext.ComponentQuery.query('mainViewPort [region=center] grid').filter(function(grid){
                return grid.getStore().id==store.id;
            });
            callback(grids);
            /*
            | Query all grids that uses this store and refresh its view to reapply the css class record-dirty class so it would be colored to red
            | ------------------------------ END ------------------------------
            */
        }
        var reject = function(){
            for(var action in store.operationRecords){
                var records = store.operationRecords[action];
                records.forEach(function(rec){
                    var indx = store.indexOf(rec);
                    if(indx > -1){
                        var stRec = store.getAt(indx);
                        stRec.reject();
                        if(action == 'create'){
                            store.remove(stRec);
                        }
                        else{
                            stRec.set(stRec.previousValues);
                            stRec.dirty = false;
                        }
                    }
                })
            }
            store.operationRecords = {};
            refreshGrids(function(grids){
                grids.forEach(function(grid){
                    grid.getView().refresh();
                    if(grid.down('#saveToDbButtonItemId')){
                        grid.down('#saveToDbButtonItemId').disable(true);
                    }
                });
            });
        };
        Ext.Msg.show({
            // title: '',
            msg: 'Do you want to keep you\'re changes?',
            icon: Ext.Msg.ERROR,
            buttons: Ext.Msg.YESNO,
            listeners: {
                beforeclose: function(panel, eOpts){
                    reject();
                }
            },
            fn: function (btn) {
                if (btn === 'yes') {
                    for(var action in store.operationRecords){
                        var records = store.operationRecords[action];
                        records.forEach(function(rec){
                            var indx = store.indexOf(rec);
                            if(indx > -1){
                                var stRec = store.getAt(indx);
                                stRec.dirty = true;
                                if(action == 'create'){
                                    stRec.phantom = true;
                                }
                            }
                        })
                    }
                    refreshGrids(function(grids){
                        grids.forEach(function(grid){
                            grid.getView().refresh();
                            if(grid.down('#saveToDbButtonItemId')){
                                grid.down('#saveToDbButtonItemId').enable(true);
                            }
                        })
                    })
                }
                else{
                    reject();
                }
            }
        });
    },
    showMask: true,
    model: 'Minerals.model.MineralValuation.NotesModel',

    proxy: {
        api: {
            create: '/api/Minerals/ProperRecords/ProperRecordsManageNote/CreateManageNote?subjectTypeCd=3',
            read: '/api/Minerals/ProperRecords/ProperRecordsManageNote/ReadManageNote?subjectTypeCd=3',
            update: '/api/Minerals/ProperRecords/ProperRecordsManageNote/UpdateManageNote?subjectTypeCd=3'
        }
    }
    
}); 
