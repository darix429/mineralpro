Ext.define('Minerals.store.ProperRecords.GravityCdStore', {
    extend: 'MineralPro.store.BaseStore',
//    autoLoad: true,
    
    model: 'Minerals.model.ProperRecords.GravityCdModel',

    proxy: {
        api: {
            read: '/api/Minerals/ProperRecords/ReadGravityCode',
        }      
    }
    
}); 
