Ext.define('Minerals.store.ProperRecords.AgencyStore', {
    extend: 'MineralPro.store.BaseStore',
   // autoLoad: true,
    
    model: 'Minerals.model.ProperRecords.AgencyModel',

    proxy: {
        api: {
            read: '/api/Minerals/ProperRecords/ReadAgency',
        }      
    }
    
}); 
