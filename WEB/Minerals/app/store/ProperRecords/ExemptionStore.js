Ext.define('Minerals.store.ProperRecords.ExemptionStore', {
    extend: 'MineralPro.store.BaseStore',
   // autoLoad: true,
    
    model: 'Minerals.model.ProperRecords.ExemptionModel',

    proxy: {
        api: {
            read: '/api/Minerals/ProperRecords/ReadExemption',
        }      
    }
    
}); 
