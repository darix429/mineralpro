Ext.define('Minerals.store.ProperRecords.OwnerComboStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,    
    model: 'Minerals.model.ProperRecords.OwnerComboModel',

    proxy: {
        api: {
          //  create: '/api/ProperRecords/ProperRecordsManageOwner/CreateOwner',
            read: '/api/Minerals/ProperRecords/ReadOwnerCombo',
          //  update: '/api/ProperRecords/ProperRecordsManageOwner/UpdateOwner'
        }      
    }
    
}); 
