Ext.define('Minerals.store.MineralValuation.QuickNoteStore', {
    extend: 'MineralPro.store.BaseStore',
//     autoLoad: true,
    
    model: 'Minerals.model.MineralValuation.QuickNoteModel',

     proxy: {
         api: {
             create: '',
             read: '/api/Minerals/ProperRecords/ProperRecordsManageNote/ReadQuickNote',
             update: ''
         }      
     }

//    data: [
//        {
//            quickNoteCd: 1,  
//            quickNoteDesc: 'lorem ipsum'
//        },
//        {
//            quickNoteCd: 2,  
//            quickNoteDesc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
//        },
//        {
//            quickNoteCd: 3,  
//            quickNoteDesc: 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s.'
//        }
//    ]
    
}); 
