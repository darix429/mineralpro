Ext.define('Minerals.store.MineralValuation.NoteTypeStore', {
    extend: 'MineralPro.store.BaseStore',
//     autoLoad: true,
    
    model: 'Minerals.model.MineralValuation.NoteTypeModel',

     proxy: {
         api: {
             create: '',
             read: '/api/Minerals/ProperRecords/ProperRecordsManageNote/ReadNoteType',
             update: ''
         }      
     }

//    data: [
//        {
//            quickNoteCd: 1,  
//            quickNoteDesc: 'lorem ipsum'
//        },
//        {
//            quickNoteCd: 2,  
//            quickNoteDesc: 'Lorem Ipsum is simply dummy text of the printing and typesetting industry.'
//        },
//        {
//            quickNoteCd: 3,  
//            quickNoteDesc: 'Lorem Ipsum has been the industrys standard dummy text ever since the 1500s.'
//        }
//    ]
    
}); 
