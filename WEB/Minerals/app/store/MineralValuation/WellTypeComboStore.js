Ext.define('Minerals.store.MineralValuation.WellTypeComboStore', {
    extend: 'MineralPro.store.BaseStore',
//    autoLoad: true,
    
    model: 'Minerals.model.MineralValuation.WellTypeComboModel',

    proxy: {
        api: {
            create: '',
            read: '/api/Minerals/MineralValuation/ReadWellTypeCombo',
            update: ''
        }      
    }
}); 
