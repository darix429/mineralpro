Ext.define('Minerals.store.MineralValuation.FieldComboStore', {
    extend: 'MineralPro.store.BaseStore',
//    autoLoad: true,
    
    model: 'Minerals.model.MineralValuation.FieldComboModel',

    proxy: {
        api: {
            create: '',
            read: '/api/Minerals/MineralValuation/ReadFieldCombo',
            update: ''
        }      
    }
}); 
