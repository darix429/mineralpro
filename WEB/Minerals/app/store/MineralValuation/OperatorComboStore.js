Ext.define('Minerals.store.MineralValuation.OperatorComboStore', {
    extend: 'MineralPro.store.BaseStore',
//    autoLoad: true,
    
    model: 'Minerals.model.MineralValuation.OperatorComboModel',

    proxy: {
        api: {
            create: '',
            read: '/api/Minerals/MineralValuation/ReadOperatorCombo',
            update: ''
        }      
    }
}); 
