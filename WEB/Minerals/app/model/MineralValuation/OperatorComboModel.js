Ext.define('Minerals.model.MineralValuation.OperatorComboModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'operatorId', type: "int", defaultValue: ''},
        {name: 'operatorName', type: "string", defaultValue: ''},
    ]
});