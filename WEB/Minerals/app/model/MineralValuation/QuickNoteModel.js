Ext.define('Minerals.model.MineralValuation.QuickNoteModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'quickNoteCd', type: "string", defaultValue: ''},
        {name: 'quickNoteDesc', type: "string", defaultValue: ''},
    ]
});