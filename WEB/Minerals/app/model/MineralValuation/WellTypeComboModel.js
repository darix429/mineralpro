Ext.define('Minerals.model.MineralValuation.WellTypeComboModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'wellTypeCd', type: "string", defaultValue: ''},
        {name: 'wellTypeDesc', type: "string", defaultValue: ''},
    ]
});