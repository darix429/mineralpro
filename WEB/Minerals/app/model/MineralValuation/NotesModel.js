Ext.define('Minerals.model.MineralValuation.NotesModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[

        {name: 'subjectId', type: "int", defaultValue: 1},      
        {name: 'seqNumber', type: "int", defaultValue: ''},
        {name: 'createDt', type: "string", defaultValue: ''},
        {name: 'subjectId', type: "int", defaultValue: 0},
        {name: 'securityCd', type: "int", defaultValue: 1},
        {name: 'quickNoteCd', type: "int", defaultValue: 0},
        {name: 'rrcNumber', type: "int", defaultValue: 0},
        {name: 'quickNoteDesc', type: "string", defaultValue: ''},
        {name: 'groupCd', type: "string", defaultValue: ''},
        {name: 'referenceAppraisalYear', type: "string", defaultValue: ''},
        {name: 'noteTypeCd', type: "string", defaultValue: ''},
        {name: 'noteTypeDesc', type: "string", defaultValue: ''},
        {name: 'note', type: "string", defaultValue: ''},
        {name: 'noteAppraiserName', type: "string", defaultValue: ''},

    ]
});