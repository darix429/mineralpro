Ext.define('Minerals.model.MineralValuation.FieldComboModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'fieldId', type: "int", defaultValue: ''},
        {name: 'fieldName', type: "string", defaultValue: ''},
    ]
});