Ext.define('Minerals.model.MineralValuation.NoteTypeModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'noteTypeCd', type: "string", defaultValue: ''},
        {name: 'noteTypeDesc', type: "string", defaultValue: ''},
    ]
});