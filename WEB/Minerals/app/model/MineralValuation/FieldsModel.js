Ext.define('Minerals.model.MineralValuation.FieldsModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'rrcNumber', type: "int", defaultValue: 0},      
        {name: 'fieldId', type: "int", defaultValue: 0},
        {name: 'fieldName', type: "string", defaultValue: ''},
        {name: 'wellDepth', type: "int", defaultValue: 0},
        {name: 'gravityCd', type: "int", defaultValue: 0},
        {name: 'gravityDesc', type: "string", defaultValue: ''},
    ]
});