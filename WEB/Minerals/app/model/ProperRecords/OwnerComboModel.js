Ext.define('Minerals.model.ProperRecords.OwnerComboModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'displayField', type: "string", defaultValue: ''},
        {name: 'valueField', type: "int", defaultValue: ''},
        {name: 'fieldName', type: "string", defaultValue: ''},
    ]
});