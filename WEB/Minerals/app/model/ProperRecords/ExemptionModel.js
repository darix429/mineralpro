Ext.define('Minerals.model.ProperRecords.ExemptionModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'exemptionCd', type: "int", defaultValue: 0},
        {name: 'exemptionDesc', type: "string", defaultValue: ''},
    ]
});