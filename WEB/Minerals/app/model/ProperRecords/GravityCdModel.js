Ext.define('Minerals.model.ProperRecords.GravityCdModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'gravityDesc', type: "string", defaultValue: ''},
        {name: 'gravityCd', type: "int", defaultValue: ''}
    ]
});