Ext.define('Minerals.model.ProperRecords.AgencyModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'agencyCdx', type: "string", defaultValue: '00000'},
        {name: 'agencyName', type: "string", defaultValue: ''},
    ]
});