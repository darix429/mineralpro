Ext.define('ChiefAppraiser.sub.DefaultPage.store.AccountStatusByYearStore', {
    extend: 'Ext.data.Store',

    alias: 'store.account-status-by-year',

    fields: ['nbrOfAccount', 'acStatus', 'percent' ],
    data: [
        { nbrOfAccount: 253323, acStatus: 'Certified', percent: 42.2},
        { nbrOfAccount: 7814, acStatus: 'Permit Incomplete', percent: 1.3 },
        { nbrOfAccount: 37487, acStatus: 'Valuation Incomplete', percent: 6.2 },
        { nbrOfAccount: 228031, acStatus: 'Inactive', percent: 37.8},
        { nbrOfAccount: 739, acStatus: 'BPP Field Work Incomplete', percent: 0.1 },
        { nbrOfAccount: 37308, acStatus: 'Appraised', percent: 6.2 },
        { nbrOfAccount: 37512, acStatus: 'Under Protest', percent: 6.2 }
    ]
});
