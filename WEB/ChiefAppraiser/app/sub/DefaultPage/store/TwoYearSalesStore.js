Ext.define('ChiefAppraiser.sub.DefaultPage.store.TwoYearSalesStore', {
    extend: 'Ext.data.Store',
    alias: 'store.two-year-sales',

    fields: ['vType', '2015', '2016'],
    data: [
        { vType: 'Market Value', 2015: 14349196014, 2016: 14107933382 },
        { vType: 'Appraised Value', 2015: 12832556712, 2016: 12545785263 },
        { vType: 'Taxable Value', 2015: 11166311162, 2016: 10796223297}
    ]

});
