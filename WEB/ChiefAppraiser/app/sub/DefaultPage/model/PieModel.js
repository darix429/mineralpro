Ext.define('ChiefAppraiser.sub.DefaultPage.model.PieModel', {
    extend: 'ChiefAppraiser.sub.DefaultPage.model.BaseModel',
    fields: ['id', 'g0', 'g1', 'g2', 'g3', 'g4', 'g5', 'g6', 'name']
});
