Ext.define('ChiefAppraiser.sub.DashboardShowOverallSummary.store.TenYrAppraisalStore', {
    extend: 'ChiefAppraiser.store.ChiefAppraiserBaseStore',
    autoLoad: false,
    reloadOnAppraisalYrChange: true,
    storeId: 'TenYrAppraisalStore',
    forceReload: false,
    appraisalYear: '',
    proxy: {
        api: {
            create: '',
            read: '/api/ChiefAppraiser/DashboardShowOverallSummary/ReadTenYrAppraisal',
            update: ''
        },
        timeout: 300000,
        reader: {
            type: 'json',
            transform: {
                fn: function(response){
                    MineralPro.config.RuntimeUtility.DebugLog('Entering TenYrAppraisalStore transform.') 
                    if(response.success){
                        var data = response.data;
                        var appraisalYear = Ext.getStore('TenYrAppraisalStore').getProxy().getExtraParams().appraisalYear;
                        var sortAlphaNum = function(arr){
                            var alpha = arr.filter(function(item){ return isNaN(parseInt(item)); }).sort();
                            var numeric = arr.filter(function(item){ return !isNaN(parseInt(item)); }).sort(function(a, b){
                                return parseInt(b) - parseInt(a);
                            })
                            return Ext.Array.merge(alpha, numeric);
                        }
                        var formattedEntityData = [];
                        var taxUnits = Array.from(data, function(item){
                            return item.taxUnit;
                        }).filter(function(x, i, a){ 
                            return a.indexOf(x) == i; 
                        });
                        taxUnits = sortAlphaNum(taxUnits);
                        var remZeroEmptyColData = function(nd){
                            var cols = Ext.Object.getKeys(nd[0]).filter(function(item){
                                return ['id','item','details','detail_columns'].indexOf(item) == -1;
                            });
                            Ext.each(cols, function(col){
                                var hasValue = nd.some(function(row){
                                    var detRowVal = row[col] || '';
                                    detRowVal = detRowVal.trim()
                                    return detRowVal != '0' && detRowVal != '';
                                });
                                if(!hasValue){
                                    Ext.each(nd, function(row){
                                        row.detail_columns = row.detail_columns.filter(function(c){
                                            return c.dataIndex != col;
                                        })
                                    })
                                }
                            });
                            return nd;
                        };
                        var formatData = function(ndata){
                            var totallyExempt  = Ext.Object.getKeys(ndata).filter(function(item){ return item.startsWith('# of Owners-'); });
                            var group = {
                                'Total Appraised Value':['# of Unit Appraisals','# of Leases in Units','# of Lease Appraisals','# of D.O. Owners','# of D.O. Owner Interests'],
                                'Equipment Value':['# of Production Wells','# of Injection Wells','# of Service Wells'],
                                'Totally Exempt Value': totallyExempt, //['# of Owners-COLLEGE','# of Owners-ECTOR COUNTY','# of Owners-FEDERAL','# of Owners-LOCAL GOVERNMENT','# of Owners-NON-PROFIT/CHARITABLE','# of Owners-TEXAS (STATE)','# of Owners-UNIVERSITY LANDS'],
                                'Under $500 Exempt': ['# of Owners-UNDER $500'],
                                'Taxable Value': []
                            };
                            var nd = [];
                            for(var prop in group){
                                var cols = [];
                                var details = [];
                                var sortedColKeys = Ext.Object.getKeys(ndata[prop]);
                                sortedColKeys = sortAlphaNum(sortedColKeys);
                                if(typeof ndata[prop] == 'object' && group[prop].length){
                                    cols = [{
                                        "dataIndex": "item",
                                        "text": "Details",
                                        "width": 200
                                    }].concat(
                                        sortedColKeys.map(function(key){
                                            return {
                                                "dataIndex": key,
                                                "text": key,
                                                "align": "end",
                                                "width": 150,
                                                "style": {
                                                    "textAlign": 'center'
                                                },
                                                renderer: function(val, meta) {
                                                    val = val || ''
                                                    return  Ext.util.Format.number(val, '0,000');
                                                },
                                            }
                                        })
                                    );
                                    // cols.sort(function(a,b){
                                    //     a = a.dataIndex;
                                    //     b = b.dataIndex;
                                    //     return isNaN(parseInt(a)) && isNaN(parseInt(b)) ? b > a : (a == 'item' ? -1 : b == 'item' ? 1 : parseInt(b)-parseInt(a));
                                    // });
                                    details = group[prop].map(function(k){
                                        return Ext.Object.merge(ndata[k] || {},{
                                            item: k
                                        })  
                                    });
                                }
                                nd.push(Ext.Object.merge({
                                    "item": prop,
                                    "detail_columns": cols,
                                    "details": details
                                },ndata[prop]));
                            }
                            nd = remZeroEmptyColData(nd);
                            return nd;
                        };
                        var generateCmps = function(tab, formattedData){
                            if(tab){
                                var panel = tab.down('panel');
                                panel.removeAll();
                                var grids = [];
                                formattedData.forEach(function(item,i){
                                    // var gridCols = item.data[0].detail_columns.sort(function(a,b){
                                    //     a = a.dataIndex;
                                    //     b = b.dataIndex;
                                    //     return isNaN(parseInt(a)) && isNaN(parseInt(b)) ? b > a : (a == 'item' ? -1 : b == 'item' ? 1 : parseInt(b)-parseInt(a));
                                    // }).map(function(col){
                                    //     if(col.dataIndex == 'item'){
                                    //         return col;
                                    //     }
                                    //     col.renderer = function(val, meta) {
                                    //         val = val || ''
                                    //         return  Ext.util.Format.number(val, '$000,000');
                                    //     };
                                    //     return col;
                                    // })
                                    
                                    var gridCols = [];
                                    item.data[0].detail_columns.forEach(function(col){
                                        if(col.dataIndex == 'item'){
                                            gridCols.push(col);
                                        }
                                        else{
                                            gridCols.push(
                                                Ext.Object.merge({},col,{
                                                    renderer: function(val, meta) {
                                                        val = val || ''
                                                        return  Ext.util.Format.number(val, '$000,000');
                                                    }
                                                })
                                            );
                                        }
                                    })

                                    grids.push({
                                        xtype:'gridpanel',
                                        cls: 'nested-grid',
                                        viewConfig: {
                                            enableTextSelection: true,
                                            listeners: {
                                                // refresh: function (dataview) {
                                                //     Ext.each(dataview.panel.getColumns(), function (column) {
                                                //         if (column.getTdType() == 'gridcolumn') {
                                                //             column.autoSize();
                                                //         }
                                                //     });
                                                // },
                                                expandBody: function(rowNode, record, expandRow, eOpts){
                                                    // MineralPro.config.RuntimeUtility.DebugLog('Entering TenYrAppraisalStore expandBody');
                                                    // MineralPro.config.RuntimeUtility.DebugLog('Leaving TenYrAppraisalStore expandBody');
                                                },
                                                collapseBody: function(rowNode, record, expandRow, eOpts){
                                                    // MineralPro.config.RuntimeUtility.DebugLog('Entering TenYrAppraisalStore collapseBody');
                                                    // MineralPro.config.RuntimeUtility.DebugLog('Leaving TenYrAppraisalStore collapseBody');  
                                                }
                                            }
                                        },
                                        tools: [{
                                            text: 'Expand all',
                                            itemId: 'collapseBtn',
                                            iconCls: 'fa fa-plus',
                                            callback: function(owner, btn){
                                                MineralPro.config.RuntimeUtility.DebugLog('Entering TenYrAppraisalStore tools expandAll.') 
                                                var cls = btn.iconCls;
                                                var newCls = cls.indexOf('fa-plus') > -1 ? 'fa fa-minus' : 'fa fa-plus';
                                                var grid = btn.up('grid');
                                                grid.suspendLayouts();
                                                var expand = cls == 'fa fa-plus' ? true : false;
                                                var rowwidget = grid.getPlugin('rowwidget');
                                                rowwidget.expandAll( expand );
                                                grid.resumeLayouts(true);
                                                btn.setIconCls(newCls);
                                                MineralPro.config.RuntimeUtility.DebugLog('Leaving TenYrAppraisalStore tools expandAll.') 
                                            }
                                        }],
                                        title: item.entity || item.year,
                                        scrollable: 'x',
                                        layout: 'anchor',
                                        itemId: 'appraisalGrid'+(item.entity || item.year) ,
                                        hideMode: 'offsets',
                                        enableColumnHide: false,
                                        enableColumnMove: false,
                                        sortableColumns: false,
                                        store: {
                                            data: item.data
                                        },
                                        columns: gridCols,
                                        listeners: {
                                            columnresize: function(ct, column, width, eOpts){
                                                var me = this;
                                                me.query('grid').forEach(function(g){
                                                    g.getView().refresh();
                                                });
                                            }  
                                        },
                                        plugins: [
                                            {
                                                ptype: 'rowwidget',
                                                pluginId: 'rowwidget',
                                                widget: {
                                                    xtype: 'gridpanel',
                                                    enableColumnHide: false,
                                                    enableColumnMove: false,
                                                    enableColumnResize: false,
                                                    scrollable: false,
                                                    hideHeaders: true,
                                                    sortableColumns: false,
                                                    padding: 0,
                                                    width: '100%',
                                                    listeners: {
                                                        afterrender: function(gridWidget){
                                                            setTimeout(function(){
                                                                gridWidget.getView().refresh();
                                                            },100);
                                                        }
                                                    },
                                                    viewConfig: {
                                                        enableTextSelection: true,
                                                        listeners: {
                                                            refresh: function (dataview) {
                                                                var gridColumns = dataview.ownerCt.ownerCmp.getGridColumns();
                                                                if(dataview && dataview.panel && dataview.panel.getColumns()){
                                                                    dataview.panel.getColumns().forEach(function(column, i){
                                                                        var col = gridColumns[i+1];
                                                                        if(col && typeof col.getWidth=='function'){
                                                                            column.setWidth(col.getWidth());
                                                                        }
                                                                    })
                                                                    dataview.grid.setWidth('100%');
                                                                }
                                                            }
                                                        }
                                                    },
                                                    bind: {
                                                        store: {
                                                            data: '{record.details}',
                                                        },
                                                        columns: '{record.detail_columns}'
                                                    }
                                                },
                                                expandAll: function (expand) {
                                                    MineralPro.config.RuntimeUtility.DebugLog('Entering TenYrAppraisalStore gridpanel expandAll.') 
                                                    expand = typeof expand !== 'undefined' ? expand : true;
                                                    var grid = this.grid,
                                                    store = grid.getStore(),
                                                    rowExpander = grid.getPlugin('rowwidget'),
                                                    nodes = rowExpander.view.getNodes();
                                                    grid.suspendLayouts();
                                                    for (var i = 0; i < nodes.length; i++) {
                                                        var node = Ext.fly(nodes[i]);

                                                        if (node.hasCls(rowExpander.rowCollapsedCls) === expand) {
                                                            rowExpander.toggleRow(i, store.getAt(i));
                                                        }
                                                    }
                                                    grid.resumeLayouts(true);
                                                    MineralPro.config.RuntimeUtility.DebugLog('Leaving TenYrAppraisalStore gridpanel expandAll.') 
                                                },
                                            }
                                        ]
                                    });
                                });
                                panel.add(grids);
                            }
                        }
                        /*------------------------------ START ------------------------------
                        | appraisal by entity
                        */
                        var filterYearData = [];
                        for(var yr = appraisalYear; yr >= appraisalYear - 10; yr--){
                            var ndata = data.filter(function(item){ return item.appraisalYear==yr; }).reduce(function(obj, item){
                                obj[item.itemName]=obj[item.itemName] || {};
                                taxUnits.forEach(function(taxUnit){
                                    var exists = Ext.Object.getKeys(obj[item.itemName]).includes(taxUnit);
                                    if(!exists){
                                        obj[item.itemName][taxUnit] = "0";
                                    }
                                });
                                obj[item.itemName][item.taxUnit] = obj[item.itemName][item.taxUnit] || '';
                                obj[item.itemName][item.taxUnit] = item.number;
                                return obj;
                            },{});
                            formattedEntityData.push({
                                year: yr,
                                data: formatData(ndata)
                            });
                            filterYearData.push({category: yr});
                        }
                        var appraisalYearValuesByEntity = Ext.ComponentQuery.query('AppraisalYearValuesByEntity')[0];
                        var filterYearStore = Ext.create('Ext.data.Store',{
                            data: filterYearData
                        });
                        var appraisalYearValuesTagField = appraisalYearValuesByEntity.down('#filterTagField');
                        
                        appraisalYearValuesTagField.setStore(filterYearStore);
                        generateCmps(appraisalYearValuesByEntity, formattedEntityData);
                        /*
                        | appraisal by entity
                        | ------------------------------ END ------------------------------
                        */

                        /*------------------------------ START ------------------------------
                        | appraisal by appraisalyear
                        */
                        var filterEntityData = [];
                        var formattedYearData = [];
                        taxUnits.forEach(function(taxUnit){
                            var ndata = data.filter(function(item){ return item.taxUnit==taxUnit; }).reduce(function(obj, item){
                                obj[item.itemName]=obj[item.itemName] || {};
                                for(var yr = appraisalYear; yr >= appraisalYear - 10; yr--){
                                    var exists = Ext.Object.getKeys(obj[item.itemName]).includes(yr.toString());
                                    if(!exists){
                                        obj[item.itemName][yr] = "0";
                                    }
                                }
                                obj[item.itemName][item.appraisalYear] = obj[item.itemName][item.appraisalYear] || '';
                                obj[item.itemName][item.appraisalYear] = item.number;
                                return obj;
                            },{});

                            formattedYearData.push({
                                entity: taxUnit,
                                data: formatData(ndata)
                            });
                            filterEntityData.push({category: taxUnit});
                        });
                        var entityValuesByAppYr = Ext.ComponentQuery.query('EntityValuesByAppraisalYear')[0];
                        var filterEntityStore = Ext.create('Ext.data.Store',{
                            data: filterEntityData
                        });
                        var entityValuesTagField = entityValuesByAppYr.down('#filterTagField');
                        
                        entityValuesTagField.setStore(filterEntityStore);
                        generateCmps(entityValuesByAppYr, formattedYearData);
                        /*
                        | appraisal by appraisalyear
                        | ------------------------------ END ------------------------------
                        */
                            
                        
                    }
                    MineralPro.config.RuntimeUtility.DebugLog('Leaving TenYrAppraisalStore transform.')    
                    return data;
                }
            }
        }
    },
    groupField: 'category',
    operations: [],
    listeners: {
        beforeload: function(store, operation){
            MineralPro.config.RuntimeUtility.DebugLog('Entering TenYrAppraisalStore beforeload.') 
            var me = store;
            if(MineralPro.config.Runtime.appraisalYear.length == 0){
               MineralPro.config.Runtime.appraisalYear = new Date().getFullYear();
            }
            var params = me.getProxy().extraParams;
            
            var abortAll = false;
            if(this.operations.length){
                abortAll = this.operations.every(function(op, indx){
                    var opExtraParams = op._proxy.extraParams
                    var equal = Ext.Object.getKeys(params).every(function(key){
                        return params[key] == opExtraParams[key];
                    })
                    MineralPro.config.RuntimeUtility.DebugLog(equal?'Extra Params are equal with operation #'+indx : 'Will abort operation #'+indx); 
                    if(!equal){
                        op.abort = true;
                        op.abort();
                        return true;
                    }
                    return false;
                })
            }
            var diffParamsWithPrev = !!(store.getCount() <= 0 || store.appraisalYear != operation._proxy.extraParams.appraisalYear);
            MineralPro.config.RuntimeUtility.DebugLog('operationsLength: '+this.operations.length+'\nabortAll: '+abortAll+'\ndiffParamsWithPrev: '+diffParamsWithPrev+'\nstoreAppraisalYear: '+store.appraisalYear+'\nopAppraisalYear: '+operation._proxy.extraParams.appraisalYear);
            if(store.forceReload || ((abortAll || !this.operations.length) && diffParamsWithPrev)){
                MineralPro.config.RuntimeUtility.DebugLog('abortAll: '+abortAll+' Execute latest operation, cancel previously created operations.')
                params['appraisalYear']=MineralPro.config.Runtime.appraisalYear;         
                me.getProxy().extraParams=params;
                this.operations.push(operation);
            }
            else if((!abortAll && this.operations.length) || !diffParamsWithPrev){
                /*------------------------------ START ------------------------------
                | cancel latest request when a request with the same params already exists.
                */
                MineralPro.config.RuntimeUtility.DebugLog('Cancel latest operation since an existing operation with the same params is already requested.') 
                return false;
            }
            else{
                MineralPro.config.RuntimeUtility.DebugLog('ELSE') ;
            }
            MineralPro.config.RuntimeUtility.DebugLog('Leaving TenYrAppraisalStore beforeload.') 
        },
        load: function(store, records, successful, operation, eOpts){
            MineralPro.config.RuntimeUtility.DebugLog('Entering TenYrAppraisalStore load.');
            /*Will remove aborted operations and the current operation*/
            this.operations = this.operations.filter(function(op){
                return (op.id != operation.id) && !op.abort;
            });
            store.appraisalYear = operation._proxy.extraParams.appraisalYear;
            store.forceReload = false;
            MineralPro.config.RuntimeUtility.DebugLog('Leaving TenYrAppraisalStore load.') 
        }
    }
}); 
