Ext.define('ChiefAppraiser.sub.DashboardShowOverallSummary.store.AppraisalProgressDetailedStore', {
    extend: 'ChiefAppraiser.store.ChiefAppraiserBaseStore',
    requires: ['Ext.data.reader.Json'],
    autoLoad: false,
    reloadOnAppraisalYrChange: true,
    storeId: 'AppraisalProgressDetailedStore',
    proxy: {
        api: {
            create: '',
            read: '/api/ChiefAppraiser/DashboardShowOverallSummary/ReadAppraisalProgressDetails',
            update: ''
        },
        timeout: 300000
    },
    listeners: {
        beforeload: function(store, operation){
            var me = store;
            if(MineralPro.config.Runtime.appraisalYear.length == 0){
               MineralPro.config.Runtime.appraisalYear = new Date().getFullYear();
            }
            var params = me.getProxy().extraParams;
            params['appraisalYear']=MineralPro.config.Runtime.appraisalYear;         
            me.getProxy().extraParams=params;
        }
    }
}); 
