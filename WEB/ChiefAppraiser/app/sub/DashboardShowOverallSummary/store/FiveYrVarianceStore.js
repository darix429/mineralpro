Ext.define('ChiefAppraiser.sub.DashboardShowOverallSummary.store.FiveYrVarianceStore', {
    extend: 'ChiefAppraiser.store.ChiefAppraiserBaseStore',
   	autoLoad: false,
    reloadOnAppraisalYrChange: true,
 	storeId: 'FiveYrVarianceStore',
    
    proxy: {
        api: {
            create: '',
            read: '/api/ChiefAppraiser/DashboardShowOverallSummary/ReadFiveYrVariance',
            update: ''
        },
        timeout: 300000 
    }
    
}); 
