Ext.define('ChiefAppraiser.sub.DashboardShowOverallSummary.store.NumberOfJEsStore', {
    extend: 'ChiefAppraiser.store.ChiefAppraiserBaseStore',
    requires: ['Ext.data.reader.Json'],
    autoLoad: false,
    reloadOnAppraisalYrChange: true,
    storeId: 'NumberOfJEsStore',
    proxy: {
        api: {
            create: '',
            read: '/api/ChiefAppraiser/DashboardShowOverallSummary/ReadNumberOfJEs',
            update: ''
        },
        timeout: 300000
    },
    listeners: {
        beforeload: function(store, operation){
            var me = store;
            if(MineralPro.config.Runtime.appraisalYear.length == 0){
               MineralPro.config.Runtime.appraisalYear = new Date().getFullYear();
            }
            var params = me.getProxy().extraParams;
            params['appraisalYear']=MineralPro.config.Runtime.appraisalYear;         
            me.getProxy().extraParams=params;
        }
    }
}); 
