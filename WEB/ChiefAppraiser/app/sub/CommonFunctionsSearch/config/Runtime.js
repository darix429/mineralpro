/**
 * Contains the global configuration during runtime.
 */

Ext.define('ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime', {
    singleton: true, //default configuration for global variable

//**********************************************
//**shared configuration for all sub functions**
//**********************************************   
   searchOption: '',
   searchField: '',
   searchType: ''

})