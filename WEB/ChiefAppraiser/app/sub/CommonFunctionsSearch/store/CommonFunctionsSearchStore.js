Ext.define('ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
  
    model: 'ChiefAppraiser.sub.CommonFunctionsSearch.model.CommonFunctionsSearchModel',
    proxy: {

        api: {
        //    create: '',
            read: 'app/sub/CommonFunctionsSearch/api/ReadAccount.php',
       //     update: ''
//            destroy: ''
        }
       
    }
}); 
