Ext.define('ChiefAppraiser.sub.CommonFunctionsSearch.model.CommonFunctionsSearchTypeModel', {
    extend: 'MineralPro.model.BaseModel',
    fields: [
        {name: 'accountNum' , defaultValue: 1},
        {name: 'idAccount', defaultValue: ''},
        {name: 'appFunction', defaultValue: ''},
        {name: 'idApp', defaultValue: 1},
        {name: 'appName', defaultValue: ''}
    ]
});