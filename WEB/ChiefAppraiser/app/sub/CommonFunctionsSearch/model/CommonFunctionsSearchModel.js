Ext.define('ChiefAppraiser.sub.CommonFunctionsSearch.model.CommonFunctionsSearchModel', {
    extend: 'MineralPro.model.BaseModel',
   fields: [
        {name: 'accountNum' , defaultValue: '', type:"string"},
        {name: 'idAccount', defaultValue: 1 ,type:"int"},
        {name: 'idAccountYear', defaultValue: 1 ,type:"int"},
        {name: 'displayName', defaultValue: '',type:"string"},
        {name: 'name', defaultValue: '',type:"string"},
        {name: 'idApp', defaultValue: 1, type:"int"},
        {name: 'masterOwnerId', defaultValue: 1,type:"int"},
        {name: 'altAccountNum', defaultValue: '',type:"string"},
        {name: 'mapNum', defaultValue: '',type:"string"},
        {name: 'streetAddress', defaultValue: '',type:"string"},
        {name: 'landValue', defaultValue: 1,type:"int"},
        {name: 'imprValue', defaultValue: 1,type:"int"},
        {name: 'totalValue', defaultValue: 1,type:"int"},
    ],

});