
Ext.define('ChiefAppraiser.store.ChiefAppraiserBaseStore', {
    extend: 'Ext.data.Store',
    // autoLoad: true,
    constructor : function(config) {
        this.callParent([config]);
        this.proxy.on('exception', this.onProxyException, this);
    },
    operationRecords: {},
    onProxyException: function(proxy, response, operation, eOpts) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering ChiefAppraiserBaseStore onProxyException.')
        var store = this;
        var storeId = store.getStoreId();
        var msgBoxItemId = storeId.replace(/\./g,'')+'MessageBox';
        var request = operation.getRequest();
        var action = request.getAction();
        Ext.getBody().unmask();
        var errorText = '<p style="font-weight:bold;">Something went wrong while connecting to the server.<p/><span style="font-weight:bold;line-height:25px;">Exception Details:</span>';
        var msgbox = Ext.ComponentQuery.query('#'+msgBoxItemId);
        var details = 'Request Action: '+action+'<br/>Request Status: '+response.status+'<br/>Request Status Text: '+response.statusText+'<br/>Response Text: '+response.responseText;
        details = '<p style="margin-top:5px;margin-left: 15px;color: red;">'+details;
        var additionalNote = '</p><p style="font-weight:bold;">If this error persists, please check you\'re server.</p>';
        var errCnfg = {
            message: errorText+details+additionalNote,
            icon: Ext.Msg.ERROR,
            buttons: Ext.Msg.OK,
            fn: function(btn){
                if(action == 'create' || action == 'update'){
                    store.rejectChanges();
                }
            }
        };
        
        store.operationRecords[action] = store.operationRecords[action] || [];
        store.operationRecords[action] = Ext.Array.merge(store.operationRecords[action], operation.getRecords());
        if(msgbox.length){
            msgbox[0].errorDetails.push(details);
            var allDetails = msgbox[0].errorDetails.join('');
            errCnfg.message = errorText+allDetails+additionalNote;
            msgbox[0].show(errCnfg);
        }
        else{
            Ext.create('Ext.window.MessageBox', {
                // set closeAction to 'destroy' if this instance is not
                // intended to be reused by the application
                closeAction: 'hide',
                title: 'Error!',
                itemId: msgBoxItemId,
                errorDetails: [details]
            }).show(errCnfg);
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving ChiefAppraiserBaseStore onProxyException.')
    },
    rejectChanges: function(){
        var store = this;
        var storeId = store.getStoreId();
        var msgBoxItemId = storeId.replace(/\./g,'')+'MessageBox';
        var msgbox = Ext.ComponentQuery.query('#'+msgBoxItemId);
        msgbox.forEach(function(msgbox){
            msgbox.destroy();
        })
        var refreshGrids = function(callback){
            /*------------------------------ START ------------------------------
            | Query all grids that uses this store and refresh its view to reapply the css class record-dirty class so it would be colored to red
            */
            var grids = Ext.ComponentQuery.query('mainViewPort [region=center] grid').filter(function(grid){
                return grid.getStore().id==store.id;
            });
            callback(grids);
            /*
            | Query all grids that uses this store and refresh its view to reapply the css class record-dirty class so it would be colored to red
            | ------------------------------ END ------------------------------
            */
        }
        var reject = function(){
            for(var action in store.operationRecords){
                var records = store.operationRecords[action];
                records.forEach(function(rec){
                    var indx = store.indexOf(rec);
                    if(indx > -1){
                        var stRec = store.getAt(indx);
                        stRec.reject();
                        if(action == 'create'){
                            store.remove(stRec);
                        }
                        else{
                            stRec.set(stRec.previousValues);
                            stRec.dirty = false;
                        }
                    }
                })
            }
            store.operationRecords = {};
            refreshGrids(function(grids){
                grids.forEach(function(grid){
                    grid.getView().refresh();
                    if(grid.down('#saveToDbButtonItemId')){
                        grid.down('#saveToDbButtonItemId').disable(true);
                    }
                });
            });
        };
        Ext.Msg.show({
            // title: '',
            msg: 'Do you want to keep you\'re changes?',
            icon: Ext.Msg.ERROR,
            buttons: Ext.Msg.YESNO,
            listeners: {
                beforeclose: function(panel, eOpts){
                    reject();
                }
            },
            fn: function (btn) {
                if (btn === 'yes') {
                    for(var action in store.operationRecords){
                        var records = store.operationRecords[action];
                        records.forEach(function(rec){
                            var indx = store.indexOf(rec);
                            if(indx > -1){
                                var stRec = store.getAt(indx);
                                stRec.dirty = true;
                                if(action == 'create'){
                                    stRec.phantom = true;
                                }
                            }
                        })
                    }
                    refreshGrids(function(grids){
                        grids.forEach(function(grid){
                            grid.getView().refresh();
                            if(grid.down('#saveToDbButtonItemId')){
                                grid.down('#saveToDbButtonItemId').enable(true);
                            }
                        })
                    })
                }
                else{
                    reject();
                }
            }
        });
    },
    showMask: false,
    tabChangeReload: true,
    proxy: {
        type: 'ajax',
        reader: {
            type: 'json',
            rootProperty: 'data',
            successProperty: 'success',
             messageProperty : 'data'
        },
        writer: {
            type: 'json',
            writeAllFields: 'true'
        }
    },
    listeners: {
        beforeload: function () {
            var me = this;
            var params = me.getProxy().extraParams
            params['appraisalYear'] = MineralPro.config.Runtime.appraisalYear;
            params['dataCount'] = me.getCount();
            params['idAppUser'] = MineralPro.config.Runtime.idAppUser;
            me.getProxy().extraParams = params

            if (me.showMask) {
                Ext.getBody().mask('Loading...')
            }

        },
        beforesync: function () {
            var me = this;
            var params = me.getProxy().extraParams
            params['appraisalYear'] = MineralPro.config.Runtime.appraisalYear;
            me.getProxy().extraParams = params
        },
        load: function (store, records, successful, operation, eOpts) {
            // Ext.getBody().unmask();
            if(successful){
                var me = this;
                if (me.showMask) {
                    Ext.globalEvents.fireEvent('checkLoadingStores');//located in CPLoginController                                                                
                }
            }
            else{
                var error = operation.getError() || '';
                var storeId = store.getId() || '';
                if(typeof error == 'string'){
                    error = error.toLowerCase();
                }
                else{
                    var statusText = error.statusText || '';
                    var errMsg = error.response && error.response.responseText ? error.response.responseText : '';
                        
                    error = {
                        statusText: statusText,
                        error: errMsg,
                        storeId: storeId
                    }
                    error = JSON.stringify(error);
                }
                if(error != 'do not refresh the data'){
                    MineralPro.config.RuntimeUtility.DebugLog('error while loading store ');
                    MineralPro.config.RuntimeUtility.DebugLog(error);
                    // Ext.Msg.show({
                    //     title: 'Error',
                    //     msg: 'Unable to load data',
                    //     buttons: Ext.Msg.OK,
                    // });

                    // var errorWindow = Ext.ComponentQuery.query('errorwindow')[0];
                    // if(!errorWindow){
                    //     Ext.create('widget.errorwindow',{ //This class has been defined on Minerals.config.view.Help.ErrorWindow
                    //         store: store,
                    //         error: error
                    //     }).show();
                    // }
                    // else{
                    //     //The error Window already exists. Just update its properties
                    //     errorWindow.store = store;
                    //     errorWindow.error = error;
                    //     errorWindow.down('#errorMsg').getEl().dom.querySelector('#expand p.store-id-cont').innerHTML = store.getId();
                    // }
                }
                else{
                    Ext.getBody().unmask();
                }
            }
        },
        update: function (store) {
            if(store.view != null && store.view != ''){
                var dirty = store.getNewRecords().length > 0
                        || store.getUpdatedRecords().length > 0
                        || store.getRemovedRecords().length > 0;
                if (dirty) {
                    if (store.view.down('#saveToDbButtonItemId')) {
                        store.view.down('#saveToDbButtonItemId').enable(true)
                    } else if (store.view.up('form')) {
                        if (store.view.up('form').down('#saveToDbButtonItemId')) {
                            store.view.up('form').down('#saveToDbButtonItemId').enable(true)
                        }
                    }
                } else {
                    if (store.view.down('#saveToDbButtonItemId')) {
                        store.view.down('#saveToDbButtonItemId').disable(true)
                    } else if (store.view.up('form')) {
                        if (store.view.down('#saveToDbButtonItemId')) {
                            store.view.down('#saveToDbButtonItemId').disable(true)
                        }
                    }
                }
            }
        },
        add: function (store) {
            if(store.view != null && store.view != ''){
                var dirty = store.getNewRecords().length > 0
                        || store.getUpdatedRecords().length > 0
                        || store.getRemovedRecords().length > 0;
                if (dirty) {
                    if (store.view.down('#saveToDbButtonItemId')) {
                        store.view.down('#saveToDbButtonItemId').enable(true)
                    } else if (store.view.up('form')) {
                        if (store.view.down('#saveToDbButtonItemId')) {
                            store.view.down('#saveToDbButtonItemId').enable(true)
                        }
                    }
                } else {
                    if (store.view.down('#saveToDbButtonItemId')) {
                        store.view.down('#saveToDbButtonItemId').disable(true)
                    } else if (store.view.up('form')) {
                        if (store.view.up('form').down('#saveToDbButtonItemId')) {
                            store.view.up('form').down('#saveToDbButtonItemId').disable(true)
                        }
                    }
                }
            }
        }
    }
});
