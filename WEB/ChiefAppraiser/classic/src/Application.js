/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('ChiefAppraiser.Application', {
    extend: 'Ext.app.Application',
    name: 'ChiefAppraiser',
    controllers: [
        'AppMainController',
        'CPLogin.controller.CPLoginController',
        'CPRegion.controller.CPRegionController',
        'ChiefAppraiser.sub.DefaultPage.controller.DefaultPageController'
    ],
    views: [
        'ChiefAppraiser.config.util.BaseAppriaserDashboardAppraisalsPanel',
        'ChiefAppraiser.sub.DashboardShowOverallSummary.view.DashboardShowOverallSummaryView',
        'ChiefAppraiser.sub.DashboardShowOverallSummary.view.AppraisalByYear.EntityValuesByAppraisalYear',
        'ChiefAppraiser.sub.DashboardShowOverallSummary.view.AppraisalByEntity.AppraisalYearValuesByEntity',
        'ChiefAppraiser.sub.DashboardShowOverallSummary.view.DashboardHeader.AppraisalDashboardHeader',
        'ChiefAppraiser.sub.DashboardShowOverallSummary.view.AppraisalProgress.AppraisalProgress',
        'ChiefAppraiser.sub.DashboardShowOverallSummary.view.Variance.Variance'
    ],
    stores:[
        'ChiefAppraiser.sub.DashboardShowOverallSummary.store.AppraisalProgressDetailedStore',
        'ChiefAppraiser.sub.DashboardShowOverallSummary.store.AppraisalProgressStore',
        'ChiefAppraiser.sub.DashboardShowOverallSummary.store.TenYrAppraisalStore',
        'ChiefAppraiser.sub.DashboardShowOverallSummary.store.FiveYrVarianceStore',
        'ChiefAppraiser.sub.DashboardShowOverallSummary.store.NumberOfJEsStore'
    ],
    requires: [
        'Ext.form.field.Radio',
        'Ext.ux.grid.FilterRow',
        'ChiefAppraiser.config.Runtime', 
        'ChiefAppraiser.config.util.MainGridActionColCmpt',
        'ChiefAppraiser.config.util.MainGridToolCmpt',
        'ChiefAppraiser.config.util.PopUpWindowCmpt',
        'ChiefAppraiser.config.view.Help.SendHelp',
        'ChiefAppraiser.config.view.Help.ErrorWindow',

        // 'ChiefAppraiser.sub.DashboardShowOverallSummary.controller.DashboardShowOverallSummaryController',
        'MineralPro.sub.CommonFunctionsSearch.controller.CommonFunctionsSearchController',
    ],
    refs: [{
            ref: 'UserLoginWindowView',
            selector: 'userLoginWindowView'
    }],
    launch: function () {
        var me = this;
        
        MineralPro.config.Runtime.appName = 'Chief Appraiser'   
        MineralPro.config.RuntimeUtility.disableBackspace();
        MineralPro.config.RuntimeUtility.enablePasswordChecking();
        MineralPro.config.RuntimeUtility.ImplementVtypes();
//        MineralPro.config.RuntimeUtility.addThemeOptions();
        
        
        Ext.globalEvents.fireEvent('checkUserSession');
        if(MineralPro.config.Runtime.appraisalYear.length == 0){
            MineralPro.config.Runtime.appraisalYear = new Date().getFullYear();
        }
        Ext.on('resize', function () {
            var windowLogin = me.getUserLoginWindowView();
            if (windowLogin) {
                windowLogin.center();
            }
        });    
    },
	// mainView: 'DashboardShowOverallSummary'
});
