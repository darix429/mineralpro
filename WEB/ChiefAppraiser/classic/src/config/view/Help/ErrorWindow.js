Ext.define('ChiefAppraiser.config.view.Help.ErrorWindow', {
    extend: 'Ext.window.Window',
    alias: 'widget.errorwindow',

    requires: [
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],
    viewModel: {
        
    },
    title: 'Oh snap!',
    layout: 'fit',
    modal: true,
    width: 500,
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            layout: {
                type: 'hbox',
                pack: 'center'
            },
            items: [
                {
                    xtype: 'button',
                    tooltip: 'Resend request',
                    text: 'Yes',
                    listeners: {
                        click: function(btn){
                            var win = btn.up('window');
                            win.store.reload();
                            win.close();
                        }
                    }
                },
                {
                    xtype: 'button',
                    text: 'No',
                    tooltip: "Don't resend request",
                    listeners: {
                        click: function(btn){
                            var win = btn.up('window')
                            win.close();
                        }
                    }
                },
                {
                    xtype: 'button',
                    text: 'Report Error',
                    tooltip: "Report Error to Adminstrator",
                    listeners: {
                        click: function(btn){
                            var win = btn.up('window');
                            var error = win.error;
                            var sendhelp = Ext.ComponentQuery.query('sendhelp')[0];
                            if(!sendhelp){
                                Ext.create('widget.sendhelp',{
                                    error: error
                                }).show();
                            }
                            else{
                                sendhelp.error = error;
                                sendhelp.down('panel#errorReport label').setText(error);
                            }
                        }
                    }
                },
            ]
        }
    ],
    initComponent: function(){
        var me = this;
        var storeId = me.store.getId();
        var label = {
            xtype: 'component',
            itemId: 'errorMsg',
            html:   '<div class="error-win-cont">'+
                    '   <p>Something went wrong while processing your request.</p>'+
                    '   <p>Would you like to try again?</p>'+
                    '   <input id="toggle" type="checkbox">'+
                    '   <label for="toggle">Details</label>'+
                    '   <div id="expand">'+
                    '       <section>'+
                    '           <p class="store-id-cont">'+storeId+'</p>'+
                    '       </section>'+
                    '   </div>'+
                    '</div>'
        };
        me.items = [label];
        me.callParent(arguments);
    },
});
                