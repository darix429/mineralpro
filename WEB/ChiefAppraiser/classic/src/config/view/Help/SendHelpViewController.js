Ext.define('ChiefAppraiser.config.view.Help.SendHelpViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.sendhelp',

    onSendClick: function(button, e, eOpts) {
        var win = button.up('window');
        var errWin = Ext.ComponentQuery.query('errorwindow')[0];
        var message = button.up('window').down('form htmleditor[name=message]').getValue();
        var error = win.error;
        win.mask('Sending...');
        Ext.Ajax.request({
            url:'/api/Packages/MineralPro/SendMailToDevs',
            params: {
                error: error,
                message: message,
                idAppUser: MineralPro.config.Runtime.idAppUser
            },
            success: function(response){
                win.unmask();
                var resp = JSON.parse(response.responseText);
                var msg = resp.msg;
                if(resp.success){
                    Ext.Msg.show({
                        title: 'Success',
                        msg: msg,
                        buttons: Ext.Msg.OK,
                        fn: function(btn){
                            if(btn==='ok'){
                                errWin.close();
                                win.close();
                            }
                        }
                    });
                }
                else{
                    var errDetails = msg ? '<p>Details: '+msg+'</p>': '';
                    Ext.Msg.show({
                        title: 'Oops!',
                        msg: 'Failed to send message.'+errDetails,
                        icon: Ext.Msg.ERROR,
                        buttons: Ext.Msg.OK,
                        fn: function(btn){
                            if(btn=='ok'){
                                errWin.close();
                                win.close();
                            }
                        }
                    });
                }
            },
            failure: function(response){
                win.unmask();
                Ext.Msg.show({
                    title: 'Oops!',
                    msg: 'Failed to send message',
                    icon: Ext.Msg.ERROR,
                    buttons: Ext.Msg.OK,
                    fn: function(btn){
                        if(btn=='ok'){
                            errWin.close();
                            win.close();
                        }
                    }
                });
            }
        });
    },

    onCancelClick: function(button, e, eOpts) {
        var win = button.up('window');
        var errWin = Ext.ComponentQuery.query('errorwindow')[0];
        errWin.close();
        win.close();
    },

    onShowErrorReportClick: function(button, e, eOpts) {
        button.up('fieldcontainer').nextSibling().expand();
    },

    onHideErrorPanel: function(button, e, eOpts){
        button.up('panel').collapse();
    }
});
