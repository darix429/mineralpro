Ext.define('ChiefAppraiser.config.view.Help.SendHelp', {
    extend: 'Ext.window.Window',
    alias: 'widget.sendhelp',

    requires: [
        'ChiefAppraiser.config.view.Help.SendHelpViewController',
        'Ext.form.Panel',
        'Ext.form.field.HtmlEditor',
        'Ext.toolbar.Toolbar',
        'Ext.button.Button'
    ],
    controller: 'sendhelp',
    viewModel: {
        
    },
    modal: true,
    height: 350,
    title: "Send message to Adminstrator",
    layout: 'fit',
    width: 500,
    dockedItems: [
        {
            xtype: 'toolbar',
            dock: 'bottom',
            layout: {
                type: 'hbox',
                pack: 'center'
            },
            items: [
                {
                    xtype: 'button',
                    text: 'Send',
                    listeners: {
                        click: 'onSendClick'
                    }
                },
                {
                    xtype: 'button',
                    text: 'Cancel',
                    listeners: {
                        click: 'onCancelClick'
                    }
                }
            ]
        }
    ],
    initComponent: function(){
        var me = this;
        var error = me.error || '';
        var items = [
            {
                xtype: 'form',
                width: '100%',
                scrollable: 'y',
                bodyPadding: 10,
                items: [
                    {
                        xtype: 'component',
                        html:   '<p>Please tell us about this problem to help us diagnose the cause of this error and improve this software.</p>',
                    },
                    {
                        xtype: 'label',
                        text: 'Please describe what you were doing when this error occured (optional): '
                    },
                    {
                        xtype: 'htmleditor',
                        name: 'message',
                        resizable: true,
                        resizeHandles: 's',
                        labelAlign: 'top',
                        width: '100%'
                    },
                    {
                        xtype: 'label',
                        text: 'We have added an error report that you can send to us. To see what data this report contains,',
                    },
                    {
                        xtype: 'fieldcontainer',
                        width: '100%',
                        items: [{
                            xtype: 'button',
                            text: 'click here',
                            listeners: {
                                click: 'onShowErrorReportClick'
                            }
                        }]
                    },
                    {
                        xtype: 'panel',
                        itemId: 'errorReport',
                        scrollable: true,
                        collapsed: true,
                        collapsible: true,
                        header: false,
                        minHeight: 200,
                        bodyStyle: {
                            background: '#f1f1f1'
                        },
                        dockedItems: [
                            {
                                xtype: 'toolbar',
                                dock: 'bottom',
                                layout: {
                                    type: 'hbox',
                                    pack: 'end'
                                },
                                items: [
                                    {
                                        xtype: 'button',
                                        text: 'Hide',
                                        listeners: {
                                            click: 'onHideErrorPanel'
                                        }
                                    }
                                ]
                            }
                        ],
                        items: [
                            {
                                xtype: 'label',
                                padding: '15 0 15 0',
                                text: error
                            }
                        ]
                    }
                ]
            }
        ];
        me.items = items;
        me.callParent(arguments);

    }
});