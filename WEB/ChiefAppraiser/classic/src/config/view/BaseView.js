
Ext.define('ChiefAppraiser.config.view.BaseView', {
    extend: 'Ext.form.Panel',
    requires: 'ChiefAppraiser.config.view.BaseViewController',
    controller: 'baseViewController',

    initComponent: function() {       
        var me = this;

        me.listeners= {
            beforeclose: me.getController().onBeforeClose
        }
      
        me.callParent(arguments);        
    }
    
})