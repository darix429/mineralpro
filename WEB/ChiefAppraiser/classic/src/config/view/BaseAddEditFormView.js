
Ext.define('ChiefAppraiser.config.view.BaseAddEditFormView', {
    extend: 'Ext.window.Window',
    requires: ['ChiefAppraiser.config.view.BaseAddEditFormViewController'],
    controller: 'baseAddEditFormViewController',
    
    layout: 'fit',
    autoShow: true,
    modal: true,
    defaults:{ maskRe: /[^'\^]/ },
    listeners: {
        afterRender: function(thisForm, options){
            var me = this
            me.keyNav = Ext.create('Ext.util.KeyNav', me.el, {
                enter: function(){
                   me.down('#saveId').fireEvent('click', me.down('#saveId'))
                },            
                esc: function(){
                    me.close()
                    },

                N: {////for add and new 
                    alt: true,
                    fn: function () {
                        me.down('#addnew').fireEvent('click', me.down('#addnew'))
                        return false;
                    }
                },
                A: {///for add to grid
                    alt: true,
                    fn: function () {
                        me.down('#saveId').fireEvent('click', me.down('#saveId'))
                        return false;
                    }
                },
                C: {///for change to grid
                    alt: true,
                    fn: function () {
                        me.down('#saveId').fireEvent('click', me.down('#saveId'))
                        return false;
                    }
                },
               S: {//save on image upload
                    alt: true,
                    fn: function () {
                        me.down('#saveId').fireEvent('click', me.down('#saveId'))
                        return false;
                    }
                },
                scope: me
            });
        }
    }

    
})