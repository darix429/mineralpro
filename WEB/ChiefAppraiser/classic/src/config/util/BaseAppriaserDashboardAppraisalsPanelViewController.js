Ext.define('ChiefAppraiser.config.util.BaseAppriaserDashboardAppraisalsPanelViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.BaseAppriaserDashboardAppraisalsPanelViewController',
    onAppraisalYearValuesByEntityAfterRender: function(panel){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppriaserDashboardAppraisalsPanelViewController onAppraisalYearValuesByEntityAfterRender.')
    	var store = Ext.getStore('TenYrAppraisalStore');
        if(store.isLoading()){
            panel.mask('loading...');
        }
        else{
            panel.unmask();
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppriaserDashboardAppraisalsPanelViewController onAppraisalYearValuesByEntityAfterRender.') 
    },
    onFilterTagFieldChange: function(tagField, newValue, oldValue, eOpts){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppriaserDashboardAppraisalsPanelViewController onFilterTagFieldChange.')
        var panel = tagField.up('panel');
        var grids = panel.query('grid');
        if(!newValue.length){
            grids.forEach(function(grid){
                if(grid.isHidden()){
                    grid.show();
                }
            })
        }
        else{
            grids.forEach(function(grid){
                if(newValue.indexOf(grid.getItemId().replace('appraisalGrid','')) == -1){
                    grid.hide();
                }
            })
            newValue.forEach(function(item,i){
                var grid = panel.down('#appraisalGrid'+item);
                if(grid && grid.isHidden()){
                    grid.show();
                }
            });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppriaserDashboardAppraisalsPanelViewController onFilterTagFieldChange.')
    },
    onReloadTenYrStore: function(btn){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAppriaserDashboardAppraisalsPanelViewController onReloadTenYrStore.')
        var me = this;
        var view = me.getView();
        var store = Ext.getStore('TenYrAppraisalStore');
        store.forceReload = true;
        store.reload();
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAppriaserDashboardAppraisalsPanelViewController onReloadTenYrStore.')
    }
});
