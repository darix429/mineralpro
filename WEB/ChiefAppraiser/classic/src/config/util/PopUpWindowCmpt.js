Ext.define('ChiefAppraiser.config.util.PopUpWindowCmpt', {
    singleton: true, //default configuration for global variable

    addAndNewButton:{                          
        xtype: 'button',
        itemId:'addnew',
        text: 'Add and New',
        listeners: {
            click: 'onAddAndNew'
        }
    },

    addToGridButton:{                          
        xtype: 'button',
        text: 'Add to Grid',
        itemId: 'saveId',
        listeners: {
            click: 'onAddListToGrid'
        }
    },
    
    changeInGridButton:{                          
        xtype: 'button',
        text: 'Change in Grid',
        itemId: 'saveId',
        listeners: {
            click: 'onEditListToGrid'
        }
    },
    
    cancelButton: {
        xtype: 'button',
        text: 'Cancel',
        listeners: {
            click: function(){
                this.up('window').close();
            }
        } 
    },

})
    
    
    