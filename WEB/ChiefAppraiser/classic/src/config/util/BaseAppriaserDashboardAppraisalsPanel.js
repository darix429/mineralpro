Ext.define('ChiefAppraiser.config.util.BaseAppriaserDashboardAppraisalsPanel', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.BaseAppriaserDashboardAppraisalsPanel',
    requires: ['ChiefAppraiser.config.util.BaseAppriaserDashboardAppraisalsPanelViewController'],

    stores: ['TenYrAppraisalStore'],
    initComponent: function(){
        var me = this;
        var store = Ext.getStore('TenYrAppraisalStore');
        store.on({
            beforeload: function(store, operation){
                if(me.rendered && !me.isMasked(true)){
                    me.mask('loading...');
                }
            },
            load: function(){
                if(me.rendered && me.isMasked(true)){
                    me.unmask();
                }
            }
        })
        me.callParent(arguments);
    },
    controller: 'BaseAppriaserDashboardAppraisalsPanelViewController',
    viewModel: {
    },
    listeners: {
        afterlayout: 'onAppraisalYearValuesByEntityAfterRender'
    },
    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        items: [{
            text: 'Reload',
            itemId: 'reloadBtn',
            listeners: {
                click: 'onReloadTenYrStore'
            }
        },{
            text: 'Expand all',
            enableToggle: true,
            toggleHandler: function (button, pressed, eOpts) {
                var text = pressed ? 'expanding...' : 'collapsing...';
                button.setText(text);
                button.disable();
                var grids = button.up('panel').query('grid');
                grids.forEach(function(grid){
                    var rowwidget = grid.getPlugin('rowwidget');
                    if(rowwidget){
                        rowwidget.expandAll(pressed);
                    }
                    var collapseBtn = grid.tools.filter(function(tool){ return tool.getItemId() == 'collapseBtn'; })[0]
                    var nIconCls = pressed ? 'fa fa-minus' : 'fa fa-plus';
                    if(collapseBtn){
                        collapseBtn.setIconCls(nIconCls);
                    }
                })
                if (pressed) {
                    button.setText('Collapse all');
                }
                else {
                    button.setText('Expand all');
                }
                button.enable();
            },
            pressed: false
        },{
            xtype: 'tagfield',
            itemId: 'filterTagField',
            fieldLabel: 'Filter',
            valueField: 'category',
            displayField  : 'category',
            labelAlign: 'right',
            queryMode: 'local',
            filterPickList: true,
            listeners: {
                change: 'onFilterTagFieldChange'
            }
        }]
    }]
});