Ext.define('ChiefAppraiser.config.util.MainGridActionColCmpt', {
    singleton: true, //default configuration for global variable

    editCls: {
        icon: ChiefAppraiser.config.Runtime.editCls,
        tooltip: ChiefAppraiser.config.Runtime.editTooltip,
        handler: function (grid, rowIndex, colIndex, item, e, record) {
            this.up('grid').getSelectionModel().select(rowIndex)
            if (record.get('rowDeleteFlag') != 'D') {
                this.up('grid').getController().onOpenEditListForm(grid, rowIndex, colIndex, item)
            } else {
                Ext.Msg.show({
                    iconCls: ChiefAppraiser.config.Runtime.editDeletedDataWarningCls,
                    title: ChiefAppraiser.config.Runtime.editDeletedDataWarningTitle,
                    msg: ChiefAppraiser.config.Runtime.editDeletedDataWarningMsg,
                    buttons: Ext.Msg.OK
                });
            }
        },         
    },
    deleteCls: {
        getClass: function (colConfig, cellData, record, rowIndex, colIndex, dataStore) {
            if (record.get('rowDeleteFlag') == 'D') {
                return(ChiefAppraiser.config.Runtime.undoDeleteCls);
            } else {
                return(ChiefAppraiser.config.Runtime.deleteCls);
            }
        },
        getTip: function (colConfig, cellData, record, rowIndex, colIndex, dataStore) {
            if (record.get('rowDeleteFlag') == 'D') {
                return(ChiefAppraiser.config.Runtime.undoDeleteTooltip);
            } else {
                return(ChiefAppraiser.config.Runtime.deleteTooltip);
            }
        },
        handler: function (gridView, rowIndex, colIndex, clickedItem, eventObj, record, row) {
            this.up('grid').getSelectionModel().select(rowIndex)
            if (record.get('rowDeleteFlag') == 'D') {
                record.set('rowDeleteFlag', record.previousValues.rowDeleteFlag);                                       
            } else {
                record.set('rowDeleteFlag', 'D');            
            }
        }
    },
})


    