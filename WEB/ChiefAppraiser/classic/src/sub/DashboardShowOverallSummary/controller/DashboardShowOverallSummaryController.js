
Ext.define('ChiefAppraiser.sub.DashboardShowOverallSummary.controller.DashboardShowOverallSummaryController', {
    extend: 'Ext.app.Controller',
  
    models: [
        'ChiefAppraiser.sub.DashboardShowOverallSummary.model.SampleModel'
    ],
    
    stores: [
    ],  
    
    views: [
        'ChiefAppraiser.sub.DashboardShowOverallSummary.view.DashboardShowOverallSummaryView',
    ],          
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden']
});


