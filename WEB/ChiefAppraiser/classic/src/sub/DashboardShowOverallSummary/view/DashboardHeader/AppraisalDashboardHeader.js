Ext.define('ChiefAppraiser.sub.DashboardShowOverallSummary.view.DashboardHeader.AppraisalDashboardHeader' ,{
    extend: 'MineralPro.config.view.BaseHeaderView',
    alias : 'widget.AppraisalDashboardHeader',
    requires: [
    	'ChiefAppraiser.sub.DashboardShowOverallSummary.view.DashboardHeader.AppraisalDashboardHeaderViewController',
    	'ChiefAppraiser.sub.DashboardShowOverallSummary.view.AppraisalProgress.AppraisalProgress',
    	'Ext.XTemplate',
    	'Ext.chart.PolarChart',
        'Ext.chart.series.Pie',
        'Ext.chart.series.sprite.PieSlice',
        'Ext.chart.interactions.Rotate'
    ],
    controller: 'AppraisalDashboardHeader',
    layout: {
    	type: 'hbox',
    	pack: 'left',
    	align: 'top'
    },
    width: '100%',
    requires: [
    	'Ext.chart.PolarChart',
        'Ext.chart.series.Pie',
        'Ext.chart.series.sprite.PieSlice',
        'Ext.chart.interactions.Rotate'
    ],
    initComponent: function(){
    	var selected_year = MineralPro.config.Runtime.appraisalYear,
    		progress_store = Ext.StoreManager.lookup('AppraisalProgressStore'),
    		completed = progress_store.getData().items.map(function(item){return item.getData();}).filter(function(item){ return item.selectedAppraisalYear==selected_year&&item.lastYearAppraisalWasDone==selected_year; }) || [],
    		incompleted = progress_store.getData().items.map(function(item){return item.getData();}).filter(function(item){ return item.selectedAppraisalYear==selected_year&&item.lastYearAppraisalWasDone!=selected_year; }) || [],
    		complete_num_of_appraisal = completed.map(function(item){ return item.numberOfAppraisals; }).reduce(function(accumulator,curr){return accumulator+curr;},0) || 0;
    		incomplete_num_of_appraisal = incompleted.map(function(item){ return item.numberOfAppraisals; }).reduce(function(accumulator,curr){return accumulator+curr;},0) || 0;
    		total_num_of_appraisal = complete_num_of_appraisal + incomplete_num_of_appraisal,
    		total_num_of_appraisal = total_num_of_appraisal > 0 ? total_num_of_appraisal : 1,
    		complete_percent = (complete_num_of_appraisal/total_num_of_appraisal)*100,
    		incomplete_percent = (incomplete_num_of_appraisal/total_num_of_appraisal)*100;
		
    	var items = [
        // {
            // xtype: 'dataview',
		    // // data: [
            ////  {"appraisalYear":2014, "totalAppraisedValue":4942119600, "numberOfRRCs":1563, "maxAppraisedValueByRRC":584015338, "avgAppraisedValueByRRC":3161944.722, "minAppraisedValueByRRC":0},
			// // 	{"appraisalYear":2015, "totalAppraisedValue":3233407691, "numberOfRRCs":1644, "maxAppraisedValueByRRC":360633777, "avgAppraisedValueByRRC":1966792.999, "minAppraisedValueByRRC":0},
			// // 	{"appraisalYear":2016, "totalAppraisedValue":1794223537, "numberOfRRCs":1680, "maxAppraisedValueByRRC":184165296, "avgAppraisedValueByRRC":1067990.201, "minAppraisedValueByRRC":0},
			// // 	{"appraisalYear":2017, "totalAppraisedValue":2077416171, "numberOfRRCs":1702, "maxAppraisedValueByRRC":145152857, "avgAppraisedValueByRRC":1220573.544, "minAppraisedValueByRRC":0},
			// // 	{"appraisalYear":2018, "totalAppraisedValue":2082381424, "numberOfRRCs":1703, "maxAppraisedValueByRRC":145152857, "avgAppraisedValueByRRC":1222772.416, "minAppraisedValueByRRC":0}
			// // ],
			// store: 'FiveYearAppraisalStore',
			// height: 95,
			// tpl: [
			// 	'<table class="header-tbl">',
			// 	'	<thead>',
			// 	'		<tr>',
			// 	'			<td>Appraisal Year</td>',
			// 	'			<td>Total Appraised Value</td>',
			// 	'			<td># of RRC\'s</td>',
			// 	'			<td>Maximum Appraised Value by RRC</td>',
			// 	'			<td>Average Appraised Value by RRC</td>',
			// 	'			<td>Minimum Apraised Value by RRC</td>',
			// 	'		</tr>',
			// 	'	</thead>',
			// 	'	<tbody>',
			// 	'		<tpl for=".">',
			// 	'			<tr class="fya-item">',
			// 	'				<td>{appraisalYear}</td>',
			// 	'				<td>{totalAppraisedValue:usMoney()}</td>',
			// 	'				<td>{numberOfRRCs:usMoney()}</td>',
			// 	'				<td>{maxAppraisedValueByRRC:usMoney()}</td>',
			// 	'				<td>{avgAppraisedValueByRRC:usMoney()}</td>',
			// 	'				<td>{minAppraisedValueByRRC:usMoney()}</td>',
			// 	'			</tr>',
			// 	'		</tpl>',
			// 	'	</tbody>',
			// 	'</table>',
			// ],
			// itemSelector: 'tr.fya-item',
			// listeners: {
			// 	itemclick: function(cmp, record, item, index, e, eOpts){
			// 		console.log('You clicked on ');
			// 		console.log(record);
			// 	}
			// }
   //  	},
        {
        	xtype:'AppraisalProgress',
        	withDetails: true,
        }
    ];
    	
    	this.items = items;
    	this.callParent();
    }
}
);