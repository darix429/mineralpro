Ext.define('ChiefAppraiser.sub.DashboardShowOverallSummary.view.DashboardShowOverallSummaryView' ,{
    extend: 'MineralPro.config.view.BaseTabPanelView',
    // extend: 'Ext.tab.Panel',
    alias : 'widget.DashboardShowOverallSummary',
    layout:'border',
    requires: [
        'ChiefAppraiser.sub.DashboardShowOverallSummary.view.DashboardShowOverallSummaryViewController',
        
        'ChiefAppraiser.sub.DashboardShowOverallSummary.view.DashboardHeader.AppraisalDashboardHeader',
        'ChiefAppraiser.sub.DashboardShowOverallSummary.view.Variance.Variance',
        'ChiefAppraiser.sub.DashboardShowOverallSummary.view.AppraisalByYear.EntityValuesByAppraisalYear',
        'ChiefAppraiser.sub.DashboardShowOverallSummary.view.AppraisalByEntity.AppraisalYearValuesByEntity',
        'ChiefAppraiser.sub.DashboardShowOverallSummary.view.AppraisalProgress.AppraisalProgress'
    ],
    controller: 'DashboardShowOverallSummary',
    selectedId: '0',//Need default value '0'
    // selectedIdIndex: '',
    selectedName: '0',//Need default value '0'
    // selectedNameIndex: '',
    id: 'DashboardShowOverallSummaryId',
    listeners: {
        beforeclose: 'onDashboardShowOverallSummaryTabBeforeClose',
        // beforetabchange: 'onDashboardShowOverallSummaryTabBeforeChange',
        // tabchange: 'onDashboardShowOverallSummaryTabChange',
        afterrender: 'onDashboardShowOverallSummaryAfterRender',
        activate: 'onActivate'
    },
    items: [
        {
            title: 'Appraisal Progress',
            xtype: 'AppraisalProgress'
        },
        {
            title: 'Appraised Value Variance',
            xtype: 'Variance'
        },
        {
            title: 'Appriasal Year Values by Entity',
            xtype: 'AppraisalYearValuesByEntity'
        },
        {
            title: 'Entity Values by Appriasal Years',
            xtype: 'EntityValuesByAppraisalYear'
        }
        ]
});    