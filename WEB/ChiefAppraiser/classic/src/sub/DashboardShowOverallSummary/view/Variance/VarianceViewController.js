Ext.define('ChiefAppraiser.sub.DashboardShowOverallSummary.view.Variance.VarianceViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.VarianceViewController',
    filterGrid: function(){
        var me = this;
        var view = me.getView();
        var grid = view.down('#varianceGrid');
        var store = grid.getStore();
        var filterCategories = Ext.ComponentQuery.query('#searchCateg')[0];
        filterCategories = filterCategories ? filterCategories.getValue() : [];
        var keyword = Ext.ComponentQuery.query('#searchKeyword')[0];
        keyword = keyword ? keyword.getValue() : '';
        store.clearFilter();
        
        var words = ['fifthYear','fourthYear','thirdYear','secondYear','firstYear','rrcNumber','variance'];
        var appraisalYear = store.getProxy().getExtraParams().appraisalYear;
        var filterFields = filterCategories.indexOf('all') > -1 ? words : [];
        if(filterCategories.indexOf('all') == -1){
            filterCategories.forEach(function(categ){
                if(isNaN(parseInt(categ))){
                    filterFields.push(categ);
                }
                else{
                    var indx = parseInt(appraisalYear) - parseInt(categ);
                    indx = indx ? indx-1 : 0;
                    filterFields.push(words[indx]);
                }
            });
        }
        if(keyword){
            store.filterBy(function(rec){
                keyword = keyword && isNaN(parseFloat(keyword)) ? isNaN(parseFloat(keyword.replace(",",""))) ? keyword : parseFloat(keyword.replace(",","")) : keyword;
                var result = filterFields.filter(function(field){
                    var recVal = rec.get(field) || ''; 
                    recVal = isNaN(recVal) ? parseFloat(recVal.replace(",","")) : recVal;
                    return  recVal == keyword;
                })
                return result.length;
            })
        }
        else{
            me.clearGridFilters();
        }
    },
    clearGridFilters: function(){
        var me = this;
        var view = me.getView();
        var grid = view.down('#varianceGrid');
        var store = grid.getStore();
        var keyword = Ext.ComponentQuery.query('#searchKeyword')[0];
        keyword.suspendEvent('change')
        keyword.setValue('');
        keyword.resumeEvent('change')
        store.clearFilter();
    },
    onVarianceGridFilterChange: function(store, filters, eOpts){
        MineralPro.config.RuntimeUtility.DebugLog('Entering VarianceViewController onVarianceGridFilterChange.') 
        var me = this;
        var view = me.getView();
        var clrBtn = view.down('#clearButtonItemId');
        if(filters.length){
            clrBtn.show();
        }
        else{
            clrBtn.hide();
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving VarianceViewController onVarianceGridFilterChange.') 
    },
    onSearchFieldKeyup: function(field, e){
        MineralPro.config.RuntimeUtility.DebugLog('Entering VarianceViewController onSearchFieldKeyup.') 
        var me = this;
        var search = new Ext.util.DelayedTask(function () {       
            me.filterGrid();
        });
        search.delay(900);
        MineralPro.config.RuntimeUtility.DebugLog('Leaving VarianceViewController onSearchFieldKeyup.') 
    },
    onSearchDataGrid: function(btn){
        MineralPro.config.RuntimeUtility.DebugLog('Entering VarianceViewController onSearchDataGrid.') 
        var me = this;
        me.filterGrid();
        MineralPro.config.RuntimeUtility.DebugLog('Leaving VarianceViewController onSearchDataGrid.') 
    },
    onClearSearch: function(btn){
        MineralPro.config.RuntimeUtility.DebugLog('Entering VarianceViewController onClearSearch.') 
        var me = this;
        me.clearGridFilters();
        MineralPro.config.RuntimeUtility.DebugLog('Leaving VarianceViewController onClearSearch.') 
    },
    onEntityValuesByAppraisalYearAfterRender: function(){
        MineralPro.config.RuntimeUtility.DebugLog('Entering VarianceViewController onEntityValuesByAppraisalYearAfterRender')
        MineralPro.config.RuntimeUtility.DebugLog('Leaving VarianceViewController onEntityValuesByAppraisalYearAfterRender')
    },
    onSearchCategChange: function(btn, nval, oval){
        MineralPro.config.RuntimeUtility.DebugLog('Entering VarianceViewController onSearchCategChange')
        var me = this;
        if(nval[nval.length-1] == 'all'){
            btn.setValue('all');
        }
        else if(nval.length > 1 && nval.indexOf('all') > -1){
            nval = nval.filter(function(item){ return item!='all'; });
            btn.setValue(nval);
        }
        me.filterGrid();
        MineralPro.config.RuntimeUtility.DebugLog('Leaving VarianceViewController onSearchCategChange')
    }
});