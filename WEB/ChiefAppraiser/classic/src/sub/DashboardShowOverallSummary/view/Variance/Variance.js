Ext.define('ChiefAppraiser.sub.DashboardShowOverallSummary.view.Variance.Variance' ,{
    extend: 'Ext.panel.Panel',
    alias : 'widget.Variance',
    requires: [
        'ChiefAppraiser.sub.DashboardShowOverallSummary.view.Variance.VarianceViewController',
        'Ext.grid.Panel',
        'Ext.toolbar.Toolbar',
    ],
    stores: ['FiveYrVarianceStore'],
    viewModel: {
    },
    controller: 'VarianceViewController',
    height: '100%',
    width: '100%',
    layout: 'fit',
    initComponent: function(){
        MineralPro.config.RuntimeUtility.DebugLog('Entering Variance initComponent');
        var me = this;
        MineralPro.config.Runtime.readOnly=false;
        var store = Ext.getStore('FiveYrVarianceStore');
        store.on({
            load: function(){
                MineralPro.config.RuntimeUtility.DebugLog('Entering Variance initComponent FiveYrVarianceStore load');
                var appraisalYear = store.getProxy().getExtraParams().appraisalYear;
                var grid = me.down('#varianceGrid');
                if(grid){
                    var cols = grid.getColumns();
                    var lastFiveCols = cols.slice(-5);
                    lastFiveCols.forEach(function(col, i){
                        col.setText(appraisalYear - i);
                    })
                }
                var searchCateg = Ext.ComponentQuery.query('#searchCateg');
                searchCateg = searchCateg.length ? searchCateg[0] : '';
                if(searchCateg){
                    var data = [{
                        name: 'All',
                        value: 'all'
                    },{
                        name: 'RRC Number',
                        value: 'rrcNumber'
                    },{
                        name: 'Variance',
                        value: 'variance'
                    }];
                    for(var yr = appraisalYear; yr > (appraisalYear-5); yr--){
                        data.push({
                            name: yr,
                            value: yr
                        })
                    }
                    searchCateg.getStore().loadRawData(data);
                    searchCateg.suspendEvent('change');
                    searchCateg.setValue('rrcNumber');
                    searchCateg.resumeEvent('change');
                }
                MineralPro.config.RuntimeUtility.DebugLog('Leaving Variance initComponent FiveYrVarianceStore load');
            }
        })
        this.callParent(arguments);
        MineralPro.config.RuntimeUtility.DebugLog('Leaving Variance initComponent');
    },
    viewConfig: {

    },
    listeners: {
        afterrender: 'onEntityValuesByAppraisalYearAfterRender'
    },
    items: [
    {
        xtype: 'gridpanel',
        itemId: 'varianceGrid',
        tools: [{
            xtype: 'textfield',
            labelAlign: 'right',
            itemId: 'searchKeyword',
            width: 300,
            emptyText: 'Search Keyword',
            enableKeyEvents: true,
            listeners: {
                keyup: 'onSearchFieldKeyup'
            }
        },{
            xtype: 'tagfield',
            itemId: 'searchCateg',
            listeners: {
                change: 'onSearchCategChange'
            },
            store: {data: []},
            queryMode: 'local',
            emptyText: 'Search Category',
            displayField: 'name',
            valueField: 'value',
            filterPickList: true,
            growMin: 30,
            growMax: 75,
            grow: true,
            minWidth: 200,
            maxWidth: 400
        },{
            xtype: 'button',
            width: 90,
            iconCls: 'search',
            tooltip: 'Search',
            text: 'Search',
            itemId: 'searchButtonItemId',
            listeners: {
                click: 'onSearchDataGrid'
            }
        },{
            xtype: 'button',
            width: 90,
            tooltip: 'Clear',
            text: 'Clear',
            itemId: 'clearButtonItemId',
            listeners: {
                click: 'onClearSearch'
            }
        }],
        viewConfig: {
            listeners: {
                refresh: function(dataview) {
                    Ext.each(dataview.panel.columns, function(column) {
                        if (column.autoSizeColumn === true){
                            column.autoSize();
                        }
                    })
                }
            }
        },
        listeners: {
            filterchange: 'onVarianceGridFilterChange',
        },
        width: '100%',
        height: '100%',
        store: 'FiveYrVarianceStore',
        columns: [{
            text: 'Current<br>Appraisal<br>Year',
            dataIndex: 'appraisalYear'
        },{
            text: 'RRC Number',
            dataIndex: 'rrcNumber'
        },{
            text: 'Variance<br>From Previous<br>Year',
            dataIndex: 'variance',
            renderer: function(value){
                return Ext.util.Format.percent(value);
            }
        },{
            text: 'Total Appraised Value',
            shrinkWrap: true,
            columns: [{
                text: MineralPro.config.Runtime.appraisalYear,
                dataIndex: 'fifthYear',
                align: 'end',
                renderer: function(value){
                    return Ext.util.Format.number(value, '$0,000');
                }
            },{
                text: '',
                dataIndex: 'fourthYear',
                align: 'end',
                renderer: function(value){
                    return Ext.util.Format.number(value, '$0,000');
                }
            },{
                text: '',
                dataIndex: 'thirdYear',
                align: 'end',
                renderer: function(value){
                    return Ext.util.Format.number(value, '$0,000');
                }
            },{
                text: '',
                dataIndex: 'secondYear',
                align: 'end',
                renderer: function(value){
                    return Ext.util.Format.number(value, '$0,000');
                }
            },{
                text: '',
                dataIndex: 'firstYear',
                align: 'end',
                renderer: function(value){
                    return Ext.util.Format.number(value, '$0,000');
                }
            }]
        }]
    }]

});
