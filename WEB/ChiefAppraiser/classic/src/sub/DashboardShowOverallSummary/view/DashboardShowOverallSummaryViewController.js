Ext.define('ChiefAppraiser.sub.DashboardShowOverallSummary.view.DashboardShowOverallSummaryViewController' ,{
    extend: 'MineralPro.config.view.BaseTabPanelViewController',
    alias : 'controller.DashboardShowOverallSummary',
    onTabChange: function(tabPanel, newCard, oldCard, eOpts){
        MineralPro.config.RuntimeUtility.DebugLog('Entering DashboardShowOverallSummaryViewController onDashboardShowOverallSummaryTabChange.') 
        newCard.stores.forEach(function(storeId){
            var store = Ext.getStore(storeId);
            if(store){
                MineralPro.config.RuntimeUtility.DebugLog('Will reload Store '+storeId);
                store.reload();
            }
        });
        MineralPro.config.RuntimeUtility.DebugLog('Leaving DashboardShowOverallSummaryViewController onDashboardShowOverallSummaryTabChange.') 
    },
    onDashboardShowOverallSummaryAfterRender: function(){
    
    },
    onActivate: function(panel){
        MineralPro.config.RuntimeUtility.DebugLog('Entering DashboardShowOverallSummaryViewController onActivate.') 
        Ext.getBody().unmask();
        MineralPro.config.RuntimeUtility.DebugLog('Leaving DashboardShowOverallSummaryViewController onActivate.') 
    }
});