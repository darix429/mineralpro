Ext.define('ChiefAppraiser.sub.DashboardShowOverallSummary.view.AppraisalByEntity.AppraisalYearValuesByEntity', {
    extend: 'ChiefAppraiser.config.util.BaseAppriaserDashboardAppraisalsPanel',
    alias: 'widget.AppraisalYearValuesByEntity',

    requires: [
    'ChiefAppraiser.sub.DashboardShowOverallSummary.view.AppraisalByEntity.AppraisalYearValuesByEntityViewController',
    'Ext.grid.Panel',
    'Ext.toolbar.Toolbar',
    'Ext.button.Segmented',
    'Ext.button.Button',
    'Ext.view.Table',
    'Ext.grid.column.Template',
    
    ],
    controller: 'AppraisalYearValuesByEntity',
    viewModel: {
    },
    height: '100%',
    width: '100%',
    layout: 'fit',
    items: [
    {
        xtype: 'panel',
        width: '100%',
        height: '100%',
        layout: 'anchor',
        flex: 1,
        scrollable: true,
        items: []
    }]

});