Ext.define('ChiefAppraiser.sub.DashboardShowOverallSummary.view.AppraisalByYear.EntityValuesByAppraisalYear', {
    extend: 'ChiefAppraiser.config.util.BaseAppriaserDashboardAppraisalsPanel',
    alias: 'widget.EntityValuesByAppraisalYear',

    requires: [
    'ChiefAppraiser.sub.DashboardShowOverallSummary.view.AppraisalByYear.EntityValuesByAppraisalYearViewController',
    'Ext.grid.Panel',
    'Ext.grid.plugin.RowWidget',
    'Ext.toolbar.Toolbar',
    'Ext.button.Segmented',
    'Ext.button.Button',
    'Ext.view.Table',
    'Ext.grid.column.Template'
    ],
    
    controller: 'EntityValuesByAppraisalYear',
    viewModel: {
    },
    height: '100%',
    width: '100%',
    layout: 'fit',
    bodyCls: 'appraisal-entity-body',
    viewConfig: {

    },
    items: [
    {
        xtype: 'panel',
        width: '100%',
        height: '100%',
        layout: 'anchor',
        flex: 1,
        scrollable: true,
        items: []
    }]

});
