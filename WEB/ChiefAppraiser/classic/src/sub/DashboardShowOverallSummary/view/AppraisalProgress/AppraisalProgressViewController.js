Ext.define('ChiefAppraiser.sub.DashboardShowOverallSummary.AppraisalProgress.AppraisalProgressViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.AppraisalProgress',
    onSeriesTooltipRender: function (tooltip, record, item) {
    	MineralPro.config.RuntimeUtility.DebugLog('Entering AppraisalProgressViewController onSeriesTooltipRender.')
    	
    	var total = record.store.getData().items.reduce(function(acc, cur){
    		return acc+cur.get('data')
    	},0);
    	var percentage = Ext.util.Format.percent(record.get('data') / total)
    	var html =''
    			+ '<div style="text-align: center">'
    			+ '		<div style="padding: 0 25px; font-weight: bold; font-size: 20px;">'+percentage+'</div>'
    			+ '		<div style="padding-top: 5px;">'+record.get('name')+'</div>'
    			+ '		<div style="font-size: 10px; letter-spacing: 2px;">'+Ext.util.Format.number(record.get('data'),'0,000')+'</div>';
    			+ '</div>'
        tooltip.setHtml(html);
        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppraisalProgressViewController onSeriesTooltipRender.') 
    },
    onProgressDetailsGridAfterRender: function(grid){
        var store = grid.getStore();
        store.on({
            load: function(store, records, successful, operation, eOpts){
                if(successful && grid){
                    grid.setHideHeaders(!records.length);
                }
            }
        })
    }
});