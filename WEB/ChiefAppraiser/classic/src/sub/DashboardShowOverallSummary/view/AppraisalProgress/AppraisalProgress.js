Ext.define('ChiefAppraiser.sub.DashboardShowOverallSummary.view.AppraisalProgress.AppraisalProgress', {
    extend: 'Ext.panel.Panel',
    alias: 'widget.AppraisalProgress',

    requires: [
    'ChiefAppraiser.sub.DashboardShowOverallSummary.AppraisalProgress.AppraisalProgressViewController',
    'Ext.chart.PolarChart',
    'Ext.XTemplate'
    ],
    stores: ['NumberOfJEsStore','AppraisalProgressDetailedStore'],
    controller: 'AppraisalProgress',
    viewModel: {

    },
    layout: 'fit',
    width: '100%',
    height:'100%',
    initComponent: function(){
        var me = this;
        var jeStore = Ext.getStore('NumberOfJEsStore');
        jeStore.on({
            load: function(store){
                var grid = me.down('#jeCountGrid');
                var data = store.getAt(0).getData();
                data = Ext.Object.merge(data,{
                    //'JE Entry Year': store.getProxy().getExtraParams().appraisalYear
                });
                var columns = [];
                Ext.Object.getKeys(data).forEach(function(key){
                    if(key != 'id'){
                        columns.push({
                            text: key,
                            dataIndex: key
                        });
                    }
                });
                columns.sort(function(a,b){
                    a = a.text;
                    b = b.text;
                    var desc = 'JE Entry Year';
                    return isNaN(parseInt(a)) && isNaN(parseInt(b)) ? b > a : a == desc ? -1 : b == desc ? 1 : parseInt(b) - parseInt(a);
                });
                var nstore = Ext.create('Ext.data.Store', {
                    data : [data]
                });    
                grid.reconfigure(nstore, columns);
            }
        });
        jeStore.load();
        me.callParent(arguments);
    },
    listeners: {},
    items: [{
        xtype:'panel',
        layout:'border',
        height:'100%',
        width:'100%',
        items: [{
            xtype: 'panel',
            region: 'center',
            height: '70%',
            layout: 'border',
            items: [{
                xtype:'container',
                region:'center',
                // scrollable: true,
                height: '100%',
                style: 'background: #fff',
                width:'70%',
                items: [{
                    xtype: 'polar',
                    itemId: 'progressPieChart',
                    theme: 'Muted',
                    width: '100%',
                    height: 450,
                    insetPadding: 50,
                    innerPadding: 20,
                    // interactions: ['rotate', 'itemhighlight'],
                    interactions: ['rotate'],
                    legend: {
                        type: 'sprite',
                        docked: 'left'
                    },
                    store: 'AppraisalProgressStore',
                    series: [
                        {
                            type: 'pie',
                            angleField: 'data',
                            // donut: 50,
                            highlight: true,
                            label: {
                                field: 'name',
                                display: 'outside',
                                renderer: function(text, sprite, config, rendererData, index){
                                    var store = rendererData.store;
                                    var label = rendererData.field;
                                    var indx = store.find('name',text)
                                    var rec = store.getAt(indx);
                                    var data = rec.get('data');
                                    var totData = store.getData().items.reduce(function(prev,cur){
                                        return prev+cur.get('data');
                                    },0);
                                    var percent = (data/totData)*100;
                                    percent = Math.round(percent)+'%';
                                    text = text.toUpperCase();
                                    return text+' ( '+(percent)+' )';
                                }
                            },
                            tooltip: {
                                trackMouse: true,
                                autoHide: false,
                                renderer: 'onSeriesTooltipRender'
                            }
                        }
                    ]
                }]
            },{
                xtype:'gridpanel',
                region:'east',
                // title: 'Details',
                // collapsible: true,
                // split: true,
                width: '30%',
                store: 'AppraisalProgressDetailedStore',
                dockedItems: [{
                    xtype: 'dataview',
                    width: '100%',
                    store: 'AppraisalProgressStore',
                    itemSelector: 'p.status',
                    tpl: [
                    '<tpl for=".">',
                        '<p class="status {name}"><label>{name}:</label><label> {data}</label><p>',
                    '</tpl>',
                    {
                        formatNumber: function(value){
                            return Ext.util.Format.number(value,'0,000');
                        }
                    }
                    ]
                }],
                listeners: {
                    afterrender: 'onProgressDetailsGridAfterRender'
                },
                columns: [{
                    text: 'Appraisal Year',
                    dataIndex: 'selectedAppraisalYear',
                    flex: 1
                },{
                    text: 'Previous Appraisal Year',
                    dataIndex: 'lastYearAppraisalWasDone',
                    flex: 1
                },{
                    text: '# of Appraisals',
                    dataIndex: 'numberOfAppraisals',
                    flex: 1,
                    renderer: function(value){
                        return Ext.util.Format.number(value,'0,000');
                    }
                }]
            }]
        },{
            xtype:'gridpanel',
            region:'south',
            itemId: 'jeCountGrid',
            title: 'Number of JEs for Appraisal Year',
            collapsible: true,
            split: true,
            height: '30%',
            store: {
                data: []
            },
            columns: []
        }]
    }]
});