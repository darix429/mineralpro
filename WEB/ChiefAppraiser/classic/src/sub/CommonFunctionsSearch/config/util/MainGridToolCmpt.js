Ext.define('ChiefAppraiser.sub.CommonFunctionsSearch.config.util.MainGridToolCmpt', {
    singleton: true, //default configuration for global variable

    searchTextField: {
        xtype: 'textfield',
        width: 300,
        labelAlign: 'right',
        typeAhead: true,
        displayField: 'displayField',
        valueField: 'displayField',
        itemId: 'searchTextFieldItemId',
        listeners: {
            beforerender: function () {
                var me = this
                var grid = me.up('grid')
                me.setFieldLabel(grid.gridSearchTextFieldLabel)
                me.emptyText = grid.gridSearchTextFieldEmptyText
            },
        },
    },
    // start of search function
    searchComboTypeOne: {
        xtype: 'combo',
        width: 300,
        labelAlign: 'right',
        typeAhead: true,
        displayField: 'displayField',
        valueField: 'displayField',
        itemId: 'searchComboTypeOneItemId',
        //static data
        store: [
            'Account Number',
            'GEO Account Number',
            'Owner',
            'Master Owner',
            'Prior Owner',
            //'Situs Address',
            //'Legal information',
            //'DBA',
            //'Alt Account Number'
        ],
        listeners: {
            beforerender: function () {
                var me = this
                var grid = me.up('grid')
                //me.setStore(grid.gridSearchTypeOneStore)
                me.setFieldLabel(grid.gridSearchComboOneLabel)
                me.emptyText = grid.gridSearchComboOneEmptyText
            },
        },
    },
    searchComboTypeTwo: {
        xtype: 'combo',
        width: 300,
        labelAlign: 'right',
        typeAhead: true,
        displayField: 'displayField',
        valueField: 'displayField',
        itemId: 'searchComboTypeTwoItemId',
        store: ['Contains', 'Start With', 'Match Exactly'], //static data
        listeners: {
            beforerender: function () {
                var me = this
                var grid = me.up('grid')
                //me.setStore(grid.gridSearchTypeTwoStore)
                me.setFieldLabel(grid.gridSearchComboTwoLabel)
                me.emptyText = grid.gridSearchComboTwoEmptyText
            },
        },
    },
    searchButton: {
        xtype: 'button',
        width: 90,
        iconCls: '../resources/icons/readOnly.png',
        tooltip: 'Search',
        text: 'Search',
        itemId: 'searchButtonItemId',
        listeners: {
            click: 'onSearchDataGrid'
        }
    },
    // end of search function
})


    