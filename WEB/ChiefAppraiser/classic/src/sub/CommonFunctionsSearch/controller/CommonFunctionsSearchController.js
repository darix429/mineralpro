///**
// * Handles the controller processing for Client List
// */

Ext.define('ChiefAppraiser.sub.CommonFunctionsSearch.controller.CommonFunctionsSearchController', {
    extend: 'Ext.app.Controller',
  
    models: ['ChiefAppraiser.sub.CommonFunctionsSearch.model.CommonFunctionsSearchModel',
    'ChiefAppraiser.sub.CommonFunctionsSearch.model.CommonFunctionsSearchOptionModel',
    'ChiefAppraiser.sub.CommonFunctionsSearch.model.CommonFunctionsSearchTypeModel',
    'ChiefAppraiser.sub.CommonFunctionsSearch.model.CommonFunctionsSearchDetailsModel'
    ],
    
    stores: ['ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchStore',  
//    'ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchOptionStore',  
//    'ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchTypeStore',
    'ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchDetailsStore',
    'ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchTileStore'
    ],  
    
    views: [
    'ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchView',
    'ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchColumnGridView',
    'ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchTileGridView',
    'ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchTileView',
    'ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchColumnView',
    'ChiefAppraiser.sub.CommonFunctionsSearch.view.AccountView'
    ],          
    
    requires: ['Ext.form.ComboBox',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden',
    'ChiefAppraiser.sub.CommonFunctionsSearch.config.util.MainGridToolCmpt',
    'ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime']
});


