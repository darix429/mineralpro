Ext.define('ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchTileView', {
    extend: 'ChiefAppraiser.config.view.BaseView',
    alias : 'widget.CommonFunctionsSearchTileView',
    layout:'border',
    
    //need as unique identifier for each subfunction
    id: 'CommonFunctionsSearchTileViewId',
    
    items: [{
        xtype:'CommonFunctionsSearchTileGridView',
        region:'center'
    },]
    
});    