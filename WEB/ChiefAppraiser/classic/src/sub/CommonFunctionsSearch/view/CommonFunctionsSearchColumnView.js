Ext.define('ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchColumnView', {
    extend: 'ChiefAppraiser.config.view.BaseView',
    alias : 'widget.CommonFunctionsSearchColumnView',
    layout:'border',
    
    //need as unique identifier for each subfunction
    id: 'CommonFunctionsSearchColumnViewId',
    
    items: [{
        xtype:'CommonFunctionsSearchColumnGridView',
        region:'center'
    },]
    
});    