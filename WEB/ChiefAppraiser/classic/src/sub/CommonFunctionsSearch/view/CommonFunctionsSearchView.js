Ext.define('ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchView' ,{
//    extend: 'ChiefAppraiser.config.view.BaseTabPanelView',
    extend: 'Ext.tab.Panel',
    alias : 'widget.CommonFunctionsSearch',
    layout:'border',
    requires: [
        'ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchViewController',
    ],
    
    controller: 'CommonFunctionsSearchViewController',
    id: 'CommonFunctionsSearchId',
    
    items: [{
        xtype:'CommonFunctionsSearchColumnView',
        title: 'Column View'
    },{
        title: 'Tile View',
        xtype: 'CommonFunctionsSearchTileView' 
    }],

    initComponent: function() {       
        var me = this;

        me.listeners= {
            tabchange: me.getController().onTabChange,
            beforetabchange: me.getController().onBeforeTabChange,
            beforeclose: me.getController().onBeforeClose
            }
      
        me.callParent(arguments);        
    }
});    

