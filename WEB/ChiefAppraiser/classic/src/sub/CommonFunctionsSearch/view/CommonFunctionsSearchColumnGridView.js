
Ext.define('ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchColumnGridView', {
    extend: 'ChiefAppraiser.config.view.BaseGridView',
    alias: 'widget.CommonFunctionsSearchColumnGridView',
    store: 'ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchStore',
    requires: ['ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchGridViewController',
    ],
//    xtype: 'paging-grid',

    controller: 'CommonFunctionsSearchGridViewController',
    //gridSearchTypeOneStore: 'ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchOptionStore',
    gridSearchComboOneLabel: 'Search Option',
    gridSearchComboOneEmptyText: 'Search Options...',
    //gridSearchTypeTwoStore: 'ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchTypeStore',
    gridSearchComboTwoLabel: 'Search Type',
    gridSearchComboTwoEmptyText: 'Search Type...',
    gridSearchTextFieldLabel: 'Search Field',
    gridSearchTextFieldEmptyText: 'Search Text here...',
    addFormAliasName: 'AccountView', //widget form to call for account view list

//    height: 500,
//    width: 750,
//    frame: true,
//    loadMask: true,
    
    initComponent: function () {
        var me = this;
        me.width = 760;
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')]

        Ext.apply(me, {
            store: 'ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchStore',
            viewConfig: {
                trackOver: false,
                stripeRows: false
            },
            // grid columns
            columns: [{
                    id: 'AccountNumber',
                    text: "Account Number",
                    dataIndex: 'accountNum',
                    flex: 1,
                    sortable: true,
                    filterElement: new Ext.form.TextField(),
                    renderer: function (value) {
                        return '<a href="javascript:void(0);" >' + value + '</a>';
                    },
                    listeners: {
                        click: function (cell, td, rowIndex, cellIndex, e, record, value, index) {
                            me.getController().onOpenAccountListView(record, rowIndex);
                        }
                    }
                }, {
                    text: "GEO Account Number",
                    dataIndex: 'altAccountNum',
                    flex: 1,
                    sortable: true,
                    filterElement: new Ext.form.TextField()
                }, {
                    text: "Owner Name ",
                    dataIndex: 'name',
                    flex: 1,
                    sortable: true,
                    filterElement: new Ext.form.TextField()
                }, {
                    text: "Master Owner Id",
                    dataIndex: 'masterOwnerId',
                    flex: 1,
                    sortable: true,
                    filterElement: new Ext.form.TextField()
                }, {
                    text: "Prior Owner Name",
                    dataIndex: 'displayName',
                    flex: 1,
                    sortable: true,
                    filterElement: new Ext.form.TextField()
                }, {
                    text: "Situs Address",
                    dataIndex: 'streetAddress',
                    flex: 1,
                    sortable: true,
                    filterElement: new Ext.form.TextField()
                }, {
                    text: "Legal Information",
                    dataIndex: '',
                    flex: 1,
                    sortable: true,
                    filterElement: new Ext.form.TextField()
                }, {
                    text: "DBA",
                    dataIndex: '',
                    flex: 1,
                    sortable: true,
                    filterElement: new Ext.form.TextField()
                }, {
                    text: "Alt Account Number",
                    dataIndex: '',
                    flex: 1,
                    sortable: true,
                    filterElement: new Ext.form.TextField()
                }, {
                    text: "Total Land Value",
                    dataIndex: 'landValue',
                    flex: 1,
                    sortable: true,
                    filterElement: new Ext.form.TextField()
                }, {
                    text: "Total Improvement Value",
                    dataIndex: 'imprValue',
                    flex: 1,
                    sortable: true,
                    filterElement: new Ext.form.TextField()
                }, {
                    text: "Total Market Value",
                    dataIndex: 'totalValue',
                    flex: 1,
                    sortable: true,
                    filterElement: new Ext.form.TextField()
                }, {
                    text: "Exemption List",
                    dataIndex: '',
                    flex: 1,
                    sortable: true,
                    filterElement: new Ext.form.TextField()
                }],
        });
            
        me.tools = [
                ChiefAppraiser.sub.CommonFunctionsSearch.config.util.MainGridToolCmpt.searchComboTypeOne,
                ChiefAppraiser.sub.CommonFunctionsSearch.config.util.MainGridToolCmpt.searchTextField,
                ChiefAppraiser.sub.CommonFunctionsSearch.config.util.MainGridToolCmpt.searchComboTypeTwo,
                ChiefAppraiser.sub.CommonFunctionsSearch.config.util.MainGridToolCmpt.searchButton,
                ChiefAppraiser.config.util.MainGridToolCmpt.clearFilterButton
        ],
                me.callParent();
    },
    listeners:{
        selectionchange: 'onLoadSelectionChange',
    }
});