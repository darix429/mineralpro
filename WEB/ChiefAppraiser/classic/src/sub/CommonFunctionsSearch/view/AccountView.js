      
Ext.define('ChiefAppraiser.sub.CommonFunctionsSearch.view.AccountView', {
    extend:'Ext.window.Window',
    store:  'ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchDetailsStore',
    alias: 'widget.AccountView',
    modal: true,
    width:880,
    height:360,

    initComponent: function() {
        var me = this;
        me.items = [{
            xtype: 'form',
            layout: 'column',
            //bodyPadding: 5,
			//layout: 'fit',
			autoScroll: true,
            layoutConfig:{align:'stretch'}, 
            autoheight: true,
            items:[{
                xtype: 'fieldset',
                margin:'0 0 0 20',
                autoheight: true, 
                defaults:{
                    width:385,
                    xtype: 'displayfield', // update existing config
                    margin:'0 0 -5 0',
                    
                },
                title: 'Owner Details',
                items: [{
                    fieldLabel: 'Owner Name',
                    name : 'name',
                    labelAlign: 'left',
					labelWidth: 120
                },{
                    fieldLabel: 'Prior Owner Name',
                    name : 'displayName',
                    labelAlign: 'left',
					labelWidth: 120
                },{
                    fieldLabel: 'AddressLine 1',
                    name : 'displayAddrLine1',
                    labelAlign: 'left',
					labelWidth: 120
                },{
                    fieldLabel: 'City',
                    name : 'displayCity',
                    labelAlign: 'left',
					labelWidth: 120
                },{
                    fieldLabel: 'State',
                    name : 'displayStateCd',
                    labelAlign: 'left',
					labelWidth: 120
                },{
                    fieldLabel: 'Zip Code',
                    name : 'displayZipcode',
                    labelAlign: 'left',
					labelWidth: 120
                },{
                    fieldLabel: 'Country',
                    name : 'displayCountryCd',
                    labelAlign: 'left',
                    allowBlank: false,
                    msgTarget: 'side',
					labelWidth: 120
                },{
                    fieldLabel: 'Phone Number',
                    name : 'displayPhoneNum',
                    labelAlign: 'left',
                    allowBlank: false,
                    msgTarget: 'side',
					labelWidth: 120
                },{
                    fieldLabel: 'Birth Date',
                    name : 'displayBirthDt',
                    labelAlign: 'left',
                    allowBlank: false,
                    msgTarget: 'side',
					labelWidth: 120
                },{
                    fieldLabel: 'Email Address',
                    name : 'displayEmailAddress',
                    labelAlign: 'left',
                    allowBlank: false,
                    msgTarget: 'side',
					labelWidth: 120
                }]
            },
            {
                xtype: 'fieldset',
                margin:'0 0 0 20',//top-
                defaults:{
                    width:385,
                    xtype: 'displayfield', // update existing config
                    margin:'0 0 -5 0',
                    
                },
                title: 'Account Details',
                items: [{
                    fieldLabel: 'Account Number',
                    name : 'accountNum',
                    labelAlign: 'left',
					labelWidth: 120
                },{
                    fieldLabel: 'Account Name',
                    name : 'accountName',
                    labelAlign: 'left',
					labelWidth: 120
                
                },{
                    fieldLabel: 'Street Address',
                    name : 'streetAddress',
                    labelAlign: 'left',
					labelWidth: 120
                
                },{
                    fieldLabel: 'Unit',
                    name : 'unit',
                    labelAlign: 'left',
					labelWidth: 120
                
                },{
                    fieldLabel: 'Building',
                    name : 'building',
                    labelAlign: 'left',
					labelWidth: 120
                
                },{
                    fieldLabel: 'City',
                    name : 'city',
                    labelAlign: 'left',
					labelWidth: 120
                
                }]
            },
			 {
                xtype: 'fieldset',
                margin:'0 0 0 20',//top-
                defaults:{
                    width:385,
                    xtype: 'displayfield', // update existing config
                    margin:'0 0 -5 0',
                    
                },
                title: 'Account Value',
                items: [{
                    fieldLabel: 'Total Land Value',
                    name : 'landValue',
                    labelAlign: 'left',
					labelWidth: 120
                },{
                    fieldLabel: 'Total Improvement Value',
                    name : 'imprValue',
                    labelAlign: 'left',
					labelWidth: 120
                
                },{
                    fieldLabel: 'Total Market Value',
                    name : 'totalValue',
                    labelAlign: 'left',
					labelWidth: 120
                
                }]
            }
            ]
            
        },
    ];

        this.callParent(arguments);
    },
	
   /*  tbar: [     '->',
		{text:'Previous Order'}, // <--- Buttons
		{text:'Next Order'},
		{text:'Back'},

	], */
});