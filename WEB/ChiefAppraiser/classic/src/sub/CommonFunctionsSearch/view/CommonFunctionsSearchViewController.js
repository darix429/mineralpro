//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchViewController', {
    extend : 'Ext.app.ViewController',
//    extend: 'ChiefAppraiser.config.view.BaseTabPanelViewController',
    alias: 'controller.CommonFunctionsSearchViewController',
    
     onTabChange: function(tabPanel, newCard, oldCard, eOpts){
        MineralPro.config.RuntimeUtility.DebugLog('Entering CommonFunctionsSearchTileGridViewController onTabChange.')       
        var activetab = tabPanel.getActiveTab();
        var task = new Ext.util.DelayedTask(function () { 
        if((ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchOption != ''
                && ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchOption != null)
            || (ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchField != ''
                && ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchField != null)
            || (ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchType != ''
                && ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchType != null)){
        
            activetab.down('#searchComboTypeOneItemId').setValue(ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchOption)
            activetab.down('#searchTextFieldItemId').setValue(ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchField)
            activetab.down('#searchComboTypeTwoItemId').setValue(ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchType)
            activetab.down('#searchButtonItemId').click();                         
        }    
                        
        Ext.getBody().unmask()    
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CommonFunctionsSearchTileGridViewController onTabChange.')
        
         });
        task.delay(700);
    },
    
    onBeforeTabChange: function(tabPanel, newCard, oldCard, eOpts){
        MineralPro.config.RuntimeUtility.DebugLog('Entering CommonFunctionsSearchTileGridViewController onBeforeTabChange.')                  
        Ext.getBody().mask('Loading...')
        var me = this;        
        var activetab = tabPanel.getActiveTab();
        
        ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchOption = activetab.down('#searchComboTypeOneItemId').value
        ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchField = activetab.down('#searchTextFieldItemId').value
        ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchType = activetab.down('#searchComboTypeTwoItemId').value
        
        var task = new Ext.util.DelayedTask(function () { 
        Ext.get(document.activeElement).blur();
                                           
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CommonFunctionsSearchTileGridViewController onBeforeTabChange.')
        });
        task.delay(400);
    },
       
    onBeforeClose: function(tabPanel, newCard, oldCard, eOpts){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseTabPanelViewController onBeforeClose.')
        var me=this,
            saveData = false,   
            saveStore = [],
            arrayGrid= me.getActiveTab().query('grid'); 
        
        for(var x=0; x<arrayGrid.length; x++){
            var store = arrayGrid[x].getStore();
            
            if(store.getNewRecords().length > 0 
                    || store.getUpdatedRecords().length > 0 
                    || store.getRemovedRecords().length > 0){
                    saveStore.push(store)
                    saveData = true
            } 
            
        }
        
        if(saveData){  
            Ext.Msg.show({
                title: BP4v5.config.Runtime.tabCloseConfirmationIcon,    
                msg: BP4v5.config.Runtime.tabCloseConfirmationMsg,
                iconCls: BP4v5.config.Runtime.tabCloseConfirmationIcon,
                buttons: Ext.Msg.YESNO,               
                scope: this,
                width: 250,
                
                fn: function(btn) { 
                    if (btn === 'yes'){
                        Ext.each(saveStore, function(store){       
                            store.getProxy().extraParams = {
                                createLength: store.getNewRecords().length,
                                updateLength: store.getUpdatedRecords().length, 
                            };    
                            store.sync();  
                        })
                        me.close()
                    }else{                         
                        me.close()
                    }                                        
                }
                
                
//                fn: function(btn) { 
//                    if (btn === 'yes'){
//                        Ext.each(saveStore, function(store){       
//                            store.getProxy().extraParams = {
//                                createLength: store.getNewRecords().length,
//                                updateLength: store.getUpdatedRecords().length, 
//                            };    
//                            store.sync({
//                                callback: function(records, operation, success) {
//                                    store.reload();
//                                }
//                            });  
//                            store.commitChanges();
//                        })
//                        me.close()
//                    }else{
//                        Ext.each(saveStore, function(store){
//                            store.rejectChanges()
//                        })                          
//                        me.close()
//                    }                                        
//                }
            });  
            return false;       
        }
        
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseTabPanelViewController onBeforeClose.')
    },
});