//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchGridViewController', {
    extend: 'ChiefAppraiser.config.view.BaseGridViewController',
    alias: 'controller.CommonFunctionsSearchGridViewController',
    onSearchDataGrid: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering CommonFunctionsSearchGridViewController onSearchDataGrid.')
        var me = this;
        var view = me.getView();
        var store = view.getStore();

        store.getProxy().extraParams = {
            SearchOption: view.down('#searchComboTypeOneItemId').value,
            SearchField: view.down('#searchTextFieldItemId').value,
            SearchType: view.down('#searchComboTypeTwoItemId').value
        }
        store.reload();
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CommonFunctionsSearchGridViewController onSearchDataGrid.')
    },
    onOpenAccountListView: function (record, rowIndex) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering CommonFunctionsSearchGridViewController onOpenOrderListView.')
        var me = this;
        var mainView = me.getView();
        Ext.getBody().mask('Loading...');

        // var record = Ext.create(mainView.mainListModel);
        /*   var addView = Ext.widget(mainView.addFormAliasName);
         addView.down('form').loadRecord(record);
         console.log(record); */

        var idAccount = record.get('idAccount');
        var store = Ext.getStore('ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchDetailsStore');


        store.load({
            params: {
                sec: idAccount
            },
            callback: function (records, operation, success) {
                var addView = Ext.widget(mainView.addFormAliasName);
                // console.log(addView);
                // console.log(addView.down('form'));
                addView.down('form').loadRecord(records[0]);
                addView.show();
                Ext.getBody().unmask();
            }
        });



        MineralPro.config.RuntimeUtility.DebugLog('Leaving CommonFunctionsSearchGridViewController onOpenOrderListView.')
    },
    /**
     * Clear value for all filter
     */
    onClearListFilter: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering CommonFunctionsSearchGridViewController onClearListFilter.')
        var me = this;
        var cols = me.getView().columns
        var view = me.getView();
        var store = view.getStore();
        store.getProxy().setExtraParams({});
        view.down().items.items[1].setValue('');
        view.down().items.items[2].setValue('');
        view.down().items.items[3].setValue('');
        Ext.each(cols, function (col) {
            if (col.filterElement)
                col.filterElement.setValue('');
        });
        store.reload();
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CommonFunctionsSearchGridViewController onClearListFilter.')
    },
    /* onShowColumnView: function(){
     MineralPro.config.RuntimeUtility.DebugLog('Entering CommonFunctionsSearchGridViewController onShowColumnView.')	
     var me = this;
     var view = me.getView();
     var grid = view.down('pivotgrid');
     var st = Ext.getStore('ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchStore');
     Ext.suspendLayouts();
     grid.setTitle('Account Column View');
     grid.reconfigure(st,[{
     text: 'First Name',
     dataIndex: 'subSystem'
     }, {
     text: 'Last Name',
     dataIndex: 'appFunction'
     }, {
     width: 130,
     text: 'Employee No.',
     dataIndex: 'appName'
     }, {
     flex: 1,
     text: 'Department',
     dataIndex: 'subSystem'
     }]);
     view.down('#showTileView').enable();
     view.down('#showColumnView').disable();
     Ext.resumeLayouts(true);
     
     MineralPro.config.RuntimeUtility.DebugLog('Leaving CommonFunctionsSearchGridViewController onShowColumnView.')	
     },
     
     onShowTileView: function(){
     MineralPro.config.RuntimeUtility.DebugLog('Entering CommonFunctionsSearchGridViewController onShowTileView.')	
     var me = this;
     var view = me.getView();
     var grid = view.down('pivotgrid');
     var st = Ext.getStore('ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchStore');
     Ext.suspendLayouts();
     grid.setTitle('Account Tile View');
     grid.reconfigure(st,[{
     flex: 1,
     text: 'City',
     dataIndex: 'subSystem'
     }, {
     text: 'Total Employees',
     dataIndex: 'appFunction',
     width: 140
     }, {
     text: 'Manager',
     dataIndex: 'appName',
     width: 120
     }]);
     view.down('#showTileView').disable();
     view.down('#showColumnView').enable();
     Ext.resumeLayouts(true);
     
     MineralPro.config.RuntimeUtility.DebugLog('Leaving CommonFunctionsSearchGridViewController onShowTileView.')	
     }, */


    onLoadSelectionChange: function (model, records) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering CommonFunctionsSearchGridViewController onLoadSelectionChange.')

        if (records.length > 0) {
            CPRegion.config.Runtime.headerAccountSearch = records[0].get('accountNum')
            Ext.globalEvents.fireEvent('searchHeader');
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving CommonFunctionsSearchGridViewController onLoadSelectionChange.')
    },
});