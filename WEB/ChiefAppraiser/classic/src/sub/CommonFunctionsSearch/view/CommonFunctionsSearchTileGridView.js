
Ext.define('ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchTileGridView', {
    extend: 'ChiefAppraiser.config.view.BaseGridView',
    alias: 'widget.CommonFunctionsSearchTileGridView',
    store: 'ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchTileStore',
    requires: [
        'ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchTileGridViewController',
    ],
    controller: 'CommonFunctionsSearchTileGridViewController',
//    gridSearchTypeOneStore: 'ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchOptionStore',
    gridSearchComboOneLabel: 'Search Option',
    gridSearchComboOneEmptyText: 'Search Options...',
//    gridSearchTypeTwoStore: 'ChiefAppraiser.sub.CommonFunctionsSearch.store.CommonFunctionsSearchTypeStore',
    gridSearchComboTwoLabel: 'Search Type',
    gridSearchComboTwoEmptyText: 'Search Type...',
    gridSearchTextFieldLabel: 'Search Field',
    gridSearchTextFieldEmptyText: 'Search Text here...',
    frame: true,
    layout: 'fit',
//    stateful: false, 
    enableLocking: true,
    
    initComponent: function () {
        var me = this;

        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')]

        me.columns = [{
                text: "Field Name",
                dataIndex: 'fieldName',
                width: 150,
                sortable: true,
                locked: true,
                filterElement: new Ext.form.TextField()
            }]

        me.tools = [
            ChiefAppraiser.sub.CommonFunctionsSearch.config.util.MainGridToolCmpt.searchComboTypeOne,
            ChiefAppraiser.sub.CommonFunctionsSearch.config.util.MainGridToolCmpt.searchTextField,
            ChiefAppraiser.sub.CommonFunctionsSearch.config.util.MainGridToolCmpt.searchComboTypeTwo,
            ChiefAppraiser.sub.CommonFunctionsSearch.config.util.MainGridToolCmpt.searchButton
        ],
                me.callParent(arguments);
    },
    listeners: {
        beforerender: 'onBeforeRender',
        headerclick: 'onHeaderClick'
    }
});