//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('ChiefAppraiser.sub.CommonFunctionsSearch.view.CommonFunctionsSearchTileGridViewController', {
    extend: 'ChiefAppraiser.config.view.BaseGridViewController',
    alias: 'controller.CommonFunctionsSearchTileGridViewController',
    
onBeforeRender: function (model, records) {
       MineralPro.config.RuntimeUtility.DebugLog('Entering CommonFunctionsSearchTileGridViewController onBeforeRender.')

        if((ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchOption == ''
                || ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchOption == null)
            && (ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchField == ''
                || ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchField == null)
            && (ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchType == ''
                || ChiefAppraiser.sub.CommonFunctionsSearch.config.Runtime.searchType == null)){

            var me = this;
            var view = me.getView();
            var store = view.getStore();

            store.load({
                callback: function (records, operation, success, a){
                    if(operation){
                        var response = operation._response.responseText;
                        if(response){
                             var columnDefs = Ext.decode(response);
                             var colString = columnDefs.columnField[0].columnField;
                             var colArray =  colString.split(',');
                             var colLength = view.columns.length;

                             Ext.each(colArray, function (col) {
                                 col = col.replace('[', '').replace(']', '');

                             var column = Ext.create('Ext.grid.column.Column', {
                                     text: col,
                                     dataIndex: col,
                                     width: 150,
                                     });      
                             view.items.items[1].headerCt.insert(colLength, column);
                             colLength++
                             })                                         
                         view.getView().refresh();
                         }
                    }
                }
            });
        }
       MineralPro.config.RuntimeUtility.DebugLog('Leaving CommonFunctionsSearchTileGridViewController onBeforeRender.')
   },

    onSearchDataGrid: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering CommonFunctionsSearchTileGridViewController onSearchDataGrid.')
        var me = this;
        var view = me.getView();
        var store = view.getStore();              
        var columns = view.getColumns();
        
        Ext.suspendLayouts();
        view.suspendEvents();
                               
        store.getProxy().extraParams = {
            SearchOption: view.down('#searchComboTypeOneItemId').value,
            SearchField: view.down('#searchTextFieldItemId').value,
            SearchType: view.down('#searchComboTypeTwoItemId').value
        }
        
        store.load({
           callback: function (records, operation, success, a){
               var response = operation._response.responseText;
               if(response){
                   
                    Ext.each(columns , function (col) {
                        if(col.dataIndex != 'fieldName'){
                            view.items.items[1].headerCt.remove(col)
                        }             
                    })   
                   
                    var columnDefs = Ext.decode(response);
                    var colString = columnDefs.columnField[0].columnField;
                    var colLength = view.columns.length;
                    if(colString != null){ 
                        var colArray =  colString.split(',');

                        Ext.each(colArray, function (col) {                                     
                            col = col.replace('[', '').replace(']', '');

                            var column = Ext.create('Ext.grid.column.Column', {
                                     text: col,
                                     dataIndex: col,
                                     width: 150,
                                      //locked: false
                                     });      
                            view.items.items[1].headerCt.insert(colLength, column);
                            colLength++
                         }) 
                    }else{
                        var column = Ext.create('Ext.grid.column.Column', {
                                     width: 150,
                        });     
                        view.items.items[1].headerCt.insert(colLength, column);
                    }
                
 
                

                 view.resumeEvents();   
                 Ext.resumeLayouts();
                 view.getView().refresh();
                }
            }
       });       
        MineralPro.config.RuntimeUtility.DebugLog('Leaving CommonFunctionsSearchTileGridViewController onSearchDataGrid.')
    },


//onBeforeRender: function (model, records) {
//       MineralPro.config.RuntimeUtility.DebugLog('Entering CommonFunctionsSearchTileGridViewController onBeforeRender.')
//       var me = this;
//       var view = me.getView();
//       var store = view.getStore();
//       
//       store.load({
//           callback: function (records, operation, success, a){  
//               var response = operation._response.responseText;
//               var columnDefs = Ext.decode(response);
//               var colString = columnDefs.columnField[0].columnField;
//               var colArray =  colString.split(',');
//                 
//               var column = [{
//                       text: "Field Name",
//                       dataIndex: 'fieldName',
//                       width: 150,
//                       sortable: true,
//                       locked: true,
//                       filterElement: new Ext.form.TextField()
//               }] ;
//               
//               Ext.each(colArray, function (col) {
//                   col = col.replace('[', '').replace(']', '');
//                   var item = {
//                       text: col,
//                       dataIndex: col,
//                       sortable: false,
//                       width: 150
//                   }                  
//                   column.push(item);
//               })    
//               
//           view.reconfigure(view.store, column);                                
//           view.getView().refresh();
//           }
//       });
//       MineralPro.config.RuntimeUtility.DebugLog('Leaving CommonFunctionsSearchTileGridViewController onBeforeRender.')
//   },
//
//onSearchDataGrid: function () {
//        MineralPro.config.RuntimeUtility.DebugLog('Entering CommonFunctionsSearchTileGridViewController onSearchDataGrid.')
//        var me = this;
//        var view = me.getView();
//        var store = view.getStore();              
//        
//        Ext.suspendLayouts();
//        view.suspendEvents();
//                               
//        var column = [{
//                    text: "Field Name",
//                    dataIndex: 'fieldName',
//                    width: 150,
//                    sortable: true,
//                    locked: true,
//                    filterElement: new Ext.form.TextField()
//            }] ; 
//                      
//        store.getProxy().extraParams = {
//            SearchOption: view.down('#searchComboTypeOneItemId').value,
//            SearchField: view.down('#searchTextFieldItemId').value,
//            SearchType: view.down('#searchComboTypeTwoItemId').value
//        }
//        store.load({
//            callback: function (records, operation, success, a){                
//                var response = operation._response.responseText;
//                var columnDefs = Ext.decode(response);
//                var colString = columnDefs.columnField[0].columnField;
//                if(colString){
//                    var colArray =  colString.split(',');
//
//                    Ext.each(colArray, function (col) {
//                        col = col.replace('[', '').replace(']', '');
//                        var item = {
//                            text: col,
//                            dataIndex: col,
//                            sortable: false,
//                            width: 150
//                        }
//                    
//                    column.push(item);   
//                    })
//                }
//            view.resumeEvents();   
//            Ext.resumeLayouts();
//            view.reconfigure(view.store, column);  
//            view.getView().refresh();
//            }
//        });
//        
//        MineralPro.config.RuntimeUtility.DebugLog('Leaving CommonFunctionsSearchTileGridViewController onSearchDataGrid.')
//    },

    onHeaderClick: function (ct , column , e , t , eOpts) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering CommonFunctionsSearchTileGridViewController onHeaderClick.')
        
        if(column.dataIndex != 'fieldName'){
            CPRegion.config.Runtime.headerAccountSearch = column.dataIndex
            Ext.globalEvents.fireEvent('searchHeader');
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving CommonFunctionsSearchTileGridViewController onHeaderClick.')
    },
});