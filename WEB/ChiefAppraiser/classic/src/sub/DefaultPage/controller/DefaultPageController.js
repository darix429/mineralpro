///**
// * Handles the controller processing for Client List
// */

Ext.define('ChiefAppraiser.sub.DefaultPage.controller.DefaultPageController', {
    extend: 'Ext.app.Controller',
  
    models: [
        'ChiefAppraiser.sub.DefaultPage.model.PieModel',
    ],
    
    stores: [
        'ChiefAppraiser.sub.DefaultPage.store.PieStore',
	'ChiefAppraiser.sub.DefaultPage.store.TaxUnitValueStore',
	'ChiefAppraiser.sub.DefaultPage.store.AccountStatusByYearStore',
	'ChiefAppraiser.sub.DefaultPage.store.TwoYearSalesStore',
    ],  
    
    views: [
        'ChiefAppraiser.sub.DefaultPage.view.DefaultPageView',
        'ChiefAppraiser.sub.DefaultPage.view.CountryPropertyView',
        'ChiefAppraiser.sub.DefaultPage.view.CityPropertyView',
        'ChiefAppraiser.sub.DefaultPage.view.AccountStatusView'
    ],          
    
    requires: [
        'Ext.chart.theme.Muted',
        'Ext.chart.series.Pie3D',
        'Ext.chart.series.Bar3D',      
        'Ext.chart.axis.Numeric',
        'Ext.chart.axis.Category',
        'Ext.chart.axis.Numeric3D',
        'Ext.chart.axis.Category3D',
        'Ext.chart.grid.VerticalGrid3D',
        'Ext.chart.series.Line',
        
        'Ext.chart.interactions.ItemHighlight',
        'Ext.chart.interactions.RotatePie3D',
        
        'Ext.draw.sprite.Text'
    ]
});


