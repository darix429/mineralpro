/**
 * This example shows how to create a 3D Pie chart.
 *
 * The example makes use of the 'rotate' interaction. To use it, click or tap and then
 * drag anywhere on the chart.
 */
Ext.define('ChiefAppraiser.sub.DefaultPage.view.AccountStatusView', {
    extend: 'Ext.chart.PolarChart',
    alias: 'widget.AccountStatusView',
    
    requires: ['ChiefAppraiser.sub.DefaultPage.view.AccountStatusViewController'],
    controller: 'AccountStatusViewController',
    
    reference: 'chart',
    innerPadding: 40,

    store: {
        type: 'account-status-by-year'
    },
    legend: {
        type: 'sprite',
        docked: 'bottom'
    },
    theme: 'Muted',
    interactions: ['itemhighlight', 'rotatePie3d'],
    sprites: [{
            type: 'text',
            text: 'Account Status: 2016 ',
            fontSize: 15,
            textAlign: 'center', 
            fontWeight: 'bold',
            width: 200,
            height: 30,
            x: 220, // the sprite x position
            y: 20  // the sprite y position
        }],
    series: [
        {
            type: 'pie3d',
            angleField: 'percent',
            donut: 30,
            distortion: 0.6,
            highlight: {
                margin: 80
            },
            label: {
                display: 'rotate',
                font: 'bold 10px Arial',
                field: 'acStatus'
            },
            tooltip: {
                trackMouse: true,
                renderer: 'onSeriesTooltipRender'
            }
        }
    ],
//    plugins: {
//        ptype: 'chartitemevents',
//        moveEvents: true
//    },
//    listeners: {
//        itemclick: function () {
//            alert('click')
//        },
//        spriteclick: function () {
//            alert('sprite click')
//        }
//    }

});
