/**
 * This example shows how to create a 3D Pie chart.
 *
 * The example makes use of the 'rotate' interaction. To use it, click or tap and then
 * drag anywhere on the chart.
 */
Ext.define('ChiefAppraiser.sub.DefaultPage.view.CityPropertyView', {
    extend: 'Ext.chart.CartesianChart',
    alias: 'widget.CityPropertyView',

    requires: ['ChiefAppraiser.sub.DefaultPage.view.CityPropertyViewController'],
    controller: 'CityPropertyViewController',

    reference: 'chart',
    width: '30%',
    height: '100%',
    store: {
        type: 'tax-unit-value'
    },
    legend: {
        type: 'sprite',
        docked: 'bottom'
    },
    sprites: [{
            type: 'text',
            text: 'City Property Value 2016',
            fontSize: 15,
            fontWeight: 'bold',
            width: 100,
            height: 30,
            x: 120, // the sprite x position
            y: 20  // the sprite y position
        }],
    axes: [{
            type: 'numeric',
            position: 'left',
            fields: ['mktValue', 'apprValue', 'txblValue'],
            title: 'Total Value',
            grid: true,
            renderer: 'onAxisLabelRender'
        }, {
            type: 'category',
            position: 'bottom',
            fields: 'city',
            label: {
                rotate: {
                    degrees: -75
                }
            }
        }],
    series: [{
            type: 'line',
            title: 'Market Value',
            xField: 'city',
            yField: 'mktValue',
            marker: {
                type: 'square',
                fx: {
                    duration: 200,
                    easing: 'backOut'
                }
            },
            highlightCfg: {
                scaling: 2
            },
            tooltip: {
                trackMouse: true,
                renderer: 'onSeriesTooltipRender2'
            }
        }, {
            type: 'line',
            title: 'Appraised Value',
            xField: 'city',
            yField: 'apprValue',
            marker: {
                type: 'arrow',
                fx: {
                    duration: 200,
                    easing: 'backOut'
                }
            },
            highlightCfg: {
                scaling: 2
            },
            tooltip: {
                trackMouse: true,
                renderer: 'onSeriesTooltipRender2'
            }
        }, {
            type: 'line',
            title: 'Taxable Value',
            xField: 'city',
            yField: 'txblValue',
            marker: {
                type: 'arrow',
                fx: {
                    duration: 200,
                    easing: 'backOut'
                }
            },
            highlightCfg: {
                scaling: 2
            },
            tooltip: {
                trackMouse: true,
                renderer: 'onSeriesTooltipRender2'
            }
        }],
//    plugins: {
//        ptype: 'chartitemevents',
//        moveEvents: true
//    },
//    listeners: {
//        itemclick: 'onFloatItem',
//        spriteclick: function () {
//            alert('sprite click')
//        }
//    }

});
