Ext.define('ChiefAppraiser.sub.DefaultPage.view.CityPropertyViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.CityPropertyViewController',

    onFloatItem: function (){
      var me = this;
      var view = me.getView();
   
    },
    onAxisLabelRender: function (axis, label, layoutContext) {
        var value = layoutContext.renderer(label) / 1000000;
        return value === 0 ? '$0' : Ext.util.Format.number(value, '$0M');
    },

    onSeriesTooltipRender2: function (tooltip, record, item) {
        var title = item.series.getTitle();
        
        tooltip.setHtml(title + ' on ' + record.get('city') + ': ' +
            record.get(item.series.getYField()) + '%');
    },  

});
