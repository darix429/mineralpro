Ext.define('ChiefAppraiser.sub.DefaultPage.view.AccountStatusViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.AccountStatusViewController',

    onSeriesTooltipRender: function (tooltip, record, item) {
        tooltip.setHtml(record.get('acStatus') + ': ' + record.get('percent') + '%');
    },

});
