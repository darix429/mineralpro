Ext.define('ChiefAppraiser.sub.DefaultPage.view.CountryPropertyViewController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.CountryPropertyViewController',
    
    onAxisLabelRender: function (axis, label, layoutContext) {
        var value = layoutContext.renderer(label) / 1000000;
        return value === 0 ? '$0' : Ext.util.Format.number(value, '$0M');
    },
    
    onSeriesLabelRender: function (value) {
        return Ext.util.Format.number(value / 1000000, '$0M');
    },

});
