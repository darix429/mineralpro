/**
 * This example shows how to create a 3D Pie chart.
 *
 * The example makes use of the 'rotate' interaction. To use it, click or tap and then
 * drag anywhere on the chart.
 */
Ext.define('ChiefAppraiser.sub.DefaultPage.view.CountryPropertyView', {
    extend: 'Ext.chart.CartesianChart',
    alias: 'widget.CountryPropertyView',

    requires: ['ChiefAppraiser.sub.DefaultPage.view.CountryPropertyViewController'],
    controller: 'CountryPropertyViewController',

    width: '30%',
    height: '50%',
    theme: 'Muted',
    interactions: ['itemhighlight'],
    animation: {
        duration: 200
    },
    
    store: {
        type: 'two-year-sales'
    },
    legend: true,
    sprites: [{
            type: 'text',
            text: 'County Property Value 2016',
            fontSize: 15,
            fontWeight: 'bold',
            width: 100,
            height: 30,
            x: 130, // the sprite x position
            y: 20  // the sprite y position
        }],
    axes: [{
            type: 'numeric3d',
            position: 'left',
            fields: ['2015', '2016'],
            minimum: 8000000000,
            grid: false,
            title: 'Total Value',
            renderer: 'onAxisLabelRender'
        }, {
            type: 'category3d',
            position: 'bottom',
            fields: 'vType',
            label: {
                rotate: {
                    degrees: -45
                }
            },
            grid: true
        }],
    series: {
        type: 'bar3d',
        stacked: false,
        title: ['2015', '2016'],
        xField: 'vType',
        yField: ['2015', '2016'],
        label: {
            field: ['2015', '2016'],
            display: 'insideEnd',
            renderer: 'onSeriesLabelRender'
        },
        highlight: true,
        style: {
            inGroupGapWidth: -7
        }
    },
    
    
    
//    plugins: {
//        ptype: 'chartitemevents',
//        moveEvents: true
//    },
//    listeners: {
//        itemclick: function () {
//            alert('click')
//        },
//        spriteclick: function () {
//            alert('sprite click')
//        }
//    }
});
