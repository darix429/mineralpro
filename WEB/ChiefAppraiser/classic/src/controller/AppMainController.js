///**
// *Responsible for constructing the viewport and 
// *initializing the user information to the global variable
// */

Ext.define('ChiefAppraiser.controller.AppMainController', {
    extend: 'Ext.app.Controller',

    init: function(){              
        var me=this;
        me.listen({
            global: {            
                loadViewPort: me.loadViewPort
            }            
        })               
    },
    /**
    * Load the viewport for user interaction with the system
    */
    loadViewPort: function(){
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppMainController loadViewPort.')     

        var north = ChiefAppraiser.app.getController('CPRegion.controller.region.NorthController');
        
        var west = ChiefAppraiser.app.getController('CPRegion.controller.region.WestController')  

        var center = ChiefAppraiser.app.getController('CPRegion.controller.region.CenterController') 
        
        Ext.create('CPRegion.view.CPRegionViewPort').show();

        var centerView = center.getCenterView();
        var northView = north.getNorthView();
        var northCont = northView.down('#headerInfoItemId');
        var fn = function(){
            if(MineralPro.config.Runtime.appName=='Chief Appraiser'){
                if(northCont.items.items.length){
                    northView.expand();
                }
                else{
                    northView.collapse();
                }
            }
        };
        fn();
        northCont.on({
            add: function( cmp, component, index, eOpts ){
                fn: fn
            }
        })
        
        
        /*------------------------------ START ------------------------------
        | Reload stores upon changing appraisal Year on the header
        */
        var stores = Ext.StoreManager.filterBy(function(item,key){
            return item.reloadOnAppraisalYrChange;
        });
            
        northView.down('#yearItemId').on({
            change: function(combo, newVal, oldVal, record){
                if(MineralPro.config.Runtime.appName=='Chief Appraiser'){
                    stores.each(function(store){
                        store.reload();
                    });
                    fn();
                    var buttonArr = centerView.query('button');
                    Ext.each(buttonArr, function(btn){
                        btn.show();
                    })
                }
            }
        });
        /*
        | Reload stores upon changing appraisal Year on the header
        | ------------------------------ END ------------------------------
        */

        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppMainController loadViewPort.')
    }
    
});
