Ext.define('InformationServices.sub.OrganizationOrganizationList.store.OrganizationListStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    showMask: true,
    model: 'InformationServices.sub.OrganizationOrganizationList.model.OrganizationListModel',

    proxy: {
        api: {
            create: '/api/InformationServices/OrganizationOrganizationList/CreateOrganizationList',
            read: '/api/InformationServices/OrganizationOrganizationList/ReadOrganizationList',
            update: '/api/InformationServices/OrganizationOrganizationList/UpdateOrganizationList'
        }      
    }
    
}); 
