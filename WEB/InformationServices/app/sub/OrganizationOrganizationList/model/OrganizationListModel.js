Ext.define('InformationServices.sub.OrganizationOrganizationList.model.OrganizationListModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'idOrganization', type: "int", defaultValue: 1},
        {name: 'organizationNumber', type: "string", defaultValue: ''},
        {name: 'organizationName', type: "string", defaultValue: ''},
        {name: 'statusCd', type: "int", defaultValue: 0},
        {name: 'enableDeleteCount', type: "int", defaultValue: 0},
        {name: 'code', type: "int", defaultValue: 0},
        {name: 'description', type: "string", defaultValue: ''},
    ]
});