Ext.define('InformationServices.sub.ReportsExports.model.ReportsExportsModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'reportId', type: "int", defaultValue: ''}
    ]
});