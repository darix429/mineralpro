Ext.define('InformationServices.sub.ReportsExports.store.ReportsExportsStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    showMask: true,
    model: 'InformationServices.sub.ReportsExportsStateReports.model.StateReportsModel',
    proxy: {
        api: {
            read: '/api/InformationServices/ReportsExports/ReadReports'
        }      
    }
}); 
    