Ext.define('InformationServices.sub.ReportsExportsPublicExports.store.PublicExportsStore', {
    extend: 'MineralPro.store.BaseStore',
    model: 'InformationServices.sub.ReportsExportsPublicExports.model.PublicExportsModel',
    autoLoad: true,
    showMask: true,
    proxy: {
        api: {
            read: '/api/InformationServices/ReportsExports/ReadExports',
            }      
    },
}); 