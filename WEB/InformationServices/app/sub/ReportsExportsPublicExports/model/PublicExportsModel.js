Ext.define('InformationServices.sub.ReportsExportsPublicExports.model.PublicExportsModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'displayName', type: "string", defaultValue: ''},
        {name: 'url', type: "string", defaultValue: ''}
    ]
});