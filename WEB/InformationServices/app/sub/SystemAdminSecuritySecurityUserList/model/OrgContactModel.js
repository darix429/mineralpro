Ext.define('InformationServices.sub.SystemAdminSecuritySecurityUserList.model.OrgContactModel', {
    extend: 'MineralPro.model.BaseModel',
    fields: [
        {name: 'idOrganizationContact',type:"int",defaultValue: 1},
        {name: 'organizationContact',type:"string",defaultValue: ''},
        {name: 'description',type:"string",defaultValue: ''},
    ]
});