Ext.define('InformationServices.sub.SystemAdminSecuritySecurityUserList.model.SecGroupModel', {
    extend: 'MineralPro.model.BaseModel',
    fields: 
    [{name: 'idSecurityGroup',type:"int",defaultValue:1},
    {name: 'groupName',type:"string",defaultValue:''},
    {name: 'description',type:"string",defaultValue:''}]
});