Ext.define('InformationServices.sub.SystemAdminSecuritySecurityUserList.model.OrgNameModel', {
    extend: 'MineralPro.model.BaseModel',
    fields: [
        {name: 'idOrganization',type:"int",defaultValue: 1},
        {name: 'organizationName',type:"string",defaultValue: ''},
    ]
});