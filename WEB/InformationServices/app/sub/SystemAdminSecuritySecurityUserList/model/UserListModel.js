Ext.define('InformationServices.sub.SystemAdminSecuritySecurityUserList.model.UserListModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:
            [{name: 'idAppUser', type: "int", defaultValue: 1},
                {name: 'idUserOrgSecurity', type: "int", defaultValue: 1},
                {name: 'idUserOrganization', type: "int", defaultValue: 1},
                {name: 'idOrganizationContact', type: "int", defaultValue: 1},
                {name: 'emailAddress', type: "string", defaultValue: ''},
//    {name:  'password',type:"string",defaultValue:''},
                {name: 'groupName', type: "string", defaultValue: ''},
                {name: 'organizationContact', type: "string", defaultValue: ''},
                {name: 'idOrganization', type: "int", defaultValue: 1},
                {name: 'idSecurityGroup', type: "int", defaultValue: 1},
                {name: 'organizationName', type: "string", defaultValue: ''},
                {name: 'newpassword', type: "string", defaultValue: ''},
                {name: 'statusCd', type: "int", defaultValue: 0},
                {name: 'description', type: "string", defaultValue: ''},
            ]
});