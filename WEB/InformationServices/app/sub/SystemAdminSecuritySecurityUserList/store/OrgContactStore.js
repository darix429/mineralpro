Ext.define('InformationServices.sub.SystemAdminSecuritySecurityUserList.store.OrgContactStore', {
    extend: 'MineralPro.store.BaseStore',
    //autoLoad: true,
    
    model: 'InformationServices.sub.SystemAdminSecuritySecurityUserList.model.OrgContactModel',
    
    proxy: {
        api: {
            read: '/api/InformationService/SystemAdminSecuritySecurityUserList/ReadOrgContact',
        }      
    }
}); 
