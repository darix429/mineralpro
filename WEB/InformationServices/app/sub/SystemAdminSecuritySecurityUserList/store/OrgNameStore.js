Ext.define('InformationServices.sub.SystemAdminSecuritySecurityUserList.store.OrgNameStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'InformationServices.sub.SystemAdminSecuritySecurityUserList.model.OrgNameModel',
    
    proxy: {
        api: {
            read: '/api/InformationService/SystemAdminSecuritySecurityUserList/ReadOrganizationName',
        }      
    }
    
}); 
