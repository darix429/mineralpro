Ext.define('InformationServices.sub.SystemAdminSecuritySecurityUserList.store.SecGroupStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'InformationServices.sub.SystemAdminSecuritySecurityUserList.model.SecGroupModel',
    
     proxy: {
        api: {
            read: '/api/InformationService/SystemAdminSecuritySecurityUserList/ReadSecurityGroup',
        }      
    }
     
}); 
