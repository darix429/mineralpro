Ext.define('InformationServices.sub.SystemAdminSecuritySecurityUserList.store.UserListStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,   
    showMask: true,
    model: 'InformationServices.sub.SystemAdminSecuritySecurityUserList.model.UserListModel',
   
   proxy: {
        api: {
            create: '/api/InformationService/SystemAdminSecuritySecurityUserList/CreateUserList',
            read: '/api/InformationService/SystemAdminSecuritySecurityUserList/ReadUserList',
            update: '/api/InformationService/SystemAdminSecuritySecurityUserList/UpdateUserList'
        }      
    }
   
}); 
