Ext.define('InformationServices.sub.SystemAdminCodeType.model.CodeTypeModel', {
    extend: 'MineralPro.model.BaseModel',
    fields: [

        {name: 'idSystemCode', type:"int", defaultValue: 1},
        {name: 'idSystemCodeType', type:"int", defaultValue: 1},
        {name: 'codeType',  defaultValue: ''},
        {name: 'appraisalYear', type:"int", defaultValue: 1},
        {name: 'code', type:"int", defaultValue: 1},
        {name: 'description', defaultValue: ''},
        {name: 'shortDescription', defaultValue: ''},
        {name: 'activeInd', defaultValue: ''},
        {name: 'staticInd', defaultValue: ''},
        {name: 'sctDesc', defaultValue: ''},
        {name: 'tableName', defaultValue: ''}, 
        {name: 'columnName', defaultValue: ''},   
    ]
});