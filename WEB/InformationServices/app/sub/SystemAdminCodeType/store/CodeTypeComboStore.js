Ext.define('InformationServices.sub.SystemAdminCodeType.store.CodeTypeComboStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
//    showMask: true,
    model: 'InformationServices.sub.SystemAdminCodeType.model.CodeTypeModel',
    proxy: {
        api: {
//            create: '/api/InformationServices/SystemAdminCodeType/CreateCodeType',
            read: '/api/InformationServices/SystemAdminCodeType/ReadCodeTypeCombo',
//            update: '/api/InformationServices/SystemAdminCodeType/UpdateCodeType'
        }
       
    }
}); 
