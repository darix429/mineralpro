Ext.define('InformationServices.sub.SystemAdminCodeType.store.CodeTypeStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    showMask: true,
    model: 'InformationServices.sub.SystemAdminCodeType.model.CodeTypeModel',
    proxy: {
        api: {
            create: '/api/InformationServices/SystemAdminCodeType/CreateCodeType',
            read: '/api/InformationServices/SystemAdminCodeType/ReadCodeType',
            update: '/api/InformationServices/SystemAdminCodeType/UpdateCodeType'
        }
       
    }
}); 
