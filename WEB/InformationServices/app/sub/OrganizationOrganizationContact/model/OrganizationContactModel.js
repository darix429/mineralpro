Ext.define('InformationServices.sub.OrganizationOrganizationContact.model.OrganizationContactModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'idOrganizationContact', type: "int", defaultValue: 1},
        {name: 'idOrganization', type: "int", defaultValue: 1},
        {name: 'organizationName', type: "string", defaultValue: ''},
        {name: 'contactNumber', type: "int", defaultValue: ''},
        {name: 'firstName', type: "string", defaultValue: ''},
        {name: 'lastName', type: "string", defaultValue: ''},
        {name: 'middleInitial', type: "string", defaultValue: ''},
        {name: 'nameAbbreviation', type: "string", defaultValue: ''},
        {name: 'title', type: "string", defaultValue: ''},
        {name: 'statusCd', type: "string", defaultValue: ''},
        {name: 'enableDeleteCount', type: "int", defaultValue: 0},
        {name: 'code', type: "int", defaultValue: 0},
        {name: 'description', type: "string", defaultValue: ''},
    ]
});