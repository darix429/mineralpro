Ext.define('InformationServices.sub.OrganizationOrganizationContact.store.OrganizationContactStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    showMask: true,
    model: 'InformationServices.sub.OrganizationOrganizationContact.model.OrganizationContactModel',
    proxy: {
        api: {
            create: '/api/InformationServices/OrganizationOrganizationContact/CreateOrganizationContact',
            read: '/api/InformationServices/OrganizationOrganizationContact/ReadOrganizationContact',
            update: '/api/InformationServices/OrganizationOrganizationContact/UpdateOrganizationContact'
        }      
    }
}); 
    