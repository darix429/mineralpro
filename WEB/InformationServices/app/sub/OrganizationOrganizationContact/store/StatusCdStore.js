Ext.define('InformationServices.sub.OrganizationOrganizationContact.store.StatusCdStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'InformationServices.sub.OrganizationOrganizationContact.model.OrganizationContactModel',
    proxy: {
        api: {
            read: '/api/InformationServices/OrganizationOrganizationContact/ReadStatusCodeType',
        }
       
    }
}); 
