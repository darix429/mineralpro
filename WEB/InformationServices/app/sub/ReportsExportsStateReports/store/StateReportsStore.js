Ext.define('InformationServices.sub.ReportsExportsStateReports.store.StateReportsStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    showMask: true,
    model: 'InformationServices.sub.ReportsExportsStateReports.model.StateReportsModel',
    proxy: {
        api: {
            create: '/api/InformationServices/ReportsExportsStateReports/CreateStateReports',
            read: '/api/InformationServices/ReportsExports/ReadReports/STATE',
            update: '/api/InformationServices/ReportsExportsStateReports/UpdateStateReports'
        }      
    }
}); 
    