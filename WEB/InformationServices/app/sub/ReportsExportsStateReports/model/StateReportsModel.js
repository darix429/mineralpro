Ext.define('InformationServices.sub.ReportsExportsStateReports.model.StateReportsModel', {
    extend: 'MineralPro.model.BaseModel',
    fields:[
        {name: 'reportId', type: "int", defaultValue: ''},
        {name: 'appraisalYear', type: "int", defaultValue: 2017},
        {name: 'reportCategory', type: "string", defaultValue: ''},
        {name: 'reportType', type: "string", defaultValue: ''},
        {name: 'reportDesc', type: "string", defaultValue: ''},
        {name: 'displayOrder', type: "int", defaultValue: ''},
        {name: 'fileName', type: "string", defaultValue: ''},
        
    ]
});