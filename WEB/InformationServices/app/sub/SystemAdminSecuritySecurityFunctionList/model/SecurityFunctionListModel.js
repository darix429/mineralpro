Ext.define('InformationServices.sub.SystemAdminSecuritySecurityFunctionList.model.SecurityFunctionListModel', {
    extend: 'MineralPro.model.BaseModel',
    fields: [

        {name: 'idApp', type:"int", defaultValue: 1},
        {name: 'appName', defaultValue: ''},
        
        {name: 'idSubSystem', type:"int", defaultValue: 1},
        {name: 'subSystemName', defaultValue: ''},
        {name: 'subSystemNameDisplay', defaultValue: ''},
        {name: 'subSystemNameOrder', type:"int", defaultValue: 1},
        
        {name: 'idSubSystemFunction', type:"int", defaultValue: 1},             
        {name: 'functionName', defaultValue: ''},
        {name: 'functionNameDisplay', defaultValue: ''},
        {name: 'functionNameOrder', type:"int", defaultValue: 1},

        {name: 'idAppSubSystem', type:"int", defaultValue: 1},    
    ]
});