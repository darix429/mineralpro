Ext.define('InformationServices.sub.SystemAdminSecuritySecurityFunctionList.store.SecurityFunctionListStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    showMask: true,
    model: 'InformationServices.sub.SystemAdminSecuritySecurityFunctionList.model.SecurityFunctionListModel',
    proxy: {
        api: {
            create: '/api/InformationServices/SystemAdminSecuritySecurityFunctionList/CreateSecurityFunctionList',
            read: '/api/InformationServices/SystemAdminSecuritySecurityFunctionList/ReadSecurityFunctionList',
            update: '/api/InformationServices/SystemAdminSecuritySecurityFunctionList/UpdateSecurityFunctionList'
        }
       
    }
}); 
