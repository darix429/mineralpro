Ext.define('InformationServices.sub.SystemAdminSecuritySecurityFunctionList.store.SecurityFunctionListNameStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    
    model: 'InformationServices.sub.SystemAdminSecuritySecurityFunctionList.model.SecurityFunctionListNameModel',
    proxy: {

        api: {
          
            read: '/api/InformationServices/SystemAdminSecuritySecurityFunctionList/ReadSecurityFunctionListName',
           

        }
       
    }
}); 
