Ext.define('InformationServices.sub.ReportsExportsCADReports.store.CADReportsStore', {
    extend: 'MineralPro.store.BaseStore',
    autoLoad: true,
    showMask: true,
    model: 'InformationServices.sub.ReportsExportsCADReports.model.CADReportsModel',
    proxy: {
        api: {
            create: '/api/InformationServices/ReportsExportsCADReports/CreateCADReports',
            read: '/api/InformationServices/ReportsExports/ReadReports/CAD',
            update: '/api/InformationServices/ReportsExportsCADReports/UpdateCADReports'
        }      
    }
}); 
    