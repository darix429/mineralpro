Ext.define('InformationServices.sub.SystemAdminSecuritySecurityGroup.model.GroupListModel', {
    extend: 'MineralPro.model.BaseModel',
    fields: [
    {name: 'idSecurityGroup',type:"int",defaultValue: 1},
    {name: 'groupName',defaultValue: ''},
    {name: 'idFunctionSecurity',type:"int",defaultValue: 1},
    {name: 'securityCd',type:"int",defaultValue: 1},
    {name: 'idSubSystemFunction',type:"int",defaultValue: 1},
    {name: 'functionNameDisplay',defaultValue: ''},
    {name: 'idSubSystem',type:"int",defaultValue: 1},
    {name: 'subSystemNameDisplay',defaultValue: ''},
    {name: 'appName',defaultValue: ''},    
    ]
});