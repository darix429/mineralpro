Ext.define('InformationServices.sub.SystemAdminSecuritySecurityGroup.model.SecGroupModel', {
    extend: 'MineralPro.model.BaseModel',
    fields: 
    [{name: 'idSecurityGroup',type:"int",defaultValue: 1},
    {name: 'groupName',type:"string",defaultValue: ''},
    {name: 'displayField',type:"string",defaultValue: ''},
    {name: 'valueField',type:"string",defaultValue: ''},
    {name: 'securityCd',type:"int",defaultValue: 1}]
});