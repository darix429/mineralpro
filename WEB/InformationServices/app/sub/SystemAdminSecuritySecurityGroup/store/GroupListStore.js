Ext.define('InformationServices.sub.SystemAdminSecuritySecurityGroup.store.GroupListStore', {
    extend: 'MineralPro.store.BaseStore',
    //autoLoad: true,
    showMask: true,
    model: 'InformationServices.sub.SystemAdminSecuritySecurityGroup.model.GroupListModel',
    proxy: {

        api: {
            create: '/api/InformationService/SystemAdminSecuritySecurityGroup/CreateGroupList',
            read: '/api/InformationService/SystemAdminSecuritySecurityGroup/ReadGroupList',
            update: '/api/InformationService/SystemAdminSecuritySecurityGroup/UpdateGroupList'
        }
       
    }
}); 
