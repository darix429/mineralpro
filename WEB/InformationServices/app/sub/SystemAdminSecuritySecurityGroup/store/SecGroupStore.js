Ext.define('InformationServices.sub.SystemAdminSecuritySecurityGroup.store.SecGroupStore', {
    extend: 'MineralPro.store.BaseStore',
    //autoLoad: true,
    
    model: 'InformationServices.sub.SystemAdminSecuritySecurityGroup.model.SecGroupModel',
    proxy: {

        api: {
            read: '/api/InformationService/SystemAdminSecuritySecurityGroup/ReadSecurityGroup',
        }
       
    }
}); 
