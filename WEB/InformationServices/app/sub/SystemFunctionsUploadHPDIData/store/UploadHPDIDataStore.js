Ext.define('InformationServices.sub.SystemFunctionsUploadHPDIData.store.UploadHPDIDataStore', {
    extend: 'MineralPro.store.BaseStore',
    //autoLoad: true,
    showMask: true,
    model: 'InformationServices.sub.SystemFunctionsUploadHPDIData.model.UploadHPDIDataModel',
    proxy: {
        api: {
            create: '/api/InformationServices/SystemFunctionsUploadHPDIData/CreateUploadHPDIData',
            read: '/api/InformationServices/SystemFunctionsUploadHPDIData/ReadUploadHPDIData',
            update: '/api/InformationServices/SystemFunctionsUploadHPDIData/UpdateUploadHPDIData'
        }
       
    }
}); 
