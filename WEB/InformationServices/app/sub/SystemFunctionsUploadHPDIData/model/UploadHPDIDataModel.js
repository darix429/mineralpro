Ext.define('InformationServices.sub.SystemFunctionsUploadHPDIData.model.UploadHPDIDataModel', {
    extend: 'MineralPro.model.BaseModel',
    fields: [

        {name: 'tableName', type:"string", defaultValue: ''},
        {name: 'csv', type:"string", defaultValue: ''},
        {name: 'idHPDI_file', type:"int", defaultValue: ''},
        {name: 'fileFormat', type:"string", defaultValue: ''},
        {name: 'rowDeleteFlag', type:"string", defaultValue: ''},   
    ]
});