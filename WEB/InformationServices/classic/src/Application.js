/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('InformationServices.Application', {
    extend: 'Ext.app.Application',
    name: 'InformationServices',
    controllers: [
        'AppMainController',
        'CPLogin.controller.CPLoginController',
        'CPRegion.controller.CPRegionController',
        'InformationServices.sub.DefaultPage.controller.DefaultPageController'
    ],
    
     stores: [        
        'MineralPro.store.AppraisalYearStore',
        'MineralPro.store.CdJeHoldStore',
        'MineralPro.store.CdReasonStore',
        'MineralPro.store.CdTaxOfficeNotifyStore',
        'MineralPro.store.Crystal.CrystalStore',
    ],
    
    requires: [
        'Ext.form.field.Radio',
        'Ext.ux.grid.FilterRow',     
        
        'MineralPro.config.util.MainGridActionColCmpt',
        'MineralPro.config.util.MainGridToolCmpt',
        'MineralPro.config.util.PopUpWindowCmpt',
        'MineralPro.sub.CommonFunctionsSearch.controller.CommonFunctionsSearchController',
        
        'InformationServices.config.Runtime', 
        
        'InformationServices.sub.SystemAdminSecuritySecurityGroup.controller.SystemAdminSecuritySecurityGroupController',
        'InformationServices.sub.SystemAdminSecuritySecurityUserList.controller.SystemAdminSecuritySecurityUserListController',
        'InformationServices.sub.SystemAdminSecuritySecurityFunctionList.controller.SystemAdminSecuritySecurityFunctionListController',
        'InformationServices.sub.OrganizationOrganizationList.controller.OrganizationOrganizationListController',
        'InformationServices.sub.OrganizationOrganizationContact.controller.OrganizationOrganizationContactController',
        'InformationServices.sub.ReportsExportsCADReports.controller.ReportsExportsCADReportsController',
        'InformationServices.sub.ReportsExportsStateReports.controller.ReportsExportsStateReportsController',
        'InformationServices.sub.ReportsExportsPublicExports.controller.ReportsExportsPublicExportsController',
        'InformationServices.sub.SystemAdminCodeType.controller.SystemAdminCodeTypeController',
        'InformationServices.sub.SystemFunctionsUploadHPDIData.controller.SystemFunctionsUploadHPDIDataController'
    ],
    refs: [{
            ref: 'UserLoginWindowView',
            selector: 'userLoginWindowView'
    }],
    launch: function () {
        var me = this;               
        MineralPro.config.Runtime.appName = 'Information Services'   
        MineralPro.config.RuntimeUtility.disableBackspace();
        MineralPro.config.RuntimeUtility.enablePasswordChecking();
        MineralPro.config.RuntimeUtility.ImplementVtypes();
                        
//        MineralPro.config.RuntimeUtility.addThemeOptions();                       
        setTimeout(function () {
            Ext.globalEvents.fireEvent('checkUserSession');
            
            setTimeout(function () {
                
                if(MineralPro.config.Runtime.appraisalYear.length == 0){
                    me.getStore('MineralPro.store.AppraisalYearStore').load({
                        callback: function(){
                            var appYearStore = me.getStore('MineralPro.store.AppraisalYearStore')
                            var activeYearIndex = appYearStore.find('activeAppraisalYear', 'Y')
                            if(activeYearIndex != -1){
                                MineralPro.config.Runtime.appraisalYear = appYearStore.getAt(activeYearIndex).get('appraisalYear')
                            }else{
                                MineralPro.config.Runtime.appraisalYear = new Date().getFullYear();
                            }
                        }
                    })                  
                }   
                
                //me.getStore('MineralPro.store.AppraisalYearStore').load();               

            }, 200);
            
        }, 200);
        Ext.on('resize', function () {
            var windowLogin = me.getUserLoginWindowView();
            if (windowLogin) {
                windowLogin.center();
            }
        });
    },
//	mainView: 'MineralPro.view.MainViewPort'
});
