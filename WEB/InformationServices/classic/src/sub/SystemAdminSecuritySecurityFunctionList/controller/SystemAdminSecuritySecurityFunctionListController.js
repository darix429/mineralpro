///**
// * Handles the controller processing for Client List
// */

Ext.define('InformationServices.sub.SystemAdminSecuritySecurityFunctionList.controller.SystemAdminSecuritySecurityFunctionListController', {
    extend: 'Ext.app.Controller',
  
    models: ['InformationServices.sub.SystemAdminSecuritySecurityFunctionList.model.SecurityFunctionListModel',
    'InformationServices.sub.SystemAdminSecuritySecurityFunctionList.model.SecurityFunctionListNameModel',
    ],
    
    stores: ['InformationServices.sub.SystemAdminSecuritySecurityFunctionList.store.SecurityFunctionListStore',
    'InformationServices.sub.SystemAdminSecuritySecurityFunctionList.store.SecurityFunctionListNameStore',     
    ],  
    
    views: [
    'InformationServices.sub.SystemAdminSecuritySecurityFunctionList.view.SecurityFunctionListView',
    'InformationServices.sub.SystemAdminSecuritySecurityFunctionList.view.SecurityFunctionListGridView',
    'InformationServices.sub.SystemAdminSecuritySecurityFunctionList.view.SecurityFunctionListGridView',
    'InformationServices.sub.SystemAdminSecuritySecurityFunctionList.view.SecurityFunctionListAddFormView',
    'InformationServices.sub.SystemAdminSecuritySecurityFunctionList.view.SecurityFunctionListEditFormView',
    ],          
    
    requires: ['Ext.form.ComboBox',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden']
});


