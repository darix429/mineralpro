Ext.define('InformationServices.sub.SystemAdminSecuritySecurityFunctionList.view.SecurityFunctionListAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    
    requires: ['InformationServices.sub.SystemAdminSecuritySecurityFunctionList.view.SecurityFunctionListAddFormViewController'],
    controller: 'SecurityFunctionListAddFormViewController',
    
    alias: 'widget.addSecurityFunctionForm',
    title: 'Add New Function',
    
    defaultFocus : '#app_name',
    width: 350,
    
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'SystemAdminSecuritySecurityFunctionListId', //id of the main panel
    listGridStore:'InformationServices.sub.SystemAdminSecuritySecurityFunctionList.store.SecurityFunctionListStore', //store for note grid
    listGridAlias: 'SecurityFunctionListGrid', //alias name for note gridview
    
    initComponent: function() {
        var me = this
        me.items = [{
            xtype: 'form',
            defaults:{ 
                maskRe: /[^'\^]/,
                labelWidth: 130,
            },
            bodyPadding:5,
            items: [
            {
                fieldLabel: 'Application Name',
                xtype: 'combo',
                selectOnFocus: true,
                tabIndex: 1,
                id: 'myComboBox',
                itemId: 'app_name',
                displayField: 'appName',
                valueField: 'idApp',
                store: 'InformationServices.sub.SystemAdminSecuritySecurityFunctionList.store.SecurityFunctionListNameStore',
                listeners:{
                    select:function(record){
                        me.down('#appID').setValue(this.getRawValue());
                    },                 
                },
                name: 'idApp',
                typeAhead: true,
                forceSelection: true,
                allowBlank: false,
                labelAlign: 'right',
            },{
                fieldLabel: 'Subsystem',
                selectOnFocus: true,
                tabIndex: 2,
                name: 'subSystemName',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
            },{
                fieldLabel: 'Subsystem Display',
                selectOnFocus: true,
                tabIndex: 3,
                name: 'subSystemNameDisplay',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
            },{
                fieldLabel: 'Subsystem Order',
                selectOnFocus: true,
                tabIndex: 4,
                name: 'subSystemNameOrder',
                xtype: 'numberfield',
                minValue: 1,
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
            },{
                fieldLabel: 'Function',
                selectOnFocus: true,
                tabIndex: 5,
                name: 'functionName',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
            },{
                fieldLabel: 'Function Display',
                selectOnFocus: true,
                tabIndex: 6,
                name: 'functionNameDisplay',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
            },{
                fieldLabel: 'Function Order',
                selectOnFocus: true,
                tabIndex: 7,
                name: 'functionNameOrder',
                xtype: 'numberfield',
                minValue: 1,
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
            },{
                fieldLabel: 'Name',
                queryMode:'local',
                name: 'appName',
                xtype: 'hidden',
                readOnly:true,
                margin: '0 0 10',
                itemId:'appID',
                labelAlign: 'right',
                         
            }],
        }];
    
        me.listeners = {
            afterrender: function(){
                setTimeout(function () {
                    me.down('#appID').setValue(me.down('#app_name').getRawValue());
                }, 100);               
            }
        }
        me.callParent(arguments);
    },
    
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
    MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
    
});