Ext.define('InformationServices.sub.SystemAdminSecuritySecurityFunctionList.view.SecurityFunctionListEditFormView', {
    extend: 'InformationServices.sub.SystemAdminSecuritySecurityFunctionList.view.SecurityFunctionListAddFormView',
    alias: 'widget.editSecurityFunctionForm',
   
    title: 'Edit Function',
    defaultFocus: '#app_name',
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
    
});