Ext.define('InformationServices.sub.SystemAdminSecuritySecurityFunctionList.view.SecurityFunctionListView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.SystemAdminSecuritySecurityFunctionList',
    layout:'border',
    
    id: 'SystemAdminSecuritySecurityFunctionListId',    
    
    items: [{
        xtype:'SecurityFunctionListGrid',
        html:'center',
        region:'center'
    }]

});    
