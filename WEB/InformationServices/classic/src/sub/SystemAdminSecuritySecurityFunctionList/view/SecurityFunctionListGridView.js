
Ext.define('InformationServices.sub.SystemAdminSecuritySecurityFunctionList.view.SecurityFunctionListGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.SecurityFunctionListGrid',
    store:  'InformationServices.sub.SystemAdminSecuritySecurityFunctionList.store.SecurityFunctionListStore',
       
    requires: ['InformationServices.sub.SystemAdminSecuritySecurityFunctionList.view.SecurityFunctionListGridViewController'],
    controller: 'SecurityFunctionListGridViewController',     
          
//    //below are default items needed for grid checking 
    firstLoadItemId: 'app_name_function', 
    mainPanelAlias: 'SystemAdminSecuritySecurityFunctionList', //widget alias name of the main panel
    mainListModel: 'InformationServices.sub.SystemAdminSecuritySecurityFunctionList.model.SecurityFunctionListModel',     

    addFormAliasName: 'addSecurityFunctionForm', //widget form to call for add list 
    editFormAliasName: 'editSecurityFunctionForm', //widget form to call for edit

    firstFocus: 'app_name_function',    
    
    addButtonTooltip: 'Add New Function',
    addButtonText: 'Add New Function',
        
    columns:[{
        header: 'Application Name',
        text     : 'Application Name',
        flex     : 1, 
        sortable : true,
        dataIndex: 'appName',
        itemId: 'app_name_function',
       filterElement: new Ext.form.TextField(
                    {
                        listeners: {
                            afterrender: function (field) {
                                Ext.defer(function () {
                                    field.focus(true, 100);
                                }, 10);
                            },

                        }
                    }
            )         
    },{   
        header: 'Subsystem',
        text     : 'Subsystem',
        flex     : 1,
        dataIndex: 'subSystemName',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Subsystem Display',
        text     : 'Subsystem Display',
        flex     : 1,
        dataIndex: 'subSystemNameDisplay',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Subsystem Order',
        text     : 'Subsystem Order',
        flex     : 1,
        dataIndex: 'subSystemNameOrder',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Function',
        text     : 'Function',
        flex     : 1,
        dataIndex: 'functionName',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Function Display',
        text     : 'Function Display',
        flex     : 1,
        dataIndex: 'functionNameDisplay',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Function Order',
        text     : 'Function Order',
        flex     : 1,
        dataIndex: 'functionNameOrder',
        filterElement:new Ext.form.TextField()
    },{
        xtype: 'actioncolumn',
        width: InformationServices.config.Runtime.actionColWidth,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }       
    }],
 
    initComponent: function () {  
        var me = this;
        
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],
        
        me.tools= [
        MineralPro.config.util.MainGridToolCmpt.addNewDataButton
        ,MineralPro.config.util.MainGridToolCmpt.saveToDbButton                   
        ,MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
        me.callParent(arguments);
        
    }    
});