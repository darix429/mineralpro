Ext.define('InformationServices.sub.ReportsExportsCADReports.controller.ReportsExportsCADReportsController', {
    extend: 'Ext.app.Controller',
  
    models: [
        'InformationServices.sub.ReportsExportsCADReports.model.CADReportsModel',
    ],
    
    stores: [
        'InformationServices.sub.ReportsExportsCADReports.store.CADReportsStore',
        'InformationServices.sub.ReportsExports.store.ReportsExportsStore',
    ],  
    
    views: [
        'InformationServices.sub.ReportsExportsCADReports.view.CADReportsView',
        'InformationServices.sub.ReportsExportsCADReports.view.CADReportsGridView',
        'InformationServices.sub.ReportsExportsCADReports.view.CADReportsAddFormView',
        'InformationServices.sub.ReportsExportsCADReports.view.CADReportsEditFormView',
    ], 
    
    init: function(){              
        var me=this;
        me.getStore('InformationServices.sub.ReportsExports.store.ReportsExportsStore').load();              
    },
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden']
});