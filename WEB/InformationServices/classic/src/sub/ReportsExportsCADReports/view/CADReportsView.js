Ext.define('InformationServices.sub.ReportsExportsCADReports.view.CADReportsView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.ReportsExportsCADReports',
    layout:'border',
    
    id: 'ReportsExportsCADReportsId',
    
    items: [{
        xtype:'CADReportsGridView',
        html:'center',
        region:'center'
    }]
});    