
Ext.define('InformationServices.sub.ReportsExportsCADReports.view.CADReportsGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.CADReportsGridView',
    store:'InformationServices.sub.ReportsExportsCADReports.store.CADReportsStore',

    requires: ['InformationServices.sub.ReportsExportsCADReports.view.CADReportsGridViewController'],
    controller: 'CADReportsGridViewController',     
          
    //below are default items needed for grid checking 
    // firstLoadItemId: 'organizationNameItemId', 
    mainPanelAlias: 'ReportsExportsCADReports', //widget alias name of the main panel
    mainListModel: 'InformationServices.sub.ReportsExportsCADReports.model.CADReportsModel',     

    
//    detailAliasName: 'clientListDetails',//widget aliasName for detailView
    addFormAliasName: 'CADReportsAddFormView', //widget form to call for add list 
    editFormAliasName: 'CADReportsEditFormView', //widget form to call for edit
//    

    // firstFocus: 'organizationNameItemId',    
    
    addButtonTooltip: 'Add New CAD Report',
    addButtonText: 'Add New CAD Report',
        
    columns:[{ 
        header: 'Appraisal Year',
        flex     : 0,
        minWidth: 110,
        dataIndex: 'appraisalYear',
        filterElement: new Ext.form.TextField()
    },{   
        header: 'Report Category',
        flex     : 0,
        minWidth: 130,
        dataIndex: 'reportCategory',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Report Type',
        flex     : 0,
        dataIndex: 'reportType',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Report Description',
        flex     : 1,
        dataIndex: 'reportDesc',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'File Name',
        flex     : 1,
        dataIndex: 'fileName',
        filterElement:new Ext.form.TextField(),
        xtype: 'templatecolumn',
        // tpl: '<tpl if="fileName == \'YearByYearAnalysis.rpt\' || fileName == \'AppraisalSheetByOper.rpt\' || fileName == \'AppraisalSheetCompare-byTotValDesc.rpt\' || fileName == \'AppraisalSheetMailingLabels.rpt\'|| fileName == \'MineralAppraisalRollDetailCity.rpt\'|| fileName == \'MineralAppraisalRollDetailCollege.rpt\'|| fileName == \'MineralAppraisalRollDetailCounty.rpt\'|| fileName == \'MineralAppraisalRollDetailISD.rpt\'|| fileName == \'MineralAppraisalRollDetailHospital.rpt\'|| fileName == \'MineralAppraisalRollDetailSPDT.rpt\'|| fileName == \'MineralAppraisalRoll.rpt\'|| fileName == \'TotalByJurisWithExemptions.rpt\'"><a href="/CrystalReport/{appraisalYear}/{reportId}" target="_blank">{fileName}</a><tpl else>{fileName}</tpl>',
        tpl: '<a href="/CrystalReport/{reportId}?year={appraisalYear}" target="_blank">{fileName}</a>',
    },{   
        header: 'Display Order',
        flex     : 0,
        minWidth: 110,
        dataIndex: 'displayOrder',
        filterElement:new Ext.form.TextField()
    },{
        xtype: 'actioncolumn',
        width: 50,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls         
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    }],
 
    initComponent: function () {  
        var me = this;
        
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],
        
        me.tools= [
        MineralPro.config.util.MainGridToolCmpt.addNewDataButton
        ,MineralPro.config.util.MainGridToolCmpt.saveToDbButton                   
        ,MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
        me.callParent(arguments);
        
    }    
});