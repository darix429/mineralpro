Ext.define('InformationServices.sub.ReportsExportsCADReports.view.CADReportsEditFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',

    requires: ['InformationServices.sub.ReportsExportsCADReports.view.CADReportsAddFormViewController'],
    controller: 'CADReportsAddFormViewController',

    alias: 'widget.CADReportsEditFormView',
    
    title: 'Edit CAD Report',
    defaultFocus: '#reportType',

    mainPanelId: 'ReportsExportsCADReportsId', //id of the main panel
    listGridStore:'InformationServices.sub.ReportsExportsCADReports.store.CADReportsStore',
    listGridAlias: 'CADReportsGridView', //alias name for note gridview
    
    initComponent: function() {
        var me = this
        me.items = [{
            xtype: 'form',
            defaults:{ 
                maskRe: /[^'\^]/,
                labelWidth: 130,
            },
            bodyPadding:10,
            items: [{
                fieldLabel: 'Report Id',
                tabIndex: 1,
                width: 410,
                name: 'reportId',
                xtype: 'textfield',
                margin: '5 0 10',
                itemId: 'reportId',
                disabled:true,
                allowBlank: false,
                labelAlign: 'right',
                queryMode:'local',
                listeners: {  
                    blur: 'onAddCheckExisting'                                 
                }
            },{
                fieldLabel: 'Report Type',
                selectOnFocus: true,
                tabIndex: 1,
                width: 410,
                name: 'reportType',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                itemId: 'reportType',
                validateBlank: true,
                allowBlank: false,
                queryMode:'local',
            },{
                fieldLabel: 'File Name',
                selectOnFocus: true,
                tabIndex: 2,
                width:410,
                name: 'fileName',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
                queryMode:'local',
            },{
                fieldLabel: 'Report Description',
                selectOnFocus: true,
                tabIndex: 3,
                name: 'reportDesc',
                width:  410,
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                queryMode:'local',
                // validateBlank: true,
                // allowBlank: false
            },{
                fieldLabel: 'Display Order',
                tabIndex: 4,
                width:410,
                minValue: '0',
                name: 'displayOrder',
                xtype: 'numberfield',
                margin: '0 0 10',
                labelAlign: 'right',
                queryMode:'local',
                // validateBlank: true,
            }],
        }];

        me.callParent(arguments);
    },
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});