Ext.define('InformationServices.sub.ReportsExportsStateReports.controller.ReportsExportsStateReportsController', {
    extend: 'Ext.app.Controller',
  
    models: [
        'InformationServices.sub.ReportsExportsStateReports.model.StateReportsModel',
    ],
    
    stores: [
        'InformationServices.sub.ReportsExportsStateReports.store.StateReportsStore',
        'InformationServices.sub.ReportsExports.store.ReportsExportsStore',
    ],  
    
    views: [
        'InformationServices.sub.ReportsExportsStateReports.view.StateReportsView',
        'InformationServices.sub.ReportsExportsStateReports.view.StateReportsGridView',
        'InformationServices.sub.ReportsExportsStateReports.view.StateReportsAddFormView',
        'InformationServices.sub.ReportsExportsStateReports.view.StateReportsEditFormView',
    ],          

    init: function(){              
        var me=this;
        me.getStore('InformationServices.sub.ReportsExports.store.ReportsExportsStore').load();              
    },
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden']
});