Ext.define('InformationServices.sub.ReportsExportsStateReports.view.StateReportsView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.ReportsExportsStateReports',
    layout:'border',
    
    id: 'ReportsExportsStateReportsId',
    
    items: [{
        xtype:'StateReportsGridView',
        html:'center',
        region:'center'
    }]
});    