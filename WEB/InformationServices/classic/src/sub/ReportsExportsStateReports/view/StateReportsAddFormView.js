Ext.define('InformationServices.sub.ReportsExportsStateReports.view.StateReportsAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    
    requires: ['InformationServices.sub.ReportsExportsStateReports.view.StateReportsAddFormViewController'],
    controller: 'StateReportsAddFormViewController',
    
    alias: 'widget.StateReportsAddFormView',
    title: 'Add New State Report',
    
    defaultFocus : '#reportId',
    width: 440,
    
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'ReportsExportsStateReportsId', //id of the main panel
    listGridStore:'InformationServices.sub.ReportsExportsStateReports.store.StateReportsStore', //store for note grid
    // listGridAlias: 'StateReportsGridView', //alias name for note gridview

    initComponent: function() {
        var me = this
        me.items = [{
            xtype: 'form',
            defaults:{ 
                maskRe: /[^'\^]/,
                labelWidth: 130,
            },
            margin: '5 0 0 5',
            bodyPadding:5,
            items: [{
                fieldLabel: 'Report Id',
                selectOnFocus: true,
                tabIndex: 1,
                width: 410,
                name: 'reportId',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                itemId: 'reportId',
                validateBlank: true,
                allowBlank: false,
                // maxLength:9,
                minLength:1,
                maskRe:/[0-9.]/,
                listeners: {  
                    blur: 'onAddCheckExisting'                                 
                }
            },{
                fieldLabel: 'Report Type',
                selectOnFocus: true,
                tabIndex: 1,
                width: 410,
                name: 'reportType',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                itemId: 'reportType',
                validateBlank: true,
                allowBlank: false,
            },{
                fieldLabel: 'File Name',
                selectOnFocus: true,
                tabIndex: 2,
                width:410,
                name: 'fileName',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
            },{
                fieldLabel: 'Report Description',
                selectOnFocus: true,
                tabIndex: 3,
                name: 'reportDesc',
                width:  410,
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                // validateBlank: true,
                // allowBlank: false
            },{
                fieldLabel: 'Display Order',
                tabIndex: 4,
                width:410,
                minValue: '0',
                name: 'displayOrder',
                xtype: 'numberfield',
                margin: '0 0 10',
                labelAlign: 'right',
                // validateBlank: true,
            }],
        }];

        me.callParent(arguments);
    },
    
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
    MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
    
});