
Ext.define('InformationServices.sub.ReportsExportsStateReports.view.StateReportsGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.StateReportsGridView',
    store:'InformationServices.sub.ReportsExportsStateReports.store.StateReportsStore',

    requires: ['InformationServices.sub.ReportsExportsStateReports.view.StateReportsGridViewController'],
    controller: 'StateReportsGridViewController',     
          
    //below are default items needed for grid checking 
    // firstLoadItemId: 'organizationNameItemId', 
    mainPanelAlias: 'ReportsExportsStateReports', //widget alias name of the main panel
    mainListModel: 'InformationServices.sub.ReportsExportsStateReports.model.StateReportsModel',     

    
//    detailAliasName: 'clientListDetails',//widget aliasName for detailView
    addFormAliasName: 'StateReportsAddFormView', //widget form to call for add list 
    editFormAliasName: 'StateReportsEditFormView', //widget form to call for edit
//    

    // firstFocus: 'organizationNameItemId',    
    
    addButtonTooltip: 'Add New State Report',
    addButtonText: 'Add New State Report',
        
    columns:[{ 
        header: 'Appraisal Year',
        flex     : 0,
        minWidth: 110,
        dataIndex: 'appraisalYear',
        filterElement: new Ext.form.TextField()
    },{   
        header: 'Report Category',
        flex     : 0,
        minWidth: 130,
        dataIndex: 'reportCategory',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Report Type',
        flex     : 0,
        dataIndex: 'reportType',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Report Description',
        flex     : 1,
        dataIndex: 'reportDesc',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'File Name',
        flex     : 1,
        dataIndex: 'fileName',
        filterElement:new Ext.form.TextField(),
        xtype: 'templatecolumn',
        // tpl: '<tpl if="fileName == \'YearByYearAnalysis.rpt\' || fileName == \'AppraisalSheetByOper.rpt\' || fileName == \'AppraisalSheetCompare-byTotValDesc.rpt\' || fileName == \'AppraisalSheetMailingLabels.rpt\'|| fileName == \'MineralAppraisalRollDetailCity.rpt\'|| fileName == \'MineralAppraisalRollDetailCollege.rpt\'|| fileName == \'MineralAppraisalRollDetailCounty.rpt\'|| fileName == \'MineralAppraisalRollDetailISD.rpt\'|| fileName == \'MineralAppraisalRollDetailHospital.rpt\'|| fileName == \'MineralAppraisalRollDetailSPDT.rpt\'|| fileName == \'MineralAppraisalRoll.rpt\'|| fileName == \'TotalByJurisWithExemptions.rpt\'"><a href="/CrystalReport/{appraisalYear}/{reportId}" target="_blank">{fileName}</a><tpl else>{fileName}</tpl>',
        tpl: '<a href="/CrystalReport/{reportId}" target="_blank">{fileName}</a>',
    },{   
        header: 'Display Order',
        flex     : 0,
        minWidth: 110,
        dataIndex: 'displayOrder',
        filterElement:new Ext.form.TextField()
    },{
        xtype: 'actioncolumn',
        width: 50,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls         
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    }],
 
    initComponent: function () {  
        var me = this;
        
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],
        
        me.tools= [
        MineralPro.config.util.MainGridToolCmpt.addNewDataButton
        ,MineralPro.config.util.MainGridToolCmpt.saveToDbButton                   
        ,MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
        me.callParent(arguments);
        
    }    
});