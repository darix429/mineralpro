Ext.define('InformationServices.sub.ReportsExportsStateReports.view.StateReportsAddFormViewController', {
    extend : 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.StateReportsAddFormViewController',

    onAddCheckExisting: function(e, event, opt){
        MineralPro.config.RuntimeUtility.DebugLog('Entering StateReportsAddEditFormViewController onAddCheckExisting.')
        
        var me = this;  
        var view = me.getView();
        setTimeout(function (){                       
            var showAlert = false;

                var listStore = Ext.StoreMgr.lookup('InformationServices.sub.ReportsExports.store.ReportsExportsStore');

                if (listStore.findRecord(e.name, e.value, 0, false, false, true)) {
                    showAlert = true;
                    if (view.id.toLowerCase().indexOf("edit") != -1) {
                        if (listStore.getAt(view.editRecordIndex).get(e.name) == e.value) {
                            showAlert = false;
                        }
                    }                                     
                }
                if(showAlert){
                     Ext.Msg.show({
                        iconCls:MineralPro.config.Runtime.addExistingDataWarningCls,
                        title:MineralPro.config.Runtime.addExistingDataWarningTitle,
                        msg:MineralPro.config.Runtime.addExistingDataWarningMsg,
                        buttons: Ext.Msg.OK,
                        fn: function(){
                            e.focus();
                        }
                    });
                }            
        },300)
                                     
        MineralPro.config.RuntimeUtility.DebugLog('Leaving StateReportsAddEditFormViewController onAddCheckExisting.')    
    },
});