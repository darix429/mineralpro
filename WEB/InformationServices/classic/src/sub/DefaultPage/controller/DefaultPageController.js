///**
// * Handles the controller processing for Client List
// */

Ext.define('InformationServices.sub.DefaultPage.controller.DefaultPageController', {
    extend: 'Ext.app.Controller',
  
    models: [
    ],
    
    stores: [
    ],  
    
    views: [
        'InformationServices.sub.DefaultPage.view.DefaultPageView',
    ],          
    
    requires: ['Ext.form.ComboBox',
        'Ext.grid.column.Action',
        'Ext.form.FieldSet',
        'Ext.form.field.Hidden'
    ]
});


