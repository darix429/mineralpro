Ext.define('InformationServices.sub.DefaultPage.view.DefaultPageView', {
    extend: 'Ext.container.Container',
    alias: 'widget.defaultPageView',
    id: 'DefaultPageViewId',
    layout: 'hbox',
    defaults: {
        xtype: 'container',
        width: '50%'
    },
    items: [{
            items: [{
                    html:
                            '<p style="text-indent:5px; line-height:10px;"><b>Information Services: </b>'
                            + '    This application Serves as the Administrator Application of MineralPro.</p>'
                            + '    <p style="text-indent:10px; line-height:10px">Below are some list of functions with its details: </p>'
                            + '<p style="line-height:10px;"></p>'
                            + '<p style="text-indent:25px; line-height:10px;">1. System Admin Security</p>'
                            + '    <p style="text-indent:50px; line-height:10px;">-This group consist of three functions which handles the security for the MineralPro.</p>'
                            + '<p style="line-height:10px;"></p>'
                            + '    <p style="text-indent:50px; line-height:10px;">a. Security Group</p>'
                            + '        <p style="text-indent:75px; line-height:10px;">-This functions allows you to add and delete security group.</p>'
                            + '        <p style="text-indent:75px; line-height:10px;">-It will also allow you to set the Access level for each function.</p>'
                            + '        <p style="text-indent:75px; line-height:10px;">ie. (No Access, Read Only, Read/Write)</p>'
                            + '<p style="line-height:10px;"></p>'
                            + '    <p style="text-indent:50px; line-height:10px;">b. Security Userlist</p>'
                            + '        <p style="text-indent:75px; line-height:10px;">-This will allow you to add, modify and delete user information.</p>'
                            + '        <p style="text-indent:75px; line-height:10px;">-It will also allow you to assign security group (Access level) for each user.</p>'
                            + '<p style="line-height:10px;"></p>'
                            + '    <p style="text-indent:50px; line-height:10px;">c. Security Function List</p>'
                            + '        <p style="text-indent:75px; line-height:10px;">-This will allow you to add, modify and delete Functions for each application.</p>'
                            + '<p style="line-height:10px;"></p>'
                            + '<p style="text-indent:25px; line-height:10px;">2. Organization</p>'
                            + '    <p style="text-indent:50px; line-height:10px;">-This group consist of two functions for manipulating organization data.</p>'
                            + '<p style="line-height:10px;"></p>'
                            + '    <p style="text-indent:50px; line-height:10px;">a. Organization List</p>'
                            + '	<p style="text-indent:75px; line-height:10px;">-This function allows you to add, modify and delete organization.</p>'
                            + '    <p style="text-indent:50px; line-height:10px;">b. Organization contact</p>'
                            + '	<p style="text-indent:75px; line-height:10px;">-This function allow you to add contact details for each organization.</p>'
                            + '<p style="line-height:10px;"></p>'
                }]
        }, {
            items: [{
                    html:
                            '<p style="text-indent:25px; line-height:10px;">3. Common Functions</p>'
                            + '    <p style="text-indent:50px; line-height:10px;">-This group consists of functions which can be shared on all MineralPro applications</p>'
                            + '<p style="line-height:10px;"></p>'
                            + '    <p style="text-indent:50px; line-height:10px;">a. Search</p>'
                            + '	<p style="text-indent:75px; line-height:10px;">-This function allows you to search existing data in MineralPro</p>'                            
                            + '<p style="line-height:10px;"></p>'
                }]
        }]

});    