//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('InformationServices.sub.SystemAdminCodeType.view.CodeTypeAddFormViewController', {
    extend : 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.CodeTypeAddFormViewController',
    
 onCheckCodeType: function (me, e, key) {

        MineralPro.config.RuntimeUtility.DebugLog('Entering CodeTypeAddFormViewController onCheckCodeType.')
        var window = me.up('window');
        var listStore = Ext.StoreMgr.lookup(window.listGridStore);
        var allRecords = listStore.getData();
        if(listStore.getData().getSource())
            allRecords=listStore.getData().getSource()
        var record;

//        allRecords.findBy(function (frecord, id) {
//            if (frecord.get('codeType') == me.value) {
//                record = frecord;
//            }
//        })

                var codeNumber = 1;
        
                listStore.each(function (rec) {
                    if (rec.get('codeType') == me.value) {
                        record = rec
                        if (rec.get('code') >= codeNumber) {
                            codeNumber = rec.get('code') + 1
                        }
                    }
                })

        if (record) {
            var form = window.down('form');
            form.getRecord().set('sctDesc', record.get('sctDesc'));
            form.getRecord().set('columnName', record.get('columnName'));

            window.down('#sctDescItemId').setValue(record.get('sctDesc'));
//            window.down('#sctDescItemId').disable(true);
            window.down('#columnNameItemId').setValue(record.get('columnName'));
//            window.down('#columnNameItemId').disable(true);
            //console.log(me)
            if(me.enterKeyPressed){
                window.down('#shortDescriptionItemId').focus();
            }          
//            window.down('#appraisalYearItemId').focus();
            window.down('#codeNumberItemId').setValue(codeNumber)

        } else {
            window.down('#sctDescItemId').setValue('');
            window.down('#sctDescItemId').enable(true);
            window.down('#columnNameItemId').setValue('');
            window.down('#columnNameItemId').enable(true);
        }

        MineralPro.config.RuntimeUtility.DebugLog('Leaving CodeTypeAddFormViewController onCheckCodeType.')
    },
//    onChangeCodeNumber: function (me, newValue, oldValue, eOpts) {
//        MineralPro.config.RuntimeUtility.DebugLog('Entering CodeTypeAddFormViewController onChangeCodeNumber.')
//
//        var window = me.up('window');
//
//        var listStore = Ext.StoreMgr.lookup(window.listGridStore);
//        var allRecords = listStore.getData();
//        if(listStore.getData().getSource())
//            allRecords=listStore.getData().getSource() 
//        var cd_type = window.down('#codeType').getValue();
//        var appraisalYear = window.down('#appraisalYearItemId').getValue();
//        var showMsg = false;
//          
//        allRecords.findBy(function (frecord, id) {
//            if (frecord.get('codeType') == cd_type && frecord.get('code') == me.getValue() && frecord.get('appraisalYear') == appraisalYear ) {
//                showMsg = true;
//            }
//        })
//        if (showMsg) {
//            Ext.Msg.show({
//                iconCls: InformationServices.config.Runtime.codeTypeErrorIcon,
//                title: InformationServices.config.Runtime.codeTypeErrorTitle,
//                msg: InformationServices.config.Runtime.codeTypeErrorMsg,
//                buttons: Ext.Msg.OK,
//                fn: function () {
//                    me.focus();
//                }
//            });
//        }
//        MineralPro.config.RuntimeUtility.DebugLog('Leaving CodeTypeAddFormViewController onChangeCodeNumber.')
//    }
});
