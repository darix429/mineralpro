Ext.define('InformationServices.sub.SystemAdminCodeType.view.CodeTypeEditFormView', {
   extend: 'MineralPro.config.view.BaseAddEditFormView',
    
    requires: ['InformationServices.sub.SystemAdminCodeType.view.CodeTypeAddFormViewController'],
    controller: 'CodeTypeAddFormViewController',
    
    alias: 'widget.editCodeTypeForm',
    title: 'Edit Code Type',
    defaultFocus: '#codeType',
    width: 350,
    
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'SystemAdminCodeTypeId', //id of the main panel
    listGridStore:'InformationServices.sub.SystemAdminCodeType.store.CodeTypeStore', //store for note grid
    listGridAlias: 'CodeTypeGrid', //alias name for note gridview

    initComponent: function() {
        var me = this
        me.items = [{
            xtype: 'form',
            defaults:{ 
                maskRe: /[^'\^]/,
                labelWidth: 130,
            },
            bodyPadding:5,
            items: [{
                fieldLabel: 'Code Type',
                selectOnFocus: true,
                tabIndex: 1,
                name: 'codeType',
                xtype: 'numberfield',
                margin: '0 0 10',
                disabled: true,
                labelAlign: 'right',
                itemId:'codeType',
                validateBlank: true,
                allowBlank: false,
            },{
                fieldLabel: 'Code Description',
                selectOnFocus: true,
//                tabIndex: 4,
                name: 'sctDesc',
                xtype: 'textfield',
                margin: '0 0 10',
                disabled: true,
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
            },{
                fieldLabel: 'Code Number',
                selectOnFocus: true,
//                tabIndex: 5,
                name: 'code',
                xtype: 'numberfield',
                margin: '0 0 10',
                disabled: true,
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
            },{
                fieldLabel: 'Short Description',
                selectOnFocus: true,
                tabIndex: 2,
                name: 'shortDescription',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
            },{
                fieldLabel: 'Description',
                selectOnFocus: true,
                tabIndex: 3,
                name: 'description',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
//            },{
//                xtype: 'radiogroup',
//                fieldLabel: 'Active',
//                columns: 2,
//                labelAlign: 'right',
//                width: 300,   
//                itemId: 'myActive',
//                items: [{
//                    xtype: 'radiofield',
//                    boxLabel: 'Yes',
//                    name: 'activeInd',
//                    inputValue: 'Y'
//                },
//                {
//                    xtype: 'radiofield',
//                    boxLabel: 'No',
//                    name: 'activeInd',
//                    inputValue: 'N'
//                }]
//            },{
//                xtype: 'radiogroup',
//                fieldLabel: 'Static',
//                columns: 2,
//                labelAlign: 'right',
//                width: 300,   
//                itemId: 'myStatic',
//                items: [{
//                    xtype: 'radiofield',
//                    boxLabel: 'Yes',
//                    name: 'staticInd',
//                    inputValue: 'Y'
//                },
//                {
//                    xtype: 'radiofield',
//                    boxLabel: 'No',
//                    name: 'staticInd',
//                    inputValue: 'N'
//                }]
            },{
                fieldLabel: 'Column Name',
                selectOnFocus: true,
//                tabIndex: 7,
                name: 'columnName',
                xtype: 'textfield',
                margin: '0 0 10',
                disabled: true,
                labelAlign: 'right',
                validateBlank: true,
//                allowBlank: false,
            }],
        }];
    
    
        me.callParent(arguments);
    },
    
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
    
});
    
   
  
    
    