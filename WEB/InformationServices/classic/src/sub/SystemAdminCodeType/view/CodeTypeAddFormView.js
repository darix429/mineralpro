Ext.define('InformationServices.sub.SystemAdminCodeType.view.CodeTypeAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    
    requires: ['InformationServices.sub.SystemAdminCodeType.view.CodeTypeAddFormViewController'],
    controller: 'CodeTypeAddFormViewController',
    
    alias: 'widget.addCodeTypeForm',
    title: 'Add New Code',
    
    defaultFocus : '#codeType',
    width: 350,
    
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'SystemAdminCodeTypeId', //id of the main panel
    listGridStore:'InformationServices.sub.SystemAdminCodeType.store.CodeTypeStore', //store for note grid
    listGridAlias: 'CodeTypeGrid', //alias name for note gridview

    initComponent: function() {
        var me = this
        me.items = [{
            xtype: 'form',
            defaults:{ 
                maskRe: /[^'\^]/,
                labelWidth: 130,
            },
            bodyPadding:5,
            items: [{
                fieldLabel: 'Code Type',
                selectOnFocus: true,
                tabIndex: 1,
                name: 'codeType',
                xtype: 'combobox',
                valueField: 'codeType',
                displayField: 'codeType',
                store: 'InformationServices.sub.SystemAdminCodeType.store.CodeTypeComboStore',
                queryMode: 'local',
                margin: '0 0 10',
                forceSelection: true,
//                minValue: '0',
                labelAlign: 'right',
                itemId:'codeType',
                validateBlank: true,
                allowBlank: false,
                enableKeyEvents: true,
                        listeners: {
                            keydown: function(form, e){
                                if(e.keyCode == 18){
                                    this.enterKeyPressed = true;
                                }
                            },
                            change: 'onCheckCodeType'
                            
                        }
            },{
                fieldLabel: 'Code Description',
                selectOnFocus: true,
//                tabIndex: 3,
                name: 'sctDesc',
                xtype: 'textfield',
                margin: '0 0 10',
//                disabled: true,
                readOnly: true,
                cls: 'x-item-disabled',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
                itemId: 'sctDescItemId'
            },{
                fieldLabel: 'Code Number',
                selectOnFocus: true,
//                tabIndex: 4,
                name: 'code',
                xtype: 'numberfield',
                margin: '0 0 10',
                readOnly: true,
                cls: 'x-item-disabled',
                minValue: '0',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
                itemId: 'codeNumberItemId'
//                listeners: {
//                        blur: 'onChangeCodeNumber'
//                    }
            },{
                fieldLabel: 'Short Description',
                tabIndex: 2,
                name: 'shortDescription',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                itemId: 'shortDescriptionItemId'
            },{
                fieldLabel: 'Description',
                tabIndex: 3,
                name: 'description',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
//            },{
//                xtype: 'radiogroup',
//                fieldLabel: 'Active',
//                columns: 2,
//                tabIndex: 7,
//                labelAlign: 'right',
//                width: 300,   
//                itemId: 'myActive',
//                items: [{
//                    xtype: 'radiofield',
//                    boxLabel: 'Yes',
//                    name: 'activeInd',
//                    checked: true,
//                    inputValue: 'Y'
//                },
//                {
//                    xtype: 'radiofield',
//                    boxLabel: 'No',
//                    name: 'activeInd',
//                    inputValue: 'N'
//                }]
//            },{
//                xtype: 'radiogroup',
//                fieldLabel: 'Static',
//                columns: 2,
//                tabIndex: 8,
//                labelAlign: 'right',
//                width: 300,   
//                itemId: 'myStatic',
//                items: [{
//                    xtype: 'radiofield',
//                    boxLabel: 'Yes',
//                    name: 'staticInd',
//                    checked: true,
//                    inputValue: 'Y'
//                },
//                {
//                    xtype: 'radiofield',
//                    boxLabel: 'No',
//                    name: 'staticInd',
//                    inputValue: 'N'
//                }]
            },{
                fieldLabel: 'Column Name',
                selectOnFocus: true,
//                tabIndex: 9,
                name: 'columnName',
                xtype: 'textfield',
                margin: '0 0 10',
//                disabled: true,
                readOnly: true,
                cls: 'x-item-disabled',
                labelAlign: 'right',
                validateBlank: true,
//                allowBlank: false,
                itemId: 'columnNameItemId'
            }],
        }];
    
    
        me.callParent(arguments);
    },
    
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
    MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
    
});