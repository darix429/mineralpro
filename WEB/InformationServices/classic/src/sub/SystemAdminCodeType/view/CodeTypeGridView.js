
Ext.define('InformationServices.sub.SystemAdminCodeType.view.CodeTypeGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.CodeTypeGrid',
    store:  'InformationServices.sub.SystemAdminCodeType.store.CodeTypeStore',
       
    requires: ['InformationServices.sub.SystemAdminCodeType.view.CodeTypeGridViewController'],
    controller: 'CodeTypeGridViewController',     
          
//    //below are default items needed for grid checking 
    firstLoadItemId: 'code_type', 
    mainPanelAlias: 'SystemAdminCodeType', //widget alias name of the main panel
    mainListModel: 'InformationServices.sub.SystemAdminCodeType.model.CodeTypeModel',     

    addFormAliasName: 'addCodeTypeForm', //widget form to call for add list 
    editFormAliasName: 'editCodeTypeForm', //widget form to call for edit

    firstFocus: 'code_type',    
    
    addButtonTooltip: 'Add New Code',
    addButtonText: 'Add New Code',
        
    columns:[{   
        header: 'Code Type',
        text     : 'Code Type',
        flex     : 1,
        dataIndex: 'codeType',
        itemId: 'code_type',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Code Description',
        text     : 'Code Description',
        flex     : 1,
        dataIndex: 'sctDesc',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Code Number',
        text     : 'Code Number',
        flex     : 1,
        dataIndex: 'code',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Short Description',
        text     : 'Short Description',
        flex     : 1,
        dataIndex: 'shortDescription',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Description',
        text     : 'Description',
        flex     : 1,
        dataIndex: 'description',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'ActiveInd',
        text     : 'ActiveInd',
        flex     : 1,
        dataIndex: 'activeInd',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'StaticInd',
        text     : 'StaticInd',
        flex     : 1,
        dataIndex: 'staticInd',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Column Name',
        text     : 'Column Name',
        flex     : 1,
        dataIndex: 'columnName',
        filterElement:new Ext.form.TextField()
    },{
        xtype: 'actioncolumn',
        width: InformationServices.config.Runtime.actionColWidth,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }       
    }],
 
    initComponent: function () {  
        var me = this;
        
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],
        
        me.tools= [
        MineralPro.config.util.MainGridToolCmpt.addNewDataButton
        ,MineralPro.config.util.MainGridToolCmpt.saveToDbButton                   
        ,MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
        me.callParent(arguments);
        
    }    
});