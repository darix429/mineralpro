Ext.define('InformationServices.sub.SystemAdminCodeType.view.CodeTypeView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.SystemAdminCodeType',
    layout:'border',
    
    id: 'SystemAdminCodeTypeId',    
    
    items: [{
        xtype:'CodeTypeGrid',
        html:'center',
        region:'center'
    }]

});    
