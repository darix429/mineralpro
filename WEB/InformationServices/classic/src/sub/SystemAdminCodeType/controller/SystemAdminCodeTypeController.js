
Ext.define('InformationServices.sub.SystemAdminCodeType.controller.SystemAdminCodeTypeController', {
    extend: 'Ext.app.Controller',
  
    models: [
        'InformationServices.sub.SystemAdminCodeType.model.CodeTypeModel'
    ],
    
    stores: [
        'InformationServices.sub.SystemAdminCodeType.store.CodeTypeStore',
        'InformationServices.sub.SystemAdminCodeType.store.CodeTypeComboStore'
    ],  
    
    views: [
        'InformationServices.sub.SystemAdminCodeType.view.CodeTypeView',
        'InformationServices.sub.SystemAdminCodeType.view.CodeTypeGridView',
        'InformationServices.sub.SystemAdminCodeType.view.CodeTypeAddFormView',
        'InformationServices.sub.SystemAdminCodeType.view.CodeTypeEditFormView',
    ],          
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden']
});


