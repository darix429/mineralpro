//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('InformationServices.sub.SystemFunctionsUploadHPDIData.view.UploadHPDIDataGridViewController', {
    extend : 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.UploadHPDIDataGridViewController',
    
    onRunFile: function (button) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering UploadHPDIDataGridViewController onRunFile.')
        var me = this;
        var mainView = me.getView();
        var win = mainView.up().up().up();
        var selectedHpdi = mainView.getSelectionModel().getSelection();
        var hpdiStore = mainView.getStore();
        //var selected = [];
        //console.log(hpdiStore)
        Ext.each(selectedHpdi, function (item) {
            item.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
            //selected.push(item.data);
        });
        //console.log(selected)
        Ext.Msg.show({
                title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                msg:'Are you sure you want to Execute this selected File?',
                iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                buttons: Ext.Msg.YESNO,
                scope: this,
                width: 250,
                fn: function (btn) {
                    if (btn === 'yes') {
                        win.mask('Please wait while files are being execute...')
                        hpdiStore.getProxy().setTimeout(120000);// 2 min timeout
                        hpdiStore.getProxy().extraParams = {
                            updateLength: '1',
                            run: true
                        };
                        hpdiStore.sync({
                            success: function(batch, options){
                                Ext.Msg.show({
                                    title:'Success!',
                                    msg:'Files have successfully been execute.',
                                    buttons: Ext.Msg.OK
                                });
                                win.unmask()
                                hpdiStore.reload();
                            },
                            failure: function(a,b){
                                Ext.Msg.show({
                                    title:'Fail!',
                                    msg:'Files have failed to execute.',
                                    buttons: Ext.Msg.OK
                                });
                                win.unmask()
                                hpdiStore.reload();
                            }
                        });
                        hpdiStore.commitChanges();
                    }else{
                        hpdiStore.rejectChanges();
                    }
                }
        })            
        MineralPro.config.RuntimeUtility.DebugLog('Leaving UploadHPDIDataGridViewController onRunFile.')
    },
    /**
     * Open up the Edit form for editing
     */
    onOpenEditListForm: function (grid, rowIndex, colIndex) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onOpenEditListForm.')
        var me = this;
        var mainView = me.getView();
        var rec = mainView.getStore().getAt(rowIndex);        
        var editView = Ext.widget(mainView.editFormAliasName);
        
        editView.editRecordIndex = rowIndex   
        editView.down('form').loadRecord(rec); 
        editView.down('form').down('#csvItemId').emptyText = rec.data.csv
        editView.down('form').down('#fmtItemId').emptyText = rec.data.fileFormat
        editView.down('form').down('#csvItemId').value = rec.data.csv
        editView.down('form').down('#fmtItemId').value = rec.data.fileFormat
        editView.show();  
    
    }

});