//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('InformationServices.sub.SystemFunctionsUploadHPDIData.view.UploadHPDIDataAddFormViewController', {
    extend : 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.UploadHPDIDataAddFormViewController',
 

    onUploadFile: function (button) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering UploadHPDIDataAddFormViewController onUploadFile.')
         var me = this;                     
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore); 
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues(); 
        var csv = form.down('#csvItemId').value;
        var fmt = form.down('#fmtItemId').value;
        if(form.isValid()){
            Ext.Msg.show({
                title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                msg:MineralPro.config.Runtime.syncGridMsgConfimationMsg,
                iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                buttons: Ext.Msg.YESNO,
                scope: this,
                width: 250,
                fn: function (btn) {
                    if (btn === 'yes') {
                        record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                        record.set('csv', csv.replace(/C:\\fakepath\\/g, ''))
                        record.set('fileFormat', fmt.replace(/C:\\fakepath\\/g, ''))
                        record.set(values); 
                        form.submit({
                            url: '/api/InformationServices/SystemFunctionsUploadHPDIData/UploadFile',
                            waitMsg: 'Uploading your file...',
                            success: function(fp, o) {
                                Ext.Msg.show({
                                    title:'Success!',
                                    msg:'File has been uploaded.',
                                    buttons: Ext.Msg.OK
                                });
                                listStore.insert(0, record);
                                listStore.getProxy().extraParams = {
                                    createLength: listStore.getNewRecords().length,
                                    updateLength: listStore.getUpdatedRecords().length
                                };

                                listStore.sync({
                                    success: function(batch, options){
                                            listStore.reload({
                                            });
                                    }
                                });
                                listStore.commitChanges();
                                 win.close();
                            },
                            failure: function() {
                                Ext.Msg.alert("Error", Ext.JSON.decode(this.response.responseText).message);
                            }
                        });
                        
                    }
                }
            });   
            
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving UploadHPDIDataAddFormViewController onUploadFile.')
    },
    onUploadUpdateFile: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering UploadHPDIDataAddFormViewController onUploadUpdateFile.')

        var me = this;        
        var view = me.getView();
        var mainPanel = Ext.getCmp(view.mainPanelId);
        var listGrid = mainPanel.down(view.listGridAlias)
        var listStore = Ext.StoreMgr.lookup(view.listGridStore); 
        var win = button.up('window');
        var form = win.down('form');
        //var record = form.getRecord();
        var csv = form.down('#csvItemId').value;
        var fmt = form.down('#fmtItemId').value; 
	    var record = listGrid.getStore().getAt(view.editRecordIndex);
        var values = form.getValues();
        
        if(form.isValid()){              
            Ext.Msg.show({
                title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                msg:MineralPro.config.Runtime.syncGridMsgConfimationMsg,
                iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                buttons: Ext.Msg.YESNO,
                scope: this,
                width: 250,
                fn: function (btn) {
                    if (btn === 'yes') {
                        record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                        record.set('csv', csv.replace(/C:\\fakepath\\/g, ''))
                        record.set('fileFormat', fmt.replace(/C:\\fakepath\\/g, ''))
                        record.set(values); 
                        form.submit({
                            url: '/api/InformationServices/SystemFunctionsUploadHPDIData/UploadFile',
                            waitMsg: 'Uploading your file...',
                            success: function(fp, o) {
                                Ext.Msg.show({
                                    title:'Success!',
                                    msg:'File has been uploaded.',
                                    buttons: Ext.Msg.OK
                                });
                                listStore.insert(0, record);
                                listStore.getProxy().extraParams = {
                                    createLength: listStore.getNewRecords().length,
                                    updateLength: listStore.getUpdatedRecords().length
                                };

                                listStore.sync({
                                    success: function(batch, options){
                                            listStore.reload({
                                            });
                                    }
                                });
                                listStore.commitChanges();
                                 win.close();
                            },
                            failure: function() {
                                Ext.Msg.alert("Error", Ext.JSON.decode(this.response.responseText).message);
                            }
                        });
                    }
                }
            });                             
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        } 
        MineralPro.config.RuntimeUtility.DebugLog('Leaving UploadHPDIDataAddFormViewController onUploadUpdateFile.')
    },
    onSyncListGridData: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseGridViewController onSyncListGridData.')
        var me = this;       
        var view = me.getView();
        var listStore = view.getStore();
                                  
        var dirty = listStore.getNewRecords().length > 0
        || listStore.getUpdatedRecords().length > 0
        || listStore.getRemovedRecords().length > 0;
        if (dirty) {
            Ext.Msg.show({
                title:MineralPro.config.Runtime.syncGridMsgConfimationTitle,
                msg:MineralPro.config.Runtime.syncGridMsgConfimationMsg,
                iconCls:MineralPro.config.Runtime.syncGridMsgConfimationIcon,
                buttons: Ext.Msg.YESNO,
                scope: this,
                width: 250,
                fn: function (btn) {
                    if (btn === 'yes') {
                        view.getSelectionModel().deselectAll(true);
                        listStore.getProxy().extraParams = {
                            createLength: listStore.getNewRecords().length,
                            updateLength: listStore.getUpdatedRecords().length
                        };

                        listStore.sync({
                            success: function(batch, options){
                                var mainPanel = view.up(view.mainPanelAlias)
                                var getIndex = function(){
                                    /* Get selected Row if exists, otherwise select index 0 */
                                    var selectedIndex = 0;
                                    if(mainPanel){
                                        selectedIndex = listStore.findBy(function(rec){
                                            var selectedId = rec.get(mainPanel.selectedIdIndex) == mainPanel.selectedId;
                                            var rrcNumber = rec.get(mainPanel.selectedRRCNumberIndex) == mainPanel.selectedRRCNumber;
                                            return selectedId && rrcNumber;
                                        });
                                    }
                                    return selectedIndex > -1 ? selectedIndex : 0;
                                }

                                if(mainPanel.selectedId){
                                    listStore.getProxy().extraParams = {
                                        selectedId: mainPanel.selectedId
                                    };
                                    listStore.reload({
                                        callback: function (record, operation, success){
                                            if(success){
                                                view.getSelectionModel().select(getIndex(), false, false);
                                            }
                                            else{
                                                Ext.Msg.show({
                                                    title:'Oops!',
                                                    msg:'Something went wrong while retrieving Leases',
                                                    buttons: Ext.Msg.OK
                                                });
                                            }
                                        }
                                    });
                                }
                                else{
                                    view.getSelectionModel().select(getIndex(), false, false);
                                }
                                if(view.affectedComboStore){
                                    var affectedComboStoreArr = view.affectedComboStore;
                                    Ext.each(affectedComboStoreArr, function(store){
                                        Ext.StoreMgr.lookup(store).reload();
                                    })
                                }
                            },
                            failure: function(batch, options){
                                Ext.Msg.show({
                                    iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                                    title:MineralPro.config.Runtime.listSyncMsgErrorTitle,
                                    msg:MineralPro.config.Runtime.listSyncMsgErrorMsg,
                                    buttons: Ext.Msg.OK
                                });  
                            }
                        });
                        listStore.commitChanges();
                    }
                }
            });
        } else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.listSyncMsgErrorIcon,
                title:MineralPro.config.Runtime.listSyncMsgErrorTitle,
                msg:MineralPro.config.Runtime.listSyncMsgErrorMsg,
                buttons: Ext.Msg.OK
            });                      
        }                         
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseGridViewController onSyncListGridData.')
    },
});