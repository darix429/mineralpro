Ext.define('InformationServices.sub.SystemFunctionsUploadHPDIData.view.UploadHPDIDataView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.SystemFunctionsUploadHPDIData',
    layout:'border',
    
    id: 'SystemFunctionsUploadHPDIDataId',    
    
    items: [{
        xtype:'UploadHPDIDataGrid',
        html:'center',
        region:'center'
    }]

});    
