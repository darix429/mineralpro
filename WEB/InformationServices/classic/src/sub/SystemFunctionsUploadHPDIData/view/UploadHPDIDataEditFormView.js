Ext.define('InformationServices.sub.SystemFunctionsUploadHPDIData.view.UploadHPDIDataEditFormView', {
    extend: 'InformationServices.sub.SystemFunctionsUploadHPDIData.view.UploadHPDIDataAddFormView',
    alias: 'widget.editUploadHPDIDataForm',
   
    title: 'Edit Function',
    defaultFocus: '#tableNameItemId',
    buttons: [uploadFile = {
                xtype: 'button',
                itemId: 'uploadFileId',
               // tooltip: 'Upload and Save to Database',
                text: 'Upload and Update File',
                listeners: {
                    click: 'onUploadUpdateFile',
                }
            },
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
    
});