
Ext.define('InformationServices.sub.SystemFunctionsUploadHPDIData.view.UploadHPDIDataGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.UploadHPDIDataGrid',
    store:  'InformationServices.sub.SystemFunctionsUploadHPDIData.store.UploadHPDIDataStore',
       
    requires: ['InformationServices.sub.SystemFunctionsUploadHPDIData.view.UploadHPDIDataGridViewController'],
    controller: 'UploadHPDIDataGridViewController',     
          
//    //below are default items needed for grid checking 
    firstLoadItemId: 'table_name', 
    mainPanelAlias: 'SystemFunctionsUploadHPDIData', //widget alias name of the main panel
    mainListModel: 'InformationServices.sub.SystemFunctionsUploadHPDIData.model.UploadHPDIDataModel',     

    addFormAliasName: 'addUploadHPDIDataForm', //widget form to call for add list 
    editFormAliasName: 'editUploadHPDIDataForm', //widget form to call for edit
    selectFirstRow: false,
    firstFocus: 'table_name',    
    
    addButtonTooltip: 'Add New File',
    addButtonText: 'Add New File',
        
    columns:[{
        header: 'Table Name',
        text     : 'Table Name',
        flex     : 1, 
        sortable : true,
        dataIndex: 'tableName',
        itemId: 'table_name',
       filterElement: new Ext.form.TextField()         
    },{   
        header: 'CSV',
        text     : 'CSV',
        flex     : 1,
        dataIndex: 'csv',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'File Format',
        text     : 'File Format',
        flex     : 1,
        dataIndex: 'fileFormat',
        filterElement:new Ext.form.TextField()
    },{
        xtype: 'actioncolumn',
        width: InformationServices.config.Runtime.actionColWidth,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }       
    }],
    
    initComponent: function () {  
        var me = this;

        me.selModel= Ext.create('Ext.selection.CheckboxModel',{
            checkOnly: true,
             listeners: {
                selectionchange: function(){
                    var items = me.getSelectionModel().getSelected().length;
                    if(items == 4){
                        me.down('#RunButtonId').enable();
                    }else{
                        me.down('#RunButtonId').disable();
                    }
                }
            } 
        })

        var RunButton = {
                xtype: 'button',
                disabled: true,
                itemId: 'RunButtonId',
                tooltip: 'Execute',
                text: 'Execute',
                align: 'left',
                //iconCls: Minerals.config.Runtime.deleteCls,
                listeners: {
                    click: 'onRunFile',
                    afterrender: function(){
                        var me = this
                        me.setHidden(MineralPro.config.Runtime.access_level)
                    }
                }
            }
        
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],
        
        me.tools= [
        RunButton    
        ,MineralPro.config.util.MainGridToolCmpt.addNewDataButton
        ,MineralPro.config.util.MainGridToolCmpt.saveToDbButton                   
        ,MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
        me.callParent(arguments);
        
    }    
});