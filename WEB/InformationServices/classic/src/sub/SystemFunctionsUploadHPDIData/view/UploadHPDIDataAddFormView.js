Ext.define('InformationServices.sub.SystemFunctionsUploadHPDIData.view.UploadHPDIDataAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    
    requires: ['InformationServices.sub.SystemFunctionsUploadHPDIData.view.UploadHPDIDataAddFormViewController'],
    controller: 'UploadHPDIDataAddFormViewController',
    
    alias: 'widget.addUploadHPDIDataForm',
    title: 'Add New File',
    
    defaultFocus : '#tableNameItemId',
    width: 470,
    
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'SystemFunctionsUploadHPDIDataId', //id of the main panel
    listGridStore:'InformationServices.sub.SystemFunctionsUploadHPDIData.store.UploadHPDIDataStore', //store for note grid
    listGridAlias: 'UploadHPDIDataGrid', //alias name for note gridview
    
    initComponent: function() {
        var me = this
        me.items = [{
            xtype: 'form',
            defaults:{ 
                maskRe: /[^'\^]/,
                labelWidth: 100,
            },
            bodyPadding:5,
            items: [
            {
                fieldLabel: 'Table Name',
                selectOnFocus: true,
                tabIndex: 1,
                itemId: 'tableNameItemId',
                name: 'tableName',
                xtype: 'combo',
                store: ['HPDIOTHER_DESC','HPDIOTHER_PROD','HPDI_DESC','HPDI_PROD'] ,
                displayField: 'tableName',  
                typeAhead: true,
                forceSelection: true,
                selectOnFocus: true,
                margin: '0 0 10',
                anchor: '100%',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
            },{
                xtype: 'filefield',
                name: 'csv',
                itemId: 'csvItemId',
                fieldLabel: 'CSV file',
                msgTarget: 'side',
                labelAlign: 'right',
                allowBlank: false,
                anchor: '100%',
                emptyText: 'Select CSV file...',
                buttonText: 'Select file',
                tabIndex: 2,
                validateBlank: true,
                margin: '0 0 10',
            },{
                xtype: 'filefield',
                name: 'fileFormat',
                itemId: 'fmtItemId',
                fieldLabel: 'FMT file',
                msgTarget: 'side',
                labelAlign: 'right',
                allowBlank: false,
                anchor: '100%',
                emptyText: 'Select FMT file...',
                buttonText: 'Select file',
                tabIndex: 3,
                validateBlank: true,
                margin: '0 0 10', 
            }],
        }];
    
        me.listeners = {
            afterrender: function(){
                // setTimeout(function () {
                //     me.down('#appID').setValue(me.down('#app_name').getRawValue());
                // }, 100);               
            }
        }
        
        me.callParent(arguments);

        
    },
    
    
    buttons: [
    uploadFile = {
                xtype: 'button',
                itemId: 'uploadFileId',
               // tooltip: 'Upload and Save to Database',
                text: 'Upload and Save',
                listeners: {
                    click: 'onUploadFile',
                }
            },
    //MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
    
});