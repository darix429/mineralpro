///**
// * Handles the controller processing for Client List
// */

Ext.define('InformationServices.sub.SystemFunctionsUploadHPDIData.controller.SystemFunctionsUploadHPDIDataController', {
    extend: 'Ext.app.Controller',
  
    models: ['InformationServices.sub.SystemFunctionsUploadHPDIData.model.UploadHPDIDataModel'
    ],
    
    stores: ['InformationServices.sub.SystemFunctionsUploadHPDIData.store.UploadHPDIDataStore'   
    ],  
    
    views: [
    'InformationServices.sub.SystemFunctionsUploadHPDIData.view.UploadHPDIDataView',
    'InformationServices.sub.SystemFunctionsUploadHPDIData.view.UploadHPDIDataGridView',
    'InformationServices.sub.SystemFunctionsUploadHPDIData.view.UploadHPDIDataGridView',
    'InformationServices.sub.SystemFunctionsUploadHPDIData.view.UploadHPDIDataAddFormView',
    'InformationServices.sub.SystemFunctionsUploadHPDIData.view.UploadHPDIDataEditFormView',
    ],          
    
    requires: ['Ext.form.ComboBox',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden']
});


