///**
// * Handles the controller processing for Client List
// */

Ext.define('InformationServices.sub.SystemAdminSecuritySecurityGroup.controller.SystemAdminSecuritySecurityGroupController', {
    extend: 'Ext.app.Controller',
  
    models: ['InformationServices.sub.SystemAdminSecuritySecurityGroup.model.GroupListModel',
    'InformationServices.sub.SystemAdminSecuritySecurityGroup.model.SecGroupModel'
    ],
    
    stores: ['InformationServices.sub.SystemAdminSecuritySecurityGroup.store.GroupListStore',
    'InformationServices.sub.SystemAdminSecuritySecurityGroup.store.SecGroupStore'
    ], 
    
    views: [
    'InformationServices.sub.SystemAdminSecuritySecurityGroup.view.SecurityGroupView',
    'InformationServices.sub.SystemAdminSecuritySecurityGroup.view.SecurityGroupGridView',  
    'InformationServices.sub.SystemAdminSecuritySecurityGroup.view.SecurityGroupAddFormView',
    'InformationServices.sub.SystemAdminSecuritySecurityGroup.view.SecurityGroupEditFormView'
    ],          
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden']        
});


