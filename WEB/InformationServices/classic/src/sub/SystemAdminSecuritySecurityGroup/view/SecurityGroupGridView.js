
Ext.define('InformationServices.sub.SystemAdminSecuritySecurityGroup.view.SecurityGroupGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.securityGroupGrid',
   
    requires: ['InformationServices.sub.SystemAdminSecuritySecurityGroup.view.SecurityGroupGridViewController',
    'Ext.selection.CheckboxModel'], 
    controller: 'securityGroupGridViewController',
          
    store:'InformationServices.sub.SystemAdminSecuritySecurityGroup.store.GroupListStore',       

    //below are default items needed for grid checking 
   firstLoadItemId: 'sec_group', 
    mainPanelAlias: 'SystemAdminSecuritySecurityGroup', //widget alias name of the main panel
    mainListModel: 'InformationServices.sub.SystemAdminSecuritySecurityGroup.model.GroupListModel',     
    
    addFormAliasName: 'addSecurityGroupForm', //widget form to call for add list 

   firstFocus: 'sec_group',    
    
    //configuration for add button
    addButtonTooltip: 'Add New Security Group',
    addButtonText: 'Add New Security Group',
   
    //cofiguration for combo in grid tool
    gridToolComboStore: 'InformationServices.sub.SystemAdminSecuritySecurityGroup.store.SecGroupStore',
    gridToolComboFieldLabel: 'Security Group',
    gridToolComboEmptyText: 'Choose Security Group',
        
    columns:[{
        header: 'Security Group',
        flex     : 1, 
        sortable : true,
        dataIndex: 'groupName',
        itemId: 'sec_group',
        filterElement: new Ext.form.TextField()
          
    },{   
        header: 'Application',
        flex     : 1,
        dataIndex: 'appName',
        filterElement:new Ext.form.TextField()
    },{     
        header: 'Subsystem',
        flex     : 1,
        dataIndex: 'subSystemNameDisplay',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Function',
        flex     : 1,
        dataIndex: 'functionNameDisplay',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Security Code',
        flex     : 1,
        dataIndex: 'securityCd',
        filterElement:new Ext.form.TextField(),
        editor:{
            xtype: 'combo',
            typeAhead:true,
            store: [['0','No Access'],['1','Read Only'],['2','Read/Write']],
//            name: 'security_cd_new',
//            store: 'InformationServices.sub.SystemAdminSecuritySecurityGroup.store.CodeTypeStore',
//            displayField: 'description',
//            valueField: 'idCodeType',
            listeners: {
                focus: function (cb) {
                   cb.expand();
                }
            }
        },
        renderer: function(value){
            if(value == "No Access" || value == "0"){
                return 'No Access <img src="'+InformationServices.config.Runtime.securityGroupEditSecurityCodeIcon+'" style="float:right" title="'+InformationServices.config.Runtime.securityGroupEditSecurityCodeTitle+'">';
            }else if(value == "Read Only" || value == "1"){
                return 'Read Only <img src="'+InformationServices.config.Runtime.securityGroupEditSecurityCodeIcon+'" style="float:right" title="'+InformationServices.config.Runtime.securityGroupEditSecurityCodeTitle+'">';
            }else if(value == "Read/Write" || value == "2"){
                return 'Read/Write <img src="'+InformationServices.config.Runtime.securityGroupEditSecurityCodeIcon+'" style="float:right" title="'+InformationServices.config.Runtime.securityGroupEditSecurityCodeTitle+'">';
            }
        }         
    }],

    listeners: {
        afterrender: function(){
            var me = this;
            var comboSelection = me.down('#comboSelectionItemId');
            var comboSelectionStore = comboSelection.getStore();
            comboSelectionStore.load({
                callback: function(){
                    comboSelection.select(comboSelectionStore.getData().getAt(0));
                }
            })
        }
    },

 
    initComponent: function () {  
        var me = this;
        
        me.selModel= Ext.create('Ext.selection.CheckboxModel',{
            checkOnly: true 
        }), 
        
        me.plugins= [Ext.create('Ext.ux.grid.FilterRow'),
            Ext.create('Ext.grid.plugin.CellEditing', {
                clicksToEdit: 1  
        })]
    
        var deleteGroupButton = {
                xtype: 'button',
                tooltip: InformationServices.config.Runtime.securityGroupDeleteBtnToolTip,
                text: InformationServices.config.Runtime.securityGroupDeleteBtnText,
                iconCls: InformationServices.config.Runtime.deleteCls,
                listeners: {
                    click: 'onDeleteGroup',
                    afterrender: function(){
                        var me = this
                        me.setHidden(InformationServices.config.Runtime.access_level)
                    }
                }
            }
        
        var editGroupButton = {
                xtype: 'button',
                tooltip: InformationServices.config.Runtime.securityGroupEditBtnToolTip,
                text: InformationServices.config.Runtime.securityGroupEditBtnText,
                iconCls: InformationServices.config.Runtime.securityGroupEditBtnIcon,
                listeners: {
                    click: 'onEditMultipleGroup',
                    afterrender: function(){
                        var me = this
                        me.setHidden(InformationServices.config.Runtime.access_level)
                    }
                }
            }
    
        me.tools= [
        MineralPro.config.util.MainGridToolCmpt.comboSelection,
        MineralPro.config.util.MainGridToolCmpt.addNewDataButton,
       // deleteGroupButton,        
        editGroupButton, 
        MineralPro.config.util.MainGridToolCmpt.saveToDbButton,
        MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
   
        me.callParent(arguments);
    },
    
    
});