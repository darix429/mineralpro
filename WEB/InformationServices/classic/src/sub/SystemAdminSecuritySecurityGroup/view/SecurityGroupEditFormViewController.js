//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('InformationServices.sub.SystemAdminSecuritySecurityGroup.view.SecurityGroupEditFormViewController', {
    extend : 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.securityGroupEditFormViewController',
    
    onEditMultipleGroupToGrid: function(button){
         MineralPro.config.RuntimeUtility.DebugLog('Entering SecurityGroupEditFormViewController onEditAllProfitToGrid.')
        var me = this;
        
        var win = button.up('window');
        var form = win.down('form');  
      
        if(form.isValid()){
            var view = me.getView();                      
            var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);                             
            var selected = listGrid.getSelectionModel().selected            
            
            for(var i=0; i<selected.length; i++){                           
                selected.items[i].set('securityCd', form.getValues().securityCd)
            }          
            win.close();                 
        }else {
             Ext.Msg.show({
                iconCls: InformationServices.config.Runtime.addEditToGridMsgErrorIcon,
                title: InformationServices.config.Runtime.addEditToGridMsgErrorTitle,    
                msg: InformationServices.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        }     
        MineralPro.config.RuntimeUtility.DebugLog('Leaving SecurityGroupEditFormViewController onEditAllProfitToGrid.')
    }
  
});