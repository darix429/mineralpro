Ext.define('InformationServices.sub.SystemAdminSecuritySecurityGroup.view.SecurityGroupEditFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    alias: 'widget.editSecurityGroup',
    title: 'Update Checked Group',
    defaultFocus: '#secuityComboId',
    requires: ['InformationServices.sub.SystemAdminSecuritySecurityGroup.view.SecurityGroupEditFormViewController'],
    controller: 'securityGroupEditFormViewController',
    saveAction: 'onEditMultipleGroupToGrid',
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'SystemAdminSecuritySecurityGroupId', //id of the main panel
    listGridStore: 'InformationServices.sub.SystemAdminSecuritySecurityGroup.store.GroupListStore', //store for note grid
    listGridAlias: 'securityGroupGrid', //alias name for note gridview

    initComponent: function () {
        var me = this;
        me.items = [{
                xtype: 'form',
                bodyPadding: 5,
                items: [{
                        xtype: 'fieldset',
                        defaults: {maskRe: /[^'\^]/},
                        title: 'Update Security Code',
                        defaultType: 'textfield',
                        layout: 'anchor',
                        items: [{
                                allowBlank: false,
                                xtype: 'combo',
                                itemId: 'secuityComboId',
                                typeAhead: true,
                                selectOnFocus: true,
                                tabIndex: 1,
                                name: 'securityCd',
                                store: [['0', 'No Access'], ['1', 'Read Only'], ['2', 'Read/Write']],
//                    store: 'InformationServices.sub.SystemAdminSecuritySecurityGroup.store.CodeTypeStore',
//                    displayField: 'description',
//                    valueField: 'idCodeType'
                            }]
                    }]
            }];
        this.callParent(arguments);
    },
    buttons: [{
            xtype: 'button',
            text: 'Change in Grid',
            itemId: 'saveId',
            listeners: {
                click: 'onEditMultipleGroupToGrid'
            }
        },
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});