//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('InformationServices.sub.SystemAdminSecuritySecurityGroup.view.SecurityGroupGridViewController', {
    extend : 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.securityGroupGridViewController',
    
    onDeleteGroup: function(){
        var me = this;
        var listGrid = this.getView()
        
        if(listGrid.dockedItems.items[0].items.items[1].value){
        Ext.Msg.show({
            iconCls: InformationServices.config.Runtime.securityGroupDeleteConfirmationIcon,
            title: InformationServices.config.Runtime.securityGroupDeleteConfirmationTitle,    
            msg: InformationServices.config.Runtime.securityGroupDeleteConfirmationMsg
                +listGrid.dockedItems.items[0].items.items[1].rawValue+'?',
            buttons: Ext.Msg.YESNO,
            scope: this,
            width: 250,
            fn: function(btn) {
                if (btn === 'yes') { 
                    Ext.Ajax.request({
                        url: '/api/InformationService/SystemAdminSecuritySecurityGroup/UpdateGroupList',
                        method: 'POST',
                        params: {
                            groupName: listGrid.dockedItems.items[0].items.items[1].value 
                        },
                        success: function(){
                            listGrid.dockedItems.items[0].items.items[1].setValue('')
                            Ext.StoreMgr.lookup(listGrid.gridToolComboStore).reload();                            
                            listGrid.getStore().reload();
                        }
                    });                                                                                 
                }
            }
        })
        }else{
            Ext.Msg.show({
                iconCls: InformationServices.config.Runtime.securityGroupDeleteAlertIcon,
                title: InformationServices.config.Runtime.securityGroupDeleteAlertTitle,    
                msg: InformationServices.config.Runtime.securityGroupDeleteAlertMsg,               
                buttons: Ext.Msg.OK
            });
        }
    },
     onEditMultipleGroup: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering SecurityGroupGridViewController onEditMultipleGroup.')
        var me = this;
        var selected = me.getView().getSelectionModel().selected    
        if(selected.length>0){       
            var view = Ext.widget('editSecurityGroup')
            var record = Ext.create('InformationServices.sub.SystemAdminSecuritySecurityGroup.model.GroupListModel')
            view.down('form').loadRecord(record);
            view.show();
        }else{
            Ext.Msg.show({
                iconCls: InformationServices.config.Runtime.multipleEditAlertIcon,
                title: InformationServices.config.Runtime.multipleEditAlertTitle,    
                msg: InformationServices.config.Runtime.multipleEditAlertMsg,                
                buttons: Ext.Msg.OK
            });
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving SecurityGroupGridViewController onEditMultipleGroup.')
    }, /**
     * Sync the client grid data to database (add, edit, delete)
     */
    onSyncListGridData: function () {
        MineralPro.config.RuntimeUtility.DebugLog('Entering SecurityGroupGridViewController onSyncListGridData.')
        var me = this;       
        var view = me.getView();
        var listStore = view.getStore();
                                  
        var dirty = listStore.getNewRecords().length > 0
        || listStore.getUpdatedRecords().length > 0
        || listStore.getRemovedRecords().length > 0;
        if (dirty) {
            Ext.Msg.show({
                title: InformationServices.config.Runtime.syncGridMsgConfimationTitle,
                msg: InformationServices.config.Runtime.syncGridMsgConfimationMsg,
                iconCls: InformationServices.config.Runtime.syncGridMsgConfimationIcon,
                buttons: Ext.Msg.YESNO,
                scope: this,
                width: 250,
                fn: function (btn) {
                    if (btn === 'yes') {
                        listStore.getProxy().extraParams = {
                            createLength: listStore.getNewRecords().length,
                            updateLength: listStore.getUpdatedRecords().length
                        };

                        listStore.sync({
                            callback: function (records, operation, success) {                                
                            }
                        });
                        var selectedgrid = view.getSelectionModel().getSelection();
                        listStore.commitChanges();
                        view.getSelectionModel().select(selectedgrid, false, false);
                    }
                }
            });
        } else {
            Ext.Msg.show({
                iconCls: InformationServices.config.Runtime.listSyncMsgErrorIcon,
                title: InformationServices.config.Runtime.listSyncMsgErrorTitle,
                msg: InformationServices.config.Runtime.listSyncMsgErrorMsg,
                buttons: Ext.Msg.OK
            });                      
        }                         
        MineralPro.config.RuntimeUtility.DebugLog('Leaving SecurityGroupGridViewController onSyncListGridData.')
    }
});