//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('InformationServices.sub.SystemAdminSecuritySecurityGroup.view.SecurityGroupAddFormViewController', {
    extend: 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.securityGroupAddFormViewController',
    onAddAndNew: function (button) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering SecurityGroupAddFormViewController onAddListToGrid.')
        var me = this;
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore);
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();
        var secstore = Ext.StoreMgr.lookup(view.gridToolComboStore);

        var val = view.down('#securityId').getValue();
        if (secstore.findRecord('groupName', val, 0, false, false, true)) {
            Ext.Msg.show({
                iconCls: InformationServices.config.Runtime.addExistingDataWarningCls,
                title: InformationServices.config.Runtime.addExistingDataWarningTitle,
                msg: InformationServices.config.Runtime.addExistingDataWarningMsg,
                buttons: Ext.Msg.OK,
                fn: function () {
                    button.focus();
                }
            });

        } else {
            if (form.isValid()) {
                record.set('rowUpdateUserid', InformationServices.config.Runtime.idAppUser)
                record.set(values);
                listStore.insert(0, record);

                listStore.getProxy().extraParams = {
                    createLength: listStore.getNewRecords().length,
                };

                listStore.sync({
                    callback: function (records, operation, success) {
                        listStore.reload({
                            callback: function (records, operation, success) {                               
                                secstore.reload({
                                    callback: function(){
                                        listGrid.dockedItems.items[0].items.items[1].setValue(record.get('groupName'));
                                    }
                                })
                            }
                        });                        
                    }
                });
               listStore.commitChanges();

                var rec = Ext.create(listGrid.mainListModel);   
                form.loadRecord(rec);
            } else {
                Ext.Msg.show({
                    iconCls: InformationServices.config.Runtime.addEditToGridMsgErrorIcon,
                    title: InformationServices.config.Runtime.addEditToGridMsgErrorTitle,
                    msg: InformationServices.config.Runtime.addEditToGridMsgErrorMsg,
                    buttons: Ext.Msg.OK
                });
            }
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving SecurityGroupAddFormViewController onAddListToGrid.')
    },
    onAddListToGrid: function (button) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering SecurityGroupAddFormViewController onAddListToGrid.')
        var me = this;
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore);
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();
        var secstore = Ext.StoreMgr.lookup(view.gridToolComboStore);

        var val = view.down('#securityId').getValue();
        if (secstore.findRecord('groupName', val, 0, false, false, true)) {

            Ext.Msg.show({
                iconCls: InformationServices.config.Runtime.addExistingDataWarningCls,
                title: InformationServices.config.Runtime.addExistingDataWarningTitle,
                msg: InformationServices.config.Runtime.addExistingDataWarningMsg,
                buttons: Ext.Msg.OK,
                fn: function () {
                    button.focus();
                }
            });

        } else {

            if (form.isValid()) {
                record.set('rowUpdateUserid', InformationServices.config.Runtime.idAppUser)
                record.set(values);
                win.close();
                listStore.insert(0, record);

                listGrid.getSelectionModel().select(0, false, true);

                listStore.getProxy().extraParams = {
                    createLength: listStore.getNewRecords().length,
                    updateLength: listStore.getUpdatedRecords().length
                };

                listStore.sync({
                    callback: function (records, operation, success) {
                        listStore.reload({
                            callback: function (records, operation, success) {   
                                secstore.reload({
                                    callback: function(){
                                        listGrid.dockedItems.items[0].items.items[1].setValue(record.get('groupName'));                                       
                                    }
                                })                               
                            }
                        });
                    }
                });
                listStore.commitChanges();

                listGrid.fireEvent('afterlayout');
            } else {
                Ext.Msg.show({
                    iconCls: InformationServices.config.Runtime.addEditToGridMsgErrorIcon,
                    title: InformationServices.config.Runtime.addEditToGridMsgErrorTitle,
                    msg: InformationServices.config.Runtime.addEditToGridMsgErrorMsg,
                    buttons: Ext.Msg.OK
                });
            }
        }
        MineralPro.config.RuntimeUtility.DebugLog('Leaving SecurityGroupAddFormViewController onAddListToGrid.')
    }
});