Ext.define('InformationServices.sub.SystemAdminSecuritySecurityGroup.view.SecurityGroupAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    
    requires: ['InformationServices.sub.SystemAdminSecuritySecurityGroup.view.SecurityGroupAddFormViewController'],
    controller: 'securityGroupAddFormViewController',
    modal: true,
    alias: 'widget.addSecurityGroupForm',
    title: 'Add New Security Group',
    defaultFocus : '#securityId',
      
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'SystemAdminSecuritySecurityGroupId', //id of the main panel
    listGridStore:'InformationServices.sub.SystemAdminSecuritySecurityGroup.store.GroupListStore', //store for note grid
    listGridAlias: 'securityGroupGrid', //alias name for note gridview
    gridToolComboStore: 'InformationServices.sub.SystemAdminSecuritySecurityGroup.store.SecGroupStore',
    
    initComponent: function() {
        var me= this;
        me.items = [{
            xtype: 'form',
            bodyPadding: 5,
            items: [{
                xtype:'fieldset',
                defaults:{ maskRe: /[^'\^]/ },
                title: 'Security Information',
                defaultType: 'textfield',
                layout: 'anchor',
    
                items :[           
                {
                    fieldLabel: 'Security Group',
                    xtype: 'textfield',
                    selectOnFocus: true,
                    tabIndex: 2,
                    name: 'groupName', 
                    allowBlank: false,
                    itemId: 'securityId',
                    labelAlign: 'right',
                    msgTarget: 'side'
                } ]
            }]        
        }];

        this.callParent(arguments);
    },
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
    MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});