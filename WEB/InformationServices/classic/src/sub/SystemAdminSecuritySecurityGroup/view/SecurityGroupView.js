Ext.define('InformationServices.sub.SystemAdminSecuritySecurityGroup.view.SecurityGroupView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.SystemAdminSecuritySecurityGroup',
    layout:'border',
    
    //need as unique identifier for each subfunction
    id: 'SystemAdminSecuritySecurityGroupId',
        
    items: [{
        xtype:'securityGroupGrid',
        html:'center',
        region:'center'
    }]
});    