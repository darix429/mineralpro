Ext.define('InformationServices.sub.OrganizationOrganizationList.view.OrganizationListView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.OrganizationOrganizationList',
    layout:'border',
    
    id: 'OrganizationOrganizationListId',
    
    items: [{
        xtype:'organizationListGridView',
        html:'center',
        region:'center'
    }]
});    