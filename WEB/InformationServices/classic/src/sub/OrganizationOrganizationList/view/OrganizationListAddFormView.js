Ext.define('InformationServices.sub.OrganizationOrganizationList.view.OrganizationListAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['InformationServices.sub.OrganizationOrganizationList.view.OrganizationListAddFormViewController'],
    controller: 'organizationListAddFormViewController',
    alias: 'widget.organizationListAddFormView',
    title: 'Add New Organization',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#organazationNameId',
    width: 350,
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'OrganizationOrganizationListId', //id of the main panel
    listGridStore: 'InformationServices.sub.OrganizationOrganizationList.store.OrganizationListStore', //store for note grid
    listGridAlias: 'organizationListGridView', //alias name for note gridview

    initComponent: function () {
        var me = this; me.items = [{
            xtype: 'form',
            defaults:{ 
                maskRe: /[^'\^]/,
                labelWidth: 130,
            },
            bodyPadding:5,
            items: [{          
                fieldLabel: 'Organization Name',               
                selectOnFocus: true,
                tabIndex: 1,
                itemId: 'organazationNameId',
                name: 'organizationName',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
                //enableKeyEvents: true,
                listeners: {
                    blur: 'onAddCheckExisting'
                }
            },{
                fieldLabel: 'Organization Number',
                selectOnFocus: true,
                tabIndex: 2,
                name: 'organizationNumber',
                xtype: 'numberfield',
                minValue: '0',
                maxValue: '2147483647',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
                enableKeyEvents: true,
                listeners: {
                    keyup: 'onAddCheckExisting'
                }
            },{
                fieldLabel: 'Status',
                xtype: 'combo',
                selectOnFocus: true,
                tabIndex: 3,
                itemId: 'StatusItemId',
                displayField: 'description',
                valueField: 'code',
                name: 'code',
                typeAhead: true,
                store: 'InformationServices.sub.OrganizationOrganizationContact.store.StatusCdStore',
                forceSelection: true,
                allowBlank: false,
                labelAlign: 'right',
                msgTarget: 'side',
                queryMode: 'local',
                listeners:{
                    select:function(record){
                        me.down('#hiddenStatusItemId').setValue(me.down('#StatusItemId').getRawValue());
                    },   
                    beforequery: function(queryVV){
                        queryVV.combo.expand();
                        queryVV.combo.store.load();
                        return false;
                    }
                },    
            },{
                fieldLabel: 'Hidden Status',
                queryMode:'local',
                name: 'description',
                xtype: 'hidden',
                readOnly:true,
                margin: '0 0 10',
                itemId:'hiddenStatusItemId',
                labelAlign: 'right',
            }],
        }];

        me.listeners = {
            afterrender: function(){
                setTimeout(function () {
                   me.down('#hiddenStatusItemId').setValue(me.down('#StatusItemId').getRawValue());
                }, 100);               
            }
        }
        me.callParent(arguments);
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
        MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});