
Ext.define('InformationServices.sub.OrganizationOrganizationList.view.OrganizationListGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.organizationListGridView',
    store:'InformationServices.sub.OrganizationOrganizationList.store.OrganizationListStore',

    requires: ['InformationServices.sub.OrganizationOrganizationList.view.OrganizationListGridViewController'],
    controller: 'organizationListGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'organizationNameItemId', 
    mainPanelAlias: 'OrganizationOrganizationList', //widget alias name of the main panel
    mainListModel: 'InformationServices.sub.OrganizationOrganizationList.model.OrganizationListModel',     

//    detailAliasName: 'clientListDetails',//widget aliasName for detailView
    addFormAliasName: 'organizationListAddFormView', //widget form to call for add list 
    editFormAliasName: 'organizationListEditFormView', //widget form to call for edit
//    

    //this will be the items for header.
    //headerInfo will hold the define name for the header *Optional
//    headerInfo: 'InformationServices.sub.OrganizationOrganizationList.view.OrganizationListHeaderView',
 
    firstFocus: 'organizationNameItemId',    
    
    addButtonTooltip: 'Add New Organization',
    addButtonText: 'Add New Organization',
        
    columns:[{ 
        header: 'Organization Name',
        flex     : 1,
        dataIndex: 'organizationName',
        itemId: 'organizationNameItemId',
        filterElement: new Ext.form.TextField()
    },{   
        header: 'Organization Number',
        flex     : 1,
        dataIndex: 'organizationNumber',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Status',
        flex     : 1,
        dataIndex: 'description',
        filterElement : new Ext.form.ComboBox({                              
            showFilterIcon:true,
            triggerAction           : 'all',                
            typeAhead               : true,                                
            mode                    : 'local',
            listWidth               : 160,
            hideTrigger             : false,
            emptyText               : 'Select',
            store                   : Ext.create('InformationServices.sub.OrganizationOrganizationContact.store.StatusCdStore'),
            listeners: {
                beforerender: function(combo) {
                    combo.store.load({
                        callback: function(records, operation, success) {
                            var type = [];
                            for(i = 0;i < records.length;i++){
                                var a = [];
                                a.push(records[i].data.description)
                                a.push(records[i].data.description);
                                type.push(a);
                            }
                            combo.bindStore(type);
                            var recordSelected = combo.getStore().getAt(0);  
                            combo.setValue(recordSelected.get('field1'));
                        }
                    });
                }
            }
        }),
    },{
     xtype: 'actioncolumn',
        width: InformationServices.config.Runtime.actionColWidth,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls,
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    }],

    initComponent: function () {  
        var me = this;
               
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],       
               
        me.tools= [
        MineralPro.config.util.MainGridToolCmpt.addNewDataButton
        ,MineralPro.config.util.MainGridToolCmpt.saveToDbButton                   
        ,MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
        me.callParent(arguments);
        
    }    
});