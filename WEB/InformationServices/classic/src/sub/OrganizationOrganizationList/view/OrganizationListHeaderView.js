Ext.define('InformationServices.sub.OrganizationOrganizationList.view.OrganizationListHeaderView' ,{
    extend: 'MineralPro.config.view.BaseHeaderView',
    alias : 'widget.OrganizationOrganizationListHeaderView',  

    defaults: {
        xtype: 'container',
        layout: 'hbox',
        width: '100%',
        height: 30,
    },
    items: [{
        defaults: {
            xtype: 'displayfield',
            labelStyle: 'font-weight:bold;',
            width: '33%',
        },               
        items:[{
            fieldLabel: 'Organization Name',
            labelWidth: 150,
            name: 'organizationName',
        },{
            fieldLabel: 'Organization Number',
            labelWidth: 150,
            name: 'organizationNumber',
        },{
            fieldLabel: 'Status',
            labelWidth: 150,
            name: 'statusCd',    
            listeners:{
                render: function(status){
                     if(status.value == 1){
                        status.setValue('Active');
                    }else if(status.value == 2){
                        status.setValue('Inactive');
                    }
                }
            }
        }]
    }]      
});    