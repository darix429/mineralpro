Ext.define('InformationServices.sub.OrganizationOrganizationList.view.OrganizationListEditFormView', {
    extend: 'InformationServices.sub.OrganizationOrganizationList.view.OrganizationListAddFormView',
    requires: ['InformationServices.sub.OrganizationOrganizationList.view.OrganizationListEditFormViewController'],
    controller: 'organizationListEditFormViewController',
    alias: 'widget.organizationListEditFormView',
    
    
    title: 'Edit Organization',
    defaultFocus: '#organazationNameId',
    editRecordIndex: 0,
    
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});