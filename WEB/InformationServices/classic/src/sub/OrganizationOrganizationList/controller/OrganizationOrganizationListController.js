///**
// * Handles the controller processing for Client List
// */

Ext.define('InformationServices.sub.OrganizationOrganizationList.controller.OrganizationOrganizationListController', {
    extend: 'Ext.app.Controller',
  
    models: [
        'InformationServices.sub.OrganizationOrganizationList.model.OrganizationListModel'
    ],
    
    stores: [
        'InformationServices.sub.OrganizationOrganizationList.store.OrganizationListStore',
        'InformationServices.sub.OrganizationOrganizationContact.store.StatusCdStore'
    ],  
    
    views: [
        'InformationServices.sub.OrganizationOrganizationList.view.OrganizationListView',
        'InformationServices.sub.OrganizationOrganizationList.view.OrganizationListGridView',
        'InformationServices.sub.OrganizationOrganizationList.view.OrganizationListAddFormView',
        'InformationServices.sub.OrganizationOrganizationList.view.OrganizationListEditFormView',
        'InformationServices.sub.OrganizationOrganizationList.view.OrganizationListHeaderView'
    ],          
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden']
});


