Ext.define('InformationServices.sub.ReportsExportsPublicExports.controller.ReportsExportsPublicExportsController', {
    extend: 'Ext.app.Controller',
  
    models: [
        'InformationServices.sub.ReportsExportsPublicExports.model.PublicExportsModel',
    ],
    
    stores: [
        'InformationServices.sub.ReportsExportsPublicExports.store.PublicExportsStore',
    ],  
    
    views: [
        'InformationServices.sub.ReportsExportsPublicExports.view.PublicExportsFormView',
        'InformationServices.sub.ReportsExportsPublicExports.view.PublicExportsView',
        'InformationServices.sub.ReportsExportsPublicExports.view.PublicExportsGridView',
    ], 
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden']
});