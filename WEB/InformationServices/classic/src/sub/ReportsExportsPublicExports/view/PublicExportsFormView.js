var loadingPanel = Ext.widget('panel', {                      
    floating: true,
    html: '<div style="height: 44px; padding: 12px;color:white;background-color: #32404e;"><img src="../resources/images/spinner2.gif" style="width:20px;height:20px;margin-right:10px;float:left;"><p style="margin-top: 4px;float:left">Download in progress.<p></div>', 
    shadow: false,
    hideBorder: false,
});


Ext.define('InformationServices.sub.ReportsExportsPublicExports.view.PublicExportsFormView', {
    extend: 'Ext.window.Window',
    
    alias: 'widget.PublicExportsFormView',
    width: 320,
    modal: true,
    
    mainPanelId: 'ReportsExportsPublicExportsId', //id of the main panel
    listGridStore:'InformationServices.sub.ReportsExportsPublicExports.store.PublicExportsStore', //store for note grid
    listGridAlias: 'PublicExportsGridView', //alias name for note gridview

    initComponent: function() {
        var me = this;
        var target = "";
        
        
        console.log(loadingPanel.isVisible())
    
        me.items = [{
            xtype: 'form',
            defaults:{ 
                maskRe: /[^'\^]/,
                labelWidth: 130,
            },
            padding: '15 15 15 15',
            items: [{
                    xtype : 'hidden',
                    name: 'url',
                    itemId: 'url',
                    queryMode:'local',
                    tabIndex: 10,
                },{
                    xtype: 'container',
                    itemId: 'progressContainer',
                    hidden: true,
                    layout: {
                        type: "hbox",
                        pack: "center",
                        align: "center"
                    },
                    items: [{
                        itemId: 'progressLabel',
                        xtype : 'label',
                        text: 'Please wait, it may take a few minutes...'
                    }]
                },{
                    xtype: 'container',
                    hidden: true,
                    itemId: 'closeButton',
                    margin: '15 0 0',
                    layout: {
                        type: "vbox",
                        pack: "center",
                        align: "center"
                    },
                    items: [{
                        xtype  : "button",
                        text: 'Ok',
                        width: 90,
                        listeners: {
                            click: function(item) {
                                me.close();
                                
                                
                            }
                        }
                    }],
                    
                },{
                    fieldLabel: 'Appraisal Year',
                    selectOnFocus: true,
                    tabIndex: 1,
                    labelWidth: 100,
                    name : 'year',
                    xtype:'combo',
                    itemId:'year',
                    labelAlign: 'right',
                    margin: '0 0 15 0',  
                    displayField: 'appraisalYear',
                    valueField: 'appraisalYear',
                    queryMode: 'local',
                    store: 'MineralPro.store.AppraisalYearStore',
                    typeAhead: true,
                    queryMode:'local',
                    listeners:{
                        beforerender: function(combo) {
                            combo.setValue(combo.getStore().data.items[0]);
                        }
                    }, 
                    forceSelection: true,
                    msgTarget: 'side' 
                },{
                    xtype: 'container',
                    itemId: 'button',
                    layout: {
                        type: "vbox",
                        pack: "center",
                        align: "center"
                    },
                    items: [{
                        xtype  : "button",
                        text: 'Export',
                        itemId: 'download',
                        listeners: {
                            click: function(item) {
								target = me.down('#url').getValue() + "?appraisal_year="+ me.down('#year').getValue();

                                var str = me.down('#url').getValue();
                                var n = str.lastIndexOf('/');
                                var result = str.substring(n + 1);

                                if(result == "ExportCertifiedFormat" || result == "ExportCertifiedFormatSummary"){
                                    me.down('#closeButton').show()
                                    me.down('#year').hide()
                                    me.down('#button').hide()
                                    me.down('#progressContainer').show()
                                    me.tools.close.hide()
                                }else{
                                    me.close();
                                }

                                loadingPanel.show().alignTo(Ext.getBody(), 'br-br', [-8, -8]);
								exportTXT(target, me, loadingPanel);
                            }
                        }
                    }],
                    
                }],
        }];

        me.listeners = {
            beforerender: function() {
                if(loadingPanel.isVisible()){
                    me.down('#closeButton').show()
                    me.down('#year').hide()
                    me.down('#button').hide()
                    me.down('#progressContainer').show()
                }
            }
        } 
        
        me.callParent(arguments);
    },
    
});

function exportTXT(url, dialog, loadingPanel){
	Ext.Ajax.setTimeout(9999999);
	Ext.Ajax.request({
		url: url,
		method: "get",
		success: function(res,opts){
			var file = Ext.decode(res.responseText);
			window.open("/api/download/"+file.filename+"/"+file.path);
            loadingPanel.hide();
            dialog.close();
		},
		failure: function(res,opts){
            loadingPanel.hide();
            dialog.close();
            Ext.Msg.alert('Error', 'Download Failed!.', Ext.emptyFn);
		}
	})
}