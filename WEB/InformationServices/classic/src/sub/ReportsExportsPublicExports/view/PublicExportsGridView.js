Ext.define('InformationServices.sub.ReportsExportsPublicExports.view.PublicExportsGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.PublicExportsGridView',
    store: 'InformationServices.sub.ReportsExportsPublicExports.store.PublicExportsStore',

    requires: ['InformationServices.sub.ReportsExportsPublicExports.view.PublicExportsGridViewController'],
    controller: 'PublicExportsGridViewController',     
   
    mainPanelAlias: 'ReportsExportsPublicExports', //widget alias name of the main panel
    mainListModel: 'InformationServices.sub.ReportsExportsPublicExports.model.PublicExportsModel',     
    style: 'position:relative!important;',

    initComponent: function() {
        var me = this;
        me.columns = [{ 
            itemId: 'nameCol',
            header: 'Export to Public',
            flex     : 1,
            dataIndex: 'displayName',
        }], 

        me.listeners = {
            rowclick: function(grid, record, e, rowIndex) {
                var formView = Ext.widget('PublicExportsFormView');
                formView.down('form').loadRecord(record); 
                formView.show();
            }
        } 
        
        me.callParent(arguments);
    }
});

