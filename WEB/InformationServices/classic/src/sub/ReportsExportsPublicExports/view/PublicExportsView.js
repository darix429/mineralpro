Ext.define('InformationServices.sub.ReportsExportsPublicExports.view.PublicExportsView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.ReportsExportsPublicExports',
    layout:'border',
    
    id: 'ReportsExportsPublicExportsId',
    
    items: [{
        xtype:'PublicExportsGridView',
        html:'center',
        region:'center'
    }]
});    