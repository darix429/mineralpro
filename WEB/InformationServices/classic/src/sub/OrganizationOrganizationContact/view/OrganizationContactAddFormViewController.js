//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('InformationServices.sub.OrganizationOrganizationContact.view.OrganizationContactAddFormViewController', {
    extend: 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.OrganizationContactAddFormViewController',
    
    onAddAndNew: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering OrganizationContactAddFormViewController onAddAndNew.')
        var me = this;                     
        var view = me.getView();
        var listStore = Ext.StoreMgr.lookup(view.listGridStore); 
        var listGrid = Ext.getCmp(view.mainPanelId).down(view.listGridAlias);
                
        var win = button.up('window');
        var form = win.down('form');
        var record = form.getRecord();
        var values = form.getValues();                        
        if(form.isValid()){
            record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
            record.set(values);    
            listStore.insert(0,record);
            
            listGrid.getSelectionModel().select(0, false, true);  
			
            //form.reset()          
            var new_record = Ext.create(listGrid.mainListModel)                    
            form.loadRecord(new_record);
            view.focus(view.defaultFocus);
            
            setTimeout(function () {
                view.down('#organizationNameId').setValue(record.data.idOrganization); 
                view.down('#hiddenOrganizationNameId').setValue(view.down('#organizationNameId').getRawValue()); 
                view.down('#hiddenStatusItemId').setValue(view.down('#StatusItemId').getRawValue());              
            }, 100);  
            
            //listGrid.fireEvent('afterlayout');
            
        }else {
            Ext.Msg.show({
                iconCls: InformationServices.config.Runtime.addEditToGridMsgErrorIcon,
                title: InformationServices.config.Runtime.addEditToGridMsgErrorTitle,    
                msg: InformationServices.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        }  
        MineralPro.config.RuntimeUtility.DebugLog('Leaving OrganizationContactAddFormViewController onAddAndNew.')
    },
});