
Ext.define('InformationServices.sub.OrganizationOrganizationContact.view.OrganizationContactGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.OrganizationContactGridView',
    store:'InformationServices.sub.OrganizationOrganizationContact.store.OrganizationContactStore',

    requires: ['InformationServices.sub.OrganizationOrganizationContact.view.OrganizationContactGridViewController'],
    controller: 'OrganizationContactGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'organizationNameItemId', 
    mainPanelAlias: 'OrganizationOrganizationContact', //widget alias name of the main panel
    mainListModel: 'InformationServices.sub.OrganizationOrganizationContact.model.OrganizationContactModel',     

    
//    detailAliasName: 'clientListDetails',//widget aliasName for detailView
    addFormAliasName: 'OrganizationContactAddFormView', //widget form to call for add list 
    editFormAliasName: 'OrganizationContactEditFormView', //widget form to call for edit
//    

    firstFocus: 'organizationNameItemId',    
    
    addButtonTooltip: 'Add New Organization Contact',
    addButtonText: 'Add New Organization Contact',
        
    columns:[{ 
        header: 'Organization Name',
        flex     : 1,
        dataIndex: 'organizationName',
        itemId: 'organizationNameItemId',
        filterElement: new Ext.form.TextField()
    },{   
        header: 'First Name',
        flex     : 1,
        dataIndex: 'firstName',
        filterElement:new Ext.form.TextField()
    },{ 
        header: 'Last Name',
        flex     : 1,
        dataIndex: 'lastName',
        filterElement:new Ext.form.TextField()
    },{ 
        header: 'Middle Initial',
        flex     : 1,
        dataIndex: 'middleInitial',
        filterElement:new Ext.form.TextField()
    },{     
        header: 'Abbreviation Name',
        flex     : 1,
        dataIndex: 'nameAbbreviation',
        filterElement:new Ext.form.TextField()
    },{ 
        header: 'Contact Number',
        flex     : 1,
        dataIndex: 'contactNumber',
        filterElement:new Ext.form.TextField()    
    },{ 
        header: 'Title',
        flex     : 1,
        dataIndex: 'title',
        filterElement:new Ext.form.TextField()
    },{ 
        header: 'Status',
        flex     : 1,
        itemId: 'status',
        dataIndex: 'description',
        filterElement : new Ext.form.ComboBox({                              
            showFilterIcon:true,
            triggerAction           : 'all',                
            typeAhead               : true,                                
            mode                    : 'local',
            listWidth               : 160,
            hideTrigger             : false,
            emptyText               : 'Select',
            store                   : Ext.create('InformationServices.sub.OrganizationOrganizationContact.store.StatusCdStore'),
            listeners: {
                beforerender: function(combo) {
                    combo.store.load({
                        callback: function(records, operation, success) {
                            var type = [];
                            for(i = 0;i < records.length;i++){
                                var a = [];
                                a.push(records[i].data.description)
                                a.push(records[i].data.description);
                                type.push(a);
                            }
                            combo.bindStore(type);
                            var recordSelected = combo.getStore().getAt(0);  
                            combo.setValue(recordSelected.get('field1'));
                        }
                    });
                }
            }
        }),
    },{
     xtype: 'actioncolumn',
        width: InformationServices.config.Runtime.actionColWidth,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls,
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    }],
 
    initComponent: function () {  
        var me = this;
        
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],
        
        me.tools= [
        {
            xtype: 'button',
            itemId: 'addNewDataButtonItemId',
            iconCls:MineralPro.config.Runtime.addNewBtnIconCls,
            listeners: {
                click: function() {
                    me.down('#status').config.filterElement.setValue('')
                    var record = Ext.create('InformationServices.sub.OrganizationOrganizationContact.model.OrganizationContactModel');      
                    var addView = Ext.widget('OrganizationContactAddFormView');                    
                    record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                    addView.down('form').loadRecord(record);
                    addView.show()
                },
                // click: 'onOpenAddListForm',
                beforerender: function () {
                    var me = this;
                    me.tooltip = me.up('grid').addButtonTooltip
                    me.text = me.up('grid').addButtonText
                },
                afterrender: function(){
                    var me = this
                        me.setHidden(MineralPro.config.Runtime.access_level)
                }
            }
        }
        ,MineralPro.config.util.MainGridToolCmpt.saveToDbButton                   
        ,MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
        me.callParent(arguments);
        
    }    
});