Ext.define('InformationServices.sub.OrganizationOrganizationContact.view.OrganizationContactEditFormView', {
    extend: 'InformationServices.sub.OrganizationOrganizationContact.view.OrganizationContactAddFormView',
    requires: ['InformationServices.sub.OrganizationOrganizationContact.view.OrganizationContactEditFormViewController'],
    controller: 'OrganizationContactEditFormViewController',
    alias: 'widget.OrganizationContactEditFormView',
    
    
    title: 'Edit Organization Contact',
    defaultFocus: '#firstNameId',
    editRecordIndex: 0,
    
    listeners: {
        afterrender: function(){
            var me = this;
            var organizationNameId = me.down('#organizationNameId');
                organizationNameId.disable(true);
        }
    },

    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
    MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});