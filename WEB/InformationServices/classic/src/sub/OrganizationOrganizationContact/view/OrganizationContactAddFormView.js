Ext.define('InformationServices.sub.OrganizationOrganizationContact.view.OrganizationContactAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['InformationServices.sub.OrganizationOrganizationContact.view.OrganizationContactAddFormViewController'],
    controller: 'OrganizationContactAddFormViewController',
    alias: 'widget.OrganizationContactAddFormView',
    title: 'Add New Organization Contact',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#organizationNameId',
    width: 350,
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'OrganizationOrganizationContactId', //id of the main panel
    listGridStore: 'InformationServices.sub.OrganizationOrganizationContact.store.OrganizationContactStore', //store for note grid
    listGridAlias: 'OrganizationContactGridView', //alias name for note gridview

    initComponent: function () {
        var me = this; me.items = [{
            xtype: 'form',
            defaults:{ 
                maskRe: /[^'\^]/,
                labelWidth: 130,
            },
            bodyPadding:5,
            items: [{                                                                 
                fieldLabel: 'Organization Name',
                xtype: 'combo',
                selectOnFocus: true,
                tabIndex: 1,
                itemId: 'organizationNameId',
                displayField: 'organizationName',
                valueField: 'idOrganization',
                name: 'idOrganization',
                typeAhead: true,
                store: 'InformationServices.sub.OrganizationOrganizationContact.store.OrganizationStore',
                forceSelection: true,
                allowBlank: false,
                labelAlign: 'right',
                msgTarget: 'side',
                queryMode: 'local',
                listeners:{
                    select:function(record){
                        me.down('#hiddenOrganizationNameId').setValue(me.down('#organizationNameId').getRawValue());
                    },   
                    beforequery: function(queryVV){
                        queryVV.combo.expand();
                        queryVV.combo.store.load();
                        return false;
                    }
                },
                
            },{
                fieldLabel: 'First Name',
                selectOnFocus: true,
                tabIndex: 2,
                itemId: 'firstNameId',
                name: 'firstName',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,
            },{
                fieldLabel: 'Last Name',
                selectOnFocus: true,
                tabIndex: 3,
                name: 'lastName',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false,    
            },{
                fieldLabel: 'Middle Initial',
                selectOnFocus: true,
                tabIndex: 4,
                name: 'middleInitial',
                xtype: 'textfield',
                maxLength: 1,
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: true, 
            },{
                fieldLabel: 'Abbreviation Name',
                selectOnFocus: true,
                tabIndex: 5,
                name: 'nameAbbreviation',
                xtype: 'textfield',               
                margin: '0 0 10',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false, 
                listeners: {
                    blur: 'onAddCheckExisting'
                },
                validator: function(value) {
                    if(value === null || value === undefined || value === ""){                                
                        return "You forgot to enter a Abbreviation Name.";
                    }
                    else {
                        return true;
                    } 
                }
            },{
                fieldLabel: 'Contact Number',
                selectOnFocus: true,
                tabIndex: 6,
                name: 'contactNumber',
                xtype: 'numberfield',
                maxValue: '9999',
                maxLength: '4',
                margin: '0 0 10',
                minValue: '0',
                labelAlign: 'right',
                validateBlank: true,
                allowBlank: false, 
                listeners: {
                    blur: 'onAddCheckExisting'
                }
            },{
                fieldLabel: 'Title',
                selectOnFocus: true,
                tabIndex: 7,
                name: 'title',
                xtype: 'textfield',
                margin: '0 0 10',
                labelAlign: 'right',
                //validateBlank: true,
                //allowBlank: false,    
            },{
                fieldLabel: 'Status',
                xtype: 'combo',
                selectOnFocus: true,
                tabIndex: 8,
                itemId: 'StatusItemId',
                displayField: 'description',
                valueField: 'code',
                name: 'code',
                typeAhead: true,
                store: 'InformationServices.sub.OrganizationOrganizationContact.store.StatusCdStore',
                forceSelection: true,
                allowBlank: false,
                labelAlign: 'right',
                msgTarget: 'side',
                queryMode: 'local',
                listeners:{
                    select:function(record){
                        me.down('#hiddenStatusItemId').setValue(me.down('#StatusItemId').getRawValue());
                    },   
                    beforequery: function(queryVV){
                        queryVV.combo.expand();
                        queryVV.combo.store.load();
                        return false;
                    }
                },    
            },{
                fieldLabel: 'Hidden Organization Name',
                queryMode:'local',
                name: 'organizationName',
                xtype: 'hidden',
                readOnly:true,
                margin: '0 0 10',
                itemId:'hiddenOrganizationNameId',
                labelAlign: 'right',
            },{
                fieldLabel: 'Hidden Status',
                queryMode:'local',
                name: 'description',
                xtype: 'hidden',
                readOnly:true,
                margin: '0 0 10',
                itemId:'hiddenStatusItemId',
                labelAlign: 'right',
            }],
        }];

        me.listeners = {
            afterrender: function(){
                setTimeout(function () {
                    me.down('#hiddenOrganizationNameId').setValue(me.down('#organizationNameId').getRawValue());
                    me.down('#hiddenStatusItemId').setValue(me.down('#StatusItemId').getRawValue());
                }, 100);               
            }
        }

        me.callParent(arguments);
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
        MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});
