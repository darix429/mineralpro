Ext.define('InformationServices.sub.OrganizationOrganizationContact.view.OrganizationContactView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.OrganizationOrganizationContact',
    layout:'border',
    
    id: 'OrganizationOrganizationContactId',
    
    items: [{
        xtype:'OrganizationContactGridView',
        html:'center',
        region:'center'
    }]
});    