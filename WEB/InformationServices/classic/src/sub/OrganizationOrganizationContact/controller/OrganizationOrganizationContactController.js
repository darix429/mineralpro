///**
// * Handles the controller processing for Client List
// */

Ext.define('InformationServices.sub.OrganizationOrganizationContact.controller.OrganizationOrganizationContactController', {
    extend: 'Ext.app.Controller',
  
    models: [
        'InformationServices.sub.OrganizationOrganizationContact.model.OrganizationContactModel',
    ],
    
    stores: [
        'InformationServices.sub.OrganizationOrganizationContact.store.OrganizationContactStore',
        'InformationServices.sub.OrganizationOrganizationContact.store.OrganizationStore',
        'InformationServices.sub.OrganizationOrganizationContact.store.StatusCdStore'
    ],  
    
    views: [
        'InformationServices.sub.OrganizationOrganizationContact.view.OrganizationContactView',
        'InformationServices.sub.OrganizationOrganizationContact.view.OrganizationContactGridView',
        'InformationServices.sub.OrganizationOrganizationContact.view.OrganizationContactAddFormView',
        'InformationServices.sub.OrganizationOrganizationContact.view.OrganizationContactEditFormView',
    ],          
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden']
});


