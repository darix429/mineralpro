///**
// * Handles the controller processing for Client List
// */

Ext.define('InformationServices.sub.SystemAdminSecuritySecurityUserList.controller.SystemAdminSecuritySecurityUserListController', {
    extend: 'Ext.app.Controller',
  
    models: ['InformationServices.sub.SystemAdminSecuritySecurityUserList.model.UserListModel',
    'InformationServices.sub.SystemAdminSecuritySecurityUserList.model.OrgNameModel',
    'InformationServices.sub.SystemAdminSecuritySecurityUserList.model.SecGroupModel',
    'InformationServices.sub.SystemAdminSecuritySecurityUserList.model.OrgNameModel',
    ],
    
    stores: ['InformationServices.sub.SystemAdminSecuritySecurityUserList.store.UserListStore',
    'InformationServices.sub.SystemAdminSecuritySecurityUserList.store.OrgNameStore',
    'InformationServices.sub.SystemAdminSecuritySecurityUserList.store.SecGroupStore',
    'InformationServices.sub.SystemAdminSecuritySecurityUserList.store.OrgContactStore',    
    ],  
    
    views: [
    'InformationServices.sub.SystemAdminSecuritySecurityUserList.view.SecurityUserListView',
    'InformationServices.sub.SystemAdminSecuritySecurityUserList.view.SecurityUserListGridView',
    'InformationServices.sub.SystemAdminSecuritySecurityUserList.view.SecurityUserListAddFormView',
    'InformationServices.sub.SystemAdminSecuritySecurityUserList.view.SecurityUserListEditFormView',
    ],          
    
    requires: ['Ext.form.ComboBox',
//    'Ext.ux.grid.FilterRow',
    'Ext.grid.column.Action',
    'Ext.form.FieldSet',
    'Ext.form.field.Hidden']
});


