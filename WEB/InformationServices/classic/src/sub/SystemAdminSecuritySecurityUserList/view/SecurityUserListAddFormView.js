Ext.define('InformationServices.sub.SystemAdminSecuritySecurityUserList.view.SecurityUserListAddFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['InformationServices.sub.SystemAdminSecuritySecurityUserList.view.SecurityUserListAddFormViewController'],
    controller: 'securityUserListAddFormViewController',
    alias: 'widget.addUserListForm',
    title: 'Add New User',
    saveAction: 'onAddListToGrid',
    defaultFocus: '#organizationNameId',
    width: 350,
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'SystemAdminSecuritySecurityUserListId', //id of the main panel
    listGridStore: 'InformationServices.sub.SystemAdminSecuritySecurityUserList.store.UserListStore', //store for note grid
    listGridAlias: 'securityUserListGrid', //alias name for note gridview

    initComponent: function () {
        var me = this;
        me.items = [{
                xtype: 'form',
                defaults: {
                    maskRe: /[^'\^]/,
                    labelWidth: 130,
                },
                bodyPadding: 5,
                items: [{
                        xtype: 'fieldset',
                        defaults: {maskRe: /[^'\^]/},
                        title: 'Security Information',
                        defaultType: 'textfield',
                        layout: 'anchor',
                        defaults: {
                            anchor: '100%',
                            labelWidth: 130,
                        },
                        items: [{
                                fieldLabel: 'Organization Name',
                                xtype: 'combo',
                                selectOnFocus: true,
                                tabIndex: 1,
                                itemId: 'organizationNameId',
                                displayField: 'organizationName',
                                valueField: 'idOrganization',
                                name: 'idOrganization',
                                typeAhead: true,
                                store: 'InformationServices.sub.SystemAdminSecuritySecurityUserList.store.OrgNameStore',
                                forceSelection: true,
                                allowBlank: false,
                                labelAlign: 'right',
                                msgTarget: 'side',
                                queryMode: 'local',
                                listeners: {
                                    change: function (fieldLabel, newval, oldval) {
                                        var st = Ext.getStore('InformationServices.sub.SystemAdminSecuritySecurityUserList.store.OrgContactStore');
                                        st.load({
                                            params: {
                                                idOrganization: newval
                                            },
                                            callback: function (records, operation, success) {
                                                if (st.totalCount == 0) {
                                                    Ext.Msg.show({
                                                        iconCls: InformationServices.config.Runtime.securityUserListNoContactAlertIcon,
                                                        title: InformationServices.config.Runtime.securityUserListNoContactAlertTitle,
                                                        msg: InformationServices.config.Runtime.securityUserListNoContactAlertMsg,
                                                        buttons: Ext.Msg.OK
                                                    });
                                                }else{
                                                    me.down('#idContactName').select(1);
                                                }
                                                me.down('#idcontact').setValue(me.down('#idContactName').getRawValue());

                                            }
                                        });
                                        me.down('#orgname').setValue(me.down('#organizationNameId').getRawValue());
                                    },
                                    beforequery: function(queryVV){
                                        queryVV.combo.expand();
                                        queryVV.combo.store.load();
                                        return false;
                                    }
                                }
                            }, {
                                fieldLabel: 'Organization Contact',
                                xtype: 'combo',
                                selectOnFocus: true,
                                tabIndex: 2,
                                itemId: 'idContactName',
                                displayField: 'organizationContact',
                                valueField: 'idOrganizationContact',
                                name: 'idOrganizationContact',
                                store: 'InformationServices.sub.SystemAdminSecuritySecurityUserList.store.OrgContactStore',
                                submitValue: true,
                                typeAhead: true,
                                forceSelection: true,
                                allowBlank: false,
                                queryMode: 'local',
                                labelAlign: 'right',
                                msgTarget: 'side',
                                listeners: {
                                    Select: function (record) {
                                        me.down('#statusDescription').setValue(record.valueCollection.items[0].data.description);
                                        me.down('#idcontact').setValue(me.down('#idContactName').getRawValue());
                                    },   
                                    blur: 'onAddCheckExisting'                                 
                                }
                            }, {
                                fieldLabel: 'Security Group',
                                xtype: 'combo',
                                selectOnFocus: true,
                                tabIndex: 3,
                                itemId: 'securityGroupId',
                                displayField: 'groupName',
                                valueField: 'idSecurityGroup',
                                store: 'InformationServices.sub.SystemAdminSecuritySecurityUserList.store.SecGroupStore',
                                name: 'idSecurityGroup',
                                hiddenName: 'idSecurityGroup',
                                submitValue: true,
                                typeAhead: true,
                                //forceSelection: true,
                                allowBlank: false,
                                queryMode: 'local',
                                labelAlign: 'right',
                                msgTarget: 'side',
                                listeners: {
                                    Select: function (record) {
                                        me.down('#idsec').setValue(me.down('#securityGroupId').getRawValue());
                                    },
                                    beforequery: function(queryVV){
                                        queryVV.combo.expand();
                                        queryVV.combo.store.load();
                                        return false;
                                    }
                                }
                            }, {
                                fieldLabel: 'Contact Name',
                                xtype: 'hidden',
                                name: 'organizationContact',
                                readOnly: true,
                                queryMode: 'local',
                                itemId: 'idcontact'
                            }, {
                                xtype: 'hidden',
                                name: 'description',
                                itemId: 'statusDescription'
                            }]
                    }, {
                        xtype: 'fieldset',
                        defaults: {
                            maskRe: /[^'\^]/,
                            labelWidth: 130,
                        },
                        title: 'Account Information',
                        defaultType: 'textfield',
                        layout: 'anchor',
                        defaults: {
                            anchor: '100%',
                            labelWidth: 130,
                        },
                        items: [{
                                fieldLabel: 'Email Address',
                                selectOnFocus: true,
                                tabIndex: 4,
                                name: 'emailAddress',
                                xtype: 'textfield',
                                vtype: 'email',
                                margin: '0 0 10',
                                labelAlign: 'right',
                                validateBlank: true,
                                allowBlank: false,
                                msgTarget: 'side'
                            }, {
                                fieldLabel: 'Password',
                                name: 'password',
                                selectOnFocus: true,
                                tabIndex: 5,
                                xtype: 'textfield',
                                inputType: 'password',
                                margin: '0 0 10',
                                labelAlign: 'right',
                                validateBlank: true,
                                allowBlank: false,
                                msgTarget: 'side',
                                id: 'pwd',
                                itemId: 'passwordText',
                                maxLength: 32,
                                minLength: 6,
                                minLengthText: 'Password must be at least 6 characters'
                            }, {
                                fieldLabel: 'Confirm Password',
                                name: 'confirmpassword',
                                selectOnFocus: true,
                                tabIndex: 6,
                                xtype: 'textfield',
                                inputType: 'password',
                                itemId: 'confirmPasswordText',
                                margin: '0 0 10',
                                labelAlign: 'right',
                                validateBlank: true,
                                allowBlank: false,
                                msgTarget: 'side',
                                vtype: 'password',
                                initialPassField: 'pwd',
                                maxLength: 32,
                                minLength: 6,
                                minLengthText: 'Password must be at least 6 characters'

                            }, {
                                fieldLabel: 'Organization Name',
                                xtype: 'hidden',
                                name: 'organizationName',
                                readOnly: true,
                                queryMode: 'local',
                                itemId: 'orgname'
                            }, {
                                fieldLabel: 'Security Group',
                                xtype: 'hidden',
                                name: 'groupName',
                                readOnly: true,
                                queryMode: 'local',
                                itemId: 'idsec'
                            }]
                    }]
            }];

        me.listeners = {
            afterrender: function () {                
                 setTimeout(function () {
                    me.down('#orgname').setValue(me.down('#organizationNameId').getRawValue());
                    me.down('#idcontact').setValue(me.down('#idContactName').getRawValue());
                    me.down('#idsec').setValue(me.down('#securityGroupId').getRawValue());  
                }, 100);                                                                                                    
            }}

        me.callParent(arguments);
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.addAndNewButton,
        MineralPro.config.util.PopUpWindowCmpt.addToGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});