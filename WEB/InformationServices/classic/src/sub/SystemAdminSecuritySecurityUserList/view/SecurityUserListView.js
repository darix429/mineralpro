Ext.define('InformationServices.sub.SystemAdminSecuritySecurityUserList.view.SecurityUserListView' ,{
    extend: 'MineralPro.config.view.BaseView',
    alias : 'widget.SystemAdminSecuritySecurityUserList',
    layout:'border',
    
    id: 'SystemAdminSecuritySecurityUserListId',
    
    items: [{
        xtype:'securityUserListGrid',
        html:'center',
        region:'center'
    }]
});    