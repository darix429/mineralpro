
Ext.define('InformationServices.sub.SystemAdminSecuritySecurityUserList.view.SecurityUserListGridView', {
    extend: 'MineralPro.config.view.BaseGridView',
    alias: 'widget.securityUserListGrid',
    store:'InformationServices.sub.SystemAdminSecuritySecurityUserList.store.UserListStore',

    requires: ['InformationServices.sub.SystemAdminSecuritySecurityUserList.view.SecurityUserListGridViewController'],
    controller: 'securityUserListGridViewController',     
          
    //below are default items needed for grid checking 
    firstLoadItemId: 'app_name_id', 
    mainPanelAlias: 'SystemAdminSecuritySecurityUserList', //widget alias name of the main panel
    mainListModel: 'InformationServices.sub.SystemAdminSecuritySecurityUserList.model.UserListModel',     

    
//    detailAliasName: 'clientListDetails',//widget aliasName for detailView
    addFormAliasName: 'addUserListForm', //widget form to call for add list 
    editFormAliasName: 'editUserListForm', //widget form to call for edit
//    


	
    firstFocus: 'app_name_id',    
    
    addButtonTooltip: 'Add New User',
    addButtonText: 'Add New User',
        
    columns:[{ 
        header: 'Email Address',
        flex     : 1,
        dataIndex: 'emailAddress',
        itemId: 'app_name_id',
        filterElement: new Ext.form.TextField()
    },{   
        header: 'Organization Contact',
        flex     : 1,
        dataIndex: 'organizationContact',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Organization Name',
        flex     : 1,
        dataIndex: 'organizationName',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Security Group',
        flex     : 1,
        dataIndex: 'groupName',
        filterElement:new Ext.form.TextField()
    },{   
        header: 'Status',
        flex     : 1,
        itemId: 'status',
        dataIndex: 'description',
        filterElement : new Ext.form.ComboBox({                            
            showFilterIcon:true,
            triggerAction           : 'all',                
            typeAhead               : true,                                
            mode                    : 'local',
            listWidth               : 160,
            hideTrigger             : false,
            emptyText               : 'Select',
            // store                   :[['0','ACTIVE'],['14','INACTIVE']],
            store                   : Ext.create('InformationServices.sub.OrganizationOrganizationContact.store.StatusCdStore'),
            listeners: {
                beforerender: function(combo) {
                    combo.store.load({
                        callback: function(records, operation, success) {
                            var type = [];
                            for(i = 0;i < records.length;i++){
                                var a = [];
                                a.push(records[i].data.description)
                                a.push(records[i].data.description);
                                type.push(a);
                            }
                            combo.bindStore(type);
                            var recordSelected = combo.getStore().getAt(0);  
                            combo.setValue(recordSelected.get('field1'));
                        }
                    });
                }
            }
        }),
        // renderer: function(value,column,row){
        //     return row.data.description;
        // }
    },{
     xtype: 'actioncolumn',
        width: InformationServices.config.Runtime.actionColWidth,
        items: [
            MineralPro.config.util.MainGridActionColCmpt.editCls,
            MineralPro.config.util.MainGridActionColCmpt.deleteCls,
        ],
        listeners: {
            afterrender: function(){
                var me = this       
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    }],
 
    initComponent: function () {  
        var me = this;
        
        me.plugins = [Ext.create('Ext.ux.grid.FilterRow')],
        
        me.tools= [
        {
            xtype: 'button',
            itemId: 'addNewDataButtonItemId',
            iconCls:MineralPro.config.Runtime.addNewBtnIconCls,
            listeners: {
                click: function() {
                    me.down('#status').config.filterElement.setValue('')
                    var record = Ext.create('InformationServices.sub.SystemAdminSecuritySecurityUserList.model.UserListModel');      
                    var addView = Ext.widget('addUserListForm');                    
                    record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
                    addView.down('form').loadRecord(record);
                    addView.show()
                },
                // click: 'onOpenAddListForm',
                beforerender: function () {
                    var me = this;
                    me.tooltip = me.up('grid').addButtonTooltip
                    me.text = me.up('grid').addButtonText
                },
                afterrender: function(){
                    var me = this
                        me.setHidden(MineralPro.config.Runtime.access_level)
                }
            }
        }
        ,MineralPro.config.util.MainGridToolCmpt.saveToDbButton                   
        ,MineralPro.config.util.MainGridToolCmpt.clearFilterButton],   
        
        me.callParent(arguments);
        
    }    
});