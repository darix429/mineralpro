//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('InformationServices.sub.SystemAdminSecuritySecurityUserList.view.SecurityUserListGridViewController', {
    extend: 'MineralPro.config.view.BaseGridViewController',
    alias: 'controller.securityUserListGridViewController',
    onOpenEditListForm: function (grid, rowIndex, colIndex) {
        MineralPro.config.RuntimeUtility.DebugLog('Entering SecurityUserListGridViewController onOpenEditListForm.')
        var me = this;
        var mainView = me.getView();
        var rec = mainView.getStore().getAt(rowIndex)

        var editView = Ext.widget(mainView.editFormAliasName);
        editView.editRecordIndex = rowIndex
        editView.down('form').loadRecord(rec);

        var OrgContactStore = Ext.getStore('InformationServices.sub.SystemAdminSecuritySecurityUserList.store.OrgContactStore');
        var SecGroupStore = Ext.getStore('InformationServices.sub.SystemAdminSecuritySecurityUserList.store.SecGroupStore');
        var OrgNameStore = Ext.getStore('InformationServices.sub.SystemAdminSecuritySecurityUserList.store.OrgNameStore');
        
        OrgContactStore.load({
            params: {
                idOrganization: rec.get('idOrganization')
            },
            callback: function () {
                editView.down('#idContactName').select(rec.get('idOrganizationContact'));
            }
        });
        SecGroupStore.load({
            callback: function () {
                editView.down('#securityGroupId').select(rec.get('idSecurityGroup'));
            }
        });
        OrgNameStore.load({
            callback: function () {
                editView.down('#organizationNameId').select(rec.get('idOrganization'));
            }
        });
        editView.show();
        MineralPro.config.RuntimeUtility.DebugLog('Leaving SecurityUserListGridViewController onOpenEditListForm.')
    }

});
