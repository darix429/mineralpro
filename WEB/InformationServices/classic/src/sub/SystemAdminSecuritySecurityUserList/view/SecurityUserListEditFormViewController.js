//This is the view controller for UserLoginWindowView. New feature in extjs5

Ext.define('InformationServices.sub.SystemAdminSecuritySecurityUserList.view.SecurityUserListEditFormViewController', {
    extend : 'MineralPro.config.view.BaseAddEditFormViewController',
    alias: 'controller.securityUserListEditFormViewController',
    onEditListToGrid: function(button){
        MineralPro.config.RuntimeUtility.DebugLog('Entering BaseAddEditFormViewController onEditListToGrid.')
        var me = this;        
        var view = me.getView();
        var mainPanel = Ext.getCmp(view.mainPanelId);
        var listGrid = mainPanel.down(view.listGridAlias)
        
        var win = button.up('window');
        var form = win.down('form');
        var newPassword = view.down('#passwordStatus').checkboxCmp.getValue();
	    var record = listGrid.getStore().getAt(view.editRecordIndex);
        var values = form.getValues();
        
        if(newPassword){
            record.set('newpassword', values.password)
        } else {
            record.set('newpassword', '');    
        }
        
        if(form.isValid()){
            record.set('rowUpdateUserid', MineralPro.config.Runtime.idAppUser)
            record.set(values);                

            if(listGrid.withDetailsView){
                var detailView = mainPanel.down(listGrid.detailAliasName);
                detailView.loadRecord(record);
            }
            
            win.close();                             
        }else {
            Ext.Msg.show({
                iconCls:MineralPro.config.Runtime.addEditToGridMsgErrorIcon,
                title:MineralPro.config.Runtime.addEditToGridMsgErrorTitle,    
                msg:MineralPro.config.Runtime.addEditToGridMsgErrorMsg,               
                buttons: Ext.Msg.OK
            });
        } 
        MineralPro.config.RuntimeUtility.DebugLog('Leaving BaseAddEditFormViewController onEditListToGrid.')
    },
});