Ext.define('InformationServices.sub.SystemAdminSecuritySecurityUserList.view.SecurityUserListEditFormView', {
    extend: 'MineralPro.config.view.BaseAddEditFormView',
    requires: ['InformationServices.sub.SystemAdminSecuritySecurityUserList.view.SecurityUserListEditFormViewController'],
    controller: 'securityUserListEditFormViewController',
    alias: 'widget.editUserListForm',
    title: 'Edit User',
    defaultFocus: '#idContactName',
    width: 350,
    //Note mainPanelId is the id of the Main Panel for each function
    mainPanelId: 'SystemAdminSecuritySecurityUserListId', //id of the main panel
    listGridStore: 'InformationServices.sub.SystemAdminSecuritySecurityUserList.store.UserListStore', //store for note grid
    listGridAlias: 'securityUserListGrid', //alias name for note gridview

    editRecordIndex: 0,

    initComponent: function () {
        var me = this;
        me.items = [{
                xtype: 'form',
                defaults: {
                    maskRe: /[^'\^]/,
                    labelWidth: 130,
                },
                bodyPadding: 5,
                items: [{
                        xtype: 'fieldset',
                        defaults: {
                            maskRe: /[^'\^]/,
                            labelWidth: 130,
                        },
                        title: 'Security Information',
                        defaultType: 'textfield',
                        layout: 'anchor',
                        defaults: {
                            anchor: '100%',
                            labelWidth: 130,
                        },
                        items: [{
                                fieldLabel: 'Organization Name',
                                xtype: 'combo',
                                selectOnFocus: true,
                                tabIndex: 1,
                                itemId: 'organizationNameId',
                                displayField: 'organizationName',
                                valueField: 'idOrganization',
                                name: 'idOrganization',
                                typeAhead: true,
                                disabled: true,
                                store: 'InformationServices.sub.SystemAdminSecuritySecurityUserList.store.OrgNameStore',
                                forceSelection: true,
                                allowBlank: false,
                                labelAlign: 'right',
                                msgTarget: 'side',
                                listeners: {
                                    change: function (fieldLabel, newval, oldval) {
                                        var st = Ext.getStore('InformationServices.sub.SystemAdminSecuritySecurityUserList.store.OrgContactStore');
                                        st.load({
                                            params: {
                                                idOrganization: newval
                                            },
                                            callback: function (records, operation, success) {
                                                if (st.totalCount == 0) {
                                                    Ext.Msg.show({
                                                        iconCls: InformationServices.config.Runtime.securityUserListNoContactAlertIcon,
                                                        title: InformationServices.config.Runtime.securityUserListNoContactAlertTitle,
                                                        msg: InformationServices.config.Runtime.securityUserListNoContactAlertMsg,
                                                        buttons: Ext.Msg.OK
                                                    });
                                                }else{
                                                    me.down('#idContactName').select(1);
                                                }
                                                me.down('#idcontact').setValue(me.down('#idContactName').getRawValue());

                                            }
                                        });
                                        me.down('#orgname').setValue(me.down('#organizationNameId').getRawValue());
                                    },
                                     beforequery: function(queryVV){
                                        queryVV.combo.expand();
                                        queryVV.combo.store.load();
                                        return false;
                                    }
                                }
                            }, {
                                fieldLabel: 'Organization Contact',
                                xtype: 'combo',
                                selectOnFocus: true,
                                tabIndex: 2,
                                itemId: 'idContactName',
                                displayField: 'organizationContact',
                                valueField: 'idOrganizationContact',
                                name: 'idOrganizationContact',
                                store: 'InformationServices.sub.SystemAdminSecuritySecurityUserList.store.OrgContactStore',
                                submitValue: true,
                                typeAhead: true,
                                forceSelection: true,
                                allowBlank: true,
                                queryMode: 'local',
                                labelAlign: 'right',
                                msgTarget: 'side',
                                listeners: {
                                    select: function (record) {
                                        me.down('#idcontact').setValue(me.down('#idContactName').getRawValue());
                                    },   
                                    blur: 'onAddCheckExisting' 
                                }
                            }, {
                                fieldLabel: 'Security Group',
                                xtype: 'combo',
                                selectOnFocus: true,
                                tabIndex: 3,
                                itemId: 'securityGroupId',
                                displayField: 'groupName',
                                valueField: 'idSecurityGroup',
                                store: 'InformationServices.sub.SystemAdminSecuritySecurityUserList.store.SecGroupStore',
                                name: 'idSecurityGroup',
                                hiddenName: 'idSecurityGroup',
                                submitValue: true,
                                typeAhead: true,
                                //forceSelection: true,
                                allowBlank: false,
                                queryMode: 'local',
                                labelAlign: 'right',
                                msgTarget: 'side',
                                listeners: {
                                    Select: function (record) {
                                        me.down('#idsec').setValue(me.down('#securityGroupId').getRawValue());
                                    },
                                     beforequery: function(queryVV){
                                        queryVV.combo.expand();
                                        queryVV.combo.store.load();
                                        return false;
                                    }
                                }
                            }, {
                                fieldLabel: 'Contact Name',
                                xtype: 'hidden',
                                name: 'organizationContact',
                                readOnly: true,
                                queryMode: 'local',
                                itemId: 'idcontact'
                            }]
                    }, {
                        xtype: 'fieldset',
                        defaults: {
                            maskRe: /[^'\^]/,
                            labelWidth: 130,
                        },
                        title: 'Account Information',
                        defaultType: 'textfield',
                        layout: 'anchor',
                        defaults: {
                            anchor: '100%',
                            labelWidth: 130,
                        },
                        items: [{
                                fieldLabel: 'Email Address',
                                selectOnFocus: true,
                                tabIndex: 4,
                                name: 'emailAddress',
                                xtype: 'textfield',
                                vtype: 'email',
                                margin: '0 0 10',
                                itemId: 'email_add',
                                labelAlign: 'right',
                                validateBlank: true,
                                allowBlank: false,
                                msgTarget: 'side'
                            }, {
                                fieldLabel: 'Organization Name',
                                xtype: 'hidden',
                                name: 'organizationName',
                                readOnly: true,
                                queryMode: 'local',
                                itemId: 'orgname'
                            }, {
                                fieldLabel: 'SecurityGroup',
                                xtype: 'hidden',
                                name: 'groupName',
                                readOnly: true,
                                queryMode: 'local',
                                itemId: 'idsec'
                            }]
                    }, {
                        xtype: 'fieldset',
                        itemId:'passwordStatus',
                        title: 'Change Password',
                        checkboxToggle: true,
                        collapsed: true,
                        defaultType: 'textfield',
                        layout: 'anchor',
                        defaults: {
                            anchor: '100%',
                            labelWidth: 130,
                        },
                        listeners: {
                            collapse: function(p) {
                                p.items.each(function(i) {
                                    i.disable();
                                },
                                this);
                            },
                            expand: function(p) {
                                p.items.each(function(i) {
                                    i.enable();
                                },
                                this);
                            }
                        },
                        items: [{
                                fieldLabel: 'Password',
                                selectOnFocus: true,
                                tabIndex: 5,
                                name: 'password',
                                xtype: 'textfield',
                                inputType: 'password',
                                margin: '0 0 10',
                                labelAlign: 'right',
                                msgTarget: 'side',
                                id: 'pwd',
                                maxLength: 32,
                                allowBlank: true,
                                minLength: 6,
                                minLengthText: 'Password must be at least 6 characters'
                            }, {
                                fieldLabel: 'Confirm Password',
                                name: 'confirmpassword',
                                selectOnFocus: true,
                                tabIndex: 6,
                                xtype: 'textfield',
                                inputType: 'password',
                                margin: '0 0 10',
                                labelAlign: 'right',
                                msgTarget: 'side',
                                vtype: 'password',
                                allowBlank: true,
                                initialPassField: 'pwd',
                                maxLength: 32,
                                minLength: 6,
                                minLengthText: 'Password must be at least 6 characters'                           
                            }]
                    }]
            }];

        this.callParent(arguments);
    },
    buttons: [MineralPro.config.util.PopUpWindowCmpt.changeInGridButton,
        MineralPro.config.util.PopUpWindowCmpt.cancelButton]
});