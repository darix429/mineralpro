///**
// *Responsible for constructing the viewport and 
// *initializing the user information to the global variable
// */

Ext.define('Exemptions.controller.AppMainController', {
    extend: 'Ext.app.Controller',

    init: function(){              
        var me=this;
        me.listen({
            global: {            
                loadViewPort: me.loadViewPort
            }            
        })               
    },
    /**
     * Load the viewport for user interaction with the system
     */
    loadViewPort: function(){
        MineralPro.config.RuntimeUtility.DebugLog('Entering AppMainController loadViewPort.')     
                                  
        var task = new Ext.util.DelayedTask(function () {       
           Exemptions.app.getController('CPRegion.controller.region.NorthController') 
         });        
        
         var task2 = new Ext.util.DelayedTask(function () {       
           Exemptions.app.getController('CPRegion.controller.region.WestController')  
         }); 
         
          var task3 = new Ext.util.DelayedTask(function () {       
           Exemptions.app.getController('CPRegion.controller.region.CenterController') 
            Ext.create('CPRegion.view.CPRegionViewPort').show();
         }); 
       
        task.delay(200); 
        task2.delay(200);                            
        task3.delay(200); 
      
        MineralPro.config.RuntimeUtility.DebugLog('Leaving AppMainController loadViewPort.')
    }
    
});
