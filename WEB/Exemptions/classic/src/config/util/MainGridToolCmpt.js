Ext.define('Exemptions.config.util.MainGridToolCmpt', {
    singleton: true, //default configuration for global variable

    addNewDataButton: {
        xtype: 'button',
        iconCls:Exemptions.config.Runtime.addNewBtnIconCls,
        listeners: {
            click: 'onOpenAddListForm',
            beforerender: function () {
                var me = this;
                me.tooltip = me.up('grid').addButtonTooltip
                me.text = me.up('grid').addButtonText
            },
            afterrender: function(){
                var me = this
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    },
    saveToDbButton: {
        xtype: 'button',
        iconCls:Exemptions.config.Runtime.saveToDbBtnIconCls,
        tooltip:Exemptions.config.Runtime.saveGridBtnText,
        text:Exemptions.config.Runtime.saveGridBtnTooltip,
        listeners: {
            click: 'onSyncListGridData',
            afterrender: function(){
                var me = this
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    },
    clearFilterButton: {
        xtype: 'button',
        iconCls:Exemptions.config.Runtime.refreshBtnIconCls,
        tooltip:Exemptions.config.Runtime.refreshBtnTooltip,
        text:Exemptions.config.Runtime.refreshBtnText,
        listeners: {
            click: 'onClearListFilter'
        }
    },
    saveButton: {
        xtype: 'button',
        iconCls:Exemptions.config.Runtime.saveToDbBtnIconCls,
        listeners: {
            click: 'onSyncListGridData',
            beforerender: function () {
                var me = this;
                me.tooltip = me.up('panel').saveButtonTooltip
                me.text = me.up('panel').saveButtonText
            },
            afterrender: function(){
                var me = this
                me.setHidden(MineralPro.config.Runtime.access_level)
            }
        }
    },
    cancelGridButton: {
        xtype: 'button',
        iconCls:Exemptions.config.Runtime.cancelGridIconCls,
        tooltip:Exemptions.config.Runtime.cancelGridTooltip,
        text:Exemptions.config.Runtime.cancelGridText,
        listeners: {
            click: 'onCancelGrid'
        }
    },
    
     comboSelection: {
        xtype: 'combo',
        width: 350,
        labelAlign: 'right',
        typeAhead: true,
        displayField: 'displayField',
        valueField: 'displayField',
        listeners: {
            beforerender: function () {
                var me = this
                var grid = me.up('grid')
                me.setStore(grid.gridToolComboStore)
                me.setFieldLabel(grid.gridToolComboFieldLabel)
                me.emptyText = grid.gridToolComboEmptyText

            },
            /// added for focus on dropdown
            afterrender: function (field) {
                Ext.defer(function () {
                    field.focus(true, 100);
                }, 10);                
            },
            change: function (fieldLabel, newval, oldval) {
                var me = this
                var listStore = me.up('grid').getStore();
                listStore.getProxy().extraParams = {
                    sec: newval
                }
                listStore.load();                              
            },
            beforequery: function(queryVV){
                queryVV.combo.expand();
                queryVV.combo.store.load();
                return false;
            }
        }
    },
    
})


    