Ext.define('Exemptions.config.util.MainGridActionColCmpt', {
    singleton: true, //default configuration for global variable

    editCls: {
        icon:Exemptions.config.Runtime.editCls,
        tooltip:Exemptions.config.Runtime.editTooltip,
        handler: function (grid, rowIndex, colIndex, item, e, record) {
            this.up('grid').getSelectionModel().select(rowIndex)
            if (record.get('rowDeleteFlag') != 'D') {
                this.up('grid').getController().onOpenEditListForm(grid, rowIndex, colIndex, item)
            } else {
                Ext.Msg.show({
                    iconCls:Exemptions.config.Runtime.editDeletedDataWarningCls,
                    title:Exemptions.config.Runtime.editDeletedDataWarningTitle,
                    msg:Exemptions.config.Runtime.editDeletedDataWarningMsg,
                    buttons: Ext.Msg.OK
                });
            }
        },         
    },
    deleteCls: {
        getClass: function (colConfig, cellData, record, rowIndex, colIndex, dataStore) {
            if (record.get('rowDeleteFlag') == 'D') {
                return(Exemptions.config.Runtime.undoDeleteCls);
            } else {
                return(Exemptions.config.Runtime.deleteCls);
            }
        },
        getTip: function (colConfig, cellData, record, rowIndex, colIndex, dataStore) {
            if (record.get('rowDeleteFlag') == 'D') {
                return(Exemptions.config.Runtime.undoDeleteTooltip);
            } else {
                return(Exemptions.config.Runtime.deleteTooltip);
            }
        },
        handler: function (gridView, rowIndex, colIndex, clickedItem, eventObj, record, row) {
            this.up('grid').getSelectionModel().select(rowIndex)
            if (record.get('rowDeleteFlag') == 'D') {
                record.set('rowDeleteFlag', record.previousValues.rowDeleteFlag);                                         
            } else {
                record.set('rowDeleteFlag', 'D');            
            }
        }
    },
})


    