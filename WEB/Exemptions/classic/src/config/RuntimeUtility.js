Ext.define('Exemptions.config.RuntimeUtility', {
    statics: {
       
        getDateName: function () {
            MineralPro.config.RuntimeUtility.DebugLog('Entering RuntimeUtility getDateName.')

            var today = new Date();
            var year = today.getFullYear();
            var month = ("0" + parseInt(parseInt(today.getMonth()) + 1)).slice(-2);
            var date = ("0" + today.getDate()).slice(-2);
            var hour = ("0" + today.getHours()).slice(-2);
            var min = ("0" + today.getMinutes()).slice(-2);
            var sec = ("0" + today.getSeconds()).slice(-2);
            var millisec = ("00" + today.getMilliseconds()).slice(-3);

            MineralPro.config.RuntimeUtility.DebugLog('Leaving RuntimeUtility getDateName.')

            return(month + date + year + '_' + hour + min + sec + millisec)
        },
        setComboDetails: function (combo, newval) {
            MineralPro.config.RuntimeUtility.DebugLog('Entering RuntimeUtility setComboDetails.')

            if (combo.emptyText.indexOf('Distributor') > -1 && newval != null && newval != '') {
                Maintenance.config.Runtime.distributor = newval;
            } else if (combo.emptyText.indexOf('Client') > -1 && newval != null && newval != '') {
                Maintenance.config.Runtime.client = newval;
            } else if (combo.emptyText.indexOf('Store') > -1 && newval != null && newval != '') {
                Maintenance.config.Runtime.store = newval;
            } else if (combo.emptyText.indexOf('Vendor') > -1 && newval != null && newval != '') {
                Maintenance.config.Runtime.vendor = newval;
            }

            MineralPro.config.RuntimeUtility.DebugLog('Leaving RuntimeUtility setComboDetails.')
        },
        onTabChange: function (tab) {
            MineralPro.config.RuntimeUtility.DebugLog('Entering RuntimeUtility onTabChange.')           

            var arrayGrid = tab.getActiveTab().query('grid')
            var arrayCombo = tab.getActiveTab().query('combo')

            Ext.each(arrayGrid, function (grid) {
                grid.getStore().reload({ 
                    callback: function() {
                        grid.getSelectionModel().select(0, false, false);
                    }
                });
            })

            Ext.each(arrayCombo, function (combo) {                              
                var comboStore = combo.getStore();
                comboStore.load(function (records, operation, success) {
                    if (combo.emptyText.indexOf('Distributor') > -1) {
                        comboStore.findBy(function (record, id) {
                            if (record.get('valueField') == Maintenance.config.Runtime.distributor
                                    || record.get('displayField') == Maintenance.config.Runtime.distributor) {
                                if (combo.valueField == 'displayField') {
                                    combo.setValue(record.get('displayField'));
                                } else {
                                    combo.setValue(record.get('valueField'));
                                }
                            }
                        })
                    } else if (combo.emptyText.indexOf('Client') > -1) {
                        var task = new Ext.util.DelayedTask(function () {
                            comboStore.findBy(function (record, id) {
                                if (record.get('valueField') == Maintenance.config.Runtime.client
                                        || record.get('displayField') == Maintenance.config.Runtime.client) {
                                    if (combo.valueField == 'displayField') {
                                        combo.setValue(record.get('displayField'));
                                    } else {
                                        combo.setValue(record.get('valueField'));
                                    }
                                }
                            })
                        });
                        task.delay(200);
                    } else if (combo.emptyText.indexOf('Store') > -1) {  
                        var task = new Ext.util.DelayedTask(function () {
                            comboStore.findBy(function (record, id) {
                                if (record.get('valueField') == Maintenance.config.Runtime.store
                                        || record.get('displayField') == Maintenance.config.Runtime.store) {
                                    if (combo.valueField == 'displayField') {
                                        combo.setValue(record.get('displayField'));
                                    } else {
                                        combo.setValue(record.get('valueField'));
                                    }
                                }
                            })
                        });
                        task.delay(400);
                    } else if (combo.emptyText.indexOf('Vendor') > -1) {
                        comboStore.findBy(function (record, id) {
                            if (record.get('valueField') == Maintenance.config.Runtime.vendor
                                    || record.get('displayField') == Maintenance.config.Runtime.vendor) {
                                if (combo.valueField == 'displayField') {
                                    combo.setValue(record.get('displayField'));
                                } else {
                                    combo.setValue(record.get('valueField'));
                                }
                            }
                        })
                    }
                });
            })

            MineralPro.config.RuntimeUtility.DebugLog('Leaving RuntimeUtility onTabChange.')
        }
    }
})