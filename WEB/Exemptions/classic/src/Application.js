/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('Exemptions.Application', {
    extend: 'Ext.app.Application',
    name: 'Exemptions',
    controllers: [
        'AppMainController',
        'CPLogin.controller.CPLoginController',
        'CPRegion.controller.CPRegionController',
        'Exemptions.sub.DefaultPage.controller.DefaultPageController'
    ],
    requires: [
        'Ext.form.field.Radio',
        'Ext.ux.grid.FilterRow',
        'Exemptions.config.Runtime', 
        'Exemptions.config.util.MainGridActionColCmpt',
        'Exemptions.config.util.MainGridToolCmpt',
        'Exemptions.config.util.PopUpWindowCmpt',
    ],
    refs: [{
            ref: 'UserLoginWindowView',
            selector: 'userLoginWindowView'
    }],
    launch: function () {
        var me = this;               
        MineralPro.config.Runtime.appName = ' Exemptions'   
        MineralPro.config.RuntimeUtility.disableBackspace();
        MineralPro.config.RuntimeUtility.enablePasswordChecking();
        MineralPro.config.RuntimeUtility.ImplementVtypes();
                        
//        MineralPro.config.RuntimeUtility.addThemeOptions();                       
        setTimeout(function () {
            Ext.globalEvents.fireEvent('checkUserSession');
        }, 800);
        Ext.on('resize', function () {
            var windowLogin = me.getUserLoginWindowView();
            if (windowLogin) {
                windowLogin.center();
            }
        });
    },
//	mainView: 'MineralPro.view.MainViewPort'
});
