///**
// * Handles the controller processing for Client List
// */

Ext.define('Administration.sub.DefaultPage.controller.DefaultPageController', {
    extend: 'Ext.app.Controller',
  
    models: [
    ],
    
    stores: [
    ],  
    
    views: [
        'Administration.sub.DefaultPage.view.DefaultPageView',
    ],          
    
    requires: ['Ext.form.ComboBox',
        'Ext.grid.column.Action',
        'Ext.form.FieldSet',
        'Ext.form.field.Hidden'
    ]
});


