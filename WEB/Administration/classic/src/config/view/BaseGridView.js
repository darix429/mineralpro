
Ext.define('Administration.config.view.BaseGridView', {
    extend: 'Ext.grid.Panel',
    requires: ['Administration.config.view.BaseGridViewController'],
    alias: 'widget.BaseGridView',
    controller: 'baseGridViewController',
    frame:true,
    layout: 'fit',
           
    initComponent: function() {       
        var me = this;        
        me.callParent(arguments);        
    },
    
   listeners : {
        callAdd:"onOpenAddListForm",
        callSave:"onSyncListGridData",
        callFilter:"onClearListFilter",
        callCancel:"onCancelGrid",
//        selectionchange: 'onLoadSelectionChange',
//        afterlayout: 'onAfterLayout',
        afterrender: function(window, options,field) {
            var me = this;
            this.keyNav = new Ext.util.KeyMap({
                target: window.el,
                binding: [
                {
                    key: "a",
                    alt:true,
                    fn: function(e){                                    
                        //alert('Alt + a was pressed');
                        me.fireEvent("callAdd");
                        return false;
                    }
                }, {
                    key: "s",
                    alt:true,
                    fn: function(){                                    
                        //alert('Alt + s was pressed');
                        me.fireEvent("callSave");
                        return false;
                    }
                }, {
                    key: "u",
                    alt:true,
                    fn: function(){                                    
                        //alert('Alt + u was pressed');
                        me.fireEvent("callFilter");
                        return false;
                    }
                }, {
                    key: "c",
                    alt:true,
                    fn: function(){                                    
                        //alert('Alt + c was pressed');
                        me.fireEvent("callCancel");
                        return false;
                    }
                }
                ],
                scope: me
            });
        }
                    
    },
            
    viewConfig: {
        getRowClass: function(record) {
            if (record.get('rowDeleteFlag') == 'D') {
                return 'strike-through-row';
            } 
            if (record.dirty) {
                return 'dirty-record';
            }
        },
        loadMask: true
    }
})