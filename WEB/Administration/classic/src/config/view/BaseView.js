
Ext.define('Administration.config.view.BaseView', {
    extend: 'Ext.form.Panel',
    requires: 'Administration.config.view.BaseViewController',
    controller: 'baseViewController',

    initComponent: function() {       
        var me = this;

        me.listeners= {
            beforeclose: me.getController().onBeforeClose
        }
      
        me.callParent(arguments);        
    }
    
})