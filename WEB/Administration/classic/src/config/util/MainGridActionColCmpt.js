Ext.define('Administration.config.util.MainGridActionColCmpt', {
    singleton: true, //default configuration for global variable

    editCls: {
        icon:Administration.config.Runtime.editCls,
        tooltip:Administration.config.Runtime.editTooltip,
        handler: function (grid, rowIndex, colIndex, item, e, record) {
            this.up('grid').getSelectionModel().select(rowIndex)
            if (record.get('rowDeleteFlag') != 'D') {
                this.up('grid').getController().onOpenEditListForm(grid, rowIndex, colIndex, item)
            } else {
                Ext.Msg.show({
                    iconCls:Administration.config.Runtime.editDeletedDataWarningCls,
                    title:Administration.config.Runtime.editDeletedDataWarningTitle,
                    msg:Administration.config.Runtime.editDeletedDataWarningMsg,
                    buttons: Ext.Msg.OK
                });
            }
        },         
    },
    deleteCls: {
        getClass: function (colConfig, cellData, record, rowIndex, colIndex, dataStore) {
            if (record.get('rowDeleteFlag') == 'D') {
                return(Administration.config.Runtime.undoDeleteCls);
            } else {
                return(Administration.config.Runtime.deleteCls);
            }
        },
        getTip: function (colConfig, cellData, record, rowIndex, colIndex, dataStore) {
            if (record.get('rowDeleteFlag') == 'D') {
                return(Administration.config.Runtime.undoDeleteTooltip);
            } else {
                return(Administration.config.Runtime.deleteTooltip);
            }
        },
        handler: function (gridView, rowIndex, colIndex, clickedItem, eventObj, record, row) {
            this.up('grid').getSelectionModel().select(rowIndex)
            if (record.get('rowDeleteFlag') == 'D') {
                record.set('rowDeleteFlag', record.previousValues.rowDeleteFlag);                                          
            } else {
                record.set('rowDeleteFlag', 'D');            
            }
        }
    },
})


    