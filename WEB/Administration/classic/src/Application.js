/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('Administration.Application', {
    extend: 'Ext.app.Application',
    name: 'Administration',
    controllers: [
        'AppMainController',
        'CPLogin.controller.CPLoginController',
        'CPRegion.controller.CPRegionController',
        'Administration.sub.DefaultPage.controller.DefaultPageController'
    ],
    stores:[
        'MineralPro.store.AppraisalYearStore'
    ],
    requires: [
        'Ext.form.field.Radio',
        'Ext.ux.grid.FilterRow',
        'Administration.config.Runtime', 
        'Administration.config.util.MainGridActionColCmpt',
        'Administration.config.util.MainGridToolCmpt',
        'Administration.config.util.PopUpWindowCmpt',
        
        'MineralPro.sub.CommonFunctionsSearch.controller.CommonFunctionsSearchController',
    ],
    refs: [{
            ref: 'UserLoginWindowView',
            selector: 'userLoginWindowView'
    }],
    launch: function () {
        var me = this;               
        MineralPro.config.Runtime.appName = 'Administration'   
        MineralPro.config.RuntimeUtility.disableBackspace();
        MineralPro.config.RuntimeUtility.enablePasswordChecking();
        MineralPro.config.RuntimeUtility.ImplementVtypes();
                        
//        MineralPro.config.RuntimeUtility.addThemeOptions();                       
        setTimeout(function () {
            Ext.globalEvents.fireEvent('checkUserSession');
                setTimeout(function () {
                    if(MineralPro.config.Runtime.appraisalYear.length == 0){
                         MineralPro.config.Runtime.appraisalYear = new Date().getFullYear();
                    }
                    me.getStore('MineralPro.store.AppraisalYearStore').load();
                }, 200);
        }, 200);

        Ext.on('resize', function () {
            var windowLogin = me.getUserLoginWindowView();
            if (windowLogin) {
                windowLogin.center();
            }
        });
    },
//	mainView: 'MineralPro.view.MainViewPort'
});
