USE [MINERALPRO]
GO

/****** Object:  View [dbo].[vAppraisalPriorYear]    Script Date: 9/25/2017 8:36:26 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW  [dbo].[vAppUserSummary]  
AS
SELECT rowUpdateUserid, sum(cnt) cnt FROM (
	SELECT rowUpdateUserid, count(*) cnt FROM AppUserSubSystemFavorite WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM LeaseOwner WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM Operator WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM Organization WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM SystemCodeType WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM Field WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM LeaseValueSummary WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM OrganizationContact WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM SystemCode WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM Agency WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM ApprDeclineSchedule WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM SystemCodeGroup WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM AppUser WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM SubjectList WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM CalcTaxAdj WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM SystemSetting WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM ApprAmortSchedule WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM ArbParticipant WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM UserOrganization WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM CalcEscalatedPriceAdj WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM AppraisalYear WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM SecurityGroup WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM Owner WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM CalcEscalatedOpExpAdj WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM UserOrgSecurity WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM NoteType WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM CalcEquipmentSchedule WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM App WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM QuickNote WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM CalcEquipmentPVSchedule WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM SubSystem WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM Note WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM Report WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM Unit WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM AppSubSystem WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM ReportScript WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM UnitValueSummary WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM SubSystemFunction WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM OwnerExemption WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM Exemption WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM FunctionSecurity WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM Lease WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM Appraisal WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM TaxUnit WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM UserFunction WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM AppUserFavorite WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM LeaseTaxUnit WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid 
	UNION SELECT rowUpdateUserid, count(*) cnt FROM TaxUnitExemption WHERE rowDeleteFlag = '' GROUP BY rowUpdateUserid
) a GROUP BY rowUpdateUserid


GO


