USE [MINERALPRO]
GO

/****** Object:  View [dbo].[vAppUserSummary]    Script Date: 9/25/2017 10:36:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW  [dbo].[vCodeTypeSummary]  
AS
SELECT appraisalYear, columnName, cdValue, sum(cnt) cnt FROM 
	(SELECT appraisalYear, 'interestTypeCd' columnName, interestTypeCd cdValue, count(1) cnt FROM LeaseOwner GROUP BY appraisalYear, interestTypeCd 
	UNION SELECT appraisalYear, 'lockCd' columnName, lockCd cdValue, count(1) cnt FROM LeaseOwner GROUP BY appraisalYear, lockCd 
	UNION SELECT appraisalYear, 'gravityCd' columnName, gravityCd cdValue, count(1) cnt FROM Field GROUP BY appraisalYear, gravityCd 
	UNION SELECT appraisalYear, 'stateCd' columnName, stateCd cdValue, count(1) cnt FROM Agency GROUP BY appraisalYear, stateCd 
	UNION SELECT appraisalYear, 'countryCd' columnName, countryCd cdValue, count(1) cnt FROM Agency GROUP BY appraisalYear, countryCd 
	UNION SELECT appraisalYear, 'statusCd' columnName, statusCd cdValue, count(1) cnt FROM Agency GROUP BY appraisalYear, statusCd 
	UNION SELECT appraisalYear, 'subjectTypeCd' columnName, subjectTypeCd cdValue, count(1) cnt FROM ApprDeclineSchedule GROUP BY appraisalYear, subjectTypeCd 
	UNION SELECT appraisalYear, 'declineTypeCd' columnName, declineTypeCd cdValue, count(1) cnt FROM ApprDeclineSchedule GROUP BY appraisalYear, declineTypeCd 
	UNION SELECT appraisalYear, 'subjectTypeCd' columnName, subjectTypeCd cdValue, count(1) cnt FROM SubjectList GROUP BY appraisalYear, subjectTypeCd 
	UNION SELECT appraisalYear, 'taxTypeCd' columnName, taxTypeCd cdValue, count(1) cnt FROM CalcTaxAdj GROUP BY appraisalYear, taxTypeCd 
	UNION SELECT appraisalYear, 'subjectTypeCd' columnName, subjectTypeCd cdValue, count(1) cnt FROM ApprAmortSchedule GROUP BY appraisalYear, subjectTypeCd 
	UNION SELECT appraisalYear, 'stateCd' columnName, stateCd cdValue, count(1) cnt FROM Owner GROUP BY appraisalYear, stateCd 
	UNION SELECT appraisalYear, 'countryCd' columnName, countryCd cdValue, count(1) cnt FROM Owner GROUP BY appraisalYear, countryCd 
	UNION SELECT appraisalYear, 'groupCd' columnName, groupCd cdValue, count(1) cnt FROM NoteType GROUP BY appraisalYear, groupCd 
	UNION SELECT appraisalYear, 'valueTypeCd' columnName, valueTypeCd cdValue, count(1) cnt FROM CalcEquipmentSchedule GROUP BY appraisalYear, valueTypeCd 
	UNION SELECT appraisalYear, 'groupCd' columnName, groupCd cdValue, count(1) cnt FROM QuickNote GROUP BY appraisalYear, groupCd 
	UNION SELECT appraisalYear, 'subjectTypeCd' columnName, subjectTypeCd cdValue, count(1) cnt FROM CalcDeclineSchedule GROUP BY appraisalYear, subjectTypeCd 
	UNION SELECT appraisalYear, 'declineTypeCd' columnName, declineTypeCd cdValue, count(1) cnt FROM CalcDeclineSchedule GROUP BY appraisalYear, declineTypeCd 
	UNION SELECT appraisalYear, 'exemptionTypeCd' columnName, exemptionTypeCd cdValue, count(1) cnt FROM OwnerExemption GROUP BY appraisalYear, exemptionTypeCd 
	UNION SELECT appraisalYear, 'subjectTypeCd' columnName, subjectTypeCd cdValue, count(1) cnt FROM CalcAppraisalSummary GROUP BY appraisalYear, subjectTypeCd 
	UNION SELECT appraisalYear, 'exemptionTypeCd' columnName, exemptionTypeCd cdValue, count(1) cnt FROM Exemption GROUP BY appraisalYear, exemptionTypeCd 
	UNION SELECT appraisalYear, 'cadCategoryCd' columnName, cadCategoryCd cdValue, count(1) cnt FROM Lease GROUP BY appraisalYear, cadCategoryCd 
	UNION SELECT appraisalYear, 'subjectTypeCd' columnName, subjectTypeCd cdValue, count(1) cnt FROM Appraisal GROUP BY appraisalYear, subjectTypeCd 
	UNION SELECT appraisalYear, 'gravityCd' columnName, gravityCd cdValue, count(1) cnt FROM Appraisal GROUP BY appraisalYear, gravityCd 
	UNION SELECT appraisalYear, 'taxUnitTypeCd' columnName, taxUnitTypeCd cdValue, count(1) cnt FROM TaxUnit GROUP BY appraisalYear, taxUnitTypeCd 
	UNION SELECT appraisalYear, 'subjectTypeCd' columnName, subjectTypeCd cdValue, count(1) cnt FROM CalcAmortSchedule GROUP BY appraisalYear, subjectTypeCd 
	UNION SELECT appraisalYear, 'taxUnitTypeCd' columnName, taxUnitTypeCd cdValue, count(1) cnt FROM LeaseTaxUnit GROUP BY appraisalYear, taxUnitTypeCd 
	UNION SELECT appraisalYear, 'taxUnitTypeCd' columnName, taxUnitTypeCd cdValue, count(1) cnt FROM TaxUnitExemption GROUP BY appraisalYear, taxUnitTypeCd 
	UNION SELECT appraisalYear, 'exemptionTypeCd' columnName, exemptionTypeCd cdValue, count(1) cnt FROM TaxUnitExemption GROUP BY appraisalYear, exemptionTypeCd
	)a
GROUP BY appraisalYear, columnName, cdValue



GO


