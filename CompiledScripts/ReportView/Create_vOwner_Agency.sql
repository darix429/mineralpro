USE [MINERALPRO]
GO

DROP VIEW IF EXISTS  [dbo].[vOwner_Agency]

/****** Object:  View [dbo].[vOwner_Agency]    Script Date: 8/10/2017 12:21:57 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


/****** Object:  View vOwner_Agency    Script Date: 5/14/2008 9:34:39 AM ******/
CREATE VIEW [dbo].[vOwner_Agency]
AS
SELECT  Owner.appraisalYear AS APPRAISAL_YR
	, Owner.ownerId
	, Owner.name1
	, Owner.addrLine1 AS ADDRESS_LINE1
	, Owner.addrLine2 AS ADDRESS_LINE2 
    , Owner.addrLine3 AS ADDRESS_LINE3
	, Owner.city AS CITY  
    , Owner.stateCd AS STATE_CD
	, v.codeDesc AS STATE_CD_DESC
	, Owner.zipcode AS ZIPCODE
	, Agency.agencyName AS TR_NAME 
    , Agency.addrLine1 AS TR_ADDRESS1
	, Agency.addrLine2 AS TR_ADDRESS2
	, Agency.addrLine3 AS TR_ADDRESS3
	, Agency.city AS TR_CITY
	, Agency.stateCd AS TR_STATE_CD 
    , x.codeDesc TR_STATE_CODE_DESC
    , Agency.zipcode AS TR_ZIPCODE

	,CASE WHEN NOT Agency.zipcode IS NULL 
			AND LEN(Agency.zipcode) > 0 
	THEN Agency.zipcode 
	ELSE Owner.zipcode
	 END AS ZIPCODE_SORT, Owner.agencyCdx

FROM dbo.Owner 
LEFT OUTER JOIN dbo.Agency 
ON   Owner.agencyCdx = Agency.agencyCdx 
AND  Owner.appraisalYear = Agency.appraisalYear

LEFT OUTER JOIN dbo.vSystemCode_and_Type v
ON v.codeTypeDesc =  'STATE CODES'
AND v.appraisalYear = Owner.appraisalYear
AND v.code = Owner.stateCd

LEFT OUTER JOIN dbo.vSystemCode_and_Type x
ON x.codeTypeDesc =  'STATE CODES'
AND x.appraisalYear = Agency.appraisalYear
AND x.code = Agency.stateCd










GO


