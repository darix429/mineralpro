USE [MINERALPRO]
GO

DROP VIEW IF EXISTS [dbo].[vSystemCode_and_Type]

/****** Object:  View [dbo].[vSystemCode_and_Type]    Script Date: 8/10/2017 12:22:34 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vSystemCode_and_Type]
AS
SELECT DISTINCT a.codeType, a.columnName, a.description codeTypeDesc
	, b.code, b.shortDescription codeName, b.description codeDesc
	, b.appraisalYear
	FROM  dbo.SystemCodeType a
	LEFT JOIN dbo.SystemCode b
	ON a.codeType = b.codeType
	WHERE b.activeInd = 'y'










GO


