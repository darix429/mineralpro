USE [MINERALPRO]
GO

DROP VIEW IF EXISTS [dbo].[vLease_Well_Type]

/****** Object:  View [dbo].[vLease_Well_Type]    Script Date: 8/27/2017 11:04:50 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vLease_Well_Type]
AS
SELECT leaseId, a.appraisalYear, b.codeDesc wellType
	FROM MINERALPRO.dbo.Lease a
	JOIN MINERALPRO.dbo.Appraisal p
	ON a.appraisalYear = p.appraisalYear
	AND (
		(a.leaseId = p.subjectId
		AND
		p.subjectTypeCd = 2)  -- 2 is for Lease
		OR
		(a.unitId = p.subjectId
		AND
		p.subjectTypeCd = 1)  -- 1 is for Unit
	)
	JOIN MINERALPRO.dbo.vSystemCode_and_Type b
	ON p.wellTypeCd = b.code
	AND p.appraisalYear = b.appraisalYear	
	AND b.codeTypeDesc = 'MRL WELL TYPE CODE'
	GROUP BY leaseId, a.appraisalYear, b.codeDesc 






GO


