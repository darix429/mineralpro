USE [MINERALPRO]
GO

DROP VIEW IF EXISTS [dbo].[vARBMEMBERS]

/****** Object:  View [dbo].[vARBMEMBERS]    Script Date: 8/26/2017 2:31:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vARBMEMBERS]
AS
SELECT     MAX(A.appraisalYear) AS appraisalYear, MAX(ISNULL(ARB1.fullName, '') + ISNULL(', ' + ARB2.fullName, '') + ISNULL(', ' + ARB3.fullName, '') + ISNULL(', ' + ARB4.fullName, '') 
                      + ISNULL(', ' + ARB5.fullName, '') + ISNULL(', ' + ARB6.fullName, '') + ISNULL(', ' + ARB7.fullName, '') + ISNULL(', ' + ARB8.fullName, '') + ISNULL(', ' + ARB9.fullName, '') 
                      + ISNULL(', ' + ARB10.fullName, '')) AS ARBMEMBERS, MAX(ISNULL(ARB1.fullName, '')) AS ARBMEMBER1, MAX(ISNULL(ARB2.fullName, '')) AS ARBMEMBER2, 
                      MAX(ISNULL(ARB3.fullName, '')) AS ARBMEMBER3, MAX(ISNULL(ARB4.fullName, '')) AS ARBMEMBER4, MAX(ISNULL(ARB5.fullName, '')) AS ARBMEMBER5, 
                      MAX(ISNULL(ARB6.fullName, '')) AS ARBMEMBER6, MAX(ISNULL(ARB7.fullName, '')) AS ARBMEMBER7, MAX(ISNULL(ARB8.fullName, '')) AS ARBMEMBER8, 
                      MAX(ISNULL(ARB9.fullName, '')) AS ARBMEMBER9, MAX(ISNULL(ARB10.fullName, '')) AS ARBMEMBER10
FROM         dbo.ArbParticipant AS A LEFT OUTER JOIN
                          (SELECT     appraisalYear, fullName
                            FROM          dbo.ArbParticipant
                            WHERE      (seqNumber = 1)) AS ARB1 ON A.appraisalYear = ARB1.appraisalYear LEFT OUTER JOIN
                          (SELECT     appraisalYear, fullName
                            FROM          dbo.ArbParticipant AS ArbParticipant_9
                            WHERE      (seqNumber = 2)) AS ARB2 ON A.appraisalYear = ARB2.appraisalYear LEFT OUTER JOIN
                          (SELECT     appraisalYear, fullName
                            FROM          dbo.ArbParticipant AS ArbParticipant_8
                            WHERE      (seqNumber = 3)) AS ARB3 ON A.appraisalYear = ARB3.appraisalYear LEFT OUTER JOIN
                          (SELECT     appraisalYear, fullName
                            FROM          dbo.ArbParticipant AS ArbParticipant_7
                            WHERE      (seqNumber = 4)) AS ARB4 ON A.appraisalYear = ARB4.appraisalYear LEFT OUTER JOIN
                          (SELECT     appraisalYear, fullName
                            FROM          dbo.ArbParticipant AS ArbParticipant_6
                            WHERE      (seqNumber = 5)) AS ARB5 ON A.appraisalYear = ARB5.appraisalYear LEFT OUTER JOIN
                          (SELECT     appraisalYear, fullName
                            FROM          dbo.ArbParticipant AS ArbParticipant_5
                            WHERE      (seqNumber = 6)) AS ARB6 ON A.appraisalYear = ARB6.appraisalYear LEFT OUTER JOIN
                          (SELECT     appraisalYear, fullName
                            FROM          dbo.ArbParticipant AS ArbParticipant_4
                            WHERE      (seqNumber = 7)) AS ARB7 ON A.appraisalYear = ARB7.appraisalYear LEFT OUTER JOIN
                          (SELECT     appraisalYear, fullName
                            FROM          dbo.ArbParticipant AS ArbParticipant_3
                            WHERE      (seqNumber = 8)) AS ARB8 ON A.appraisalYear = ARB8.appraisalYear LEFT OUTER JOIN
                          (SELECT     appraisalYear, fullName
                            FROM          dbo.ArbParticipant AS ArbParticipant_2
                            WHERE      (seqNumber = 9)) AS ARB9 ON A.appraisalYear = ARB9.appraisalYear LEFT OUTER JOIN
                          (SELECT     appraisalYear, fullName
                            FROM          dbo.ArbParticipant AS ArbParticipant_1
                            WHERE      (seqNumber = 10)) AS ARB10 ON A.appraisalYear = ARB10.appraisalYear
GROUP BY A.appraisalYear


GO


