USE [MINERALPRO]
GO

DROP VIEW IF EXISTS [dbo].[vOperators_for_Appraised_Lease_and_Unit]

/****** Object:  View [dbo].[vOperators_for_Appraised_Lease_and_Unit]    Script Date: 8/22/2017 11:30:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vOperators_for_Appraised_Lease_and_Unit]
AS
SELECT DISTINCT idAppraisal, operatorID, appraisalYear, accountName, accountNum, unitInd, codeDesc
	
	FROM (
	SELECT operatorId, a.appraisalYear, a.idAppraisal, c.leaseName accountName
		, c.leaseId accountNum, 'N' unitInd, b.codeDesc
		FROM dbo.Appraisal a
		LEFT OUTER JOIN [dbo].[vSystemCode_and_Type] b
		ON a.subjectTypeCd = b.code
		AND a.appraisalYear = b.appraisalYear
		AND b.codeTypeDesc = 'SUBJECT TYPE CODES'
		AND b.codeDesc = 'LEASE'
		LEFT OUTER JOIN dbo.Lease c
		ON a.appraisalYear = c.appraisalYear
		AND a.subjectId = c.leaseId
		WHERE c.unitId <= 0 AND c.operatorId > 0 and a.rowDeleteFlag <> 'D'
			 
	UNION
	SELECT operatorId, d.appraisalYear, d.idAppraisal, f.unitName accountName
		,f.unitId accountNum, 'Y' unitInd, e.codeDesc
		FROM dbo.Appraisal d
		LEFT  OUTER JOIN [dbo].[vSystemCode_and_Type] e
		ON d.subjectTypeCd = e.code
		AND d.appraisalYear = e.appraisalYear
		AND e.codeTypeDesc = 'SUBJECT TYPE CODES'
		AND e.codeDesc = 'UNIT'
		lefT OUTER JOIN dbo.Unit f
		ON d.appraisalYear = f.appraisalYear
		AND d.subjectId = f.unitId
		WHERE f.operatorId > 0 and d.rowDeleteFlag <> 'D'  
	) b
























GO


