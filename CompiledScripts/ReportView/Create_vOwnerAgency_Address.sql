USE [MINERALPRO]
GO

DROP VIEW IF EXISTS [dbo].[vOwnerAgency_Address]

/****** Object:  View [dbo].[vOwnerAgency_Address]    Script Date: 8/27/2017 1:29:44 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



/****** Object:  View vOwner_Agency    Script Date: 5/14/2008 9:34:39 AM ******/
CREATE VIEW [dbo].[vOwnerAgency_Address]
AS
SELECT DISTINCT a.OWNER_ID, a.OWNER_NAME, a.NAME, a.AGENCY_CDX, a.APPRAISAL_YR, a.ADDRESS_LINE1, a.ADDRESS_LINE2, a.ADDRESS_LINE3 
	   , a.CITY, a.STATE_CD_DESC, a.ZIPCODE
FROM (
	SELECT  Owner.appraisalYear AS  APPRAISAL_YR
	, Owner.ownerId OWNER_ID
	, Owner.name1 AS OWNER_NAME
	, Owner.name1 AS NAME
	, Owner.addrLine1 AS ADDRESS_LINE1
	, Owner.addrLine2 AS ADDRESS_LINE2 
    , Owner.addrLine3 AS ADDRESS_LINE3
	, Owner.city AS CITY  
	, v.codeDesc AS STATE_CD_DESC
	, Owner.zipcode AS ZIPCODE
	, Owner.agencyCdx AGENCY_CDX

	FROM dbo.Owner 
	LEFT OUTER JOIN dbo.Agency 
		ON   Owner.agencyCdx = Agency.agencyCdx 
		AND  Owner.appraisalYear = Agency.appraisalYear
	

	LEFT OUTER JOIN dbo.vSystemCode_and_Type v
		ON v.codeTypeDesc =  'STATE CODES'
		AND v.appraisalYear = Owner.appraisalYear
		AND v.code = Owner.stateCd

	WHERE  Owner.rowDeleteFlag <> 'D'
	AND (Agency.agencyName IS NULL OR  Owner.agencyCdx = '00000' OR LEN(Owner.agencyCdx) = 0)
	 

UNION

	SELECT  Owner.appraisalYear  AS APPRAISAL_YR
	, Owner.ownerId AS  OWNER_ID
	, Owner.name1 AS OWNER_NAME
	, Agency.agencyName as NAME
    , Agency.addrLine1 AS ADDRESS_LINE1
	, Agency.addrLine2 AS ADDRESS_LINE2
	, Agency.addrLine3 AS ADDRESS_LINE3
	, Agency.city AS CITY
    , x.codeDesc AS STATE_CODE_DESC
    , Agency.zipcode AS ZIPCODE
 	, Agency.agencyCdx  AGENCY_CDX
	

	FROM dbo.Owner 
	LEFT OUTER JOIN dbo.Agency 
		ON   Owner.agencyCdx = Agency.agencyCdx 
		AND  Owner.appraisalYear = Agency.appraisalYear

	LEFT OUTER JOIN dbo.vSystemCode_and_Type x
		ON x.codeTypeDesc =  'STATE CODES'
		AND x.appraisalYear = Agency.appraisalYear
		AND x.code = Agency.stateCd

	WHERE Owner.rowDeleteFlag <> 'D'
		AND Owner.agencyCdx <> '00000' 
		AND LEN(Owner.agencyCdx) > 0
		AND Agency.agencyName IS NOT NULL

) a









GO


