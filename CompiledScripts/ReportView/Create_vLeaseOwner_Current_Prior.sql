USE [MINERALPRO]
GO

DROP VIEW IF EXISTS [dbo].[vLeaseOwner_Current_Prior]

/****** Object:  View [dbo].[vLeaseOwner_Current_Prior]    Script Date: 8/10/2017 12:19:53 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vLeaseOwner_Current_Prior]
AS
SELECT     
	CASE WHEN LeaseOwner.appraisalYear IS NULL 
       THEN LeaseOwnerPrior.appraisalYear + 1 ELSE LeaseOwner.appraisalYear 
	   END AS appraisalYearMaster 
                     
	, CASE WHEN LeaseOwner.leaseId IS NULL 
       THEN LeaseOwnerPrior.leaseId ELSE LeaseOwner.leaseId 
	   END AS leaseIdMaster 

    , CASE WHEN LeaseOwner.ownerId IS NULL 
       THEN LeaseOwnerPrior.ownerId ELSE LeaseOwner.ownerId 
	   END AS ownerIdMaster 

    ,  CASE WHEN LeaseOwner.interestTypeCd IS NULL 
       THEN LeaseOwnerPrior.interestTypeCd ELSE LeaseOwner.interestTypeCd 
	   END AS interestTypeCdMaster 

    , LeaseOwner.appraisalYear
	, LeaseOwner.leaseId
	, LeaseOwner.ownerId
	, LeaseOwner.interestTypeCd
    , LeaseOwner.interestPercent
	, LeaseOwner.ownerValue
	, LeaseOwner.lockCd
	--, LeaseOwner.FUTURE_YR
    , LeaseOwner.rowUpdateUserid
	, LeaseOwner.rowUpdateDt
	, LeaseOwner.rowDeleteFlag
	, LeaseOwner.idLeaseOwner
	, LeaseOwnerPrior.idLeaseOwner AS idLeaseOwner_PRIOR
    , LeaseOwnerPrior.appraisalYear AS APPRAISAL_YR_PRIOR
	, LeaseOwnerPrior.leaseId AS ACCOUNT_NUM_PRIOR
	, LeaseOwnerPrior.ownerId AS OWNER_ID_PRIOR
	, LeaseOwnerPrior.interestTypeCd AS LEASE_INT_TYP_CD_PRIOR
    , LeaseOwnerPrior.interestPercent AS LEASE_INT_PCT_PRIOR
	, LeaseOwnerPrior.ownerValue AS OWNER_VAL_PRIOR
    , LeaseOwnerPrior.lockCd AS FREEZE_CD_PRIOR
	--, LeaseOwnerPrior.FUTURE_YR AS FUTURE_YR_PRIOR
	, LeaseOwnerPrior.rowUpdateUserid AS LST_UPDT_EMPL_ID_PRIOR
	, LeaseOwnerPrior.rowUpdateDt AS LST_UPDT_TS_PRIOR 
    , LeaseOwnerPrior.rowDeleteFlag AS POST_FLG_PRIOR

FROM  LeaseOwner 
LEFT OUTER JOIN LeaseOwner LeaseOwnerPrior 
ON LeaseOwner.appraisalYear = LeaseOwnerPrior.appraisalYear + 1 
	AND LeaseOwner.leaseId = LeaseOwnerPrior.leaseId 
	AND LeaseOwner.ownerId = LeaseOwnerPrior.ownerId 
	AND LeaseOwner.interestTypeCd = LeaseOwnerPrior.interestTypeCd







GO


