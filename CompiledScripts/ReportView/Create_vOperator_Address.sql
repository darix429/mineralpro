USE [MINERALPRO]
GO

DROP VIEW IF EXISTS [dbo].[vOperator_Address]

/****** Object:  View [dbo].[vOperator_Address]    Script Date: 8/10/2017 12:21:00 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vOperator_Address]
AS
SELECT b.appraisalYear
	  ,b.[operatorId]
      ,b.[operatorName]
      ,a.[ownerId]
      ,a.[name1] ownerName1
      ,a.[name2] ownerName2
      ,a.[name3] ownerName3
      ,a.[addrLine1] ownerAddressLine1
      ,a.[addrLine2] ownerAddressLine2
      ,a.[addrLine3] ownerAddressLine3
      ,a.[city] ownerCity
      ,cd1.codeName ownerState
      ,a.[zipcode] ownerZip
      ,a.[zipsort]
      ,a.[countryCd] ownerCountry
      ,a.[phoneNum] ownerPhone
      ,a.[faxNum] ownerFax
      ,a.[emailAddress] ownerEmail
      ,a.[birthDt] ownerBirthDate
      ,a.[confidentialInd]
      ,a.[ownerChangeCd]

FROM  dbo.Operator b
LEFT JOIN dbo.Owner a
	ON b.appraisalYear = a.appraisalYear 
	AND b.ownerId = a.ownerID
LEFT JOIN dbo.vSystemCode_and_Type cd1
	ON a.stateCd = cd1.code
	AND a.appraisalYear = cd1.appraisalYear
	AND cd1.codeTypeDesc = 'STATE CODES'









GO


