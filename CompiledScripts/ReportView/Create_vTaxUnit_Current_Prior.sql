USE [MINERALPRO]
GO

DROP VIEW IF EXISTS [dbo].[vTaxUnit_Current_Prior]

/****** Object:  View [dbo].[vTaxUnit_Current_Prior]    Script Date: 8/10/2017 12:23:08 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vTaxUnit_Current_Prior]
AS
SELECT  CASE WHEN dbo.TaxUnit.appraisalYear IS NULL 
             THEN TaxUnitPrior.appraisalYear + 1
			 ELSE dbo.TaxUnit.appraisalYear
			 END AS appraisalYearMaster 
                      
		, CASE WHEN dbo.TaxUnit.taxUnitTypeCd IS NULL 
              THEN TaxUnitPrior.taxUnitTypeCd 
			  ELSE dbo.TaxUnit.taxUnitTypeCd 
			  END AS taxUnitTypeCdMaster  
                      
		, CASE WHEN dbo.TaxUnit.taxUnitCd IS NULL 
               THEN TaxUnitPrior.taxUnitCd 
			   ELSE dbo.TaxUnit.taxUnitCd 
			   END AS taxUnitCdMaster 
			    
         , CASE WHEN dbo.TaxUnit.taxingInd IS NULL 
                THEN TaxUnitPrior.taxingInd 
				ELSE dbo.TaxUnit.taxingInd 
				END AS taxingIndMaster 

         , dbo.TaxUnit.appraisalYear
		 , dbo.TaxUnit.taxUnitTypeCd
		 , dbo.TaxUnit.taxUnitCd
		 --, dbo.TaxUnit.SPTD_TAX_NUM 
         , dbo.TaxUnit.taxUnitAbbr
		 , dbo.TaxUnit.estimatedTaxRate
		 , dbo.TaxUnit.taxingInd
		 --, dbo.TaxUnit.FUTURE_YR  
         , dbo.TaxUnit.rowUpdateUserid
		 , dbo.TaxUnit.rowUpdateDt
		 , dbo.TaxUnit.rowDeleteFlag
         , TaxUnitPrior.appraisalYear AS APPRAISAL_YR_PRIOR
		 , TaxUnitPrior.taxUnitTypeCd AS JURIS_TYP_CD_PRIOR 
         , TaxUnitPrior.taxUnitCd AS TAX_JURIS_CD_PRIOR
		 --, TaxUnitPrior.SPTD_TAX_NUM AS SPTD_TAX_NUM_PRIOR 
         , TaxUnitPrior.taxUnitAbbr AS SHORT_NAME_PRIOR
		 , TaxUnitPrior.estimatedTaxRate AS TAX_RATE_PRIOR  
         , TaxUnitPrior.taxingInd AS LIST_ON_NOTICE_IND_PRIOR
		 --, TaxUnitPrior.FUTURE_YR AS FUTURE_YR_PRIOR 
         , TaxUnitPrior.rowUpdateUserid AS LST_UPDT_EMPL_ID_PRIOR
		 , TaxUnitPrior.rowUpdateDt AS LST_UPDT_TS_PRIOR 
         , TaxUnitPrior.rowDeleteFlag AS POST_FLG_PRIOR

FROM  dbo.TaxUnit 
LEFT OUTER JOIN  dbo.TaxUnit TaxUnitPrior 
	ON  dbo.TaxUnit.appraisalYear = (TaxUnitPrior.appraisalYear + 1)
	--ON  dbo.TaxUnit.appraisalYear = TaxUnitPrior.appraisalYear 
	AND  dbo.TaxUnit.taxUnitTypeCd = TaxUnitPrior.taxUnitTypeCd 
	AND dbo.TaxUnit.taxUnitCd = TaxUnitPrior.taxUnitCd






GO


