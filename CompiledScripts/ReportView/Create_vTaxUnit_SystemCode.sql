USE [MINERALPRO]
GO

DROP VIEW IF EXISTS  [dbo].[vTaxUnit_SystemCode] 

/****** Object:  View [dbo].[vTaxUnit_SystemCode]    Script Date: 8/10/2017 12:23:47 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO






CREATE VIEW  [dbo].[vTaxUnit_SystemCode]  
AS
SELECT SystemCode.shortDescription LEGACY_VALUE_STRINGS
 , LeaseTaxUnit.appraisalYear APPRAISAL_YR
 , LeaseTaxUnit.leaseId ACCOUNT_NUM
 , TaxUnit.taxingInd LIST_ON_NOTICE_IND

 FROM   (MINERALPRO.dbo.LeaseTaxUnit LeaseTaxUnit 

 INNER JOIN MINERALPRO.dbo.SystemCode SystemCode 
 ON ((LeaseTaxUnit.appraisalYear =SystemCode.appraisalYear) 
 AND (LeaseTaxUnit.taxUnitTypeCd = SystemCode.codeType)) 
 AND (LeaseTaxUnit.taxUnitCd = SystemCode.code))
 
 INNER JOIN MINERALPRO.dbo.TaxUnit TaxUnit
  ON ((LeaseTaxUnit.appraisalYear =TaxUnit.appraisalYear) 
  AND (LeaseTaxUnit.taxUnitTypeCd = TaxUnit.taxUnitTypeCd)) 
  AND (LeaseTaxUnit.taxUnitCd = TaxUnit.taxUnitCd)

 --WHERE  LeaseTaxUnit.appraisalYear =2016 
	--AND LeaseTaxUnit.leaseId = '10000' 
	--AND TaxUnit.taxingInd = 'Y'

 --ORDER BY SystemCode.shortDescription









GO


