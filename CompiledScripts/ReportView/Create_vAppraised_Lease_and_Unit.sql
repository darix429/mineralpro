USE [MINERALPRO]
GO

DROP VIEW IF EXISTS [dbo].[vAppraised_Lease_and_Unit]

/****** Object:  View [dbo].[vAppraised_Lease_and_Unit]    Script Date: 8/10/2017 12:18:03 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW [dbo].[vAppraised_Lease_and_Unit]
AS
SELECT DISTINCT idAppraisal, appraisalYear, idAccount, accountName, accountNum, unitInd, codeDesc, wellTypeCd, operatorID
	
	FROM (
	SELECT a.appraisalYear, a.idAppraisal, c.idLease idAccount, c.leaseName accountName
		, c.leaseId accountNum, 'N' unitInd, b.codeDesc, wellTypeCd, operatorID
		FROM dbo.Appraisal a
		LEFT OUTER JOIN [dbo].[vSystemCode_and_Type] b
		ON a.subjectTypeCd = b.code
		AND a.appraisalYear = b.appraisalYear
		AND codeTypeDesc = 'SUBJECT TYPE CODES'
		AND codeDesc = 'LEASE'
		LEFT OUTER JOIN dbo.Lease c
		ON a.appraisalYear = c.appraisalYear
		AND a.subjectId = c.leaseId
		-- what is this unitId
		WHERE unitId <= 0 -- AND operatorId > 0
	UNION
	SELECT  d.appraisalYear, d.idAppraisal, f.idUnit idAccount, f.unitName accountName
		,f.unitId accountNum, 'Y' unitInd,  e.codeDesc, wellTypeCd, operatorID 
		FROM dbo.Appraisal d
		LEFT  OUTER JOIN [dbo].[vSystemCode_and_Type] e
		ON d.subjectTypeCd = e.code
		AND d.appraisalYear = e.appraisalYear
		AND e.codeTypeDesc = 'SUBJECT TYPE CODES'
		AND e.codeDesc = 'UNIT'
		lefT OUTER JOIN dbo.Unit f
		ON d.appraisalYear = f.appraisalYear
		AND d.subjectId = f.unitId
		--WHERE operatorId > 0
	) b

























GO

