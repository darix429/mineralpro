USE [MINERALPRO]
GO

DROP VIEW IF EXISTS [dbo].[vOperator_Name_by_Lease]

/****** Object:  View [dbo].[vOperator_Name_By_Lease]    Script Date: 8/13/2017 11:40:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vOperator_Name_by_Lease]
AS

SELECT a.leaseId
	   , a.unitId
	   , a.operatorId
	   , c.operatorName
	   , a.appraisalYear
FROM (
	SELECT a.leaseId
		   , a.unitId
		   , a.operatorId
		   , a.appraisalYear
	FROM  dbo.Lease a
	WHERE unitId = 0

	UNION

	SELECT a.leaseId
		   , a.unitId
		   , b.operatorId
		   , a.appraisalYear
	FROM  dbo.Lease a
	JOIN dbo.Unit b
		ON a.unitId = b.unitId
		AND a.appraisalYear = b.appraisalYear
		AND a.unitId <> 0
) a
LEFT JOIN dbo.Operator c
	ON a.operatorId = c.operatorId
	AND a.appraisalYear = c.appraisalYear






GO


