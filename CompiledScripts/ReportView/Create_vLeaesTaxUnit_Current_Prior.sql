USE [MINERALPRO]
GO

DROP VIEW IF EXISTS [dbo].[vLeaseTaxUnit_Current_Prior]

/****** Object:  View [dbo].[vLeaseTaxUnit_Current_Prior]    Script Date: 8/10/2017 12:20:36 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE VIEW [dbo].[vLeaseTaxUnit_Current_Prior]
AS
SELECT     
	CASE WHEN LeaseTaxUnit.appraisalYear IS NULL 
         THEN LeaseTaxUnitPrior.appraisalYear + 1 
		 ELSE LeaseTaxUnit.appraisalYear 
		 END AS appraisalYearMaster 

    ,CASE WHEN LeaseTaxUnit.taxUnitTypeCd IS NULL 
         THEN LeaseTaxUnitPrior.taxUnitTypeCd
		 ELSE LeaseTaxUnit.taxUnitTypeCd 
		 END AS taxUnitTypeCdMaster 

    ,CASE WHEN LeaseTaxUnit.taxUnitCd IS NULL 
         THEN LeaseTaxUnitPrior.taxUnitCd
		 ELSE LeaseTaxUnit.taxUnitCd 
		 END AS taxUnitCdMaster 
    
	,CASE WHEN LeaseTaxUnit.leaseId IS NULL 
         THEN LeaseTaxUnitPrior.leaseId 
		 ELSE LeaseTaxUnit.leaseId 
		 END AS leaseIdMaster 

    , LeaseTaxUnit.leaseId
	, LeaseTaxUnit.appraisalYear
	, LeaseTaxUnit.taxUnitTypeCd
	, LeaseTaxUnit.taxUnitCd
	--, LeaseTaxUnit.JURIS_VAL
	, LeaseTaxUnit.taxUnitPercent
	--, LeaseTaxUnit.FUTURE_YR
	, LeaseTaxUnit.rowUpdateUserid
	, LeaseTaxUnit.rowUpdateDt
    , LeaseTaxUnit.rowDeleteFlag
	, LeaseTaxUnit.idLeaseTaxUnit
	, LeaseTaxUnitPrior.idLeaseTaxUnit idLeaseTaxUnit_PRIOR
	, LeaseTaxUnitPrior.leaseId AS ACCOUNT_NUM_PRIOR 
    , LeaseTaxUnitPrior.appraisalYear AS APPRAISAL_YR_PRIOR  
    , LeaseTaxUnitPrior.taxUnitTypeCd AS JURIS_TYP_CD_PRIOR
	, LeaseTaxUnitPrior.taxUnitCd AS JURIS_CD_PRIOR 
    --, LeaseTaxUnitPrior.JURIS_VAL AS JURIS_VAL_PRIOR
	, LeaseTaxUnitPrior.taxUnitPercent AS JURIS_PCT_PRIOR 
    , LeaseTaxUnitPrior.rowUpdateUserid AS LST_UPDT_EMPL_ID_PRIOR 
    , LeaseTaxUnitPrior.rowUpdateDt AS LST_UPDT_TS_PRIOR
	, LeaseTaxUnitPrior.rowDeleteFlag AS POST_FLG_PRIOR 
    --,LeaseTaxUnitPrior.FUTURE_YR AS FUTURE_YR_PRIOR

FROM  LeaseTaxUnit 
LEFT OUTER JOIN LeaseTaxUnit LeaseTaxUnitPrior 
ON  LeaseTaxUnit.appraisalYear = LeaseTaxUnitPrior.appraisalYear + 1
 AND  LeaseTaxUnit.taxUnitCd = LeaseTaxUnitPrior.taxUnitCd 
 AND  LeaseTaxUnit.taxUnitTypeCd = LeaseTaxUnitPrior.taxUnitTypeCd 
 AND  LeaseTaxUnit.leaseId = LeaseTaxUnitPrior.leaseId






GO


