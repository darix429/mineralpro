USE [MINERALPRO]
GO

DROP VIEW IF EXISTS [dbo].[vAppraisalPriorYear]  

/****** Object:  View [dbo].[vAppraisalPriorYear]    Script Date: 8/10/2017 12:17:16 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE VIEW  [dbo].[vAppraisalPriorYear]  
AS
SELECT     a.appraisalYear, a.totalValue, a.workingInterestTotalValue
		 , a.royaltyInterestTotalValue, a.subjectId,  a.subjectTypeCd
		 , a.rrcNumber
FROM      dbo.Appraisal  a
JOIN 
	(SELECT     (appraisalYear - 1) priorYear
	   FROM     dbo.AppraisalYear 
	   WHERE    currentInd = 'Y'
	) b
ON appraisalYear = priorYear







GO

