USE [MINERALPRO]
GO

DROP VIEW IF EXISTS [dbo].[vLease_Current_Prior]

/****** Object:  View [dbo].[vLease_Current_Prior]    Script Date: 8/10/2017 12:18:35 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE VIEW [dbo].[vLease_Current_Prior]
AS
SELECT     
	CASE WHEN dbo.Lease.appraisalYear IS NULL 
	   THEN (LeasePrior.appraisalYear + 1) ELSE dbo.Lease.appraisalYear 
	   END AS appraisalYearMaster
	, CASE WHEN dbo.Lease.leaseId IS NULL 
	   THEN LeasePrior.leaseId ELSE dbo.Lease.leaseId 
	   END AS leaseIdMaster 
	--CASE WHEN dbo.Lease.TAX_OBJ_ID IS NULL 
	--   THEN LeasePrior.TAX_OBJ_ID ELSE dbo.Lease.TAX_OBJ_ID 
	--   END AS TAX_OBJ_ID_MASTER
    , Lease.appraisalYear 
    , Lease.leaseId
	--, Lease.TAX_OBJ_ID
	, Lease.divisionOrderDescription
	, Lease.description
	, Lease.comment
	, Lease.operatorId
	, Lease.leaseYear
	, Lease.changeReasonCd
	, Lease.acres
	, LeaseValueSummary.royaltyInterestPercent   
	, LeaseValueSummary.workingInterestPercent
	, LeaseValueSummary.overridingRoyaltyPercent
	, LeaseValueSummary.oilPaymentPercent
	--, LeaseValueSummary.LEASE_DO_CNT
	, LeaseValueSummary.royaltyInterestValue
	, LeaseValueSummary.workingInterestValue
	--, Lease.RRC_NUM
	, LeaseValueSummary.tractNum
	, LeaseValueSummary.unitWorkingInterestPercent
	, LeaseValueSummary.unitRoyaltyInterestPercent
	--, Lease.FUTURE_YR
	, Lease.rowUpdateUserid
	, Lease.rowUpdateDt
	, Lease.rowDeleteFlag
	, Lease.idLease
	, LeasePrior.idLease idLease_PRIOR

	, LeasePrior.appraisalYear AS APPRAISAL_YR_PRIOR
	, LeasePrior.leaseId AS ACCOUNT_NUM_PRIOR 
    --, LeasePrior.TAX_OBJ_ID AS TAX_OBJ_PRIOR ** same as ACCOUNT_NUM
	, LeasePrior.divisionOrderDescription AS PIPELINE_PRIOR 
    , LeasePrior.description AS LEASE_DESC_PRIOR
	, LeasePrior.comment AS LEASE_NOT_PRIOR 
    , LeasePrior.operatorId AS OPERATOR_NUM_PRIOR
	, LeasePrior.leaseYear AS LEASE_YR_PRIOR 
    , LeasePrior.changeReasonCd AS LEASE_CHG_CD_PRIOR
	, LeasePrior.acres AS LEASE_ACRES_PRIOR 
    , LeaseValueSummaryPrior.royaltyInterestPercent AS LEASE_RI_PCT_PRIOR
	, LeaseValueSummaryPrior.workingInterestPercent AS LEASE_WI_PCT_PRIOR 
    , LeaseValueSummaryPrior.overridingRoyaltyPercent AS LEASE_OR_PCT_PRIOR
	, LeaseValueSummaryPrior.oilPaymentPercent AS LEASE_OP_PCT_PRIOR 
    --, LeasePrior.LEASE_DO_CNT AS LEASE_DO_CNT_PRIOR
	, LeaseValueSummaryPrior.royaltyInterestValue AS LEASE_RI_VALUE_PRIOR 
    , LeaseValueSummaryPrior.workingInterestValue AS LEASE_WI_VALUE_PRIOR
	--, LeasePrior.RRC_NUM AS RRC_NUM_PRIOR 
    , LeaseValueSummaryPrior.tractNum AS TRACT_NUM_PRIOR
	, LeaseValueSummaryPrior.unitWorkingInterestPercent AS UNIT_WI_PCT_PRIOR 
    , LeaseValueSummaryPrior.unitRoyaltyInterestPercent AS UNIT_RI_PCT_PRIOR
	--, LeasePrior.FUTURE_YR AS FUTURE_YR_PRIOR 
    , LeasePrior.rowUpdateUserid AS LST_UPDT_EMPL_ID_PRIOR
	, LeasePrior.rowUpdateDt AS LST_UPDT_TS_PRIOR 
    , LeasePrior.rowDeleteFlag AS POST_FLG_PRIOR

FROM     dbo.Lease

JOIN dbo.LeaseValueSummary LeaseValueSummary  
 ON dbo.Lease.appraisalYear =  LeaseValueSummary.appraisalYear  
   AND dbo.Lease.leaseId = LeaseValueSummary.leaseId 


LEFT OUTER JOIN dbo.Lease LeasePrior
 ON dbo.Lease.appraisalYear = (LeasePrior.appraisalYear + 1)
   AND dbo.Lease.leaseId = LeasePrior.leaseId 
   --AND dbo.Lease.TAX_OBJ_ID = LeasePrior.TAX_OBJ_ID

JOIN dbo.LeaseValueSummary LeaseValueSummaryPrior 
 ON LeasePrior.appraisalYear =  LeaseValueSummaryPrior.appraisalYear 
   AND LeasePrior.leaseId = LeaseValueSummaryPrior.leaseId 





GO


