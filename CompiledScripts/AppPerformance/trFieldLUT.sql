Use MINERALPRO
GO

IF (OBJECT_ID('trFieldInsertLUT') IS NOT NULL)
  DROP TRIGGER dbo.trFieldInsertLUT
GO

CREATE TRIGGER dbo.trFieldInsertLUT
   ON  dbo.Field
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('Field')  
END
GO

IF (OBJECT_ID('trFieldUpdateLUT') IS NOT NULL)
  DROP TRIGGER dbo.trFieldUpdateLUT
GO

CREATE TRIGGER dbo.trFieldUpdateLUT
   ON  dbo.Field
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('Field')
END
GO
