Use MINERALPRO
GO

IF (OBJECT_ID('trAgencyInsertLUT') IS NOT NULL)
  DROP TRIGGER dbo.trAgencyInsertLUT
GO

CREATE TRIGGER dbo.trAgencyInsertLUT
   ON  dbo.Agency
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('Agency')  
END
GO

IF (OBJECT_ID('trAgencyUpdateLUT') IS NOT NULL)
  DROP TRIGGER dbo.trAgencyUpdateLUT
GO

CREATE TRIGGER dbo.trAgencyUpdateLUT
   ON  dbo.Agency
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('Agency')
END
GO
