Use MINERALPRO
GO

IF (OBJECT_ID('trOwnerInsertLUT') IS NOT NULL)
  DROP TRIGGER dbo.trOwnerInsertLUT
GO

CREATE TRIGGER dbo.trOwnerInsertLUT
   ON  dbo.Owner
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('Owner')  
END
GO

IF (OBJECT_ID('trOwnerUpdateLUT') IS NOT NULL)
  DROP TRIGGER dbo.trOwnerUpdateLUT
GO

CREATE TRIGGER dbo.trOwnerUpdateLUT
   ON  dbo.Owner
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('Owner')
END
GO
