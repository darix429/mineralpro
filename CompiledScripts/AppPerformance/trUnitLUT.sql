Use MINERALPRO
GO

IF (OBJECT_ID('trUnitInsertLUT') IS NOT NULL)
  DROP TRIGGER dbo.trUnitInsertLUT
GO

CREATE TRIGGER dbo.trUnitInsertLUT
   ON  dbo.Unit
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('Unit', 'Operator')  
END
GO

IF (OBJECT_ID('trUnitUpdateLUT') IS NOT NULL)
  DROP TRIGGER dbo.trUnitUpdateLUT
GO

CREATE TRIGGER dbo.trUnitUpdateLUT
   ON  dbo.Unit
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('Unit', 'Operator')
END
GO
