Use MINERALPRO
GO

IF (OBJECT_ID('trLeaseInsertLUT') IS NOT NULL)
  DROP TRIGGER dbo.trLeaseInsertLUT
GO

CREATE TRIGGER dbo.trLeaseInsertLUT
   ON  dbo.Lease
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('Lease', 'Operator', 'Unit')  
END
GO

IF (OBJECT_ID('trLeaseUpdateLUT') IS NOT NULL)
  DROP TRIGGER dbo.trLeaseUpdateLUT
GO

CREATE TRIGGER dbo.trLeaseUpdateLUT
   ON  dbo.Lease
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('Lease', 'Operator', 'Unit')
END
GO
