Use MINERALPRO
GO

IF (OBJECT_ID('trOperatorInsertLUT') IS NOT NULL)
  DROP TRIGGER dbo.trOperatorInsertLUT
GO

CREATE TRIGGER dbo.trOperatorInsertLUT
   ON  dbo.Operator
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('Operator','Owner')
END
GO

IF (OBJECT_ID('trOperatorUpdateLUT') IS NOT NULL)
  DROP TRIGGER dbo.trOperatorUpdateLUT
GO

CREATE TRIGGER dbo.trOperatorUpdateLUT
   ON  dbo.Operator
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('Operator','Owner') 
END
GO
