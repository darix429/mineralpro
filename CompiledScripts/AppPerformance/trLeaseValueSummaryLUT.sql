Use MINERALPRO
GO

IF (OBJECT_ID('trLeaseValueSummaryInsertLUT') IS NOT NULL)
  DROP TRIGGER dbo.trLeaseValueSummaryInsertLUT
GO

CREATE TRIGGER dbo.trLeaseValueSummaryInsertLUT
   ON  dbo.LeaseValueSummary
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('LeaseValueSummary', 'Lease')  
END
GO

IF (OBJECT_ID('trLeaseValueSummaryUpdateLUT') IS NOT NULL)
  DROP TRIGGER dbo.trLeaseValueSummaryUpdateLUT
GO

CREATE TRIGGER dbo.trLeaseValueSummaryUpdateLUT
   ON  dbo.LeaseValueSummary
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('LeaseValueSummary', 'Lease')
END
GO
