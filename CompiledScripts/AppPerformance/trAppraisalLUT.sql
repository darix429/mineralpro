Use MINERALPRO
GO

IF (OBJECT_ID('trAppraisalInsertLUT') IS NOT NULL)
  DROP TRIGGER dbo.trAppraisalInsertLUT
GO

CREATE TRIGGER dbo.trAppraisalInsertLUT
   ON  dbo.Appraisal
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('Appraisal', 'Field', 'Lease')  
END
GO

IF (OBJECT_ID('trAppraisalUpdateLUT') IS NOT NULL)
  DROP TRIGGER dbo.trAppraisalUpdateLUT
GO

CREATE TRIGGER dbo.trAppraisalUpdateLUT
   ON  dbo.Appraisal
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('Appraisal', 'Field', 'Lease')
END
GO
