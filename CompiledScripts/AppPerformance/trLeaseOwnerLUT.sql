Use MINERALPRO
GO

IF (OBJECT_ID('trLeaseOwnerInsertLUT') IS NOT NULL)
  DROP TRIGGER dbo.trLeaseOwnerInsertLUT
GO

CREATE TRIGGER dbo.trLeaseOwnerInsertLUT
   ON  dbo.LeaseOwner
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('LeaseOwner','Owner')
END
GO

IF (OBJECT_ID('trLeaseOwnerUpdateLUT') IS NOT NULL)
  DROP TRIGGER dbo.trLeaseOwnerUpdateLUT
GO

CREATE TRIGGER dbo.trLeaseOwnerUpdateLUT
   ON  dbo.LeaseOwner
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('LeaseOwner','Owner')
END
GO