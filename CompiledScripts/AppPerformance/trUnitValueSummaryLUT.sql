Use MINERALPRO
GO

IF (OBJECT_ID('trUnitValueSummaryInsertLUT') IS NOT NULL)
  DROP TRIGGER dbo.trUnitValueSummaryInsertLUT
GO

CREATE TRIGGER dbo.trUnitValueSummaryInsertLUT
   ON  dbo.UnitValueSummary
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('UnitValueSummary', 'Unit')  
END
GO

IF (OBJECT_ID('trUnitValueSummarytUpdateLUT') IS NOT NULL)
  DROP TRIGGER dbo.trUnitValueSummarytUpdateLUT
GO

CREATE TRIGGER dbo.trUnitValueSummarytUpdateLUT
   ON  dbo.UnitValueSummary
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON
	UPDATE LastUpdateTime SET LastUpdateTime = getdate()
	WHERE tableName in ('UnitValueSummary', 'Unit')
END
GO
