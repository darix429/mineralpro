USE [MINERALPRO]
GO

/****** Object:  Table [dbo].[LastUpdateTime]    Script Date: 10/20/2017 10:02:42 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[LastUpdateTime](
	[idLastUpdateTime] [bigint] IDENTITY(1,1) NOT NULL,
	[tableName] [varchar](50) NOT NULL CONSTRAINT [DF_LastUpdateTime_appName]  DEFAULT (''),
	[currentUserId] [varchar](50) NOT NULL CONSTRAINT [DF_LastUpdateTime_displayName]  DEFAULT (''),
	[lastUpdateTime] [datetime] NOT NULL CONSTRAINT [DF_LastUpdateTime_lastUpdateTime]  DEFAULT (getdate()),
	[dataRefreshTime] [datetime] NOT NULL CONSTRAINT [DF_LastUpdateTime_dataRefreshTime]  DEFAULT (getdate()),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_LastUpdateTime_rowUpdateUserid]  DEFAULT ((0)),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_LastUpdateTime_rowUpdateDt]  DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_LastUpdateTime_rowDeleteFlag]  DEFAULT (''),
 CONSTRAINT [PK_LastUpdateTime] PRIMARY KEY CLUSTERED 
(
	[idLastUpdateTime] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO


