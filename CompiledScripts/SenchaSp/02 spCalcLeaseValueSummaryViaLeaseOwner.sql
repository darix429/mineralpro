  DROP PROCEDURE spCalcLeaseValueSummaryViaLeaseOwner

USE MINERALPRO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE spCalcLeaseValueSummaryViaLeaseOwner
	@leaseId varchar(15) = '',
	@appraisalYear varchar(4) = ''
AS
BEGIN
	SET NOCOUNT ON;

	IF OBJECT_ID (N'#forUpate', N'U') IS NOT NULL 
	DROP TABLE #forUpate

SELECT
	leaseId,
	interestTypeCd, 
	SUM(interestPercent) interestPercent
	INTO #forUpate
FROM LeaseOwner
WHERE leaseId = @leaseId
AND appraisalYear = @appraisalYear
AND rowDeleteFlag = ''
GROUP BY leaseId, interestTypeCd


  --type: 1-royalty; 2-overriding; 3-oilpayment; 4-working

  UPDATE LeaseValueSummary
  SET 
	  royaltyInterestPercent = (SELECT ISNULL(SUM(interestPercent),0) 
					FROM #forUpate WHERE interestTypeCd = 1 AND leaseId = lvs.leaseId),
	  overridingRoyaltyPercent = (SELECT ISNULL(SUM(interestPercent),0)	
					FROM #forUpate WHERE interestTypeCd = 2 AND leaseId = lvs.leaseId),
	  oilPaymentPercent = (SELECT ISNULL(SUM(interestPercent),0)
					FROM #forUpate WHERE interestTypeCd = 3 AND leaseId = lvs.leaseId),
	  workingInterestPercent = (SELECT ISNULL(SUM(interestPercent),0) 
					FROM #forUpate WHERE interestTypeCd = 4 AND leaseId = lvs.leaseId),
	  workingInterestValue = (SELECT ISNULL(SUM(interestPercent),0)  * lvs.appraisedValue 
					FROM #forUpate WHERE interestTypeCd = 4 AND leaseId = lvs.leaseId),
	  royaltyInterestValue = (SELECT ISNULL(SUM(interestPercent),0) * lvs.appraisedValue 
					FROM #forUpate WHERE interestTypeCd <> 4 AND leaseId = lvs.leaseId)
  FROM LeaseValueSummary lvs
  WHERE lvs.leaseId = @leaseId
  AND appraisalYear = @appraisalYear
  AND rowDeleteFlag = ''

END
GO
