USE [MINERALPRO]
GO

/****** Object:  StoredProcedure [dbo].[spDeleteAppUser]    Script Date: 9/25/2017 9:26:25 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spDeleteSubSystemFunction]
 @idSubSystemFunction int,
 @idAppUser int
AS
BEGIN
	UPDATE FunctionSecurity
	SET rowDeleteFlag = 'D',
		rowUpdateUserid = @idAppUser
	WHERE idSubSystemFunction = @idSubSystemFunction

	DELETE UserFunction
	FROM AppUserFavorite
	WHERE idSubSystemFunction = @idSubSystemFunction

END



GO


