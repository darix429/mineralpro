USE MINERALPRO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE spCalcUnitValueSummaryViaLeaseValueSummary
	@unitId varchar(15) = '',
	@appraisalYear varchar(4) = '',
	@rowUpdateUserid varchar (25)
AS
BEGIN
	UPDATE unitValueSummary SET 
        royaltyInterestPercent = (
			SELECT SUM(unitRoyaltyInterestPercent) 
			FROM leaseValueSummary 
			WHERE leaseId IN(
				SELECT leaseId FROM Lease WHERE unitId = @unitId AND appraisalYear = @appraisalYear AND rowDeleteFlag = '')  
			AND appraisalYear = @appraisalYear  AND rowDeleteFlag = ''), 
        workingInterestpercent = (
			SELECT SUM(unitWorkingInterestPercent) 
			FROM leaseValueSummary WHERE leaseId IN(
				SELECT leaseId FROM Lease WHERE unitId = @unitId AND appraisalYear = @appraisalYear AND rowDeleteFlag = '')  
			AND appraisalYear = @appraisalYear  AND rowDeleteFlag = ''), 
        weightedWorkingInterestpercent = (
			SELECT SUM(workingInterestPercent * unitWorkingInterestPercent ) 
			FROM leaseValueSummary WHERE leaseId IN(
				SELECT leaseId FROM Lease WHERE unitId = @unitId AND appraisalYear = @appraisalYear AND rowDeleteFlag = '')  
			AND appraisalYear = @appraisalYear  AND rowDeleteFlag = ''), 
        weightedRoyaltyInterestPercent = (
			SELECT SUM(royaltyInterestPercent * unitRoyaltyInterestPercent) 
			FROM leaseValueSummary WHERE leaseId IN(
				SELECT leaseId FROM Lease WHERE unitId = @unitId AND appraisalYear = @appraisalYear AND rowDeleteFlag = '')  
			AND appraisalYear = @appraisalYear  AND rowDeleteFlag = ''), 
		workingInterestValue = (
			SELECT SUM(workingInterestValue) 
			FROM leaseValueSummary WHERE leaseId IN(
				SELECT leaseId FROM Lease WHERE unitId = @unitId AND appraisalYear = @appraisalYear AND rowDeleteFlag = '')  
			AND appraisalYear = @appraisalYear  AND rowDeleteFlag = ''), 
		royaltyInterestValue = (
			SELECT SUM(royaltyInterestValue) 
			FROM leaseValueSummary WHERE leaseId IN(
				SELECT leaseId FROM Lease WHERE unitId = @unitId AND appraisalYear = @appraisalYear AND rowDeleteFlag = '')  
			AND appraisalYear = @appraisalYear  AND rowDeleteFlag = ''), 
        rowUpdateDt = GETDATE(), 
        rowUpdateUserid = @rowUpdateUserid 
    WHERE unitId = @unitId ;
END
GO