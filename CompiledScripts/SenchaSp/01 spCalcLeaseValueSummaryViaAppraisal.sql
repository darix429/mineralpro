USE MINERALPRO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE spCalcLeaseValueSummaryViaAppraisal
	@leaseId varchar(15) = '',
	@appraisalYear varchar(4) = ''
AS
BEGIN
	UPDATE LeaseValueSummary
	SET appraisedValue = a.totalValue,
		workingInterestValue = a.workingInterestTotalValue,
		royaltyInterestValue = a.royaltyInterestTotalValue
	FROM LeaseValueSummary lvs
	INNER JOIN (
		   SELECT 
			   subjectId,
			   appraisalYear,
			   SUM(totalValue) totalValue,
			   SUM(workingInterestTotalValue) workingInterestTotalValue,
			   SUM(royaltyInterestTotalValue) royaltyInterestTotalValue
			FROM Appraisal
			WHERE subjectTypeCd = 2
				  AND subjectId = @leaseId
				  AND appraisalYear = @appraisalYear
				  AND rowDeleteFlag = ''
			GROUP BY subjectId, appraisalYear) a 
	ON lvs.leaseId = a.subjectId
	AND lvs.appraisalYear = a.appraisalYear
	AND lvs.rowDeleteFlag = ''
END
GO