
USE MINERALPRO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE spUpdateNoteViaAppraisal
	@subjectId varchar(15) = '',
	@subjectTypeCd varchar(1) = '',
	@appraisalYear varchar(4)= ''
AS
BEGIN
	
UPDATE Note
SET rrcNumber = a.rrcNumber
FROM Note n
INNER JOIN Appraisal a ON a.subjectId = n.subjectId
	AND a.subjectTypeCd = n.subjectTypeCd
	AND a.appraisalYear =n.referenceAppraisalYear
	AND a.rowDeleteFlag = ''
WHERE n.subjectId = @subjectId
	AND n.subjectTypeCd = @subjectTypeCd
	AND n.referenceAppraisalYear = @appraisalYear
	AND n.rowDeleteFlag = ''
END
GO
