USE MINERALPRO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE spCalcUnitValueSummaryViaAppraisal
	@unitId varchar(15) = '',
	@appraisalYear varchar(4) = ''
AS
BEGIN
	UPDATE UnitValueSummary
	SET workingInterestValue = a.workingInterestTotalValue,
		royaltyInterestValue = a.royaltyInterestTotalValue
	FROM UnitValueSummary uvs
	INNER JOIN (
		   SELECT 
			   subjectId,
			   appraisalYear,
			   SUM(workingInterestTotalValue) workingInterestTotalValue,
			   SUM(royaltyInterestTotalValue) royaltyInterestTotalValue
			FROM Appraisal
			WHERE subjectTypeCd = 1
				  AND subjectId = @unitId
				  AND appraisalYear = @appraisalYear
				  AND rowDeleteFlag = ''
			GROUP BY subjectId, appraisalYear) a 
	ON uvs.unitId = a.subjectId
	AND uvs.appraisalYear = a.appraisalYear
	AND uvs.rowDeleteFlag = ''
END
GO