USE [MINERALPRO]
GO

/****** Object:  StoredProcedure [dbo].[spCalcAmortizationYear]    Script Date: 9/25/2017 8:42:45 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[spDeleteAppUser]
 @deleteIdAppUser int,
 @activeIdAppUser int
AS
BEGIN
	UPDATE UserOrganization
	SET rowDeleteFlag = 'D',
		rowUpdateUserid = @activeIdAppUser
	WHERE idAppUser = @deleteIdAppUser

	UPDATE UserFunction
	SET rowDeleteFlag = 'D',
		rowUpdateUserid = @activeIdAppUser
	WHERE idAppUser = @deleteIdAppUser

	UPDATE UserOrgSecurity
	SET rowDeleteFlag = 'D',
		rowUpdateUserid = @activeIdAppUser
	WHERE idUserOrganization in (SELECT idUserOrganization FROM UserOrganization WHERE idAppUser = @deleteIdAppUser)

	DELETE AppUserFavorite
	FROM AppUserFavorite
	WHERE idAppUser = @deleteIdAppUser

END


GO


