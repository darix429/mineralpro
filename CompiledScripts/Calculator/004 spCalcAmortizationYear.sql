USE [MINERALPRO]
GO


IF (OBJECT_ID('spCalcAmortizationYear') IS NOT NULL)
  DROP PROCEDURE dbo.spCalcAmortizationYear
GO

CREATE PROCEDURE [dbo].[spCalcAmortizationYear]
 @queueUserid Bigint
,@yearNumber Int
,@appraisalYear Int
AS
BEGIN
	SET NOCOUNT ON

    Declare @wellTypeOil      smallint = 1
    Declare @wellTypeGas      smallint = 2
    Declare @operatingDecline smallint = 1  --OPERATING EXP DECLINE
    Declare @oilDecline       smallint = 2  --OIL DECLINE
    Declare @gasDecline       smallint = 3  --GAS DECLINE
    Declare @productDecline   smallint = 4  --PRODUCT DECLINE

--appraisalYear	subjectTypeCd	subjectId	rrcNumber	declineTypeCd	declineyears	runningTotal
--2017	1	990122	40136	3	1	1
--2017	1	990122	40136	3	1	2
--2017	1	990122	40136	3	99	101

--Example data above.
--This select is using the Running Total column to find where the current Amortization Year falls in the decline groupings.
--Year 1 uses row 1 because AmortYear=1 is less than or equal to the running total of 1
--Year 2 uses row 2 because AmortYear=2 is less than or equal to the running total of 2
--Year 3 uses row 3 because AmortYear=3 is less than or equal to the running total of 101
--Year 4 uses row 3 because AmortYear=4 is less than or equal to the running total of 101
--Year 5 uses row 3 because AmortYear=5 is less than or equal to the running total of 101

--This select pulls only the row required to calculate the current amortization year
----For each appraisal
----And for each decline type
------Oil
------Gas
------Product
------Operating Expense

    Select cs.[appraisalYear]
         , cs.[subjectTypeCd]
         , cs.[subjectId]
         , cs.[rrcNumber]
         , decl.declineTypeCd
         , decl.declinePercent
      Into #decline
      From #CalcAppraisalSummary cs
      Left Join #CalcDeclineSchedule decl
        On decl.queueUserid = cs.queueUserid
       And decl.appraisalYear = cs.appraisalYear
       And decl.subjectTypeCd = cs.subjectTypeCd
       And decl.subjectId = cs.subjectId
       And decl.rrcNumber = cs.rrcNumber
       And decl.seqNumber = (Select Min(sub.seqNumber)
                               From (SELECT *
                                          , Sum(declineYears) 
                                            over(PARTITION BY 
                                            [appraisalYear]
                                          , [subjectTypeCd]
                                          , [subjectId]
                                          , [rrcNumber]
                                          , [declineTypeCd]
                                            order by 
                                            seqNumber
                                            rows unbounded preceding) as runningTotal
                                       FROM #CalcDeclineSchedule) sub
                              Where sub.subjectTypeCd = cs.subjectTypeCd
                                And sub.subjectId = cs.subjectId
                                And sub.rrcNumber = cs.rrcNumber
                                And sub.declineTypeCd = decl.declineTypeCd
                                And sub.queueUserid = @queueUserid
                                And sub.runningTotal >= @yearNumber)
     Where cs.queueUserid = @queueUserid
       And cs.appraisalYear = @appraisalYear

    CREATE NONCLUSTERED INDEX DeclineIX ON #decline
    (
	    [appraisalYear] DESC,
	    [subjectTypeCd] ASC,
	    [subjectId] ASC,
	    [rrcNumber] ASC,
	    [declineTypeCd] ASC
    )

    --Calculator runs in stages
    ----This process runs 1 time per amortization year (1-25 years)
    ----We start with an insert of calculated pre-requisite values
    ----Followed up with subsequent updates that do more pre-requisites
    ----And then a finalization update

    Delete a
      from #CalcAmortSchedule a
     Where a.queueUserid = @queueUserid
       And a.appraisalYear = @appraisalYear
       And a.yearNumber >= @yearNumber


--Calculates the initial NET prices and Estimated Annuals of each mineral type.
--Note--
--The Coalesce is using the prior CalcAmortSchedule year in the calculation
----The first time through coalesce uses the values provided by the calling process (appraisal screen or mass calc)
----Each time after, the value comes from CalcAmortSchedule (year - 1)
    INSERT INTO #CalcAmortSchedule
               ([queueUserid]
               ,[appraisalYear]
               ,[subjectTypeCd]
               ,[subjectId]
               ,[rrcNumber]
               ,[yearNumber]
               ,[netOilPrice]
               ,[netProductPrice]
               ,[netWorkingInterestGasPrice]
               ,[netRoyaltyInterestGasPrice]
               ,[estAnnualOilProduction]
               ,[estAnnualGasProduction]
               ,[estAnnualProductProduction]
               ,[operatingExpense]
               ,[dailyAverageOil]
               ,[dailyAverageGas]
               ,[dailyAverageProduct]
               ,[declinePercentOil]
               ,[declinePercentGas]
               ,[declinePercentProduct]
               )
    Select cs.queueUserid
         , cs.appraisalYear
         , cs.subjectTypeCd
         , cs.subjectId
         , cs.rrcNumber
         , @yearNumber YearNumber
         , Case @yearNumber When 1 Then dbo.udfBankRound(dbo.udfBankRound(Coalesce(cw.netOilPrice, cs.grossOilPrice) * (1.000000000 - oilTax.taxPercent),2) * (1.000000000 + oilEsc.adjPercent),2) 
                            Else dbo.udfBankRound(Coalesce(cw.netOilPrice, cs.grossOilPrice) * (1.000000000 + oilEsc.adjPercent),2) 
                            End [netOilPrice]
         , Case @yearNumber When 1 Then dbo.udfBankRound(dbo.udfBankRound(Coalesce(cw.netProductPrice, cs.grossProductPrice) * (1.000000000 - oilTax.taxPercent),2) * (1.000000000 + oilEsc.adjPercent),2) 
                            Else dbo.udfBankRound(Coalesce(cw.netProductPrice, cs.grossProductPrice) * (1.000000000 + oilEsc.adjPercent),2) 
                            End [netProductPrice]
         , Case @yearNumber When 1 Then dbo.udfBankRound(dbo.udfBankRound(Coalesce(cw.netWorkingInterestGasPrice, cs.grossWorkingInterestGasPrice) * (1.000000000 - gasTax.taxPercent),2) * (1.000000000 + gasEsc.adjPercent),2)
                            else dbo.udfBankRound(Coalesce(cw.netWorkingInterestGasPrice, cs.grossWorkingInterestGasPrice) * (1.000000000 + gasEsc.adjPercent),2)
                            End [netWorkingInterestGasPrice]
         , Case @yearNumber When 1 Then dbo.udfBankRound(dbo.udfBankRound(Coalesce(cw.netRoyaltyInterestGasPrice, cs.grossRoyaltyInterestGasPrice) * (1.000000000 - gasTax.taxPercent),2) * (1.000000000 + gasEsc.adjPercent),2) 
                            Else dbo.udfBankRound(Coalesce(cw.netRoyaltyInterestGasPrice, cs.grossRoyaltyInterestGasPrice) * (1.000000000 + gasEsc.adjPercent),2)
                            End [netRoyaltyInterestGasPrice]
         , dbo.udfBankRound((Coalesce(cw.[dailyAverageOil], cs.dailyAverageOil) * (1.000000000 - Isnull(oilDecline.declinePercent, 0))) * 365, 0) [estAnnualOilProduction]
         , dbo.udfBankRound((Coalesce(cw.[dailyAverageGas], cs.dailyAverageGas) * (1.000000000 - Isnull(gasDecline.declinePercent, 0))) * 365, 0) [estAnnualGasProduction]
         , dbo.udfBankRound((Coalesce(cw.[dailyAverageProduct], cs.dailyAverageProduct) * (1.000000000 - Isnull(prodDecline.declinePercent, 0))) * 365, 0) [estAnnualProductProduction]
         , Case @yearNumber When 1 Then dbo.udfBankRound((Isnull(cs.operatingExpense,0.0) * (1.000000000 + Isnull(opExp.adjPercent, 0.0))) * (1.000000000 - Isnull(operDecline.declinePercent, 0.0)), 0) 
                            Else dbo.udfBankRound((Isnull(cw.operatingExpense,0.0) * (1.000000000 + Isnull(opExp.adjPercent, 0.0))) * (1.000000000 - Isnull(operDecline.declinePercent, 0.0)), 0) 
                            End [operatingExpense]
         , (Coalesce(cw.[dailyAverageOil], cs.dailyAverageOil) * (1.000000000 - Isnull(oilDecline.declinePercent, 0))) [dailyAverageOil]
         , (Coalesce(cw.[dailyAverageGas], cs.dailyAverageGas) * (1.000000000 - Isnull(gasDecline.declinePercent, 0))) [dailyAverageGas]
         , (Coalesce(cw.[dailyAverageProduct], cs.dailyAverageProduct) * (1.000000000 - Isnull(prodDecline.declinePercent, 0))) [dailyAverageProduct]
         , Isnull(oilDecline.declinePercent,0.0) [declinePercentOil]
         , Isnull(gasDecline.declinePercent,0.0) [declinePercentGas]
         , Isnull(prodDecline.declinePercent,0.0) [declinePercentProduct]
      From #CalcAppraisalSummary cs
      Join [dbo].[CalcTaxAdj] oilTax
        On oilTax.appraisalYear = cs.appraisalYear
       And oilTax.wellTypeCd = @wellTypeOil
      Join [dbo].[CalcTaxAdj] gasTax
        On gasTax.appraisalYear = cs.appraisalYear
       And gasTax.wellTypeCd = @wellTypeGas
      Join [dbo].[CalcEscalatedPriceAdj] oilEsc
        On oilEsc.appraisalYear = cs.appraisalYear
       And oilEsc.wellTypeCd = @wellTypeOil
       And oilEsc.yearNumber = @yearNumber
      Join [dbo].[CalcEscalatedPriceAdj] gasEsc
        On gasEsc.appraisalYear = cs.appraisalYear
       And gasEsc.wellTypeCd = @wellTypeGas
       And gasEsc.yearNumber = @yearNumber
      Left Join #CalcAmortSchedule cw
        On cw.queueUserid = cs.queueUserid
       And cw.appraisalYear = cs.appraisalYear
       And cw.subjectTypeCd = cs.subjectTypeCd
       And cw.subjectId = cs.subjectId
       And cw.rrcNumber = cs.rrcNumber
       And cw.yearNumber = @yearNumber - 1
      Left Join #decline oilDecline
        On oilDecline.appraisalYear = cs.appraisalYear
       And oilDecline.subjectTypeCd = cs.subjectTypeCd
       And oilDecline.subjectId = cs.subjectId
       And oilDecline.rrcNumber = cs.rrcNumber
       And oilDecline.declineTypeCd = @oilDecline
      Left Join #decline gasDecline
        On gasDecline.appraisalYear = cs.appraisalYear
       And gasDecline.subjectTypeCd = cs.subjectTypeCd
       And gasDecline.subjectId = cs.subjectId
       And gasDecline.rrcNumber = cs.rrcNumber
       And gasDecline.declineTypeCd = @gasDecline
      Left Join #decline prodDecline
        On prodDecline.appraisalYear = cs.appraisalYear
       And prodDecline.subjectTypeCd = cs.subjectTypeCd
       And prodDecline.subjectId = cs.subjectId
       And prodDecline.rrcNumber = cs.rrcNumber
       And prodDecline.declineTypeCd = @productDecline
      Left Join #decline operDecline
        On operDecline.appraisalYear = cs.appraisalYear
       And operDecline.subjectTypeCd = cs.subjectTypeCd
       And operDecline.subjectId = cs.subjectId
       And operDecline.rrcNumber = cs.rrcNumber
       And operDecline.declineTypeCd = @operatingDecline
      Left Join [dbo].[CalcEscalatedOpExpAdj] opExp
        On opExp.appraisalYear = cs.appraisalYear
       And opexp.yearNumber = @yearNumber
     Where cs.appraisalYear = @appraisalYear
       And cs.queueUserid = @queueUserid

--Calculate gross revenues
    Update cw
       Set cw.grossWorkingInterestOilRevenue = dbo.udfBankRound(cw.estAnnualOilProduction * cs.workingInterestPercent * cw.netOilPrice, 0)
         , cw.grossRoyaltyInterestOilRevenue = dbo.udfBankRound(cw.estAnnualOilProduction * cs.royaltyInterestPercent * cw.netOilPrice, 0)
         , cw.grossWorkingInterestGasRevenue = dbo.udfBankRound(cw.estAnnualGasProduction * cs.workingInterestPercent * cw.netWorkingInterestGasPrice, 0)
         , cw.grossRoyaltyInterestGasRevenue = dbo.udfBankRound(cw.estAnnualGasProduction * cs.royaltyInterestPercent * cw.netRoyaltyInterestGasPrice, 0)
         , cw.grossWorkingInterestProductRevenue = dbo.udfBankRound(cw.estAnnualProductProduction * cs.workingInterestPercent * cw.netProductPrice, 0)
         , cw.grossRoyaltyInterestProductRevenue = dbo.udfBankRound(cw.estAnnualProductProduction * cs.royaltyInterestPercent * cw.netProductPrice, 0)
      From #CalcAmortSchedule cw
      Join #CalcAppraisalSummary cs
        On cs.[queueUserid] = cw.queueUserid
       And cs.[appraisalYear] = cw.appraisalYear
       And cs.[subjectTypeCd] = cw.subjectTypeCd
       And cs.[subjectId] = cw.subjectId
       And cs.[rrcNumber] = cw.rrcNumber
     Where cw.yearNumber = @yearNumber
       And cw.appraisalYear = @appraisalYear
       And cw.queueUserid = @queueUserid

--Calculate annual revenue
    Update cw
       Set cw.annualOilRevenue = cw.grossWorkingInterestOilRevenue + cw.grossRoyaltyInterestOilRevenue
         , cw.annualGasRevenue = cw.grossWorkingInterestGasRevenue + cw.grossRoyaltyInterestGasRevenue
         , cw.annualProductRevenue = cw.grossWorkingInterestProductRevenue + cw.grossRoyaltyInterestProductRevenue
         , cw.grossWorkingInterestTotalRevenue = dbo.udfBankRound(cw.grossWorkingInterestOilRevenue + cw.grossWorkingInterestGasRevenue + cw.grossWorkingInterestProductRevenue, 0)
         , cw.grossRoyaltyInterestTotalRevenue = dbo.udfBankRound(cw.grossRoyaltyInterestOilRevenue + cw.grossRoyaltyInterestGasRevenue + cw.grossRoyaltyInterestProductRevenue, 0)
      From #CalcAmortSchedule cw
     Where cw.yearNumber = @yearNumber
       And cw.queueUserid = @queueUserid

--Calculate revenue percentages
    Update cw
       Set cw.[netWorkingInterestTotalRevenue] = cw.grossWorkingInterestTotalRevenue - Isnull(cw.operatingExpense,0.0)
         , cw.[workingInterestOilRevenuePercent] = Case When cw.grossWorkingInterestTotalRevenue > 0 
                                                        Then dbo.udfBankRound(cw.grossWorkingInterestOilRevenue / cw.grossWorkingInterestTotalRevenue, 6)
                                                        Else 0 End
         , cw.[workingInterestGasRevenuePercent] = Case When cw.grossWorkingInterestGasRevenue > 0
                                                        Then dbo.udfBankRound(cw.grossWorkingInterestGasRevenue / cw.grossWorkingInterestTotalRevenue, 6)
                                                        Else 0 End
         , cw.[workingInterestProductRevenuePercent] = Case When cw.grossWorkingInterestProductRevenue > 0
                                                        Then dbo.udfBankRound(cw.grossWorkingInterestProductRevenue / cw.grossWorkingInterestTotalRevenue, 6)
                                                        Else 0 End
      From #CalcAmortSchedule cw
     Where cw.yearNumber = @yearNumber
       And cw.appraisalYear = @appraisalYear
       And cw.queueUserid = @queueUserid

--Calculate values based on revenue percentages
    Update cw
       Set cw.workingInterestOilValue = dbo.udfBankRound(cw.netWorkingInterestTotalRevenue * cw.workingInterestOilRevenuePercent * (1.000000000 / Power((1.000000000 + cs.discountRate) , (@yearNumber - 0.5))), 0)
         , cw.workingInterestGasValue = dbo.udfBankRound(cw.netWorkingInterestTotalRevenue * cw.workingInterestGasRevenuePercent * (1.000000000 / Power((1.000000000 + cs.discountRate) , (@yearNumber - 0.5))), 0)
         , cw.workingInterestProductValue = dbo.udfBankRound(cw.netWorkingInterestTotalRevenue * cw.workingInterestProductRevenuePercent * (1.000000000 / Power((1.000000000 + cs.discountRate) , (@yearNumber - 0.5))), 0)
         , cw.royaltyInterestOilValue = dbo.udfBankRound(cw.grossRoyaltyInterestOilRevenue * (1.000000000 / Power((1.000000000 + cs.discountRate) , (@yearNumber - 0.5))), 0)
         , cw.netRoyaltyInterestGasRevenue = dbo.udfBankRound(cw.grossRoyaltyInterestGasRevenue * (1.000000000 / Power((1.000000000 + cs.discountRate) , (@yearNumber - 0.5))), 0)
         , cw.netRoyaltyInterestProductRevenue = dbo.udfBankRound(cw.grossRoyaltyInterestProductRevenue * (1.000000000 / Power((1.000000000 + cs.discountRate) , (@yearNumber - 0.5))), 0)
      From #CalcAmortSchedule cw
      Join #CalcAppraisalSummary cs
        On cs.[queueUserid] = cw.queueUserid
       And cs.[appraisalYear] = cw.appraisalYear
       And cs.[subjectTypeCd] = cw.subjectTypeCd
       And cs.[subjectId] = cw.subjectId
       And cs.[rrcNumber] = cw.rrcNumber
     Where cw.yearNumber = @yearNumber
       And cw.appraisalYear = @appraisalYear
       And cw.queueUserid = @queueUserid

--Summarize values in to PV buckets
    Update cw
       Set cw.workingInterestTotalPV = cw.workingInterestOilValue + cw.workingInterestGasValue + cw.workingInterestProductValue
         , cw.royaltyInterestTotalPV = cw.royaltyInterestOilValue + cw.netRoyaltyInterestGasRevenue + cw.netRoyaltyInterestProductRevenue
      From #CalcAmortSchedule cw
     Where cw.yearNumber = @yearNumber
       And cw.appraisalYear = @appraisalYear
       And cw.queueUserid = @queueUserid

END

GO


Grant Execute on [dbo].[spCalcAmortizationYear] to Public
Go