USE [MINERALPRO]
GO

/*
dbo.spCalcSave 44,2017
*/

IF (OBJECT_ID('spCalcSave') IS NOT NULL)
  DROP PROCEDURE dbo.spCalcSave
GO

CREATE PROCEDURE dbo.spCalcSave
 @queueUserid Bigint
,@appraisalYear Int
--1 indicates save called by non-appraisal screen
--2 indicates save called by mass-calculate
,@CalcSourceId Int = 0
AS
BEGIN
	SET NOCOUNT ON


    Declare @unitTypeCode Int =(SELECT c.code
                                FROM dbo.SystemCodeType ct
                                Join dbo.SystemCode c
                                  On c.codeType = ct.codeType
                                 And c.appraisalYear = @appraisalYear
                                 And c.[description] like 'UNIT'
                                 And c.rowDeleteFlag <> 'D'
                               Where ct.columnName like 'subjectTypeCd')
    Declare @leaseTypeCode Int =(SELECT c.code
                                FROM dbo.SystemCodeType ct
                                Join dbo.SystemCode c
                                  On c.codeType = ct.codeType
                                 And c.appraisalYear = @appraisalYear
                                 And c.[description] like 'LEASE'
                                 And c.rowDeleteFlag <> 'D'
                               Where ct.columnName like 'subjectTypeCd')
    Declare @wiTypeCode Int =(SELECT c.code
                                FROM dbo.SystemCodeType ct
                                Join dbo.SystemCode c
                                  On c.codeType = ct.codeType
                                 And c.appraisalYear = 2017
                                 And c.[description] like 'WORKING INTEREST'
                                 And c.rowDeleteFlag <> 'D'
                               Where ct.columnName like 'LEASE_INT_TYP_CD')
    Declare @UNIVERSITY_LANDS Int =(SELECT c.code
                                      FROM dbo.SystemCodeType ct
                                      Join dbo.SystemCode c
                                        On c.codeType = ct.codeType
                                       And c.appraisalYear = 2017
                                       And c.[description] like 'UNIVERSITY LANDS'
                                       And c.rowDeleteFlag <> 'D'
                                     Where ct.columnName like 'EXEMPTION_CD')


--Clear out decline schedules for the current working set
----- Note -----
----- This decline schedule is the schedule from the last calculation, which matches the currently calculated appraisal.
----- However, if the user changes the schedule after calculation but prior to pressing save, then there would be differences.
----- I guess this depends on if mineralPro calls this save process before or after updating the decline schedule itself.
    DELETE FROM ads
      From dbo.ApprDeclineSchedule ads
      Join dbo.CalcDeclineSchedule cd
        On cd.queueUserid = @queueUserid
       And cd.appraisalYear = ads.appraisalYear
       And cd.subjectTypeCd = ads.subjectTypeCd
       And cd.subjectId = ads.subjectId
       And cd.rrcNumber = ads.rrcNumber
     WHERE ads.appraisalYear = @appraisalYear

--Insert decline schedules for all units and leases in the working set      
    INSERT INTO dbo.ApprDeclineSchedule
         ( appraisalYear
         , subjectTypeCd
         , subjectId
         , rrcNumber
         , declineTypeCd
         , seqNumber
         , declineYears
         , declinePercent
         )
    Select cd.appraisalYear
         , cd.subjectTypeCd
         , cd.subjectId
         , cd.rrcNumber
         , cd.declineTypeCd
         , cd.seqNumber
         , cd.declineYears
         , cd.declinePercent
      From CalcDeclineSchedule cd
     Where cd.queueUserid = @queueUserid
       And cd.appraisalYear = @appraisalYear

--Update main Appraisal table (summary level values)
    UPDATE a
       SET a.yearLife = s.yearLife
         , a.reserveOilValue = s.accumOilProduction
         , a.accumOilProduction = s.accumOilProduction 
         , a.accumGasProduction = s.accumGasProduction
         , a.accumProductProduction = s.accumProductProduction
         , a.workingInterestTotalPV = s.workingInterestTotalPV
         , a.workingInterestTotalValue = s.workingInterestTotalPV + s.equipmentValue
         , a.royaltyInterestTotalValue = s.royaltyInterestTotalPV
         , a.totalValue = s.workingInterestTotalPV + s.royaltyInterestTotalPV + s.equipmentValue
         , a.workingInterestOilValue = s.workingInterestOilValue
         , a.workingInterestGasValue = s.workingInterestGasValue
         , a.workingInterestProductValue = s.workingInterestProductValue
         , a.royaltyInterestOilValue = s.royaltyInterestOilValue
         , a.royaltyInterestGasValue = s.royaltyInterestGasValue
         , a.royaltyInterestProductValue = s.royaltyInterestProductValue
         , a.lastAppraisalYear = s.appraisalYear
         , a.rowUpdateDt = getdate()
         , a.lastCalcDt = getdate()
         , a.lastCalcUserid = @queueUserid
         , a.lastCalcSourceId = @CalcSourceId
      From [dbo].Appraisal a
      Join [dbo].[vCalcResultAppraisal] s
        On s.queueUserid = @queueUserid
       And s.appraisalYear = a.appraisalYear
       And s.subjectTypeCd = a.subjectTypeCd
       And s.subjectId = a.subjectId
       And s.rrcNumber = a.rrcNumber
       
--Clear out ApprAmortSchedule table (detail level values)
    Delete From a
      From dbo.ApprAmortSchedule a
      Join [dbo].[vCalcResultAppraisal] s
        On s.queueUserid = @queueUserid
       And s.appraisalYear = a.appraisalYear
       And s.subjectTypeCd = a.subjectTypeCd
       And s.subjectId = a.subjectId
       And s.rrcNumber = a.rrcNumber

--Insert amortization schedules for all units and leases in the current working set
    Insert Into dbo.ApprAmortSchedule 
         ( appraisalYear 
         , subjectTypeCd
         , subjectId
         , rrcNumber
         , yearNumber
         , netOilPrice
         , netWorkingInterestGasPrice
         , netRoyaltyInterestGasPrice
         , netProductPrice
         , estAnnualOilProduction
         , grossWorkingInterestOilRevenue
         , grossRoyaltyInterestOilRevenue
         , estAnnualGasProduction
         , grossWorkingInterestGasRevenue
         , grossRoyaltyInterestGasRevenue
         , estAnnualProductProduction
         , grossWorkingInterestProductRevenue
         , grossRoyaltyInterestProductRevenue
         , grossWorkingInterestTotalRevenue
         , operatingExpense
         , netWorkingInterestTotalRevenue
         , netRoyaltyInterestOilRevenue
         , netRoyaltyInterestGasRevenue
         , netRoyaltyInterestProductRevenue
         , workingInterestTotalPV 
	     )
    Select a.appraisalYear
         , a.subjectTypeCd
         , a.subjectId
         , a.rrcNumber
         , a.yearNumber
         , a.netOilPrice
         , a.netWorkingInterestGasPrice
         , a.netRoyaltyInterestGasPrice
         , a.netProductPrice
         , a.estAnnualOilProduction
         , a.grossWorkingInterestOilRevenue
         , a.grossRoyaltyInterestOilRevenue
         , a.estAnnualGasProduction
         , a.grossWorkingInterestGasRevenue
         , a.grossRoyaltyInterestGasRevenue
         , a.estAnnualProductProduction
         , a.grossWorkingInterestProductRevenue
         , a.grossRoyaltyInterestProductRevenue
         , a.grossWorkingInterestTotalRevenue
         , a.operatingExpense
         , a.netWorkingInterestTotalRevenue
         , a.royaltyInterestOilValue
         , a.netRoyaltyInterestGasRevenue
         , a.netRoyaltyInterestProductRevenue
         , a.workingInterestTotalPV
      From dbo.CalcAmortSchedule a
     Where a.queueUserid = @queueUserid
       And a.workingInterestTotalPV > 0

--Specify @unitTypeCode to only update using unit information from the view
    update a
       Set a.workingInterestValue = s.workingInterestTotalPV + s.equipmentValue  --Working interest value includes Equipment Value
         , a.royaltyInterestValue = s.royaltyInterestTotalPV
         , a.rowUpdateDt = getdate()
         , a.lastCalcDt = getdate()
         , a.lastCalcUserid = @queueUserid
         , a.lastCalcSourceId = @CalcSourceId
      From dbo.UnitValueSummary a
      Join [dbo].[vCalcResultSummary] s
        On s.queueUserid = @queueUserid
       And s.appraisalYear = a.appraisalYear
       And s.subjectTypeCd = @unitTypeCode
       And s.subjectId = a.unitId
     Where a.rowDeleteFlag <> 'D'

--This update handles distribution of a UNIT to all leases
    Update lv
       Set lv.workingInterestValue = 
           Case When uv.weightedWorkingInterestPercent > 0 
                Then Round((crs.workingInterestTotalPV + crs.equipmentValue) / uv.weightedWorkingInterestPercent * lv.unitWorkingInterestPercent * lv.workingInterestPercent, 0)
                Else 0.0 End 
         , lv.royaltyInterestValue = 
           Case When uv.weightedRoyaltyInterestPercent > 0
                Then Round(crs.royaltyInterestTotalPV / uv.weightedRoyaltyInterestPercent * lv.unitRoyaltyInterestPercent * (lv.royaltyInterestPercent + lv.oilPaymentPercent + lv.overridingRoyaltyPercent), 0)
                Else 0.0 End 
         , lv.appraisedValue =
           Case When uv.weightedWorkingInterestPercent > 0 
                Then Round((crs.workingInterestTotalPV + crs.equipmentValue) / uv.weightedWorkingInterestPercent * lv.unitWorkingInterestPercent * lv.workingInterestPercent, 0)
                Else 0.0 End  
         + Case When uv.weightedRoyaltyInterestPercent > 0
                Then Round(crs.royaltyInterestTotalPV / uv.weightedRoyaltyInterestPercent * lv.unitRoyaltyInterestPercent * (lv.royaltyInterestPercent + lv.oilPaymentPercent + lv.overridingRoyaltyPercent), 0)
                Else 0.0 End
         , lv.rowUpdateDt = getdate()
         , lv.lastCalcDt = getdate()
         , lv.lastCalcUserid = @queueUserid
         , lv.lastCalcSourceId = @CalcSourceId
      From LeaseValueSummary lv
--Join LEASE to get 'unitId' for each lease
      Join dbo.Lease l
        On l.appraisalYear = lv.appraisalYear
       And l.leaseId = lv.leaseId
       And l.rowDeleteFlag <> 'D'
--Sum all Appraisals for a unit (rrcNumber)
--Appraisal table was updated earlier in this procedure
--This limits the update to what was in the calc working set
      Join dbo.[vCalcResultSummary] crs
        On crs.appraisalYear = l.appraisalYear
       And crs.subjectId = l.unitId
       And crs.queueUserid = @queueUserid
       And crs.subjectTypeCd = @unitTypeCode
--Join UnitValueSummary for weighted percents
      Join dbo.UnitValueSummary uv
        On uv.appraisalYear = l.appraisalYear
       And uv.unitId = l.unitId
     Where lv.rowDeleteFlag <> 'D'
       And lv.appraisalYear = @appraisalYear

--Update the individual leases
    Update lvs
       Set lvs.workingInterestValue = crs.workingInterestTotalPV + crs.equipmentValue
         , lvs.royaltyInterestValue = crs.royaltyInterestTotalPV
         , lvs.appraisedValue = crs.workingInterestTotalPV + crs.equipmentValue + crs.royaltyInterestTotalPV
         , lvs.rowUpdateDt = getdate()
         , lvs.lastCalcDt = getdate()
         , lvs.lastCalcUserid = @queueUserid
         , lvs.lastCalcSourceId = @CalcSourceId
      From LeaseValueSummary lvs
-- This limits the update to what was in the calc working set
      Join [dbo].[vCalcResultSummary] crs
        On crs.queueUserid = @queueUserid
       And crs.appraisalYear = lvs.appraisalYear
       And crs.subjectTypeCd = @leaseTypeCode
       And crs.subjectId = lvs.leaseId
     Where lvs.rowDeleteFlag <> 'D'
       And lvs.appraisalYear = @appraisalYear

-- Distribute Leases over Owners
   Update lo
      Set [ownerValue] =
          Case
          When oe.exemptionCd is not null
          Then Case
               When oe.exemptionCd = @UNIVERSITY_LANDS
               Then Case
                    When lo.[interestTypeCd] in (@wiTypeCode)
                    Then Case
                         when lvs.workingInterestPercent > 0
                          And lo.interestPercent > 0
                         Then Round(lvs.workingInterestValue / lvs.workingInterestPercent
                                                       * lo.interestPercent,0)
                         Else 0.0
                         End
                    Else Case 
                         When (lvs.oilPaymentPercent
                              +lvs.overridingRoyaltyPercent
                              +lvs.royaltyInterestPercent) > 0
                         And   lo.interestPercent > 0
                         Then  Round(lvs.[royaltyInterestValue] /(lvs.oilPaymentPercent
                                                          + lvs.overridingRoyaltyPercent
                                                          + lvs.royaltyInterestPercent)
                                                          * lo.interestPercent,0)
                         Else 0.0
                         End
                    End
               Else 0.0
               End
            Else Case
                    When lo.[interestTypeCd] in (@wiTypeCode)
                    Then Case
                         when lvs.workingInterestPercent > 0
                          And lo.interestPercent > 0
                         Then Round(lvs.workingInterestValue / lvs.workingInterestPercent
                                                       * lo.interestPercent,0)
                         Else 0.0
                         End
                    Else Case 
                         When (lvs.oilPaymentPercent
                              +lvs.overridingRoyaltyPercent
                              +lvs.royaltyInterestPercent) > 0
                         And   lo.interestPercent > 0
                         Then  Round(lvs.[royaltyInterestValue] /(lvs.oilPaymentPercent
                                                          + lvs.overridingRoyaltyPercent
                                                          + lvs.royaltyInterestPercent)
                                                          * lo.interestPercent,0)
                         Else 0.0
                         End
                    End
            End
         , lo.rowUpdateDt = getdate()
         , lo.lastCalcDt = getdate()
         , lo.lastCalcUserid = @queueUserid
         , lo.lastCalcSourceId = @CalcSourceId
     From dbo.LeaseOwner lo
     Join dbo.LeaseValueSummary lvs
       On lvs.appraisalYear = lo.appraisalYear
      And lvs.leaseId = lo.leaseId
     Left Join dbo.OwnerExemption oe
       On oe.appraisalYear = lo.appraisalYear
      And oe.ownerId = lo.ownerId
    Where lo.appraisalYear = @appraisalYear

END

Grant Execute on dbo.spCalcSave to Public
GO