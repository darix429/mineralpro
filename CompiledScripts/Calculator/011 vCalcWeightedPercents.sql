Use MINERALPRO
GO

IF (OBJECT_ID('vCalcWeightedPercents') IS NOT NULL)
  DROP View dbo.vCalcWeightedPercents
GO

CREATE VIEW [dbo].[vCalcWeightedPercents]
AS
SELECT        CASE WHEN SUM(lvs.unitWorkingInterestPercent) > 0 THEN dbo.udfBankRound(SUM(lvs.unitWorkingInterestPercent * lvs.workingInterestPercent) / SUM(lvs.unitWorkingInterestPercent), 6) 
                         ELSE 0.000000000 END AS weightedWorkingInterestPercent, CASE WHEN SUM(lvs.unitRoyaltyInterestPercent) 
                         > 0 THEN dbo.udfBankRound(SUM(lvs.unitRoyaltyInterestPercent * (lvs.royaltyInterestPercent + lvs.overridingRoyaltyPercent + lvs.oilPaymentPercent)) / SUM(lvs.unitRoyaltyInterestPercent), 6) 
                         ELSE 0.000000000 END AS weightedRoyaltyInterestPercent, uvs.appraisalYear, uvs.unitId
FROM            dbo.UnitValueSummary AS uvs INNER JOIN
                         dbo.Lease AS l ON l.appraisalYear = uvs.appraisalYear AND l.unitId = uvs.unitId AND l.rowDeleteFlag <> 'D' INNER JOIN
                         dbo.LeaseValueSummary AS lvs ON lvs.appraisalYear = uvs.appraisalYear AND lvs.leaseId = l.leaseId AND lvs.rowDeleteFlag <> 'D'
GROUP BY uvs.appraisalYear, uvs.unitId
GO

