use MINERALPRO
go

Declare @queueUserid Int = 22  --Produser
Declare @appraisalyear Int = 2017
Declare @subjectId Int = 901100
Declare @subjectTypeCd Int = 1
Declare @save Bit = 0

update [MINERALPRO].[dbo].[Appraisal]
   set dailyAverageOil = 4026.60	
     , dailyAverageGas = 8141.00
 where appraisalYear = @appraisalyear
   and subjectTypeCd = @subjectTypeCd
   and subjectId = @subjectId
   and rrcNumber = 19815

--Pull data from Appr Tables and load to staging Calc tables
Execute dbo.spCalcPreLoad @queueUserid, @appraisalYear, @subjectTypeCd, @subjectId

--Calculate up to 25 years of amortization
Execute spCalcAmortizationYears @queueUserid, @appraisalyear, 25, @save

----Save values to database
--Execute dbo.spCalcSave 44,2017
--Execute dbo.spCalcPreLoad 44, 0, 0, 0, 0, 44