Use MINERALPRO
GO

Select subjectid
     , rrcNumber
     , yearNumber
     , FORMAT(estAnnualOilProduction, 'C0', 'en-us') estAnnualOilProduction
     , netOilPrice
     , FORMAT(grossWorkingInterestOilRevenue
     + grossRoyaltyInterestOilRevenue, 'C0', 'en-us')  AnnualOilRevenue
     , FORMAT(estAnnualGasProduction, 'C0', 'en-us') estAnnualGasProduction
     , netWorkingInterestGasPrice
     , netRoyaltyInterestGasPrice
     , FORMAT(grossWorkingInterestGasRevenue
     + grossRoyaltyInterestGasRevenue, 'C0', 'en-us')  AnnualGasRevenue
     , FORMAT(estAnnualProductProduction, 'C0', 'en-us') estAnnualProductProduction
     , FORMAT(grossWorkingInterestProductRevenue
     + grossRoyaltyInterestProductRevenue, 'C0', 'en-us')  AnnualPrdRevenue
     , FORMAT(grossWorkingInterestTotalRevenue, 'C0', 'en-us') grossWorkingInterestTotalRevenue
     , FORMAT(operatingExpense, 'C0', 'en-us') operatingExpense
     , FORMAT(netWorkingInterestTotalRevenue, 'C0', 'en-us') netWorkingInterestTotalRevenue
     , FORMAT(workingInterestTotalPV, 'C0', 'en-us') workingInterestTotalPV
     , FORMAT(royaltyInterestTotalPV, 'C0', 'en-us') royaltyInterestTotalPV
  FROM [MINERALPRO].[dbo].[CalcAmortSchedule]
 Where subjectId = 901100 and appraisalYear = 2017 and rrcNumber = 19815
 and workingInterestTotalPV > 0
 and queueUserid = 22
 order by yearNumber

Select FORMAT(sum(estAnnualOilProduction), 'C0', 'en-us') estAnnualOilProduction
     , FORMAT(sum(grossWorkingInterestOilRevenue
     + grossRoyaltyInterestOilRevenue), 'C0', 'en-us')  AnnualOilRevenue
     , FORMAT(sum(estAnnualGasProduction), 'C0', 'en-us') estAnnualGasProduction
     , FORMAT(sum(grossWorkingInterestGasRevenue
     + grossRoyaltyInterestGasRevenue), 'C0', 'en-us')  AnnualGasRevenue
     , FORMAT(sum(estAnnualProductProduction), 'C0', 'en-us') estAnnualProductProduction
     , FORMAT(sum(grossWorkingInterestProductRevenue
     + grossRoyaltyInterestProductRevenue), 'C0', 'en-us')  AnnualPrdRevenue
     , FORMAT(sum(grossWorkingInterestTotalRevenue), 'C0', 'en-us') grossWorkingInterestTotalRevenue
     , FORMAT(sum(operatingExpense), 'C0', 'en-us') operatingExpense
     , FORMAT(sum(netWorkingInterestTotalRevenue), 'C0', 'en-us') netWorkingInterestTotalRevenue
     , FORMAT(sum(workingInterestTotalPV), 'C0', 'en-us') workingInterestTotalPV
     , FORMAT(sum(royaltyInterestTotalPV), 'C0', 'en-us') royaltyInterestTotalPV
  FROM [MINERALPRO].[dbo].[CalcAmortSchedule]
 Where subjectId = 901100 and appraisalYear = 2017 and rrcNumber = 19815
  and workingInterestTotalPV > 0
  and queueUserid = 22