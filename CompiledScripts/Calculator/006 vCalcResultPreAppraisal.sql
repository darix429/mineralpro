USE [MINERALPRO]
GO

IF (OBJECT_ID('vCalcResultPreAppraisal') IS NOT NULL)
  DROP View dbo.vCalcResultPreAppraisal
GO

CREATE VIEW [dbo].[vCalcResultPreAppraisal]
AS
SELECT a.queueUserid, a.appraisalYear, a.subjectTypeCd, a.subjectId, a.rrcNumber, SUM(a.estAnnualOilProduction) AS accumOilProduction, SUM(a.estAnnualGasProduction) 
                  AS accumGasProduction, SUM(a.estAnnualProductProduction) AS accumProductProduction, SUM(a.grossWorkingInterestOilRevenue) AS grossWorkingInterestOilRevenue, 
                  SUM(a.grossWorkingInterestGasRevenue) AS grossWorkingInterestGasRevenue, SUM(a.grossWorkingInterestProductRevenue) AS grossWorkingInterestProductRevenue, 
                  SUM(a.grossTotalRevenue) AS grossTotalRevenue, SUM(a.grossRoyaltyInterestOilRevenue) AS grossRoyaltyInterestOilRevenue, SUM(a.grossRoyaltyInterestGasRevenue) 
                  AS grossRoyaltyInterestGasRevenue, SUM(a.grossRoyaltyInterestProductRevenue) AS grossRoyaltyInterestProductRevenue, MAX(a.operatingExpense) 
                  AS operatingExpense, SUM(a.workingInterestOilValue) AS workingInterestOilValue, SUM(a.workingInterestGasValue) AS workingInterestGasValue, 
                  MAX(a.workingInterestProductValue) AS workingInterestProductValue, SUM(a.royaltyInterestOilValue) AS royaltyInterestOilValue, SUM(a.netRoyaltyInterestGasRevenue) 
                  AS royaltyInterestGasValue, SUM(a.netRoyaltyInterestProductRevenue) AS royaltyInterestProductValue, SUM(a.workingInterestTotalNetValue) 
                  AS workingInterestTotalNetValue, SUM(a.annualOilRevenue) AS annualOilRevenue, SUM(a.annualGasRevenue) AS annualGasRevenue, SUM(a.annualProductRevenue) 
                  AS annualProductRevenue, SUM(a.grossWorkingInterestTotalRevenue) AS grossWorkingInterestTotalRevenue, SUM(a.grossRoyaltyInterestTotalRevenue) 
                  AS grossRoyaltyInterestTotalRevenue, SUM(a.netWorkingInterestTotalRevenue) AS netWorkingInterestTotalRevenue, SUM(a.netRoyaltyInterestOilRevenue) 
                  AS netRoyaltyInterestOilRevenue, SUM(a.workingInterestTotalPV) AS workingInterestTotalPV, SUM(a.royaltyInterestTotalPV) AS royaltyInterestTotalPV, 
                  COUNT(a.yearNumber) AS yearLife, MAX(b.equipmentValue) AS equipmentValue
                  ,MAX(b.dailyAverageOil) As dailyAverageOil
                  ,MAX(b.[dailyAverageGas]) As [dailyAverageGas]
                  ,MAX(b.[dailyAverageProduct]) As [dailyAverageProduct]
FROM     dbo.CalcAmortSchedule AS a INNER JOIN
                  dbo.CalcAppraisalSummary AS b ON b.queueUserid = a.queueUserid AND b.appraisalYear = a.appraisalYear AND b.subjectTypeCd = a.subjectTypeCd AND 
                  b.subjectId = a.subjectId AND b.rrcNumber = a.rrcNumber
WHERE  (a.workingInterestTotalPV > 0)
GROUP BY a.queueUserid, a.appraisalYear, a.subjectTypeCd, a.subjectId, a.rrcNumber


GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Summarize all years' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vCalcResultPreAppraisal'
GO


