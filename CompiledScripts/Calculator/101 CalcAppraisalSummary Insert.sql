Declare @queueUserid bigint = 44   --Produser
Declare @appraisalYear smallint = 2017

--if you do not want to use one of these parameters, leave the value set to 0
Declare @subjectTypeCd smallint = 1
Declare @subjectId int = 902000
Declare @rrcNumber int = 0
--if you do not want to use one of these parameters, leave the value set to 0

Declare @sql varchar(max) = ''

Delete From CalcAppraisalSummary
 Where queueUserid = @queueUserid

set @sql = '
Insert into CalcAppraisalSummary
SELECT ' + Cast(@queueUserid as Varchar(10)) + '
      ,a.appraisalYear
      ,a.subjectTypeCd
      ,a.subjectId
      ,a.rrcNumber
      ,a.maxYearLife
      ,a.discountRate
      ,a.grossOilPrice
      ,a.grossProductPrice
      ,a.grossWorkingInterestGasPrice
      ,a.grossRoyaltyInterestGasPrice
      ,a.operatingExpense
      ,a.dailyAverageOil
      ,a.dailyAverageGas
      ,a.dailyAverageProduct
      ,Case a.subjectTypeCd when 2 then isnull(l.workingInterestPercent,0) else isnull(p.weightedWorkingInterestPercent,0) end workingInterestPercent
      ,Case a.subjectTypeCd when 2 then isnull(l.royaltyInterestPercent,0) else isnull(p.weightedRoyaltyInterestPercent,0) end royaltyInterestPercent
      ,a.productionEquipmentValue
      +a.serviceEquipmentValue
      +a.injectionEquipmentValue As EquipmentValue
  FROM dbo.Appraisal a
  Left Outer Join dbo.LeaseValueSummary l
    On l.appraisalYear = a.appraisalYear
   And l.leaseId = a.subjectId
   And l.rowDeleteFlag <> ''D''
  Left Outer Join dbo.UnitValueSummary p
    On p.appraisalYear = a.appraisalYear
   And p.UnitId = a.subjectId
   And p.rowDeleteFlag <> ''D''
 where a.appraisalYear = ' + cast(@appraisalYear as varchar(4))

Set @sql = @sql + '
   And a.rowDeleteFlag <> ''D'''
   
if @subjectTypeCd <> 0
Begin
    Set @sql = @sql + '
   and a.subjectTypeCd = ' + Cast(@subjectTypeCd as Varchar(10))
End

if @rrcNumber <> 0
begin
   Set @sql = @sql + '
   and a.rrcNumber = ' + Cast(@rrcNumber as Varchar(10))
end

if  @subjectId <> 0
begin
   Set @sql = @sql + '
   and a.subjectId = ' + Cast(@subjectId as Varchar(10))
end

Execute @sql

Delete From CalcDeclineSchedule
 Where queueUserid = @queueUserid

Set @sql = '
Insert Into CalcDeclineSchedule 
SELECT ' + Cast(@queueUserid as Varchar(10)) + '
      ,a.appraisalYear
      ,a.subjectTypeCd
      ,a.subjectId
      ,a.rrcNumber
      ,a.declineTypeCd
      ,a.seqNumber
      ,a.declineYears
      ,a.declinePercent
  FROM dbo.ApprDeclineSchedule a
 Where a.appraisalYear = @appraisalYear
   And a.rowDeleteFlag <> ''D''
   And exists (Select 1 From CalcAppraisalSummary c
                Where c.queueUserid = ' + cast(@queueUserid as varchar(10)) + '
                  And c.appraisalYear = a.appraisalYear
                  And c.subjectTypeCd = a.subjectTypeCd
                  And c.subjectId = a.subjectId
                  And c.rrcNumber = a.rrcNumber)'

Execute @sql