USE [MINERALPRO]
GO

IF OBJECT_ID( 'dbo.CalcDecline', 'u' ) IS NOT NULL DROP TABLE CalcDecline
IF OBJECT_ID( 'dbo.CalcDeclineSchedule', 'u' ) IS NOT NULL DROP TABLE CalcDeclineSchedule

CREATE TABLE [dbo].[CalcDeclineSchedule](
	[idCalcDeclineSchedule] [bigint] IDENTITY(1,1) NOT NULL,
	[queueUserid] [bigint] NOT NULL,
	[appraisalYear] [smallint] NOT NULL,
	[subjectTypeCd] [smallint] NOT NULL,
	[subjectId] [int] NOT NULL,
	[rrcNumber] [int] NOT NULL,
	[declineTypeCd] [smallint] NOT NULL,
	[seqNumber] [smallint] NOT NULL,
	[declineYears] [smallint] NOT NULL,
	[declinePercent] [decimal](4, 3) NOT NULL,
 CONSTRAINT [PK_CalcDeclineSchedule] PRIMARY KEY CLUSTERED 
(
	[idCalcDeclineSchedule] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[CalcDeclineSchedule] ADD  CONSTRAINT [DF_CalcDeclineSchedule_appraisalYear]  DEFAULT ((0)) FOR [appraisalYear]
GO

ALTER TABLE [dbo].[CalcDeclineSchedule] ADD  CONSTRAINT [DF_CalcDeclineSchedule_subjectTypeCd]  DEFAULT ((0)) FOR [subjectTypeCd]
GO

ALTER TABLE [dbo].[CalcDeclineSchedule] ADD  CONSTRAINT [DF_CalcDeclineSchedule_subjectId]  DEFAULT ((0)) FOR [subjectId]
GO

ALTER TABLE [dbo].[CalcDeclineSchedule] ADD  CONSTRAINT [DF_CalcDeclineSchedule_rrcNumber]  DEFAULT ((0)) FOR [rrcNumber]
GO

ALTER TABLE [dbo].[CalcDeclineSchedule] ADD  CONSTRAINT [DF_CalcDeclineSchedule_declineTypeCd]  DEFAULT ((0)) FOR [declineTypeCd]
GO

ALTER TABLE [dbo].[CalcDeclineSchedule] ADD  CONSTRAINT [DF_CalcDeclineSchedule_seqNumber]  DEFAULT ((0)) FOR [seqNumber]
GO

ALTER TABLE [dbo].[CalcDeclineSchedule] ADD  CONSTRAINT [DF_CalcDeclineSchedule_declineYears]  DEFAULT ((0)) FOR [declineYears]
GO

ALTER TABLE [dbo].[CalcDeclineSchedule] ADD  CONSTRAINT [DF_CalcDeclineSchedule_declinePercent]  DEFAULT ((0.0)) FOR [declinePercent]
GO

USE [MINERALPRO]
GO

CREATE NONCLUSTERED INDEX [AltKey_CalcDeclineSchedule] ON [dbo].[CalcDeclineSchedule]
(
	[queueUserid] ASC,
	[appraisalYear] ASC,
	[subjectTypeCd] ASC,
	[subjectId] ASC,
	[rrcNumber] ASC,
	[declineTypeCd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

