Use MineralPro
GO

IF (OBJECT_ID('spCalcMass') IS NOT NULL)
  DROP PROCEDURE dbo.spCalcMass
GO

CREATE PROCEDURE dbo.spCalcMass
 @queueUserid Bigint
AS
BEGIN
   SET NOCOUNT ON

   --Call pre-load specifying the mass-userid.  This will load subjects from SubjectList
   Execute dbo.spCalcPreLoad @queueUserid, 0, 0, 0, 0, @queueUserid

   Declare @var1 smallint
   DECLARE calcCursor CURSOR FOR
   Select Distinct sl.appraisalYear
     From dbo.SubjectList sl
    Where sl.queueUserid = @queueUserid
--Calculate in appraisal Year order
    Order by sl.appraisalYear

   OPEN calcCursor

   FETCH NEXT FROM calcCursor INTO @var1

   WHILE @@FETCH_STATUS = 0
   BEGIN
      execute dbo.spCalcAmortizationYears @queueUserid, @var1, 25, 1, 0, 0, 2

      FETCH NEXT FROM calcCursor INTO @var1
   END

   DEALLOCATE calcCursor
END