USE [MINERALPRO]
GO

IF (OBJECT_ID('vCalcResultSummary') IS NOT NULL)
  DROP View dbo.vCalcResultSummary
GO

CREATE VIEW [dbo].[vCalcResultSummary]
AS
SELECT queueUserid, appraisalYear, subjectTypeCd, subjectId, SUM(accumOilProduction) AS accumOilProduction, SUM(accumGasProduction) AS accumGasProduction, 
                  SUM(accumProductProduction) AS accumProductProduction, SUM(grossWorkingInterestOilRevenue) AS grossWorkingInterestOilRevenue, 
                  SUM(grossWorkingInterestGasRevenue) AS grossWorkingInterestGasRevenue, SUM(grossWorkingInterestProductRevenue) AS grossWorkingInterestProductRevenue, 
                  SUM(grossTotalRevenue) AS grossTotalRevenue, SUM(grossRoyaltyInterestOilRevenue) AS grossRoyaltyInterestOilRevenue, SUM(grossRoyaltyInterestGasRevenue) 
                  AS grossRoyaltyInterestGasRevenue, SUM(grossRoyaltyInterestProductRevenue) AS grossRoyaltyInterestProductRevenue, SUM(operatingExpense) AS operatingExpense, 
                  SUM(workingInterestOilValue) AS workingInterestOilValue, SUM(workingInterestGasValue) AS workingInterestGasValue, SUM(workingInterestProductValue) 
                  AS workingInterestProductValue, SUM(royaltyInterestOilValue) AS royaltyInterestOilValue, SUM(royaltyInterestGasValue) AS royaltyInterestGasValue, 
                  SUM(royaltyInterestProductValue) AS royaltyInterestProductValue, SUM(workingInterestTotalNetValue) AS workingInterestTotalNetValue, SUM(annualOilRevenue) 
                  AS annualOilRevenue, SUM(annualGasRevenue) AS annualGasRevenue, SUM(annualProductRevenue) AS annualProductRevenue, SUM(grossWorkingInterestTotalRevenue) 
                  AS grossWorkingInterestTotalRevenue, SUM(grossRoyaltyInterestTotalRevenue) AS grossRoyaltyInterestTotalRevenue, SUM(netWorkingInterestTotalRevenue) 
                  AS netWorkingInterestTotalRevenue, SUM(netRoyaltyInterestOilRevenue) AS netRoyaltyInterestOilRevenue, SUM(workingInterestTotalPV) AS workingInterestTotalPV, 
                  SUM(royaltyInterestTotalPV) AS royaltyInterestTotalPV, SUM(equipmentValue) AS equipmentValue
                  ,sum([dailyAverageOil]) AS [dailyAverageOil]
                  ,sum([dailyAverageGas]) AS [dailyAverageGas]
                  ,sum([dailyAverageProduct]) AS [dailyAverageProduct]
FROM     dbo.vCalcResultAppraisal
GROUP BY queueUserid, appraisalYear, subjectTypeCd, subjectId

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Summarize all appraisals' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vCalcResultSummary'
GO
