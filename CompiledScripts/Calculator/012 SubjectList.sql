USE [MINERALPRO]
GO


IF OBJECT_ID( 'dbo.SubjectList', 'u' ) IS NOT NULL DROP TABLE [SubjectList]


CREATE TABLE [dbo].[SubjectList](
	[idSubjectList] [bigint] IDENTITY(1,1) NOT NULL,
	[queueUserid] [bigint] NOT NULL,
	[appraisalYear] [smallint] NOT NULL,
	[subjectTypeCd] [smallint] NOT NULL,
	[subjectId] [int] NOT NULL,
	[rowUpdateUserid] [int] NOT NULL,
	[rowUpdateDt] [datetime] NOT NULL,
	[rowDeleteFlag] [char](1) NOT NULL,
 CONSTRAINT [PK_SubjectList] PRIMARY KEY CLUSTERED 
(
	[idSubjectList] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SubjectList] ADD  CONSTRAINT [DF_SubjectList_queueUserid]  DEFAULT ((0)) FOR [queueUserid]
GO

ALTER TABLE [dbo].[SubjectList] ADD  CONSTRAINT [DF_SubjectList_appraisalYear]  DEFAULT ((0)) FOR [appraisalYear]
GO

ALTER TABLE [dbo].[SubjectList] ADD  CONSTRAINT [DF_SubjectList_subjectTypeCd]  DEFAULT ((0)) FOR [subjectTypeCd]
GO

ALTER TABLE [dbo].[SubjectList] ADD  CONSTRAINT [DF_SubjectList_subjectId]  DEFAULT ((0)) FOR [subjectId]
GO

ALTER TABLE [dbo].[SubjectList] ADD  CONSTRAINT [DF_SubjectList_rowUpdateUserid]  DEFAULT ((0)) FOR [rowUpdateUserid]
GO

ALTER TABLE [dbo].[SubjectList] ADD  CONSTRAINT [DF_SubjectList_rowUpdateDt]  DEFAULT (getdate()) FOR [rowUpdateDt]
GO

ALTER TABLE [dbo].[SubjectList] ADD  CONSTRAINT [DF_SubjectList_rowDeleteFlag]  DEFAULT ('') FOR [rowDeleteFlag]
GO


