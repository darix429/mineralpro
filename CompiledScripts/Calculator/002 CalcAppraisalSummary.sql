USE [MINERALPRO]
GO

IF OBJECT_ID( 'dbo.CalcStaging', 'u' ) IS NOT NULL DROP TABLE CalcStaging
IF OBJECT_ID( 'dbo.CalcAppraisalSummary', 'u' ) IS NOT NULL DROP TABLE CalcAppraisalSummary
GO

CREATE TABLE [dbo].[CalcAppraisalSummary](
	[idCalcAppraisalSummary] [bigint] IDENTITY(1,1) NOT NULL,
	[queueUserid] [bigint] NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_queueUserid]  DEFAULT (0),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_appraisalYear]  DEFAULT ((0)),
	[subjectTypeCd] [smallint] NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_subjectTypeCd]  DEFAULT ((0)),
	[subjectId] [int] NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_subjectId]  DEFAULT ((0)),
	[rrcNumber] [int] NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_rrcNumber]  DEFAULT ((0)),
	[amortTerm] [int] NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_amortTerm]  DEFAULT ((0)),
	[discountRate] [decimal](4, 3) NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_discountRate_1]  DEFAULT ((0.0)),
	[grossOilPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_grossOilPrice]  DEFAULT ((0.0)),
	[grossProductPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_grossProductPrice]  DEFAULT ((0.0)),
	[grossWorkingInterestGasPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_grossWorkingInterestGasPrice]  DEFAULT ((0.0)),
	[grossRoyaltyInterestGasPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_grossRoyaltyInterestGasPrice]  DEFAULT ((0.0)),
	[operatingExpense] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_operatingExpense_1]  DEFAULT ((0)),
	[dailyAverageOil] [decimal](7, 2) NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_dailyAverageOil_1]  DEFAULT ((0.0)),
	[dailyAverageGas] [decimal](7, 2) NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_dailyAverageGas_1]  DEFAULT ((0.0)),
	[dailyAverageProduct] [decimal](7, 2) NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_dailyAverageProduct_1]  DEFAULT ((0.0)),
	[workingInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_workingInterestPercent]  DEFAULT ((0)),
	[royaltyInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_royaltyInterestPercent]  DEFAULT ((0)),
	[equipmentValue] [decimal](18, 0) NOT NULL CONSTRAINT [DF_CalcAppraisalSummary_equipmentValue]  DEFAULT ((0)),
 CONSTRAINT [PK_CalcAppraisalSummary] PRIMARY KEY CLUSTERED 
(
	[idCalcAppraisalSummary] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO