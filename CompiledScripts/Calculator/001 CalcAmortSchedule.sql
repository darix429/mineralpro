USE [MINERALPRO]
GO

IF OBJECT_ID( 'dbo.CalcWork', 'u' ) IS NOT NULL DROP TABLE CalcWork
IF OBJECT_ID( 'dbo.CalcAmortSchedule', 'u' ) IS NOT NULL DROP TABLE CalcAmortSchedule
GO

CREATE TABLE [dbo].[CalcAmortSchedule](
	[idCalcAmortSchedule] [bigint] IDENTITY(1,1) NOT NULL,
	[queueUserid] [bigint] NOT NULL CONSTRAINT [DF_CalcAmortSchedule_queueUserid]  DEFAULT (0),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_CalcAmortSchedule_appraisalYear]  DEFAULT ((0)),
	[subjectTypeCd] [smallint] NOT NULL CONSTRAINT [DF_CalcAmortSchedule_subjectTypeCd]  DEFAULT ((0)),
	[subjectId] [int] NOT NULL CONSTRAINT [DF_CalcAmortSchedule_subjectId]  DEFAULT ((0)),
	[rrcNumber] [int] NOT NULL CONSTRAINT [DF_CalcAmortSchedule_rrcNumber]  DEFAULT ((0)),
	[yearNumber] [smallint] NOT NULL CONSTRAINT [DF_CalcAmortSchedule_yearNumber]  DEFAULT ((0)),
	[estAnnualOilProduction] [bigint] NOT NULL CONSTRAINT [DF_CalcAmortSchedule_estAnnualOilProduction]  DEFAULT ((0)),
	[estAnnualGasProduction] [bigint] NOT NULL CONSTRAINT [DF_CalcAmortSchedule_estAnnualGasProduction]  DEFAULT ((0)),
	[estAnnualProductProduction] [bigint] NOT NULL CONSTRAINT [DF_CalcAmortSchedule_estAnnualProductProduction]  DEFAULT ((0)),
	[netOilPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_netOilPrice]  DEFAULT ((0.0)),
	[netProductPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_netProductPrice]  DEFAULT ((0.0)),
	[netWorkingInterestGasPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_netWorkingInterestGasPrice]  DEFAULT ((0.0)),
	[netRoyaltyInterestGasPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_netRoyaltyInterestGasPrice]  DEFAULT ((0.0)),
	[grossWorkingInterestOilRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_grossWorkingInterestOilRevenue]  DEFAULT ((0)),
	[grossWorkingInterestGasRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_grossWorkingInterestGasRevenue]  DEFAULT ((0)),
	[grossWorkingInterestProductRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_grossWorkingInterestProductRevenue]  DEFAULT ((0)),
	[grossTotalRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_grossWorkingInterestTotalRevenue]  DEFAULT ((0)),
	[grossRoyaltyInterestOilRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_grossRoyaltyInterestOilRevenue]  DEFAULT ((0)),
	[grossRoyaltyInterestGasRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_grossRoyaltyInterestGasRevenue]  DEFAULT ((0)),
	[grossRoyaltyInterestProductRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_grossRoyaltyInterestProductRevenue]  DEFAULT ((0)),
	[workingInterestOilRevenuePercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_workingInterestOilRevenuePercent]  DEFAULT ((0)),
	[workingInterestGasRevenuePercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_workingInterestGasRevenuePercent]  DEFAULT ((0)),
	[workingInterestProductRevenuePercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_workingInterestProductRevenuePercent]  DEFAULT ((0)),
	[operatingExpense] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_operatingExpense]  DEFAULT ((0)),
	[workingInterestOilValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_workingInterestOilValue]  DEFAULT ((0)),
	[workingInterestGasValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_workingInterestGasValue]  DEFAULT ((0)),
	[workingInterestProductValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_workingInterestProductValue]  DEFAULT ((0)),
	[royaltyInterestOilValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_royaltyInterestOilValue]  DEFAULT ((0)),
	[netRoyaltyInterestGasRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_netRoyaltyInterestGasRevenue]  DEFAULT ((0)),
	[netRoyaltyInterestProductRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_netRoyaltyInterestProductRevenue]  DEFAULT ((0)),
	[workingInterestTotalNetValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_workingInterestTotalNetValue]  DEFAULT ((0)),
	[annualOilRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_annualOilRevenue]  DEFAULT ((0)),
	[annualGasRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_annualGasRevenue]  DEFAULT ((0)),
	[annualProductRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_annualProductRevenue]  DEFAULT ((0)),
	[grossWorkingInterestTotalRevenue] [decimal](15, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_grossWorkingInterestTotalRevenue_1]  DEFAULT ((0)),
	[grossRoyaltyInterestTotalRevenue] [decimal](15, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_grossRoyaltyInterestTotalRevenue]  DEFAULT ((0)),
	[netWorkingInterestTotalRevenue] [decimal](18, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_netWorkingInterestTotalRevenue]  DEFAULT ((0)),
	[netRoyaltyInterestOilRevenue] [decimal](18, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_netRoyaltyInterestOilRevenue]  DEFAULT ((0)),
	[workingInterestTotalPV] [decimal](18, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_workingInterestTotalPV]  DEFAULT ((0)),
	[royaltyInterestTotalPV] [decimal](18, 0) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_royaltyInterestTotalPV]  DEFAULT ((0)),
	[dailyAverageOil] [decimal](13, 6) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_dailyAverageOil]  DEFAULT ((0.0)),
	[dailyAverageGas] [decimal](13, 6) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_dailyAverageGas]  DEFAULT ((0.0)),
	[dailyAverageProduct] [decimal](13, 6) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_dailyAverageProduct]  DEFAULT ((0.0)),
	[declinePercentOil] [decimal](4, 3) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_declinePercent]  DEFAULT ((0.0)),
	[declinePercentGas] [decimal](4, 3) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_declinePercentOil1]  DEFAULT ((0.0)),
	[declinePercentProduct] [decimal](4, 3) NOT NULL CONSTRAINT [DF_CalcAmortSchedule_declinePercentOil1_1]  DEFAULT ((0.0)),
 CONSTRAINT [PK_CalcAmortSchedule] PRIMARY KEY CLUSTERED 
(
	[idCalcAmortSchedule] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO