USE [MINERALPRO]
GO

IF (OBJECT_ID('vCalcResultAppraisal') IS NOT NULL)
  DROP View dbo.vCalcResultAppraisal
GO

CREATE VIEW [dbo].[vCalcResultAppraisal]
AS
SELECT crpa.queueUserid, crpa.appraisalYear, crpa.subjectTypeCd, crpa.subjectId, crpa.rrcNumber, crpa.accumOilProduction, crpa.accumGasProduction, 
                  crpa.accumProductProduction, crpa.grossWorkingInterestOilRevenue, crpa.grossWorkingInterestGasRevenue, crpa.grossWorkingInterestProductRevenue, 
                  crpa.grossTotalRevenue, crpa.grossRoyaltyInterestOilRevenue, crpa.grossRoyaltyInterestGasRevenue, crpa.grossRoyaltyInterestProductRevenue, crpa.operatingExpense, 
                  crpa.workingInterestOilValue, crpa.workingInterestGasValue, crpa.workingInterestProductValue, crpa.royaltyInterestOilValue, crpa.royaltyInterestGasValue, 
                  crpa.royaltyInterestProductValue, crpa.workingInterestTotalNetValue, crpa.annualOilRevenue, crpa.annualGasRevenue, crpa.annualProductRevenue, 
                  crpa.grossWorkingInterestTotalRevenue, crpa.grossRoyaltyInterestTotalRevenue, crpa.netWorkingInterestTotalRevenue, crpa.netRoyaltyInterestOilRevenue, 
                  crpa.workingInterestTotalPV, crpa.royaltyInterestTotalPV, crpa.yearLife, dbo.udfBankRound(crpa.equipmentValue * Isnull(sch.factor,1), 0) AS equipmentValue
                  ,crpa.[dailyAverageOil]
                  ,crpa.[dailyAverageGas]
                  ,crpa.[dailyAverageProduct]
FROM     dbo.vCalcResultPreAppraisal AS crpa LEFT JOIN
                  dbo.CalcEquipmentPVSchedule AS sch ON sch.appraisalYear = crpa.appraisalYear AND sch.yearNumber = crpa.yearLife

GO

EXEC sys.sp_addextendedproperty @name=N'MS_Description', @value=N'Apply Equip PV Factor' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vCalcResultAppraisal'
GO

