USE [MINERALPRO]
GO

drop FUNCTION dbo.udfBankRound
go
CREATE FUNCTION dbo.udfBankRound
(
 @value Decimal(24,9) 
,@decimals int = 0
)
RETURNS Decimal(24,9)
AS
BEGIN
   DECLARE @result Decimal(24,9)
   Declare @start  Decimal(24,9)

   Select @start = @value * POWER(10, @decimals)
    
   Select @result = cast(@start + 1 as int) - @start

   if @result = .5
   begin
      Select @result = round( @start - (cast( cast(@start + 1 as int) % 2 as decimal(24,9))/10) , 0 )
   end
   Else
   begin
      Select @result = round( @start , 0 )
   end

    
   Select @result = @result / POWER(10, @decimals)

	RETURN @result

END
GO

