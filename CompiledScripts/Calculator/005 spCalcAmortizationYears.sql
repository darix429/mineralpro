Use MineralPro
GO

IF (OBJECT_ID('spCalcAmortizationYears') IS NOT NULL)
  DROP PROCEDURE dbo.spCalcAmortizationYears
GO

CREATE PROCEDURE dbo.spCalcAmortizationYears
 @queueUserid Bigint
,@appraisalYear Int
,@amortYears Int
,@save Bit = 0
--Not used from appraisal screen
,@subjectTypeCd smallint = 0
,@subjectId int = 0
--Do not use this next field
--0 indicates save called by appraisal screen
--1 indicates save called by non-appraisal screen
--2 indicates save called by mass-calculate
,@CalcSourceId Int = 0
AS
BEGIN
   SET NOCOUNT ON

   If @subjectTypeCd <> 0
   Begin
      Execute dbo.spCalcPreLoad @queueUserid, @appraisalYear, @subjectTypeCd, @subjectId, 0
   End

   
   Declare @unitId Int = 0
   


   Declare @unitTypeCode Int =(SELECT c.code
                                 FROM dbo.SystemCodeType ct
                                 Join dbo.SystemCode c
                                   On c.codeType = ct.codeType
                                  And c.appraisalYear = @appraisalYear
                                  And c.[description] like 'UNIT'
                                  And c.rowDeleteFlag <> 'D'
                                Where ct.columnName like 'subjectTypeCd')

   if @subjectTypeCd <> @unitTypeCode
   begin
      Select @unitId = l.unitId
        From dbo.Lease l
       Where l.leaseId = @subjectId

      If @unitId <> 0
      begin
         Set @subjectId = @unitId
         Set @subjectTypeCd = @unitTypeCode
      end
   end

--If lngUnitID > 0 Then
--   recalc and save the economic unit wgt avg WI and wgt avg RI before continuing
--Calculate the weighted interest percents before amortization years

   UPDATE uvs
      Set weightedWorkingInterestPercent = cwp.weightedWorkingInterestPercent
        , weightedRoyaltyInterestPercent = cwp.weightedRoyaltyInterestPercent
     From [dbo].[UnitValueSummary] uvs
     Join [dbo].vCalcWeightedPercents cwp
       On cwp.appraisalYear = uvs.appraisalYear
      And cwp.unitId = uvs.unitId
    Where uvs.appraisalYear = @appraisalYear
      And uvs.unitId in 
      (
          Select cas.subjectId 
            From CalcAppraisalSummary cas 
           Where cas.subjectTypeCd = 1
             And cas.queueUserid = @queueUserid 
      )

   CREATE TABLE #CalcAmortSchedule (
	   [idCalcAmortSchedule] [bigint],
	   [queueUserid] [bigint] DEFAULT (0),
	   [appraisalYear] [smallint] DEFAULT ((0)),
	   [subjectTypeCd] [smallint] DEFAULT ((0)),
	   [subjectId] [int] DEFAULT ((0)),
	   [rrcNumber] [int] DEFAULT ((0)),
	   [yearNumber] [smallint] DEFAULT ((0)),
	   [estAnnualOilProduction] [bigint] DEFAULT ((0)),
	   [estAnnualGasProduction] [bigint] DEFAULT ((0)),
	   [estAnnualProductProduction] [bigint] DEFAULT ((0)),
	   [netOilPrice] [decimal](5, 2) DEFAULT ((0.0)),
	   [netProductPrice] [decimal](5, 2) DEFAULT ((0.0)),
	   [netWorkingInterestGasPrice] [decimal](5, 2) DEFAULT ((0.0)),
	   [netRoyaltyInterestGasPrice] [decimal](5, 2) DEFAULT ((0.0)),
	   [grossWorkingInterestOilRevenue] [decimal](13, 0) DEFAULT ((0)),
	   [grossWorkingInterestGasRevenue] [decimal](13, 0) DEFAULT ((0)),
	   [grossWorkingInterestProductRevenue] [decimal](13, 0) DEFAULT ((0)),
	   [grossTotalRevenue] [decimal](13, 0) DEFAULT ((0)),
	   [grossRoyaltyInterestOilRevenue] [decimal](13, 0) DEFAULT ((0)),
	   [grossRoyaltyInterestGasRevenue] [decimal](13, 0) DEFAULT ((0)),
	   [grossRoyaltyInterestProductRevenue] [decimal](13, 0) DEFAULT ((0)),
	   [workingInterestOilRevenuePercent] [decimal](9, 8) DEFAULT ((0)),
	   [workingInterestGasRevenuePercent] [decimal](9, 8) DEFAULT ((0)),
	   [workingInterestProductRevenuePercent] [decimal](9, 8) DEFAULT ((0)),
	   [operatingExpense] [decimal](13, 0) DEFAULT ((0)),
	   [workingInterestOilValue] [decimal](13, 0) DEFAULT ((0)),
	   [workingInterestGasValue] [decimal](13, 0) DEFAULT ((0)),
	   [workingInterestProductValue] [decimal](13, 0) DEFAULT ((0)),
	   [royaltyInterestOilValue] [decimal](13, 0) DEFAULT ((0)),
	   [netRoyaltyInterestGasRevenue] [decimal](13, 0) DEFAULT ((0)),
	   [netRoyaltyInterestProductRevenue] [decimal](13, 0) DEFAULT ((0)),
	   [workingInterestTotalNetValue] [decimal](13, 0) DEFAULT ((0)),
	   [annualOilRevenue] [decimal](13, 0) DEFAULT ((0)),
	   [annualGasRevenue] [decimal](13, 0) DEFAULT ((0)),
	   [annualProductRevenue] [decimal](13, 0) DEFAULT ((0)),
	   [grossWorkingInterestTotalRevenue] [decimal](15, 0) DEFAULT ((0)),
	   [grossRoyaltyInterestTotalRevenue] [decimal](15, 0) DEFAULT ((0)),
	   [netWorkingInterestTotalRevenue] [decimal](18, 0) DEFAULT ((0)),
	   [netRoyaltyInterestOilRevenue] [decimal](18, 0) DEFAULT ((0)),
	   [workingInterestTotalPV] [decimal](18, 0) DEFAULT ((0)),
	   [royaltyInterestTotalPV] [decimal](18, 0) DEFAULT ((0)),
	   [dailyAverageOil] [decimal](13, 6) DEFAULT ((0.0)),
	   [dailyAverageGas] [decimal](13, 6) DEFAULT ((0.0)),
	   [dailyAverageProduct] [decimal](13, 6) DEFAULT ((0.0)),
	   [declinePercentOil] [decimal](4, 3) DEFAULT ((0.0)),
	   [declinePercentGas] [decimal](4, 3) DEFAULT ((0.0)),
	   [declinePercentProduct] [decimal](4, 3) DEFAULT ((0.0))
   ) 

   CREATE TABLE #CalcAppraisalSummary (
	   [idCalcAppraisalSummary] [bigint],
	   [queueUserid] [bigint] DEFAULT (0),
	   [appraisalYear] [smallint] DEFAULT ((0)),
	   [subjectTypeCd] [smallint] DEFAULT ((0)),
	   [subjectId] [int] DEFAULT ((0)),
	   [rrcNumber] [int] DEFAULT ((0)),
	   [amortTerm] [int] DEFAULT ((0)),
	   [discountRate] [decimal](4, 3) DEFAULT ((0.0)),
	   [grossOilPrice] [decimal](5, 2) DEFAULT ((0.0)),
	   [grossProductPrice] [decimal](5, 2) DEFAULT ((0.0)),
	   [grossWorkingInterestGasPrice] [decimal](5, 2) DEFAULT ((0.0)),
	   [grossRoyaltyInterestGasPrice] [decimal](5, 2) DEFAULT ((0.0)),
	   [operatingExpense] [decimal](13, 0) DEFAULT ((0)),
	   [dailyAverageOil] [decimal](7, 2) DEFAULT ((0.0)),
	   [dailyAverageGas] [decimal](7, 2) DEFAULT ((0.0)),
	   [dailyAverageProduct] [decimal](7, 2) DEFAULT ((0.0)),
	   [workingInterestPercent] [decimal](9, 8) DEFAULT ((0)),
	   [royaltyInterestPercent] [decimal](9, 8) DEFAULT ((0)),
	   [equipmentValue] [decimal](18, 0) DEFAULT ((0)) 
   ) 


   CREATE TABLE #CalcDeclineSchedule (
	   [idCalcDeclineSchedule] [bigint] DEFAULT ((0.0)),
	   [queueUserid] [bigint] DEFAULT ((0.0)),
	   [appraisalYear] [smallint] DEFAULT ((0.0)),
	   [subjectTypeCd] [smallint] DEFAULT ((0.0)),
	   [subjectId] [int] DEFAULT ((0.0)),
	   [rrcNumber] [int] DEFAULT ((0.0)),
	   [declineTypeCd] [smallint] DEFAULT ((0.0)),
	   [seqNumber] [smallint] DEFAULT ((0.0)),
	   [declineYears] [smallint] DEFAULT ((0.0)),
	   [declinePercent] [decimal](4, 3) DEFAULT ((0.0))
   ) 

   Insert Into #CalcAmortSchedule 
   Select *
     From dbo.CalcAmortSchedule a
    Where a.queueUserid = @queueUserid
   
   Insert Into #CalcAppraisalSummary 
   Select *
     From dbo.CalcAppraisalSummary a
    Where a.queueUserid = @queueUserid

   Insert Into #CalcDeclineSchedule 
   Select *
     From dbo.CalcDeclineSchedule a
    Where a.queueUserid = @queueUserid

   Declare @amortYear     Int = 1

   --Calculate up to 25 years
   while @amortYear <= @amortYears
   Begin
      Execute dbo.spCalcAmortizationYear @queueUserid, @amortYear, @appraisalYear

      Set @amortYear = @amortYear + 1
   End


   Delete From dbo.CalcAmortSchedule
    Where queueUserid = @queueUserid
    
   Insert Into dbo.CalcAmortSchedule (
	    [queueUserid] 
	   ,[appraisalYear] 
	   ,[subjectTypeCd] 
	   ,[subjectId] 
	   ,[rrcNumber] 
	   ,[yearNumber] 
	   ,[estAnnualOilProduction] 
	   ,[estAnnualGasProduction] 
	   ,[estAnnualProductProduction] 
	   ,[netOilPrice] 
	   ,[netProductPrice] 
	   ,[netWorkingInterestGasPrice] 
	   ,[netRoyaltyInterestGasPrice] 
	   ,[grossWorkingInterestOilRevenue] 
	   ,[grossWorkingInterestGasRevenue] 
	   ,[grossWorkingInterestProductRevenue] 
	   ,[grossTotalRevenue] 
	   ,[grossRoyaltyInterestOilRevenue] 
	   ,[grossRoyaltyInterestGasRevenue] 
	   ,[grossRoyaltyInterestProductRevenue] 
	   ,[workingInterestOilRevenuePercent] 
	   ,[workingInterestGasRevenuePercent] 
	   ,[workingInterestProductRevenuePercent] 
	   ,[operatingExpense] 
	   ,[workingInterestOilValue] 
	   ,[workingInterestGasValue] 
	   ,[workingInterestProductValue] 
	   ,[royaltyInterestOilValue] 
	   ,[netRoyaltyInterestGasRevenue] 
	   ,[netRoyaltyInterestProductRevenue] 
	   ,[workingInterestTotalNetValue] 
	   ,[annualOilRevenue] 
	   ,[annualGasRevenue] 
	   ,[annualProductRevenue] 
	   ,[grossWorkingInterestTotalRevenue]
	   ,[grossRoyaltyInterestTotalRevenue]
	   ,[netWorkingInterestTotalRevenue] 
	   ,[netRoyaltyInterestOilRevenue] 
	   ,[workingInterestTotalPV] 
	   ,[royaltyInterestTotalPV] 
	   ,[dailyAverageOil] 
	   ,[dailyAverageGas] 
	   ,[dailyAverageProduct]
	   ,[declinePercentOil] 
	   ,[declinePercentGas] 
	   ,[declinePercentProduct] 
      )
   Select 
	    [queueUserid] 
	   ,[appraisalYear] 
	   ,[subjectTypeCd] 
	   ,[subjectId] 
	   ,[rrcNumber] 
	   ,[yearNumber] 
	   ,[estAnnualOilProduction] 
	   ,[estAnnualGasProduction] 
	   ,[estAnnualProductProduction] 
	   ,[netOilPrice] 
	   ,[netProductPrice] 
	   ,[netWorkingInterestGasPrice] 
	   ,[netRoyaltyInterestGasPrice] 
	   ,[grossWorkingInterestOilRevenue] 
	   ,[grossWorkingInterestGasRevenue] 
	   ,[grossWorkingInterestProductRevenue] 
	   ,[grossTotalRevenue] 
	   ,[grossRoyaltyInterestOilRevenue] 
	   ,[grossRoyaltyInterestGasRevenue] 
	   ,[grossRoyaltyInterestProductRevenue] 
	   ,[workingInterestOilRevenuePercent] 
	   ,[workingInterestGasRevenuePercent] 
	   ,[workingInterestProductRevenuePercent] 
	   ,[operatingExpense] 
	   ,[workingInterestOilValue] 
	   ,[workingInterestGasValue] 
	   ,[workingInterestProductValue] 
	   ,[royaltyInterestOilValue] 
	   ,[netRoyaltyInterestGasRevenue] 
	   ,[netRoyaltyInterestProductRevenue] 
	   ,[workingInterestTotalNetValue] 
	   ,[annualOilRevenue] 
	   ,[annualGasRevenue] 
	   ,[annualProductRevenue] 
	   ,[grossWorkingInterestTotalRevenue]
	   ,[grossRoyaltyInterestTotalRevenue]
	   ,[netWorkingInterestTotalRevenue] 
	   ,[netRoyaltyInterestOilRevenue] 
	   ,[workingInterestTotalPV] 
	   ,[royaltyInterestTotalPV] 
	   ,[dailyAverageOil] 
	   ,[dailyAverageGas] 
	   ,[dailyAverageProduct]
	   ,[declinePercentOil] 
	   ,[declinePercentGas] 
	   ,[declinePercentProduct] 
     From #CalcAmortSchedule a
    Where a.queueUserid = @queueUserid

    --Select *
    -- From #CalcAmortSchedule a
    --Where a.queueUserid = @queueUserid

    --Select *
    -- From #CalcAmortSchedule a
    --Where a.queueUserid = @queueUserid
    

   Delete From dbo.CalcAppraisalSummary
    Where queueUserid = @queueUserid
       
   Insert Into dbo.CalcAppraisalSummary (
	    [queueUserid]
	   ,[appraisalYear]
	   ,[subjectTypeCd]
	   ,[subjectId] 
	   ,[rrcNumber] 
	   ,[amortTerm] 
	   ,[discountRate]
	   ,[grossOilPrice]
	   ,[grossProductPrice]
	   ,[grossWorkingInterestGasPrice] 
	   ,[grossRoyaltyInterestGasPrice] 
	   ,[operatingExpense] 
	   ,[dailyAverageOil] 
	   ,[dailyAverageGas] 
	   ,[dailyAverageProduct] 
	   ,[workingInterestPercent] 
	   ,[royaltyInterestPercent] 
	   ,[equipmentValue] 
      )
   Select 
	    [queueUserid]
	   ,[appraisalYear]
	   ,[subjectTypeCd]
	   ,[subjectId] 
	   ,[rrcNumber] 
	   ,[amortTerm] 
	   ,[discountRate]
	   ,[grossOilPrice]
	   ,[grossProductPrice]
	   ,[grossWorkingInterestGasPrice] 
	   ,[grossRoyaltyInterestGasPrice] 
	   ,[operatingExpense] 
	   ,[dailyAverageOil] 
	   ,[dailyAverageGas] 
	   ,[dailyAverageProduct] 
	   ,[workingInterestPercent] 
	   ,[royaltyInterestPercent] 
	   ,[equipmentValue] 
     From #CalcAppraisalSummary a
    Where a.queueUserid = @queueUserid
    

   Delete From dbo.CalcDeclineSchedule
    Where queueUserid = @queueUserid
    
   Insert Into dbo.CalcDeclineSchedule (
	    [queueUserid]
	   ,[appraisalYear] 
	   ,[subjectTypeCd] 
	   ,[subjectId] 
	   ,[rrcNumber] 
	   ,[declineTypeCd] 
	   ,[seqNumber] 
	   ,[declineYears] 
	   ,[declinePercent] 
   )
   Select 
	    [queueUserid]
	   ,[appraisalYear] 
	   ,[subjectTypeCd] 
	   ,[subjectId] 
	   ,[rrcNumber] 
	   ,[declineTypeCd] 
	   ,[seqNumber] 
	   ,[declineYears] 
	   ,[declinePercent] 
     From #CalcDeclineSchedule a
    Where a.queueUserid = @queueUserid

   
   drop table #CalcAmortSchedule
   drop table #CalcAppraisalSummary
   drop table #CalcDeclineSchedule

   if @save = 1
--0 indicates save called by appraisal screen
--1 indicates save called by non-appraisal screen
--2 indicates save called by mass-calculate
      Execute dbo.spCalcSave @queueUserid, @appraisalYear, 1 

END
GO

Grant Execute on dbo.spCalcAmortizationYears to public
GO
