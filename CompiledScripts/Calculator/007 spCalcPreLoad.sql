Use MINERALPRO
GO

IF (OBJECT_ID('spCalcPreLoad') IS NOT NULL)
   DROP PROCEDURE dbo.spCalcPreLoad
GO

CREATE PROCEDURE dbo.spCalcPreLoad
 @queueUserid bigint
,@appraisalYear smallint
--if you do not want to use one of these parameters, leave the value set to 0
--BEGIN
,@subjectTypeCd smallint = 0
,@subjectId int = 0
,@rrcNumber int = 0
--END
--if this is for Mass calculation, specify user id here
--BEGIN
,@massUserid bigint = 0
--END
AS
BEGIN
	SET NOCOUNT ON

   Declare @unitId Int = 0

   if @subjectTypeCd = 2
   begin
      Select @unitId = l.unitId
        From dbo.Lease l
       Where l.leaseId = @subjectId

      If @unitId <> 0
      begin
         Set @subjectId = @unitId
         Set @subjectTypeCd = 1
      end
   end

   Declare @sql varchar(8000) = ''

   Delete From CalcAppraisalSummary
    Where queueUserid = @queueUserid

   Select @sql = '
   Insert into CalcAppraisalSummary
   SELECT ' + Cast(@queueUserid as Varchar(10)) + '
         ,a.appraisalYear
         ,a.subjectTypeCd
         ,a.subjectId
         ,a.rrcNumber
         ,a.maxYearLife
         ,a.discountRate
         ,a.grossOilPrice
         ,a.grossProductPrice
         ,a.grossWorkingInterestGasPrice
         ,a.grossRoyaltyInterestGasPrice
         ,a.operatingExpense
         ,a.dailyAverageOil
         ,a.dailyAverageGas
         ,a.dailyAverageProduct
         ,Case a.subjectTypeCd when 2 then isnull(l.workingInterestPercent,0) else isnull(p.weightedWorkingInterestPercent,0) end workingInterestPercent
         ,Case a.subjectTypeCd when 2 then isnull(l.royaltyInterestPercent,0) + isnull(l.overridingRoyaltyPercent,0) + isnull(l.oilPaymentPercent,0) else isnull(p.weightedRoyaltyInterestPercent,0) end royaltyInterestPercent
         ,a.productionEquipmentValue
         +a.serviceEquipmentValue
         +a.injectionEquipmentValue As EquipmentValue
     FROM dbo.Appraisal a
     Left Outer Join dbo.LeaseValueSummary l
       On l.appraisalYear = a.appraisalYear
      And l.leaseId = a.subjectId
      And l.rowDeleteFlag <> ''D''
     Left Outer Join dbo.UnitValueSummary p
       On p.appraisalYear = a.appraisalYear
      And p.UnitId = a.subjectId
      And p.rowDeleteFlag <> ''D''
    where a.rowDeleteFlag <> ''D'''
   
   if @appraisalYear <> 0
   Begin
      Select @sql = @sql + '
      And a.appraisalYear = ' + cast(@appraisalYear as varchar(4))
   End

   if @subjectTypeCd <> 0
   Begin
      Select @sql = @sql + '
      And a.subjectTypeCd = ' + Cast(@subjectTypeCd as Varchar(10))
   End

   if  @subjectId <> 0
   begin
      Select @sql = @sql + '
      And a.subjectId = ' + Cast(@subjectId as Varchar(10))
   end

   if @rrcNumber <> 0
   begin
      Select @sql = @sql + '
      and a.rrcNumber = ' + Cast(@rrcNumber as Varchar(10))
   end

   if @massUserid <> 0
   Begin
      Select @sql = @sql + '
      and exists (Select 1
                    From dbo.SubjectList sl
                   Where sl.queueUserid = ' + Cast(@queueUserid as Varchar(10)) + '
                     And sl.appraisalYear = a.appraisalYear
                     And sl.subjectTypeCd = a.subjectTypeCd
                     And sl.subjectId = a.subjectId)'
   End

   --print(@sql)
   execute(@sql)

   Delete From CalcDeclineSchedule
    Where queueUserid = @queueUserid

   Insert Into CalcDeclineSchedule 
   SELECT @queueUserid
         ,a.appraisalYear
         ,a.subjectTypeCd
         ,a.subjectId
         ,a.rrcNumber
         ,a.declineTypeCd
         ,a.seqNumber
         ,a.declineYears
         ,a.declinePercent
     FROM dbo.ApprDeclineSchedule a
    Where a.rowDeleteFlag <> 'D'
      And exists (Select 1 From CalcAppraisalSummary c
                   Where c.queueUserid = @queueUserid
                     And c.appraisalYear = a.appraisalYear
                     And c.subjectTypeCd = a.subjectTypeCd
                     And c.subjectId = a.subjectId
                     And c.rrcNumber = a.rrcNumber)
                  

      Delete From CalcAmortSchedule
      Where queueUserid = @queueUserid

      Insert Into dbo.CalcAmortSchedule
         ( queueUserid
         , appraisalYear
         , subjectTypeCd
         , subjectId
         , rrcNumber
         , yearNumber
         , netOilPrice
         , netWorkingInterestGasPrice
         , netRoyaltyInterestGasPrice
         , netProductPrice
         , estAnnualOilProduction
         , grossWorkingInterestOilRevenue
         , grossRoyaltyInterestOilRevenue
         , estAnnualGasProduction
         , grossWorkingInterestGasRevenue
         , grossRoyaltyInterestGasRevenue
         , estAnnualProductProduction
         , grossWorkingInterestProductRevenue
         , grossRoyaltyInterestProductRevenue
         , grossWorkingInterestTotalRevenue
         , operatingExpense
         , netWorkingInterestTotalRevenue
         , royaltyInterestOilValue
         , netRoyaltyInterestGasRevenue
         , netRoyaltyInterestProductRevenue
         , workingInterestTotalPV
         , workingInterestOilValue
         , workingInterestGasValue
         , workingInterestProductValue
	      )
      SELECT @queueUserid
         , aas.appraisalYear 
         , aas.subjectTypeCd
         , aas.subjectId
         , aas.rrcNumber
         , aas.yearNumber
         , aas.netOilPrice
         , aas.netWorkingInterestGasPrice
         , aas.netRoyaltyInterestGasPrice
         , aas.netProductPrice
         , aas.estAnnualOilProduction
         , aas.grossWorkingInterestOilRevenue
         , aas.grossRoyaltyInterestOilRevenue
         , aas.estAnnualGasProduction
         , aas.grossWorkingInterestGasRevenue
         , aas.grossRoyaltyInterestGasRevenue
         , aas.estAnnualProductProduction
         , aas.grossWorkingInterestProductRevenue
         , aas.grossRoyaltyInterestProductRevenue
         , aas.grossWorkingInterestTotalRevenue
         , aas.operatingExpense
         , aas.netWorkingInterestTotalRevenue
         , aas.netRoyaltyInterestOilRevenue
         , aas.netRoyaltyInterestGasRevenue
         , aas.netRoyaltyInterestProductRevenue
         , aas.workingInterestTotalPV

         , dbo.udfBankRound(aas.netWorkingInterestTotalRevenue * 
           Case When aas.grossWorkingInterestTotalRevenue > 0 Then dbo.udfBankRound(aas.grossWorkingInterestOilRevenue / aas.grossWorkingInterestTotalRevenue, 6) Else 0 End
         * (1.000000000 / Power((1.000000000 + a.discountRate) , (aas.yearNumber - 0.5))), 0)

         , dbo.udfBankRound(aas.netWorkingInterestTotalRevenue *          
           Case When aas.grossWorkingInterestGasRevenue > 0 Then dbo.udfBankRound(aas.grossWorkingInterestGasRevenue / aas.grossWorkingInterestTotalRevenue, 6) Else 0 End
         * (1.000000000 / Power((1.000000000 + a.discountRate) , (aas.yearNumber - 0.5))), 0)

         , dbo.udfBankRound(aas.netWorkingInterestTotalRevenue 
         * Case When aas.grossWorkingInterestProductRevenue > 0 Then dbo.udfBankRound(aas.grossWorkingInterestProductRevenue / aas.grossWorkingInterestTotalRevenue, 6) Else 0 End
         * (1.000000000 / Power((1.000000000 + a.discountRate) , (aas.yearNumber - 0.5))), 0)
         --, a.workingInterestOilValue
         --, a.workingInterestGasValue
         --, a.workingInterestProductValue
      From dbo.ApprAmortSchedule aas
      Join dbo.Appraisal a
         On a.appraisalYear = aas.appraisalYear
         And a.subjectTypeCd = aas.subjectTypeCd
         And a.subjectId = aas.subjectId
         And a.rrcNumber = aas.rrcNumber
       Where aas.rowDeleteFlag <> 'D'
         And exists (Select 1 From CalcAppraisalSummary c
                      Where c.queueUserid = @queueUserid
                        And c.appraisalYear = aas.appraisalYear
                        And c.subjectTypeCd = aas.subjectTypeCd
                        And c.subjectId = aas.subjectId
                        And c.rrcNumber = aas.rrcNumber)
END
GO
