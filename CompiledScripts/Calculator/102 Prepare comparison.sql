Use MINERALPRO
GO

SELECT *
  Into [MINERALPRO].[dbo].[Appraisal2]
  FROM [MINERALPRO].[dbo].[Appraisal]
 Where subjectId = 901100 and appraisalYear = 2017 and rrcNumber = 19815

Select *
  Into [MINERALPRO].[dbo].[ApprAmortSchedule2]
  From [MINERALPRO].[dbo].[ApprAmortSchedule]
 Where subjectId = 901100 and appraisalYear = 2017 and rrcNumber = 19815

Select *
  Into [MINERALPRO].[dbo].[ApprDeclineSchedule2]
  From [MINERALPRO].[dbo].[ApprDeclineSchedule]
 Where subjectId = 901100 and appraisalYear = 2017 and rrcNumber = 19815

Select *
  Into [MINERALPRO].[dbo].[CalcAppraisalSummary2]
  From [MINERALPRO].[dbo].[CalcAppraisalSummary]
 Where subjectId = 901100 and appraisalYear = 2017 and rrcNumber = 19815

Select *
  Into CalcDeclineSchedule2
  From CalcDeclineSchedule
 Where subjectId = 901100 and appraisalYear = 2017 and rrcNumber = 19815
 
Select *
  Into LeaseValueSummary2
  From LeaseValueSummary
 Where leaseid in (select leaseid from lease where appraisalYear = 2017 and unitid = 901100)
   And appraisalYear = 2017

Select *
  Into UnitValueSummary2
  From UnitValueSummary
 Where unitId = 901100 and appraisalYear = 2017 