USE [MINERALPRO]
GO

DROP FUNCTION [dbo].[udfRollSpecialProcessing]
GO

CREATE FUNCTION dbo.udfRollSpecialProcessing
(
 @name varchar(50)
,@futureYear smallint
,@cols varchar(max)
)
RETURNS varchar(max)
AS
BEGIN
	DECLARE @var1 varchar(max)

   --Rules to perform for all tables
   SELECT @var1 = replace(@cols,'a.appraisalYear',Cast(@futureYear as char(4)))

   --Example for special business rules.
   Select @var1 = 
          Case @name
          --Shows how to replace varchar column name with literal value
               When 'abc' Then Replace(@var1,'a.ResetEachYear','''DefaultValue''')

          --Else do nothing
               Else @var1
          End

	RETURN @var1
END
GO

