USE [MINERALPRO]
GO


IF (OBJECT_ID('spRoll') IS NOT NULL)
  DROP PROCEDURE [dbo].[spRoll]
GO

CREATE PROCEDURE [dbo].[spRoll]
 @queueUserid Bigint
,@appraisalYear smallint
,@restart bit = 0
AS
BEGIN
	SET NOCOUNT ON

   Print ' Roll Start ' + cast(getdate() as varchar(50))

   Declare @futureYear smallint = @appraisalYear + 1
   Declare @tableName varchar(50)
   Declare @columns varchar(max)
   Declare @sql  varchar(max)

   If @restart = 1
   begin   
       Update st       
          Set rollDt = '1900-01-01'
            , rowBeforeCount = 0
            , rowAfterCount = 0
         From dbo.SystemTable st
        Where st.appraisalYear = @appraisalYear
   end

   DECLARE rollCursor CURSOR FOR
   Select st.name
     From SystemTable st
    Where st.appraisalYear = @appraisalYear
      And st.rollInd = 'Y'
      And st.rollDt = '1900-01-01'
    Order by st.rollSeqNumber


   OPEN rollCursor

   FETCH NEXT FROM rollCursor INTO @tableName

   WHILE @@FETCH_STATUS = 0
   BEGIN
      set @columns = ''
       
      SELECT @columns =
          STUFF((    
          SELECT ',a.' + sc.name
            From sys.tables st
            Join sys.columns sc
              On sc.object_id = st.object_id
--Bypass all identity fields
             And sc.is_identity = 0
--Bypass all computed fields (ZIP_SORT)
             And sc.is_computed = 0
           Where st.name like @tableName
           Order by sc.column_id
             FOR XML PATH('') 
               ), 1, 1,'')

       Print @tableName + ' Start ' + cast(getdate() as varchar(50))

       Set @sql = 'Delete From [' + @tableName + '] Where appraisalYear >= ' + Cast(@futureYear as char(4))
       Execute(@sql)

       Set @sql =        ' Insert into [' + @tableName + ']'
       Set @sql = @sql + ' (' + replace(@columns,'a.','') + ')'
       Set @sql = @sql + ' Select ' + dbo.udfRollSpecialProcessing(@tableName, @futureYear, @columns)
       Set @sql = @sql + '   From dbo.[' + @tableName + '] a'
       Set @sql = @sql + '  Where a.appraisalYear = ' + Cast(@appraisalYear as char(4))
       Set @sql = @sql + '    And a.rowDeleteFlag <> ''D'''
       Execute(@sql)

       
       Set @sql = '
       Update st       
          Set [rowBeforeCount] = (Select count(*) From [' + @tableName + '] Where appraisalYear = ' + Cast(@appraisalYear as char(4)) + ')
            , [rowAfterCount] = (Select count(*) From [' + @tableName + '] Where appraisalYear = ' + Cast(@futureYear as char(4)) + ')
         From SystemTable st
        Where appraisalYear = ' + Cast(@appraisalYear as char(4)) + '
          And name like ''' + @tablename + ''''
       Execute(@sql)

       Update st       
          Set rollDt = getdate()
         From dbo.SystemTable st
        Where st.name = @tableName
          And st.appraisalYear = @appraisalYear

      Print @tableName + ' Start ' + cast(getdate() as varchar(50))

      FETCH NEXT FROM rollCursor INTO @tableName
   END

   DEALLOCATE rollCursor

   Delete From SubjectList Where queueUserid = @queueUserid

   --Calculate all appraisals
   Print ' Calculate Start ' + cast(getdate() as varchar(50))
   Insert into SubjectList (queueUserid,appraisalYear,subjectTypeCd,subjectId,rowUpdateUserid)
   Select Distinct
          @queueUserid
        , a.appraisalYear
        , a.subjectTypeCd
        , a.subjectId
        , @queueUserid
     From Appraisal a
    Where a.appraisalYear = @futureYear

    Execute dbo.spCalcMass @queueUserid

   Print ' Calculate End ' + cast(getdate() as varchar(50))
   Print ' Roll End ' + cast(getdate() as varchar(50))

   
   --There should not be any other rows on the appraisalYear table with Y as COLL or CURR
   UPDATE a
      SET a.collectionInd = 'N'
        , a.currentInd = 'N'
     From dbo.AppraisalYear a
                  

   INSERT INTO dbo.AppraisalYear (appraisalYear, currentInd, collectionInd, effectiveDt, expirationDt)
   VALUES ( @futureYear
          , 'Y'
          , 'Y'
          , '07/25/' + Cast(@appraisalYear As Char(4))
          , '07/25/' + Cast(@futureYear As Char(4))
          )
      
End

Grant Execute on [dbo].[spRoll] to Public
GO
