USE [MINERALPRO]
GO


IF OBJECT_ID( 'dbo.SystemTable', 'u' ) IS NOT NULL DROP TABLE SystemTable
GO

CREATE TABLE [dbo].[SystemTable](
	[idSystemTable] [bigint] IDENTITY(1,1) NOT NULL,
	[name] [sysname] NOT NULL,
	[rollInd] [char](10) NOT NULL,
	[rollDt] [datetime] NOT NULL,
	[jeTriggerInd] [char](1) NOT NULL,
	[rowUpdateUserid] [int] NOT NULL,
	[rowUpdateDt] [datetime] NOT NULL,
	[rowDeleteFlag] [char](1) NOT NULL,
 CONSTRAINT [PK_SystemTable] PRIMARY KEY CLUSTERED 
(
	[idSystemTable] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[SystemTable] ADD  CONSTRAINT [DF_SystemTable_rollInd]  DEFAULT ('N') FOR [rollInd]
GO

ALTER TABLE [dbo].[SystemTable] ADD  CONSTRAINT [DF_SystemTable_rollDt]  DEFAULT ('1900-01-01') FOR [rollDt]
GO

ALTER TABLE [dbo].[SystemTable] ADD  CONSTRAINT [DF_SystemTable_jeTriggerInd]  DEFAULT ('N') FOR [jeTriggerInd]
GO

ALTER TABLE [dbo].[SystemTable] ADD  CONSTRAINT [DF_SystemTable_rowUpdateUserid]  DEFAULT ((0)) FOR [rowUpdateUserid]
GO

ALTER TABLE [dbo].[SystemTable] ADD  CONSTRAINT [DF_SystemTable_rowUpdateDt]  DEFAULT (getdate()) FOR [rowUpdateDt]
GO

ALTER TABLE [dbo].[SystemTable] ADD  CONSTRAINT [DF_SystemTable_rowDeleteFlag]  DEFAULT ('') FOR [rowDeleteFlag]
GO


