USE [MINERALPRO]

DECLARE @maxAppraisalYear SMALLINT

SELECT @maxAppraisalYear = MAX(appraisalYear) - 1
  FROM [MINERALPRO].[dbo].[AppraisalYear]

DELETE 
  FROM [MINERALPRO].[dbo].[AppraisalYear]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[SystemSetting]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[SystemCode]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[SystemCodeGroup]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[NoteType]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[QuickNote]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[TaxUnit]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[Exemption]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[TaxUnitExemption]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[Agency]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[Operator]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[Field]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[Report]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[ReportScript]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[ArbMember]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[Unit]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[UnitValueSummary]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[Lease]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[LeaseValueSummary]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[LeaseOwner]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[LeaseTaxUnit]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[OwnerExemption]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[Appraisal]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[ApprAmortSchedule]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[ApprDeclineSchedule]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[CalcTaxAdj]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[CalcEscalatedPriceAdj]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[CalcEscalatedOpExpAdj]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[CalcEquipmentSchedule]
 WHERE appraisalYear < @maxAppraisalYear

DELETE 
  FROM [MINERALPRO].[dbo].[CalcEquipmentPVSchedule]
 WHERE appraisalYear < @maxAppraisalYear
