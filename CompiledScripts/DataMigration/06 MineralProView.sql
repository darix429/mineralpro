USE [master]
GO

CREATE DATABASE [MineralProView]
GO

USE [MineralProView]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE VIEW [dbo].[ACCOUNT_APPRL_YEAR]
AS
SELECT        l.appraisalYear AS APPRAISAL_YR, CAST(l.leaseId AS VARCHAR(25)) AS ACCOUNT_NUM, l.leaseName AS ACCOUNT_NAME, l.cadCategoryCd AS P_SPTD_CD, l.unitId AS ECONOMIC_UNIT_ID, 
                         l.lastUpdateUserid AS LST_ACCT_UPDT_ID, lvs.lastAppraisalYear AS LAST_APPRAISAL_YR, lvs.appraisedValue AS TOT_VAL, lvs.appraisalDt AS APPRAISAL_DT, l.rowUpdateUserid AS LST_UPDT_EMPL_ID, 
                         l.rowUpdateDt AS LST_UPDT_TS, l.rowDeleteFlag AS POST_FLG
FROM            MINERALPRO.dbo.Lease AS l INNER JOIN
                         MINERALPRO.dbo.LeaseValueSummary AS lvs ON lvs.appraisalYear = l.appraisalYear AND lvs.leaseId = l.leaseId
GO


CREATE VIEW [dbo].[ACCOUNT_JURISDICTIONS]
AS
SELECT        appraisalYear AS APPRAISAL_YR, CAST(leaseId AS VARCHAR(25)) AS ACCOUNT_NUM, taxUnitTypeCd AS JURIS_TYP_CD, taxUnitCd AS JURIS_CD, taxUnitPercent AS JURIS_PCT, rowUpdateUserid AS LST_UPDT_EMPL_ID, 
                         rowUpdateDt AS LST_UPDT_TS, rowDeleteFlag AS POST_FLG
FROM            MINERALPRO.dbo.LeaseTaxUnit
GO


CREATE VIEW [dbo].[CODE_TABLE_MASTER]
AS
SELECT        appraisalYear AS APPRAISAL_YR, codeType AS CODE_TYP, code AS CODE, shortDescription AS LEGACY_VALUE, description AS CODE_DESCRIPTION, activeInd AS ACTIVE_IND, staticInd AS STATIC_CODE_IND, 
                         rowUpdateUserid AS LST_UPDT_EMPL_ID, rowUpdateDt AS LST_UPDT_TS, rowDeleteFlag AS POST_FLG
FROM            MINERALPRO.dbo.SystemCode
GO


CREATE VIEW [dbo].[ECON_UNIT_MRL]
AS
SELECT        u.appraisalYear AS APPRAISAL_YR, u.unitId AS ECONOMIC_UNIT_ID, u.operatorId AS OPERATOR_NUM, uvs.workingInterestPercent AS WI_FACTOR, uvs.royaltyInterestPercent AS RI_FACTOR, 
                         uvs.weightedWorkingInterestPercent AS WGT_WI_PCT, uvs.weightedRoyaltyInterestPercent AS ASWGT_RI_PCT, uvs.workingInterestValue AS TOT_WI_VAL, uvs.royaltyInterestValue AS TOT_RI_VAL, 
                         u.rowUpdateUserid AS LST_UPDT_EMPL_ID, u.rowUpdateDt AS LST_UPDT_TS, u.rowDeleteFlag AS POST_FLG
FROM            MINERALPRO.dbo.Unit AS u INNER JOIN
                         MINERALPRO.dbo.UnitValueSummary AS uvs ON uvs.appraisalYear = u.appraisalYear AND uvs.unitId = u.unitId
GO


CREATE VIEW [dbo].[ECONOMIC_UNIT]
AS
SELECT        u.appraisalYear AS APPRAISAL_YR, u.unitId AS ECONOMIC_UNIT_ID, u.unitName AS ECON_UNIT_DESC, u.operatorId AS OPERATOR_NUM, uvs.workingInterestPercent AS WI_FACTOR, uvs.royaltyInterestPercent AS RI_FACTOR,
                          uvs.weightedWorkingInterestPercent AS WGT_WI_PCT, uvs.weightedRoyaltyInterestPercent AS WGT_RI_PCT, uvs.workingInterestValue AS TOT_WI_VAL, uvs.royaltyInterestValue AS TOT_RI_VAL, 
                         u.rowUpdateUserid AS LST_UPDT_EMPL_ID, u.rowUpdateDt AS LST_UPDT_TS, u.rowDeleteFlag AS POST_FLG
FROM            MINERALPRO.dbo.Unit AS u INNER JOIN
                         MINERALPRO.dbo.UnitValueSummary AS uvs ON uvs.appraisalYear = u.appraisalYear AND uvs.unitId = u.unitId
GO


CREATE VIEW [dbo].[MRL_APPRL_YEAR]
AS
SELECT        appraisalYear AS APPRAISAL_YR, CASE SubjectTypeCd WHEN 1 THEN subjectId ELSE 0 END AS ECONOMIC_UNIT_ID, CASE SubjectTypeCd WHEN 2 THEN CAST(subjectId AS VARCHAR(25)) ELSE '' END AS ACCOUNT_NUM, 
                         rrcNumber AS RRC_NUM, fieldId AS FIELD_NUM, wellDepth AS WELL_DEPTH, gravityCd AS GRAVITY_CD, wellDescription AS WELL_NUM, wellTypeCd AS WELL_TYP_CD, comment AS PRODUCTION_ZONE, 
                         productionEquipmentCount AS PROD_EQUIP_COUNT, productionEquipmentSchedule AS PROD_EQUIP_SCHED, productionEquipmentValue AS PROD_EQUIP_VAL, serviceEquipmentCount AS SERV_EQUIP_COUNT, 
                         serviceEquipmentSchedule AS SERV_EQUIP_SCHED, serviceEquipmentValue AS SERV_EQUIP_VAL, injectionEquipmentCount AS INJ_EQUIP_COUNT, injectionEquipmentSchedule AS INJ_EQUIP_SCHED, 
                         injectionEquipmentValue AS INJ_EQUIP_VAL, maxYearLife AS MAX_YEAR_LIFE, discountRate AS DISCOUNT_RATE, grossOilPrice AS GROSS_OIL_PRICE, grossProductPrice AS GROSS_PROD_PRICE, 
                         grossWorkingInterestGasPrice AS GROSS_GAS_WI_PRICE, grossRoyaltyInterestGasPrice AS GROSS_GAS_RI_PRICE, operatingExpense AS OPERATING_EXP, dailyAverageOil AS DAILY_AVG_OIL, 
                         dailyAverageGas AS DAILY_AVG_GAS, dailyAverageProduct AS DAILY_AVG_PRODUCT, yearLife AS YEAR_LIFE, reserveOilValue AS OIL_RESERVES, accumOilProduction AS ACCUM_OIL_PRODUCTION, 
                         accumGasProduction AS ACCUM_GAS_PRODUCTION, accumProductProduction AS ACCUM_PRODUCT_PRODUCTION, totalValue AS TOT_VAL, workingInterestOilValue AS OIL_WI_TOT_VAL, 
                         workingInterestGasValue AS GAS_WI_TOT_VAL, workingInterestProductValue AS PRODUCT_WI_TOT_VAL, workingInterestTotalValue AS WI_TOT_VAL, workingInterestTotalPV AS PV_WI_TOT_VAL, 
                         royaltyInterestOilValue AS OIL_RI_TOT_VAL, royaltyInterestGasValue AS GAS_RI_TOT_VAL, royaltyInterestProductValue AS PRODUCT_RI_TOT_VAL, royaltyInterestTotalValue AS RI_TOT_VAL, 
                         lastAppraisalYear AS LAST_APPRAISAL_YR, rowUpdateUserid AS LST_UPDT_EMPL_ID, rowUpdateDt AS LST_UPDT_TS, rowDeleteFlag AS POST_FLG
FROM            MINERALPRO.dbo.Appraisal
GO


CREATE VIEW [dbo].[MRL_OBJECT]
AS
SELECT        l.appraisalYear AS APPRAISAL_YR, CAST(l.leaseId AS varchar(25)) AS ACCOUNT_NUM, l.divisionOrderDescription AS PIPELINE, l.description AS LEASE_DESC, l.comment AS LEASE_NOTE, 
                         l.changeReasonCd AS LEASE_CHG_CD, l.operatorId AS OPERATOR_NUM, l.acres AS LEASE_ACRES, l.leaseYear AS LEASE_YR, lvs.workingInterestPercent AS LEASE_WI_PCT, lvs.royaltyInterestPercent AS LEASE_RI_PCT, 
                         lvs.overridingRoyaltyPercent AS LEASE_OR_PCT, lvs.oilPaymentPercent AS LEASE_OP_PCT, lvs.workingInterestValue AS LEASE_WI_VALUE, lvs.royaltyInterestValue AS LEASE_RI_VALUE, lvs.tractNum AS TRACT_NUM, 
                         lvs.unitWorkingInterestPercent AS UNIT_WI_PCT, lvs.unitRoyaltyInterestPercent AS UNIT_RI_PCT, l.rowUpdateUserid AS LST_UPDT_EMPL_ID, l.rowUpdateDt AS LST_UPDT_TS, l.rowDeleteFlag AS POST_FLG
FROM            MINERALPRO.dbo.Lease AS l INNER JOIN
                         MINERALPRO.dbo.LeaseValueSummary AS lvs ON lvs.appraisalYear = l.appraisalYear AND lvs.leaseId = l.leaseId
GO


CREATE VIEW [dbo].[MRL_OPERATORS]
AS
SELECT        appraisalYear AS APPRAISAL_YR, operatorId AS OPERATOR_NUM, operatorName AS OPERATOR_NAME, ownerId AS OWNER_ID, rowUpdateUserid AS LST_UPDT_EMPL_ID, rowUpdateDt AS LST_UPDT_TS, 
                         rowDeleteFlag AS POST_FLG
FROM            MINERALPRO.dbo.Operator
GO


CREATE VIEW [dbo].[MRL_OWNER]
AS
SELECT        appraisalYear AS APPRAISAL_YR, CAST(leaseId AS varchar(25)) AS ACCOUNT_NUM, ownerId AS OWNER_ID, interestTypeCd AS LEASE_INT_TYP_CD, interestPercent AS LEASE_INT_PCT, ownerValue AS OWNER_VAL, 
                         lockCd AS FREEZE_CD, rowUpdateUserid AS LST_UPDT_EMP_ID, rowUpdateDt AS LST_UPDT_TS, rowDeleteFlag AS POST_FLG
FROM            MINERALPRO.dbo.LeaseOwner
GO


CREATE VIEW [dbo].[MRL_OWNER_EXEMPTIONS]
AS
SELECT        appraisalYear AS APPRAISAL_YR, ownerId AS OWNER_ID, exemptionTypeCd AS EXEMPTION_TYP_CD, exemptionCd AS EXEMPTION_CD, YEAR(effectiveDt) AS EFFECTIVE_YR, rowUpdateUserid AS LST_UPDT_EMPL_ID, 
                         rowUpdateDt AS LST_UPDT_TS, rowDeleteFlag AS POST_FLG
FROM            MINERALPRO.dbo.OwnerExemption
GO


CREATE VIEW [dbo].[MRL_RRC_FIELDS]
AS
SELECT        appraisalYear AS APPRAISAL_YR, fieldId AS RRC_FIELD_NUM, fieldName AS FIELD_NAME, wellDepth AS FIELD_DEPTH, gravityCd AS GRAVITY_CD, rowUpdateUserid AS LST_UPT_EMPL_ID, rowUpdateDt AS LST_UPT_TS, 
                         rowDeleteFlag AS POST_FLG
FROM            MINERALPRO.dbo.Field
GO


CREATE VIEW [dbo].[OWNER]
AS
Select *
  From (
SELECT      ownerId as OWNER_ID
           ,name1 as NAME
           ,NAME2 as NAME2
           ,phoneNum as PHONE_NUM
           ,birthDt as BIRTH_DT
           ,Cast(CASE WHEN confidentialInd = 'N' THEN 0 ELSE 1 END as bit) as PRIVACY_IND
           ,ownerTypeCdx as OWNER_TYP_CDX
           ,ownerChangeCd as OWNER_CHG_CD
           ,agencyCdx as TAX_REP_ID
           ,rowUpdateUserid as LST_UPDT_EMPL_ID
           ,rowUpdateDt as LST_UPDT_TS
           ,rowDeleteFlag as POST_FLG
           ,RANK() OVER (PARTITION BY ownerId ORDER BY appraisalYear desc) AS Owner_Rank
  FROM [MINERALPRO].[dbo].[Owner]) a
  Where a.Owner_Rank = 1
GO


CREATE VIEW [dbo].[OWNER_ADDRESS]
AS
SELECT        appraisalYear AS APPRAISAL_YR, ownerId AS OWNER_ID, addrLine1 AS ADDRESS_LINE1, addrLine2 AS ADDRESS_LINE2, addrLine3 AS ADDRESS_LINE3, city AS CITY, stateCd AS STATE_CD, zipcode AS ZIPCODE, 
                         countryCd AS COUNTRY_CD, rowUpdateUserid AS LST_UPDT_EMPL_ID, rowUpdateDt AS LST_UPDT_TS, rowDeleteFlag AS POST_FLG
FROM            MINERALPRO.dbo.Owner
GO


CREATE VIEW [dbo].[TAX_JURIS]
AS
SELECT        appraisalYear AS APPRAISAL_YR, taxUnitTypeCd AS JURIS_TYP_CD, taxUnitCd AS TAX_JURIS_CD, taxUnitAbbr AS SHORT_NAME, taxUnitName AS LONG_NAME, taxUnitCdx AS MNRL_JURIS_CDX, 
                         estimatedTaxRate AS TAX_RATE, CAST(CASE WHEN taxingInd = 'Y' THEN 1 ELSE 0 END AS BIT) AS LIST_ON_NOTICE_IND, rowUpdateUserid AS LST_UPDT_EMPL_ID, rowUpdateDt AS LST_UPDT_TS, 
                         rowDeleteFlag AS POST_FLG
FROM            MINERALPRO.dbo.TaxUnit
GO


USE [master]
GO
ALTER DATABASE [MineralProView] SET  READ_WRITE 
GO
