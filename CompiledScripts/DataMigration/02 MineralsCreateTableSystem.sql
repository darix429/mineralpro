USE [MINERALPRO]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF OBJECT_ID('[dbo].[AppUserSubSystemFavorite]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[AppUserSubSystemFavorite]; 
GO

IF OBJECT_ID('[dbo].[AppUserFavorite]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[AppUserFavorite]; 
GO

IF OBJECT_ID('[dbo].[UserFunction]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[UserFunction]; 
GO

IF OBJECT_ID('[dbo].[FunctionSecurity]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[FunctionSecurity]; 
GO

IF OBJECT_ID('[dbo].[SubSystemFunction]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[SubSystemFunction]; 
GO

IF OBJECT_ID('[dbo].[AppSubSystem]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[AppSubSystem]; 
GO

IF OBJECT_ID('[dbo].[SubSystem]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[SubSystem]; 
GO

IF OBJECT_ID('[dbo].[App]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[App]; 
GO

IF OBJECT_ID('[dbo].[UserOrgSecurity]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[UserOrgSecurity]; 
GO

IF OBJECT_ID('[dbo].[SecurityGroup]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[SecurityGroup]; 
GO

IF OBJECT_ID('[dbo].[UserOrganization]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[UserOrganization]; 
GO

IF OBJECT_ID('[dbo].[AppUser]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[AppUser]; 
GO

IF OBJECT_ID('[dbo].[OrganizationContact]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[OrganizationContact]; 
GO

IF OBJECT_ID('[dbo].[Organization]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[Organization]; 
GO

CREATE TABLE [dbo].[Organization](
	[idOrganization] [bigint] IDENTITY(1,1) NOT NULL,
	[organizationNumber] [int] NOT NULL CONSTRAINT [DF_Organization_organizationNumber] DEFAULT (0),
	[organizationName] [varchar](50) NOT NULL CONSTRAINT [DF_Organization_organizationName] DEFAULT (''),
	[statusCd] [smallint] NOT NULL CONSTRAINT [DF_Organization_statusCd] DEFAULT (0),
	[statusDt] [datetime] NOT NULL CONSTRAINT [DF_Organization_statusDt] DEFAULT ('1900-01-01'),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_Organization_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_Organization_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_Organization_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_Organization] PRIMARY KEY CLUSTERED 
(
	[idOrganization] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_Organization] ON [dbo].[Organization]
(
	[organizationNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_Organization_organizationName] ON [dbo].[Organization]
(
	[organizationName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE TABLE [dbo].[OrganizationContact](
	[idOrganizationContact] [bigint] IDENTITY(1,1) NOT NULL,
	[idOrganization] [bigint] NOT NULL CONSTRAINT [DF_OrganizationContact_idOrganization] DEFAULT (0),
	[contactNumber] [int] NOT NULL CONSTRAINT [DF_OrganizationContact_contactNumber] DEFAULT (0),
	[firstName] [varchar](32) NOT NULL CONSTRAINT [DF_OrganizationContact_firstName] DEFAULT (''),
	[lastName] [varchar](32) NOT NULL CONSTRAINT [DF_OrganizationContact_lastName] DEFAULT (''),
	[middleInitial] [char](1) NOT NULL CONSTRAINT [DF_OrganizationContact_middleInitial] DEFAULT (''),
	[nameAbbreviation] [varchar](8) NOT NULL CONSTRAINT [DF_OrganizationContact_nameAbbreviation] DEFAULT (''),
	[title] [varchar](64) NOT NULL CONSTRAINT [DF_OrganizationContact_title] DEFAULT (''),
	[statusCd] [smallint] NOT NULL CONSTRAINT [DF_OrganizationContact_statusCd] DEFAULT (0),
	[statusDt] [datetime] NOT NULL CONSTRAINT [DF_OrganizationContact_statusDt] DEFAULT ('1900-01-01'),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_OrganizationContact_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_OrganizationContact_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_OrganizationContact_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_OrganizationContact] PRIMARY KEY CLUSTERED 
(
	[idOrganizationContact] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Foreign Key
--	[idOrganization] ASC

ALTER TABLE [dbo].[OrganizationContact]  WITH CHECK ADD  CONSTRAINT [FK_OrganizationContact_Organization] FOREIGN KEY([idOrganization])
REFERENCES [dbo].[Organization] ([idOrganization])
GO

ALTER TABLE [dbo].[OrganizationContact] CHECK CONSTRAINT [FK_OrganizationContact_Organization]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_OrganizationContact] ON [dbo].[OrganizationContact]
(
	[contactNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_OrganizationContact_nameAbbreviation] ON [dbo].[OrganizationContact]
(
	[nameAbbreviation] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE TABLE [dbo].[AppUser](
	[idAppUser] [bigint] IDENTITY(1,1) NOT NULL,
	[idOrganizationContact] [bigint] NOT NULL CONSTRAINT [DF_AppUser_idOrganizationContact] DEFAULT (0),
	[emailAddress] [varchar](100) NOT NULL CONSTRAINT [DF_AppUser_emailAddress] DEFAULT (''),
	[password] [varchar](150) NOT NULL CONSTRAINT [DF_AppUser_password] DEFAULT (''),
	[nameAbbreviation] [varchar](8) NOT NULL CONSTRAINT [DF_AppUser_nameAbbreviation] DEFAULT (''),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_AppUser_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_AppUser_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_AppUser_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_AppUser] PRIMARY KEY CLUSTERED 
(
	[idAppUser] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Foreign Key
--	[idOrganizationContact] ASC

ALTER TABLE [dbo].[AppUser]  WITH CHECK ADD  CONSTRAINT [FK_AppUser_OrganizationContact] FOREIGN KEY([idOrganizationContact])
REFERENCES [dbo].[OrganizationContact] ([idOrganizationContact])
GO

ALTER TABLE [dbo].[AppUser] CHECK CONSTRAINT [FK_AppUser_OrganizationContact]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_AppUser] ON [dbo].[AppUser]
(
	[idOrganizationContact]
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE TABLE [dbo].[UserOrganization](
	[idUserOrganization] [bigint] IDENTITY(1,1) NOT NULL,
	[idAppUser] [bigint] NOT NULL CONSTRAINT [DF_UserOrganization_idAppUser] DEFAULT (0),
	[idOrganization] [bigint] NOT NULL CONSTRAINT [DF_UserOrganization_idOrganization] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_UserOrganization_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_UserOrganization_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_UserOrganization_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_UserOrganization] PRIMARY KEY CLUSTERED 
(
	[idUserOrganization] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Foreign Keys
--	[idAppUser] ASC
--	[idOrganization] ASC

ALTER TABLE [dbo].[UserOrganization]  WITH CHECK ADD  CONSTRAINT [FK_UserOrganization_AppUser] FOREIGN KEY([idAppUser])
REFERENCES [dbo].[AppUser] ([idAppUser])
GO

ALTER TABLE [dbo].[UserOrganization] CHECK CONSTRAINT [FK_UserOrganization_AppUser]
GO

ALTER TABLE [dbo].[UserOrganization]  WITH CHECK ADD  CONSTRAINT [FK_UserOrganization_Organization] FOREIGN KEY([idOrganization])
REFERENCES [dbo].[Organization] ([idOrganization])
GO

ALTER TABLE [dbo].[UserOrganization] CHECK CONSTRAINT [FK_UserOrganization_Organization]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_UserOrganization] ON [dbo].[UserOrganization]
(
	[idAppUser] ASC,
	[idOrganization] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE TABLE [dbo].[SecurityGroup](
	[idSecurityGroup] [bigint] IDENTITY(1,1) NOT NULL,
	[groupName] [varchar](50) NOT NULL CONSTRAINT [DF_SecurityGroup_groupName] DEFAULT (''),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_SecurityGroup_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_SecurityGroup_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_SecurityGroup_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_SecurityGroup] PRIMARY KEY CLUSTERED 
(
	[idSecurityGroup] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_SecurityGroup] ON [dbo].[SecurityGroup]
(
	[groupName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE TABLE [dbo].[UserOrgSecurity](
	[idUserOrgSecurity] [bigint] IDENTITY(1,1) NOT NULL,
	[idUserOrganization] [bigint] NOT NULL CONSTRAINT [DF_UserOrgSecurity_idUserOrganization] DEFAULT (0),
	[idSecurityGroup] [bigint] NOT NULL CONSTRAINT [DF_UserOrgSecurity_idSecurityGroup] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_UserOrgSecurity_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_UserOrgSecurity_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_UserOrgSecurity_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_UserOrgSecurity] PRIMARY KEY CLUSTERED 
(
	[idUserOrgSecurity] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Foreign Keys
--	[idUserOrganization] ASC
--	[idSecurityGroup] ASC

ALTER TABLE [dbo].[UserOrgSecurity]  WITH CHECK ADD  CONSTRAINT [FK_UserOrgSecurity_SecurityGroup] FOREIGN KEY([idSecurityGroup])
REFERENCES [dbo].[SecurityGroup] ([idSecurityGroup])
GO

ALTER TABLE [dbo].[UserOrgSecurity] CHECK CONSTRAINT [FK_UserOrgSecurity_SecurityGroup]
GO

ALTER TABLE [dbo].[UserOrgSecurity]  WITH CHECK ADD  CONSTRAINT [FK_UserOrgSecurity_UserOrganization] FOREIGN KEY([idUserOrganization])
REFERENCES [dbo].[UserOrganization] ([idUserOrganization])
GO

ALTER TABLE [dbo].[UserOrgSecurity] CHECK CONSTRAINT [FK_UserOrgSecurity_UserOrganization]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_UserOrgSecurity] ON [dbo].[UserOrgSecurity]
(
	[idUserOrganization] ASC,
	[idSecurityGroup] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE TABLE [dbo].[App](
	[idApp] [bigint] IDENTITY(1,1) NOT NULL,
	[appName] [varchar](50) NOT NULL CONSTRAINT [DF_App_appName] DEFAULT (''),
	[displayName] [varchar](50) NOT NULL CONSTRAINT [DF_App_displayName] DEFAULT (''),
	[displaySeqNumber] [smallint] NOT NULL CONSTRAINT [DF_App_displaySeqNumber] DEFAULT (0),
	[url] [varchar](255) NOT NULL CONSTRAINT [DF_App_url] DEFAULT (''),
	[imageUrl] [varchar](255) NOT NULL CONSTRAINT [DF_App_imageUrl] DEFAULT (''),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_App_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_App_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_App_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_App] PRIMARY KEY CLUSTERED 
(
	[idApp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_App] ON [dbo].[App]
(
	[appName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_App_displayName] ON [dbo].[App]
(
	[displayName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE TABLE [dbo].[SubSystem](
	[idSubSystem] [bigint] IDENTITY(1,1) NOT NULL,
	[subSystemName] [varchar](50) NOT NULL CONSTRAINT [DF_SubSystem_subSystemName] DEFAULT (''),
	[displayName] [varchar](50) NOT NULL CONSTRAINT [DF_SubSystem_displayName] DEFAULT (''),
	[displaySeqNumber] [smallint] NOT NULL CONSTRAINT [DF_SubSystem_displaySeqNumber] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_SubSystem_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_SubSystem_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_SubSystem_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_SubSystem] PRIMARY KEY CLUSTERED 
(
	[idSubSystem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_SubSystem] ON [dbo].[SubSystem]
(
	[subSystemName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE TABLE [dbo].[AppSubSystem](
	[idAppSubSystem] [bigint] IDENTITY(1,1) NOT NULL,
	[idApp] [bigint] NOT NULL CONSTRAINT [DF_AppSubSystem_idApp] DEFAULT (0),
	[idSubSystem] [bigint] NOT NULL CONSTRAINT [DF_AppSubSystem_idSubSystem] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_AppSubSystem_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_AppSubSystem_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_AppSubSystem_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_AppSubSystem] PRIMARY KEY CLUSTERED 
(
	[idAppSubSystem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Alternate Keys
--	[idApp] ASC
--	[idSubSystem] ASC

ALTER TABLE [dbo].[AppSubSystem]  WITH CHECK ADD  CONSTRAINT [FK_AppSubSystem_App] FOREIGN KEY([idApp])
REFERENCES [dbo].[App] ([idApp])
GO

ALTER TABLE [dbo].[AppSubSystem] CHECK CONSTRAINT [FK_AppSubSystem_App]
GO

ALTER TABLE [dbo].[AppSubSystem]  WITH CHECK ADD  CONSTRAINT [FK_AppSubSystem_SubSystem] FOREIGN KEY([idSubSystem])
REFERENCES [dbo].[SubSystem] ([idSubSystem])
GO

ALTER TABLE [dbo].[AppSubSystem] CHECK CONSTRAINT [FK_AppSubSystem_SubSystem]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_AppSubSystem] ON [dbo].[AppSubSystem]
(
	[idApp] ASC,
	[idSubSystem] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE TABLE [dbo].[SubSystemFunction](
	[idSubSystemFunction] [bigint] IDENTITY(1,1) NOT NULL,
	[idSubSystem] [bigint] NOT NULL CONSTRAINT [DF_SubSystemFunction_idSubSystem] DEFAULT (0),
	[functionName] [varchar](50) NOT NULL CONSTRAINT [DF_SubSystemFunction_functionName] DEFAULT (''),
	[displayName] [varchar](50) NOT NULL CONSTRAINT [DF_SubSystemFunction_displayName] DEFAULT (''),
	[displaySeqNumber] [smallint] NOT NULL CONSTRAINT [DF_SubSystemFunction_displaySeqNumber] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_SubSystemFunction_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_SubSystemFunction_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_SubSystemFunction_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_SubSystemFunction] PRIMARY KEY CLUSTERED 
(
	[idSubSystemFunction] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Alternate Key
--	[idSubSystem] ASC

ALTER TABLE [dbo].[SubSystemFunction]  WITH CHECK ADD  CONSTRAINT [FK_SubSystemFunction_SubSystem] FOREIGN KEY([idSubSystem])
REFERENCES [dbo].[SubSystem] ([idSubSystem])
GO

ALTER TABLE [dbo].[SubSystemFunction] CHECK CONSTRAINT [FK_SubSystemFunction_SubSystem]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_SubSystemFunction] ON [dbo].[SubSystemFunction]
(
	[idSubSystem] ASC,
	[functionName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_SubSystemFunction_displayName] ON [dbo].[SubSystemFunction]
(
	[idSubSystem] ASC,
	[displayName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE TABLE [dbo].[FunctionSecurity](
	[idFunctionSecurity] [bigint] IDENTITY(1,1) NOT NULL,
	[idSubSystemFunction] [bigint] NOT NULL CONSTRAINT [DF_FunctionSecurity_idSubSystemFunction] DEFAULT (0),
	[idSecurityGroup] [bigint] NOT NULL CONSTRAINT [DF_FunctionSecurity_idSecurityGroup] DEFAULT (0),
	[securityCd] [smallint] NOT NULL CONSTRAINT [DF_FunctionSecurity_securityCd] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_FunctionSecurity_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_FunctionSecurity_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_FunctionSecurity_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_FunctionSecurity] PRIMARY KEY CLUSTERED 
(
	[idFunctionSecurity] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Alternate Keys
--	[idSubSystemFunction] ASC
--	[idSecurityGroup] ASC

ALTER TABLE [dbo].[FunctionSecurity]  WITH CHECK ADD  CONSTRAINT [FK_FunctionSecurity_SecurityGroup] FOREIGN KEY([idSecurityGroup])
REFERENCES [dbo].[SecurityGroup] ([idSecurityGroup])
GO

ALTER TABLE [dbo].[FunctionSecurity] CHECK CONSTRAINT [FK_FunctionSecurity_SecurityGroup]
GO

ALTER TABLE [dbo].[FunctionSecurity]  WITH CHECK ADD  CONSTRAINT [FK_FunctionSecurity_SubSystemFunction] FOREIGN KEY([idSubSystemFunction])
REFERENCES [dbo].[SubSystemFunction] ([idSubSystemFunction])
GO

ALTER TABLE [dbo].[FunctionSecurity] CHECK CONSTRAINT [FK_FunctionSecurity_SubSystemFunction]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_FunctionSecurity] ON [dbo].[FunctionSecurity]
(
	[idSubSystemFunction] ASC,
	[idSecurityGroup] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE TABLE [dbo].[UserFunction](
	[idUserFunction] [bigint] IDENTITY(1,1) NOT NULL,
	[idAppUser] [bigint] NOT NULL CONSTRAINT [DF_UserFunction_idAppUser] DEFAULT (0),
	[idSubSystemFunction] [bigint] NOT NULL CONSTRAINT [DF_UserFunction_idSubSystemFunction] DEFAULT (0),
	[lockedInd] [char](1) NOT NULL CONSTRAINT [DF_UserFunction_lockedInd] DEFAULT ('N'),
	[lockedDt] [datetime] NOT NULL CONSTRAINT [DF_UserFunction_lockedDt] DEFAULT ('1900-01-01'),
	[activeTabInd] [char](1) NOT NULL CONSTRAINT [DF_UserFunction_activeTabInd] DEFAULT ('N'),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_UserFunction_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_UserFunction_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_UserFunction_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_UserFunction] PRIMARY KEY CLUSTERED 
(
	[idUserFunction] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Alternate Keys
--	[idAppUser] ASC
--	[idSubSystemFunction] ASC

ALTER TABLE [dbo].[UserFunction]  WITH CHECK ADD  CONSTRAINT [FK_UserFunction_AppUser] FOREIGN KEY([idAppUser])
REFERENCES [dbo].[AppUser] ([idAppUser])
GO

ALTER TABLE [dbo].[UserFunction] CHECK CONSTRAINT [FK_UserFunction_AppUser]
GO

ALTER TABLE [dbo].[UserFunction]  WITH CHECK ADD  CONSTRAINT [FK_UserFunction_SubSystemFunction] FOREIGN KEY([idSubSystemFunction])
REFERENCES [dbo].[SubSystemFunction] ([idSubSystemFunction])
GO

ALTER TABLE [dbo].[UserFunction] CHECK CONSTRAINT [FK_UserFunction_SubSystemFunction]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_UserFunction] ON [dbo].[UserFunction]
(
	[idAppUser] ASC,
	[idSubSystemFunction] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE TABLE [dbo].[AppUserFavorite](
	[idAppUserFavorite] [bigint] identity(1,1),
	[idAppUser] [bigint] NOT NULL CONSTRAINT [DF_AppUserFavorite_idAppUser] DEFAULT (0),
	[idApp] [bigint] NOT NULL CONSTRAINT [DF_AppUserFavorite_idApp] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_AppUserFavorite_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_AppUserFavorite_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_AppUserFavorite_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_AppUserFavorite] PRIMARY KEY CLUSTERED 
(
	[idAppUserFavorite] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Foreign Keys
--	[idAppUser] ASC
--	[idApp] ASC

ALTER TABLE [dbo].[AppUserFavorite]  WITH CHECK ADD  CONSTRAINT [FK_AppUserFavorite_App] FOREIGN KEY([idApp])
REFERENCES [dbo].[App] ([idApp])
GO

ALTER TABLE [dbo].[AppUserFavorite] CHECK CONSTRAINT [FK_AppUserFavorite_App]
GO

ALTER TABLE [dbo].[AppUserFavorite]  WITH CHECK ADD  CONSTRAINT [FK_AppUserFavorite_AppUser] FOREIGN KEY([idAppUser])
REFERENCES [dbo].[AppUser] ([idAppUser])
GO

ALTER TABLE [dbo].[AppUserFavorite] CHECK CONSTRAINT [FK_AppUserFavorite_AppUser]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_AppUserFavorite] ON [dbo].[AppUserFavorite]
(
	[idAppUser] ASC,
	[idApp] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE TABLE [dbo].[AppUserSubSystemFavorite](
	[idAppUserSubSystemFavorite] [bigint] identity(1,1),
	[idAppUser] [bigint] NOT NULL CONSTRAINT [DF_AppUserSubSystemFavorite_idAppUser] DEFAULT (0),
	[idSubSystemFunction] [bigint] NOT NULL CONSTRAINT [DF_AppUserSubSystemFavorite_idSubSystemFunction] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_AppUserSubSystemFavorite_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_AppUserSubSystemFavorite_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_AppUserSubSystemFavorite_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_AppUserSubSystemFavorite] PRIMARY KEY CLUSTERED 
(
	[idAppUserSubSystemFavorite] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

-- Foreign Keys
--	[idAppUser] ASC
--	[idSubSystemFunction] ASC

ALTER TABLE [dbo].[AppUserSubSystemFavorite]  WITH CHECK ADD  CONSTRAINT [FK_AppUserSubSystemFavorite_AppUser] FOREIGN KEY([idAppUser])
REFERENCES [dbo].[AppUser] ([idAppUser])
GO

ALTER TABLE [dbo].[AppUserSubSystemFavorite] CHECK CONSTRAINT [FK_AppUserSubSystemFavorite_AppUser]
GO

ALTER TABLE [dbo].[AppUserSubSystemFavorite]  WITH CHECK ADD  CONSTRAINT [FK_AppUserSubSystemFavorite_SubSystemFunction] FOREIGN KEY([idSubSystemFunction])
REFERENCES [dbo].[SubSystemFunction] ([idSubSystemFunction])
GO

ALTER TABLE [dbo].[AppUserSubSystemFavorite] CHECK CONSTRAINT [FK_AppUserSubSystemFavorite_SubSystemFunction]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_AppUserSubSystemFavorite] ON [dbo].[AppUserSubSystemFavorite]
(
	[idAppUser] ASC,
	[idSubSystemFunction] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[SystemCodeType]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[SystemCodeType]; 
GO

CREATE TABLE [dbo].[SystemCodeType](
	[idSystemCodeType] [bigint] IDENTITY(1,1) NOT NULL,
	[codeType] [smallint] NOT NULL CONSTRAINT [DF_SystemCodeType_codeType] DEFAULT (0),
	[description] [varchar](255) NOT NULL CONSTRAINT [DF_SystemCodeType_description] DEFAULT (''),
	[columnName] [varchar](50) NOT NULL CONSTRAINT [DF_SystemCodeType_columnName] DEFAULT (''),
	[tableNames] [varchar](3900) NOT NULL CONSTRAINT [DF_SystemCodeType_tableNames] DEFAULT (''),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_SystemCodeType_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_SystemCodeType_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_SystemCodeType_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_SystemCodeType] PRIMARY KEY CLUSTERED 
(
	[idSystemCodeType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_SystemCodeType] ON [dbo].[SystemCodeType]
(
	[codeType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[SystemCode]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[SystemCode]; 
GO

CREATE TABLE [dbo].[SystemCode](
	[idSystemCode] [bigint] IDENTITY(1,1) NOT NULL,
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_SystemCode_appraisalYear] DEFAULT (0),
	[codeType] [smallint] NOT NULL CONSTRAINT [DF_SystemCode_codeType] DEFAULT (0),
	[code] [smallint] NOT NULL CONSTRAINT [DF_SystemCode_code] DEFAULT (0),
	[shortDescription] [varchar](25) NOT NULL CONSTRAINT [DF_SystemCode_shortDescription] DEFAULT (''),
	[description] [varchar](50) NOT NULL CONSTRAINT [DF_SystemCode_description] DEFAULT (''),
	[activeInd] [char](1) NOT NULL CONSTRAINT [DF_SystemCode_activeInd] DEFAULT ('Y'),
	[staticInd] [char](1) NOT NULL CONSTRAINT [DF_SystemCode_staticInd] DEFAULT ('Y'),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_SystemCode_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_SystemCode_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_SystemCode_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_SystemCode] PRIMARY KEY CLUSTERED 
(
	[idSystemCode] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_SystemCode] ON [dbo].[SystemCode]
(
	[appraisalYear] DESC,
	[codeType] ASC,
	[code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[SystemCodeGroup]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[SystemCodeGroup]; 
GO

CREATE TABLE [dbo].[SystemCodeGroup](
	[idSystemCodeGroup] [bigint] IDENTITY(1,1) NOT NULL,
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_SystemCodeGroup_appraisalYear] DEFAULT (0),
	[codeTypeParent] [smallint] NOT NULL CONSTRAINT [DF_SystemCodeGroup_codeTypeParent] DEFAULT (0),
	[codeParent] [smallint] NOT NULL CONSTRAINT [DF_SystemCodeGroup_codeParent] DEFAULT (0),
	[codeTypeChild] [smallint] NOT NULL CONSTRAINT [DF_SystemCodeGroup_codeTypeChild] DEFAULT (0),
	[codeChild] [smallint] NOT NULL CONSTRAINT [DF_SystemCodeGroup_codeChild] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_SystemCodeGroup_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_SystemCodeGroup_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_SystemCodeGroup_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_SystemCodeGroup] PRIMARY KEY CLUSTERED 
(
	[idSystemCodeGroup] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_SystemCodeGroup] ON [dbo].[SystemCodeGroup]
(
	[appraisalYear] DESC,
	[codeTypeParent] ASC,
	[codeParent] ASC,
	[codeTypeChild] ASC,
	[codeChild] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[SystemSetting]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[SystemSetting]; 
GO

CREATE TABLE [dbo].[SystemSetting](
	[idSystemSetting] [bigint] IDENTITY(1,1) NOT NULL,
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_SystemSetting_appraisalYear] DEFAULT (0),
	[settingName] [varchar](50) NOT NULL CONSTRAINT [DF_SystemSetting_settingName] DEFAULT (''),
	[settingValue] [varchar](255) NOT NULL CONSTRAINT [DF_SystemSetting_settingValue] DEFAULT (''),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_SystemSetting_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_SystemSetting_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_SystemSetting_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_SystemSetting] PRIMARY KEY CLUSTERED 
(
	[idSystemSetting] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_SystemSetting] ON [dbo].[SystemSetting]
(
	[appraisalYear] DESC,
	[settingName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
