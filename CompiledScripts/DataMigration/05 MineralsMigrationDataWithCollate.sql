USE [MINERALPRO]

DECLARE @MIN_APPRAISAL_YR SMALLINT
DECLARE @MAX_APPRAISAL_YR SMALLINT

SELECT @MIN_APPRAISAL_YR = MIN(APPRAISAL_YR)
  FROM [Minerals].[dbo].[APPRAISAL_YEAR]

SELECT @MAX_APPRAISAL_YR = MAX(APPRAISAL_YR)
  FROM [Minerals].[dbo].[APPRAISAL_YEAR]

SET @MIN_APPRAISAL_YR = 1999
--SET @MIN_APPRAISAL_YR = @MAX_APPRAISAL_YR - 1


DELETE FROM [MINERALPRO].[dbo].[AppraisalYear]


INSERT INTO [MINERALPRO].[dbo].[AppraisalYear]
           (appraisalYear
           ,currentInd
           ,futureInd
           ,collectionInd
           ,effectiveDt
           ,expirationDt
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            APY.APPRAISAL_YR
           ,APY.CURR_APPRL_YR_IND
           ,APY.FUTR_APPRL_YR_IND
           ,APY.COLL_APPRL_YR_IND
           ,APY.EFFECTIVE_DT
           ,APY.EXPIRATION_DT
           ,ISNULL(AU9.idAppUser,0) -- APY.LST_UPDT_EMPL_ID
           ,APY.LST_UPDT_TS
           ,CASE APY.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[APPRAISAL_YEAR] APY
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = APY.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE APY.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND APY.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[Report]


INSERT INTO [MINERALPRO].[dbo].[Report]
           (appraisalYear
           ,reportId
           ,reportTypeCdx --(CRX, LTR, XTR)
           ,reportCategoryCdx --(Report for CAD, Report to State)
           ,description
           ,fileName
           ,displaySeqNumber
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            APY.APPRAISAL_YR
           ,RPT.REPORT_ID
           ,'CRX'
           ,'Report for CAD'
           ,RPT.REPORT_DESC
           ,CASE
              WHEN LEFT(RPT.REPORT_FILE, 41) = '\\ECADBU\Public\MarsReports\MARSMinerals\'
                THEN RIGHT(RPT.REPORT_FILE,LEN(RPT.REPORT_FILE) - 41)
              WHEN LEFT(RPT.REPORT_FILE, 55) = '\\ECADBU\Public\Beyond Appraisal\Prod\Minerals\Reports\'
                THEN RIGHT(RPT.REPORT_FILE,LEN(RPT.REPORT_FILE) - 55)
              ELSE RPT.REPORT_FILE
            END
           ,RPT.SEQ_NUM
           ,ISNULL(AU9.idAppUser,0) -- RPT.LST_UPDT_EMPL_ID
           ,RPT.LST_UPDT_TS
           ,CASE RPT.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[REPORTS] RPT
 INNER JOIN [Minerals].[dbo].[APPRAISAL_YEAR] APY
    ON APY.FUTR_APPRL_YR_IND <> 'Y'
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = RPT.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE APY.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND RPT.POST_FLG <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[Report]
           (appraisalYear
           ,reportId
           ,reportTypeCdx --(CRX, LTR, XTR)
           ,reportCategoryCdx --(Report for CAD, Report to State)
           ,description
           ,fileName
           ,displaySeqNumber
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            APY.APPRAISAL_YR
           ,24
           ,'XTR'
           ,'Report for CAD'
           ,'Public Records'
           ,''
           ,24
           ,ISNULL(AU9.idAppUser,0)
           ,getdate()
           ,''
  FROM [Minerals].[dbo].[APPRAISAL_YEAR] APY
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = 'dbo'
 WHERE APY.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND APY.POST_FLG <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[Report]
           (appraisalYear
           ,reportId
           ,reportTypeCdx --(CRX, LTR, XTR)
           ,reportCategoryCdx --(Report for CAD, Report to State)
           ,description
           ,fileName
           ,displaySeqNumber
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            APY.APPRAISAL_YR
           ,25
           ,'XTR'
           ,'Report to State'
           ,'Mineral Certified Format'
           ,''
           ,1
           ,ISNULL(AU9.idAppUser,0)
           ,getdate()
           ,''
  FROM [Minerals].[dbo].[APPRAISAL_YEAR] APY
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = 'dbo'
 WHERE APY.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND APY.POST_FLG <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[Report]
           (appraisalYear
           ,reportId
           ,reportTypeCdx --(CRX, LTR, XTR)
           ,reportCategoryCdx --(Report for CAD, Report to State)
           ,description
           ,fileName
           ,displaySeqNumber
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            APY.APPRAISAL_YR
           ,26
           ,'XTR'
           ,'Report to State'
           ,'PTD Submission'
           ,''
           ,2
           ,ISNULL(AU9.idAppUser,0)
           ,getdate()
           ,''
  FROM [Minerals].[dbo].[APPRAISAL_YEAR] APY
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = 'dbo'
 WHERE APY.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND APY.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[Exemption]


INSERT INTO [MINERALPRO].[dbo].[Exemption]
           (appraisalYear
           ,exemptionTypeCd -- 270
           ,exemptionCd -- 3430 / 3440
           ,standardInd
           ,totalInd
           ,exemptionValue
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            EXM.APPRAISAL_YR
           ,EXM.EXEMPTION_TYP_CD
           ,EXM.EXEMPTION_CD
           ,CASE EXM.STD_EXEMPTION WHEN 1 THEN 'Y' ELSE 'N' END
           ,CASE EXM.TOTAL_EXEMPTION WHEN 1 THEN 'Y' ELSE 'N' END
           ,EXM.STD_EXEMPTION_AMT
           ,ISNULL(AU9.idAppUser,0) -- EXM.LST_UPDT_EMPL_ID
           ,EXM.LST_UPDT_TS
           ,CASE EXM.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[EXEMPTIONS] EXM
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = EXM.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE EXM.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND EXM.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[TaxUnit]


INSERT INTO [MINERALPRO].[dbo].[TaxUnit]
           (appraisalYear
           ,taxUnitTypeCd -- 970
           ,taxUnitCd -- 780 / 1060 / 1070 / 1080 / 1090 / 1100
           ,taxUnitAbbr
           ,taxUnitName
           ,taxUnitCdx
           ,estimatedTaxRate
           ,taxingInd
           ,defaultInd
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            TXJ.APPRAISAL_YR
           ,TXJ.JURIS_TYP_CD
           ,TXJ.TAX_JURIS_CD
           ,CASE TXJ.SHORT_NAME 
             WHEN '' THEN CAST(TXJ.JURIS_TYP_CD AS VARCHAR(4)) + CAST(TXJ.TAX_JURIS_CD AS VARCHAR(1)) 
             ELSE TXJ.SHORT_NAME
             END
           ,ISNULL(CTM.CODE_DESCRIPTION,'')
           ,ISNULL(CTM.LEGACY_VALUE,'')
           ,TXJ.TAX_RATE
           ,CASE TXJ.LIST_ON_NOTICE_IND WHEN 1 THEN 'Y' ELSE 'N' END
           ,CASE TXJ.JURIS_TYP_CD
             WHEN 1060 THEN 'Y'
             WHEN 1070 THEN 'Y'
             WHEN 1080 THEN 'Y'
             WHEN 1100 THEN 'Y'
             ELSE 'N'
             END
           ,ISNULL(AU9.idAppUser,0) -- TXJ.LST_UPDT_EMPL_ID
           ,TXJ.LST_UPDT_TS
           ,CASE TXJ.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[TAX_JURIS] TXJ
  LEFT OUTER JOIN [Minerals].[dbo].[CODE_TABLE_MASTER] CTM
    ON CTM.APPRAISAL_YR = TXJ.APPRAISAL_YR
   AND CTM.CODE_TYP = TXJ.JURIS_TYP_CD
   AND CTM.CODE = TXJ.TAX_JURIS_CD
--   AND CTM.POST_FLG <> 'D'
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = TXJ.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE TXJ.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND TXJ.POST_FLG <> 'D' 

--APY	CODE_TYP	CODE	CODE_DESCRIPTION
--2017	970		0	UNASSIGNED
--2017	970		780	CITY
--2017	970		1060	COUNTY
--2017	970		1070	SCHOOL
--2017	970		1080	HOSPITAL
--2017	970		1090	SPECIAL
--2017	970		1100	COLLEGE


DELETE FROM [MINERALPRO].[dbo].[TaxUnitExemption]


INSERT INTO [MINERALPRO].[dbo].[TaxUnitExemption]
           (appraisalYear
           ,taxUnitTypeCd -- 970
           ,taxUnitCd -- 780 / 1060 / 1070 / 1080 / 1090 / 1100
           ,exemptionTypeCd -- 270
           ,exemptionCd -- 3430 / 3440
           ,ownerExemptInd)
--           ,rowUpdateUserid
--           ,rowUpdateDt
--           ,rowDeleteFlag)
SELECT
            TJE.APPRAISAL_YR
           ,TJE.JURIS_TYP_CD
           ,TJE.TAX_JURIS_CD
           ,TJE.EXEMPTION_TYP_CD
           ,TJE.EXEMPTION_CD
           ,CASE TJE.OWNER_EXEMPT_IND WHEN 1 THEN 'Y' ELSE 'N' END
--            ,ISNULL(AU9.idAppUser,0) -- TJE.LST_UPDT_EMPL_ID
--           ,TJE.LST_UPDT_TS
--           ,CASE TJE.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[TAX_JURIS_EXEMPTIONS] TJE
--  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
--    ON AU9.nameAbbreviation = TJE.LST_UPDT_EMPL_ID
 WHERE TJE.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND TJE.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[Operator]


INSERT INTO [MINERALPRO].[dbo].[Operator]
           (appraisalYear
           ,operatorId
           ,operatorName
           ,ownerId
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            MOP.APPRAISAL_YR
           ,MOP.OPERATOR_NUM
           ,MOP.OPERATOR_NAME
           ,MOP.OWNER_ID
           ,ISNULL(AU9.idAppUser,0) -- MOP.LST_UPDT_EMPL_ID
           ,MOP.LST_UPDT_TS
           ,CASE MOP.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[MRL_OPERATORS] MOP
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = MOP.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE MOP.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND MOP.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[Field]


INSERT INTO [MINERALPRO].[dbo].[Field]
           (appraisalYear
           ,fieldId
           ,fieldName
           ,wellDepth
           ,gravityCd -- 3510
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            MRF.APPRAISAL_YR
           ,MRF.RRC_FIELD_NUM
           ,MRF.FIELD_NAME
           ,MRF.FIELD_DEPTH
           ,MRF.GRAVITY_CD
           ,ISNULL(AU9.idAppUser,0) -- MRF.LST_UPT_EMPL_ID
           ,MRF.LST_UPT_TS
           ,CASE MRF.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[MRL_RRC_FIELDS] MRF
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = MRF.LST_UPT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE MRF.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND MRF.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[Agency]


INSERT INTO [MINERALPRO].[dbo].[Agency]
           (appraisalYear
           ,agencyCdx
           ,agencyname
           ,addrLine1
           ,addrLine2
           ,addrLine3
           ,city
           ,stateCd -- 790
           ,zipcode
           ,countryCd -- 810
           ,phoneNum
           ,faxNum
           ,emailAddress
           ,statusCd  -- 30
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            TXR.APPRAISAL_YR
           ,CASE 
               WHEN LEN(TXR.TAXPAYER_REP_ID) = 1 THEN ('0000' + TXR.TAXPAYER_REP_ID)
               WHEN LEN(TXR.TAXPAYER_REP_ID) = 2 THEN ('000' + TXR.TAXPAYER_REP_ID)
               WHEN LEN(TXR.TAXPAYER_REP_ID) = 3 THEN ('00' + TXR.TAXPAYER_REP_ID)
               END
           ,TXR.NAME
           ,TXR.ADDRESS_LINE1
           ,'' -- addrLine2
           ,'' -- addrLine2
           ,TXR.CITY
           ,TXR.STATE_CD
           ,TXR.ZIPCODE
           ,0 -- countryCd
           ,TXR.PHONE_NUM
           ,TXR.FAX_NUM
           ,'' -- emailAddress
           ,CASE TXR.ACTIVE_IND WHEN 1 THEN 0 ELSE 14 END
           ,ISNULL(AU9.idAppUser,0) -- TXR.LST_UPDT_EMPL_ID
           ,TXR.LST_UPDT_TS
           ,CASE TXR.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[TAXPAYER_REP] TXR
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = TXR.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE TXR.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND TXR.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[ArbParticipant]


INSERT INTO [dbo].[ArbParticipant]
           ([appraisalYear]
           ,[groupCdx]
           ,[fullName]
           ,[seqNumber]
           ,[arbParticipantId]
           ,[chairmanInd]
           ,[lastName]
           ,[firstName]
           ,[title])
     VALUES
           (2017
           ,'ARB MEMBER'
           ,'MS. CAMILLA BLAIN'
           ,1
           ,1
           ,'Y'
           ,'BLAIN'
           ,'CAMILLA'
           ,'CHAIRMAN')


INSERT INTO [dbo].[ArbParticipant]
           ([appraisalYear]
           ,[groupCdx]
           ,[fullName]
           ,[seqNumber]
           ,[arbParticipantId]
           ,[chairmanInd]
           ,[lastName]
           ,[firstName]
           ,[title])
     VALUES
           (2017
           ,'ARB MEMBER'
           ,'MS. DIANE LEE'
           ,2
           ,2
           ,'N'
           ,'LEE'
           ,'DIANE'
           ,'VICE CHAIRMAN')


INSERT INTO [dbo].[ArbParticipant]
           ([appraisalYear]
           ,[groupCdx]
           ,[fullName]
           ,[seqNumber]
           ,[arbParticipantId]
           ,[chairmanInd]
           ,[lastName]
           ,[firstName]
           ,[title])
     VALUES
           (2017
           ,'ARB MEMBER'
           ,'MS. DEBI HAYS'
           ,3
           ,3
           ,'N'
           ,'HAYS'
           ,'DEBI'
           ,'MEMBER')


INSERT INTO [dbo].[ArbParticipant]
           ([appraisalYear]
           ,[groupCdx]
           ,[fullName]
           ,[seqNumber]
           ,[arbParticipantId]
           ,[chairmanInd]
           ,[lastName]
           ,[firstName]
           ,[title])
     VALUES
           (2017
           ,'ARB MEMBER'
           ,'MS. JANIS MORGAN'
           ,4
           ,4
           ,'N'
           ,'MORGAN'
           ,'JANIS'
           ,'SECRETARY')


INSERT INTO [dbo].[ArbParticipant]
           ([appraisalYear]
           ,[groupCdx]
           ,[fullName]
           ,[seqNumber]
           ,[arbParticipantId]
           ,[chairmanInd]
           ,[lastName]
           ,[firstName]
           ,[title])
     VALUES
           (2017
           ,'ARB MEMBER'
           ,'MR. NATHEN BIEBER'
           ,5
           ,5
           ,'N'
           ,'BIEBER'
           ,'NATHEN'
           ,'MEMBER')


DELETE FROM [MINERALPRO].[dbo].[Owner]


INSERT INTO [MINERALPRO].[dbo].[Owner]
           (appraisalYear
           ,ownerId
           ,name1
           ,name2
           ,name3
           ,addrLine1
           ,addrLine2
           ,addrLine3
           ,city
           ,stateCd -- 790
           ,zipcode
           ,countryCd -- 810
           ,phoneNum
           ,emailAddress
           ,birthDt
           ,confidentialInd
           ,ownerChangeCd  -- 3190
           ,agencyCdx
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            OAD.APPRAISAL_YR
           ,OWN.OWNER_ID
           ,OWN.NAME -- OWNER NAME
           ,OWN.NAME2 -- 779 = 'SHELL OWNER', ALL OTHERS BLANK
           ,'' -- name3
           ,OAD.ADDRESS_LINE1 -- 5486 <> '', 1320 'C/O', 1621 'TRAN', 2538 NAME OR ADDRESS
           ,OAD.ADDRESS_LINE2 -- 636 <> ''
           ,OAD.ADDRESS_LINE3 -- 14967 <> ''
           ,OAD.CITY
           ,OAD.STATE_CD
           ,OAD.ZIPCODE
           ,OAD.COUNTRY_CD
           ,OWN.PHONE_NUM
           ,'' -- emailAddress
           ,OWN.BIRTH_DT
           ,CASE WHEN OWN.PRIVACY_IND = 0 THEN 'N' ELSE 'Y' END
           ,OWN.OWNER_CHG_CD
           ,CASE 
               WHEN LEN(OWN.TAX_REP_ID) = 0 THEN ('' + OWN.TAX_REP_ID)
               WHEN LEN(OWN.TAX_REP_ID) = 1 THEN ('0000' + OWN.TAX_REP_ID)
               WHEN LEN(OWN.TAX_REP_ID) = 2 THEN ('000' + OWN.TAX_REP_ID)
               WHEN LEN(OWN.TAX_REP_ID) = 3 THEN ('00' + OWN.TAX_REP_ID)
               END
           ,CASE 
               WHEN OAD.LST_UPDT_TS > OWN.LST_UPDT_TS THEN ISNULL(AU2.idAppUser,0) 
               ELSE ISNULL(AU9.idAppUser,0)
            END
           ,CASE 
               WHEN OAD.LST_UPDT_TS > OWN.LST_UPDT_TS THEN OAD.LST_UPDT_TS
               ELSE OWN.LST_UPDT_TS
            END
           ,CASE 
               WHEN OAD.LST_UPDT_TS > OWN.LST_UPDT_TS THEN CASE OAD.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
               ELSE CASE OWN.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
            END
  FROM [Minerals].[dbo].[OWNER] OWN
 INNER JOIN [Minerals].[dbo].[OWNER_ADDRESS] OAD
    ON OAD.OWNER_ID = OWN.OWNER_ID
   AND OAD.POST_FLG <> 'D'
   AND OAD.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU2
    ON AU2.nameAbbreviation = OAD.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = OWN.LST_UPDT_EMPL_ID  COLLATE DATABASE_DEFAULT
 WHERE OWN.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[Note]


INSERT INTO [MINERALPRO].[dbo].[Note]
           (subjectTypeCd -- 3540 (NEW) (1-UNIT, 2-LEASE, 3-OWNER)
           ,subjectId
           ,rrcNumber
           ,seqNumber
           ,createDt
           ,noteTypeCd -- From NoteType
           ,quickNoteCd -- From QuickNote
           ,securityCd -- 530 (0-UN, 1-GEN, 2-CONF)
           ,groupCd -- 3530 (NEW) (0-UN, 1-PR/E)
           ,referenceAppraisalYear
           ,note
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            3 -- OWNER
           ,CMT.OWNER_ID
           ,0 -- NO RRC AT THE OWNER LEVEL
           ,CMT.SEQ_NUM
           ,CMT.CREATE_TS
           ,CMT.COMMENT_TYP_CD
           ,CMT.CANNED_COMMENT_CD
           ,CMT.COMMENT_SEC_CD
           ,CASE CMT.PRE_IND WHEN 'Y' THEN 1 ELSE 0 END
           ,CMT.APPL_APPRAISAL_YR
           ,CMT.COMMENT_TEXT
           ,ISNULL(AU9.idAppUser,0) -- CMT.LST_UPDT_EMPL_ID
           ,CMT.LST_UPDT_TS
           ,CASE CMT.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[COMMENT] CMT
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = CMT.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE CMT.OWNER_ID <> 0
--     AND CMT.POST_FLG <> 'D'


DELETE FROM [MINERALPRO].[dbo].[OwnerExemption]


INSERT INTO [MINERALPRO].[dbo].[OwnerExemption]
           (appraisalYear
           ,ownerId
           ,exemptionTypeCd -- 270
           ,exemptionCd -- 3430 / 3440
           ,effectiveDt
           ,expirationDt
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            MOE.APPRAISAL_YR
           ,MOE.OWNER_ID
           ,MOE.EXEMPTION_TYP_CD
           ,MOE.EXEMPTION_CD
           ,CAST(MOE.EFFECTIVE_YR AS CHAR(4)) + '-01-01' 
           ,'9999-12-31'
           ,ISNULL(AU9.idAppUser,0) -- MOE.LST_UPDT_EMPL_ID
           ,MOE.LST_UPDT_TS
           ,CASE MOE.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[MRL_OWNER_EXEMPTIONS] MOE
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = MOE.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE MOE.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
   AND MOE.EXEMPTION_CD <> 0
--   AND MOE.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[Lease]


INSERT INTO [MINERALPRO].[dbo].[Lease]
           (appraisalYear
           ,leaseId
           ,leaseName
--           ,divisionOrderDt
--           ,divisionOrderDescription
           ,description
           ,comment
           ,cadCategoryCd -- 640
           ,changeReasonCd -- 3190
           ,operatorId
           ,acres
           ,leaseYear
           ,unitId
           ,lastUpdateUserid
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            AAY.APPRAISAL_YR
           ,CAST(AAY.ACCOUNT_NUM AS INT)
           ,AAY.ACCOUNT_NAME
--           ,'1900-01-01' <== DEFAULT
--           ,ISNULL(MOB.PIPELINE,'')
           ,ISNULL(MOB.LEASE_DESC,'')
           ,ISNULL(MOB.LEASE_NOTE,'')
           ,AAY.P_SPTD_CD
           ,ISNULL(MOB.LEASE_CHG_CD,0) -- 3190
           ,CASE WHEN AAY.ECONOMIC_UNIT_ID = 0 THEN ISNULL(MOB.OPERATOR_NUM,0) ELSE 0 END
           ,ISNULL(MOB.LEASE_ACRES,0.0)
           ,ISNULL(MOB.LEASE_YR,0)
           ,AAY.ECONOMIC_UNIT_ID
           ,ISNULL(AU.idAppUser,0) -- AAY.LST_ACCT_UPDT_ID
           ,CASE 
               WHEN ISNULL(MOB.LST_UPDT_TS,'1900-01-01') > AAY.LST_UPDT_TS THEN ISNULL(AU2.idAppUser,0) 
               ELSE ISNULL(AU9.idAppUser,0)
            END
           ,CASE 
               WHEN ISNULL(MOB.LST_UPDT_TS,'1900-01-01') > AAY.LST_UPDT_TS THEN ISNULL(MOB.LST_UPDT_TS,'1900-01-01')
               ELSE AAY.LST_UPDT_TS
            END
           ,CASE 
               WHEN ISNULL(MOB.LST_UPDT_TS,'1900-01-01') > AAY.LST_UPDT_TS THEN CASE ISNULL(MOB.POST_FLG,'') WHEN 'D' THEN 'D' ELSE '' END
               ELSE CASE AAY.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
            END
  FROM [Minerals].[dbo].[ACCOUNT_APPRL_YEAR] AAY
  LEFT OUTER JOIN [Minerals].[dbo].[MRL_OBJECT] MOB
    ON MOB.APPRAISAL_YR = AAY.APPRAISAL_YR
   AND MOB.ACCOUNT_NUM = AAY.ACCOUNT_NUM
--   AND MOB.POST_FLG <> 'D'
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU 
    ON AU.nameAbbreviation = AAY.LST_ACCT_UPDT_ID COLLATE DATABASE_DEFAULT
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU2
    ON AU2.nameAbbreviation = MOB.LST_UPDT_EMPL_ID  COLLATE DATABASE_DEFAULT
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = AAY.LST_UPDT_EMPL_ID  COLLATE DATABASE_DEFAULT
 WHERE AAY.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND AAY.POST_FLG <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[Note]
           (subjectTypeCd -- 3540 (NEW) (1-UNIT, 2-LEASE, 3-OWNER)
           ,subjectId
           ,rrcNumber
           ,seqNumber
           ,createDt
           ,noteTypeCd -- From NoteType
           ,quickNoteCd -- From QuickNote
           ,securityCd -- 530 (0-UN, 1-GEN, 2-CONF)
           ,groupCd -- 3530 (NEW) (0-UN, 1-PR/E)
           ,referenceAppraisalYear
           ,note
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            2 -- LEASE
           ,CAST(CMT.ACCOUNT_NUM AS INT)
           ,CMT.RRC_NUM
           ,CMT.SEQ_NUM
           ,CMT.CREATE_TS
           ,CMT.COMMENT_TYP_CD
           ,CMT.CANNED_COMMENT_CD
           ,CMT.COMMENT_SEC_CD
           ,CASE CMT.PRE_IND WHEN 'Y' THEN 1 ELSE 0 END
           ,CMT.APPL_APPRAISAL_YR
           ,CMT.COMMENT_TEXT
           ,ISNULL(AU9.idAppUser,0) -- CMT.LST_UPDT_EMPL_ID
           ,CMT.LST_UPDT_TS
           ,CASE CMT.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[COMMENT] CMT
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = CMT.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE CMT.ACCOUNT_NUM NOT IN('','0')
--     AND CMT.POST_FLG <> 'D'


DELETE FROM [MINERALPRO].[dbo].[LeaseTaxUnit]


INSERT INTO [MINERALPRO].[dbo].[LeaseTaxUnit]
           (appraisalYear
           ,leaseId
           ,taxUnitTypeCd -- 970
           ,taxUnitCd -- 780 / 1060 / 1070 / 1080 / 1090 / 1100
           ,taxUnitPercent
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            ACJ.APPRAISAL_YR
           ,CAST(ACJ.ACCOUNT_NUM AS INT)
           ,ACJ.JURIS_TYP_CD
           ,ACJ.JURIS_CD
           ,ACJ.JURIS_PCT
           ,ISNULL(AU9.idAppUser,0) -- ACJ.LST_UPDT_EMPL_ID
           ,ACJ.LST_UPDT_TS
           ,CASE ACJ.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[ACCOUNT_JURISDICTIONS] ACJ
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = ACJ.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE ACJ.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND ACJ.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[LeaseOwner]


INSERT INTO [MINERALPRO].[dbo].[LeaseOwner]
           (appraisalYear
           ,leaseId
           ,ownerId
           ,interestTypeCd -- 3000
           ,interestPercent
           ,ownerValue
           ,lockCd -- 40
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            MOW.APPRAISAL_YR
           ,CAST(MOW.ACCOUNT_NUM AS INT)
           ,MOW.OWNER_ID
           ,MOW.LEASE_INT_TYP_CD
           ,MOW.LEASE_INT_PCT
           ,CASE MOW.OWNER_VAL
              WHEN 0 THEN
                dbo.udfBankRound(
                  CASE MOW.LEASE_INT_TYP_CD
                    WHEN 4
                      THEN
                        CASE
                          WHEN ISNULL(MOB.LEASE_WI_PCT,0) = 0
                            THEN 0
                          ELSE
                            (ISNULL(MOB.LEASE_WI_VALUE,0) / ISNULL(MOB.LEASE_WI_PCT,0)) * MOW.LEASE_INT_PCT
                          END
                    ELSE
                        CASE
                          WHEN ISNULL(MOB.LEASE_OP_PCT,0) + ISNULL(MOB.LEASE_OR_PCT,0) + ISNULL(MOB.LEASE_RI_PCT,0) = 0
                            THEN 0
                          ELSE
                            (ISNULL(MOB.LEASE_RI_VALUE,0) / (ISNULL(MOB.LEASE_OP_PCT,0) + ISNULL(MOB.LEASE_OR_PCT,0) + ISNULL(MOB.LEASE_RI_PCT,0)) * MOW.LEASE_INT_PCT)
                          END
                    END, 0)
              ELSE
                MOW.OWNER_VAL 
              END AS OWNER_VAL
           ,MOW.FREEZE_CD
           ,ISNULL(AU9.idAppUser,0) -- MOW.LST_UPDT_EMP_ID
           ,MOW.LST_UPDT_TS
           ,CASE MOW.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[MRL_OWNER] MOW
  LEFT OUTER JOIN [Minerals].[dbo].[MRL_OBJECT] MOB
    ON MOB.APPRAISAL_YR = MOW.APPRAISAL_YR
   AND MOB.ACCOUNT_NUM = MOW.ACCOUNT_NUM
--   AND MOB.POST_FLG <> 'D' 
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = MOW.LST_UPDT_EMP_ID COLLATE DATABASE_DEFAULT
 WHERE MOW.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND MOW.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[LeaseValueSummary]


INSERT INTO [MINERALPRO].[dbo].[LeaseValueSummary]
           (appraisalYear
           ,leaseId
           ,workingInterestPercent
           ,royaltyInterestPercent
           ,overridingRoyaltyPercent
           ,oilPaymentPercent
           ,workingInterestValue
           ,royaltyInterestValue
           ,lastAppraisalYear
           ,appraisedValue
           ,appraisalDt
           ,tractNum
           ,unitWorkingInterestPercent
           ,unitRoyaltyInterestPercent
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            AAY.APPRAISAL_YR
           ,CAST(AAY.ACCOUNT_NUM AS INT)
           ,ISNULL(MOB.LEASE_WI_PCT,0.0)
           ,ISNULL(MOB.LEASE_RI_PCT,0.0)
           ,ISNULL(MOB.LEASE_OR_PCT,0.0)
           ,ISNULL(MOB.LEASE_OP_PCT,0.0)
           ,ISNULL(MOB.LEASE_WI_VALUE,0)
           ,ISNULL(MOB.LEASE_RI_VALUE,0)
           ,AAY.LAST_APPRAISAL_YR
           ,AAY.TOT_VAL
           ,AAY.APPRAISAL_DT
           ,ISNULL(MOB.TRACT_NUM,'')
           ,ISNULL(MOB.UNIT_WI_PCT,0)
           ,ISNULL(MOB.UNIT_RI_PCT,0)
           ,CASE 
               WHEN ISNULL(MOB.LST_UPDT_TS,'1900-01-01') > AAY.LST_UPDT_TS THEN ISNULL(AU2.idAppUser,0) 
               ELSE ISNULL(AU9.idAppUser,0)
            END
           ,CASE 
               WHEN ISNULL(MOB.LST_UPDT_TS,'1900-01-01') > AAY.LST_UPDT_TS THEN ISNULL(MOB.LST_UPDT_TS,'1900-01-01')
               ELSE AAY.LST_UPDT_TS
            END
           ,CASE 
               WHEN ISNULL(MOB.LST_UPDT_TS,'1900-01-01') > AAY.LST_UPDT_TS THEN CASE ISNULL(MOB.POST_FLG,'') WHEN 'D' THEN 'D' ELSE '' END
               ELSE CASE AAY.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
            END
  FROM [Minerals].[dbo].[ACCOUNT_APPRL_YEAR] AAY
  LEFT OUTER JOIN [Minerals].[dbo].[MRL_OBJECT] MOB
    ON MOB.APPRAISAL_YR = AAY.APPRAISAL_YR
   AND MOB.ACCOUNT_NUM = AAY.ACCOUNT_NUM
--   AND MOB.POST_FLG <> 'D'
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU2
    ON AU2.nameAbbreviation = MOB.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = AAY.LST_UPDT_EMPL_ID  COLLATE DATABASE_DEFAULT
 WHERE AAY.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND AAY.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[CalcTaxAdj]


INSERT INTO [MINERALPRO].[dbo].[CalcTaxAdj]
           (appraisalYear
           ,wellTypeCd -- 3140
           ,taxTypeCd -- 3180
           ,taxPercent
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            MTA.APPRAISAL_YR
           ,MTA.WELL_TYP_CD -- 3140
           ,MTA.MRL_TAX_TYP_CD -- 3180
           ,MTA.TAX_PCT
           ,ISNULL(AU9.idAppUser,0) -- MTA.LST_UPDT_EMPL_ID
           ,MTA.LST_UPDT_TS
           ,CASE MTA.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[MRL_TAX_ADJ] MTA
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = MTA.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE MTA.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND MTA.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[CalcEscalatedPriceAdj]


INSERT INTO [MINERALPRO].[dbo].[CalcEscalatedPriceAdj]
           (appraisalYear
           ,wellTypeCd -- 3140
           ,yearNumber
           ,adjPercent)
--           ,rowUpdateUserid
--           ,rowUpdateDt
--           ,rowDeleteFlag)
SELECT
            MEA.APPRAISAL_YR
           ,MEA.WELL_TYP_CD
           ,MEA.APPRL_ADJ_YEAR
           ,MEA.ADJ_PCT
--           ,ISNULL(AU9.idAppUser,0) -- MEA.LST_UPDT_EMP_ID
--           ,MEA.LST_UPDT_TS
--           ,CASE MEA.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[MRL_ESCALATED_ADJ] MEA
--  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
--    ON AU9.nameAbbreviation = MEA.LST_UPDT_EMP_ID
 WHERE MEA.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND MEA.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[CalcEscalatedOpExpAdj]


INSERT INTO [MINERALPRO].[dbo].[CalcEscalatedOpExpAdj]
           (appraisalYear
           ,yearNumber
           ,adjPercent)
--           ,rowUpdateUserid
--           ,rowUpdateDt
--           ,rowDeleteFlag)
SELECT
            MEO.APPRAISAL_YR
           ,MEO.APPRL_ADJ_YEAR
           ,MEO.ADJ_PCT
--           ,ISNULL(AU9.idAppUser,0) -- MEO.LST_UPDT_EMP_ID
--           ,MEO.LST_UPDT_TS
--           ,CASE MEO.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[MRL_ESCALATED_ADJ_OPER] MEO
--  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
--    ON AU9.nameAbbreviation = MEO.LST_UPDT_EMP_ID
 WHERE MEO.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND MEO.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[CalcEquipmentSchedule]


INSERT INTO [MINERALPRO].[dbo].[CalcEquipmentSchedule]
           (appraisalYear
           ,scheduleNumber
           ,wellTypeCd -- 3150
           ,valueTypeCd -- 3160
           ,maxDepth
           ,salvageValue
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            MLE.APPRAISAL_YR
           ,MLE.EQUIP_SCHED
           ,MLE.EQUIP_SCHED_WT_CD
           ,MLE.ES_WT_SCHED_VAL_CD
           ,MLE.MAX_DEPTH
           ,MLE.SALVAGE_VAL
           ,ISNULL(AU9.idAppUser,0) -- MLE.LST_UPDT_EMPL_ID
           ,MLE.LST_UPDT_TS
           ,CASE MLE.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[MRL_LEASE_EQUIP_SCHED] MLE
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = MLE.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE MLE.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND MLE.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[CalcEquipmentPVSchedule]


INSERT INTO [MINERALPRO].[dbo].[CalcEquipmentPVSchedule]
           (appraisalYear
           ,yearNumber
           ,factor)
--           ,rowUpdateUserid
--           ,rowUpdateDt
--           ,rowDeleteFlag)
SELECT
            EPV.APPRAISAL_YR
           ,EPV.PV_YEAR
           ,EPV.PV_FACTOR
--           ,ISNULL(AU9.idAppUser,0) -- EPV.LST_UPDT_EMP_ID
--           ,EPV.LST_UPDT_TS
--           ,CASE EPV.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[EQUIP_PV_SCHED] EPV
--  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
--    ON AU9.nameAbbreviation = EPV.LST_UPDT_EMP_ID
 WHERE EPV.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND EPV.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[Unit]


INSERT INTO [MINERALPRO].[dbo].[Unit]
           (appraisalYear
           ,unitId
           ,unitName
           ,operatorId
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            ECU.APPRAISAL_YR
           ,ECU.ECONOMIC_UNIT_ID
           ,ECU.ECON_UNIT_DESC
           ,ISNULL(EUM.OPERATOR_NUM,0)
           ,CASE 
               WHEN ISNULL(EUM.LST_UPDT_TS,'1900-01-01') > ECU.LST_UPDT_TS THEN ISNULL(AU2.idAppUser,0) 
               ELSE ISNULL(AU9.idAppUser,0)
            END
           ,CASE 
               WHEN ISNULL(EUM.LST_UPDT_TS,'1900-01-01') > ECU.LST_UPDT_TS THEN ISNULL(EUM.LST_UPDT_TS,'1900-01-01')
               ELSE ECU.LST_UPDT_TS
            END
           ,CASE 
               WHEN ISNULL(EUM.LST_UPDT_TS,'1900-01-01') > ECU.LST_UPDT_TS THEN CASE ISNULL(EUM.POST_FLG,'') WHEN 'D' THEN 'D' ELSE '' END
               ELSE CASE ECU.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
            END
  FROM [Minerals].[dbo].[ECONOMIC_UNIT] ECU
  LEFT OUTER JOIN [Minerals].[dbo].[ECON_UNIT_MRL] EUM
    ON EUM.APPRAISAL_YR = ECU.APPRAISAL_YR
   AND EUM.ECONOMIC_UNIT_ID = ECU.ECONOMIC_UNIT_ID
--   AND EUM.POST_FLG <> 'D' 
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU2
    ON AU2.nameAbbreviation = EUM.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = ECU.LST_UPDT_EMPL_ID  COLLATE DATABASE_DEFAULT
 WHERE ECU.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND ECU.POST_FLG <> 'D'


INSERT INTO [MINERALPRO].[dbo].[Note]
           (subjectTypeCd -- 3540 (NEW) (1-UNIT, 2-LEASE, 3-OWNER)
           ,subjectId
           ,rrcNumber
           ,seqNumber
           ,createDt
           ,noteTypeCd -- From NoteType
           ,quickNoteCd -- From QuickNote
           ,securityCd -- 530 (0-UN, 1-GEN, 2-CONF)
           ,groupCd -- 3530 (NEW) (0-UN, 1-PR/E)
           ,referenceAppraisalYear
           ,note
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            1 -- UNIT
           ,CMT.ECONOMIC_UNIT_ID
           ,CMT.RRC_NUM
           ,CMT.SEQ_NUM
           ,CMT.CREATE_TS
           ,CMT.COMMENT_TYP_CD
           ,CMT.CANNED_COMMENT_CD
           ,CMT.COMMENT_SEC_CD
           ,CASE CMT.PRE_IND WHEN 'Y' THEN 1 ELSE 0 END
           ,CMT.APPL_APPRAISAL_YR
           ,CMT.COMMENT_TEXT
           ,ISNULL(AU9.idAppUser,0) -- CMT.LST_UPDT_EMPL_ID
           ,CMT.LST_UPDT_TS
           ,CASE CMT.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[COMMENT] CMT
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = CMT.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE CMT.ECONOMIC_UNIT_ID <> 0
--     AND CMT.POST_FLG <> 'D'


DELETE FROM [MINERALPRO].[dbo].[UnitValueSummary]


INSERT INTO [MINERALPRO].[dbo].[UnitValueSummary]
           (appraisalYear
           ,unitId
           ,workingInterestPercent
           ,royaltyInterestPercent
           ,weightedWorkingInterestPercent
           ,weightedRoyaltyInterestPercent
           ,workingInterestValue
           ,royaltyInterestValue
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            ECU.APPRAISAL_YR
           ,ECU.ECONOMIC_UNIT_ID
           ,ISNULL(EUM.WI_FACTOR,0.0)
           ,ISNULL(EUM.RI_FACTOR,0.0)
           ,ISNULL(EUM.WGT_WI_PCT,0.0)
           ,ISNULL(EUM.WGT_RI_PCT,0.0)
           ,ISNULL(EUM.TOT_WI_VAL,0)
           ,ISNULL(EUM.TOT_RI_VAL,0)
           ,CASE 
               WHEN ISNULL(EUM.LST_UPDT_TS,'1900-01-01') > ECU.LST_UPDT_TS THEN ISNULL(AU2.idAppUser,0) 
               ELSE ISNULL(AU9.idAppUser,0)
            END
           ,CASE 
               WHEN ISNULL(EUM.LST_UPDT_TS,'1900-01-01') > ECU.LST_UPDT_TS THEN ISNULL(EUM.LST_UPDT_TS,'1900-01-01')
               ELSE ECU.LST_UPDT_TS
            END
           ,CASE 
               WHEN ISNULL(EUM.LST_UPDT_TS,'1900-01-01') > ECU.LST_UPDT_TS THEN CASE ISNULL(EUM.POST_FLG,'') WHEN 'D' THEN 'D' ELSE '' END
               ELSE CASE ECU.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
            END
  FROM [Minerals].[dbo].[ECONOMIC_UNIT] ECU
  LEFT OUTER JOIN [Minerals].[dbo].[ECON_UNIT_MRL] EUM
    ON EUM.APPRAISAL_YR = ECU.APPRAISAL_YR
   AND EUM.ECONOMIC_UNIT_ID = ECU.ECONOMIC_UNIT_ID
--   AND EUM.POST_FLG <> 'D' 
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU2
    ON AU2.nameAbbreviation = EUM.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = ECU.LST_UPDT_EMPL_ID  COLLATE DATABASE_DEFAULT
 WHERE ECU.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND ECU.POST_FLG <> 'D'
   AND EUM.ECONOMIC_UNIT_ID IS NOT NULL


DELETE FROM [MINERALPRO].[dbo].[Appraisal]


INSERT INTO [MINERALPRO].[dbo].[Appraisal]
           (appraisalYear
           ,subjectTypeCd -- 3540 (NEW) (1-UNIT, 2-LEASE)
           ,subjectId 
           ,rrcNumber 
           ,fieldId 
           ,wellDepth 
           ,gravityCd -- 3510
           ,wellDescription 
           ,wellTypeCd -- 3140
           ,comment 
           ,productionEquipmentCount 
           ,productionEquipmentSchedule 
           ,productionEquipmentValue 
           ,serviceEquipmentCount 
           ,serviceEquipmentSchedule 
           ,serviceEquipmentValue 
           ,injectionEquipmentCount 
           ,injectionEquipmentSchedule 
           ,injectionEquipmentValue 
           ,maxYearLife 
           ,discountRate 
           ,grossOilPrice 
           ,grossProductPrice 
           ,grossWorkingInterestGasPrice 
           ,grossRoyaltyInterestGasPrice 
           ,operatingExpense 
           ,dailyAverageOil 
           ,dailyAverageGas 
           ,dailyAverageProduct 
           ,yearLife 
           ,reserveOilValue 
           ,accumOilProduction 
           ,accumGasProduction 
           ,accumProductProduction 
           ,totalValue 
           ,workingInterestOilValue 
           ,workingInterestGasValue 
           ,workingInterestProductValue 
           ,workingInterestTotalValue 
           ,workingInterestTotalPV 
           ,royaltyInterestOilValue 
           ,royaltyInterestGasValue 
           ,royaltyInterestProductValue 
           ,royaltyInterestTotalValue 
           ,lastAppraisalYear 
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            MAY.APPRAISAL_YR
           ,CASE MAY.ECONOMIC_UNIT_ID WHEN 0 THEN 2 ELSE 1 END
           ,CASE MAY.ECONOMIC_UNIT_ID WHEN 0 THEN CAST(MAY.ACCOUNT_NUM AS INT) ELSE ECONOMIC_UNIT_ID END
           ,MAY.RRC_NUM
           ,MAY.FIELD_NUM
           ,MAY.WELL_DEPTH
           ,MAY.GRAVITY_CD
           ,MAY.WELL_NUM
           ,MAY.WELL_TYP_CD
           ,MAY.PRODUCTION_ZONE
           ,MAY.PROD_EQUIP_COUNT
           ,MAY.PROD_EQUIP_SCHED
           ,MAY.PROD_EQUIP_VAL
           ,MAY.SERV_EQUIP_COUNT
           ,MAY.SERV_EQUIP_SCHED
           ,MAY.SERV_EQUIP_VAL
           ,MAY.INJ_EQUIP_COUNT
           ,MAY.INJ_EQUIP_SCHED
           ,MAY.INJ_EQUIP_VAL
           ,MAY.MAX_YEAR_LIFE
           ,MAY.DISCOUNT_RATE
           ,MAY.GROSS_OIL_PRICE
           ,MAY.GROSS_PROD_PRICE
           ,MAY.GROSS_GAS_WI_PRICE
           ,MAY.GROSS_GAS_RI_PRICE
           ,MAY.OPERATING_EXP
           ,MAY.DAILY_AVG_OIL
           ,MAY.DAILY_AVG_GAS
           ,MAY.DAILY_AVG_PRODUCT
           ,MAY.YEAR_LIFE
           ,MAY.OIL_RESERVES
           ,MAY.ACCUM_OIL_PRODUCTION
           ,MAY.ACCUM_GAS_PRODUCTION
           ,MAY.ACCUM_PRODUCT_PRODUCTION
           ,MAY.TOT_VAL
           ,MAY.OIL_WI_TOT_VAL
           ,MAY.GAS_WI_TOT_VAL
           ,MAY.PRODUCT_WI_TOT_VAL
           ,MAY.WI_TOT_VAL
           ,MAY.PV_WI_TOT_VAL
           ,MAY.OIL_RI_TOT_VAL
           ,MAY.GAS_RI_TOT_VAL
           ,MAY.PRODUCT_RI_TOT_VAL
           ,MAY.RI_TOT_VAL
           ,MAY.LAST_APPRAISAL_YR
           ,ISNULL(AU9.idAppUser,0) -- MAY.LST_UPDT_EMPL_ID
           ,MAY.LST_UPDT_TS
           ,CASE MAY.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[MRL_APPRL_YEAR] MAY
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = MAY.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE MAY.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND MAY.POST_FLG <> 'D'


DELETE FROM [MINERALPRO].[dbo].[ApprDeclineSchedule]


INSERT INTO [MINERALPRO].[dbo].[ApprDeclineSchedule]
           (appraisalYear
           ,subjectTypeCd -- 3540 (NEW) (1-UNIT, 2-LEASE)
           ,subjectId 
           ,rrcNumber 
           ,declineTypeCd -- 3520
           ,seqNumber
           ,declineYears
           ,declinePercent
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            MDP.APPRAISAL_YR
           ,CASE MDP.ECONOMIC_UNIT_ID WHEN 0 THEN 2 ELSE 1 END
           ,CASE MDP.ECONOMIC_UNIT_ID WHEN 0 THEN CAST(MDP.ACCOUNT_NUM AS INT) ELSE ECONOMIC_UNIT_ID END
           ,MDP.RRC_NUM
           ,MDP.DECLINE_TYP_CD
           ,MDP.SEQ_NUM
           ,MDP.DECLINE_YRS
           ,MDP.DECLINE_PCT
           ,ISNULL(AU9.idAppUser,0) -- MDP.LST_UPDT_EMPL_ID
           ,MDP.LST_UPDT_TS
           ,CASE MDP.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[MRL_DECLINE_PCT] MDP
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = MDP.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE MDP.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND MDP.POST_FLG <> 'D'


DELETE FROM [MINERALPRO].[dbo].[ApprAmortSchedule]


INSERT INTO [MINERALPRO].[dbo].[ApprAmortSchedule]
           (appraisalYear
           ,subjectTypeCd -- 3540 (NEW) (1-UNIT, 2-LEASE)
           ,subjectId 
           ,rrcNumber 
           ,yearNumber
           ,estAnnualOilProduction 
           ,estAnnualGasProduction 
           ,estAnnualProductProduction 
           ,netOilPrice 
           ,netProductPrice 
           ,netWorkingInterestGasPrice 
           ,netRoyaltyInterestGasPrice 
           ,grossWorkingInterestOilRevenue 
           ,grossWorkingInterestGasRevenue 
           ,grossWorkingInterestProductRevenue 
           ,grossWorkingInterestTotalRevenue 
           ,grossRoyaltyInterestOilRevenue 
           ,grossRoyaltyInterestGasRevenue 
           ,grossRoyaltyInterestProductRevenue 
           ,workingInterestOilRevenuePercent 
           ,workingInterestGasRevenuePercent 
           ,workingInterestProductRevenuePercent 
           ,operatingExpense 
           ,netWorkingInterestOilRevenue 
           ,netWorkingInterestGasRevenue 
           ,netWorkingInterestProductRevenue 
           ,netWorkingInterestTotalRevenue 
           ,netRoyaltyInterestOilRevenue 
           ,netRoyaltyInterestGasRevenue 
           ,netRoyaltyInterestProductRevenue 
           ,workingInterestTotalPV 
           ,workingInterestTotalNetValue 
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            MAS.APPRAISAL_YR
           ,CASE MAS.ECONOMIC_UNIT_ID WHEN 0 THEN 2 ELSE 1 END
           ,CASE MAS.ECONOMIC_UNIT_ID WHEN 0 THEN CAST(MAS.ACCOUNT_NUM AS INT) ELSE ECONOMIC_UNIT_ID END
           ,MAS.RRC_NUM
           ,MAS.AMORT_YR_NUM
           ,MAS.EST_ANN_OIL_PROD
           ,MAS.EST_ANN_GAS_PROD
           ,MAS.EST_ANN_PRODUCT_PROD
           ,MAS.NET_OIL_PRICE
           ,MAS.NET_PRODUCT_PRICE
           ,MAS.NET_GAS_WI_PRICE
           ,MAS.NET_GAS_RI_PRICE
           ,MAS.GROSS_WI_OIL_REV
           ,MAS.GROSS_WI_GAS_REV
           ,MAS.GROSS_WI_PRODUCT_REV
           ,MAS.GROSS_WI_TOT_REV
           ,MAS.GROSS_RI_OIL_REV
           ,MAS.GROSS_RI_GAS_REV
           ,MAS.GROSS_RI_PRODUCT_REV
           ,MAS.OIL_WI_REV_PCT
           ,MAS.GAS_WI_REV_PCT
           ,MAS.PRODUCT_WI_REV_PCT
           ,MAS.OPERATING_EXP
           ,MAS.NET_WI_OIL_REV
           ,MAS.NET_WI_GAS_REV
           ,MAS.NET_WI_PRODUCT_REV
           ,MAS.NET_TOT_WI_REV
           ,MAS.NET_RI_OIL_REV
           ,MAS.NET_RI_GAS_REV
           ,MAS.NET_RI_PRODUCT_REV
           ,MAS.TOT_PV_WI_VAL
           ,MAS.TOT_NET_WI_VAL
           ,ISNULL(AU9.idAppUser,0) -- MAS.LST_UPDT_EMPL_ID
           ,MAS.LST_UPDT_TS
           ,CASE MAS.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[MRL_AMORT_SCHED] MAS
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = MAS.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
 WHERE MAS.APPRAISAL_YR BETWEEN @MIN_APPRAISAL_YR AND @MAX_APPRAISAL_YR
--   AND MAS.POST_FLG <> 'D'