USE [MINERALPRO]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

IF OBJECT_ID('[dbo].[AppraisalYear]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[AppraisalYear]; 
GO

CREATE TABLE [dbo].[AppraisalYear](
	[idAppraisalYear] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_AppraisalYear_appraisalYear] DEFAULT (0),
	[currentInd] [char](1) NOT NULL CONSTRAINT [DF_AppraisalYear_currentInd] DEFAULT ('N'),
	[futureInd] [char](1) NOT NULL CONSTRAINT [DF_AppraisalYear_futureInd] DEFAULT ('N'),
	[collectionInd] [char](1) NOT NULL CONSTRAINT [DF_AppraisalYear_collectionInd] DEFAULT ('N'),
	[effectiveDt] [datetime] NOT NULL CONSTRAINT [DF_AppraisalYear_effectiveDt] DEFAULT (getdate()),
	[expirationDt] [datetime] NOT NULL CONSTRAINT [DF_AppraisalYear_expirationDt] DEFAULT (getdate()),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_AppraisalYear_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_AppraisalYear_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_AppraisalYear_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_AppraisalYear] PRIMARY KEY CLUSTERED 
(
	[idAppraisalYear] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_AppraisalYear] ON [dbo].[AppraisalYear]
(
	[appraisalYear] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[NoteType]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[NoteType]; 
GO

CREATE TABLE [dbo].[NoteType](
	[idNoteType] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_NoteType_appraisalYear] DEFAULT (0),
	[groupCd] [smallint] NOT NULL CONSTRAINT [DF_NoteType_groupCd] DEFAULT (0),
	[noteTypeCd] [smallint] NOT NULL CONSTRAINT [DF_NoteType_noteTypeCd] DEFAULT (0),
	[fieldNoteInd] [char](1) NOT NULL CONSTRAINT [DF_NoteType_fieldNoteInd] DEFAULT ('N'),
	[description] [varchar](50) NOT NULL CONSTRAINT [DF_NoteType_description] DEFAULT (''),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_NoteType_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_NoteType_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_NoteType_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_NoteType] PRIMARY KEY CLUSTERED 
(
	[idNoteType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_NoteType] ON [dbo].[NoteType]
(
	[appraisalYear] DESC,
	[groupCd] ASC,
	[noteTypeCd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[QuickNote]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[QuickNote]; 
GO

CREATE TABLE [dbo].[QuickNote](
	[idQuickNote] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_QuickNote_appraisalYear] DEFAULT (0),
	[groupCd] [smallint] NOT NULL CONSTRAINT [DF_QuickNote_groupCd] DEFAULT (0),
	[noteTypeCd] [smallint] NOT NULL CONSTRAINT [DF_QuickNote_noteTypeCd] DEFAULT (0),
	[quickNoteCd] [smallint] NOT NULL CONSTRAINT [DF_QuickNote_quickNoteCd] DEFAULT (0),
	[description] [varchar](50) NOT NULL CONSTRAINT [DF_QuickNote_description] DEFAULT (''),
	[note] [varchar](3900) NOT NULL CONSTRAINT [DF_QuickNote_note] DEFAULT (''),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_QuickNote_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_QuickNote_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_QuickNote_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_QuickNote] PRIMARY KEY CLUSTERED 
(
	[idQuickNote] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_QuickNote] ON [dbo].[QuickNote]
(
	[appraisalYear] DESC,
	[groupCd] ASC,
	[noteTypeCd] ASC,
	[quickNoteCd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[Report]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[Report]; 
GO

CREATE TABLE [dbo].[Report](
	[idReport] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_Report_appraisalYear] DEFAULT (0),
	[reportId] [smallint] NOT NULL CONSTRAINT [DF_Report_reportId] DEFAULT (0),
	[reportTypeCdx] [varchar](15) NOT NULL CONSTRAINT [DF_Report_reportTypeCdx] DEFAULT (''),
	[reportCategoryCdx] [varchar](50) NOT NULL CONSTRAINT [DF_Report_reportCategoryCdx] DEFAULT (''),
	[description] [varchar](50) NOT NULL CONSTRAINT [DF_Report_description] DEFAULT (''),
	[fileName] [varchar](100) NOT NULL CONSTRAINT [DF_Report_fileName] DEFAULT (''),
	[displaySeqNumber] [smallint] NOT NULL CONSTRAINT [DF_Report_displaySeqNumber] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_Report_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_Report_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_Report_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_Report] PRIMARY KEY CLUSTERED 
(
	[idReport] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_Report] ON [dbo].[Report]
(
	[appraisalYear] DESC,
	[reportId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[ReportScript]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[ReportScript]; 
GO

CREATE TABLE [dbo].[ReportScript](
	[idReportScript] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_ReportScript_appraisalYear] DEFAULT (0),
	[reportId] [smallint] NOT NULL CONSTRAINT [DF_ReportScript_reportId] DEFAULT (0),
	[seqNumber] [smallint] NOT NULL CONSTRAINT [DF_ReportScript_seqNumber] DEFAULT (0),
	[description] [varchar](50) NOT NULL CONSTRAINT [DF_ReportScript_description] DEFAULT (''),
	[scriptName] [varchar](100) NOT NULL CONSTRAINT [DF_ReportScript_scriptName] DEFAULT (''),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_ReportScript_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_ReportScript_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_ReportScript_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_ReportScript] PRIMARY KEY CLUSTERED 
(
	[idReportScript] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_ReportScript] ON [dbo].[ReportScript]
(
	[appraisalYear] DESC,
	[reportId] ASC,
	[seqNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[Exemption]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[Exemption]; 
GO

CREATE TABLE [dbo].[Exemption](
	[idExemption] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_Exemption_appraisalYear] DEFAULT (0),
	[exemptionTypeCd] [smallint] NOT NULL CONSTRAINT [DF_Exemption_exemptionTypeCd] DEFAULT (0),
	[exemptionCd] [smallint] NOT NULL CONSTRAINT [DF_Exemption_exemptionCd] DEFAULT (0),
	[standardInd] [char](1) NOT NULL CONSTRAINT [DF_Exemption_standardInd] DEFAULT ('N'),
	[totalInd] [char](1) NOT NULL CONSTRAINT [DF_Exemption_totalInd] DEFAULT ('N'),
	[exemptionValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Exemption_exemptionValue] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_Exemption_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_Exemption_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_Exemption_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_Exemption] PRIMARY KEY CLUSTERED 
(
	[idExemption] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_Exemption] ON [dbo].[Exemption]
(
	[appraisalYear] DESC,
	[exemptionTypeCd] ASC,
	[exemptionCd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[TaxUnit]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[TaxUnit]; 
GO

CREATE TABLE [dbo].[TaxUnit](
	[idTaxUnit] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_TaxUnit_appraisalYear] DEFAULT (0),
	[taxUnitTypeCd] [smallint] NOT NULL CONSTRAINT [DF_TaxUnit_taxUnitTypeCd] DEFAULT (0),
	[taxUnitCd] [smallint] NOT NULL CONSTRAINT [DF_TaxUnit_taxUnitCd] DEFAULT (0),
	[taxUnitAbbr] [varchar](15) NOT NULL CONSTRAINT [DF_TaxUnit_taxUnitAbbr] DEFAULT (''),
	[taxUnitName] [varchar](50) NOT NULL CONSTRAINT [DF_TaxUnit_taxUnitName] DEFAULT (''),
	[taxUnitCdx] [varchar](15) NOT NULL CONSTRAINT [DF_TaxUnit_taxUnitCdx] DEFAULT (''),
	[estimatedTaxRate] [decimal](9, 8) NOT NULL CONSTRAINT [DF_TaxUnit_estimatedTaxRate] DEFAULT (0.0),
	[taxingInd] [char](1) NOT NULL CONSTRAINT [DF_TaxUnit_taxingInd] DEFAULT ('Y'),
	[defaultInd] [char](1) NOT NULL CONSTRAINT [DF_TaxUnit_defaultInd] DEFAULT ('N'),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_TaxUnit_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_TaxUnit_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_TaxUnit_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_TaxUnit] PRIMARY KEY CLUSTERED 
(
	[idTaxUnit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_TaxUnit] ON [dbo].[TaxUnit]
(
	[appraisalYear] DESC,
	[TaxUnitTypeCd] ASC,
	[TaxUnitCd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_TaxUnit_taxUnitAbbr] ON [dbo].[TaxUnit]
(
	[appraisalYear] DESC,
	[taxUnitAbbr] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_TaxUnit_taxUnitName] ON [dbo].[TaxUnit]
(
	[appraisalYear] DESC,
	[taxUnitName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_TaxUnit_taxUnitCdx] ON [dbo].[TaxUnit]
(
	[appraisalYear] DESC,
	[taxUnitCdx] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[TaxUnitExemption]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[TaxUnitExemption]; 
GO

CREATE TABLE [dbo].[TaxUnitExemption](
	[idTaxUnitExemption] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_TaxUnitExemption_appraisalYear] DEFAULT (0),
	[taxUnitTypeCd] [smallint] NOT NULL CONSTRAINT [DF_TaxUnitExemption_taxUnitTypeCd] DEFAULT (0),
	[taxUnitCd] [smallint] NOT NULL CONSTRAINT [DF_TaxUnitExemption_taxUnitCd] DEFAULT (0),
	[exemptionTypeCd] [smallint] NOT NULL CONSTRAINT [DF_TaxUnitExemption_exemptionTypeCd] DEFAULT (0),
	[exemptionCd] [smallint] NOT NULL CONSTRAINT [DF_TaxUnitExemption_exemptionCd] DEFAULT (0),
	[ownerExemptInd] [char](1) NOT NULL CONSTRAINT [DF_TaxUnitExemption_standardInd] DEFAULT ('N'),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_TaxUnitExemption_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_TaxUnitExemption_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_TaxUnitExemption_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_TaxUnitExemption] PRIMARY KEY CLUSTERED 
(
	[idTaxUnitExemption] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_TaxUnitExemption] ON [dbo].[TaxUnitExemption]
(
	[appraisalYear] DESC,
	[TaxUnitTypeCd] ASC,
	[TaxUnitCd] ASC,
	[exemptionTypeCd] ASC,
	[exemptionCd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[Operator]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[Operator]; 
GO

CREATE TABLE [dbo].[Operator](
	[idOperator] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_Operator_appraisalYear] DEFAULT (0),
	[operatorId] [int] NOT NULL CONSTRAINT [DF_Operator_operatorId] DEFAULT (0),
	[operatorName] [varchar](50) NOT NULL CONSTRAINT [DF_Operator_operatorName] DEFAULT (''),
	[ownerId] [int] NOT NULL CONSTRAINT [DF_Operator_ownerId] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_Operator_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_Operator_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_Operator_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_Operator] PRIMARY KEY CLUSTERED 
(
	[idOperator] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_Operator] ON [dbo].[Operator]
(
	[appraisalYear] DESC,
	[operatorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[Field]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[Field]; 
GO

CREATE TABLE [dbo].[Field](
	[idField] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_Field_appraisalYear] DEFAULT (0),
	[fieldId] [int] NOT NULL CONSTRAINT [DF_Field_fieldId] DEFAULT (0),
	[fieldName] [varchar](50) NOT NULL CONSTRAINT [DF_Field_fieldName] DEFAULT (''),
	[wellDepth] [int] NOT NULL CONSTRAINT [DF_Field_wellDepth] DEFAULT (0),
	[gravityCd] [smallint] NOT NULL CONSTRAINT [DF_Field_gravityCd] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_Field_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_Field_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_Field_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_Field] PRIMARY KEY CLUSTERED 
(
	[idField] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_Field] ON [dbo].[Field]
(
	[appraisalYear] DESC,
	[fieldId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[Agency]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[Agency]; 
GO

CREATE TABLE [dbo].[Agency](
	[idAgency] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_Agency_appraisalYear] DEFAULT (0),
	[agencyCdx] [varchar](6) NOT NULL CONSTRAINT [DF_Agency_agencyCdx] DEFAULT (''),
	[agencyName] [varchar](50) NOT NULL CONSTRAINT [DF_Agency_agencyName] DEFAULT (''),
	[addrLine1] [varchar](50) NOT NULL CONSTRAINT [DF_Agency_addrLine1] DEFAULT (''),
	[addrLine2] [varchar](50) NOT NULL CONSTRAINT [DF_Agency_addrLine2] DEFAULT (''),
	[addrLine3] [varchar](50) NOT NULL CONSTRAINT [DF_Agency_addrLine3] DEFAULT (''),
	[city] [varchar](35) NOT NULL CONSTRAINT [DF_Agency_city] DEFAULT (''),
	[stateCd] [smallint] NOT NULL CONSTRAINT [DF_Agency_stateCd] DEFAULT (0),
	[zipcode] [varchar](9) NOT NULL CONSTRAINT [DF_Agency_zipcode] DEFAULT (''),
	[countryCd] [smallint] NOT NULL CONSTRAINT [DF_Agency_countryCd] DEFAULT (0),
	[phoneNum] [varchar](10) NOT NULL CONSTRAINT [DF_Agency_phoneNum] DEFAULT (''),
	[faxNum] [varchar](10) NOT NULL CONSTRAINT [DF_Agency_faxNum] DEFAULT (''),
	[emailAddress] [varchar](50) NOT NULL CONSTRAINT [DF_Agency_emailAddress] DEFAULT (''),
	[statusCd] [smallint] NOT NULL CONSTRAINT [DF_Agency_statusCd] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_Agency_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_Agency_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_Agency_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_Agency] PRIMARY KEY CLUSTERED 
(
	[idAgency] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_Agency] ON [dbo].[Agency]
(
	[appraisalYear] DESC,
	[agencyCdx] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[ArbParticipant]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[ArbParticipant]; 
GO

CREATE TABLE [dbo].[ArbParticipant](
	[idArbParticipant] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_ArbParticipant_appraisalYear] DEFAULT (0),
	[groupCdx] [varchar](50) NOT NULL CONSTRAINT [DF_ArbParticipant_groupCdx] DEFAULT (''),
	[fullName] [varchar](50) NOT NULL CONSTRAINT [DF_ArbParticipant_fullName] DEFAULT (''),
	[seqNumber] [smallint] NOT NULL CONSTRAINT [DF_ArbParticipant_seqNumber] DEFAULT (0),
	[arbParticipantId] [int] NOT NULL CONSTRAINT [DF_ArbParticipant_arbParticipantId] DEFAULT (0),
	[chairmanInd] [char](1) NOT NULL CONSTRAINT [DF_ArbParticipant_chairmanInd] DEFAULT ('N'),
	[lastName] [varchar](32) NOT NULL CONSTRAINT [DF_ArbParticipant_lastName] DEFAULT (''),
	[firstName] [varchar](32) NOT NULL CONSTRAINT [DF_ArbParticipant_firstName] DEFAULT (''),
	[title] [varchar](50) NOT NULL CONSTRAINT [DF_ArbParticipant_title] DEFAULT (''),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_ArbParticipant_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_ArbParticipant_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_ArbParticipant_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_ArbParticipant] PRIMARY KEY CLUSTERED 
(
	[idArbParticipant] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_ArbParticipant] ON [dbo].[ArbParticipant]
(
	[appraisalYear] DESC,
	[groupCdx] ASC,
	[fullName] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[Owner]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[Owner]; 
GO

CREATE TABLE [dbo].[Owner](
	[idOwner] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_Owner_appraisalYear] DEFAULT (0),
	[ownerId] [int] NOT NULL CONSTRAINT [DF_Owner_ownerId] DEFAULT (0),
	[name1] [varchar](50) NOT NULL CONSTRAINT [DF_Owner_name1] DEFAULT (''),
	[name2] [varchar](50) NOT NULL CONSTRAINT [DF_Owner_name2] DEFAULT (''),
	[name3] [varchar](50) NOT NULL CONSTRAINT [DF_Owner_name3] DEFAULT (''),
	[addrLine1] [varchar](50) NOT NULL CONSTRAINT [DF_Owner_addrLine1] DEFAULT (''),
	[addrLine2] [varchar](50) NOT NULL CONSTRAINT [DF_Owner_addrLine2] DEFAULT (''),
	[addrLine3] [varchar](50) NOT NULL CONSTRAINT [DF_Owner_addrLine3] DEFAULT (''),
	[city] [varchar](35) NOT NULL CONSTRAINT [DF_Owner_city] DEFAULT (''),
	[stateCd] [smallint] NOT NULL CONSTRAINT [DF_Owner_stateCd] DEFAULT (0),
	[zipcode] [varchar](9) NOT NULL CONSTRAINT [DF_Owner_zipcode] DEFAULT (''),
	[zipsort] AS (substring([ZIPCODE],1,5)),
	[countryCd] [smallint] NOT NULL CONSTRAINT [DF_Owner_countryCd] DEFAULT (0),
	[phoneNum] [varchar](10) NOT NULL CONSTRAINT [DF_Owner_phoneNum] DEFAULT (''),
	[faxNum] [varchar](10) NOT NULL CONSTRAINT [DF_Owner_faxNum] DEFAULT (''),
	[emailAddress] [varchar](50) NOT NULL CONSTRAINT [DF_Owner_emailAddress] DEFAULT (''),
	[birthDt] [datetime] NOT NULL CONSTRAINT [DF_Owner_birthDt] DEFAULT ('1900-01-01'),
	[confidentialInd] [char](1) NOT NULL CONSTRAINT [DF_Owner_confidentialInd] DEFAULT ('N'),
	[ownerChangeCd] [smallint] NOT NULL CONSTRAINT [DF_Owner_ownerChangeCd] DEFAULT (0),
	[agencyCdx] [varchar](6) NOT NULL CONSTRAINT [DF_Owner_agencyCdx] DEFAULT (''),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_Owner_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_Owner_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_Owner_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_Owner] PRIMARY KEY CLUSTERED 
(
	[idOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_Owner] ON [dbo].[Owner]
(
	[appraisalYear] DESC,
	[ownerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[Note]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[Note]; 
GO

CREATE TABLE [dbo].[Note](
	[idNote] [bigint] NOT NULL IDENTITY(1,1),
	[subjectTypeCd] [smallint] NOT NULL CONSTRAINT [DF_Note_subjectTypeCd] DEFAULT (0),
	[subjectId] [int] NOT NULL CONSTRAINT [DF_Note_subjectId] DEFAULT (0),
	[rrcNumber] [int] NOT NULL CONSTRAINT [DF_Note_rrcNumber] DEFAULT (0),
	[seqNumber] [smallint] NOT NULL CONSTRAINT [DF_Note_seqNumber] DEFAULT (0),
	[createDt] [datetime] NOT NULL CONSTRAINT [DF_Note_createDt] DEFAULT (getdate()),
	[noteTypeCd] [smallint] NOT NULL CONSTRAINT [DF_Note_noteTypeCd] DEFAULT (0),
	[quickNoteCd] [smallint] NOT NULL CONSTRAINT [DF_Note_quickNoteCd] DEFAULT (0),
	[securityCd] [smallint] NOT NULL CONSTRAINT [DF_Note_securityCd] DEFAULT (0),
	[groupCd] [smallint] NOT NULL CONSTRAINT [DF_Note_groupCd] DEFAULT (0),
	[referenceAppraisalYear] [smallint] NOT NULL CONSTRAINT [DF_Note_referenceAppraisalYear] DEFAULT (0),
	[note] [varchar](3900) NOT NULL CONSTRAINT [DF_Note_note] DEFAULT (''),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_Note_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_Note_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_Note_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_Note] PRIMARY KEY CLUSTERED 
(
	[idNote] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_Note] ON [dbo].[Note]
(
	[subjectTypeCd] ASC,
	[subjectId] ASC,
	[rrcNumber] ASC,
	[seqNumber] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[OwnerExemption]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[OwnerExemption]; 
GO

CREATE TABLE [dbo].[OwnerExemption](
	[idOwnerExemption] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_OwnerExemption_appraisalYear] DEFAULT (0),
	[ownerId] [int] NOT NULL CONSTRAINT [DF_OwnerExemption_ownerId] DEFAULT (0),
	[exemptionTypeCd] [smallint] NOT NULL CONSTRAINT [DF_OwnerExemption_exemptionTypeCd] DEFAULT (0),
	[exemptionCd] [smallint] NOT NULL CONSTRAINT [DF_OwnerExemption_exemptionCd] DEFAULT (0),
	[effectiveDt] [datetime] NOT NULL CONSTRAINT [DF_OwnerExemption_effectiveDt] DEFAULT (getdate()),
	[expirationDt] [datetime] NOT NULL CONSTRAINT [DF_OwnerExemption_expirationDt] DEFAULT (getdate()),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_OwnerExemption_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_OwnerExemption_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_OwnerExemption_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_OwnerExemption] PRIMARY KEY CLUSTERED 
(
	[idOwnerExemption] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_OwnerExemption] ON [dbo].[OwnerExemption]
(
	[appraisalYear] DESC,
	[ownerId] ASC,
	[exemptionTypeCd] ASC,
	[exemptionCd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[Lease]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[Lease]; 
GO

CREATE TABLE [dbo].[Lease](
	[idLease] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_Lease_appraisalYear] DEFAULT (0),
	[leaseId] [int] NOT NULL CONSTRAINT [DF_Lease_leaseId] DEFAULT (0),
	[leaseName] [varchar](50) NOT NULL CONSTRAINT [DF_Lease_leaseName] DEFAULT (''),
	[divisionOrderDt] [datetime] NOT NULL CONSTRAINT [DF_Lease_divisionOrderDt] DEFAULT ('1900-01-01'),
	[divisionOrderDescription] [varchar](50) NOT NULL CONSTRAINT [DF_Lease_divisionOrderDescription] DEFAULT (''),
	[description] [varchar](60) NOT NULL CONSTRAINT [DF_Lease_description] DEFAULT (''),
	[comment] [varchar](50) NOT NULL CONSTRAINT [DF_Lease_comment] DEFAULT (''),
	[cadCategoryCd] [smallint] NOT NULL CONSTRAINT [DF_Lease_cadCategoryCd] DEFAULT (0),
	[changeReasonCd] [smallint] NOT NULL CONSTRAINT [DF_Lease_changeReasonCd] DEFAULT (0),
	[operatorId] [int] NOT NULL CONSTRAINT [DF_Lease_operatorId] DEFAULT (0),
	[acres] [decimal](10, 3) NOT NULL CONSTRAINT [DF_Lease_acres] DEFAULT (0.0),
	[leaseYear] [smallint] NOT NULL CONSTRAINT [DF_Lease_leaseYear] DEFAULT (0),
	[unitId] [int] NOT NULL CONSTRAINT [DF_Lease_unitId] DEFAULT (0),
	[lastUpdateUserid] [int] NOT NULL CONSTRAINT [DF_Lease_lastUpdateUserid] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_Lease_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_Lease_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_Lease_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_Lease] PRIMARY KEY CLUSTERED 
(
	[idLease] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_Lease] ON [dbo].[Lease]
(
	[appraisalYear] DESC,
	[leaseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[LeaseTaxUnit]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[LeaseTaxUnit]; 
GO

CREATE TABLE [dbo].[LeaseTaxUnit](
	[idLeaseTaxUnit] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_LeaseTaxUnit_appraisalYear] DEFAULT (0),
	[leaseId] [int] NOT NULL CONSTRAINT [DF_LeaseTaxUnit_leaseId] DEFAULT (0),
	[taxUnitTypeCd] [smallint] NOT NULL CONSTRAINT [DF_LeaseTaxUnit_taxUnitTypeCd] DEFAULT (0),
	[taxUnitCd] [smallint] NOT NULL CONSTRAINT [DF_LeaseTaxUnit_taxUnitCd] DEFAULT (0),
	[taxUnitPercent] [decimal](4, 3) NOT NULL CONSTRAINT [DF_LeaseTaxUnit_taxUnitPercent] DEFAULT (0.0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_LeaseTaxUnit_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_LeaseTaxUnit_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_LeaseTaxUnit_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_LeaseTaxUnit] PRIMARY KEY CLUSTERED 
(
	[idLeaseTaxUnit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_LeaseTaxUnit] ON [dbo].[LeaseTaxUnit]
(
	[appraisalYear] DESC,
	[leaseId] ASC,
	[TaxUnitTypeCd] ASC,
	[TaxUnitCd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[LeaseOwner]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[LeaseOwner]; 
GO

CREATE TABLE [dbo].[LeaseOwner](
	[idLeaseOwner] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_LeaseOwner_appraisalYear] DEFAULT (0),
	[leaseId] [int] NOT NULL CONSTRAINT [DF_LeaseOwner_leaseId] DEFAULT (0),
	[ownerId] [int] NOT NULL CONSTRAINT [DF_LeaseOwner_ownerId] DEFAULT (0),
	[interestTypeCd] [smallint] NOT NULL CONSTRAINT [DF_LeaseOwner_interestTypeCd] DEFAULT (0),
	[interestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_LeaseOwner_interestPercent] DEFAULT (0.0),
	[ownerValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_LeaseOwner_ownerValue] DEFAULT (0),
	[lockCd] [smallint] NOT NULL CONSTRAINT [DF_LeaseOwner_lockCd] DEFAULT (0),
	[lastCalcDt] [datetime] NOT NULL CONSTRAINT [DF_LeaseOwner_lastCalcDt] DEFAULT ('1900-01-01'),
	[lastCalcUserid] [int] NOT NULL CONSTRAINT [DF_LeaseOwner_lastCalcUserid] DEFAULT (0),
	[lastCalcSourceId] [bigint] NOT NULL CONSTRAINT [DF_LeaseOwner_lastCalcSourceId] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_LeaseOwner_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_LeaseOwner_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_LeaseOwner_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_LeaseOwner] PRIMARY KEY CLUSTERED 
(
	[idLeaseOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_LeaseOwner] ON [dbo].[LeaseOwner]
(
	[appraisalYear] DESC,
	[leaseId] ASC,
	[ownerId] ASC,
	[interestTypeCd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[LeaseValueSummary]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[LeaseValueSummary]; 
GO

CREATE TABLE [dbo].[LeaseValueSummary](
	[idLeaseValueSummary] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_LeaseValueSummary_appraisalYear] DEFAULT (0),
	[leaseId] [int] NOT NULL CONSTRAINT [DF_LeaseValueSummary_leaseId] DEFAULT (0),
	[workingInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_LeaseValueSummary_workingInterestPercent] DEFAULT (0.0),
	[royaltyInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_LeaseValueSummary_royaltyInterestPercent] DEFAULT (0.0),
	[overridingRoyaltyPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_LeaseValueSummary_overridingRoyaltyPercent] DEFAULT (0.0),
	[oilPaymentPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_LeaseValueSummary_oilPaymentPercent] DEFAULT (0.0),
	[workingInterestValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_LeaseValueSummary_workingInterestValue] DEFAULT (0),
	[royaltyInterestValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_LeaseValueSummary_royaltyInterestValue] DEFAULT (0),
	[lastAppraisalYear] [smallint] NOT NULL CONSTRAINT [DF_LeaseValueSummary_lastAppraisalYear] DEFAULT (0),
	[appraisedValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_LeaseValueSummary_appraisedValue] DEFAULT (0),
	[appraisalDt] [datetime] NOT NULL CONSTRAINT [DF_LeaseValueSummary_appraisalDt] DEFAULT ('1900-01-01'),
	[tractNum] [varchar](10) NOT NULL CONSTRAINT [DF_LeaseValueSummary_tractNum] DEFAULT (''),
	[unitWorkingInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_LeaseValueSummary_unitWorkingInterestPercent] DEFAULT (0.0),
	[unitRoyaltyInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_LeaseValueSummary_unitRoyaltyInterestPercent] DEFAULT (0.0),
	[lastCalcDt] [datetime] NOT NULL CONSTRAINT [DF_LeaseValueSummary_lastCalcDt] DEFAULT ('1900-01-01'),
	[lastCalcUserid] [int] NOT NULL CONSTRAINT [DF_LeaseValueSummary_lastCalcUserid] DEFAULT (0),
	[lastCalcSourceId] [bigint] NOT NULL CONSTRAINT [DF_LeaseValueSummary_lastCalcSourceId] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_LeaseValueSummary_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_LeaseValueSummary_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_LeaseValueSummary_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_LeaseValueSummary] PRIMARY KEY CLUSTERED 
(
	[idLeaseValueSummary] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_LeaseValueSummary] ON [dbo].[LeaseValueSummary]
(
	[appraisalYear] DESC,
	[leaseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[CalcTaxAdj]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[CalcTaxAdj]; 
GO

CREATE TABLE [dbo].[CalcTaxAdj](
	[idCalcTaxAdj] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_CalcTaxAdj_appraisalYear] DEFAULT (0),
	[wellTypeCd] [smallint] NOT NULL CONSTRAINT [DF_CalcTaxAdj_wellTypeCd] DEFAULT (0),
	[taxTypeCd] [smallint] NOT NULL CONSTRAINT [DF_CalcTaxAdj_taxTypeCd] DEFAULT (0),
	[taxPercent] [decimal](4, 3) NOT NULL CONSTRAINT [DF_CalcTaxAdj_taxPercent] DEFAULT (0.0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_CalcTaxAdj_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_CalcTaxAdj_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_CalcTaxAdj_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_CalcTaxAdj] PRIMARY KEY CLUSTERED 
(
	[idCalcTaxAdj] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_CalcTaxAdj] ON [dbo].[CalcTaxAdj]
(
	[appraisalYear] DESC,
	[wellTypeCd] ASC,
	[taxTypeCd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[CalcEscalatedPriceAdj]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[CalcEscalatedPriceAdj]; 
GO

CREATE TABLE [dbo].[CalcEscalatedPriceAdj](
	[idCalcEscalatedPriceAdj] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_CalcEscalatedPriceAdj_appraisalYear] DEFAULT (0),
	[wellTypeCd] [smallint] NOT NULL CONSTRAINT [DF_CalcEscalatedPriceAdj_wellTypeCd] DEFAULT (0),
	[yearNumber] [smallint] NOT NULL CONSTRAINT [DF_CalcEscalatedPriceAdj_yearNumber] DEFAULT (0),
	[adjPercent] [decimal](5, 4) NOT NULL CONSTRAINT [DF_CalcEscalatedPriceAdj_adjPercent] DEFAULT (0.0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_CalcEscalatedPriceAdj_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_CalcEscalatedPriceAdj_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_CalcEscalatedPriceAdj_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_CalcEscalatedPriceAdj] PRIMARY KEY CLUSTERED 
(
	[idCalcEscalatedPriceAdj] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_CalcEscalatedPriceAdj] ON [dbo].[CalcEscalatedPriceAdj]
(
	[appraisalYear] DESC,
	[wellTypeCd] ASC,
	[yearNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[CalcEscalatedOpExpAdj]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[CalcEscalatedOpExpAdj]; 
GO

CREATE TABLE [dbo].[CalcEscalatedOpExpAdj](
	[idCalcEscalatedOpExpAdj] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_CalcEscalatedOpExpAdj_appraisalYear] DEFAULT (0),
	[yearNumber] [smallint] NOT NULL CONSTRAINT [DF_CalcEscalatedOpExpAdj_yearNumber] DEFAULT (0),
	[adjPercent] [decimal](5, 4) NOT NULL CONSTRAINT [DF_CalcEscalatedOpExpAdj_adjPercent] DEFAULT (0.0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_CalcEscalatedOpExpAdj_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_CalcEscalatedOpExpAdj_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_CalcEscalatedOpExpAdj_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_CalcEscalatedOpExpAdj] PRIMARY KEY CLUSTERED 
(
	[idCalcEscalatedOpExpAdj] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_CalcEscalatedOpExpAdj] ON [dbo].[CalcEscalatedOpExpAdj]
(
	[appraisalYear] DESC,
	[yearNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[CalcEquipmentSchedule]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[CalcEquipmentSchedule]; 
GO

CREATE TABLE [dbo].[CalcEquipmentSchedule](
	[idCalcEquipmentSchedule] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_CalcEquipmentSchedule_appraisalYear] DEFAULT (0),
	[scheduleNumber] [smallint] NOT NULL CONSTRAINT [DF_CalcEquipmentSchedule_scheduleNumber] DEFAULT (0),
	[wellTypeCd] [smallint] NOT NULL CONSTRAINT [DF_CalcEquipmentSchedule_wellTypeCd] DEFAULT (0),
	[valueTypeCd] [smallint] NOT NULL CONSTRAINT [DF_CalcEquipmentSchedule_valueTypeCd] DEFAULT (0),
	[maxDepth] [int] NOT NULL CONSTRAINT [DF_CalcEquipmentSchedule_maxDepth] DEFAULT (0),
	[salvageValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcEquipmentSchedule_salvageValue] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_CalcEquipmentSchedule_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_CalcEquipmentSchedule_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_CalcEquipmentSchedule_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_CalcEquipmentSchedule] PRIMARY KEY CLUSTERED 
(
	[idCalcEquipmentSchedule] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_CalcEquipmentSchedule] ON [dbo].[CalcEquipmentSchedule]
(
	[appraisalYear] DESC,
	[scheduleNumber] ASC,
	[wellTypeCd] ASC,
	[valueTypeCd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[CalcEquipmentPVSchedule]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[CalcEquipmentPVSchedule]; 
GO

CREATE TABLE [dbo].[CalcEquipmentPVSchedule](
	[idCalcEquipmentPVSchedule] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_CalcEquipmentPVSchedule_appraisalYear] DEFAULT (0),
	[yearNumber] [smallint] NOT NULL CONSTRAINT [DF_CalcEquipmentPVSchedule_yearNumber] DEFAULT (0),
	[factor] [decimal](7, 6) NOT NULL CONSTRAINT [DF_CalcEquipmentPVSchedule_factor] DEFAULT (0.0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_CalcEquipmentPVSchedule_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_CalcEquipmentPVSchedule_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_CalcEquipmentPVSchedule_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_CalcEquipmentPVSchedule] PRIMARY KEY CLUSTERED 
(
	[idCalcEquipmentPVSchedule] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_CalcEquipmentPVSchedule] ON [dbo].[CalcEquipmentPVSchedule]
(
	[appraisalYear] DESC,
	[yearNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[Unit]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[Unit]; 
GO

CREATE TABLE [dbo].[Unit](
	[idUnit] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_Unit_appraisalYear] DEFAULT (0),
	[unitId] [int] NOT NULL CONSTRAINT [DF_Unit_unitId] DEFAULT (0),
	[unitName] [varchar](50) NOT NULL CONSTRAINT [DF_Unit_unitName] DEFAULT (''),
	[operatorId] [int] NOT NULL CONSTRAINT [DF_Unit_operatorId] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_Unit_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_Unit_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_Unit_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_Unit] PRIMARY KEY CLUSTERED 
(
	[idUnit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_Unit] ON [dbo].[Unit]
(
	[appraisalYear] DESC,
	[unitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[UnitValueSummary]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[UnitValueSummary]; 
GO

CREATE TABLE [dbo].[UnitValueSummary](
	[idUnitValueSummary] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_UnitValueSummary_appraisalYear] DEFAULT (0),
	[unitId] [int] NOT NULL CONSTRAINT [DF_UnitValueSummary_unitId] DEFAULT (0),
	[workingInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_UnitValueSummary_workingInterestPercent] DEFAULT (0.0),
	[royaltyInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_UnitValueSummary_royaltyInterestPercent] DEFAULT (0.0),
	[weightedWorkingInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_UnitValueSummary_weightedWorkingInterestPercent] DEFAULT (0.0),
	[weightedRoyaltyInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_UnitValueSummary_weightedRoyaltyInterestPercent] DEFAULT (0.0),
	[workingInterestValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_UnitValueSummary_workingInterestValue] DEFAULT (0),
	[royaltyInterestValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_UnitValueSummary_royaltyInterestValue] DEFAULT (0),
	[lastCalcDt] [datetime] NOT NULL CONSTRAINT [DF_UnitValueSummary_lastCalcDt] DEFAULT ('1900-01-01'),
	[lastCalcUserid] [int] NOT NULL CONSTRAINT [DF_UnitValueSummary_lastCalcUserid] DEFAULT (0),
	[lastCalcSourceId] [bigint] NOT NULL CONSTRAINT [DF_UnitValueSummary_lastCalcSourceId] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_UnitValueSummary_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_UnitValueSummary_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_UnitValueSummary_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_UnitValueSummary] PRIMARY KEY CLUSTERED 
(
	[idUnitValueSummary] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_UnitValueSummary] ON [dbo].[UnitValueSummary]
(
	[appraisalYear] DESC,
	[unitId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[Appraisal]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[Appraisal]; 
GO

CREATE TABLE [dbo].[Appraisal](
	[idAppraisal] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_Appraisal_appraisalYear] DEFAULT (0),
	[subjectTypeCd] [smallint] NOT NULL CONSTRAINT [DF_Appraisal_subjectTypeCd] DEFAULT (0),
	[subjectId] [int] NOT NULL CONSTRAINT [DF_Appraisal_subjectId] DEFAULT (0),
	[rrcNumber] [int] NOT NULL CONSTRAINT [DF_Appraisal_rrcNumber] DEFAULT (0),
	[fieldId] [int] NOT NULL CONSTRAINT [DF_Appraisal_fieldId] DEFAULT (0),
	[wellDepth] [int] NOT NULL CONSTRAINT [DF_Appraisal_wellDepth] DEFAULT (0),
	[gravityCd] [smallint] NOT NULL CONSTRAINT [DF_Appraisal_gravityCd] DEFAULT (0),
	[wellDescription] [varchar](50) NOT NULL CONSTRAINT [DF_Appraisal_wellDescription] DEFAULT (''),
	[wellTypeCd] [smallint] NOT NULL CONSTRAINT [DF_Appraisal_wellTypeCd] DEFAULT (0),
	[comment] [varchar](50) NOT NULL CONSTRAINT [DF_Appraisal_comment] DEFAULT (''),
	[productionEquipmentCount] [smallint] NOT NULL CONSTRAINT [DF_Appraisal_productionEquipmentCount] DEFAULT (0),
	[productionEquipmentSchedule] [smallint] NOT NULL CONSTRAINT [DF_Appraisal_productionEquipmentSchedule] DEFAULT (0),
	[productionEquipmentValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_productionEquipmentValue] DEFAULT (0),
	[serviceEquipmentCount] [smallint] NOT NULL CONSTRAINT [DF_Appraisal_serviceEquipmentCount] DEFAULT (0),
	[serviceEquipmentSchedule] [smallint] NOT NULL CONSTRAINT [DF_Appraisal_serviceEquipmentSchedule] DEFAULT (0),
	[serviceEquipmentValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_serviceEquipmentValue] DEFAULT (0),
	[injectionEquipmentCount] [smallint] NOT NULL CONSTRAINT [DF_Appraisal_injectionEquipmentCount] DEFAULT (0),
	[injectionEquipmentSchedule] [smallint] NOT NULL CONSTRAINT [DF_Appraisal_injectionEquipmentSchedule] DEFAULT (0),
	[injectionEquipmentValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_injectionEquipmentValue] DEFAULT (0),
	[maxYearLife] [smallint] NOT NULL CONSTRAINT [DF_Appraisal_maxYearLife] DEFAULT (0),
	[discountRate] [decimal](4, 3) NOT NULL CONSTRAINT [DF_Appraisal_discountRate] DEFAULT (0.0),
	[grossOilPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_Appraisal_grossOilPrice] DEFAULT (0.0),
	[grossProductPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_Appraisal_grossProductPrice] DEFAULT (0.0),
	[grossWorkingInterestGasPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_Appraisal_grossWorkingInterestGasPrice] DEFAULT (0.0),
	[grossRoyaltyInterestGasPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_Appraisal_grossRoyaltyInterestGasPrice] DEFAULT (0.0),
	[operatingExpense] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_operatingExpense] DEFAULT (0),
	[dailyAverageOil] [decimal](7, 2) NOT NULL CONSTRAINT [DF_Appraisal_dailyAverageOil] DEFAULT (0.0),
	[dailyAverageGas] [decimal](7, 2) NOT NULL CONSTRAINT [DF_Appraisal_dailyAverageGas] DEFAULT (0.0),
	[dailyAverageProduct] [decimal](7, 2) NOT NULL CONSTRAINT [DF_Appraisal_dailyAverageProduct] DEFAULT (0.0),
	[yearLife] [smallint] NOT NULL CONSTRAINT [DF_Appraisal_yearLife] DEFAULT (0),
	[reserveOilValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_reserveOilValue] DEFAULT (0),
	[accumOilProduction] [bigint] NOT NULL CONSTRAINT [DF_Appraisal_accumOilProduction] DEFAULT (0),
	[accumGasProduction] [bigint] NOT NULL CONSTRAINT [DF_Appraisal_accumGasProduction] DEFAULT (0),
	[accumProductProduction] [bigint] NOT NULL CONSTRAINT [DF_Appraisal_accumProductProduction] DEFAULT (0),
	[totalValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_totalValue] DEFAULT (0),
	[workingInterestOilValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_workingInterestOilValue] DEFAULT (0),
	[workingInterestGasValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_workingInterestGasValue] DEFAULT (0),
	[workingInterestProductValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_workingInterestProductValue] DEFAULT (0),
	[workingInterestTotalValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_workingInterestTotalValue] DEFAULT (0),
	[workingInterestTotalPV] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_workingInterestTotalPV] DEFAULT (0),
	[royaltyInterestOilValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_royaltyInterestOilValue] DEFAULT (0),
	[royaltyInterestGasValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_royaltyInterestGasValue] DEFAULT (0),
	[royaltyInterestProductValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_royaltyInterestProductValue] DEFAULT (0),
	[royaltyInterestTotalValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_royaltyInterestTotalValue] DEFAULT (0),
	[lastAppraisalYear] [smallint] NOT NULL CONSTRAINT [DF_Appraisal_lastAppraisalYear] DEFAULT (0),
	[lastCalcDt] [datetime] NOT NULL CONSTRAINT [DF_Appraisal_lastCalcDt] DEFAULT ('1900-01-01'),
	[lastCalcUserid] [int] NOT NULL CONSTRAINT [DF_Appraisal_lastCalcUserid] DEFAULT (0),
	[lastCalcSourceId] [bigint] NOT NULL CONSTRAINT [DF_Appraisal_lastCalcSourceId] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_Appraisal_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_Appraisal_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_Appraisal_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_Appraisal] PRIMARY KEY CLUSTERED 
(
	[idAppraisal] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_Appraisal] ON [dbo].[Appraisal]
(
	[appraisalYear] DESC,
	[subjectTypeCd] ASC,
	[subjectId] ASC,
	[rrcNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[ApprDeclineSchedule]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[ApprDeclineSchedule]; 
GO

CREATE TABLE [dbo].[ApprDeclineSchedule](
	[idApprDeclineSchedule] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_ApprDeclineSchedule_appraisalYear] DEFAULT (0),
	[subjectTypeCd] [smallint] NOT NULL CONSTRAINT [DF_ApprDeclineSchedule_subjectTypeCd] DEFAULT (0),
	[subjectId] [int] NOT NULL CONSTRAINT [DF_ApprDeclineSchedule_subjectId] DEFAULT (0),
	[rrcNumber] [int] NOT NULL CONSTRAINT [DF_ApprDeclineSchedule_rrcNumber] DEFAULT (0),
	[declineTypeCd] [smallint] NOT NULL CONSTRAINT [DF_ApprDeclineSchedule_declineTypeCd] DEFAULT (0),
	[seqNumber] [smallint] NOT NULL CONSTRAINT [DF_ApprDeclineSchedule_seqNumber] DEFAULT (0),
	[declineYears] [smallint] NOT NULL CONSTRAINT [DF_ApprDeclineSchedule_declineYears] DEFAULT (0),
	[declinePercent] [decimal](4, 3) NOT NULL CONSTRAINT [DF_ApprDeclineSchedule_declinePercent] DEFAULT (0.0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_ApprDeclineSchedule_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_ApprDeclineSchedule_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_ApprDeclineSchedule_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_ApprDeclineSchedule] PRIMARY KEY CLUSTERED 
(
	[idApprDeclineSchedule] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_ApprDeclineSchedule] ON [dbo].[ApprDeclineSchedule]
(
	[appraisalYear] DESC,
	[subjectTypeCd] ASC,
	[subjectId] ASC,
	[rrcNumber] ASC,
	[declineTypeCd] ASC,
	[seqNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[ApprAmortSchedule]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[ApprAmortSchedule]; 
GO

CREATE TABLE [dbo].[ApprAmortSchedule](
	[idApprAmortSchedule] [bigint] NOT NULL IDENTITY(1,1),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_ApprAmortSchedule_appraisalYear] DEFAULT (0),
	[subjectTypeCd] [smallint] NOT NULL CONSTRAINT [DF_ApprAmortSchedule_subjectTypeCd] DEFAULT (0),
	[subjectId] [int] NOT NULL CONSTRAINT [DF_ApprAmortSchedule_subjectId] DEFAULT (0),
	[rrcNumber] [int] NOT NULL CONSTRAINT [DF_ApprAmortSchedule_rrcNumber] DEFAULT (0),
	[yearNumber] [smallint] NOT NULL CONSTRAINT [DF_ApprAmortSchedule_yearNumber] DEFAULT (0),
	[estAnnualOilProduction] [bigint] NOT NULL CONSTRAINT [DF_ApprAmortSchedule_estAnnualOilProduction] DEFAULT (0),
	[estAnnualGasProduction] [bigint] NOT NULL CONSTRAINT [DF_ApprAmortSchedule_estAnnualGasProduction] DEFAULT (0),
	[estAnnualProductProduction] [bigint] NOT NULL CONSTRAINT [DF_ApprAmortSchedule_estAnnualProductProduction] DEFAULT (0),
	[netOilPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netOilPrice] DEFAULT (0.0),
	[netProductPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netProductPrice] DEFAULT (0.0),
	[netWorkingInterestGasPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netWorkingInterestGasPrice] DEFAULT (0.0),
	[netRoyaltyInterestGasPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netRoyaltyInterestGasPrice] DEFAULT (0.0),
	[grossWorkingInterestOilRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_grossWorkingInterestOilRevenue] DEFAULT (0),
	[grossWorkingInterestGasRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_grossWorkingInterestGasRevenue] DEFAULT (0),
	[grossWorkingInterestProductRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_grossWorkingInterestProductRevenue] DEFAULT (0),
	[grossWorkingInterestTotalRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_grossWorkingInterestTotalRevenue] DEFAULT (0),
	[grossRoyaltyInterestOilRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_grossRoyaltyInterestOilRevenue] DEFAULT (0),
	[grossRoyaltyInterestGasRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_grossRoyaltyInterestGasRevenue] DEFAULT (0),
	[grossRoyaltyInterestProductRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_grossRoyaltyInterestProductRevenue] DEFAULT (0),
	[workingInterestOilRevenuePercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_workingInterestOilRevenuePercent] DEFAULT (0),
	[workingInterestGasRevenuePercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_workingInterestGasRevenuePercent] DEFAULT (0),
	[workingInterestProductRevenuePercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_workingInterestProductRevenuePercent] DEFAULT (0),
	[operatingExpense] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_operatingExpense] DEFAULT (0),
	[netWorkingInterestOilRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netWorkingInterestOilRevenue] DEFAULT (0),
	[netWorkingInterestGasRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netWorkingInterestGasRevenue] DEFAULT (0),
	[netWorkingInterestProductRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netWorkingInterestProductRevenue] DEFAULT (0),
	[netWorkingInterestTotalRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netWorkingInterestTotalRevenue] DEFAULT (0),
	[netRoyaltyInterestOilRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netRoyaltyInterestOilRevenue] DEFAULT (0),
	[netRoyaltyInterestGasRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netRoyaltyInterestGasRevenue] DEFAULT (0),
	[netRoyaltyInterestProductRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netRoyaltyInterestProductRevenue] DEFAULT (0),
	[workingInterestTotalPV] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_workingInterestTotalPV] DEFAULT (0),
	[workingInterestTotalNetValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_workingInterestTotalNetValue] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_ApprAmortSchedule_rowUpdateUserid] DEFAULT (0),
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_ApprAmortSchedule_rowUpdateDt] DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_ApprAmortSchedule] PRIMARY KEY CLUSTERED 
(
	[idApprAmortSchedule] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_ApprAmortSchedule] ON [dbo].[ApprAmortSchedule]
(
	[appraisalYear] DESC,
	[subjectTypeCd] ASC,
	[subjectId] ASC,
	[rrcNumber] ASC,
	[yearNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[JournalEntry]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[JournalEntry]; 
GO

CREATE TABLE [dbo].[JournalEntry](
	[idJournalEntry] [bigint] IDENTITY(1,1) NOT NULL,
	[appUser] [bigint] NOT NULL CONSTRAINT [DF_JournalEntry_appUser]  DEFAULT (0),
	[appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_JournalEntry_appraisalYear]  DEFAULT (0),
	[batchId] [int] NOT NULL CONSTRAINT [DF_JournalEntry_batchId]  DEFAULT (0),
	[openDt] [datetime] NOT NULL CONSTRAINT [DF_JournalEntry_openDt]  DEFAULT (getdate()),
	[closeDt] [datetime] NOT NULL CONSTRAINT [DF_JournalEntry_closeDt]  DEFAULT ('1900-01-01'),
	[jeReasonCd] [smallint] NOT NULL CONSTRAINT [DF_JournalEntry_jeReasonCd]  DEFAULT (0),
	[description] [varchar](255) NOT NULL CONSTRAINT [DF_JournalEntry_description]  DEFAULT (''),   
	[taxOfficeNotifyCd] [smallint] NOT NULL CONSTRAINT [DF_JournalEntry_taxOfficeNotifyCd]  DEFAULT (0),
	[jeHoldCd] [smallint] NOT NULL CONSTRAINT [DF_JournalEntry_jeHoldCd]  DEFAULT (0),
	[collectionExtractDt] [datetime] NOT NULL CONSTRAINT [DF_JournalEntry_collectionExtractDt]  DEFAULT ('1900-01-01'),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_JournalEntry_rowUpdateUserid]  DEFAULT (0),   
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_JournalEntry_rowUpdateDt]  DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_JournalEntry_rowDeleteFlag]  DEFAULT (''),   
  
 CONSTRAINT [PK_JournalEntry] PRIMARY KEY CLUSTERED 
(
	[idJournalEntry] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_JournalEntry] ON [dbo].[JournalEntry]
(
	[appraisalYear] DESC,
	[appUser] ASC,
	[idJournalEntry] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

IF OBJECT_ID('[dbo].[JournalEntryBatch]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[JournalEntryBatch]; 
GO

CREATE TABLE [dbo].[JournalEntryBatch](
	[idJournalEntryBatch] [bigint] IDENTITY(1,1) NOT NULL,
	[batchId] [int] NOT NULL CONSTRAINT [DF_JournalEntryBatch_batchId]  DEFAULT (0),
	[createDt] [datetime] NOT NULL CONSTRAINT [DF_JournalEntryBatch_createDt]  DEFAULT (getdate()),
	[processDt] [datetime] NOT NULL CONSTRAINT [DF_JournalEntryBatch_processDt]  DEFAULT ('1900-01-01'),
	[description] [varchar](50) NOT NULL CONSTRAINT [DF_JournalEntryBatch_description]  DEFAULT (''),   
	[jeCount] [int] NOT NULL CONSTRAINT [DF_JournalEntryBatch_jeCount]  DEFAULT (0),
	[leaseOwnerCount] [int] NOT NULL CONSTRAINT [DF_JournalEntryBatch_leaseOwnerCount]  DEFAULT (0),
	[nonValueChangeCount] [int] NOT NULL CONSTRAINT [DF_JournalEntryBatch_nonValueChangeCount]  DEFAULT (0),
	[valueChangeCount] [int] NOT NULL CONSTRAINT [DF_JournalEntryBatch_valueChangeCount]  DEFAULT (0),
	[beforeOwnerValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_JournalEntryBatch_beforeOwnerValue] DEFAULT (0),
	[afterOwnerValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_JournalEntryBatch_afterOwnerValue] DEFAULT (0),
	[rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_JournalEntryBatch_rowUpdateUserid]  DEFAULT (0),   
	[rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_JournalEntryBatch_rowUpdateDt]  DEFAULT (getdate()),
	[rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_JournalEntryBatch_rowDeleteFlag]  DEFAULT (''),   
  
 CONSTRAINT [PK_JournalEntryBatch] PRIMARY KEY CLUSTERED 
(
	[idJournalEntryBatch] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_JournalEntryBatch] ON [dbo].[JournalEntryBatch]
(
	[batchId] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

/*  VALUES
	[exemptionValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Exemption_exemptionValue] DEFAULT (0),
	[estimatedTaxRate] [decimal](9, 8) NOT NULL CONSTRAINT [DF_TaxUnit_estimatedTaxRate] DEFAULT (0.0),
	[acres] [decimal](10, 3) NOT NULL CONSTRAINT [DF_Lease_acres] DEFAULT (0.0),
	[taxUnitPercent] [decimal](4, 3) NOT NULL CONSTRAINT [DF_LeaseTaxUnit_taxUnitPercent] DEFAULT (0.0),
	[interestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_LeaseOwner_interestPercent] DEFAULT (0.0),
	[ownerValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_LeaseOwner_ownerValue] DEFAULT (0),
	[workingInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_LeaseValueSummary_workingInterestPercent] DEFAULT (0.0),
	[royaltyInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_LeaseValueSummary_royaltyInterestPercent] DEFAULT (0.0),
	[overridingRoyaltyPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_LeaseValueSummary_overridingRoyaltyPercent] DEFAULT (0.0),
	[oilPaymentPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_LeaseValueSummary_oilPaymentPercent] DEFAULT (0.0),
	[workingInterestValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_LeaseValueSummary_workingInterestValue] DEFAULT (0),
	[royaltyInterestValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_LeaseValueSummary_royaltyInterestValue] DEFAULT (0),
	[appraisedValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_LeaseValueSummary_appraisedValue] DEFAULT (0),
	[unitWorkingInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_LeaseValueSummary_unitWorkingInterestPercent] DEFAULT (0.0),
	[unitRoyaltyInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_LeaseValueSummary_unitRoyaltyInterestPercent] DEFAULT (0.0),
	[taxPercent] [decimal](4, 3) NOT NULL CONSTRAINT [DF_CalcTaxAdj_taxPercent] DEFAULT (0.0),
	[adjPercent] [decimal](5, 4) NOT NULL CONSTRAINT [DF_CalcEscalatedPriceAdj_adjPercent] DEFAULT (0.0),
	[adjPercent] [decimal](5, 4) NOT NULL CONSTRAINT [DF_CalcEscalatedOpExpAdj_adjPercent] DEFAULT (0.0),
	[salvageValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_CalcEquipmentSchedule_salvageValue] DEFAULT (0),
	[factor] [decimal](7, 6) NOT NULL CONSTRAINT [DF_CalcEquipmentPVSchedule_factor] DEFAULT (0.0),
	[workingInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_UnitValueSummary_workingInterestPercent] DEFAULT (0.0),
	[royaltyInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_UnitValueSummary_royaltyInterestPercent] DEFAULT (0.0),
	[weightedWorkingInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_UnitValueSummary_weightedWorkingInterestPercent] DEFAULT (0.0),
	[weightedRoyaltyInterestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_UnitValueSummary_weightedRoyaltyInterestPercent] DEFAULT (0.0),
	[workingInterestValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_UnitValueSummary_workingInterestValue] DEFAULT (0),
	[royaltyInterestValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_UnitValueSummary_royaltyInterestValue] DEFAULT (0),
	[productionEquipmentValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_productionEquipmentValue] DEFAULT (0),
	[serviceEquipmentValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_serviceEquipmentValue] DEFAULT (0),
	[injectionEquipmentValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_injectionEquipmentValue] DEFAULT (0),
	[discountRate] [decimal](4, 3) NOT NULL CONSTRAINT [DF_Appraisal_discountRate] DEFAULT (0.0),
	[grossOilPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_Appraisal_grossOilPrice] DEFAULT (0.0),
	[grossProductPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_Appraisal_grossProductPrice] DEFAULT (0.0),
	[grossWorkingInterestGasPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_Appraisal_grossWorkingInterestGasPrice] DEFAULT (0.0),
	[grossRoyaltyInterestGasPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_Appraisal_grossRoyaltyInterestGasPrice] DEFAULT (0.0),
	[operatingExpense] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_operatingExpense] DEFAULT (0),
	[dailyAverageOil] [decimal](7, 2) NOT NULL CONSTRAINT [DF_Appraisal_dailyAverageOil] DEFAULT (0.0),
	[dailyAverageGas] [decimal](7, 2) NOT NULL CONSTRAINT [DF_Appraisal_dailyAverageGas] DEFAULT (0.0),
	[dailyAverageProduct] [decimal](7, 2) NOT NULL CONSTRAINT [DF_Appraisal_dailyAverageProduct] DEFAULT (0.0),
	[reserveOilValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_reserveOilValue] DEFAULT (0),
	[totalValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_totalValue] DEFAULT (0),
	[workingInterestOilValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_workingInterestOilValue] DEFAULT (0),
	[workingInterestGasValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_workingInterestGasValue] DEFAULT (0),
	[workingInterestProductValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_workingInterestProductValue] DEFAULT (0),
	[workingInterestTotalValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_workingInterestTotalValue] DEFAULT (0),
	[workingInterestTotalPV] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_workingInterestTotalPV] DEFAULT (0),
	[royaltyInterestOilValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_royaltyInterestOilValue] DEFAULT (0),
	[royaltyInterestGasValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_royaltyInterestGasValue] DEFAULT (0),
	[royaltyInterestProductValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_royaltyInterestProductValue] DEFAULT (0),
	[royaltyInterestTotalValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_Appraisal_royaltyInterestTotalValue] DEFAULT (0),
	[declinePercent] [decimal](4, 3) NOT NULL CONSTRAINT [DF_ApprDeclineSchedule_declinePercent] DEFAULT (0.0),
	[netOilPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netOilPrice] DEFAULT (0.0),
	[netProductPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netProductPrice] DEFAULT (0.0),
	[netWorkingInterestGasPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netWorkingInterestGasPrice] DEFAULT (0.0),
	[netRoyaltyInterestGasPrice] [decimal](5, 2) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netRoyaltyInterestGasPrice] DEFAULT (0.0),
	[grossWorkingInterestOilRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_grossWorkingInterestOilRevenue] DEFAULT (0),
	[grossWorkingInterestGasRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_grossWorkingInterestGasRevenue] DEFAULT (0),
	[grossWorkingInterestProductRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_grossWorkingInterestProductRevenue] DEFAULT (0),
	[grossWorkingInterestTotalRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_grossWorkingInterestTotalRevenue] DEFAULT (0),
	[grossRoyaltyInterestOilRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_grossRoyaltyInterestOilRevenue] DEFAULT (0),
	[grossRoyaltyInterestGasRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_grossRoyaltyInterestGasRevenue] DEFAULT (0),
	[grossRoyaltyInterestProductRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_grossRoyaltyInterestProductRevenue] DEFAULT (0),
	[workingInterestOilRevenuePercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_workingInterestOilRevenuePercent] DEFAULT (0),
	[workingInterestGasRevenuePercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_workingInterestGasRevenuePercent] DEFAULT (0),
	[workingInterestProductRevenuePercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_workingInterestProductRevenuePercent] DEFAULT (0),
	[operatingExpense] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_operatingExpense] DEFAULT (0),
	[netWorkingInterestOilRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netWorkingInterestOilRevenue] DEFAULT (0),
	[netWorkingInterestGasRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netWorkingInterestGasRevenue] DEFAULT (0),
	[netWorkingInterestProductRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netWorkingInterestProductRevenue] DEFAULT (0),
	[netWorkingInterestTotalRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netWorkingInterestTotalRevenue] DEFAULT (0),
	[netRoyaltyInterestOilRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netRoyaltyInterestOilRevenue] DEFAULT (0),
	[netRoyaltyInterestGasRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netRoyaltyInterestGasRevenue] DEFAULT (0),
	[netRoyaltyInterestProductRevenue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_netRoyaltyInterestProductRevenue] DEFAULT (0),
	[workingInterestTotalPV] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_workingInterestTotalPV] DEFAULT (0),
	[workingInterestTotalNetValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_ApprAmortSchedule_workingInterestTotalNetValue] DEFAULT (0),
    END VALUES
*/