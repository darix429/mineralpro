-- MIGRATION NOTES
-- ONLY NEED TO INSERT INTO [Minerals].[dbo].[EMPLOYEE] 'APPLOGIX', 'CONV', 'dbo' AND OTHERS ONE TIME
-- [MINERALPRO].[dbo].[AppUser].nameAbbreviation used for Migration only ... this column should be removed after the migration
-- [Minerals].[dbo].[EMPLOYEE].[USER_NAME] ... IS THIS NEEDED?
-- FUTURE_YR ... IS THIS NEEDED?

USE [MINERALPRO]

DECLARE @MIN_APPRAISAL_YR SMALLINT
DECLARE @MAX_APPRAISAL_YR SMALLINT

SELECT @MIN_APPRAISAL_YR = MIN(APPRAISAL_YR)
  FROM [Minerals].[dbo].[APPRAISAL_YEAR]

SELECT @MAX_APPRAISAL_YR = MAX(APPRAISAL_YR)
  FROM [Minerals].[dbo].[APPRAISAL_YEAR]

SET @MIN_APPRAISAL_YR = 1999
--SET @MIN_APPRAISAL_YR = @MAX_APPRAISAL_YR - 1


--DELETE FROM [MINERALPRO].[dbo].[AppUserSubSystemFavorite]


--DELETE FROM [MINERALPRO].[dbo].[AppUserFavorite]


--DELETE FROM [MINERALPRO].[dbo].[UserFunction]


DELETE FROM [MINERALPRO].[dbo].[FunctionSecurity]


DELETE FROM [MINERALPRO].[dbo].[SubSystemFunction]


DELETE FROM [MINERALPRO].[dbo].[AppSubSystem]


DELETE FROM [MINERALPRO].[dbo].[SubSystem]


DELETE FROM [MINERALPRO].[dbo].[App]


DELETE FROM [MINERALPRO].[dbo].[UserOrgSecurity]


DELETE FROM [MINERALPRO].[dbo].[SecurityGroup]


DELETE FROM [MINERALPRO].[dbo].[UserOrganization]


DELETE FROM [MINERALPRO].[dbo].[AppUser]


DELETE FROM [MINERALPRO].[dbo].[OrganizationContact]


DELETE FROM [MINERALPRO].[dbo].[Organization]


SET IDENTITY_INSERT [dbo].[Organization] ON

INSERT INTO [MINERALPRO].[dbo].[Organization]
           (idOrganization
           ,organizationNumber
           ,organizationName)

VALUES     (1
           ,068
           ,'Ector CAD')

SET IDENTITY_INSERT [dbo].[Organization] OFF


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4253
           ,'APP'
           ,'LOGIX'
           ,'APPLOGIX'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4254
           ,''
           ,'CONV'
           ,'CONV'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4255
           ,''
           ,'dbo'
           ,'dbo'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4256
           ,'RALPH'
           ,'STILLINGER'
           ,'RSTILL'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4257
           ,''
           ,'AHEATH'
           ,'AHEATH'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4258
           ,''
           ,'APCM2'
           ,'APCM2'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4259
           ,''
           ,'APCM8'
           ,'APCM8'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4260
           ,''
           ,'APMWB'
           ,'APMWB'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4261
           ,''
           ,'APRKC'
           ,'APRKC'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4262
           ,''
           ,'BRYANR'
           ,'BRYANR'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4263
           ,''
           ,'DB2ADMIN'
           ,'DB2ADMIN'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4264
           ,''
           ,'eernest'
           ,'eernest'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4265
           ,''
           ,'ERNSTINE'
           ,'ERNSTINE'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4266
           ,''
           ,'GAYOSOS'
           ,'GAYOSOS'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4267
           ,''
           ,'KMCCORD'
           ,'KMCCORD'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4268
           ,''
           ,'LLOYDV'
           ,'LLOYDV'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4269
           ,''
           ,'MILAMJ'
           ,'MILAMJ'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4270
           ,''
           ,'MTURNER'
           ,'MTURNER'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4271
           ,''
           ,'MULLEND'
           ,'MULLEND'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4272
           ,''
           ,'pstewart'
           ,'pstewart'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4273
           ,''
           ,'RBRYAN'
           ,'RBRYAN'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4274
           ,''
           ,'RJARMAN'
           ,'RJARMAN'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4275
           ,''
           ,'SGAYOSO'
           ,'SGAYOSO'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4276
           ,''
           ,'SHAUNAS'
           ,'SHAUNAS'
           ,1)


INSERT INTO [Minerals].[dbo].[EMPLOYEE]
           (EMPL_NUM_ID
           ,FIRST_NAME
           ,LAST_NAME
           ,EMPLOYEE_ID
           ,STATUS_CD)
VALUES 
           (4277
           ,''
           ,'SSCOTT'
           ,'SSCOTT'
           ,1)


INSERT INTO [MINERALPRO].[dbo].[OrganizationContact]
           (idOrganization
           ,contactNumber
           ,firstName
           ,lastName
           ,middleInitial
           ,nameAbbreviation
           ,title
           ,statusCd
           ,statusDt
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            O.idOrganization
           ,EMP.EMPL_NUM_ID
           ,EMP.FIRST_NAME
           ,EMP.LAST_NAME
           ,EMP.MIDDLE_INITIAL
           ,EMP.EMPLOYEE_ID
           ,''
           ,CASE EMP.STATUS_CD WHEN 2 THEN 14 ELSE 0 END
           ,EMP.STATUS_DT
           ,0 -- THIS FIELD IS POPULATED with idAppUser AFTER AppUser is CREATED
           ,EMP.LST_UPDT_TS
           ,CASE EMP.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[EMPLOYEE] EMP
 INNER JOIN [MINERALPRO].[dbo].[Organization] O
    ON O.organizationNumber = 068
-- WHERE EMP.POST_FLG <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[AppUser]
           (idOrganizationContact
           ,emailAddress
           ,password
           ,nameAbbreviation
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            OC.idOrganizationContact
           ,''
           ,EMP.DB_PWD
           ,EMP.EMPLOYEE_ID
           ,0 -- THIS FIELD IS POPULATED with idAppUser AFTER AppUser is CREATED
           ,EMP.LST_UPDT_TS
           ,CASE EMP.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[EMPLOYEE] EMP
 INNER JOIN [MINERALPRO].[dbo].[Organization] O
    ON O.organizationNumber = 068
 INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.contactNumber = EMP.EMPL_NUM_ID
-- WHERE EMP.POST_FLG <> 'D' 


UPDATE [MINERALPRO].[dbo].[AppUser]
   SET
      rowUpdateUserid = ISNULL(AU9.idAppUser,0)
--SELECT AU.*
--      ,EMP.[EMPLOYEE_ID]
--      ,EMP.[LST_UPDT_EMPL_ID]
--      ,EMP.[LST_UPDT_TS]
--      ,EMP.[POST_FLG]
--      ,EMP.[EMPL_NUM_ID]
--      ,AU9.*
  FROM [MINERALPRO].[dbo].[AppUser]
 INNER JOIN [Minerals].[dbo].[EMPLOYEE] EMP
    ON EMP.EMPLOYEE_ID = [MINERALPRO].[dbo].[AppUser].nameAbbreviation COLLATE DATABASE_DEFAULT
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU9
    ON AU9.nameAbbreviation = EMP.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
	


UPDATE [MINERALPRO].[dbo].[OrganizationContact] 
   SET
      rowUpdateUserid = AU.rowUpdateUserid
  FROM [MINERALPRO].[dbo].[OrganizationContact]
 INNER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.nameAbbreviation = [MINERALPRO].[dbo].[OrganizationContact].nameAbbreviation COLLATE DATABASE_DEFAULT


INSERT INTO [MINERALPRO].[dbo].[UserOrganization]
           (idAppUser
           ,idOrganization
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            AU.idAppUser
           ,O.idOrganization
           ,AU.rowUpdateUserid
           ,AU.rowUpdateDt
           ,AU.rowDeleteFlag
  FROM [MINERALPRO].[dbo].[Organization] O
  INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.idOrganization = O.idOrganization
--   AND OC.rowDeleteFlag <> 'D'
  INNER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.idOrganizationContact = OC.idOrganizationContact
--   AND AU.rowDeleteFlag <> 'D'
-- WHERE O.rowDeleteFlag <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Chief Appraiser')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Administration Admin')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Administration Director')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Administration Manager')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Administration Supervisor')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Administration Clerk')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Mineral Admin')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Mineral Director')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Mineral Manager')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Mineral Supervisor')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Mineral Appraiser')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Mineral Clerk')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Property Records Admin')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Property Records Director')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Property Records Manager')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Property Records Supervisor')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Property Records Clerk')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Exemptions Admin')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Exemptions Director')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Exemptions Manager')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Exemptions Supervisor')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('Exemptions Clerk')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('ARB Admin')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('ARB Director')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('ARB Manager')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('ARB Supervisor')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('ARB Clerk')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('IS Admin')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('IS Director')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('IS Manager')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('IS Supervisor')


INSERT INTO [MINERALPRO].[dbo].[SecurityGroup]
           (groupName)
VALUES     ('IS Engineer')


--INSERT INTO [MINERALPRO].[dbo].[UserOrgSecurity]
--SETUP IN SYSTEM (SETUP CAMAPRO AND JKCONSULTING IN BOTTOM SECTION)


INSERT INTO [MINERALPRO].[dbo].[UserOrgSecurity]
           (idUserOrganization
           ,idSecurityGroup)
SELECT
            UO.idUserOrganization
           ,SG.idSecurityGroup
  FROM [MINERALPRO].[dbo].[Organization] O
  INNER JOIN [MINERALPRO].[dbo].[UserOrganization] UO
    ON UO.idOrganization = O.idOrganization
  INNER JOIN [MINERALPRO].[dbo].[SecurityGroup] SG
    ON SG.groupName = 'IS Admin'
 WHERE O.organizationName = 'Ector CAD'


SET IDENTITY_INSERT [dbo].[App] ON 

INSERT INTO [MINERALPRO].[dbo].[App]
           (idApp
           ,appName
           ,displayName
           ,displaySeqNumber
           ,url
           ,imageUrl)
VALUES     (1
           ,'Chief Appraiser'
           ,'Chief Appraiser'
           ,1
           ,'ChiefAppraiser'
           ,'images/circle/chiefappraiser.png')


INSERT INTO [MINERALPRO].[dbo].[App]
           (idApp
           ,appName
           ,displayName
           ,displaySeqNumber
           ,url
           ,imageUrl)
VALUES     (2
           ,'Administration'
           ,'Administration'
           ,2
           ,'Administration'
           ,'images/circle/administration.png')


INSERT INTO [MINERALPRO].[dbo].[App]
           (idApp
           ,appName
           ,displayName
           ,displaySeqNumber
           ,url
           ,imageUrl)
VALUES     (3
           ,'Minerals'
           ,'Mineral Valuation'
           ,3
           ,'Minerals'
           ,'images/circle/minerals.png')


--INSERT INTO [MINERALPRO].[dbo].[App]
--           (idApp
--           ,appName
--           ,displayName
--           ,displaySeqNumber
--           ,url
--           ,imageUrl)
--VALUES     (4
--           ,'Property Records'
--           ,'Property Records'
--           ,6
--           ,'PropertyRecords'
--           ,'images/circle/record.png')


--INSERT INTO [MINERALPRO].[dbo].[App]
--           (idApp
--           ,appName
--           ,displayName
--           ,displaySeqNumber
--           ,url
--           ,imageUrl)
--VALUES     (5
--           ,'Exemptions'
--           ,'Exemptions'
--           ,7
--           ,'Exemptions'
--           ,'images/circle/exemption.png')


INSERT INTO [MINERALPRO].[dbo].[App]
           (idApp
           ,appName
           ,displayName
           ,displaySeqNumber
           ,url
           ,imageUrl)
VALUES     (6
           ,'ARB'
           ,'Mineral ARB'
           ,8
           ,'ARB'
           ,'images/circle/arb.png')


INSERT INTO [MINERALPRO].[dbo].[App]
           (idApp
           ,appName
           ,displayName
           ,displaySeqNumber
           ,url
           ,imageUrl)
VALUES     (7
           ,'Information Services'
           ,'Mineral IS'
           ,9
           ,'InformationServices'
           ,'images/circle/informationservices.png')

SET IDENTITY_INSERT [dbo].[App] OFF


SET IDENTITY_INSERT [dbo].[SubSystem] ON 

INSERT INTO [MINERALPRO].[dbo].[SubSystem]
           (idsubSystem
           ,subSystemName
           ,displayName
           ,displaySeqNumber)
VALUES     (1
           ,'Common Functions'
           ,'Common Functions'
           ,1)


--INSERT INTO [MINERALPRO].[dbo].[SubSystem]
--           (idsubSystem
--           ,subSystemName
--           ,displayName
--           ,displaySeqNumber)
--VALUES     (2
--           ,'Property Records'
--           ,'Property Records'
--           ,2)


--INSERT INTO [MINERALPRO].[dbo].[SubSystem]
--           (idsubSystem
--           ,subSystemName
--           ,displayName
--           ,displaySeqNumber)
--VALUES     (3
--           ,'Exemptions'
--           ,'Exemptions'
--           ,3)


--INSERT INTO [MINERALPRO].[dbo].[SubSystem]
--           (idsubSystem
--           ,subSystemName
--           ,displayName
--           ,displaySeqNumber)
--VALUES     (4
--           ,'Mineral Field Work'
--           ,'Mineral Field Work'
--           ,4)


INSERT INTO [MINERALPRO].[dbo].[SubSystem]
           (idsubSystem
           ,subSystemName
           ,displayName
           ,displaySeqNumber)
VALUES     (5
           ,'Mineral Valuation'
           ,'Valuation'
           ,5)


--INSERT INTO [MINERALPRO].[dbo].[SubSystem]
--           (idsubSystem
--           ,subSystemName
--           ,displayName
--           ,displaySeqNumber)
--VALUES     (6
--           ,'Mineral Analysis'
--           ,'Mineral Analysis'
--           ,6)


--INSERT INTO [MINERALPRO].[dbo].[SubSystem]
--           (idsubSystem
--           ,subSystemName
--           ,displayName
--           ,displaySeqNumber)
--VALUES     (7
--           ,'Mineral Entry'
--           ,'Mineral Entry'
--           ,7)


--INSERT INTO [MINERALPRO].[dbo].[SubSystem]
--           (idsubSystem
--           ,subSystemName
--           ,displayName
--           ,displaySeqNumber)
--VALUES     (8
--           ,'Mineral Admin'
--           ,'Mineral Admin'
--           ,8)


INSERT INTO [MINERALPRO].[dbo].[SubSystem]
           (idsubSystem
           ,subSystemName
           ,displayName
           ,displaySeqNumber)
VALUES     (9
           ,'ReportsExports'
           ,'Reports/Exports'
           ,14)


--INSERT INTO [MINERALPRO].[dbo].[SubSystem]
--           (idsubSystem
--           ,subSystemName
--           ,displayName
--           ,displaySeqNumber)
--VALUES     (10
--           ,'ARB General'
--           ,'ARB General'
--           ,15)


--INSERT INTO [MINERALPRO].[dbo].[SubSystem]
--           (idsubSystem
--           ,subSystemName
--           ,displayName
--           ,displaySeqNumber)
--VALUES     (11
--           ,'ARB Case Management'
--           ,'ARB Case Management'
--           ,16)


--INSERT INTO [MINERALPRO].[dbo].[SubSystem]
--           (idsubSystem
--           ,subSystemName
--           ,displayName
--           ,displaySeqNumber)
--VALUES     (12
--           ,'ARB Admin'
--           ,'ARB Admin'
--           ,17)


--INSERT INTO [MINERALPRO].[dbo].[SubSystem]
--           (idsubSystem
--           ,subSystemName
--           ,displayName
--           ,displaySeqNumber)
--VALUES     (13
--           ,'Website'
--           ,'Website'
--           ,18)


--INSERT INTO [MINERALPRO].[dbo].[SubSystem]
--           (idsubSystem
--           ,subSystemName
--           ,displayName
--           ,displaySeqNumber)
--VALUES     (??
--           ,'Imports'
--           ,'Imports'
--           ,19)


--INSERT INTO [MINERALPRO].[dbo].[SubSystem]
--           (idsubSystem
--           ,subSystemName
--           ,displayName
--           ,displaySeqNumber)
--VALUES     (??
--           ,'Exports'
--           ,'Exports'
--           ,20


INSERT INTO [MINERALPRO].[dbo].[SubSystem]
           (idsubSystem
           ,subSystemName
           ,displayName
           ,displaySeqNumber)
VALUES     (14
           ,'System Admin Security'
           ,'System Security'
           ,21)


INSERT INTO [MINERALPRO].[dbo].[SubSystem]
           (idsubSystem
           ,subSystemName
           ,displayName
           ,displaySeqNumber)
VALUES     (15
           ,'System Admin'
           ,'Table Maintenance'
           ,22)


INSERT INTO [MINERALPRO].[dbo].[SubSystem]
           (idsubSystem
           ,subSystemName
           ,displayName
           ,displaySeqNumber)
VALUES     (16
           ,'System Functions'
           ,'Minerals Data Functions'
           ,23)


INSERT INTO [MINERALPRO].[dbo].[SubSystem]
           (idsubSystem
           ,subSystemName
           ,displayName
           ,displaySeqNumber)
VALUES     (17
           ,'Proper Records'
           ,'Mineral Admin'
           ,24)


INSERT INTO [MINERALPRO].[dbo].[SubSystem]
           (idsubSystem
           ,subSystemName
           ,displayName
           ,displaySeqNumber)
VALUES     (18
           ,'Organization'
           ,'System Security'
           ,25)

SET IDENTITY_INSERT [dbo].[SubSystem] OFF


SET IDENTITY_INSERT [dbo].[AppSubSystem] ON 

INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
           (idAppSubSystem
           ,idApp
           ,idSubSystem)
SELECT 
            1
           ,A.idApp
           ,SS.idSubSystem
  FROM [MINERALPRO].[dbo].[App] A
 INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
    ON SS.subSystemName = 'Common Functions'
 WHERE A.appName = 'Chief Appraiser'


INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
           (idAppSubSystem
           ,idApp
           ,idSubSystem)
SELECT 
            2
           ,A.idApp
           ,SS.idSubSystem
  FROM [MINERALPRO].[dbo].[App] A
 INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
    ON SS.subSystemName = 'Common Functions'
 WHERE A.appName = 'Administration'


INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
           (idAppSubSystem
           ,idApp
           ,idSubSystem)
SELECT 
            3
           ,A.idApp
           ,SS.idSubSystem
  FROM [MINERALPRO].[dbo].[App] A
 INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
    ON SS.subSystemName = 'Common Functions'
 WHERE A.appName = 'Minerals'


--INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
--           (idAppSubSystem
--           ,idApp
--           ,idSubSystem)
--SELECT 
--            4
--           ,A.idApp
--           ,SS.idSubSystem
--  FROM [MINERALPRO].[dbo].[App] A
-- INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
--    ON SS.subSystemName = 'Mineral Field Work'
-- WHERE A.appName = 'Minerals'


INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
           (idAppSubSystem
           ,idApp
           ,idSubSystem)
SELECT 
            5
           ,A.idApp
           ,SS.idSubSystem
  FROM [MINERALPRO].[dbo].[App] A
 INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
    ON SS.subSystemName = 'Mineral Valuation'
 WHERE A.appName = 'Minerals'


--INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
--           (idAppSubSystem
--           ,idApp
--           ,idSubSystem)
--SELECT 
--            6
--           ,A.idApp
--           ,SS.idSubSystem
--  FROM [MINERALPRO].[dbo].[App] A
-- INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
--    ON SS.subSystemName = 'Mineral Analysis'
-- WHERE A.appName = 'Minerals'


--INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
--           (idAppSubSystem
--           ,idApp
--           ,idSubSystem)
--SELECT 
--            7
--           ,A.idApp
--           ,SS.idSubSystem
--  FROM [MINERALPRO].[dbo].[App] A
-- INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
--    ON SS.subSystemName = 'Mineral Entry'
-- WHERE A.appName = 'Minerals'


--INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
--           (idAppSubSystem
--           ,idApp
--           ,idSubSystem)
--SELECT 
--            8
--           ,A.idApp
--           ,SS.idSubSystem
--  FROM [MINERALPRO].[dbo].[App] A
-- INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
--    ON SS.subSystemName = 'Mineral Admin'
-- WHERE A.appName = 'Minerals'


--INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
--           (idAppSubSystem
--           ,idApp
--           ,idSubSystem)
--SELECT 
--            9
--           ,A.idApp
--           ,SS.idSubSystem
--  FROM [MINERALPRO].[dbo].[App] A
-- INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
--    ON SS.subSystemName = 'Common Functions'
-- WHERE A.appName = 'Property Records'


--INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
--           (idAppSubSystem
--           ,idApp
--           ,idSubSystem)
--SELECT 
--            10
--           ,A.idApp
--           ,SS.idSubSystem
--  FROM [MINERALPRO].[dbo].[App] A
-- INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
--    ON SS.subSystemName = 'Property Records'
-- WHERE A.appName = 'Property Records'


--INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
--           (idAppSubSystem
--           ,idApp
--           ,idSubSystem)
--SELECT 
--            11
--           ,A.idApp
--           ,SS.idSubSystem
--  FROM [MINERALPRO].[dbo].[App] A
-- INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
--    ON SS.subSystemName = 'Common Functions'
-- WHERE A.appName = 'Exemptions'


--INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
--           (idAppSubSystem
--           ,idApp
--           ,idSubSystem)
--SELECT 
--            12
--           ,A.idApp
--           ,SS.idSubSystem
--  FROM [MINERALPRO].[dbo].[App] A
-- INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
--    ON SS.subSystemName = 'Exemptions'
-- WHERE A.appName = 'Exemptions'


--INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
--           (idAppSubSystem
--           ,idApp
--           ,idSubSystem)
--SELECT 
--            13
--           ,A.idApp
--           ,SS.idSubSystem
--  FROM [MINERALPRO].[dbo].[App] A
-- INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
--    ON SS.subSystemName = 'Common Functions'
-- WHERE A.appName = 'ARB'


--INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
--           (idAppSubSystem
--           ,idApp
--           ,idSubSystem)
--SELECT 
--            14
--           ,A.idApp
--           ,SS.idSubSystem
--  FROM [MINERALPRO].[dbo].[App] A
-- INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
--    ON SS.subSystemName = 'ARB General'
-- WHERE A.appName = 'ARB'


--INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
--           (idAppSubSystem
--           ,idApp
--           ,idSubSystem)
--SELECT 
--            15
--           ,A.idApp
--           ,SS.idSubSystem
--  FROM [MINERALPRO].[dbo].[App] A
-- INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
--    ON SS.subSystemName = 'ARB Case Management'
-- WHERE A.appName = 'ARB'


--INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
--           (idAppSubSystem
--           ,idApp
--           ,idSubSystem)
--SELECT 
--            16
--           ,A.idApp
--           ,SS.idSubSystem
--  FROM [MINERALPRO].[dbo].[App] A
-- INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
--    ON SS.subSystemName = 'ARB Admin'
-- WHERE A.appName = 'ARB'


INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
           (idAppSubSystem
           ,idApp
           ,idSubSystem)
SELECT 
            17
           ,A.idApp
           ,SS.idSubSystem
  FROM [MINERALPRO].[dbo].[App] A
 INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
    ON SS.subSystemName = 'Common Functions'
 WHERE A.appName = 'Information Services'


INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
           (idAppSubSystem
           ,idApp
           ,idSubSystem)
SELECT 
            18
           ,A.idApp
           ,SS.idSubSystem
  FROM [MINERALPRO].[dbo].[App] A
 INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
    ON SS.subSystemName = 'ReportsExports'
 WHERE A.appName = 'Information Services'


--INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
--           (idAppSubSystem
--           ,idApp
--           ,idSubSystem)
--SELECT 
--            19
--           ,A.idApp
--           ,SS.idSubSystem
--  FROM [MINERALPRO].[dbo].[App] A
-- INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
--    ON SS.subSystemName = 'Website'
-- WHERE A.appName = 'Information Services'


--INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
--           (idAppSubSystem
--           ,idApp
--           ,idSubSystem)
--SELECT 
--            ??
--           ,A.idApp
--           ,SS.idSubSystem
--  FROM [MINERALPRO].[dbo].[App] A
-- INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
--    ON SS.subSystemName = 'Imports'
-- WHERE A.appName = 'Information Service


--INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
--           (idAppSubSystem
--           ,idApp
--           ,idSubSystem)
--SELECT 
--            ??
--           ,A.idApp
--           ,SS.idSubSystem
--  FROM [MINERALPRO].[dbo].[App] A
-- INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
--    ON SS.subSystemName = 'Exports'
-- WHERE A.appName = 'Information Service


INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
           (idAppSubSystem
           ,idApp
           ,idSubSystem)
SELECT 
            20
           ,A.idApp
           ,SS.idSubSystem
  FROM [MINERALPRO].[dbo].[App] A
 INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
    ON SS.subSystemName = 'System Admin Security'
 WHERE A.appName = 'Information Services'


INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
           (idAppSubSystem
           ,idApp
           ,idSubSystem)
SELECT 
            21
           ,A.idApp
           ,SS.idSubSystem
  FROM [MINERALPRO].[dbo].[App] A
 INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
    ON SS.subSystemName = 'System Admin'
 WHERE A.appName = 'Information Services'


INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
           (idAppSubSystem
           ,idApp
           ,idSubSystem)
SELECT 
            22
           ,A.idApp
           ,SS.idSubSystem
  FROM [MINERALPRO].[dbo].[App] A
 INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
    ON SS.subSystemName = 'System Functions'
 WHERE A.appName = 'Information Services'


INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
           (idAppSubSystem
           ,idApp
           ,idSubSystem)
SELECT 
            23
           ,A.idApp
           ,SS.idSubSystem
  FROM [MINERALPRO].[dbo].[App] A
 INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
    ON SS.subSystemName = 'Organization'
 WHERE A.appName = 'Information Services'


INSERT INTO [MINERALPRO].[dbo].[AppSubSystem]
           (idAppSubSystem
           ,idApp
           ,idSubSystem)
SELECT 
            24
           ,A.idApp
           ,SS.idSubSystem
  FROM [MINERALPRO].[dbo].[App] A
 INNER JOIN [MINERALPRO].[dbo].[SubSystem] SS
    ON SS.subSystemName = 'Proper Records'
 WHERE A.appName = 'Minerals'

SET IDENTITY_INSERT [dbo].[AppSubSystem] OFF


SET IDENTITY_INSERT [dbo].[SubSystemFunction] ON

INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            1
           ,SS.idSubSystem
           ,'Search'
           ,'Search'
           ,1
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'Common Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Query Tool'
--           ,'Query Tool'
--           ,2
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Common Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Toolbox'
--           ,'Toolbox'
--           ,3
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Common Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Workflow'
--           ,'Workflow'
--           ,4
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Common Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            2
--           ,SS.idSubSystem
--           ,'Notes'
--           ,'Notes'
--           ,5
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Common Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Event History'
--           ,'Event History'
--           ,6
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Common Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Audit'
--           ,'Audit'
--           ,7
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Common Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Correspondence'
--           ,'Correspondence'
--           ,8
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Common Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Account Codes'
--           ,'Account Codes'
--           ,9
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Common Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Account Custom Data'
--           ,'Account Custom Data'
--           ,10
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Common Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Account Summary'
--           ,'Account Summary'
--           ,11
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Common Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Value Summary/History'
--           ,'Value Summary/History'
--           ,12
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Common Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'GIS'
--           ,'GIS'
--           ,13
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Common Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Media'
--           ,'Media'
--           ,14
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Common Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Scanning/Imaging'
--           ,'Scanning/Imaging'
--           ,15
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Common Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            3
--           ,SS.idSubSystem
--           ,'Maintain Operator'
--           ,'Maintain Operator'
--           ,1
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Property Records'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            4
--           ,SS.idSubSystem
--           ,'Maintain RRC Field'
--           ,'Maintain RRC Fields'
--           ,2
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Property Records'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            5
--           ,SS.idSubSystem
--           ,'Maintain Agency'
--           ,'Maintain Agency'
--           ,3
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Property Records'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            6
--           ,SS.idSubSystem
--           ,'Maintain Owner'
--           ,'Maintain Owner'
--           ,4
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Property Records'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            7
--           ,SS.idSubSystem
--           ,'Lease Header'
--           ,'Lease Header'
--           ,1
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Mineral Field Work'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            8
--           ,SS.idSubSystem
--           ,'Division of Interest'
--           ,'Division of Interest'
--           ,2
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Mineral Field Work'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            9
--           ,SS.idSubSystemm
--           ,'Unit'
--           ,'Unit'
--           ,3
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Mineral Field Work'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            10
--           ,SS.idSubSystem
--           ,'Appraisal'
--           ,'Appraisal'
--           ,1
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Mineral Valuation'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            11
--           ,SS.idSubSystem
--           ,'TaxOffice Reports'
--           ,'TaxOffice Reports'
--           ,1
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Reports/Exports'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            12
--           ,SS.idSubSystem
--           ,'TaxUnit Reports'
--           ,'TaxUnit Reports'
--           ,2
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Reports/Exports'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            13
--           ,SS.idSubSystem
--           ,'Property Owner Reports'
--           ,'Property Owner Reports'
--           ,3
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Reports/Exports'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            14
           ,SS.idSubSystem
           ,'CAD Reports'
           ,'Report for CAD'
           ,4
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'ReportsExports'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            15
           ,SS.idSubSystem
           ,'State Reports'
           ,'Report to State'
           ,5
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'ReportsExports'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Document Imaging'
--           ,'Document Imaging'
--           ,1
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'ARB General'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'State Forms'
--           ,'State Forms'
--           ,2
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'ARB General'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'CAD Forms / Letters'
--           ,'CAD Forms / Letters'
--           ,3
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'ARB General'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Mass Informal'
--           ,'Mass Informal'
--           ,4
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'ARB General'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Electronic Signature'
--           ,'Electronic Signature'
--           ,1
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'ARB Case Management'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Sign-In Screen'
--           ,'Sign-In Screen'
--           ,2
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'ARB Case Management'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Informal'
--           ,'Informal'
--           ,3
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'ARB Case Management'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Table Maintenance'
--           ,'Table Maintenance'
--           ,1
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'ARB Admin'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Website Maintenance'
--           ,'Website Maintenance'
--           ,1
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Website'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Account Upload'
--           ,'Account Upload'
--           ,2
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Website'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Minerals Import'
--           ,'Minerals Import'
--           ,1
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Imports'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Tax Office'
--           ,'Tax Office'
--           ,1
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Exports'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'Public Records'
--           ,'Public Records'
--           ,2
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Exports'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            SS.idSubSystem
--           ,'EARS'
--           ,'EARS'
--           ,3
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'Exports'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            16
           ,SS.idSubSystem
           ,'Security Group'
           ,'Manage Security Group'
           ,1
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'System Admin Security'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            17
           ,SS.idSubSystem
           ,'Security UserList'
           ,'Manage User'
           ,2
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'System Admin Security'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            18
           ,SS.idSubSystem
           ,'Security FunctionList'
           ,'Manage Function'
           ,3
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'System Admin Security'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            19
           ,SS.idSubSystem
           ,'Code Type'
           ,'Manage System Code'
           ,1
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'System Admin'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            20
--           ,SS.idSubSystem
--           ,'TaxUnit Maintenance'
--           ,'TaxUnit Maintenance'
--           ,3
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'System Admin'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            21
--           ,SS.idSubSystem
--           ,'Notice Processing'
--           ,'Notice Processing'
--           ,1
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'System Functions'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            22
           ,SS.idSubSystem
           ,'Roll to New Year'
           ,'Roll to New Year'
           ,2
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'System Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            23
--           ,SS.idSubSystem
--           ,'Exemption Reset'
--           ,'Exemption Reset'
--           ,3
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'System Functions'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            24
           ,SS.idSubSystem
           ,'Mass Appraisal'
           ,'Mass Appraisal'
           ,4
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'System Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            25
--           ,SS.idSubSystem
--           ,'Totals Archive'
--           ,'Totals Archive'
--           ,7
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'System Functions'


--INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
--           (idSubSystemFunction
--           ,idSubSystem
--           ,functionName
--           ,displayName
--           ,displaySeqNumber)
--SELECT
--            26
--           ,SS.idSubSystem
--           ,'Roll Archive'
--           ,'Roll Archive'
--           ,8
--  FROM [MINERALPRO].[dbo].[SubSystem] SS
-- WHERE SS.subSystemName = 'System Functions'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            27
           ,SS.idSubSystem
           ,'Manage Lease'
           ,'Manage Lease'
           ,1
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'Mineral Valuation'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            28
           ,SS.idSubSystem
           ,'Manage Unit'
           ,'Manage Unit'
           ,2
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'Mineral Valuation'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            29
           ,SS.idSubSystem
           ,'Manage Operator'
           ,'Manage Operator'
           ,1
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'Proper Records'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            30
           ,SS.idSubSystem
           ,'Manage Field'
           ,'Manage Field'
           ,2
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'Proper Records'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            31
           ,SS.idSubSystem
           ,'Manage Agent'
           ,'Manage Agent'
           ,3
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'Proper Records'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            32
           ,SS.idSubSystem
           ,'Manage Owner'
           ,'Manage Owner'
           ,4
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'Proper Records'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            33
           ,SS.idSubSystem
           ,'Organization Contact'
           ,'Manage Contact'
           ,2
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'Organization'


INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            34
           ,SS.idSubSystem
           ,'Organization List'
           ,'Manage Organization'
           ,1
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'Organization'

INSERT INTO [MINERALPRO].[dbo].[SubSystemFunction]
           (idSubSystemFunction
           ,idSubSystem
           ,functionName
           ,displayName
           ,displaySeqNumber)
SELECT
            35
           ,SS.idSubSystem
           ,'Manage Appraisal'
           ,'Manage Appraisal'
           ,1
  FROM [MINERALPRO].[dbo].[SubSystem] SS
 WHERE SS.subSystemName = 'Mineral Valuation'
 
SET IDENTITY_INSERT [dbo].[SubSystemFunction] OFF


--INSERT INTO [MINERALPRO].[dbo].[FunctionSecurity]
--SETUP IN SYSTEM (EXCEPT FOR [SecurityGroup].groupName = 'IS Admin')


INSERT INTO [MINERALPRO].[dbo].[FunctionSecurity]
           (idSubSystemFunction
           ,idSecurityGroup
           ,securityCd)
SELECT
            SSF.idSubSystemFunction
           ,SG.idSecurityGroup
           ,2
  FROM [MINERALPRO].[dbo].[SecurityGroup] SG
 INNER JOIN [MINERALPRO].[dbo].[SubSystemFunction] SSF
    ON SSF.rowDeleteFlag = ''
 WHERE SG.groupName = 'IS Admin'


--INSERT INTO [MINERALPRO].[dbo].[UserFunction]
--SOFTWARE MANAGES / MAINTAINS


--INSERT INTO [MINERALPRO].[dbo].[AppUserFavorite]
--SETUP IN SYSTEM


--INSERT INTO [MINERALPRO].[dbo].[AppUserSubSystemFavorite]
--SETUP IN SYSTEM


DELETE FROM [MINERALPRO].[dbo].[SystemCodeType]


INSERT INTO [MINERALPRO].[dbo].[SystemCodeType]
           (codeType
           ,description
           ,columnName
           ,tableNames
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            CDT.CODE_TYP
           ,CDT.CODE_TYP_DESC
           ,''
           ,''
           ,ISNULL(AU.idAppUser,0) -- CDT.LST_UPDT_EMPL_ID
           ,CDT.LST_UPDT_TS
           ,CASE CDT.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[CODE_TYPE] CDT
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.nameAbbreviation = CDT.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
-- WHERE CDT.POST_FLG <> 'D' 
 WHERE CDT.CODE_TYP IN(
 30
,40
,270
,530
,640
,780
,790
,810
,970
,1060
,1070
,1080
,1090
,1100
,2170
,3000
,3140
,3150
,3160
,3180
,3190
,3430
,3440
,3510
,3520
)


INSERT INTO [MINERALPRO].[dbo].[SystemCodeType]
           (codeType
           ,description
           ,columnName
           ,tableNames)

VALUES     (3530
           ,'GROUP CODES (FOR SECURITY)'
           ,'groupCd'
           ,'Note')


INSERT INTO [MINERALPRO].[dbo].[SystemCodeType]
           (codeType
           ,description
           ,columnName
           ,tableNames)

VALUES     (3540
           ,'SUBJECT TYPE CODES'
           ,'subjectTypeCd'
           ,'Note, Appraisal, ApprDeclineSchedule, ApprAmortSchedule')


INSERT INTO [MINERALPRO].[dbo].[SystemCodeType]
           (codeType
           ,description
           ,columnName
           ,tableNames)

VALUES     (5240
           ,'TAX OFFICE NOTIFY CODES'
           ,'taxOfficeNotifyCd'
           ,'JournalEntry')


INSERT INTO [MINERALPRO].[dbo].[SystemCodeType]
           (codeType
           ,description
           ,columnName
           ,tableNames)

VALUES     (5250
           ,'JOURNAL ENTRY HOLD CODES'
           ,'jeHoldCd'
           ,'JournalEntry')


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'statusCd'
, tableNames = 'OrganizationContact, Agency'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 30


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'lockCd'
, tableNames = 'LeaseOwner'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 40


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'exemptionTypeCd'
, tableNames = 'Exemption, TaxUnitExemption, OwnerExemption'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 270


--UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
--  columnName = 'securityCd'
--, tableNames = 'FunctionSecurity' <== ???, 'Note'
--  FROM [MINERALPRO].[dbo].[SystemCodeType]
-- WHERE codeType = 530


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'cadCategoryCd'
, tableNames = 'Lease'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 640


--UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
--  columnName = 'taxUnitCd'
--, tableNames = 'TaxUnit, TaxUnitExemption, LeaseTaxUnit'
--  FROM [MINERALPRO].[dbo].[SystemCodeType]
-- WHERE codeType = 780


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'stateCd'
, tableNames = 'Agency, Owner'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 790


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'countryCd'
, tableNames = 'Agency, Owner'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 810


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'taxUnitTypeCd'
, tableNames = 'TaxUnit, TaxUnitExemption, LeaseTaxUnit'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 970


--UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
--  columnName = 'taxUnitCd'
--, tableNames = 'TaxUnit, TaxUnitExemption, LeaseTaxUnit'
--  FROM [MINERALPRO].[dbo].[SystemCodeType]
-- WHERE codeType = 1060


--UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
--  columnName = 'taxUnitCd'
--, tableNames = 'TaxUnit, TaxUnitExemption, LeaseTaxUnit'
--  FROM [MINERALPRO].[dbo].[SystemCodeType]
-- WHERE codeType = 1070


--UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
--  columnName = 'taxUnitCd'
--, tableNames = 'TaxUnit, TaxUnitExemption, LeaseTaxUnit'
--  FROM [MINERALPRO].[dbo].[SystemCodeType]
-- WHERE codeType = 1080


--UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
--  columnName = 'taxUnitCd'
--, tableNames = 'TaxUnit, TaxUnitExemption, LeaseTaxUnit'
--  FROM [MINERALPRO].[dbo].[SystemCodeType]
-- WHERE codeType = 1090


--UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
--  columnName = 'taxUnitCd'
--, tableNames = 'TaxUnit, TaxUnitExemption, LeaseTaxUnit'
--  FROM [MINERALPRO].[dbo].[SystemCodeType]
-- WHERE codeType = 1100


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'interestTypeCd'
, tableNames = 'LeaseOwner'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 3000


--UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
--  columnName = 'wellTypeCd'
--, tableNames = 'CalcTaxAdj, CalcEscalatedPriceAdj, Appraisal'
--  FROM [MINERALPRO].[dbo].[SystemCodeType]
-- WHERE codeType = 3140


--UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
--  columnName = 'wellTypeCd'
--, tableNames = 'CalcEquipmentSchedule'
--  FROM [MINERALPRO].[dbo].[SystemCodeType]
-- WHERE codeType = 3150


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'valueTypeCd'
, tableNames = 'CalcEquipmentSchedule'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 3160


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'taxTypeCd'
, tableNames = 'CalcTaxAdj'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 3180


--UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
--  columnName = 'ownerChangeCd', 'changeReasonCd'
--, tableNames = 'Owner', 'Lease'
--  FROM [MINERALPRO].[dbo].[SystemCodeType]
-- WHERE codeType = 3190


--UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
--  columnName = 'exemptionCd'
--, tableNames = 'Exemption, TaxUnitExemption, OwnerExemption'
--  FROM [MINERALPRO].[dbo].[SystemCodeType]
-- WHERE codeType = 3430


--UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
--  columnName = 'exemptionCd'
--, tableNames = 'Exemption, TaxUnitExemption, OwnerExemption'
--  FROM [MINERALPRO].[dbo].[SystemCodeType]
-- WHERE codeType = 3440


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'gravityCd'
, tableNames = 'Field, Appraisal'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 3510


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'declineTypeCd'
, tableNames = 'ApprDeclineSchedule'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 3520


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'groupCd'
, tableNames = 'Note'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 3530


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'subjectTypeCd'
, tableNames = 'Note, Appraisal, ApprDeclineSchedule, ApprAmortSchedule'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 3540


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  description = 'JOURNAL ENTRY REASON CODES'
, columnName = 'jeReasonCd'
, tableNames = 'JournalEntry'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 2170


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'taxOfficeNotifyCd'
, tableNames = 'JournalEntry'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 5240


UPDATE [MINERALPRO].[dbo].[SystemCodeType] SET
  columnName = 'jeHoldCd'
, tableNames = 'JournalEntry'
  FROM [MINERALPRO].[dbo].[SystemCodeType]
 WHERE codeType = 5250


DELETE FROM [MINERALPRO].[dbo].[SystemCode]


INSERT INTO [MINERALPRO].[dbo].[SystemCode]
           (appraisalYear
           ,codeType
           ,code
           ,shortDescription
           ,description
           ,activeInd
           ,staticInd
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            CTM.APPRAISAL_YR
           ,CTM.CODE_TYP
           ,CTM.CODE
           ,CTM.LEGACY_VALUE
           ,CTM.CODE_DESCRIPTION
           ,CTM.ACTIVE_IND
           ,CTM.STATIC_CODE_IND
           ,ISNULL(AU.idAppUser,0) -- CTM.LST_UPDT_EMPL_ID
           ,CTM.LST_UPDT_TS
           ,CASE CTM.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[CODE_TABLE_MASTER] CTM
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.nameAbbreviation = CTM.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
-- WHERE CTM.POST_FLG <> 'D' 
 WHERE CTM.CODE_TYP IN(
-- 30
 40
--,270
,530
--,640
,780
,790
,810
,970
,1060
,1070
,1080
,1090
,1100
,2170
,3000
,3140
,3150
,3160
,3180
,3190
,3430
,3440
,3510
,3520
)
    OR (CTM.CODE_TYP =  30 AND CTM.CODE IN(0,12))
    OR (CTM.CODE_TYP = 640 AND CTM.CODE IN(0,32))
    OR (CTM.CODE_TYP = 270 AND CTM.CODE IN(0,3,10))


UPDATE [MINERALPRO].[dbo].[SystemCode] SET
  shortDescription = 'A'
, description = 'ACTIVE'
  FROM [MINERALPRO].[dbo].[SystemCode]
 WHERE codeType = 30
   AND code = 0


UPDATE [MINERALPRO].[dbo].[SystemCode] SET
  code = 14
, shortDescription = 'I'
, description = 'INACTIVE'
  FROM [MINERALPRO].[dbo].[SystemCode]
 WHERE codeType = 30
   AND code = 12


INSERT INTO [MINERALPRO].[dbo].[SystemCode]
           (appraisalYear
           ,codeType
           ,code
           ,shortDescription
           ,description)
SELECT
            APY.APPRAISAL_YR
           ,3530
           ,0
           ,''
           ,'UNASSIGNED'
  FROM [Minerals].[dbo].[APPRAISAL_YEAR] APY
-- WHERE APY.POST_FLG <> 'D'


INSERT INTO [MINERALPRO].[dbo].[SystemCode]
           (appraisalYear
           ,codeType
           ,code
           ,shortDescription
           ,description)
SELECT
            APY.APPRAISAL_YR
           ,3530
           ,1
           ,''
           ,'PR/E'
  FROM [Minerals].[dbo].[APPRAISAL_YEAR] APY
-- WHERE APY.POST_FLG <> 'D'


INSERT INTO [MINERALPRO].[dbo].[SystemCode]
           (appraisalYear
           ,codeType
           ,code
           ,shortDescription
           ,description)
SELECT
            APY.APPRAISAL_YR
           ,3540
           ,0
           ,''
           ,'UNASSIGNED'
  FROM [Minerals].[dbo].[APPRAISAL_YEAR] APY
-- WHERE APY.POST_FLG <> 'D'


INSERT INTO [MINERALPRO].[dbo].[SystemCode]
           (appraisalYear
           ,codeType
           ,code
           ,shortDescription
           ,description)
SELECT
            APY.APPRAISAL_YR
           ,3540
           ,1
           ,''
           ,'UNIT'
  FROM [Minerals].[dbo].[APPRAISAL_YEAR] APY
-- WHERE APY.POST_FLG <> 'D'


INSERT INTO [MINERALPRO].[dbo].[SystemCode]
           (appraisalYear
           ,codeType
           ,code
           ,shortDescription
           ,description)
SELECT
            APY.APPRAISAL_YR
           ,3540
           ,2
           ,''
           ,'LEASE'
  FROM [Minerals].[dbo].[APPRAISAL_YEAR] APY
-- WHERE APY.POST_FLG <> 'D'


INSERT INTO [MINERALPRO].[dbo].[SystemCode]
           (appraisalYear
           ,codeType
           ,code
           ,shortDescription
           ,description)
SELECT
            APY.APPRAISAL_YR
           ,3540
           ,3
           ,''
           ,'OWNER'
  FROM [Minerals].[dbo].[APPRAISAL_YEAR] APY
-- WHERE APY.POST_FLG <> 'D'


INSERT INTO [MINERALPRO].[dbo].[SystemCode]
           (appraisalYear
           ,codeType
           ,code
           ,shortDescription
           ,description)
SELECT
            APY.APPRAISAL_YR
           ,5240
           ,0
           ,''
           ,'UNASSIGNED'
  FROM [Minerals].[dbo].[APPRAISAL_YEAR] APY
-- WHERE APY.POST_FLG <> 'D'


INSERT INTO [MINERALPRO].[dbo].[SystemCode]
           (appraisalYear
           ,codeType
           ,code
           ,shortDescription
           ,description)
SELECT
            APY.APPRAISAL_YR
           ,5250
           ,0
           ,''
           ,'UNASSIGNED'
  FROM [Minerals].[dbo].[APPRAISAL_YEAR] APY
-- WHERE APY.POST_FLG <> 'D'


DELETE FROM [MINERALPRO].[dbo].[SystemCodeGroup]


--INSERT INTO [MINERALPRO].[dbo].[SystemCodeGroup]
--           (appraisalYear
--           ,codeTypeParent
--           ,codeParent
--           ,codeTypeChild
--           ,codeChild
--           ,rowUpdateUserid
--           ,rowUpdateDt
--           ,rowDeleteFlag)
--SELECT
--            CDG.APPRAISAL_YR
--           ,CDG.GRP_CODE_TYP
--           ,CDG.GRP_CODE
--           ,CDG.CODE_TYP
--           ,CDG.CODE
--           ,ISNULL(AU.idAppUser,0) -- CDG.LST_UPDT_EMPL_ID
--           ,CDG.LST_UPDT_TS
--           ,CASE CDG.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
--  FROM [Minerals].[dbo].[CODE_GRP] CDG
--  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU
--    ON AU.nameAbbreviation = CDG.LST_UPDT_EMPL_ID
-- WHERE CDG.POST_FLG <> 'D' 


DELETE FROM [MINERALPRO].[dbo].[SystemSetting]


INSERT INTO [MINERALPRO].[dbo].[SystemSetting]
           (appraisalYear
           ,settingName
           ,settingValue
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            GLO.APPRAISAL_YR
           ,GLO.FIELD_NAME
           ,GLO.FIELD_VALUE
           ,ISNULL(AU.idAppUser,0) -- GLO.LST_UPDT_EMPL_ID
           ,GLO.LST_UPDT_TS
           ,CASE GLO.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[MRL_GLOBAL] GLO
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.nameAbbreviation = GLO.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
-- WHERE GLO.POST_FLG <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[SystemSetting]
           (appraisalYear
           ,settingName
           ,settingValue
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            CLI.APPRAISAL_YR
           ,'CLIENT_NAME'
           ,CLI.CLIENT_NAME
           ,ISNULL(AU.idAppUser,0) -- CLI.LST_UPDT_EMPL_ID
           ,CLI.LST_UPDT_TS
           ,CASE CLI.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[CLIENT_INFO] CLI
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.nameAbbreviation = CLI.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
-- WHERE CLI.POST_FLG <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[SystemSetting]
           (appraisalYear
           ,settingName
           ,settingValue
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            CLI.APPRAISAL_YR
           ,'CHIEF_APPRAISER'
           ,CLI.CHIEF_APPRAISER
           ,ISNULL(AU.idAppUser,0) -- CLI.LST_UPDT_EMPL_ID
           ,CLI.LST_UPDT_TS
           ,CASE CLI.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[CLIENT_INFO] CLI
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.nameAbbreviation = CLI.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
-- WHERE CLI.POST_FLG <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[SystemSetting]
           (appraisalYear
           ,settingName
           ,settingValue
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            CLI.APPRAISAL_YR
           ,'CLIENT_ADDRESS_LINE2'
           ,CLI.ADDRESS_LINE2
           ,ISNULL(AU.idAppUser,0) -- CLI.LST_UPDT_EMPL_ID
           ,CLI.LST_UPDT_TS
           ,CASE CLI.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[CLIENT_INFO] CLI
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.nameAbbreviation = CLI.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
-- WHERE CLI.POST_FLG <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[SystemSetting]
           (appraisalYear
           ,settingName
           ,settingValue
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            CLI.APPRAISAL_YR
           ,'CLIENT_CITY'
           ,CLI.CITY
           ,ISNULL(AU.idAppUser,0) -- CLI.LST_UPDT_EMPL_ID
           ,CLI.LST_UPDT_TS
           ,CASE CLI.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[CLIENT_INFO] CLI
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.nameAbbreviation = CLI.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
-- WHERE CLI.POST_FLG <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[SystemSetting]
           (appraisalYear
           ,settingName
           ,settingValue
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            CLI.APPRAISAL_YR
           ,'CLIENT_STATE'
           ,CLI.STATE
           ,ISNULL(AU.idAppUser,0) -- CLI.LST_UPDT_EMPL_ID
           ,CLI.LST_UPDT_TS
           ,CASE CLI.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[CLIENT_INFO] CLI
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.nameAbbreviation = CLI.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
-- WHERE CLI.POST_FLG <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[SystemSetting]
           (appraisalYear
           ,settingName
           ,settingValue
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            CLI.APPRAISAL_YR
           ,'CLIENT_ZIP'
           ,CLI.ZIP
           ,ISNULL(AU.idAppUser,0) -- CLI.LST_UPDT_EMPL_ID
           ,CLI.LST_UPDT_TS
           ,CASE CLI.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[CLIENT_INFO] CLI
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.nameAbbreviation = CLI.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
-- WHERE CLI.POST_FLG <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[SystemSetting]
           (appraisalYear
           ,settingName
           ,settingValue
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            CLI.APPRAISAL_YR
           ,'CLIENT_PHONE'
           ,CLI.PHONE
           ,ISNULL(AU.idAppUser,0) -- CLI.LST_UPDT_EMPL_ID
           ,CLI.LST_UPDT_TS
           ,CASE CLI.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[CLIENT_INFO] CLI
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.nameAbbreviation = CLI.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
-- WHERE CLI.POST_FLG <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[SystemSetting]
           (appraisalYear
           ,settingName
           ,settingValue
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            CLI.APPRAISAL_YR
           ,'LAST_PROTEST_DATE'
           ,CLI.LST_PROTEST_DATE
           ,ISNULL(AU.idAppUser,0) -- CLI.LST_UPDT_EMPL_ID
           ,CLI.LST_UPDT_TS
           ,CASE CLI.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[CLIENT_INFO] CLI
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.nameAbbreviation = CLI.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
-- WHERE CLI.POST_FLG <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[SystemSetting]
           (appraisalYear
           ,settingName
           ,settingValue
           ,rowUpdateUserid
           ,rowUpdateDt
           ,rowDeleteFlag)
SELECT
            CLI.APPRAISAL_YR
           ,'FIRST_HEARING_DATE'
           ,CLI.FST_HEARING_DATE
           ,ISNULL(AU.idAppUser,0) -- CLI.LST_UPDT_EMPL_ID
           ,CLI.LST_UPDT_TS
           ,CASE CLI.POST_FLG WHEN 'D' THEN 'D' ELSE '' END
  FROM [Minerals].[dbo].[CLIENT_INFO] CLI
  LEFT OUTER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.nameAbbreviation = CLI.LST_UPDT_EMPL_ID COLLATE DATABASE_DEFAULT
-- WHERE CLI.POST_FLG <> 'D' 


INSERT INTO [MINERALPRO].[dbo].[SystemSetting]
           (appraisalYear
           ,settingName
           ,settingValue)
SELECT
            APY.APPRAISAL_YR
           ,'REPORT_URL'
           ,''
  FROM [Minerals].[dbo].[APPRAISAL_YEAR] APY
 WHERE APY.POST_FLG <> 'D'  


-- SETUP FOR CAMAPRO AND JKCONSULTING ORGANIZATION / USERS
-- ONE [Organization] and [UserOrganization] INSERT STATEMENT PER ORGANIZATION
-- ONE [UserOrgSecurity] and [UserOrganization] INSERT STATEMENT PER ORGANIZATION
-- ONE [OrganizationContact] and [AppUser] INSERT STATEMENT PER USER

-- SETUP FOR CAMAPRO ORGANIZATION / USERS / SECURITY

SET IDENTITY_INSERT [dbo].[Organization] ON

INSERT INTO [MINERALPRO].[dbo].[Organization]
           (idOrganization
           ,organizationNumber
           ,organizationName)

VALUES     (2
           ,900
           ,'CamaPro')

SET IDENTITY_INSERT [dbo].[Organization] OFF


INSERT INTO [MINERALPRO].[dbo].[OrganizationContact]
           (idOrganization
           ,contactNumber
           ,firstName
           ,lastName
           ,middleInitial
           ,nameAbbreviation
           ,title
           ,statusCd
           ,statusDt)
SELECT
            OC.idOrganization
           ,9000
           ,'Mineral'
           ,'ProOne'
           ,''
           ,'MPROONE'
           ,''
           ,0
           ,GETDATE()
  FROM [MINERALPRO].[dbo].[Organization] OC
 WHERE OC.organizationName = 'CamaPro'


INSERT INTO [MINERALPRO].[dbo].[AppUser]
           (idOrganizationContact
           ,emailAddress
           ,password
           ,nameAbbreviation)
SELECT
            OC.idOrganizationContact
           ,'MineralProOne@yahoo.com'
           ,'$2a$10$5lgWm3WsL/fVa8iog3vurejUDuwr70SbkL7DnYE2g9CaESJdKCy5u'
           ,OC.nameAbbreviation
  FROM [MINERALPRO].[dbo].[Organization] O
 INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.idOrganization = O.idOrganization
   AND OC.nameAbbreviation = 'MPROONE'
 WHERE O.organizationName = 'CamaPro'


INSERT INTO [MINERALPRO].[dbo].[OrganizationContact]
           (idOrganization
           ,contactNumber
           ,firstName
           ,lastName
           ,middleInitial
           ,nameAbbreviation
           ,title
           ,statusCd
           ,statusDt)
SELECT
            idOrganization
           ,9001
           ,'Shannon'
           ,'Davis'
           ,''
           ,'SDAVIS'
           ,''
           ,0
           ,GETDATE()
  FROM [MINERALPRO].[dbo].[Organization] OC
 WHERE OC.organizationName = 'CamaPro'


INSERT INTO [MINERALPRO].[dbo].[AppUser]
           (idOrganizationContact
           ,emailAddress
           ,password
           ,nameAbbreviation)
SELECT
            OC.idOrganizationContact
           ,'shannon@cama.pro'
           ,'$2a$10$zc6xO0RDTrOS4Q2j/MUxU.fq3yhtFKFGKhr5Zs37R2nei94Ykt/Hu'
           ,OC.nameAbbreviation
  FROM [MINERALPRO].[dbo].[Organization] O
 INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.idOrganization = O.idOrganization
   AND OC.nameAbbreviation = 'SDAVIS'
 WHERE O.organizationName = 'CamaPro'


INSERT INTO [MINERALPRO].[dbo].[OrganizationContact]
           (idOrganization
           ,contactNumber
           ,firstName
           ,lastName
           ,middleInitial
           ,nameAbbreviation
           ,title
           ,statusCd
           ,statusDt)
SELECT
            idOrganization
           ,9002
           ,'Foomay'
           ,'Ting'
           ,''
           ,'FTING'
           ,''
           ,0
           ,GETDATE()
  FROM [MINERALPRO].[dbo].[Organization] OC
 WHERE OC.organizationName = 'CamaPro'


INSERT INTO [MINERALPRO].[dbo].[AppUser]
           (idOrganizationContact
           ,emailAddress
           ,password
           ,nameAbbreviation)
SELECT
            OC.idOrganizationContact
           ,'fmting@gmail.com'
           ,'$2a$10$mpyuH8NJus06DsQEkOh7Ru3M8DAS1u94st53It3lEdW4vYuLKD22S'
           ,OC.nameAbbreviation
  FROM [MINERALPRO].[dbo].[Organization] O
 INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.idOrganization = O.idOrganization
   AND OC.nameAbbreviation = 'FTING'
 WHERE O.organizationName = 'CamaPro'


INSERT INTO [MINERALPRO].[dbo].[OrganizationContact]
           (idOrganization
           ,contactNumber
           ,firstName
           ,lastName
           ,middleInitial
           ,nameAbbreviation
           ,title
           ,statusCd
           ,statusDt)
SELECT
            idOrganization
           ,9003
           ,'James'
           ,'Ting'
           ,''
           ,'JTING'
           ,''
           ,0
           ,GETDATE()
  FROM [MINERALPRO].[dbo].[Organization] OC
 WHERE OC.organizationName = 'CamaPro'


INSERT INTO [MINERALPRO].[dbo].[AppUser]
           (idOrganizationContact
           ,emailAddress
           ,password
           ,nameAbbreviation)
SELECT
            OC.idOrganizationContact
           ,'james@cama.pro'
           ,'$2a$10$SXlFBjAhxAh4A3gHKbYziOO6O4xOobqhPSC9H0d/foJ7sqHOcancO'
           ,OC.nameAbbreviation
  FROM [MINERALPRO].[dbo].[Organization] O
 INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.idOrganization = O.idOrganization
   AND OC.nameAbbreviation = 'JTING'
 WHERE O.organizationName = 'CamaPro'


INSERT INTO [MINERALPRO].[dbo].[UserOrganization]
           (idAppUser
           ,idOrganization)
SELECT
            AU.idAppUser
           ,O.idOrganization
  FROM [MINERALPRO].[dbo].[Organization] O
  INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.idOrganization = O.idOrganization
  INNER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.idOrganizationContact = OC.idOrganizationContact
 WHERE O.organizationName = 'CamaPro'


INSERT INTO [MINERALPRO].[dbo].[UserOrgSecurity]
           (idUserOrganization
           ,idSecurityGroup)
SELECT
            UO.idUserOrganization
           ,SG.idSecurityGroup
  FROM [MINERALPRO].[dbo].[Organization] O
  INNER JOIN [MINERALPRO].[dbo].[UserOrganization] UO
    ON UO.idOrganization = O.idOrganization
  INNER JOIN [MINERALPRO].[dbo].[SecurityGroup] SG
    ON SG.groupName = 'IS Admin'
 WHERE O.organizationName = 'CamaPro'


-- SETUP FOR JKCONSULTING ORGANIZATION / USERS / SECURITY

SET IDENTITY_INSERT [dbo].[Organization] ON

INSERT INTO [MINERALPRO].[dbo].[Organization]
           (idOrganization
           ,organizationNumber
           ,organizationName)

VALUES     (3
           ,901
           ,'JK Consulting')

SET IDENTITY_INSERT [dbo].[Organization] OFF


INSERT INTO [MINERALPRO].[dbo].[OrganizationContact]
           (idOrganization
           ,contactNumber
           ,firstName
           ,lastName
           ,middleInitial
           ,nameAbbreviation
           ,title
           ,statusCd
           ,statusDt)
SELECT
            idOrganization
           ,9010
           ,'Kurt'
           ,'Myers'
           ,''
           ,'KMYERS'
           ,''
           ,0
           ,GETDATE()
  FROM [MINERALPRO].[dbo].[Organization] OC
 WHERE OC.organizationName = 'JK Consulting'


INSERT INTO [MINERALPRO].[dbo].[AppUser]
           (idOrganizationContact
           ,emailAddress
           ,password
           ,nameAbbreviation)
SELECT
            OC.idOrganizationContact
           ,'kurt@jkconsultingfirm.com'
           ,'$2a$10$l/m/V418zlTWGDRS8qt7SeFeunrtlLMF5cjFj8mlX91i21rREFbw2'
           ,OC.nameAbbreviation
  FROM [MINERALPRO].[dbo].[Organization] O
 INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.idOrganization = O.idOrganization
   AND OC.nameAbbreviation = 'KMYERS'
 WHERE O.organizationName = 'JK Consulting'


INSERT INTO [MINERALPRO].[dbo].[OrganizationContact]
           (idOrganization
           ,contactNumber
           ,firstName
           ,lastName
           ,middleInitial
           ,nameAbbreviation
           ,title
           ,statusCd
           ,statusDt)
SELECT
            idOrganization
           ,9011
           ,'Jeremy'
           ,'Wilhite'
           ,''
           ,'JWILHITE'
           ,''
           ,0
           ,GETDATE()
  FROM [MINERALPRO].[dbo].[Organization] OC
 WHERE OC.organizationName = 'JK Consulting'


INSERT INTO [MINERALPRO].[dbo].[AppUser]
           (idOrganizationContact
           ,emailAddress
           ,password
           ,nameAbbreviation)
SELECT
            OC.idOrganizationContact
           ,'jeremy@jkconsultingfirm.com'
           ,'$2a$10$UyeuXy0a6sF5uJxtLYZRTeS8jMpiRx7YtubVYpVFq/VoTulckJBjy'
           ,OC.nameAbbreviation
  FROM [MINERALPRO].[dbo].[Organization] O
 INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.idOrganization = O.idOrganization
   AND OC.nameAbbreviation = 'JWILHITE'
 WHERE O.organizationName = 'JK Consulting'


INSERT INTO [MINERALPRO].[dbo].[UserOrganization]
           (idAppUser
           ,idOrganization)
SELECT
            AU.idAppUser
           ,O.idOrganization
  FROM [MINERALPRO].[dbo].[Organization] O
  INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.idOrganization = O.idOrganization
  INNER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.idOrganizationContact = OC.idOrganizationContact
 WHERE O.organizationName = 'JK Consulting'


INSERT INTO [MINERALPRO].[dbo].[UserOrgSecurity]
           (idUserOrganization
           ,idSecurityGroup)
SELECT
            UO.idUserOrganization
           ,SG.idSecurityGroup
  FROM [MINERALPRO].[dbo].[Organization] O
  INNER JOIN [MINERALPRO].[dbo].[UserOrganization] UO
    ON UO.idOrganization = O.idOrganization
  INNER JOIN [MINERALPRO].[dbo].[SecurityGroup] SG
    ON SG.groupName = 'IS Admin'
 WHERE O.organizationName = 'JK Consulting'


-- SETUP FOR TINGKER ORGANIZATION / USERS / SECURITY

SET IDENTITY_INSERT [dbo].[Organization] ON

INSERT INTO [MINERALPRO].[dbo].[Organization]
           (idOrganization
           ,organizationNumber
           ,organizationName)

VALUES     (4
           ,902
           ,'Tingker')

SET IDENTITY_INSERT [dbo].[Organization] OFF


INSERT INTO [MINERALPRO].[dbo].[OrganizationContact]
           (idOrganization
           ,contactNumber
           ,firstName
           ,lastName
           ,middleInitial
           ,nameAbbreviation
           ,title
           ,statusCd
           ,statusDt)
SELECT
            idOrganization
           ,9020
           ,'Rae'
           ,'Test'
           ,''
           ,'RT'
           ,''
           ,0
           ,GETDATE()
  FROM [MINERALPRO].[dbo].[Organization] OC
 WHERE OC.organizationName = 'Tingker'


INSERT INTO [MINERALPRO].[dbo].[AppUser]
           (idOrganizationContact
           ,emailAddress
           ,password
           ,nameAbbreviation)
SELECT
            OC.idOrganizationContact
           ,'rae@gmail.com'
           ,'$2a$10$IA6ACZksGxvPYCNG6SoDcuWqIZkYTb3ECW3lseCL4USoK9YX3ewo6'
           ,OC.nameAbbreviation
  FROM [MINERALPRO].[dbo].[Organization] O
 INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.idOrganization = O.idOrganization
   AND OC.nameAbbreviation = 'RT'
 WHERE O.organizationName = 'Tingker'


INSERT INTO [MINERALPRO].[dbo].[OrganizationContact]
           (idOrganization
           ,contactNumber
           ,firstName
           ,lastName
           ,middleInitial
           ,nameAbbreviation
           ,title
           ,statusCd
           ,statusDt)
SELECT
            idOrganization
           ,9021
           ,'Dc'
           ,'Catti'
           ,''
           ,'DC'
           ,''
           ,0
           ,GETDATE()
  FROM [MINERALPRO].[dbo].[Organization] OC
 WHERE OC.organizationName = 'Tingker'


INSERT INTO [MINERALPRO].[dbo].[AppUser]
           (idOrganizationContact
           ,emailAddress
           ,password
           ,nameAbbreviation)
SELECT
            OC.idOrganizationContact
           ,'dc@yahoo.com'
           ,'$2a$10$.gYEq465fGj.m2i6hKqX7uIYu/PN1Fy2oMl2hTmh/5ZneJ3yV6e6a'
           ,OC.nameAbbreviation
  FROM [MINERALPRO].[dbo].[Organization] O
 INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.idOrganization = O.idOrganization
   AND OC.nameAbbreviation = 'DC'
 WHERE O.organizationName = 'Tingker'


INSERT INTO [MINERALPRO].[dbo].[OrganizationContact]
           (idOrganization
           ,contactNumber
           ,firstName
           ,lastName
           ,middleInitial
           ,nameAbbreviation
           ,title
           ,statusCd
           ,statusDt)
SELECT
            idOrganization
           ,9022
           ,'Dec'
           ,'Cat'
           ,''
           ,'DEC'
           ,''
           ,0
           ,GETDATE()
  FROM [MINERALPRO].[dbo].[Organization] OC
 WHERE OC.organizationName = 'Tingker'


INSERT INTO [MINERALPRO].[dbo].[AppUser]
           (idOrganizationContact
           ,emailAddress
           ,password
           ,nameAbbreviation)
SELECT
            OC.idOrganizationContact
           ,'test@yahoo.com'
           ,'$2a$10$URTPX4L1iVyVkc61fVl//uf1eKr2yQe/KFveEOvx.p4IYWqf2Y.Nq'
           ,OC.nameAbbreviation
  FROM [MINERALPRO].[dbo].[Organization] O
 INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.idOrganization = O.idOrganization
   AND OC.nameAbbreviation = 'DEC'
 WHERE O.organizationName = 'Tingker'


INSERT INTO [MINERALPRO].[dbo].[OrganizationContact]
           (idOrganization
           ,contactNumber
           ,firstName
           ,lastName
           ,middleInitial
           ,nameAbbreviation
           ,title
           ,statusCd
           ,statusDt)
SELECT
            idOrganization
           ,9023
           ,'Juan'
           ,'Tamad'
           ,'S'
           ,'AS'
           ,''
           ,0
           ,GETDATE()
  FROM [MINERALPRO].[dbo].[Organization] OC
 WHERE OC.organizationName = 'Tingker'


INSERT INTO [MINERALPRO].[dbo].[AppUser]
           (idOrganizationContact
           ,emailAddress
           ,password
           ,nameAbbreviation)
SELECT
            OC.idOrganizationContact
           ,'juan@gmail.com'
           ,'$2a$10$DMCVz.OcBNDsGmFbP/.sGecNXQteMg27mZns5GPe.ONDIY5bSXAgK'
           ,OC.nameAbbreviation
  FROM [MINERALPRO].[dbo].[Organization] O
 INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.idOrganization = O.idOrganization
   AND OC.nameAbbreviation = 'AS'
 WHERE O.organizationName = 'Tingker'


INSERT INTO [MINERALPRO].[dbo].[OrganizationContact]
           (idOrganization
           ,contactNumber
           ,firstName
           ,lastName
           ,middleInitial
           ,nameAbbreviation
           ,title
           ,statusCd
           ,statusDt)
SELECT
            idOrganization
           ,9024
           ,'Maria'
           ,'Basa'
           ,'A'
           ,'MBA'
           ,''
           ,0
           ,GETDATE()
  FROM [MINERALPRO].[dbo].[Organization] OC
 WHERE OC.organizationName = 'Tingker'


INSERT INTO [MINERALPRO].[dbo].[AppUser]
           (idOrganizationContact
           ,emailAddress
           ,password
           ,nameAbbreviation)
SELECT
            OC.idOrganizationContact
           ,'dccat@ym.com'
           ,'$2a$10$gmfEUG7LYl0Z949hmuk2b.5yGbeZ1LdHXKh/wRoi9vUi7kR6Pe4hi'
           ,OC.nameAbbreviation
  FROM [MINERALPRO].[dbo].[Organization] O
 INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.idOrganization = O.idOrganization
   AND OC.nameAbbreviation = 'MBA'
 WHERE O.organizationName = 'Tingker'


INSERT INTO [MINERALPRO].[dbo].[UserOrganization]
           (idAppUser
           ,idOrganization)
SELECT
            AU.idAppUser
           ,O.idOrganization
  FROM [MINERALPRO].[dbo].[Organization] O
  INNER JOIN [MINERALPRO].[dbo].[OrganizationContact] OC
    ON OC.idOrganization = O.idOrganization
  INNER JOIN [MINERALPRO].[dbo].[AppUser] AU
    ON AU.idOrganizationContact = OC.idOrganizationContact
 WHERE O.organizationName = 'Tingker'


INSERT INTO [MINERALPRO].[dbo].[UserOrgSecurity]
           (idUserOrganization
           ,idSecurityGroup)
SELECT
            UO.idUserOrganization
           ,SG.idSecurityGroup
  FROM [MINERALPRO].[dbo].[Organization] O
  INNER JOIN [MINERALPRO].[dbo].[UserOrganization] UO
    ON UO.idOrganization = O.idOrganization
  INNER JOIN [MINERALPRO].[dbo].[SecurityGroup] SG
    ON SG.groupName = 'IS Admin'
 WHERE O.organizationName = 'Tingker'


UPDATE [MINERALPRO].[dbo].[AppUser]
   SET
      emailAddress = 'ann.beaver@ectorcad.org'
     ,password = '$2a$10$qiODYsHCMCRlZtJPRyIrA.9/PL2ud3Byj6NK3I3zEAG4Uh0ca4Gp.'
  FROM [MINERALPRO].[dbo].[AppUser]
 WHERE [AppUser].nameAbbreviation = 'ABEAVER'


UPDATE [MINERALPRO].[dbo].[AppUser]
   SET
      emailAddress = 'mindy.moreno@ectorcad.org'
     ,password = '$2a$10$gMpxKi1TxyNl6NRAcwKHgO38ZY0ko.Y.OczUx4FVu85vWlHSV9jhm'
  FROM [MINERALPRO].[dbo].[AppUser]
 WHERE [AppUser].nameAbbreviation = 'MMORENO'


UPDATE [MINERALPRO].[dbo].[AppUser]
   SET
      emailAddress = 'don.tohkubbi@ectorcad.org'
     ,password = '$2a$10$DTCD5js26u5aN56nwXshv.BPaMeEcvY6p7A.3GqFVNCW2kLvcNKMG'
  FROM [MINERALPRO].[dbo].[AppUser]
 WHERE [AppUser].nameAbbreviation = 'TOHKUBD'
		

UPDATE [MINERALPRO].[dbo].[AppUser]
   SET
      emailAddress = 'carlos.ponce@ectorcad.org'
     ,password = '$2a$10$LlbP5KLpKGQfTuuV2PdXUuXtFrS0U2eU9ekXyITwd4oi/FhfUcrnW'
  FROM [MINERALPRO].[dbo].[AppUser]
 WHERE [AppUser].nameAbbreviation = 'CPONCE'