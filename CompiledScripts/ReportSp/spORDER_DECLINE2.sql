-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [MINERALPRO]
GO

DROP PROCEDURE IF EXISTS Dbo.spORDER_DECLINE2
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE Dbo.spORDER_DECLINE2
	-- Add the parameters for the stored procedure here
	@PAPPRAISAL_YR Char(4) = '2017'
	, @PACCOUNT_NUM_FROM Int = 0
	, @PACCOUNT_NUM_TO Int = 9999999
	, @PRRC_NUM_FROM Int = 0
	, @PRRC_NUM_TO Int = 99999999
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT <@Param1, sysname, @p1>, <@Param2, sysname, @p2>

 SELECT ApprDeclineSchedule.declinePercent DECLINE_PCT
 , ApprDeclineSchedule.declineYears DECLINE_YRS
 , ApprDeclineSchedule.appraisalYear APPRAISAL_YR
 , ApprDeclineSchedule.subjectId ACCOUNT_NUM
 , ApprDeclineSchedule.subjectTypeCd SUBJECT_TYPE_CD
 , ApprDeclineSchedule.rrcNumber RRC_NUM
 , ApprDeclineSchedule.declineTypeCd DECLINE_TYP_CD

 FROM   MINERALPRO.dbo.ApprDeclineSchedule  
  WHERE ApprDeclineSchedule.appraisalYear = @PAPPRAISAL_YR
	AND	ApprDeclineSchedule.subjectId Between @PACCOUNT_NUM_FROM And @PACCOUNT_NUM_TO
	AND ApprDeclineSchedule.subjectId Between @PRRC_NUM_FROM And @PRRC_NUM_TO
	AND ApprDeclineSchedule.declineTypeCd = 2

END
GO
