USE [MINERALPRO]
GO

/****** Object:  StoredProcedure [dbo].[spExportPublicRecords]    Script Date: 9/20/2017 9:12:00 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




-- =============================================
-- Author:		Foomay
-- Create date: 2017-09-02
-- Description:	Export public records
-- =============================================
CREATE PROCEDURE [dbo].[spExportPublicRecords] 
	-- Add the parameters for the stored procedure here
	@appraisalYear_input smallint = 2017
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF OBJECT_ID (N'tempdb..#tempOutput', N'U') IS NOT NULL
	  DROP TABLE #tempOutput

	CREATE TABLE #tempOutput (
		outputField nvarchar(3000)
	);

	   
	DECLARE @dblTotalValue  bigint = 0;
	DECLARE @applCount int = 0;
	DECLARE @count int = 0;
	DECLARE @countTotal int = 6;
	DECLARE @appraisalYear smallint;
	DECLARE @strTemp varchar(3000) = '';
	DECLARE @rsTemp varchar(3000) = '';
	DECLARE @isUnit char = 'N';
	DECLARE @ISDPercent int;
	DECLARE @unitISDTotalValue int;
	DECLARE @strWI varchar(30);
	DECLARE @subjectTypeCd smallint 
		,@subjectId int 
		,@rrcNumber int 
		,@fieldId int 
		,@wellDepth int 
		,@gravityCd smallint 
		,@wellDescription varchar(50) 
		,@wellTypeCd smallint 
		,@productionEquipmentCount smallint 
		,@productionEquipmentSchedule smallint 
		,@productionEquipmentValue decimal(13, 0) 
		,@serviceEquipmentCount smallint 
		,@serviceEquipmentSchedule smallint 
		,@serviceEquipmentValue decimal(13, 0) 
		,@injectionEquipmentCount smallint 
		,@injectionEquipmentSchedule smallint 
		,@injectionEquipmentValue decimal(13, 0) 
		,@maxYearLife smallint 
		,@discountRate decimal(4, 3) 
		,@grossOilPrice decimal(5, 2) 
		,@grossProductPrice decimal(5, 2) 
		,@grossWorkingInterestGasPrice decimal(5, 2) 
		,@grossRoyaltyInterestGasPrice decimal(5, 2) 
		,@operatingExpense decimal(13, 0) 
		,@dailyAverageOil decimal(7, 2) 
		,@dailyAverageGas decimal(7, 2) 
		,@dailyAverageProduct decimal(7, 2) 
		,@yearLife smallint 
		,@reserveOilValue decimal(13, 0) 
		,@accumOilProduction bigint 
		,@accumGasProduction bigint 
		,@accumProductProduction bigint 
		,@totalValue decimal(13, 0) 
		,@workingInterestOilValue decimal(13, 0) 
		,@workingInterestGasValue decimal(13, 0) 
		,@workingInterestProductValue decimal(13, 0) 
		,@workingInterestTotalValue decimal(13, 0) 
		,@workingInterestTotalPV decimal(13, 0) 
		,@royaltyInterestOilValue decimal(13, 0) 
		,@royaltyInterestGasValue decimal(13, 0) 
		,@royaltyInterestProductValue decimal(13, 0) 
		,@royaltyInterestTotalValue decimal(13, 0) 
		,@rowDeleteFlag char(1) 
		-- UnitValueSummary Table
		,@weightedWorkingInterestPercent decimal(9, 8)  
		,@weightedRoyaltyInterestPercent decimal(9, 8)
		,@unitId int

		-- LeaseTaxUnit and LeaseValueSummary Table
		,@taxUnitPercent decimal(4,3)
		,@unitWorkingInterestPercent decimal(9, 8)
		,@unitRoyaltyInterestPercent decimal(9, 8)
		,@workingInterestPercent decimal(9, 8)
		,@royaltyInterestPercent decimal(9, 8)
		,@overridingRoyaltyPercent decimal(9, 8)
		,@oilPaymentPercent decimal(9, 8)
		,@leaseId int
		,@leaseName varchar(50)
		,@unitName varchar(50)

		,@declinePercent decimal(4,3)
		,@declineYears smallint
		,@pvFactor decimal(7,6)


	DECLARE rsAppl_cursor CURSOR FOR
		SELECT appraisalYear
			, subjectId
			, subjectTypeCd
			, rrcNumber
			, wellTypeCd
			, workingInterestTotalValue
			, royaltyInterestTotalValue
			, totalValue
			, productionEquipmentCount
			, serviceEquipmentCount
			, injectionEquipmentCount 
			, wellDepth
			, dailyAverageOil
			, dailyAverageGas
			, dailyAverageProduct
			, accumOilProduction
			, accumGasProduction
			, accumProductProduction
			, yearLife
			, operatingExpense 
			, grossOilPrice
			, grossWorkingInterestGasPrice
			, productionEquipmentValue
			, serviceEquipmentValue
			, injectionEquipmentValue
			, discountRate	

		FROM MINERALPRO.dbo.Appraisal 
		WHERE appraisalYear = @appraisalYear_input
		AND   rowDeleteFlag <> 'D' 
		ORDER BY rrcNumber;

	OPEN rsAppl_cursor

	FETCH NEXT FROM rsAppl_cursor
	INTO @appraisalYear
		, @subjectId
		, @subjectTypeCd
		, @rrcNumber
		, @wellTypeCd
		, @workingInterestTotalValue
		, @royaltyInterestTotalValue
		, @totalValue
		, @productionEquipmentCount
		, @serviceEquipmentCount
		, @injectionEquipmentCount 
		, @wellDepth
		, @dailyAverageOil
		, @dailyAverageGas
		, @dailyAverageProduct
		, @accumOilProduction
		, @accumGasProduction
		, @accumProductProduction
		, @yearLife
		, @operatingExpense 
		, @grossOilPrice
		, @grossWorkingInterestGasPrice
		, @productionEquipmentValue
		, @serviceEquipmentValue
		, @injectionEquipmentValue
		, @discountRate
	
	 WHILE @@FETCH_STATUS = 0
	 BEGIN
		SET @dblTotalValue += @totalValue;
		SET @applCount += 1;
		SET @strTemp = '068901068'

		IF (@subjectTypeCd = 1)  
			SET @isUnit = 'Y';
		ELSE
			SET @isUnit = 'N';	 
    
	  -- -- 1. SUBJECT ID
	   IF (@isUnit = 'Y')  
		  SET @strTemp = @strTemp 
						+ MINERALPRO.dbo.lpad(@subjectId, 12, ' ')  
						+ MINERALPRO.dbo.lpad(@subjectId, 10, ' ');
	   ELSE
		  SET @strTemp += MINERALPRO.dbo.lpad(@subjectId, 12, ' ') + space(10);
 
	   -- 2. WELL TYPE CODE
	   IF (@wellTypeCd = 1)  
			SET @strTemp += 'O';
	   ELSE 
		   BEGIN
			   if (@wellTypeCd = 2)  
					SET @strTemp += 'G';
				ELSE
   					SET @strTemp += ' ';
		   END
	  -- END IF WellTypeCd
	  -- -- 3. RRC 
	   SET @strTemp += ' 08' + FORMAT(@rrcNumber, '0#####');
 
	   --********************************************************************************
	   --- 4. Process UNIT record
	   --********************************************************************************
	   IF (@isUnit = 'Y')  
	   BEGIN
 			DECLARE rsTemp_cursor CURSOR FOR
				SELECT A.weightedWorkingInterestPercent
					  ,A.weightedRoyaltyInterestPercent 
					  ,A.unitId
					  ,B.unitName
					  ,A.appraisalYear
				FROM MINERALPRO.dbo.UnitValueSummary  A
				INNER JOIN  MINERALPRO.dbo.Unit B
					ON A.appraisalYear = B.appraisalYear
					AND A.unitId = B.unitId
				WHERE A.appraisalYear = @appraisalYear_input
					AND A.unitId =  @subjectId

 			OPEN rsTemp_cursor
			FETCH NEXT FROM rsTemp_cursor INTO
				 @weightedWorkingInterestPercent
				,@weightedRoyaltyInterestPercent
				,@unitId
				,@unitName
				,@appraisalYear

			IF @@FETCH_STATUS <> 0   -- EOF 
			BEGIN
			   SET @strWI = '000000000';
			   SET @strTemp += '000000000000000000';
			END
		 
			WHILE @@FETCH_STATUS = 0   -- Loop thru all the records
			BEGIN
				SET @strWI = FORMAT(@weightedWorkingInterestPercent * 100000000, '0########');
				SET @strTemp += @strWI + FORMAT(@weightedRoyaltyInterestPercent * 100000000, '0########');
			 
				FETCH NEXT FROM rsTemp_cursor INTO 
					 @weightedWorkingInterestPercent,@weightedRoyaltyInterestPercent
					,@unitId,@unitName, @appraisalYear;
				
			END
			--********************************************************************************
			---- CALCULATE ISD TOTAL  
			--********************************************************************************
			DECLARE rsTemp2_cursor CURSOR FOR
			SELECT B.taxUnitPercent, C.unitWorkingInterestPercent
				 , C.unitRoyaltyInterestPercent, C.workingInterestPercent
				 , C.royaltyInterestPercent, C.overridingRoyaltyPercent
				 , C.oilPaymentPercent 

				FROM MINERALPRO.dbo.Lease A 
				INNER JOIN MINERALPRO.dbo.LeaseTaxUnit B  
					ON A.appraisalYear = B.appraisalYear 
						AND A.LeaseId = B.LeaseId 
						AND B.taxUnitTypeCd = 1070
						AND B.rowDeleteFlag <> 'D'  
				INNER JOIN MINERALPRO.dbo.LeaseValueSummary C
					ON A.LeaseId = C.LeaseId
					AND A.appraisalYear = C.appraisalYear 
			 WHERE A.appraisalYear =  @appraisalYear_input
				AND A.unitId = @subjectId
				AND A.rowDeleteFlag <> 'D' 
					
			OPEN rsTemp2_cursor
			FETCH NEXT FROM rsTemp2_cursor INTO	 
				@taxUnitPercent, @unitWorkingInterestPercent, @unitRoyaltyInterestPercent
				, @workingInterestPercent, @royaltyInterestPercent, @overridingRoyaltyPercent
				, @oilPaymentPercent 
  		
			SET @unitISDTotalValue = 0

			IF @@FETCH_STATUS <> 0 -- EOF of rsTemp_cursor ***
				SET @strTemp += space(40);

			WHILE @@FETCH_STATUS = 0   -- Loop thru all the records In LeaseValueSummary 
			BEGIN	
				-- calc the wi value for each lease within a school district
				If @workingInterestTotalValue <> 0 And @weightedWorkingInterestPercent <> 0  
				   SET @unitISDTotalValue +=  MINERALPRO.dbo.udfBankRound((@workingInterestTotalValue / @weightedWorkingInterestPercent 
							 * @unitWorkingInterestPercent * @workingInterestPercent * @taxUnitPercent), 0)
           
            
				 --  ' calc the ri value for each lease within a school district
				If @royaltyInterestTotalValue <> 0 And @weightedRoyaltyInterestPercent <> 0  
				   SET @unitISDTotalValue += MINERALPRO.dbo.udfBankRound((@royaltyInterestTotalValue / @weightedRoyaltyInterestPercent 
							* @unitRoyaltyInterestPercent 
							* (@royaltyInterestPercent + @overridingRoyaltyPercent + @oilPaymentPercent) 
							* @taxUnitPercent), 0)
            
           
				--PRINT 'Unit : ' + STR(@unitId) + ' TOTAL: ' + STR(@unitISDTotalValue);
				FETCH NEXT FROM rsTemp2_cursor INTO 
					  @taxUnitPercent, @unitWorkingInterestPercent, @unitRoyaltyInterestPercent
					, @workingInterestPercent, @royaltyInterestPercent, @overridingRoyaltyPercent
					, @oilPaymentPercent
 			END

			CLOSE rsTemp2_cursor;
			DEALLOCATE rsTemp2_cursor;

			--********************************************************************************
			---- END of CALCULATE ISD TOTAL  
			--********************************************************************************

			IF @totalValue <> 0 AND @unitISDTotalValue <> 0  
			  BEGIN
				SET @ISDPercent = MINERALPRO.dbo.udfBankRound((@unitISDTotalValue / @totalValue), 6)
				If @ISDPercent > 1  
				   SET @ISDPercent = 1

				SET @strTemp += FORMAT((@ISDPercent * 1000000), '0######');
			  END
			ELSE
				SET @strTemp += '0000000';
			-- End if totalValue <> 0 
			SET @strTemp +=  MINERALPRO.dbo.lpad(STR(@totalValue), 11, ' ')  
						 +   MINERALPRO.dbo.lpad(STR(MINERALPRO.dbo.udfBankRound(@totalValue * @ISDPercent, 0)), 11, ' ');
			SET @strTemp += MINERALPRO.dbo.lpad(@unitName, 40, ' '); 
			--PRINT 'UNIT : ' + @strTemp;
		

			CLOSE rsTemp_cursor;
			DEALLOCATE rsTemp_cursor;
	   END
	   ELSE
   			--********************************************************************************
			---- PROCESS a LEASE record 
			--********************************************************************************
	   BEGIN
   			DECLARE rsTemp_cursor CURSOR FOR
				SELECT A.leaseId, B.leaseName
					 , A.workingInterestPercent, A.royaltyInterestPercent
					 , A.oilPaymentPercent, A.overridingRoyaltyPercent	
				FROM MINERALPRO.dbo.LeaseValueSummary  A
				INNER JOIN MINERALPRO.dbo.Lease B
					ON  A.appraisalYear = B.appraisalYear
					AND A.leaseId = B.leaseId
				WHERE   A.appraisalYear = @appraisalYear_input
					AND A.leaseId =  @subjectId
 			OPEN rsTemp_cursor
			FETCH NEXT FROM rsTemp_cursor INTO
				@leaseId, @leaseName, @workingInterestPercent ,@royaltyInterestPercent, @oilPaymentPercent, @overridingRoyaltyPercent	
		
			IF @@FETCH_STATUS <> 0 -- EOF
			BEGIN
				SET @strWI = '000000000';
				SET @strTemp += '000000000000000000';
			END

			WHILE @@FETCH_STATUS = 0   -- Loop thru all the records
			BEGIN
				SET @strWI = FORMAT(@workingInterestPercent *100000000, '0########');
				SET @strTemp += @strWI + FORMAT((@royaltyInterestPercent + @oilPaymentPercent + @overridingRoyaltyPercent)* 100000000, '0########');
				FETCH NEXT FROM rsTemp_cursor INTO 
					@leaseId, @leaseName, @workingInterestPercent ,@royaltyInterestPercent, @oilPaymentPercent, @overridingRoyaltyPercent;
			END
 
			--********************************************************************************
			--  CALCULATE ISD TOTAL 
			--********************************************************************************

   			DECLARE rsTemp2_cursor CURSOR FOR
			SELECT taxUnitPercent
				FROM MINERALPRO.dbo.LeaseTaxUnit  
					 WHERE appraisalYear = @appraisalYear_input
						 AND  leaseId =  @subjectId
						 AND  taxUnitTypeCd = 1070
						 AND  rowDeleteFlag <> 'D'
 			OPEN rsTemp2_cursor
			FETCH NEXT FROM rsTemp2_cursor INTO @taxUnitPercent

  			IF @@FETCH_STATUS <> 0 -- EOF of rsTemp_cursor ***
				SET @strTemp += space(40);

			WHILE @@FETCH_STATUS = 0   -- Loop thru all the records
			BEGIN
				SET @ISDPercent = @taxUnitPercent;
				IF @ISDPercent > 1 
					SET @ISDPercent = 1;
				SET @strTemp += FORMAT(@ISDPercent * 1000000, '0######');
				FETCH NEXT FROM rsTemp2_cursor INTO @taxUnitPercent;	 
			END
			-- CHECK THIS, in original script, but not right
			--IF @@FETCH_STATUS <> 0 -- EOF
			--  SET @strTemp += '0000000';
	
			SET @strTemp +=  MINERALPRO.dbo.lpad(STR(@totalValue), 11, ' ')  
						 +   MINERALPRO.dbo.lpad(STR(MINERALPRO.dbo.udfBankRound(@totalValue * @ISDPercent, 0)), 11, ' ');
			SET @strTemp += MINERALPRO.dbo.lpad(RTRIM(@leaseName), 40, ' '); 
			--PRINT 'LEASE: ' + @strTemp;

			CLOSE rsTemp2_cursor;
			DEALLOCATE rsTemp2_cursor;

			CLOSE rsTemp_cursor;
			DEALLOCATE rsTemp_cursor;
	   END 
		--********************************************************************************
		--  PROCESS EQUIPMENT COUNT AND DAILY AVERAGE
		--********************************************************************************
  
 		SET @strTemp += @strWI 
				+ MINERALPRO.dbo.lpad(@productionEquipmentCount + @serviceEquipmentCount + @injectionEquipmentCount, 4, ' ')
				+ MINERALPRO.dbo.lpad(@productionEquipmentCount, 4, ' ')
				+ MINERALPRO.dbo.lpad(@injectionEquipmentCount, 4, ' ')
				+ MINERALPRO.dbo.lpad(0, 4, ' ')    
				+ MINERALPRO.dbo.lpad(@wellDepth, 5, ' ');
   
   
		SET @strTemp += 
			CASE  @wellTypeCd   
				WHEN 1 THEN   MINERALPRO.dbo.lpad(FORMAT(MINERALPRO.dbo.udfBankRound(@dailyAverageOil * 30, 2), '#########0'), 10, space(1)) 
				WHEN 2 THEN   MINERALPRO.dbo.lpad(FORMAT(MINERALPRO.dbo.udfBankRound(@dailyAverageGas * 30, 0), '#########0'), 10, space(1)) 
				ELSE		 space(10)
			END;
 		SET @strTemp += '000000000';
		-- so far so good 10100
		-- 068901068       10100          O 080002100796875000203125001000000    3758506    3758506                      CUMMINS -12- (H E)079687500   2   2   0   0 8500      1734000000000

   		--********************************************************************************
		--  PROCESS DECLINE TYPE CODE based on WELL TYPE
		--******************************************************************************** 
		DECLARE @declineTypeCode int;
		SET @declineTypeCode = 
			CASE  @wellTypeCd   
				WHEN 1 THEN 2   -- OIL
				WHEN 2 THEN 3   -- GAS
				ELSE 0			-- OTHERS
			END;
	 
		IF (@wellTypeCd = 1 OR  @wellTypeCd = 2)
		BEGIN 
			DECLARE rsTemp_cursor CURSOR FOR
			SELECT declinePercent, declineYears   
				FROM MINERALPRO.dbo.apprDeclineSchedule  
				WHERE appraisalYear = @appraisalYear_input
					AND  subjectId =  @subjectId
					AND  rrcNumber  = @rrcNumber
					AND  declineTypeCd = @declineTypeCode  -- OIL DECLINE code
					AND  rowDeleteFlag <> 'D'
				ORDER BY seqNumber
 			OPEN rsTemp_cursor
			FETCH NEXT FROM rsTemp_cursor INTO @declinePercent, @declineYears

			---********************* starating from here 
			--- FORMAT not working
			--- ************************
			SET @count = 0;
 			WHILE @count <  6
			BEGIN 
				SET @count += 1;
  				IF @@FETCH_STATUS = 0 -- EOF of rsTemp_cursor ***
					BEGIN   
 						SET @strTemp += MINERALPRO.dbo.lpad(FORMAT((@declinePercent * 10000), '#####'), 5, ' ') 
								+  FORMAT(@declineYears * 10, '0##');
						FETCH NEXT FROM rsTemp_cursor INTO @declinePercent, @declineYears;
					END
				ELSE  -- EOF -- continue to write till the 6th times
					SET @strTemp += ' 0000000'; 
			END    -- END OF 6 count of decline percents  

			CLOSE rsTemp_cursor;
			DEALLOCATE rsTemp_cursor;
 
			IF @wellTypeCd = 1 
			   BEGIN   -- THIS IS OIL
				  if @dailyAverageOil <> 0 And @dailyAverageGas <> 0  
					 SET @strTemp +=  Format(@dailyAverageOil / @dailyAverageGas, '0##.##0'); 
				  ELSE
					 SET @strTemp += '0000000';

				  SET @strTemp += MINERALPRO.dbo.lpad(FORMAT(@accumOilProduction, '#########0'), 10, ' ');					
			   END
			ELSE
			   BEGIN	-- THIS IS GAS
		   		  if @dailyAverageOil <> 0 And @dailyAverageGas <> 0  
					 SET @strTemp += Format(@dailyAverageGas / @dailyAverageOil, '0##.##0'); 
				  ELSE
					 SET @strTemp += '0000000';

				  SET @strTemp += MINERALPRO.dbo.lpad(FORMAT(@accumGasProduction,'#########0'), 10, ' ');
			   END
     
		END
	   ELSE
		BEGIN
			--- Not GAS or OIL, just iterate ' 0000000' 6 times
			--- and fill the rest with equal amount of spaces and zeros
			SET @strTemp += ' 0000000 0000000 0000000 0000000 0000000 0000000' 
						 +  '0000000'  + space(10);
		END
   
	   SET @strTemp += FORMAT(@yearLife* 10,'0##');
 
	   ----------------******************************************************
	   --   if the well type is oil, get the gas avg as the seconday type
	   ----------------******************************************************
        
	   SET @strTemp += 
			CASE  @wellTypeCd   
				WHEN 1 THEN   MINERALPRO.dbo.lpad(FORMAT(MINERALPRO.dbo.udfBankRound(@dailyAverageGas * 30, 2), '#########0'), 10, space(1)) 
				WHEN 2 THEN   MINERALPRO.dbo.lpad(FORMAT(MINERALPRO.dbo.udfBankRound(@dailyAverageOil * 30, 0), '#########0'), 10, space(1)) 
				ELSE		 space(10)
			END;
     
	   SET @strTemp += '000000000';
	   ----------------******************************************************
	   -- RESET the declineTypeCode
	   --   if the well type is oil then get the gas as secondary type
	   ----------------******************************************************
	   SET @declineTypeCode = 
			CASE  @wellTypeCd   
				WHEN 1 THEN 3   -- OIL type use GAS as secondary
				WHEN 2 THEN 2   -- GAS type use OIL as secondary
				ELSE 0			-- OTHERS
			END;    

	   --------------------***************************************************
		IF (@wellTypeCd = 1 OR  @wellTypeCd = 2)
		BEGIN 
			DECLARE rsTemp_cursor CURSOR FOR
			SELECT declinePercent, declineYears   
				FROM MINERALPRO.dbo.apprDeclineSchedule  
				WHERE appraisalYear = @appraisalYear_input
					AND  subjectId =  @subjectId
					AND  rrcNumber  = @rrcNumber
					AND  declineTypeCd = @declineTypeCode  -- OIL DECLINE code
					AND  rowDeleteFlag <> 'D'
				ORDER BY seqNumber
 			OPEN rsTemp_cursor
			FETCH NEXT FROM rsTemp_cursor INTO @declinePercent, @declineYears

			SET @count = 0;
 			WHILE @count <  6
			BEGIN 
				SET @count += 1;
  				IF @@FETCH_STATUS = 0 -- EOF of rsTemp_cursor ***
					BEGIN  --  ERROR HERE CHECK
 						SET @strTemp += MINERALPRO.dbo.lpad(FORMAT((@declinePercent * 10000), '#####'), 5, ' ') 
								+  FORMAT(@declineYears * 10, '0##');
						FETCH NEXT FROM rsTemp_cursor INTO @declinePercent, @declineYears;
					END
				ELSE  -- EOF -- continue to write till the 6th times
					SET @strTemp += ' 0000000'; 
			END    -- END OF 6 count of decline percents  

			CLOSE rsTemp_cursor;
			DEALLOCATE rsTemp_cursor;
 
			IF @wellTypeCd = 1  -- OIL TYPE
			   SET @strTemp += MINERALPRO.dbo.lpad(FORMAT(@accumGasProduction, '#########0'), 10, ' ');					
			ELSE
			   SET @strTemp += MINERALPRO.dbo.lpad(FORMAT(@accumOilProduction,'#########0'), 10, ' ');   
		END
	   ELSE
		BEGIN
			--- Not GAS or OIL, just iterate ' 0000000' 6 times
			--- and fill the rest with equal amount of spaces and zeros
			SET @strTemp += ' 0000000 0000000 0000000 0000000 0000000 0000000' 
						 +   space(10);
		END
	   SET @strTemp += MINERALPRO.dbo.lpad(FORMAT(MINERALPRO.dbo.udfBankRound(@dailyAverageProduct * 30, 2), '#########0'), 10, ' ');
   
	   ----------------*******************************************************
	   --- PRODUCTION 
	   ----------------*******************************************************
	   SET @declineTypeCode = 2;  -- OIL TYPE
	   DECLARE rsTemp_cursor CURSOR FOR
	   SELECT declinePercent, declineYears   
			FROM MINERALPRO.dbo.apprDeclineSchedule  
			WHERE appraisalYear = @appraisalYear_input
				AND  subjectId =  @subjectId
				AND  rrcNumber  = @rrcNumber
				AND  declineTypeCd = @declineTypeCode  -- OIL DECLINE code
				AND  rowDeleteFlag <> 'D'
			ORDER BY seqNumber
	   OPEN rsTemp_cursor
	   FETCH NEXT FROM rsTemp_cursor INTO @declinePercent, @declineYears

		---*********************  
		---  LOOP 6 times
		--- ************************
		SET @count = 0;
		SET @countTotal = 6;
 		WHILE @count <  @countTotal
		BEGIN 
			SET @count += 1;
  			IF @@FETCH_STATUS = 0 -- EOF of rsTemp_cursor ***
				BEGIN  --  ERROR HERE CHECK
 					SET @strTemp += MINERALPRO.dbo.lpad(FORMAT((@declinePercent * 10000), '#####'), 5, ' ') 
							+  FORMAT(@declineYears * 10, '0##');
					FETCH NEXT FROM rsTemp_cursor INTO @declinePercent, @declineYears;
				END
			ELSE  -- EOF -- continue to write till the 6th times
				SET @strTemp += ' 0000000'; 
		END    -- END OF 6 count of decline percents  

		CLOSE rsTemp_cursor;
		DEALLOCATE rsTemp_cursor;

		SET @strTemp += MINERALPRO.dbo.lpad(FORMAT(@accumProductProduction, '#########0'), 10, ' ');  
   		SET @strTemp += MINERALPRO.dbo.lpad(FORMAT(@operatingExpense, '#######'), 7, ' ');  

		DECLARE @equipmentCount int;
		SET @equipmentCount = @productionEquipmentCount + @serviceEquipmentCount + @injectionEquipmentCount;
		IF @operatingExpense <> 0 AND @equipmentCount <> 0  
			 SET @strTemp += MINERALPRO.dbo.lpad(FORMAT(MINERALPRO.dbo.udfBankRound(@operatingExpense / @equipmentCount, 0), '######0'), 7, ' ');  
		ELSE
			 SET @strTemp += space(6) + '0';
	
		--------------******************************************************
		-- GROSS OIL 
		--------------******************************************************
	   SET @declineTypeCode = 2;  -- OIL TYPE
	   DECLARE rsTemp_cursor CURSOR FOR
	   SELECT declinePercent, declineYears   
			FROM MINERALPRO.dbo.apprDeclineSchedule  
			WHERE appraisalYear = @appraisalYear_input
				AND  subjectId =  @subjectId
				AND  rrcNumber  = @rrcNumber
				AND  declineTypeCd = @declineTypeCode  -- OIL DECLINE code
				AND  rowDeleteFlag <> 'D'
			ORDER BY seqNumber
	   OPEN rsTemp_cursor
	   FETCH NEXT FROM rsTemp_cursor INTO @declinePercent, @declineYears

		---*********************  
		---  LOOP 4 times
		--- ************************
		SET @count = 0;
		--SET @countTotal = 4;
 		WHILE @count <  4
		BEGIN 
			SET @count += 1;
  			IF @@FETCH_STATUS = 0  
				BEGIN   
 					SET @strTemp += MINERALPRO.dbo.lpad(FORMAT((@declinePercent * 10000), '#####'), 5, ' ') 
							+  FORMAT(@declineYears * 10, '0##');
					FETCH NEXT FROM rsTemp_cursor INTO @declinePercent, @declineYears;
				END
			ELSE  -- EOF -- continue to write till the 6th times
				SET @strTemp += ' 0000000'; 
		END    -- END OF 6 count of decline percents  

		CLOSE rsTemp_cursor;
		DEALLOCATE rsTemp_cursor;
 		SET @strTemp += MINERALPRO.dbo.lpad(FORMAT(@grossOilPrice*100, '###0'), 4, ' ');  
		------- NO SPECIAL GRAVITY, just pad with 3 space, contract prices and plant value with 17 space
		SET @strTemp += space(3) + MINERALPRO.dbo.lpad(FORMAT(@grossWorkingInterestGasPrice*100, '###0'), 4, ' ') 
					 + space(17);

		DECLARE @equipValue decimal(13,0);
		SET @equipValue = @productionEquipmentValue + @serviceEquipmentValue + @injectionEquipmentValue;
		SET @strTemp +=  MINERALPRO.dbo.lpad(FORMAT(@productionEquipmentValue + @serviceEquipmentValue + @injectionEquipmentValue, '##########'), 10, ' ');  
		SET @strTemp += space(10);  -- plugging cost


		--------------******************************************************   
		--Loop thru the Equipment PV Table
		----------------****************************************************** 
	  DECLARE rsTemp_cursor CURSOR FOR
	  SELECT factor   
			FROM MINERALPRO.dbo.CalcEquipmentPVSchedule
			WHERE    appraisalYear = @appraisalYear_input
				AND  yearNumber = @yearLife
		 
	   OPEN rsTemp_cursor
	   FETCH NEXT FROM rsTemp_cursor INTO @pvFactor

		---*********************  
		---  Equipment PV Schedule
		--- ************************

  		IF @@FETCH_STATUS = 0  
			SET @strTemp += MINERALPRO.dbo.lpad(FORMAT(MINERALPRO.dbo.udfBankRound(@pvFactor*@equipValue, 0), '#########0'), 10, ' ');   
		ELSE
			SET @strTemp += space(10);
	  
		CLOSE rsTemp_cursor;
		DEALLOCATE rsTemp_cursor;
		SET @strTemp += FORMAT(@discountRate * 100, '0###')  + space(40);   -- capital expenditures 40 space
 	
		--PRINT @strTemp;
		INSERT INTO #tempOutput VALUES (@strTemp)


	   -----------------******************************************************   
	   -- Loop thru the Appraisal Table, get the next record
	   -----------------******************************************************      
	   FETCH NEXT FROM rsAppl_cursor INTO  @appraisalYear
		, @subjectId
		, @subjectTypeCd
		, @rrcNumber
		, @wellTypeCd
		, @workingInterestTotalValue
		, @royaltyInterestTotalValue
		, @totalValue
		, @productionEquipmentCount
		, @serviceEquipmentCount
		, @injectionEquipmentCount 
		, @wellDepth
		, @dailyAverageOil
		, @dailyAverageGas
		, @dailyAverageProduct
		, @accumOilProduction
		, @accumGasProduction
		, @accumProductProduction
		, @yearLife
		, @operatingExpense 
		, @grossOilPrice
		, @grossWorkingInterestGasPrice
		, @productionEquipmentValue
		, @serviceEquipmentValue
		, @injectionEquipmentValue
		, @discountRate

	 END  -- END OF reAppl_cursor Loop
	 CLOSE rsAppl_cursor;
	 DEALLOCATE rsAppl_cursor;  

	SELECT * FROM #tempOutput;
	DROP TABLE #tempOutput;
END




GO


