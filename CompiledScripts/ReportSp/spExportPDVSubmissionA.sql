USE [MINERALPRO]
GO

/****** Object:  StoredProcedure [dbo].[spExportPDVSubmissionA]    Script Date: 9/20/2017 9:11:30 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO





-- =============================================
-- Author:		Foomay
-- Create date: 2017-09-02
-- Description:	Export public records
-- =============================================
CREATE PROCEDURE [dbo].[spExportPDVSubmissionA] 
	-- Add the parameters for the stored procedure here
	@appraisalYear_input smallint = 2017
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF OBJECT_ID (N'tempdb..#tempOutput', N'U') IS NOT NULL
	  DROP TABLE #tempOutput

	CREATE TABLE #tempOutput (
		outputField nvarchar(3000)
	);

	---- query starts here   
 DECLARE @blnCertified char = 'Y';   
 DECLARE @dblTotalValue  bigint = 0;
 DECLARE @UnitISDTotalValue bigint = 0;
 DECLARE @UnitCNTYTotalValue bigint = 0;
 DECLARE @applCount int = 0;
 DECLARE @ISD_TAX_JURIS smallint = 1070;
 DECLARE @COUNTY_TAX_JURIS smallint = 1060;
 DECLARE @strTemp varchar(3000) = '';
 DECLARE @rsTemp varchar(3000) = '';
 DECLARE @UNIT_TYPE smallint = 1;
 DECLARE @LEASE_TYPE smallint = 2;
 DECLARE @isUnit char = 'N';
 DECLARE @ISDPercent int;
 DECLARE @CountyPercent int;

 DECLARE @appraisalYear smallint
		,@subjectTypeCd smallint 
		,@subjectId int 
		,@rrcNumber int 
		,@fieldId int 
		,@wellDepth int 
		,@gravityCd smallint 
		,@wellDescription varchar(50) 
		,@wellTypeCd smallint 
		,@productionEquipmentCount smallint 
		,@productionEquipmentSchedule smallint 
		,@productionEquipmentValue decimal(13, 0) 
		,@serviceEquipmentCount smallint 
		,@serviceEquipmentSchedule smallint 
		,@serviceEquipmentValue decimal(13, 0) 
		,@injectionEquipmentCount smallint 
		,@injectionEquipmentSchedule smallint 
		,@injectionEquipmentValue decimal(13, 0) 
		,@maxYearLife smallint 
		,@discountRate decimal(4, 3) 
		,@grossOilPrice decimal(5, 2) 
		,@grossProductPrice decimal(5, 2) 
		,@grossWorkingInterestGasPrice decimal(5, 2) 
		,@grossRoyaltyInterestGasPrice decimal(5, 2) 
		,@operatingExpense decimal(13, 0) 
		,@dailyAverageOil decimal(7, 2) 
		,@dailyAverageGas decimal(7, 2) 
		,@dailyAverageProduct decimal(7, 2) 
		,@yearLife smallint 
		,@reserveOilValue decimal(13, 0) 
		,@accumOilProduction bigint 
		,@accumGasProduction bigint 
		,@accumProductProduction bigint 
		,@totalValue decimal(13, 0) 
		,@workingInterestOilValue decimal(13, 0) 
		,@workingInterestGasValue decimal(13, 0) 
		,@workingInterestProductValue decimal(13, 0) 
		,@workingInterestTotalValue decimal(13, 0) 
		,@workingInterestTotalPV decimal(13, 0) 
		,@royaltyInterestOilValue decimal(13, 0) 
		,@royaltyInterestGasValue decimal(13, 0) 
		,@royaltyInterestProductValue decimal(13, 0) 
		,@royaltyInterestTotalValue decimal(13, 0) 
		,@rowDeleteFlag char(1) 

		-- UnitValueSummary Table
		,@weightedWorkingInterestPercent decimal(9, 8)  
		,@weightedRoyaltyInterestPercent decimal(9, 8)
		,@unitId int
		,@unitName varchar(50)

		-- LeaseTaxUnit and LeaseValueSummary Table
		,@taxUnitPercent decimal(4,3)
		,@unitWorkingInterestPercent decimal(9, 8)
		,@unitRoyaltyInterestPercent decimal(9, 8)
		,@workingInterestPercent decimal(9, 8)
		,@royaltyInterestPercent decimal(9, 8)
		,@overridingRoyaltyPercent decimal(9, 8)
		,@oilPaymentPercent decimal(9, 8)
		,@leaseId int
		,@leaseName varchar(50)

		,@WGT_WI_PCT decimal(9, 8)
		,@WGT_RI_PCT decimal(9, 8)
		,@LEASE_WI_PCT  decimal(9, 8)
		,@JURIS_PCT_ISD  decimal(9, 8)
		,@JURIS_PCT_CNT  decimal(9, 8)
		,@ACCOUNT_NAME varchar(50)

		 -- LeaseTaxUnit and LeaseValueSummary Table
		,@X_unitId int
		,@X_taxUnitPercent decimal(4,3)
		,@X_unitWorkingInterestPercent decimal(9, 8)
		,@X_unitRoyaltyInterestPercent decimal(9, 8)
		,@X_workingInterestPercent decimal(9, 8)
		,@X_royaltyInterestPercent decimal(9, 8)
		,@X_overridingRoyaltyPercent decimal(9, 8)
		,@X_oilPaymentPercent decimal(9, 8)
 

---**************************************************************************
-- 2.Other Appraisal Table
---**************************************************************************
  DECLARE rsAppl_cursor CURSOR FOR
  SELECT  A.appraisalYear
		, A.subjectId
		, A.subjectTypeCd
		, A.rrcNumber
		, A.wellTypeCd
		, A.workingInterestTotalValue
		, A.royaltyInterestTotalValue
		, A.totalValue
		, A.productionEquipmentCount
		, A.serviceEquipmentCount
		, A.injectionEquipmentCount 
		, A.wellDepth
		, A.dailyAverageOil
		, A.dailyAverageGas
		, A.dailyAverageProduct
		, A.accumOilProduction
		, A.accumGasProduction
		, A.accumProductProduction
		, A.yearLife
		, A.operatingExpense 
		, A.grossOilPrice
		, A.grossWorkingInterestGasPrice
		, A.productionEquipmentValue
		, A.serviceEquipmentValue
		, A.injectionEquipmentValue
		, A.discountRate	
		, ISNULL(B.weightedWorkingInterestPercent,0) AS WGT_WI_PCT
		, ISNULL(B.weightedRoyaltyInterestPercent,0) AS WGT_RI_PCT 
		, ISNULL(C.workingInterestPercent,0) AS LEASE_WI_PCT  
		, ISNULL(D.taxUnitPercent,0) AS JURIS_PCT_ISD  
		, ISNULL(E.taxUnitPercent,0) AS JURIS_PCT_CNT  
		, CASE A.subjectTypeCd 
				WHEN @UNIT_TYPE THEN ISNULL(B2.unitName,' ')  -- UNIT
				WHEN @LEASE_TYPE THEN ISNULL(C2.leaseName,' ') -- LEASE
				ELSE ' ' END AS ACCOUNT_NAME

   FROM MINERALPRO.dbo.Appraisal A 
    LEFT OUTER JOIN MINERALPRO.dbo.UnitValueSummary B  
            ON B.appraisalYear = A.appraisalYear  
            AND B.unitId = A.subjectId  
            AND B.rowDeleteFlag <> 'D' 
		
	LEFT OUTER JOIN MINERALPRO.dbo.Unit B2  
            ON B.appraisalYear = B2.appraisalYear  
            AND B.unitId = B2.unitId  
 
    LEFT OUTER JOIN MINERALPRO.dbo.LeaseValueSummary C 
            ON C.appraisalYear = A.appraisalYear 
            AND C.leaseId = A.subjectId 
		
	LEFT OUTER JOIN MINERALPRO.dbo.Lease C2 
            ON C.appraisalYear = C2.appraisalYear 
            AND C.leaseId = C2.leaseId 

    LEFT OUTER JOIN MINERALPRO.dbo.LeaseTaxUnit D 
            ON D.appraisalYear = A.appraisalYear 
            AND D.leaseId = A.subjectId 
            AND D.taxUnitTypeCd =  @ISD_TAX_JURIS     
            AND D.rowDeleteFlag <> 'D' 

    LEFT OUTER JOIN MINERALPRO.dbo.LeaseTaxUnit E 
            ON E.appraisalYear = A.appraisalYear 
            AND E.leaseId = A.subjectId 
            AND E.taxUnitTypeCd =  @COUNTY_TAX_JURIS     
            AND E.rowDeleteFlag <> 'D'

    WHERE A.appraisalYear =  @appraisalYear_input  
       AND A.rowDeleteFlag <> 'D' 
	ORDER BY A.rrcNumber

  OPEN rsAppl_cursor

  FETCH NEXT FROM rsAppl_cursor
	INTO @appraisalYear
		, @subjectId
		, @subjectTypeCd
		, @rrcNumber
		, @wellTypeCd
		, @workingInterestTotalValue
		, @royaltyInterestTotalValue
		, @totalValue
		, @productionEquipmentCount
		, @serviceEquipmentCount
		, @injectionEquipmentCount 
		, @wellDepth
		, @dailyAverageOil
		, @dailyAverageGas
		, @dailyAverageProduct
		, @accumOilProduction
		, @accumGasProduction
		, @accumProductProduction
		, @yearLife
		, @operatingExpense 
		, @grossOilPrice
		, @grossWorkingInterestGasPrice
		, @productionEquipmentValue
		, @serviceEquipmentValue
		, @injectionEquipmentValue
		, @discountRate
		, @WGT_WI_PCT
		, @WGT_RI_PCT 
		, @LEASE_WI_PCT  
		, @JURIS_PCT_ISD  
		, @JURIS_PCT_CNT  
		, @ACCOUNT_NAME
  

  SET @dblTotalValue = 0
  SET @applCount = 0

  WHILE @@FETCH_STATUS = 0
  BEGIN
      SET @dblTotalValue += @totalValue;
      SET @applCount += 1;
	  SET @strTemp = '068901068';
	  
	  IF (@subjectTypeCd = @UNIT_TYPE)  
			SET @isUnit = 'Y';
	  ELSE
			SET @isUnit = 'N';	 

	--*****************************************************************	
	-- 1. SUBJECT ID
	--*****************************************************************			
	  if @isUnit = 'Y'
		  SET @strTemp = @strTemp 
						+ MINERALPRO.dbo.lpad(@subjectId, 12, ' ')  
						+ MINERALPRO.dbo.lpad(@subjectId, 10, ' ');
	  ELSE
		  SET @strTemp += MINERALPRO.dbo.lpad(@subjectId, 12, ' ') + space(10);


	--*****************************************************************	
	-- 2 WELL TYPE CODE
	--*****************************************************************		
	  SET @strTemp += 
	  CASE  @wellTypeCd   
		  WHEN 1 THEN  'O' 
		  WHEN 2 THEN  'G'
		  ELSE		 space(1) 
	  END;
 
 	  SET @strTemp += '008' + FORMAT(@rrcNumber, '0#####');
  	--*****************************************************************	
	-- 3 PROCESS UNIT RECORD FROM UnitValueSummary
	--*****************************************************************		
	if (@isUnit = 'Y')
	  BEGIN
	    SET @strTemp += FORMAT(@WGT_WI_PCT*100000000, '0########');
												        
		--************************************************************* 
		-- 4. Compare with LeaseTaxUnit table WITH ISD TaxUnit
		--************************************************************* 		
		DECLARE rsTemp2_cursor CURSOR FOR

	    SELECT S.unitId, T.taxUnitPercent, V.workingInterestValue 
			  , V.royaltyInterestValue, V.overridingRoyaltyPercent
			  , V.oilPaymentPercent, V.unitWorkingInterestPercent 
			  , V.unitRoyaltyInterestPercent

			FROM MINERALPRO.dbo.Lease S 

			INNER JOIN MINERALPRO.dbo.LeaseTaxUnit T 
				ON S.appraisalYear = T.appraisalYear 
				AND S.leaseId = T.leaseId 
				AND T.taxUnitTypeCd = @ISD_TAX_JURIS
				AND T.rowDeleteFlag <> 'D'  
				AND S.unitId > 0 

			INNER JOIN MINERALPRO.dbo.LeaseValueSummary V
				ON S.appraisalYear = V.appraisalYear 
				AND S.leaseId = V.leaseId 

			WHERE   S.appraisalYear = @appraisalYear_input
				AND S.rowDeleteFlag <> 'D' 
				AND S.unitId = @unitid

			ORDER BY S.unitId
	
		OPEN rsTemp2_cursor

		FETCH NEXT FROM rsTemp2_cursor 
			INTO @X_unitId, @X_taxUnitPercent, @X_workingInterestPercent 
			  , @X_royaltyInterestPercent, @X_overridingRoyaltyPercent
			  , @X_oilPaymentPercent, @X_unitWorkingInterestPercent 
			  , @X_unitRoyaltyInterestPercent
		
		SET @UnitISDTotalValue = 0;
		WHILE @@FETCH_STATUS = 0   -- Loop thru all the records
		BEGIN

			If @workingInterestTotalValue <> 0 AND @WGT_WI_PCT  <> 0  
                SET @UnitISDTotalValue += MINERALPRO.dbo.udfBankRound(@workingInterestTotalValue
							 / @WGT_WI_PCT *  
							  @X_unitWorkingInterestPercent *
							  @X_workingInterestPercent * 
							  @X_taxUnitPercent, 0);

            -- calc the ri value for each lease within a school district
            If @royaltyInterestTotalValue <> 0 AND @WGT_RI_PCT   <> 0  
                SET @UnitISDTotalValue += MINERALPRO.dbo.udfBankRound(@royaltyInterestTotalValue / @WGT_RI_PCT  *
							  @X_unitRoyaltyInterestPercent *  
                              (@X_royaltyInterestPercent + @X_overridingRoyaltyPercent + @X_oilPaymentPercent) *
							  @X_taxUnitPercent, 0);
 		
			FETCH NEXT FROM rsTemp2_cursor 
			INTO @X_unitId, @X_taxUnitPercent, @X_workingInterestPercent 
			  , @X_royaltyInterestPercent, @X_overridingRoyaltyPercent
			  , @X_oilPaymentPercent, @X_unitWorkingInterestPercent 
			  , @X_unitRoyaltyInterestPercent

		END  -- END OF  ISD TaxUnit
	    CLOSE rsTemp2_cursor;
		DEALLOCATE rsTemp2_cursor;
  		--************************************************************* 		
		-- SET the ISDPercent value
		--************************************************************* 		
 		If @totalValue <> 0 And @UnitISDTotalValue <> 0  
		  BEGIN
			SET @ISDPercent = MINERALPRO.dbo.udfBankRound(@UnitISDTotalValue / @totalValue, 6);
			If @ISDPercent > 1  
				SET @ISDPercent = 1
			 
			SET @strTemp += Format(@ISDPercent * 1000000, '0######');
		  END
        ELSE
		  BEGIN
			SET @ISDPercent = 0
			SET @strTemp +='0000000';
          END

  		-- *** Total Value Pre-ARB / Total Certified Value / Total Forced Value
        SET @strTemp += '00000000000' + FORMAT(@totalValue, '0##########') + '00000000000';
	    --PRINT 'Unit : ' + @strTemp;
 
	  END  -- END OF UNIT
	ELSE   
	--*****************************************************************
	-- PROCESS LEASE records
	--*****************************************************************
	  BEGIN
 	  	SET @strTemp += FORMAT(@LEASE_WI_PCT*100000000, '0########');
		SET @ISDPercent = @JURIS_PCT_ISD;

		If @ISDPercent > 1  
		   SET @ISDPercent = 1;
			 
		SET @strTemp += Format(@ISDPercent * 1000000, '0######');	
        --***Total Value Pre-ARB / Total Certified Value / Total Forced Value
		SET @strTemp += '00000000000' + FORMAT(@totalValue, '0##########') + '00000000000';
	  END  
	--*****************************************************************	
	-- Process LeaseName + Undiscounted Equipment Value + operatoring Expense
	--*****************************************************************	
    SET @strTemp += MINERALPRO.dbo.lpad(RTRIM(@ACCOUNT_NAME), 40, ' '); 
    SET @strTemp += FORMAT(@productionEquipmentValue + @serviceEquipmentValue + @injectionEquipmentValue, '0#########');
    SET @strTemp += FORMAT(@operatingExpense, '0#######');

	IF (@isUnit = 'Y')
	  BEGIN
		SET @UnitCNTYTotalValue = 0;
		--************************************************************* 
		-- 4. Compare with LeaseTaxUnit table WITH COUNTY TaxUnit
		--************************************************************* 		
		DECLARE rsTemp_cursor CURSOR FOR

	    SELECT S.unitId, T.taxUnitPercent, V.workingInterestValue 
			  , V.royaltyInterestValue, V.overridingRoyaltyPercent
			  , V.oilPaymentPercent, V.unitWorkingInterestPercent 
			  , V.unitRoyaltyInterestPercent

			FROM MINERALPRO.dbo.Lease S 

			INNER JOIN MINERALPRO.dbo.LeaseTaxUnit T 
				ON S.appraisalYear = T.appraisalYear 
				AND S.leaseId = T.leaseId 
				AND T.taxUnitTypeCd = @COUNTY_TAX_JURIS
				AND T.rowDeleteFlag <> 'D'  
				AND S.unitId > 0 

			INNER JOIN MINERALPRO.dbo.LeaseValueSummary V
				ON S.appraisalYear = V.appraisalYear 
				AND S.leaseId = V.leaseId 

			WHERE   S.appraisalYear = @appraisalYear_input
				AND S.rowDeleteFlag <> 'D' 
				AND S.unitId = @unitid

			ORDER BY S.unitId
	
		OPEN rsTemp_cursor

		FETCH NEXT FROM rsTemp_cursor 
			INTO @X_unitId, @X_taxUnitPercent, @X_workingInterestPercent 
			  , @X_royaltyInterestPercent, @X_overridingRoyaltyPercent
			  , @X_oilPaymentPercent, @X_unitWorkingInterestPercent 
			  , @X_unitRoyaltyInterestPercent

		WHILE @@FETCH_STATUS = 0   -- Loop thru all the reTemp_cursor records
		 BEGIN
			If @workingInterestTotalValue <> 0 AND @WGT_WI_PCT  <> 0  
                SET @UnitCNTYTotalValue += MINERALPRO.dbo.udfBankRound(@workingInterestTotalValue / @WGT_WI_PCT *  
							  @X_unitWorkingInterestPercent *
							  @X_workingInterestPercent * 
							  @X_taxUnitPercent, 0);

            -- calc the ri value for each lease within a school district
            If @royaltyInterestTotalValue <> 0 AND @WGT_RI_PCT   <> 0  
                SET @UnitCNTYTotalValue += MINERALPRO.dbo.udfBankRound(@royaltyInterestTotalValue / @WGT_RI_PCT  *
							  @X_unitRoyaltyInterestPercent *  
                              (@X_royaltyInterestPercent + @X_overridingRoyaltyPercent + @X_oilPaymentPercent) *
							  @X_taxUnitPercent, 0);
 		
			FETCH NEXT FROM rsTemp_cursor 
			INTO @X_unitId, @X_taxUnitPercent, @X_workingInterestPercent 
			  , @X_royaltyInterestPercent, @X_overridingRoyaltyPercent
			  , @X_oilPaymentPercent, @X_unitWorkingInterestPercent 
			  , @X_unitRoyaltyInterestPercent
		 END

		CLOSE rsTemp_cursor;
		DEALLOCATE rsTemp_cursor;
		If @totalValue <> 0 And @UnitCNTYTotalValue <> 0  
		  BEGIN
			SET @CountyPercent = MINERALPRO.dbo.udfBankRound(@UnitCNTYTotalValue / @totalValue, 6);
			If @CountyPercent > 1  
				SET @CountyPercent = 1;	 
		  END
        ELSE
		  BEGIN
			SET @CountyPercent = 0;
           END

       IF @CountyPercent < 1  
            SET @strTemp += 'Y';
       ELSE
            SET @strTemp += 'N';
        
 	  END
	ELSE
	   --***********************************************************
	   -- LEASE RECORD
	   --***********************************************************
	  BEGIN

		SET @CountyPercent = @JURIS_PCT_CNT;   
		IF @CountyPercent < 1  
			SET @strTemp += 'Y';
		ELSE
			SET @strTemp += 'N';
     
	  END
	  --*********************************************************
	  -- END of each LINE 
	  --*********************************************************
	  If @ISDPercent < 1  
         SET @strTemp += 'Y'; 
      ELSE
         SET @strTemp += 'N'; 
      
	  INSERT INTO #tempOutput VALUES( @strTemp);

	--*****************************************************************	
	-- LOOP THRU NEXT RECORD
	--*****************************************************************	
	FETCH NEXT FROM rsAppl_cursor INTO  @appraisalYear
		, @subjectId
		, @subjectTypeCd
		, @rrcNumber
		, @wellTypeCd
		, @workingInterestTotalValue
		, @royaltyInterestTotalValue
		, @totalValue
		, @productionEquipmentCount
		, @serviceEquipmentCount
		, @injectionEquipmentCount 
		, @wellDepth
		, @dailyAverageOil
		, @dailyAverageGas
		, @dailyAverageProduct
		, @accumOilProduction
		, @accumGasProduction
		, @accumProductProduction
		, @yearLife
		, @operatingExpense 
		, @grossOilPrice
		, @grossWorkingInterestGasPrice
		, @productionEquipmentValue
		, @serviceEquipmentValue
		, @injectionEquipmentValue
		, @discountRate
		, @WGT_WI_PCT
		, @WGT_RI_PCT 
		, @LEASE_WI_PCT  
		, @JURIS_PCT_ISD  
		, @JURIS_PCT_CNT  
		, @ACCOUNT_NAME
  END  -- end of Appraisal Tables Loop
 
  --PRINT 'Total Appraisals: ' + Format(@applCount, '#,##0');
  --PRINT 'Total Value     : ' + Format(@dblTotalValue, '#,##0');
   
  CLOSE rsAppl_cursor;
  DEALLOCATE rsAppl_cursor;  
 
	SELECT * FROM #tempOutput;
	DROP TABLE #tempOutput;
END





GO


