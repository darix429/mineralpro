USE [MINERALPRO]
GO

DROP PROCEDURE IF EXISTS  [dbo].[spAppraisalByLease_LeaseTaxUnit]

/****** Object:  StoredProcedure [dbo].[spAppraisalByLease_LeaseTaxUnit]    Script Date: 8/16/2017 12:38:32 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[spAppraisalByLease_LeaseTaxUnit]
	-- Add the parameters for the stored procedure here
	@PAPPRAISAL_YR Char(4) = '2017'
	, @PACCOUNT_NUM_FROM Int = 0
	, @PACCOUNT_NUM_TO Int = 9999999
	, @PRRC_NUM_FROM Int = 0
	, @PRRC_NUM_TO Int = 99999999
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	--SELECT <@Param1, sysname, @p1>, <@Param2, sysname, @p2>

SELECT LeaseTaxUnit.taxUnitPercent  
 , Appraisal.appraisalYear  
 , Appraisal.subjectId  
 , Appraisal.rrcNumber  
 , LeaseTaxUnit.rowDeleteFlag  
 , SystemCode.description  
 , Appraisal.totalValue  
 , Appraisal.workingInterestTotalValue  
 , Appraisal.royaltyInterestTotalValue  
 , LeaseTaxUnit.taxUnitTypeCd  
 , LeaseTaxUnit.taxUnitCd  
 , SystemCode.shortDescription  

 FROM   MINERALPRO.dbo.Appraisal Appraisal 
 INNER JOIN MINERALPRO.dbo.LeaseTaxUnit   
  ON Appraisal.subjectId = LeaseTaxUnit.leaseId
  AND Appraisal.appraisalYear = LeaseTaxUnit.appraisalYear 
  
  INNER JOIN MINERALPRO.dbo.SystemCode   
  ON LeaseTaxUnit.appraisalYear = SystemCode.appraisalYear 
  AND LeaseTaxUnit.taxUnitTypeCd = SystemCode.codeType 
  AND LeaseTaxUnit.taxUnitCd = SystemCode.code

 WHERE Appraisal.appraisalYear = @PAPPRAISAL_YR
    AND Appraisal.rrcNumber Between @PRRC_NUM_FROM And @PRRC_NUM_TO
	AND Appraisal.subjectId Between @PACCOUNT_NUM_FROM And @PACCOUNT_NUM_TO
	AND LeaseTaxUnit.rowDeleteFlag<>'D' 
	AND (LeaseTaxUnit.taxUnitTypeCd <> 1090 AND LeaseTaxUnit.taxUnitCd <> 99 )
    AND (LeaseTaxUnit.taxUnitTypeCd <> 1090 AND LeaseTaxUnit.taxUnitCd <> 4 )


END

GO

