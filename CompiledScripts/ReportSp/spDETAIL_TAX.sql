USE [MINERALPRO]
GO

DROP PROCEDURE IF EXISTS [dbo].[spDETAIL_TAX]

/****** Object:  StoredProcedure [dbo].[spDETAIL_TAX]    Script Date: 8/16/2017 12:38:48 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[spDETAIL_TAX]
  @PAPPRAISAL_YR Char(4) = '2017'
, @POWNER_ID_FROM Int = 0
, @POWNER_ID_TO Int = 9999
AS
BEGIN

	SET NOCOUNT ON
   
   
Declare @Lse TABLE(
	[appraisalYearMaster] [int] NULL,
	[leaseIdMaster] [int] NULL,
	[appraisalYear] [smallint] NOT NULL,
	[leaseId] [int] NOT NULL,
	[divisionOrderDescription] [varchar](50) NOT NULL,
	[description] [varchar](60) NOT NULL,
	[comment] [varchar](50) NOT NULL,
	[operatorId] [int] NOT NULL,
	[leaseYear] [smallint] NOT NULL,
	[changeReasonCd] [smallint] NOT NULL,
	[acres] [decimal](10, 3) NOT NULL,
	[royaltyInterestPercent] [decimal](9, 8) NOT NULL,
	[workingInterestPercent] [decimal](9, 8) NOT NULL,
	[overridingRoyaltyPercent] [decimal](9, 8) NOT NULL,
	[oilPaymentPercent] [decimal](9, 8) NOT NULL,
	[royaltyInterestValue] [decimal](13, 0) NOT NULL,
	[workingInterestValue] [decimal](13, 0) NOT NULL,
	[tractNum] [varchar](10) NOT NULL,
	[unitWorkingInterestPercent] [decimal](9, 8) NOT NULL,
	[unitRoyaltyInterestPercent] [decimal](9, 8) NOT NULL,
	[rowUpdateUserid] [int] NOT NULL,
	[rowUpdateDt] [datetime] NOT NULL,
	[rowDeleteFlag] [char](1) NOT NULL,
	[idLease] [bigint] NOT NULL,
	[idLease_PRIOR] [bigint] NULL,
	[APPRAISAL_YR_PRIOR] [smallint] NULL,
	[ACCOUNT_NUM_PRIOR] [int] NULL,
	[PIPELINE_PRIOR] [varchar](50) NULL,
	[LEASE_DESC_PRIOR] [varchar](60) NULL,
	[LEASE_NOT_PRIOR] [varchar](50) NULL,
	[OPERATOR_NUM_PRIOR] [int] NULL,
	[LEASE_YR_PRIOR] [smallint] NULL,
	[LEASE_CHG_CD_PRIOR] [smallint] NULL,
	[LEASE_ACRES_PRIOR] [decimal](10, 3) NULL,
	[LEASE_RI_PCT_PRIOR] [decimal](9, 8) NOT NULL,
	[LEASE_WI_PCT_PRIOR] [decimal](9, 8) NOT NULL,
	[LEASE_OR_PCT_PRIOR] [decimal](9, 8) NOT NULL,
	[LEASE_OP_PCT_PRIOR] [decimal](9, 8) NOT NULL,
	[LEASE_RI_VALUE_PRIOR] [decimal](13, 0) NOT NULL,
	[LEASE_WI_VALUE_PRIOR] [decimal](13, 0) NOT NULL,
	[TRACT_NUM_PRIOR] [varchar](10) NOT NULL,
	[UNIT_WI_PCT_PRIOR] [decimal](9, 8) NOT NULL,
	[UNIT_RI_PCT_PRIOR] [decimal](9, 8) NOT NULL,
	[LST_UPDT_EMPL_ID_PRIOR] [int] NULL,
	[LST_UPDT_TS_PRIOR] [datetime] NULL,
	[POST_FLG_PRIOR] [char](1) NULL
) 




Declare @LseOwn TABLE(
	[appraisalYearMaster] [int] NULL,
	[leaseIdMaster] [int] NULL,
	[ownerIdMaster] [int] NULL,
	[interestTypeCdMaster] [smallint] NULL,
	[appraisalYear] [smallint] NOT NULL,
	[leaseId] [int] NOT NULL,
	[ownerId] [int] NOT NULL,
	[interestTypeCd] [smallint] NOT NULL,
	[interestPercent] [decimal](9, 8) NOT NULL,
	[ownerValue] [decimal](13, 0) NOT NULL,
	[lockCd] [smallint] NOT NULL,
	[rowUpdateUserid] [int] NOT NULL,
	[rowUpdateDt] [datetime] NOT NULL,
	[rowDeleteFlag] [char](1) NOT NULL,
	[idLeaseOwner] [bigint] NOT NULL,
	[idLeaseOwner_PRIOR] [bigint] NULL,
	[APPRAISAL_YR_PRIOR] [smallint] NULL,
	[ACCOUNT_NUM_PRIOR] [int] NULL,
	[OWNER_ID_PRIOR] [int] NULL,
	[LEASE_INT_TYP_CD_PRIOR] [smallint] NULL,
	[LEASE_INT_PCT_PRIOR] [decimal](9, 8) NULL,
	[OWNER_VAL_PRIOR] [decimal](13, 0) NULL,
	[FREEZE_CD_PRIOR] [smallint] NULL,
	[LST_UPDT_EMPL_ID_PRIOR] [int] NULL,
	[LST_UPDT_TS_PRIOR] [datetime] NULL,
	[POST_FLG_PRIOR] [char](1) NULL
) 



Declare @LseTaxUnit table(
	[appraisalYearMaster] [int] NULL,
	[taxUnitTypeCdMaster] [smallint] NULL,
	[taxUnitCdMaster] [smallint] NULL,
	[leaseIdMaster] [int] NULL,
	[leaseId] [int] NOT NULL,
	[appraisalYear] [smallint] NOT NULL,
	[taxUnitTypeCd] [smallint] NOT NULL,
	[taxUnitCd] [smallint] NOT NULL,
	[taxUnitPercent] [decimal](4, 3) NOT NULL,
	[rowUpdateUserid] [int] NOT NULL,
	[rowUpdateDt] [datetime] NOT NULL,
	[rowDeleteFlag] [char](1) NOT NULL,
	[idLeaseTaxUnit] [bigint] NOT NULL,
	[idLeaseTaxUnit_PRIOR] [bigint] NULL,
	[ACCOUNT_NUM_PRIOR] [int] NULL,
	[APPRAISAL_YR_PRIOR] [smallint] NULL,
	[JURIS_TYP_CD_PRIOR] [smallint] NULL,
	[JURIS_CD_PRIOR] [smallint] NULL,
	[JURIS_PCT_PRIOR] [decimal](4, 3) NULL,
	[LST_UPDT_EMPL_ID_PRIOR] [int] NULL,
	[LST_UPDT_TS_PRIOR] [datetime] NULL,
	[POST_FLG_PRIOR] [char](1) NULL
) 

Declare  @TxUnit table(
	[appraisalYearMaster] [int] NULL,
	[taxUnitTypeCdMaster] [smallint] NULL,
	[taxUnitCdMaster] [smallint] NULL,
	[taxingIndMaster] [char](1) NULL,
	[appraisalYear] [smallint] NOT NULL,
	[taxUnitTypeCd] [smallint] NOT NULL,
	[taxUnitCd] [smallint] NOT NULL,
	[taxUnitAbbr] [varchar](15) NOT NULL,
	[estimatedTaxRate] [decimal](9, 8) NOT NULL,
	[taxingInd] [char](1) NOT NULL,
	[rowUpdateUserid] [int] NOT NULL,
	[rowUpdateDt] [datetime] NOT NULL,
	[rowDeleteFlag] [char](1) NOT NULL,
	[APPRAISAL_YR_PRIOR] [smallint] NULL,
	[JURIS_TYP_CD_PRIOR] [smallint] NULL,
	[TAX_JURIS_CD_PRIOR] [smallint] NULL,
	[SHORT_NAME_PRIOR] [varchar](15) NULL,
	[TAX_RATE_PRIOR] [decimal](9, 8) NULL,
	[LIST_ON_NOTICE_IND_PRIOR] [char](1) NULL,
	[LST_UPDT_EMPL_ID_PRIOR] [int] NULL,
	[LST_UPDT_TS_PRIOR] [datetime] NULL,
	[POST_FLG_PRIOR] [char](1) NULL
) 
   Insert Into @LseOwn 
   Select * 
   From MINERALPRO.dbo.vLeaseOwner_Current_Prior 
    WHERE  vLeaseOwner_Current_Prior.appraisalYearMaster = @PAPPRAISAL_YR
     AND vLeaseOwner_Current_Prior.ownerIdMaster Between @POWNER_ID_FROM And @POWNER_ID_TO

   Insert Into @Lse
   Select distinct vLease_Current_Prior.*  From MINERALPRO.dbo.vLease_Current_Prior As vLease_Current_Prior
   Join @LseOwn As vLeaseOwner_Current_Prior
    ON vLeaseOwner_Current_Prior.appraisalYearMaster=vLease_Current_Prior.appraisalYearMaster 
    AND vLeaseOwner_Current_Prior.leaseIdMaster=vLease_Current_Prior.leaseIdMaster 

   Insert Into @LseTaxUnit 
   Select vLeaseTaxUnit_Current_Prior.* From MINERALPRO.dbo.vLeaseTaxUnit_Current_Prior 
   Join @Lse As vLease_Current_Prior
     ON vLease_Current_Prior.appraisalYearMaster = vLeaseTaxUnit_Current_Prior.appraisalYearMaster
      AND vLease_Current_Prior.leaseIdMaster=vLeaseTaxUnit_Current_Prior.leaseIdMaster

   Insert Into @TxUnit 
   Select vTaxUnit_Current_Prior.* From MINERALPRO.dbo.vTaxUnit_Current_Prior
    WHERE  vTaxUnit_Current_Prior.appraisalYearMaster = @PAPPRAISAL_YR


    SELECT  vLeaseOwner_Current_Prior.ownerIdMaster OWNER_ID_MASTER
    , SystemCode.shortDescription LEGACY_VALUE
    , vTaxUnit_Current_Prior.estimatedTaxRate TAX_RATE --check
    , vLeaseOwner_Current_Prior.interestTypeCd LEASE_INT_TYP_CD
    , vLease_Current_Prior.workingInterestValue LEASE_WI_VALUE
    , vLease_Current_Prior.workingInterestPercent LEASE_WI_PCT
    , vLeaseOwner_Current_Prior.interestPercent LEASE_INT_PCT
    , vLease_Current_Prior.royaltyInterestValue LEASE_RI_VALUE
    , vLease_Current_Prior.royaltyInterestPercent LEASE_RI_PCT
    , vLease_Current_Prior.overridingRoyaltyPercent LEASE_OR_PCT
    , vLease_Current_Prior.oilPaymentPercent LEASE_OP_PCT
    , vLeaseOwner_Current_Prior.OWNER_VAL_PRIOR
    , vLeaseOwner_Current_Prior.LEASE_INT_TYP_CD_PRIOR
    , vLease_Current_Prior.LEASE_WI_VALUE_PRIOR
    , vLease_Current_Prior.LEASE_WI_PCT_PRIOR
    , vLeaseOwner_Current_Prior.LEASE_INT_PCT_PRIOR
    , vLease_Current_Prior.LEASE_RI_VALUE_PRIOR
    , vLease_Current_Prior.LEASE_RI_PCT_PRIOR
    , vLease_Current_Prior.LEASE_OR_PCT_PRIOR
    , vLease_Current_Prior.LEASE_OP_PCT_PRIOR
    , vLeaseOwner_Current_Prior.appraisalYearMaster APPRAISAL_YR_MASTER
    , vTaxUnit_Current_Prior.taxingIndMaster LIST_ON_NOTICE_IND_MASTER
    , vTaxUnit_Current_Prior.TAX_RATE_PRIOR
    , vLeaseTaxUnit_Current_Prior.taxUnitPercent JURIS_PCT
    , vLeaseTaxUnit_Current_Prior.JURIS_PCT_PRIOR

    FROM   @LseOwn As vLeaseOwner_Current_Prior
    INNER JOIN @Lse As vLease_Current_Prior  
    ON vLeaseOwner_Current_Prior.appraisalYearMaster=vLease_Current_Prior.appraisalYearMaster 
    AND vLeaseOwner_Current_Prior.leaseIdMaster=vLease_Current_Prior.leaseIdMaster 
 
    INNER JOIN @LseTaxUnit vLeaseTaxUnit_Current_Prior 
     ON vLease_Current_Prior.appraisalYearMaster = vLeaseTaxUnit_Current_Prior.appraisalYearMaster
      AND vLease_Current_Prior.leaseIdMaster=vLeaseTaxUnit_Current_Prior.leaseIdMaster
   
    INNER JOIN @TxUnit as vTaxUnit_Current_Prior  
    ON vLeaseTaxUnit_Current_Prior.appraisalYearMaster = vTaxUnit_Current_Prior.appraisalYearMaster 
    AND vLeaseTaxUnit_Current_Prior.taxUnitTypeCdMaster = vTaxUnit_Current_Prior.taxUnitTypeCdMaster 
    AND vLeaseTaxUnit_Current_Prior.taxUnitCdMaster = vTaxUnit_Current_Prior.taxUnitCdMaster 
 
    INNER JOIN MINERALPRO.dbo.SystemCode SystemCode 
    ON vTaxUnit_Current_Prior.appraisalYearMaster=  SystemCode.appraisalYear 
    AND vTaxUnit_Current_Prior.taxUnitTypeCdMaster = SystemCode.codeType 
    AND vTaxUnit_Current_Prior.taxUnitCd = SystemCode.code  
   
    WHERE  vLeaseOwner_Current_Prior.appraisalYearMaster = @PAPPRAISAL_YR
     AND vLeaseOwner_Current_Prior.ownerIdMaster Between @POWNER_ID_FROM And @POWNER_ID_TO
     AND vTaxUnit_Current_Prior.taxingIndMaster= 'Y'
     AND vLeaseOwner_Current_Prior.rowDeleteFlag <> 'D' 
     AND vTaxUnit_Current_Prior.rowDeleteFlag <> 'D'
     AND vLease_Current_Prior.rowDeleteFlag <> 'D' 
     AND vLeaseTaxUnit_Current_Prior.rowDeleteFlag <> 'D'  

    --ORDER BY vLeaseOwner_Current_Prior.ownerIdMaster, SystemCode.shortDescription

 
END


GO

