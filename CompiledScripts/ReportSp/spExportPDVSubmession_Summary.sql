USE [MINERALPRO]
GO

/****** Object:  StoredProcedure [dbo].[spExportPDVSubmession_Summary]    Script Date: 9/20/2017 9:11:01 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




--=============================================
-- Author:		Foomay
-- Create date: 2017-09-02
-- Description:	Export public records
-- =============================================
CREATE PROCEDURE [dbo].[spExportPDVSubmession_Summary] 
	-- Add the parameters for the stored procedure here
	@appraisalYear_input smallint = 2017
AS
BEGIN 
	IF OBJECT_ID (N'tempdb..#tempOutput', N'U') IS NOT NULL
	  DROP TABLE #tempOutput

	CREATE TABLE #tempOutput (
			outputField nvarchar(500)
	);

	INSERT INTO #tempOutput
	  SELECT 'Total Appraisals: ' + FORMAT(count(1), '#,##0')
	  FROM [MINERALPRO].[dbo].[Appraisal]
	  WHERE rowDeleteFlag <> 'D'
	  AND appraisalYear = @appraisalYear_input

	INSERT INTO #tempOutput
	  SELECT 'Total Value     : ' + FORMAT(sum(totalValue), '#,##0')
	  FROM [MINERALPRO].[dbo].[Appraisal]
	  WHERE rowDeleteFlag <> 'D'
	  AND appraisalYear = @appraisalYear_input

  
 	SELECT * FROM #tempOutput;
	DROP TABLE #tempOutput

END





GO


