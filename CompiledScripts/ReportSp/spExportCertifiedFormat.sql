USE [MINERALPRO]
GO

/****** Object:  StoredProcedure [dbo].[spExportCertifiedFormat]    Script Date: 9/20/2017 9:09:11 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



-- =============================================
-- Author:		Foomay
-- Create date: 2017-09-02
-- Description:	Export public records
-- =============================================
CREATE PROCEDURE [dbo].[spExportCertifiedFormat] 
	-- Add the parameters for the stored procedure here
	@appraisalYear_input smallint = 2017
AS
BEGIN
 
 DECLARE @dblTotalValue bigint = 0;
 DECLARE @applCount int = 0;
 DECLARE @idCount int = 0;
 DECLARE @prevOwner int = 0;
 DECLARE @dblTotHoldingsValue bigint =  0;
 DECLARE @TOTAL_OWNER_COUNT int = 0;
 DECLARE @strTemp  varchar(600);
 DECLARE @strTemp1  varchar(220);
 DECLARE @strTemp2 varchar(220);
 DECLARE @strTemp3 varchar(30);
 DECLARE @strTemp4 varchar(120);
 DECLARE @oStr1 varchar(220); 

 DECLARE @oStr2 varchar(220);
 DECLARE @oStr3 varchar(30);
 DECLARE @oStr4 varchar(120);
 DECLARE @strTotalExemptCode varchar(10);
 DECLARE @tmpName varchar(50); 
 DECLARE @tmpOpName varchar(50); 
 DECLARE @tmpAgent varchar(6);
 DECLARE @tmpDesc varchar(100);
 DECLARE @tmpOwner varchar(50);
 DECLARE @tmpAddr1 varchar(50);
 DECLARE @tmpAddr3 varchar(50);
 DECLARE @tmpCity varchar(50);
 DECLARE @tmpState varchar(10);
 DECLARE @tmpZipCode varchar(9);
 DECLARE @loopCount smallint = 0;
 DECLARE @EOF char = 'N';
 DECLARE @PREV_OWNER_RS3430_LEGACY_VALUE char = '';
 DECLARE @dblVal Float = 0;
 DECLARE @RI_INTEREST_TYPE_CD smallint = 1; 
 DECLARE @OR_INTEREST_TYPE_CD smallint = 2;
 DECLARE @OP_INTEREST_TYPE_CD smallint = 3;
 DECLARE @WI_INTEREST_TYPE_CD smallint = 4;
 DECLARE @tempInterestValue Float = 0;
 DECLARE @totalTaxUnitPct Float = 0;
 
 DECLARE  @leaseId bigint
	, @interestTypeCd smallint
	, @interestPercent decimal(9,8)
	, @description   varchar(50)
    , @OWNER_NAME varchar(50)
	, @ADDRESS_LINE1 varchar(40)
	, @ADDRESS_LINE3 varchar(40)
	, @CITY varchar(35)
	, @STATE_CD smallint
	, @STATE_DESC varchar(50)
	, @ZIPCODE varchar(9)
	, @ownerId  bigint
    , @leaseName varchar(50)
	, @OPERATOR_NAME varchar(50)
	, @ownerValue decimal(13,0)
	, @workingInterestPercent decimal(9,8)
	, @leaseYear smallint
	, @acres  decimal(10,3) 
    , @LEASE_RI_PCT_SUM decimal(9,8)
	, @TAX_REP_ID  varchar(6)
    , @royaltyInterestValue decimal(13,0)
	, @workingInterestValue decimal(13,0)
	, @LEASE_DESCRIPTION varchar(50)
	, @unitId bigint
	, @operatorId  int
	, @rstemp2_leaseId bigint
	, @rstemp2_taxUnitPercent decimal(9,8)
	, @rstemp2_shortDescription varchar(25)
    , @RSTEMP2_OPERATOR_NUM bigint
    , @RSTEMP2_OPERATOR_NAME  varchar(50)
    , @RSTEMP2_EXEMPTION_CODE smallint
    , @RS3430_LEGACY_VALUE varchar(25)
    , @ST_description varchar(50)

---*******************************************************************
--- Create a temp table to hold the records per each OWNER
---*******************************************************************

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF OBJECT_ID (N'tempdb..#tmpTbl_c', N'U') IS NOT NULL
	  DROP TABLE #tmpTbl_c

	CREATE TABLE #tmpTbl_c (
		oStr1 nvarchar(250),
		oStr2 nvarchar(250),
		oStr3 nvarchar(150),
		oStr4 nvarchar(150),
 	);
	IF OBJECT_ID (N'tempdb..#tmpTbl_c2', N'U') IS NOT NULL
	  DROP TABLE #tmpTbl_c2

	CREATE TABLE #tmpTbl_c2 (
		id int,
		outputField nvarchar(700) 
 	);
---*******************************************************************
-- Construct the main Query, save the total owner count for the last
-- owner processing
---*******************************************************************
DECLARE rsTemp_cursor CURSOR FOR
    SELECT count(*) over () AS TOTAL_OWNER_COUNT
	, A.leaseId
	, A.interestTypeCd 
	, A.interestPercent
	, D.description   
    , ISNULL(F.name1,'') AS OWNER_NAME
	, ISNULL(F.addrLine1,'') AS ADDRESS_LINE1
	, ISNULL(F.addrLine3,'') AS ADDRESS_LINE3
	, ISNULL(F.city,'') AS CITY
	, ISNULL(F.stateCd,0) AS STATE_CD
	, ISNULL(ST.codeDesc,'') AS STATE_DESC
	, ISNULL(F.zipcode,'') AS ZIPCODE
	, A.ownerId  
    , B.leaseName
	, ISNULL(E.operatorName,'') AS OPERATOR_NAME
	, A.ownerValue
	, C.workingInterestPercent
	, B.leaseYear
	, B.acres 
    ,(C.oilPaymentPercent + C.overridingRoyaltyPercent + C.royaltyInterestPercent) AS LEASE_RI_PCT_SUM
	, CAST(CAST(F.agencyCdx AS INT) AS varchar(6))  AS TAX_REP_ID   
    , C.royaltyInterestValue
	, C.workingInterestValue
	, B.description AS LEASE_DESCRIPTION
	, B.unitId
	, B.operatorId  
    , ISNULL(EUM.operatorId,0) AS RSTEMP2_OPERATOR_NUM 
    , ISNULL(MOE.operatorName,' ') AS RSTEMP2_OPERATOR_NAME 
    , ISNULL(MOEA.exemptionCd,0) AS RSTEMP2_EXEMPTION_CODE 
    , ISNULL(CTM.shortDescription,' ') AS RS3430_LEGACY_VALUE 
    , ISNULL(ST.codeDesc,' ') AS ST_description 

	FROM MINERALPRO.dbo.LeaseOwner A 
	INNER JOIN MINERALPRO.dbo.Lease B 
		ON A.appraisalYear = B.appraisalYear 
		AND A.leaseId = B.leaseId  

	INNER JOIN MINERALPRO.dbo.LeaseValueSummary C  
		ON A.appraisalYear = C.appraisalYear
		AND A.leaseId = C.leaseId  

	LEFT OUTER JOIN MINERALPRO.dbo.Operator E
		ON C.appraisalYear = E.appraisalYear  
		AND B.operatorId = E.operatorId  

	INNER JOIN MINERALPRO.dbo.SystemCode D  
		ON A.appraisalYear = D.appraisalYear 
		AND A.interestTypeCd = D.code 

		AND D.codeType = 3000 
    
	LEFT OUTER JOIN MINERALPRO.dbo.Owner F 
		ON A.appraisalYear = F.appraisalYear 
		AND A.ownerId = F.ownerId  

	LEFT OUTER JOIN MINERALPRO.dbo.Unit EUM 
		ON EUM.appraisalYear = A.appraisalYear 
		AND EUM.unitId = B.unitId 
		AND EUM.rowDeleteFlag <> 'D' 

	LEFT OUTER JOIN MINERALPRO.dbo.Operator MOE 
		ON MOE.appraisalYear = A.appraisalYear  
		AND MOE.operatorId = EUM.operatorId 

	LEFT OUTER JOIN MINERALPRO.dbo.OwnerExemption MOEA 
		ON MOEA.appraisalYear = A.appraisalYear 
		AND MOEA.ownerId = A.ownerId 
		AND MOEA.exemptionTypeCd = 3440 
		AND MOEA.rowDeleteFlag <> 'D' 

	LEFT OUTER JOIN MINERALPRO.dbo.OwnerExemption MOEB 
		ON MOEB.appraisalYear = A.appraisalYear 
		AND MOEB.ownerId = A.ownerId 
		AND MOEB.exemptionTypeCd =  3430 
		AND MOEB.rowDeleteFlag <> 'D' 

	LEFT OUTER JOIN MINERALPRO.dbo.SystemCode CTM 
		ON CTM.appraisalYear = A.appraisalYear 
		AND CTM.codeType = 3430 
		AND CTM.CODE = MOEB.exemptionCd 

	LEFT OUTER JOIN MINERALPRO.dbo.vSystemCode_and_Type ST 
		ON ST.appraisalYear = A.appraisalYear 
		AND ST.codeTypeDesc  = 'STATE CODES' 
		AND ST.code = F.stateCd 

	WHERE A.appraisalYear =  @appraisalYear_input  
		AND A.rowDeleteFlag <> 'D'  
 
	ORDER BY A.ownerId, CAST(A.leaseId AS INTEGER)

	OPEN rsTemp_cursor

    FETCH NEXT FROM rsTemp_cursor
	INTO @TOTAL_OWNER_COUNT
	, @leaseId 
	, @interestTypeCd 
	, @interestPercent 
	, @description   
    , @OWNER_NAME 
	, @ADDRESS_LINE1 
	, @ADDRESS_LINE3 
	, @CITY 
	, @STATE_CD 
	, @STATE_DESC 
	, @ZIPCODE 
	, @ownerId  
    , @leaseName 
	, @OPERATOR_NAME 
	, @ownerValue 
	, @workingInterestPercent 
	, @leaseYear 
	, @acres   
    , @LEASE_RI_PCT_SUM 
	, @TAX_REP_ID  
    , @royaltyInterestValue 
	, @workingInterestValue 
	, @LEASE_DESCRIPTION 
	, @unitId 
	, @operatorId  
    , @RSTEMP2_OPERATOR_NUM 
    , @RSTEMP2_OPERATOR_NAME  
    , @RSTEMP2_EXEMPTION_CODE 
    , @RS3430_LEGACY_VALUE 
    , @ST_description


  SET @dblTotalValue = 0
  SET @applCount = 0
  SET @prevOwner = @ownerId;

  --**********************************************************************
  -- Loop thru the entire LeaseOwner table
  -- Also process the very last owner 
  --**********************************************************************
  WHILE @@FETCH_STATUS = 0  
  BEGIN
    SET @applCount += 1;
    SET @strTemp3 = '';
	SET @strTemp4 = '';
    ---*******************************************************************
	-- build up the ADDRESS here  - save data in strTemp1
    ---*******************************************************************
	SET @tmpOwner = LTRIM(RTRIM(@OWNER_NAME));
	SET @tmpAddr1 = LTRIM(RTRIM(@ADDRESS_LINE1));
	SET @tmpAddr3 = LTRIM(RTRIM(@ADDRESS_LINE3));
	SET @tmpCity = LTRIM(RTRIM(@CITY));
	SET @tmpState = LTRIM(RTRIM(@ST_description));
	SET @tmpZipCode = LTRIM(RTRIM(@ZIPCODE));

    SET @strTemp1 = FORMAT(@ownerId, '0#########') 
				 + FORMAT(@leaseId, '0#########')  
			     + FORMAT(@interestTypeCd, '#') 
                 +  @tmpOwner + space(50 - LEN(@tmpOwner))  
                 +  @tmpAddr1 + space(40 - LEN(@tmpAddr1))  
                 +  @tmpAddr3 + space(40 - LEN(@tmpAddr3)) 
                 +  @tmpCity + space(50 - LEN(@city)) 
    IF @STATE_CD = 0
	   SET @strTemp1 += '    ' + @tmpZipcode + space(10 - LEN(@tmpZipcode));
	ELSE
	   SET @strTemp1 += @tmpState + space(4 - LEN(@tmpState)) +  @tmpZipCode + space(10 - LEN(@tmpZipCode));
 
	--**********************************************************************
	-- process working Interest Type Code 
	--**********************************************************************  
 
	IF @interestTypeCd = @WI_INTEREST_TYPE_CD  
	BEGIN   
		IF @workingInterestValue > 0 And @workingInterestPercent > 0  
		  BEGIN
		    SET @tempInterestValue = MINERALPRO.dbo.udfBankRound(@workingInterestValue / @workingInterestPercent * @interestPercent, 0);
			IF @tempInterestValue < 1  
				SET @dblVal = 1;
			ELSE
				SET @dblVal = @tempInterestValue;
		  END
	    ELSE   
			SET @dblVal = 0;
	END
	--**********************************************************************
	-- Otherwise, process OR, OP, RI interest type code
	-- Not using IF -- ELSE as too many nesting IF statement -- too confusing
	-- It's easier to read this way
	--**********************************************************************
 	IF   @interestTypeCd = @OR_INTEREST_TYPE_CD  
	  OR @interestTypeCd = @RI_INTEREST_TYPE_CD
	  OR @interestTypeCd = @OP_INTEREST_TYPE_CD
	BEGIN
 		IF @royaltyInterestValue > 0 AND @LEASE_RI_PCT_SUM > 0
		  BEGIN			  
			SET @tempInterestValue = MINERALPRO.dbo.udfBankRound(@royaltyInterestValue / @LEASE_RI_PCT_SUM * @interestPercent, 0);

  			IF @tempInterestValue < 1  
				SET @dblVal = 1;
			ELSE
				SET @dblVal = @tempInterestValue;
 		  END
	    ELSE   
		  BEGIN
			SET @dblVal = 0;
 		  END
	END
 
	--**********************************************************************
	-- construct agent code 
	--**********************************************************************
  
	-- starting strtemp2 here
	If  @TAX_REP_ID = '0' OR Len(@TAX_REP_ID) = 0
		SET @strTemp2  = space(1) + '0  ' + FORMAT(@interestPercent, '0.#####0')				     
					  + FORMAT(@interestTypeCd, '#');
    ELSE
		SET @strTemp2  = space(4 - len(@TAX_REP_ID)) + @TAX_REP_ID 
					  + FORMAT(@interestPercent, '0.#####0')	
					  + FORMAT(@interestTypeCd, '#');

	--End If TAX REP ID
	--**********************************************************************
	-- construct lease name
	--**********************************************************************
	SET @tmpName = LTRIM(RTRIM(@leaseName));
	SET @strTemp2  +=  @tmpName + space(50 - len(@tmpName))
				   + FORMAT(@leaseYear, '0###') + 'G1 ';

	--**********************************************************************
	-- construct operator name, lease desc and acres
	--********************************************************************** 
    IF (@unitId > 0)
	   SET @tmpOpName = LTRIM(RTRIM(@RSTEMP2_OPERATOR_NAME));
	ELSE
	   SET @tmpOpName = LTRIM(RTRIM(@OPERATOR_NAME));

	SET @tmpDesc = LTRIM(RTRIM(@LEASE_DESCRIPTION)); 
    SET @strTemp2 += @tmpOpName + space(50 - len(@tmpOpName)) +  @tmpDesc + space(80 - len(@tmpDesc)) 
				 +  FORMAT(@acres, '0#####.##0');
 
    ---*******************************************************************
	-- OPEN LeaseTaxUnit table  
	---*******************************************************************
 	DECLARE rsTemp2_cursor CURSOR FOR
	SELECT A.leaseId
		,  A.taxUnitPercent
		,  B.shortDescription 

		FROM MINERALPRO.dbo.LeaseTaxUnit A
		INNER JOIN  MINERALPRO.dbo.SystemCode B 
		   ON  A.appraisalYear = B.appraisalYear
 		   AND A.taxUnitTypeCd = B.codeType
		   AND A.taxUnitCd = B.code
 
		INNER JOIN  MINERALPRO.dbo.TaxUnit C
		   ON A.appraisalYear = C.appraisalYear
		   AND A.taxUnitTypeCd = C.taxUnitTypeCd
		   AND A.taxUnitCd = C.taxUnitCd

		WHERE A.appraisalYear = @appraisalYear_input
		    AND A.leaseId = @leaseId
			AND C.taxingInd = 'Y' 
			AND A.rowDeleteFlag <> 'D'

	    ORDER BY  A.leaseId, B.shortDescription

 	OPEN rsTemp2_cursor
	FETCH NEXT FROM rsTemp2_cursor INTO @rstemp2_leaseId, @rstemp2_taxUnitPercent,@rstemp2_shortDescription

  	IF @@FETCH_STATUS <> 0 -- Record not found, just pad with space rsTemp2_cursor
	  BEGIN
	     SET @strTemp3 += space(2);
	     SET @strTemp4 += space(11);
	  END
	
    ---*******************************************************************
	-- set the total counts for the TOTAL report  
	---*******************************************************************
	SET @loopCount = 0;
	WHILE @@FETCH_STATUS = 0
	  BEGIN
		--*******************************************************
		-- Set LegacyValue
		--*******************************************************
 		 SET @totalTaxUnitPct = MINERALPRO.dbo.udfBankRound(MINERALPRO.dbo.ReturnValue(@dblVal)*@rstemp2_taxUnitPercent, 0);
		 SET @strTemp3 += LTRIM(RTRIM(@rstemp2_shortDescription));
		 SET @strTemp4 += FORMAT(@totalTaxUnitPct, '0##########');

		 SET @loopCount += 1;				
 		 FETCH NEXT FROM rsTemp2_cursor INTO @rstemp2_leaseId, @rstemp2_taxUnitPercent,@rstemp2_shortDescription;
	  END
 	--*****************************************************************
	-- fill up the rest of legacy value with space up to total 10 count
	--*****************************************************************
	WHILE @loopCount < 10
	  BEGIN
		SET @strTemp3 += space(2);
	    SET @strTemp4 += space(11);
		SET @loopCount += 1;	
	  END
     
    CLOSE rsTemp2_cursor;
    DEALLOCATE rsTemp2_cursor;  

	--*****************************************************************
	-- Keep the total value
	-- don't add the dblVal here if it's a different owner group
	-- If it's the same owner group, increment the total value
	--*****************************************************************
	IF (@prevOwner = @ownerId)
	  BEGIN
		 SET @dblTotHoldingsValue +=  @dblVal;
		 SET @PREV_OWNER_RS3430_LEGACY_VALUE = @RS3430_LEGACY_VALUE
	  END
 	--*****************************************************************
	-- IF EOF or current owner  <> previous owner
	--*****************************************************************
	IF (@prevOwner <> @ownerId) OR (@applCount = @TOTAL_OWNER_COUNT) -- rstemp_cursor main loop EOF
	   BEGIN
	      -- get the total exempt code
		 IF @dblTotHoldingsValue < 500  
			 SET @strTotalExemptCode = 'Z';
		 ELSE
			 SET @strTotalExemptCode = @PREV_OWNER_RS3430_LEGACY_VALUE;  
	      --****************************************************************
		  -- The very record, hasn't written to tempTbl yet
		  --****************************************************************
	      IF @applCount = @TOTAL_OWNER_COUNT
		  	 INSERT INTO #tmpTbl_c VALUES (@strTemp1, @strTemp2, @strTemp3, @strTemp4);

		  IF LEN(LTRIM(RTRIM(@strTotalExemptCode))) = 0  
            SET @strTotalExemptCode = space(1);

		  --**************************************************************
		  -- Need to update the temp table with totalExemptCode
		  -- Then construct the string and insert into final output
		  --**************************************************************
		  DECLARE ownerTemp_cursor CURSOR FOR
		  SELECT oStr1, oStr2, oStr3, oStr4
		     FROM #tmpTbl_c
 		  OPEN ownerTemp_cursor
		  FETCH NEXT FROM ownerTemp_cursor
			INTO @oStr1, @oStr2, @oStr3, @oStr4

		  WHILE @@FETCH_STATUS = 0
		    BEGIN
			  SET @strTemp = @oStr1 + @strTotalExemptCode + @oStr2 + @oStr3 + @oStr4;
			  INSERT INTO #tmpTbl_c2 VALUES (@idCount, @strTemp);
			  SET @idCount += 1;
			  FETCH NEXT FROM ownerTemp_cursor
				INTO @oStr1, @oStr2, @oStr3, @oStr4
			END

		  SET @prevOwner = @ownerId;
		  CLOSE ownerTemp_cursor;
		  DEALLOCATE ownerTemp_cursor;
		--*****************************************************************
		-- empty out the tmpTbl for the next Owner
		--*****************************************************************
		  TRUNCATE TABLE #tmpTbl_c;
		  INSERT INTO #tmpTbl_c VALUES (@strTemp1, @strTemp2, @strTemp3, @strTemp4);
 
		 --*****************************************************
		 -- Different Owner, reset the value
		 --*****************************************************
	     SET @dblTotHoldingsValue =  @dblVal;
  		 SET @PREV_OWNER_RS3430_LEGACY_VALUE = @RS3430_LEGACY_VALUE
	   END
	ELSE
		--*****************************************************************
		--*****************************************************************
	   BEGIN
	   	 INSERT INTO #tmpTbl_c VALUES (@strTemp1, @strTemp2, @strTemp3, @strTemp4);
	   END
 
    ---*******************************************************************
	-- read next record from rsTemp cursor -- MAIN Query
	-- Don't fetch next record when reaching EOF
	---*******************************************************************
 
	FETCH NEXT FROM rsTemp_cursor
		 INTO @TOTAL_OWNER_COUNT
		    , @leaseId 
			, @interestTypeCd 
			, @interestPercent 
			, @description   
			, @OWNER_NAME 
			, @ADDRESS_LINE1 
			, @ADDRESS_LINE3 
			, @CITY 
			, @STATE_CD 
			, @STATE_DESC 
			, @ZIPCODE 
			, @ownerId  
			, @leaseName 
			, @OPERATOR_NAME 
			, @ownerValue 
			, @workingInterestPercent 
			, @leaseYear 
			, @acres   
			, @LEASE_RI_PCT_SUM 
			, @TAX_REP_ID  
			, @royaltyInterestValue 
			, @workingInterestValue 
			, @LEASE_DESCRIPTION 
			, @unitId 
			, @operatorId  
			, @RSTEMP2_OPERATOR_NUM 
			, @RSTEMP2_OPERATOR_NAME  
			, @RSTEMP2_EXEMPTION_CODE 
			, @RS3430_LEGACY_VALUE 
	 		, @ST_description
 	  
   END

  --***************************************************************************
  -- End, cleaning up
  --***************************************************************************
  CLOSE rsTemp_cursor;
  DEALLOCATE rsTemp_cursor
  
  SELECT outputField FROM #tmpTbl_c2  
     ORDER BY id
  DROP TABLE #tmpTbl_c;
  DROP TABLE #tmpTbl_c2;

END



GO


