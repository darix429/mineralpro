USE [MINERALPRO]
GO

/****** Object:  StoredProcedure [dbo].[spExportCertifiedFormat_Summary]    Script Date: 9/20/2017 9:10:31 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



--=============================================
-- Author:		Foomay
-- Create date: 2017-09-02
-- Description:	Export public records
-- =============================================
CREATE PROCEDURE [dbo].[spExportCertifiedFormat_Summary] 
	-- Add the parameters for the stored procedure here
	@appraisalYear_input smallint = 2017
AS
BEGIN 

 DECLARE @dblTotHoldingsValue bigint =  0;
 DECLARE @dblVal Float = 0;
 DECLARE @RI_INTEREST_TYPE_CD smallint = 1; 
 DECLARE @OR_INTEREST_TYPE_CD smallint = 2;
 DECLARE @OP_INTEREST_TYPE_CD smallint = 3;
 DECLARE @WI_INTEREST_TYPE_CD smallint = 4;
 DECLARE @tempInterestValue Float = 0;
 DECLARE @totalTaxUnitPct Float = 0;
 DECLARE @lng10c Float = 0
  ,@lng20c Float = 0  
  ,@lng30c Float = 0  
  ,@lng40c Float = 0  
  ,@lng50c Float = 0
  ,@lng60c Float = 0
  ,@lng70c Float = 0
  ,@lng80c Float = 0
  ,@lng90c Float = 0

  ,@dbl10 Float = 0
  ,@dbl20 Float = 0
  ,@dbl30 Float = 0
  ,@dbl40 Float = 0
  ,@dbl50 Float = 0
  ,@dbl60 Float = 0
  ,@dbl70 Float = 0
  ,@dbl80 Float = 0
  ,@dbl90 Float = 0

 DECLARE  @leaseId bigint
	, @interestTypeCd smallint
	, @interestPercent decimal(9,8)
	, @workingInterestPercent decimal(9,8)
    , @royaltyInterestValue decimal(13,0)
	, @workingInterestValue decimal(13,0)
	, @LEASE_RI_PCT_SUM decimal(9,8)

	, @rstemp2_leaseId bigint
	, @rstemp2_taxUnitPercent decimal(9,8)
	, @rstemp2_shortDescription varchar(25)

---*******************************************************************
--- Create a temp table to hold the records per each OWNER
---*******************************************************************

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF OBJECT_ID (N'tempdb..#tmpTbl', N'U') IS NOT NULL
	  DROP TABLE #tmpTbl
 
---*******************************************************************
-- Construct the main Query, save the total owner count for the last
-- owner processing
---*******************************************************************
DECLARE rsTemp_cursor CURSOR FOR
    SELECT A.leaseId
	, A.interestTypeCd 
	, A.interestPercent
	, C.workingInterestPercent
    ,(C.oilPaymentPercent + C.overridingRoyaltyPercent + C.royaltyInterestPercent) AS LEASE_RI_PCT_SUM
    , C.royaltyInterestValue
	, C.workingInterestValue
 	FROM MINERALPRO.dbo.LeaseOwner A 
	INNER JOIN MINERALPRO.dbo.Lease B 
		ON A.appraisalYear = B.appraisalYear 
		AND A.leaseId = B.leaseId  

	INNER JOIN MINERALPRO.dbo.LeaseValueSummary C  
		ON A.appraisalYear = C.appraisalYear
		AND A.leaseId = C.leaseId  

	LEFT OUTER JOIN MINERALPRO.dbo.Operator E
		ON C.appraisalYear = E.appraisalYear  
		AND B.operatorId = E.operatorId  

	INNER JOIN MINERALPRO.dbo.SystemCode D  
		ON A.appraisalYear = D.appraisalYear 
		AND A.interestTypeCd = D.code 

		AND D.codeType = 3000 
    
	LEFT OUTER JOIN MINERALPRO.dbo.Owner F 
		ON A.appraisalYear = F.appraisalYear 
		AND A.ownerId = F.ownerId  

	LEFT OUTER JOIN MINERALPRO.dbo.Unit EUM 
		ON EUM.appraisalYear = A.appraisalYear 
		AND EUM.unitId = B.unitId 
		AND EUM.rowDeleteFlag <> 'D' 

	LEFT OUTER JOIN MINERALPRO.dbo.Operator MOE 
		ON MOE.appraisalYear = A.appraisalYear  
		AND MOE.operatorId = EUM.operatorId 

	LEFT OUTER JOIN MINERALPRO.dbo.OwnerExemption MOEA 
		ON MOEA.appraisalYear = A.appraisalYear 
		AND MOEA.ownerId = A.ownerId 
		AND MOEA.exemptionTypeCd = 3440 
		AND MOEA.rowDeleteFlag <> 'D' 

	LEFT OUTER JOIN MINERALPRO.dbo.OwnerExemption MOEB 
		ON MOEB.appraisalYear = A.appraisalYear 
		AND MOEB.ownerId = A.ownerId 
		AND MOEB.exemptionTypeCd =  3430 
		AND MOEB.rowDeleteFlag <> 'D' 

	LEFT OUTER JOIN MINERALPRO.dbo.SystemCode CTM 
		ON CTM.appraisalYear = A.appraisalYear 
		AND CTM.codeType = 3430 
		AND CTM.CODE = MOEB.exemptionCd 

	LEFT OUTER JOIN MINERALPRO.dbo.vSystemCode_and_Type ST 
		ON ST.appraisalYear = A.appraisalYear 
		AND ST.codeTypeDesc  = 'STATE CODES' 
		AND ST.code = F.stateCd 

	WHERE A.appraisalYear =  @appraisalYear_input  
		AND A.rowDeleteFlag <> 'D'  
 
	ORDER BY A.ownerId, CAST(A.leaseId AS INTEGER)

	OPEN rsTemp_cursor

    FETCH NEXT FROM rsTemp_cursor
	INTO @leaseId 
	, @interestTypeCd 
	, @interestPercent 
	, @workingInterestPercent 
    , @LEASE_RI_PCT_SUM 
    , @royaltyInterestValue 
	, @workingInterestValue 

  --**********************************************************************
  -- Loop thru the entire LeaseOwner table
  -- Also process the very last owner 
  --**********************************************************************
  WHILE @@FETCH_STATUS = 0  
  BEGIN
	--**********************************************************************
	-- process working Interest Type Code 
	--**********************************************************************  
 
	IF @interestTypeCd = @WI_INTEREST_TYPE_CD  
	BEGIN   
		IF @workingInterestValue > 0 And @workingInterestPercent > 0  
		  BEGIN
		    SET @tempInterestValue = MINERALPRO.dbo.udfBankRound(@workingInterestValue / @workingInterestPercent * @interestPercent, 0);
			IF @tempInterestValue < 1  
				SET @dblVal = 1;
			ELSE
				SET @dblVal = @tempInterestValue;
		  END
	    ELSE   
			SET @dblVal = 0;
	END
	--**********************************************************************
	-- Otherwise, process OR, OP, RI interest type code
	-- Not using IF -- ELSE as too many nesting IF statement -- too confusing
	-- It's easier to read this way
	--**********************************************************************
 	IF   @interestTypeCd = @OR_INTEREST_TYPE_CD  
	  OR @interestTypeCd = @RI_INTEREST_TYPE_CD
	  OR @interestTypeCd = @OP_INTEREST_TYPE_CD
	BEGIN
 		IF @royaltyInterestValue > 0 AND @LEASE_RI_PCT_SUM > 0
		  BEGIN			  
			SET @tempInterestValue = MINERALPRO.dbo.udfBankRound(@royaltyInterestValue / @LEASE_RI_PCT_SUM * @interestPercent, 0);

  			IF @tempInterestValue < 1  
				SET @dblVal = 1;
			ELSE
				SET @dblVal = @tempInterestValue;
 		  END
	    ELSE   
		  BEGIN
			SET @dblVal = 0;
 		  END
	END
    ---*******************************************************************
	-- OPEN LeaseTaxUnit table  
	---*******************************************************************
 	DECLARE rsTemp2_cursor CURSOR FOR
	SELECT A.leaseId
		,  A.taxUnitPercent
		,  B.shortDescription 

		FROM MINERALPRO.dbo.LeaseTaxUnit A
		INNER JOIN  MINERALPRO.dbo.SystemCode B 
		   ON  A.appraisalYear = B.appraisalYear
 		   AND A.taxUnitTypeCd = B.codeType
		   AND A.taxUnitCd = B.code
 
		INNER JOIN  MINERALPRO.dbo.TaxUnit C
		   ON A.appraisalYear = C.appraisalYear
		   AND A.taxUnitTypeCd = C.taxUnitTypeCd
		   AND A.taxUnitCd = C.taxUnitCd

		WHERE A.appraisalYear = @appraisalYear_input
		    AND A.leaseId = @leaseId
			AND C.taxingInd = 'Y' 
			AND A.rowDeleteFlag <> 'D'

	    ORDER BY  A.leaseId, B.shortDescription

 	OPEN rsTemp2_cursor
	FETCH NEXT FROM rsTemp2_cursor INTO @rstemp2_leaseId, @rstemp2_taxUnitPercent,@rstemp2_shortDescription
	
    ---*******************************************************************
	-- set the total counts for the TOTAL report  
	---*******************************************************************
 	WHILE @@FETCH_STATUS = 0
	  BEGIN
		 SET @totalTaxUnitPct = MINERALPRO.dbo.udfBankRound(MINERALPRO.dbo.ReturnValue(@dblVal)*@rstemp2_taxUnitPercent, 0);
		 IF @rstemp2_shortDescription = '10'
		   BEGIN
			SET @lng10c += 1;   
			SET @dbl10  += @totalTaxUnitPct;
		   END
		 IF @rstemp2_shortDescription = '20'
		   BEGIN
			SET @lng20c += 1;   
			SET @dbl20  += @totalTaxUnitPct;
		   END
		 IF @rstemp2_shortDescription = '30'
		   BEGIN
			SET @lng30c += 1;   
			SET @dbl30  += @totalTaxUnitPct;
		   END
		 IF @rstemp2_shortDescription = '40'
		   BEGIN
			SET @lng40c += 1;   
			SET @dbl40  += @totalTaxUnitPct;
		   END
		 IF @rstemp2_shortDescription = '50'
		   BEGIN
			SET @lng50c += 1;   
			SET @dbl50  += @totalTaxUnitPct;
		   END
		 IF @rstemp2_shortDescription = '60'
		   BEGIN
			SET @lng60c += 1;   
			SET @dbl60  += @totalTaxUnitPct;
		   END
		 IF @rstemp2_shortDescription = '70'
		   BEGIN
			SET @lng70c += 1;   
			SET @dbl70 +=  @totalTaxUnitPct;
		   END
		 IF @rstemp2_shortDescription = '80'
		   BEGIN
			SET @lng80c += 1;   
			SET @dbl80  += @totalTaxUnitPct;
		   END
		 IF @rstemp2_shortDescription = '90'
		   BEGIN
			SET @lng90c += 1;   
			SET @dbl90  += @totalTaxUnitPct;
		   END
		  
		--*******************************************************

  		 FETCH NEXT FROM rsTemp2_cursor INTO @rstemp2_leaseId, @rstemp2_taxUnitPercent,@rstemp2_shortDescription;
	  END
 
     
    CLOSE rsTemp2_cursor;
    DEALLOCATE rsTemp2_cursor;  	  
    ---*******************************************************************
	-- read next record from rsTemp cursor -- MAIN Query
	-- Don't fetch next record when reaching EOF
	---*******************************************************************
 
	FETCH NEXT FROM rsTemp_cursor
		 INTO @leaseId 
			, @interestTypeCd 
			, @interestPercent 
			, @workingInterestPercent 
			, @LEASE_RI_PCT_SUM 
			, @royaltyInterestValue 
			, @workingInterestValue 

   END
  --***************************************************************************
  -- Write the totals to the summary report
  --***************************************************************************
  
BEGIN 
	IF OBJECT_ID (N'tempdb..#tempOutput', N'U') IS NOT NULL
	  DROP TABLE #tempOutput

	CREATE TABLE #tempOutput (
			outputField nvarchar(500)
	);

   INSERT INTO #tempOutput VALUES('Juris 10 Count : ' + FORMAT(@lng10c, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 10 Amount: ' + FORMAT(@dbl10, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 20 Count : ' + FORMAT(@lng20c, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 20 Amount: ' + FORMAT(@dbl20, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 30 Count : ' + FORMAT(@lng30c, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 30 Amount: ' + FORMAT(@dbl30, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 40 Count : ' + FORMAT(@lng40c, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 40 Amount: ' + FORMAT(@dbl40, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 50 Count : ' + FORMAT(@lng50c, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 50 Amount: ' + FORMAT(@dbl50, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 60 Count : ' + FORMAT(@lng60c, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 60 Amount: ' + FORMAT(@dbl60, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 70 Count : ' + FORMAT(@lng70c, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 70 Amount: ' + FORMAT(@dbl70, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 80 Count : ' + FORMAT(@lng80c, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 80 Amount: ' + FORMAT(@dbl80, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 90 Count : ' + FORMAT(@lng90c, '###0'));
   INSERT INTO #tempOutput VALUES('Juris 90 Amount: ' + FORMAT(@dbl90, '###0'));

   SELECT * FROM #tempOutput;
   DROP TABLE #tempOutput;

  --***************************************************************************
  -- End, cleaning up
  --***************************************************************************
  CLOSE rsTemp_cursor;
  DEALLOCATE rsTemp_cursor
   
  --DROP TABLE #tmpTbl;
 
END

END




GO


