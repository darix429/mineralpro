USE [MINERALPRO]
GO

/****** Object:  UserDefinedFunction [dbo].[ReturnValue]    Script Date: 9/20/2017 10:08:08 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[ReturnValue](@dblVal FLOAT)  
RETURNS FLOAT 
AS
BEGIN

 DECLARE @value FLOAT 
  -- value greater than 0 round to nearest whole number 
 IF @dblVal > 0  
    SET @value = MINERALPRO.dbo.udfBankRound(@dblVal, 0)

  -- if value is less than 1 dollar, but greater than 0 make 1 dollar
 IF @dblVal < 1  
    SET @value = 1
     
 Return  @value
   
END




GO


