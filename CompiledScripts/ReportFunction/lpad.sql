USE [MINERALPRO]
GO

/****** Object:  UserDefinedFunction [dbo].[lpad]    Script Date: 9/20/2017 10:07:40 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [dbo].[lpad]
 (
  @mstr AS varchar(200),
  @nofchars AS int,
  @fillchar AS varchar(200)=' '
 )
RETURNS varchar(200)
AS
BEGIN
 RETURN
  CASE
   WHEN LEN(@mstr) >= @nofchars THEN SUBSTRING(@mstr,1,@nofchars)
   ELSE
    SUBSTRING(REPLICATE(@fillchar,@nofchars),
     1,@nofchars-LEN(@mstr))+@mstr
  END
END



GO


