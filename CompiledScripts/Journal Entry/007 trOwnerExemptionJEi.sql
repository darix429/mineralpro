Use MINERALPRO
GO


IF (OBJECT_ID('trOwnerExemptionJEi') IS NOT NULL)
  DROP TRIGGER dbo.trOwnerExemptionJEi
GO


CREATE TRIGGER dbo.trOwnerExemptionJEi
   ON  dbo.OwnerExemption
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON

   --Uses MAX to prevent sub-queries from returning multiple values

   Declare @idJournalEntry bigint
   Declare @appraisalYear smallint
   Declare @currentYear smallint
   Declare @result bigint
   Declare @appUser bigint

   --Capture Userid
   Select @appUser = max([rowUpdateUserid]) From inserted

   --Get current year
   Select @currentYear = appraisalYear FROM dbo.AppraisalYear Where currentInd = 'Y'
   --Get year updated
   Select @appraisalYear = max(appraisalYear) from Inserted

   --If prior year, then proceed
   if @currentYear > @appraisalYear
   Begin
      -- Get JournalEntry ID to place on child tables
      Select @idJournalEntry = Isnull(b.idJournalEntry,0)
        From JournalEntry b
       Where b.appUser = (Select max([rowUpdateUserid]) From inserted)
         And b.closeDt = '1900-01-01'
         And b.appraisalYear = @appraisalYear
         
      --If JournalEntry Id found then proceed (should always be found)
      If @idJournalEntry <> 0
      Begin
         -- Capture before values (row 0)
         Insert into jeOwnerExemption (idJournalEntry, seqNumber, idOwnerExemption, appraisalYear, ownerId, exemptionTypeCd, exemptionCd)
         Select @idJournalEntry, 0, d.idOwnerExemption, d.appraisalYear, d.ownerId, d.exemptionTypeCd, d.exemptionCd
           From inserted d

         --Capture the after value
         Insert into jeOwnerExemption
         Select @idJournalEntry, 1, i.*
            From inserted i

         Execute dbo.spJeOwnerExtras @idJournalEntry, @appUser, @appraisalYear
      End
   End
END
GO
