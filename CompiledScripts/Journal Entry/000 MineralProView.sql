Use MINERALPRO
go
if not exists (select * from dbo.sysusers where name = N'guest' and uid < 16382 and hasdbaccess = 1)
	EXEC sp_grantdbaccess N'guest'
GO

/****** Object:  User guest    Script Date: 12/13/2007 5:24:57 PM ******/
exec sp_addrolemember N'db_datareader', N'guest'
GO

/****** Object:  User guest    Script Date: 12/13/2007 5:24:57 PM ******/
exec sp_addrolemember N'db_datawriter', N'guest'
GO


grant REFERENCES ,  SELECT on Agency to public;
grant REFERENCES ,  SELECT on App to public;
grant REFERENCES ,  SELECT on Appraisal to public;
grant REFERENCES ,  SELECT on AppraisalYear to public;
grant REFERENCES ,  SELECT on ApprAmortSchedule to public;
grant REFERENCES ,  SELECT on ApprDeclineSchedule to public;
grant REFERENCES ,  SELECT on AppSubSystem to public;
grant REFERENCES ,  SELECT on AppUser to public;
grant REFERENCES ,  SELECT on AppUserFavorite to public;
grant REFERENCES ,  SELECT on AppUserSubSystemFavorite to public;
grant REFERENCES ,  SELECT on ArbParticipant to public;
grant REFERENCES ,  SELECT on CalcAmortSchedule to public;
grant REFERENCES ,  SELECT on CalcAppraisalSummary to public;
grant REFERENCES ,  SELECT on CalcDeclineSchedule to public;
grant REFERENCES ,  SELECT on CalcEquipmentPVSchedule to public;
grant REFERENCES ,  SELECT on CalcEquipmentSchedule to public;
grant REFERENCES ,  SELECT on CalcEscalatedOpExpAdj to public;
grant REFERENCES ,  SELECT on CalcEscalatedPriceAdj to public;
grant REFERENCES ,  SELECT on CalcTaxAdj to public;
grant REFERENCES ,  SELECT on Exemption to public;
grant REFERENCES ,  SELECT on Field to public;
grant REFERENCES ,  SELECT on FunctionSecurity to public;
grant REFERENCES ,  SELECT on Lease to public;
grant REFERENCES ,  SELECT on LeaseOwner to public;
grant REFERENCES ,  SELECT on LeaseTaxUnit to public;
grant REFERENCES ,  SELECT on LeaseValueSummary to public;
grant REFERENCES ,  SELECT on Note to public;
grant REFERENCES ,  SELECT on NoteType to public;
grant REFERENCES ,  SELECT on Operator to public;
grant REFERENCES ,  SELECT on Organization to public;
grant REFERENCES ,  SELECT on OrganizationContact to public;
grant REFERENCES ,  SELECT on Owner to public;
grant REFERENCES ,  SELECT on OwnerExemption to public;
grant REFERENCES ,  SELECT on QuickNote to public;
grant REFERENCES ,  SELECT on Report to public;
grant REFERENCES ,  SELECT on ReportScript to public;
grant REFERENCES ,  SELECT on SecurityGroup to public;
grant REFERENCES ,  SELECT on SubjectList to public;
grant REFERENCES ,  SELECT on SubSystem to public;
grant REFERENCES ,  SELECT on SubSystemFunction to public;
grant REFERENCES ,  SELECT on SystemCode to public;
grant REFERENCES ,  SELECT on SystemCodeGroup to public;
grant REFERENCES ,  SELECT on SystemCodeType to public;
grant REFERENCES ,  SELECT on SystemSetting to public;
grant REFERENCES ,  SELECT on TaxUnit to public;
grant REFERENCES ,  SELECT on TaxUnitExemption to public;
grant REFERENCES ,  SELECT on Unit to public;
grant REFERENCES ,  SELECT on UnitValueSummary to public;
grant REFERENCES ,  SELECT on UserFunction to public;
grant REFERENCES ,  SELECT on UserOrganization to public;
grant REFERENCES ,  SELECT on UserOrgSecurity to public;
go
use MineralProView
go
if not exists (select * from dbo.sysusers where name = N'guest' and uid < 16382 and hasdbaccess = 1)
	EXEC sp_grantdbaccess N'guest'
GO

/****** Object:  User guest    Script Date: 12/13/2007 5:24:57 PM ******/
exec sp_addrolemember N'db_datareader', N'guest'
GO

/****** Object:  User guest    Script Date: 12/13/2007 5:24:57 PM ******/
exec sp_addrolemember N'db_datawriter', N'guest'
GO
grant REFERENCES ,  SELECT on ACCOUNT_APPRL_YEAR to public;
grant REFERENCES ,  SELECT on ACCOUNT_JURISDICTIONS to public;
grant REFERENCES ,  SELECT on CODE_TABLE_MASTER to public;
grant REFERENCES ,  SELECT on ECON_UNIT_MRL to public;
grant REFERENCES ,  SELECT on ECONOMIC_UNIT to public;
grant REFERENCES ,  SELECT on MRL_APPRL_YEAR to public;
grant REFERENCES ,  SELECT on MRL_OBJECT to public;
grant REFERENCES ,  SELECT on MRL_OPERATORS to public;
grant REFERENCES ,  SELECT on MRL_OWNER to public;
grant REFERENCES ,  SELECT on MRL_OWNER_EXEMPTIONS to public;
grant REFERENCES ,  SELECT on MRL_RRC_FIELDS to public;
grant REFERENCES ,  SELECT on OWNER to public;
grant REFERENCES ,  SELECT on OWNER_ADDRESS to public;
grant REFERENCES ,  SELECT on TAX_JURIS to public;
go


/****** Object:  Table [dbo].[LEGAL_INFORMATION]    Script Date: 09/25/2017 19:27:08 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

Drop TABLE [dbo].[LEGAL_INFORMATION]
GO

CREATE TABLE [dbo].[LEGAL_INFORMATION](
	[APPRAISAL_YR] [smallint] NOT NULL,
	[ACCOUNT_NUM] [varchar](17) NOT NULL,
	[EFFECTIVE_DT] [datetime] NOT NULL,
	[EXPIRATION_DT] [datetime] NOT NULL,
	[TRANSFER_DT] [datetime] NOT NULL,
	[RECEPTION_NUMBER] [varchar](50) NULL,
	[TRANSFER_CODE] [varchar](50) NULL,
	[TRANSFER_PRCS_DT] [datetime] NOT NULL,
	[SUBDIVISION_ID] [varchar](5) NOT NULL,
	[LOT] [varchar](3) NOT NULL,
	[BLOCK] [varchar](5) NOT NULL,
	[DESCRIPTION_LINE1] [varchar](33) NOT NULL,
	[DESCRIPTION_LINE2] [varchar](33) NOT NULL,
	[DESCRIPTION_LINE3] [varchar](33) NOT NULL,
	[DESCRIPTION_LINE4] [varchar](33) NOT NULL,
	[DESCRIPTION_LINE5] [varchar](33) NOT NULL,
	[SPLIT_NUM] [varchar](2) NOT NULL,
	[ABSTRACT_NUM] [varchar](4) NOT NULL,
	[PAGE_NUM] [varchar](3) NOT NULL,
	[SECTION_NUM] [char](1) NOT NULL,
	[TRACT_NUM] [varchar](3) NOT NULL,
	[OVER_NUM] [varchar](3) NOT NULL,
	[FRACTION_CD] [smallint] NOT NULL,
	[LST_UPDT_EMPL_ID] [varchar](8) NOT NULL,
	[LST_UPDT_TS] [datetime] NOT NULL,
	[POST_FLG] [char](1) NOT NULL,
	[BOOK] [varchar](50) NULL,
	[TOWNSHIP] [varchar](50) NULL,
	[RANGE] [varchar](50) NULL,
 CONSTRAINT [PK_LEGAL_INFORMATION] PRIMARY KEY CLUSTERED 
(
	[APPRAISAL_YR] ASC,
	[ACCOUNT_NUM] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON, FILLFACTOR = 80) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__APPRA__70499252]  DEFAULT (0) FOR [APPRAISAL_YR]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__ACCOU__713DB68B]  DEFAULT ('') FOR [ACCOUNT_NUM]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__EFFEC__7231DAC4]  DEFAULT (getdate()) FOR [EFFECTIVE_DT]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__EXPIR__7325FEFD]  DEFAULT (getdate()) FOR [EXPIRATION_DT]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__TRANS__741A2336]  DEFAULT (getdate()) FOR [TRANSFER_DT]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__TRANS__76026BA8]  DEFAULT (getdate()) FOR [TRANSFER_PRCS_DT]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__SUBDI__76F68FE1]  DEFAULT ('') FOR [SUBDIVISION_ID]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INFOR__LOT__77EAB41A]  DEFAULT ('') FOR [LOT]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__BLOCK__78DED853]  DEFAULT ('') FOR [BLOCK]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__DESCR__79D2FC8C]  DEFAULT ('') FOR [DESCRIPTION_LINE1]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__DESCR__7AC720C5]  DEFAULT ('') FOR [DESCRIPTION_LINE2]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__DESCR__7BBB44FE]  DEFAULT ('') FOR [DESCRIPTION_LINE3]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__DESCR__7CAF6937]  DEFAULT ('') FOR [DESCRIPTION_LINE4]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__DESCR__7DA38D70]  DEFAULT ('') FOR [DESCRIPTION_LINE5]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__SPLIT__7E97B1A9]  DEFAULT ('') FOR [SPLIT_NUM]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__ABSTR__7F8BD5E2]  DEFAULT ('') FOR [ABSTRACT_NUM]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__PAGE___007FFA1B]  DEFAULT ('') FOR [PAGE_NUM]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__SECTI__01741E54]  DEFAULT ('') FOR [SECTION_NUM]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__TRACT__0268428D]  DEFAULT ('') FOR [TRACT_NUM]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__OVER___035C66C6]  DEFAULT ('') FOR [OVER_NUM]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__FRACT__04508AFF]  DEFAULT (0) FOR [FRACTION_CD]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__LST_U__0544AF38]  DEFAULT (user_name()) FOR [LST_UPDT_EMPL_ID]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__LST_U__0638D371]  DEFAULT (getdate()) FOR [LST_UPDT_TS]
GO

ALTER TABLE [dbo].[LEGAL_INFORMATION] ADD  CONSTRAINT [DF__LEGAL_INF__POST___072CF7AA]  DEFAULT ('I') FOR [POST_FLG]
GO


Drop View [dbo].[TAXPAYER_REP]
GO
CREATE VIEW [dbo].[TAXPAYER_REP]
AS
SELECT        appraisalYear AS APPRAISAL_YR, agencyCdx AS TAXPAYER_REP_ID, agencyName AS NAME, addrLine1 AS ADDRESS_LINE1, addrLine2 AS ADDRESS_LINE2, addrLine3 AS ADDRESS_LINE3, city AS CITY, 
                         stateCd AS STATE_CD, zipcode AS ZIPCODE, countryCd, phoneNum AS PHONE_NUM, faxNum AS FAX_NUM, emailAddress, statusCd AS ACTIVE_IND, rowUpdateUserid AS LST_UPDT_EMPL_ID, rowUpdateDt AS LST_UPDT_TS, 
                         rowDeleteFlag AS POST_FLG
FROM            MINERALPRO.dbo.Agency
GO


Drop View [dbo].[APPRAISAL_YEAR]
GO
CREATE VIEW [dbo].[APPRAISAL_YEAR]
AS
SELECT        appraisalYear AS APPRAISAL_YR, currentInd AS CURR_APPRL_YR_IND, futureInd AS FUTR_APPRL_YR_IND, collectionInd AS COLL_APPRL_YR_IND, effectiveDt AS EFFECTIVE_DT, expirationDt AS EXPIRATION_DT, 
                         rowUpdateUserid AS LST_UPDT_EMPL_ID, rowUpdateDt AS LST_UPDT_TS, rowDeleteFlag AS POST_FLG
FROM            MINERALPRO.dbo.AppraisalYear
GO
