Use MINERALPRO
GO
/*
SELECT top 100 *
  FROM [MINERALPRO].[dbo].[Appraisal]
 where appraisalyear = 2016
   and subjectTypeCd = 2  --Leases
   --and subjecttypecd = 1  --Units
*/

Declare @appraisalYear smallint = 2016
Declare @appUser int = 60
Declare @subjectTypeCd int = 2
Declare @subjectId int = 1500
Declare @jdID bigint = 0

--Calculations, percents and dollar changes are PRIOR year
Insert into JournalEntry (appUser, appraisalYear, [jeReasonCd], description)
Values (@appUser, @appraisalYear, 1, 'Test Value change Prior Year')

Select @jdID = @@IDENTITY

execute dbo.spCalcAmortizationYears @appUser,@appraisalYear,25,1,@subjectTypeCd,@subjectId,1

Update [MINERALPRO].[dbo].[JournalEntry] Set closeDt = getdate() where idJournalEntry = @jdID


--Owner changes make their own JE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--Owner changes make their own JE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
----Set @appraisalYear = 2017
------Ownership changes are made in CURRENT year
----Insert into JournalEntry (appUser, appraisalYear, [jeReasonCd], description)
----Values (@appUser, @appraisalYear, 1, 'Test Owner change Current Year')

Select @jdID = @@IDENTITY

Update dbo.Owner
set name1 = 'JEREMY WAS HERE'
WHERE idOwner = 29289



Insert into JournalEntry (appUser, appraisalYear, [jeReasonCd], description, rowUpdateUserid)
Values (@appUser, @appraisalYear, 1, 'Change Int Type test',@appUser)

Select @jdID = @@IDENTITY


set @subjectTypeCd  = 2
set @subjectId  = 100018

update leaseowner
set interesttypecd = 2
,rowupdateuserid = @appUser
where ownerid = 295150


execute dbo.spCalcAmortizationYears @appUser,@appraisalYear,25,1,@subjectTypeCd,@subjectId,1

Update [MINERALPRO].[dbo].[JournalEntry] Set closeDt = getdate() where idJournalEntry = @jdID


Select 'JournalEntry' as TableName, * from dbo.JournalEntry

Select 'jeLeaseOwner' as TableName, *
  From [MINERALPRO].[dbo].[jeLeaseOwner]

Select 'jeLeaseTaxUnit' as TableName, *
  From [MINERALPRO].[dbo].[jeLeaseTaxUnit]

Select 'jeOwner' as TableName, *
  From [MINERALPRO].[dbo].[jeOwner]

Select 'jeOwnerExemption' as TableName, *
  From [MINERALPRO].[dbo].[jeOwnerExemption]



/*
Truncate Table dbo.JournalEntry
Truncate Table dbo.JournalEntryBatch

Truncate Table [MINERALPRO].[dbo].[jeLeaseOwner]
Truncate Table [MINERALPRO].[dbo].[jeOwner]
Truncate Table [MINERALPRO].[dbo].[jeLeaseOwner]
Truncate Table [MINERALPRO].[dbo].[jeLeaseTaxUnit]

*/