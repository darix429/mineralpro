Use MINERALPRO
GO


IF (OBJECT_ID('trOwnerJEi') IS NOT NULL)
  DROP TRIGGER dbo.trOwnerJEi
GO


CREATE TRIGGER dbo.trOwnerJEi
   ON  dbo.Owner
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON

   --Uses MAX to prevent sub-queries from returning multiple values

   Declare @idJournalEntry bigint
   Declare @appraisalYear smallint
   Declare @currentYear smallint
   Declare @result bigint
   Declare @appUser bigint

   --Capture Userid
   Select @appUser = max([rowUpdateUserid]) From inserted

   --Get current year
   Select @currentYear = appraisalYear FROM dbo.AppraisalYear Where currentInd = 'Y'
   --Get year updated
   Select @appraisalYear = max(appraisalYear) from Inserted

   --If current year, then proceed
   if @currentYear = @appraisalYear
   Begin
      -- Create Journal Entry
      Insert Into dbo.JournalEntry ( appUser        , appraisalYear
                                   , openDt         , closeDt
                                   , jeReasonCd     , description
                                   , rowUpdateUserid, rowUpdateDt
                                   )
      Values ( @appUser
             , @appraisalYear
             , GETDATE()
             , GETDATE()
             , 17  --Owner Change???
             , 'OWNER ADD'
             , @appUser
             , GETDATE()
             )

      -- Get Journal Entry ID
      Set @idJournalEntry = SCOPE_IDENTITY()
         
      -- Capture before values (row 0)
      Insert into jeOwner (idJournalEntry, seqNumber, idOwner, appraisalYear, ownerId)
      Select @idJournalEntry, 0, d.idOwner, d.appraisalYear, d.ownerId
        From inserted d

      --Capture the after value
      Insert into jeOwner (       
       [idJournalEntry]
      ,[seqNumber]
      ,idOwner
      ,appraisalYear
      ,ownerId
      ,name1
      ,name2
      ,name3
      ,addrLine1
      ,addrLine2
      ,addrLine3
      ,city
      ,stateCd
      ,zipcode
      ,countryCd
      ,phoneNum
      ,faxNum
      ,emailAddress
      ,birthDt
      ,confidentialInd
      ,ownerChangeCd
      ,agencyCdx
      ,rowUpdateUserid
      ,rowUpdateDt
      ,rowDeleteFlag)
      Select @idJournalEntry, 1, idOwner
      ,appraisalYear
      ,ownerId
      ,name1
      ,name2
      ,name3
      ,addrLine1
      ,addrLine2
      ,addrLine3
      ,city
      ,stateCd
      ,zipcode
      ,countryCd
      ,phoneNum
      ,faxNum
      ,emailAddress
      ,birthDt
      ,confidentialInd
      ,ownerChangeCd
      ,agencyCdx
      ,rowUpdateUserid
      ,rowUpdateDt
      ,rowDeleteFlag
        From inserted i

      Execute dbo.spJeOwnerExtras @idJournalEntry, @appUser, @appraisalYear

      Update a
         Set a.closeDt = getdate()
        From JournalEntry a
       Where a.idJournalEntry = @idJournalEntry
        
   End   

END
GO
