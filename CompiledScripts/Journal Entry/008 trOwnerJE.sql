Use MINERALPRO
GO


IF (OBJECT_ID('trOwnerJE') IS NOT NULL)
  DROP TRIGGER dbo.trOwnerJE
GO


CREATE TRIGGER dbo.trOwnerJE
   ON  dbo.Owner
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON

   --Uses MAX to prevent sub-queries from returning multiple values

   Declare @idJournalEntry bigint
   Declare @appraisalYear smallint
   Declare @currentYear smallint
   Declare @result bigint
   Declare @appUser bigint

   --Capture Userid
   Select @appUser = max([rowUpdateUserid]) From inserted

   --Get current year
   Select @currentYear = appraisalYear FROM dbo.AppraisalYear Where currentInd = 'Y'
   --Get year updated
   Select @appraisalYear = max(appraisalYear) from Inserted

   --If current year, then proceed
   if @currentYear = @appraisalYear
   Begin
      -- Create Journal Entry
      Insert Into dbo.JournalEntry ( appUser        , appraisalYear
                                   , openDt         , closeDt
                                   , jeReasonCd     , description
                                   , rowUpdateUserid, rowUpdateDt
                                   )
      Values ( @appUser
             , @appraisalYear
             , GETDATE()
             , GETDATE()
             , 17  --Owner Change???
             , 'OWNER CHANGE'
             , @appUser
             , GETDATE()
             )

      -- Get Journal Entry ID
      Set @idJournalEntry = SCOPE_IDENTITY()
         
      -- Capture before values (row 0)
      Insert into dbo.jeOwner (       
       [idJournalEntry]
      ,[seqNumber]
      ,idOwner
      ,appraisalYear
      ,ownerId
      ,name1
      ,name2
      ,name3
      ,addrLine1
      ,addrLine2
      ,addrLine3
      ,city
      ,stateCd
      ,zipcode
      ,countryCd
      ,phoneNum
      ,faxNum
      ,emailAddress
      ,birthDt
      ,confidentialInd
      ,ownerChangeCd
      ,agencyCdx
      ,rowUpdateUserid
      ,rowUpdateDt
      ,rowDeleteFlag)
      Select @idJournalEntry, 0, idOwner
      ,appraisalYear
      ,ownerId
      ,name1
      ,name2
      ,name3
      ,addrLine1
      ,addrLine2
      ,addrLine3
      ,city
      ,stateCd
      ,zipcode
      ,countryCd
      ,phoneNum
      ,faxNum
      ,emailAddress
      ,birthDt
      ,confidentialInd
      ,ownerChangeCd
      ,agencyCdx
      ,rowUpdateUserid
      ,rowUpdateDt
      ,rowDeleteFlag
        From deleted d

      --Capture the after value
      Insert into jeOwner (       
       [idJournalEntry]
      ,[seqNumber]
      ,idOwner
      ,appraisalYear
      ,ownerId
      ,name1
      ,name2
      ,name3
      ,addrLine1
      ,addrLine2
      ,addrLine3
      ,city
      ,stateCd
      ,zipcode
      ,countryCd
      ,phoneNum
      ,faxNum
      ,emailAddress
      ,birthDt
      ,confidentialInd
      ,ownerChangeCd
      ,agencyCdx
      ,rowUpdateUserid
      ,rowUpdateDt
      ,rowDeleteFlag)
      Select @idJournalEntry, 1, idOwner
      ,appraisalYear
      ,ownerId
      ,name1
      ,name2
      ,name3
      ,addrLine1
      ,addrLine2
      ,addrLine3
      ,city
      ,stateCd
      ,zipcode
      ,countryCd
      ,phoneNum
      ,faxNum
      ,emailAddress
      ,birthDt
      ,confidentialInd
      ,ownerChangeCd
      ,agencyCdx
      ,rowUpdateUserid
      ,rowUpdateDt
      ,rowDeleteFlag
        From inserted i

      Execute dbo.spJeOwnerExtras @idJournalEntry, @appUser, @appraisalYear

      Update a
         Set a.closeDt = getdate()
        From JournalEntry a
       Where a.idJournalEntry = @idJournalEntry
        
   End   

END
GO
