USE [MINERALPRO]
GO

IF (OBJECT_ID('spJeOwnerExtras') IS NOT NULL)
  DROP PROCEDURE dbo.spJeOwnerExtras
GO

CREATE PROCEDURE dbo.spJeOwnerExtras
 @idJournalEntry Bigint
,@appUser Bigint
,@appraisalYear Int
AS
BEGIN
	SET NOCOUNT ON

   -- LeaseOwner Capture before value
   print 'LeaseOwner Before'
   Insert into dbo.jeLeaseOwner
   Select @idJournalEntry, 0, lo.*
     From dbo.LeaseOwner lo
    Where not exists (Select 1
                        From dbo.jeLeaseOwner sub
                       Where sub.idJournalEntry = @idJournalEntry
                         And sub.idLeaseOwner = lo.idLeaseOwner
                         And sub.seqNumber = 0)
      And lo.appraisalYear = @appraisalYear
      And lo.ownerId in (
         Select ownerId from dbo.jeOwner lo
          Where lo.idJournalEntry = @idJournalEntry
            And lo.appraisalYear = @appraisalYear
            )

   -- LeaseOwner Capture after Value
   print 'LeaseOwner After'
   Insert into dbo.jeLeaseOwner
   Select @idJournalEntry, 1, lo.*
     From dbo.LeaseOwner lo
    Where not exists (Select 1
                        From dbo.jeLeaseOwner sub
                       Where sub.idJournalEntry = @idJournalEntry
                         And sub.idLeaseOwner = lo.idLeaseOwner
                         And sub.seqNumber = 1)
      And lo.appraisalYear = @appraisalYear
      And lo.ownerId in (
         Select ownerId from dbo.jeOwner lo
          Where lo.idJournalEntry = @idJournalEntry
            And lo.appraisalYear = @appraisalYear
            )

   -- LeaseTaxUnit Capture before value
   print 'LeaseTaxUnit Before'
   Insert into jeLeaseTaxUnit
   Select @idJournalEntry, 0, ltu.*
     From dbo.LeaseTaxUnit ltu
    Where not exists (Select 1
                        From dbo.jeLeaseTaxUnit sub
                       Where sub.idJournalEntry = @idJournalEntry
                         And sub.idLeaseTaxUnit = ltu.idLeaseTaxUnit
                         And sub.seqNumber = 0)
      And ltu.appraisalYear = @appraisalYear
      And ltu.leaseId in (
         Select leaseId
           from dbo.jeLeaseOwner lo
          Where lo.idJournalEntry = @idJournalEntry
            And lo.appraisalYear = @appraisalYear
            )

   -- LeaseTaxUnit Capture after Value
   print 'LeaseTaxUnit After'
   Insert into jeLeaseTaxUnit
   Select @idJournalEntry, 1, ltu.*
     From dbo.LeaseTaxUnit ltu
    Where not exists (Select 1
                        From dbo.jeLeaseTaxUnit sub
                       Where sub.idJournalEntry = @idJournalEntry
                         And sub.idLeaseTaxUnit = ltu.idLeaseTaxUnit
                         And sub.seqNumber = 1)
      And ltu.appraisalYear = @appraisalYear
      And ltu.leaseId in (
         Select leaseId
           from dbo.jeLeaseOwner lo
          Where lo.idJournalEntry = @idJournalEntry
            And lo.appraisalYear = @appraisalYear
            )

   -- OwnerExemption Capture before value
   print 'LeaseOwnerExemption Before'
   Insert into dbo.jeOwnerExemption
   Select @idJournalEntry, 0, oe.*
     From dbo.OwnerExemption oe
    Where not exists (Select 1
                        From dbo.jeOwnerExemption sub
                       Where sub.idJournalEntry = @idJournalEntry
                         And sub.idOwnerExemption = oe.idOwnerExemption
                         And sub.seqNumber = 0)
      And oe.appraisalYear = @appraisalYear
      And oe.ownerId in (
         Select ownerId from dbo.jeLeaseOwner lo
          Where lo.idJournalEntry = @idJournalEntry
            And lo.appraisalYear = @appraisalYear
            )

   -- OwnerExemption Capture after Value
   print 'LeaseOwnerExemption After'
   Insert into dbo.jeOwnerExemption
   Select @idJournalEntry, 1, oe.*
     From dbo.OwnerExemption oe
    Where not exists (Select 1
                        From jeOwnerExemption sub
                       Where sub.idJournalEntry = @idJournalEntry
                         And sub.idOwnerExemption = oe.idOwnerExemption
                         And sub.seqNumber = 1)
      And oe.appraisalYear = @appraisalYear
      And oe.ownerId in (
         Select ownerId from dbo.jeLeaseOwner lo
          Where lo.idJournalEntry = @idJournalEntry
            And lo.appraisalYear = @appraisalYear
            )


   -- Owner Capture before value
   Print 'Owner Before'
   Insert into dbo.jeOwner (        
       [idJournalEntry]
      ,[seqNumber]
      ,idOwner
      ,appraisalYear
      ,ownerId
      ,name1
      ,name2
      ,name3
      ,addrLine1
      ,addrLine2
      ,addrLine3
      ,city
      ,stateCd
      ,zipcode
      ,countryCd
      ,phoneNum
      ,faxNum
      ,emailAddress
      ,birthDt
      ,confidentialInd
      ,ownerChangeCd
      ,agencyCdx
      ,rowUpdateUserid
      ,rowUpdateDt
      ,rowDeleteFlag)
   Select @idJournalEntry, 0
      ,idOwner
      ,appraisalYear
      ,ownerId
      ,name1
      ,name2
      ,name3
      ,addrLine1
      ,addrLine2
      ,addrLine3
      ,city
      ,stateCd
      ,zipcode
      ,countryCd
      ,phoneNum
      ,faxNum
      ,emailAddress
      ,birthDt
      ,confidentialInd
      ,ownerChangeCd
      ,agencyCdx
      ,rowUpdateUserid
      ,rowUpdateDt
      ,rowDeleteFlag
     From dbo.Owner oe
    Where not exists (Select 1
                        From dbo.jeOwner sub
                       Where sub.idJournalEntry = @idJournalEntry
                         And sub.idOwner = oe.idOwner
                         And sub.seqNumber = 0)
      And oe.appraisalYear = @appraisalYear
      And oe.ownerId in (
         Select ownerId from dbo.jeLeaseOwner lo
          Where lo.idJournalEntry = @idJournalEntry
            And lo.appraisalYear = @appraisalYear
            )

   -- Owner Capture after Value
   Print 'Owner After'
   Insert into jeOwner (       
       [idJournalEntry]
      ,[seqNumber]
      ,idOwner
      ,appraisalYear
      ,ownerId
      ,name1
      ,name2
      ,name3
      ,addrLine1
      ,addrLine2
      ,addrLine3
      ,city
      ,stateCd
      ,zipcode
      ,countryCd
      ,phoneNum
      ,faxNum
      ,emailAddress
      ,birthDt
      ,confidentialInd
      ,ownerChangeCd
      ,agencyCdx
      ,rowUpdateUserid
      ,rowUpdateDt
      ,rowDeleteFlag)
   Select @idJournalEntry, 1
      ,idOwner
      ,appraisalYear
      ,ownerId
      ,name1
      ,name2
      ,name3
      ,addrLine1
      ,addrLine2
      ,addrLine3
      ,city
      ,stateCd
      ,zipcode
      ,countryCd
      ,phoneNum
      ,faxNum
      ,emailAddress
      ,birthDt
      ,confidentialInd
      ,ownerChangeCd
      ,agencyCdx
      ,rowUpdateUserid
      ,rowUpdateDt
      ,rowDeleteFlag
     From dbo.Owner oe
    Where not exists (Select 1
                        From jeOwner sub
                       Where sub.idJournalEntry = @idJournalEntry
                         And sub.idOwner = oe.idOwner
                         And sub.seqNumber = 1)
      And oe.appraisalYear = @appraisalYear
      And oe.ownerId in (
         Select ownerId from dbo.jeLeaseOwner lo
          Where lo.idJournalEntry = @idJournalEntry
            And lo.appraisalYear = @appraisalYear
            )

END

Grant Execute on dbo.spJeOwnerExtras to Public
GO