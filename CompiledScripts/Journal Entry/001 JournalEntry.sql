USE [MINERALPRO]
GO

IF OBJECT_ID('[dbo].[JournalEntry]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[JournalEntry]; 
GO

CREATE TABLE [dbo].[JournalEntry](
   [idJournalEntry] [bigint] IDENTITY(1,1) NOT NULL,
   [appUser] [bigint] NOT NULL CONSTRAINT [DF_JournalEntry_appUser]  DEFAULT ((0)),
   [appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_JournalEntry_appraisalYear]  DEFAULT ((0)),
   [batchId] [int] NOT NULL CONSTRAINT [DF_JournalEntry_batchId]  DEFAULT ((99999999)),
   [openDt] [datetime] NOT NULL CONSTRAINT [DF_JournalEntry_openDt]  DEFAULT (getdate()),
   [closeDt] [datetime] NOT NULL CONSTRAINT [DF_JournalEntry_closeDt]  DEFAULT ('1900-01-01'),
   [jeReasonCd] [smallint] NOT NULL CONSTRAINT [DF_JournalEntry_jeReasonCd]  DEFAULT ((0)),
   [description] [varchar](255) NOT NULL CONSTRAINT [DF_JournalEntry_description]  DEFAULT (''),   
   [taxOfficeNotifyCd] [smallint] NOT NULL CONSTRAINT [DF_JournalEntry_taxOfficeNotifyCd]  DEFAULT ((0)),
   [jeHoldCd] [smallint] NOT NULL CONSTRAINT [DF_JournalEntry_jeHoldCd]  DEFAULT ((0)),
   [collectionExtractDt] [datetime] NOT NULL CONSTRAINT [DF_JournalEntry_collectionExtractDt]  DEFAULT ('1900-01-01'),
   [rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_JournalEntry_rowUpdateUserid]  DEFAULT ((0)),   
   [rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_JournalEntry_rowUpdateDt]  DEFAULT (getdate()),
   [rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_JournalEntry_rowDeleteFlag]  DEFAULT (''),   
  
 CONSTRAINT [PK_JournalEntry] PRIMARY KEY CLUSTERED 
(
   [idJournalEntry] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_JournalEntry] ON [dbo].[JournalEntry]
(
   [appraisalYear] DESC,
   [appUser] ASC,
   [idJournalEntry] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO






IF OBJECT_ID('[dbo].[jeLeaseOwner]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[jeLeaseOwner]; 
GO

CREATE TABLE [dbo].[jeLeaseOwner](
   [idJeLeaseOwner] [bigint] NOT NULL IDENTITY(1,1),
   [idJournalEntry] [bigint] NOT NULL  CONSTRAINT [DF_jeLeaseOwner_idJournalEntry] DEFAULT (0),
   [seqNumber] [smallint] NOT NULL  CONSTRAINT [DF_jeLeaseOwner_seqNumber] DEFAULT (0),
   [idLeaseOwner] [bigint] NOT NULL  CONSTRAINT [DF_jeLeaseOwner_idLeaseOwner] DEFAULT (0),
   [appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_jeLeaseOwner_appraisalYear] DEFAULT (0),
   [leaseId] [int] NOT NULL CONSTRAINT [DF_jeLeaseOwner_leaseId] DEFAULT (0),
   [ownerId] [int] NOT NULL CONSTRAINT [DF_jeLeaseOwner_ownerId] DEFAULT (0),
   [interestTypeCd] [smallint] NOT NULL CONSTRAINT [DF_jeLeaseOwner_interestTypeCd] DEFAULT (0),
   [interestPercent] [decimal](9, 8) NOT NULL CONSTRAINT [DF_jeLeaseOwner_interestPercent] DEFAULT (0.0),
   [ownerValue] [decimal](13, 0) NOT NULL CONSTRAINT [DF_jeLeaseOwner_ownerValue] DEFAULT (0),
   [lockCd] [smallint] NOT NULL CONSTRAINT [DF_jeLeaseOwner_lockCd] DEFAULT (0),
   [lastCalcDt] [datetime] NOT NULL CONSTRAINT [DF_jeLeaseOwner_lastCalcDt] DEFAULT (getdate()),
   [lastCalcUserid] [int] NOT NULL CONSTRAINT [DF_jeLeaseOwner_lastCalcUserid] DEFAULT (0),
   [lastCalcSourceId] [bigint] NOT NULL CONSTRAINT [DF_jeLeaseOwner_lastCalcSourceId] DEFAULT (0),
   [rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_jeLeaseOwner_rowUpdateUserid] DEFAULT (0),
   [rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_jeLeaseOwner_rowUpdateDt] DEFAULT (getdate()),
   [rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_jeLeaseOwner_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_JeLeaseOwner] PRIMARY KEY CLUSTERED 
(
   [idJeLeaseOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_JeLeaseOwner] ON [dbo].[jeLeaseOwner]
(
   [idJournalEntry] DESC,
   [seqNumber] ASC,
   [idLeaseOwner] ASC,
   [appraisalYear] DESC,
   [leaseId] ASC,
   [ownerId] ASC,
   [interestTypeCd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO





IF OBJECT_ID('[dbo].[jeLeaseTaxUnit]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[jeLeaseTaxUnit]; 
GO

CREATE TABLE [dbo].[jeLeaseTaxUnit](
   [idJeLeaseTaxUnit] [bigint] NOT NULL IDENTITY(1,1),
   [idJournalEntry] [bigint] NOT NULL  CONSTRAINT [DF_jeLeaseTaxUnit_idJournalEntry] DEFAULT (0),
   [seqNumber] [smallint] NOT NULL  CONSTRAINT [DF_jeLeaseTaxUnit_seqNumber] DEFAULT (0),
   [idLeaseTaxUnit] [bigint] NOT NULL  CONSTRAINT [DF_jeLeaseTaxUnit_idLeaseTaxUnit] DEFAULT (0),
   [appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_jeLeaseTaxUnit_appraisalYear] DEFAULT (0),
   [leaseId] [int] NOT NULL CONSTRAINT [DF_jeLeaseTaxUnit_leaseId] DEFAULT (0),
   [taxUnitTypeCd] [smallint] NOT NULL CONSTRAINT [DF_jeLeaseTaxUnit_taxUnitTypeCd] DEFAULT (0),
   [taxUnitCd] [smallint] NOT NULL CONSTRAINT [DF_jeLeaseTaxUnit_taxUnitCd] DEFAULT (0),
   [taxUnitPercent] [decimal](4, 3) NOT NULL CONSTRAINT [DF_jeLeaseTaxUnit_taxUnitPercent] DEFAULT (0.0),
   [rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_jeLeaseTaxUnit_rowUpdateUserid] DEFAULT (0),
   [rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_jeLeaseTaxUnit_rowUpdateDt] DEFAULT (getdate()),
   [rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_jeLeaseTaxUnit_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_JeLeaseTaxUnit] PRIMARY KEY CLUSTERED 
(
   [idJeLeaseTaxUnit] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_JeLeaseTaxUnit] ON [dbo].[jeLeaseTaxUnit]
(
   [idJournalEntry] DESC,
   [seqNumber] ASC,
   [idJeLeaseTaxUnit] ASC,
   [appraisalYear] DESC,
   [leaseId] ASC,
   [TaxUnitTypeCd] ASC,
   [TaxUnitCd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO








IF OBJECT_ID('[dbo].[jeOwner]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[jeOwner]; 
GO

CREATE TABLE [dbo].[jeOwner](
   [idJeOwner] [bigint] NOT NULL IDENTITY(1,1),
   [idJournalEntry] [bigint] NOT NULL  CONSTRAINT [DF_jeOwner_idJournalEntry] DEFAULT (0),
   [seqNumber] [smallint] NOT NULL  CONSTRAINT [DF_jeOwner_seqNumber] DEFAULT (0),
   [idOwner] [bigint] NOT NULL  CONSTRAINT [DF_jeOwner_idOwner] DEFAULT (0),
   [appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_jeOwner_appraisalYear] DEFAULT (0),
   [ownerId] [int] NOT NULL CONSTRAINT [DF_jeOwner_ownerId] DEFAULT (0),
   [name1] [varchar](50) NOT NULL CONSTRAINT [DF_jeOwner_name1] DEFAULT (''),
   [name2] [varchar](50) NOT NULL CONSTRAINT [DF_jeOwner_name2] DEFAULT (''),
   [name3] [varchar](50) NOT NULL CONSTRAINT [DF_jeOwner_name3] DEFAULT (''),
   [addrLine1] [varchar](50) NOT NULL CONSTRAINT [DF_jeOwner_addrLine1] DEFAULT (''),
   [addrLine2] [varchar](50) NOT NULL CONSTRAINT [DF_jeOwner_addrLine2] DEFAULT (''),
   [addrLine3] [varchar](50) NOT NULL CONSTRAINT [DF_jeOwner_addrLine3] DEFAULT (''),
   [city] [varchar](35) NOT NULL CONSTRAINT [DF_jeOwner_city] DEFAULT (''),
   [stateCd] [smallint] NOT NULL CONSTRAINT [DF_jeOwner_stateCd] DEFAULT (0),
   [zipcode] [varchar](9) NOT NULL CONSTRAINT [DF_jeOwner_zipcode] DEFAULT (''),
   [zipsort] [varchar](5) NOT NULL CONSTRAINT [DF_jeOwner_zipsort] DEFAULT (''),
   [countryCd] [smallint] NOT NULL CONSTRAINT [DF_jeOwner_countryCd] DEFAULT (0),
   [phoneNum] [varchar](10) NOT NULL CONSTRAINT [DF_jeOwner_phoneNum] DEFAULT (''),
   [faxNum] [varchar](10) NOT NULL CONSTRAINT [DF_jeOwner_faxNum] DEFAULT (''),
   [emailAddress] [varchar](50) NOT NULL CONSTRAINT [DF_jeOwner_emailAddress] DEFAULT (''),
   [birthDt] [datetime] NOT NULL CONSTRAINT [DF_jeOwner_birthDt] DEFAULT ('1900-01-01'),
   [confidentialInd] [char](1) NOT NULL CONSTRAINT [DF_jeOwner_confidentialInd] DEFAULT ('N'),
   [ownerTypeCdx] [char](1) NOT NULL CONSTRAINT [DF_jeOwner_ownerTypeCdx] DEFAULT (''),
   [ownerChangeCd] [smallint] NOT NULL CONSTRAINT [DF_jeOwner_ownerChangeCd] DEFAULT (0),
   [agencyCdx] [varchar](6) NOT NULL CONSTRAINT [DF_jeOwner_agencyCdx] DEFAULT (''),
   [rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_jeOwner_rowUpdateUserid] DEFAULT (0),
   [rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_jeOwner_rowUpdateDt] DEFAULT (getdate()),
   [rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_jeOwner_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_JeOwner] PRIMARY KEY CLUSTERED 
(
   [idJeOwner] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_JeOwner] ON [dbo].[jeOwner]
(
   [idJournalEntry] DESC,
   [seqNumber] ASC,
   [idOwner] ASC,
   [appraisalYear] DESC,
   [ownerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO





IF OBJECT_ID('[dbo].[jeOwnerExemption]', 'U') IS NOT NULL 
  DROP TABLE [dbo].[jeOwnerExemption]; 
GO

CREATE TABLE [dbo].[jeOwnerExemption](
   [idJeOwnerExemption] [bigint] NOT NULL IDENTITY(1,1),
   [idJournalEntry] [bigint] NOT NULL  CONSTRAINT [DF_jeOwnerExemption_idJournalEntry] DEFAULT (0),
   [seqNumber] [smallint] NOT NULL  CONSTRAINT [DF_jeOwnerExemption_seqNumber] DEFAULT (0),
   [idOwnerExemption] [bigint] NOT NULL  CONSTRAINT [DF_jeOwnerExemption_idOwnerExemption] DEFAULT (0),
   [appraisalYear] [smallint] NOT NULL CONSTRAINT [DF_jeOwnerExemption_appraisalYear] DEFAULT (0),
   [ownerId] [int] NOT NULL CONSTRAINT [DF_jeOwnerExemption_ownerId] DEFAULT (0),
   [exemptionTypeCd] [smallint] NOT NULL CONSTRAINT [DF_jeOwnerExemption_exemptionTypeCd] DEFAULT (0),
   [exemptionCd] [smallint] NOT NULL CONSTRAINT [DF_jeOwnerExemption_exemptionCd] DEFAULT (0),
   [effectiveDt] [datetime] NOT NULL CONSTRAINT [DF_jeOwnerExemption_effectiveDt] DEFAULT (getdate()),
   [expirationDt] [datetime] NOT NULL CONSTRAINT [DF_jeOwnerExemption_expirationDt] DEFAULT (getdate()),
   [rowUpdateUserid] [int] NOT NULL CONSTRAINT [DF_jeOwnerExemption_rowUpdateUserid] DEFAULT (0),
   [rowUpdateDt] [datetime] NOT NULL CONSTRAINT [DF_jeOwnerExemption_rowUpdateDt] DEFAULT (getdate()),
   [rowDeleteFlag] [char](1) NOT NULL CONSTRAINT [DF_jeOwnerExemption_rowDeleteFlag] DEFAULT (''),

 CONSTRAINT [PK_JeOwnerExemption] PRIMARY KEY CLUSTERED 
(
   [idJeOwnerExemption] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [AltKey_JeOwnerExemption] ON [dbo].[jeOwnerExemption]
(
   [idJournalEntry] DESC,
   [seqNumber] ASC,
   [idOwnerExemption] ASC,
   [appraisalYear] DESC,
   [ownerId] ASC,
   [exemptionTypeCd] ASC,
   [exemptionCd] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO




