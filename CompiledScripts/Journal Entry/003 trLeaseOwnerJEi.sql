Use MINERALPRO
GO


IF (OBJECT_ID('trLeaseOwnerJEi') IS NOT NULL)
  DROP TRIGGER dbo.trLeaseOwnerJEi
GO


CREATE TRIGGER dbo.trLeaseOwnerJEi
   ON  dbo.LeaseOwner
   AFTER INSERT
AS 
BEGIN
	SET NOCOUNT ON

   --Uses MAX to prevent sub-queries from returning multiple values

   Declare @idJournalEntry bigint
   Declare @appraisalYear smallint
   Declare @currentYear smallint
   Declare @result bigint
   Declare @appUser bigint

   --Capture Userid
   Select @appUser = max([rowUpdateUserid]) From inserted

   --Get current year
   Select @currentYear = appraisalYear FROM dbo.AppraisalYear Where currentInd = 'Y'
   --Get year updated
   Select @appraisalYear = max(appraisalYear) from Inserted

   --If prior year, then proceed
   if @currentYear > @appraisalYear
   Begin
      -- Get JournalEntry ID to place on child tables
      Select @idJournalEntry = Isnull(b.idJournalEntry,0)
        From JournalEntry b
       Where b.appUser = (Select max([rowUpdateUserid]) From inserted)
         And b.closeDt = '1900-01-01'
         And b.appraisalYear = @appraisalYear
         
      --If JournalEntry Id found then proceed (should always be found)
      If @idJournalEntry <> 0
      Begin
         -- Capture before values (row 0)
         Insert into jeLeaseOwner (idJournalEntry, seqNumber, idLeaseOwner, appraisalYear, leaseId, ownerId)
         Select @idJournalEntry, 0, d.idLeaseOwner, d.appraisalYear, d.leaseId, d.ownerId
           From inserted d

         --Capture the after value
         Insert into jeLeaseOwner
         Select @idJournalEntry, 1, i.*
            From inserted i

         Execute dbo.spJeOwnerExtras @idJournalEntry, @appUser, @appraisalYear
      End
   End
END
GO
