Use MINERALPRO
GO


IF (OBJECT_ID('trLeaseOwnerJE') IS NOT NULL)
  DROP TRIGGER dbo.trLeaseOwnerJE
GO


CREATE TRIGGER dbo.trLeaseOwnerJE
   ON  dbo.LeaseOwner
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON

   --Uses MAX to prevent sub-queries from returning multiple values

   Declare @idJournalEntry bigint
   Declare @appraisalYear smallint
   Declare @currentYear smallint
   Declare @result bigint
   Declare @appUser bigint

   --Capture Userid
   Select @appUser = max([rowUpdateUserid]) From inserted

   --Get current year
   Select @currentYear = appraisalYear FROM dbo.AppraisalYear Where currentInd = 'Y'
   --Get year updated
   Select @appraisalYear = max(appraisalYear) from Inserted

   --If prior year, then proceed
   if @currentYear > @appraisalYear
   Begin
      -- Get JournalEntry ID to place on child tables
      Select @idJournalEntry = Isnull(b.idJournalEntry,0)
        From JournalEntry b
       Where b.appUser = (Select max([rowUpdateUserid]) From inserted)
         And b.closeDt = '1900-01-01'
         And b.appraisalYear = @appraisalYear
         
      --If JournalEntry Id found then proceed (should always be found)
      If @idJournalEntry <> 0
      Begin
         Select @result = Isnull(count(*),0)
           From jeLeaseOwner j
           Join deleted i
             On i.idLeaseOwner = j.idLeaseOwner
          Where j.idJournalEntry = @idJournalEntry

         If @result = 0
         Begin
            --Only capture before values (row 0) if no rows found in table for this JE
            Insert into jeLeaseOwner
            Select @idJournalEntry, 0, d.*
              From deleted d
         End

         --Blindly delete the after value if it exists
         Delete ja
         From jeLeaseOwner ja
         Join Inserted a
            On a.idLeaseOwner = ja.idLeaseOwner
         Where ja.seqNumber = 1
           And ja.idJournalEntry = @idJournalEntry

         --Capture the latest after value
         Insert into jeLeaseOwner
         Select @idJournalEntry, 1, i.*
            From inserted i

         Execute dbo.spJeOwnerExtras @idJournalEntry, @appUser, @appraisalYear
      End
   End
END
GO
