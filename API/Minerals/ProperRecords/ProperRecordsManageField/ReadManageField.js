module.exports = {
    ReadManageField: function (req, res) {
        //console.log("Called ReadManageField")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = "UPDATE LastUpdateTime SET dataRefreshTime = getdate() "
              +"WHERE tableName = 'Field' AND currentUserId = '"+req.query.idAppUser+"' "
              +"IF @@ROWCOUNT=0 "
              +"BEGIN "
              +"INSERT INTO LastUpdateTime (tableName, currentUserId) VALUES('Field', '"+req.query.idAppUser+"')"
              +"END "
              +"SELECT "
              +" count(a.fieldId) enableDeleteCount, "
              +"f.idField, "
              +"f.fieldId, "
              +"f.fieldName, "
              +"f.wellDepth as fieldDepth, "
              +"f.gravityCd, "
              +"sc.description gravityDesc "
              +"FROM Field f  "
              +"INNER JOIN SystemCode sc ON f.gravityCd = sc.code AND sc.codeType = '3510' AND sc.rowDeleteFlag = '' "
              +"LEFT JOIN Appraisal a on f.fieldId = a.fieldId AND f.appraisalYear = a.appraisalYear AND a.rowDeleteFlag = ''"
              +"WHERE f.appraisalYear = '"+req.query.appraisalYear+"' "
              +"AND sc.appraisalYear = '"+req.query.appraisalYear+"' "
              +"AND f.rowDeleteFlag = '' "
              +"GROUP BY f.idField, "
                +"f.fieldId, "
                +"f.fieldName, "
                +"f.wellDepth, "
                +"f.gravityCd, "
                +"sc.description "
              +"ORDER BY f.idField DESC ";

        if(req.query.dataCount == 0){      
            UtilFunctions.execSql(sql, res)
        }else{
            var sqlCond = "SELECT * FROM LastUpdateTime "
                +"WHERE lastUpdateTime > dataRefreshTime "
                +"AND tableName = 'Field' "
                +"AND currentUserId = '"+req.query.idAppUser+"'";

            UtilFunctions.execSqlCallBack(sqlCond, res, function (rec) {
                if(rec.length > 0){
                    UtilFunctions.execSql(sql, res)
                }else{
                    res.status(200).json({"success":false,"data":"Do not refresh the data"});
                }
            });                     
        }
    },//end ReadMaintainRRCField
};
