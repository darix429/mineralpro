module.exports = {
    UpdateManageField: function (req, res) {
        //console.log("Called UpdateManageField")
        var UtilFunctions = require('../../../UtilFunctions.js');
        if(req.body && req.query.updateLength){
			var arrData = req.body;
			if(req.query.updateLength == 1){arrData = [req.body];}
			
			var sql = "";     
			for (var ctr = 0; ctr < arrData.length; ctr++) {
                sql += "UPDATE Field SET "
                    +" fieldName = UPPER('" + arrData[ctr].fieldName + "'),"
                    +" wellDepth = '" + arrData[ctr].fieldDepth + "',"
                    +" gravityCd = '" + arrData[ctr].gravityCd + "',"
                    +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "'," 
                    +" rowUpdateDt = GETDATE(), "
                    +" rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "'"
                +" WHERE fieldId= '" + arrData[ctr].fieldId + "' AND appraisalYear= '"+req.query.appraisalYear+"';"; 
            }
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"Invalid request body"});
		}
    }//end UpdateMaintainRRCField
};
