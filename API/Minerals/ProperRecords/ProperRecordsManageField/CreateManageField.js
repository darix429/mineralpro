module.exports = {
    CreateManageField: function (req, res) {
        //console.log("Called CreateManageField")
        var UtilFunctions = require('../../../UtilFunctions.js');

        if(req.body){
			var arrData = req.body;
			if(req.query.createLength == 1){arrData = [req.body];}
			//console.log(arrData);
			var sql = "DECLARE @year smallint; ";
			
            sql +=" IF OBJECT_ID(N'tempdb..#tempNewFieldIds', N'U') IS NOT NULL " 
            	+ " BEGIN "
            	+ " DROP TABLE #tempNewFieldIds "
            	+ " END; "
            	+ " CREATE TABLE #tempNewFieldIds"
                + " ( "
                + "   fieldId INT "
                + " ) "
            	+ " IF OBJECT_ID(N'tempdb..#newFieldYears',N'U') IS NOT NULL "
                + "  BEGIN "
                + "  DROP TABLE #newFieldYears "
                + "  END; "
                + " CREATE TABLE #newFieldYears"
                + " ( "
                + "   year smallint "
                + " ) "
				
			var current_year = (new Date()).getFullYear();
			var appraisal_year = req.query.appraisalYear;
			var years = [];
			var last_prior = (appraisal_year <= (current_year-6))? appraisal_year : (current_year-6);
			for(year = last_prior; year<=current_year; year++){
				years.push(parseInt(year));
			}
			for(var i=0; i<years.length; i++){
				sql+="INSERT INTO #newFieldYears VALUES ('"+years[i]+"'); "
			}
			sql +=" DECLARE @numOfYears INT; "
				+ " DECLARE @fieldId INT; "
            for(var ctr=0; ctr<arrData.length; ctr++){
              
				sql += ""
					+" SET @numOfYears = (SELECT COUNT(*) FROM #newFieldYears); "
					+" SET @fieldId ="+arrData[ctr].fieldId+" "
					+" WHILE @numOfYears > 0 "
					+" BEGIN "
					+" 	SET @year = ( "
					+" 		select tbl.year "
					+" 		from "
					+" 		( "
					+" 			select row_number() over(order by year asc) as rnum, year  "
					+" 			from  "
					+" 			#newFieldYears n "
					+" 		)tbl "
					+" 		where tbl.rnum = @numOfYears "
					+" 	); "
				
					
				sql+=""
					+" IF EXISTS( SELECT fieldId from Field where fieldId = @fieldId AND appraisalYear= @year ) "
					+" 	BEGIN "
					+"		UPDATE Field SET "
					+" 		fieldName = UPPER('" + arrData[ctr].fieldName + "'),"
					+" 		wellDepth = '" + arrData[ctr].fieldDepth + "',"
					+" 		gravityCd = '" + arrData[ctr].gravityCd + "',"
					+" 		rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "'," 
					+" 		rowUpdateDt = GETDATE(), "
					+" 		rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "'"
					+" 		WHERE fieldId= '" + arrData[ctr].fieldId+"' AND appraisalYear= @year " 
					+" 	END "
					+" 	ELSE "
					+" 	BEGIN "
					+" 		INSERT INTO Field ("
					+" 			appraisalYear, "
					+" 			fieldId, "
					+" 			fieldName, "
					+" 			wellDepth, "
					+" 			gravityCd, "
					+" 			rowUpdateUserid, "
					+" 			rowDeleteFlag "
					+"		) "
					+" 		VALUES("
					+"			@year, "
					+"			@fieldId , "
					+"			UPPER('"+arrData[ctr].fieldName+"'), "
					+"			'"+arrData[ctr].fieldDepth+"', "
					+"			'"+arrData[ctr].gravityCd+"', "
					+"			'"+arrData[ctr].rowUpdateUserid+"', "
					+"			'"+arrData[ctr].rowDeleteFlag+"' "
					+"		) "
					+" END;"
							
				sql+=" SET @numOfYears = @numOfYears - 1; "
					+" INSERT INTO #tempNewFieldIds select @fieldId; "
					+" END; ";
            }
            var readNewlyInsertedField = " DECLARE @appraisalYear varchar(4) = '"+appraisal_year+"'"
				+"SELECT "
				+"	count(a.fieldId) enableDeleteCount, "
				+"	f.idField, "
				+"	f.fieldId, "
				+"	f.fieldName, "
				+"	f.wellDepth as fieldDepth, "
				+"	f.gravityCd, "
				+"	sc.description gravityDesc "
				+"FROM Field f  "
				+"	INNER JOIN SystemCode sc ON f.gravityCd = sc.code AND sc.codeType = '3510' AND sc.rowDeleteFlag = '' "
				+"	LEFT JOIN Appraisal a on f.fieldId = a.fieldId AND f.appraisalYear = a.appraisalYear AND a.rowDeleteFlag = ''"
				+"WHERE EXISTS( SELECT temp.fieldId FROM #tempNewFieldIds temp where temp.fieldId = f.fieldId ) "
				+"	AND f.appraisalYear = @appraisalYear "
				+"	AND sc.appraisalYear = @appraisalYear "
				+"	AND f.rowDeleteFlag = '' "
				+"GROUP BY f.idField, "
				+"	f.fieldId, "
				+"	f.fieldName, "
				+"	f.wellDepth, "
				+"	f.gravityCd, "
				+"	sc.description "
				+"ORDER BY f.idField DESC ";
			sql += readNewlyInsertedField;
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"No data received"});
		}
    },//end CreateMaintainOperator
    
};
