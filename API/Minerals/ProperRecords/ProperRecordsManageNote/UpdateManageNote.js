module.exports = {
    Update: function (req, res) {
        //console.log("Called UpdateManageNote")
        var UtilFunctions = require('../../../UtilFunctions.js');
        if (req.body && req.query.updateLength) {
            var arrData = req.body;
            if (req.query.updateLength == 1) {
                arrData = [req.body];
            }
            //console.log(arrData);

            var sql = "";
            for (var ctr = 0; ctr < arrData.length; ctr++) {
                sql += "UPDATE Note SET "
                        + "note = UPPER('" + arrData[ctr].note + "'),"
                        + "rrcNumber = '" + arrData[ctr].rrcNumber + "',"
                        + "securityCd = '" + arrData[ctr].securityCd + "', "
                        + "quickNoteCd = '" + arrData[ctr].quickNoteCd + "', "
                        + "noteTypeCd = '" + arrData[ctr].noteTypeCd + "', "
                        + "rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "', "
                        + "referenceAppraisalYear = '" + req.query.appraisalYear + "', "
                        + "rowUpdateDt = GETDATE(), "
                        + "rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "' "
                        + "WHERE "
                        + "idNote = '" + arrData[ctr].idNote + "'"
            }
            UtilFunctions.execSql(sql, res);
        } else {
            res.status(400).json({"success": false, "data": "Invalid request body"});
        }
    }
}
