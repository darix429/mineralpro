module.exports = {
    Read: function (req, res) {
        //console.log("Called ReadMaintainNote")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var subjectTypeCd = req.query.subjectTypeCd;
			
        var sql = "";
        var rrcNumber = "0";
        if(req.query.selectedId){
            
            var subjectId = req.query.selectedId;           
            if(req.query.selectedRRCNumber){
                if(req.query.appraisalNote){
                    rrcNumber = req.query.selectedRRCNumber;
                }
            }
                       
            sql += "DECLARE "
                +"@appraisalYear smallint = "+req.query.appraisalYear+", "
                +"@subjectTypeCd smallint = "+subjectTypeCd+", "
                +"@rrcNumber int = "+rrcNumber+", "
                +"@subjectId int = "+subjectId+" "
                +"SELECT "
                +"idNote, "
                +"subjectId, "
                +"rrcNumber, "
                +"N.rowUpdateUserid, "
                +"seqNumber, "
                //+"CONVERT(VARCHAR(23), N.rowUpdateDt, 120) createDt, "
                +"CONVERT(VARCHAR(10), N.rowUpdateDt, 120) + ' ' + "
                +"LEFT(RIGHT(CONVERT(VARCHAR(25), N.rowUpdateDt, 131), 13), 7) +  ' ' + "
                +"RIGHT(CONVERT(VARCHAR(25), N.rowUpdateDt, 131), 2)createDt, "
                +"securityCd, "
                +"N.quickNoteCd, "
                +"QN.description as 'quickNoteDesc', "
                +"N.groupCd, "
                +"referenceAppraisalYear, "
                +"N.noteTypeCd, "
                +"OC.firstName + ' ' + OC.lastName noteAppraiserName, "
                +"N.note, "
                +"NT.description noteTypeDesc "
                +"FROM Note as N "
                +"LEFT JOIN NoteType as NT ON NT.noteTypeCd = N.noteTypeCd AND NT.rowDeleteFlag = '' AND NT.appraisalYear = @appraisalYear "
                +"LEFT JOIN QuickNote as QN ON N.quickNoteCd = QN.quickNoteCd AND QN.rowDeleteFlag = '' "
                +"LEFT JOIN AppUser as AU ON N.rowUpdateUserid = AU.idAppUser AND AU.rowDeleteFlag = '' "
                +"LEFT JOIN OrganizationContact as OC ON AU.idOrganizationContact = OC.idOrganizationContact AND OC.rowDeleteFlag = '' "
                +"WHERE  "
                +"subjectTypeCd = @subjectTypeCd  "
                +"AND subjectId = @subjectId  "        
                +"AND rrcNumber = @rrcNumber  "
                //+"AND referenceAppraisalYear = @appraisalYear  "
                +"AND N.rowDeleteFlag = '' ORDER BY seqNumber DESC "; 
                   
//            if(subjectTypeCd != 3 && rrcNumber == "0"){
//                sql = "";
//            }                               
        }               
//        res.send(sql)
        UtilFunctions.execSql(sql, res)
    },//end ReadMaintainNote
};