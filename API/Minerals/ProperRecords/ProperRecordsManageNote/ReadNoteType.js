module.exports = {
    Read: function (req, res) {
        var UtilFunctions = require('../../../UtilFunctions.js');
		if(req.query.appraisalYear){
			var sql = "SELECT noteTypeCd, description noteTypeDesc FROM NoteType WHERE rowDeleteFlag='' AND appraisalYear='"+req.query.appraisalYear+"'";
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"Unknown appraisal Year"});
		}
		
    },
};