module.exports = {
    Read: function (req, res) {
        var UtilFunctions = require('../../../UtilFunctions.js');
		if(req.query.appraisalYear){
			var sql = "SELECT quickNoteCd, description quickNoteDesc FROM QuickNote WHERE rowDeleteFlag='' AND appraisalYear='"+req.query.appraisalYear+"'";
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"Unknown appraisal Year"});
		}
		
    },
};