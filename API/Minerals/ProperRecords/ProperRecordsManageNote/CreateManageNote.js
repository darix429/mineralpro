module.exports = {
    Create: function (req, res) {
        //console.log("Called CreateManageNote")
        var UtilFunctions = require('../../../UtilFunctions.js');
        if (req.body) {
            var arrData = req.body;
            var subjectTypeCd = req.query.subjectTypeCd;

            if (req.query.createLength == 1) {
                arrData = [req.body];
            }
            //console.log(arrData);
            var sql = "";
            for (var ctr = 0; ctr < arrData.length; ctr++) {
                var rrcNumber = 0
                if(req.query.appraisalNote){
                    rrcNumber = arrData[ctr].rrcNumber
                }
                sql += "INSERT INTO Note ( "
                        + "subjectTypeCd, "
                        + "subjectId, "
                        + "seqNumber, "
                        + "createDt, "
                        + "noteTypeCd, "
                        + "quickNoteCd, "
                        + "securityCd, "
                        + "groupCd, "
                        + "referenceAppraisalYear, "
                        + "note, "
                        + "rrcNumber, "
                        + "rowUpdateUserid, "
                        + "rowDeleteFlag) "
                        + "VALUES ("
                        + "'" + req.query.subjectTypeCd + "', "
                        + "'" + arrData[ctr].subjectId + "', "
                        + "(SELECT ISNULL(max(seqNumber), 0 )+1 FROM Note "
                                + "WHERE subjectTypeCd = '" + req.query.subjectTypeCd + "' "
                                + "AND subjectId= '" + arrData[ctr].subjectId + "'), "
                        + "SYSDATETIME(), "
                        + "'" + arrData[ctr].noteTypeCd + "', "
                        + "'" + arrData[ctr].quickNoteCd + "', "
                        + "'" + arrData[ctr].securityCd + "', "
                        + "'" + arrData[ctr].groupCd + "', "
                        + "'" + req.query.appraisalYear + "', "
                        + "UPPER('" + arrData[ctr].note + "'), "
                        + "'" + rrcNumber + "', "
                        + "'" + arrData[ctr].rowUpdateUserid + "', "
                        + "'" + arrData[ctr].rowDeleteFlag + "');"
            }
            UtilFunctions.execSql(sql, res);
        } else {
            res.status(400).json({"success": false, "data": "No data received"});
        }
    }
}


