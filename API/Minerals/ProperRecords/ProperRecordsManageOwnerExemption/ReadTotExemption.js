module.exports = {
    ReadTotExemption: function (req, res) {
        //console.log("Called ReadTotExemption")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = "";
        // if(req.query.codeType){
        sql = " SELECT "
                        +" codeType as codeType, "
                        +" description as totalExemptionDesc, "
                        +" code as totalExemptionCd, "
                        +" code.rowDeleteFlag "
                    +" FROM  "
                        +" SystemCode as code "
                    +" WHERE "
                        +" codeType ='3430' "
                        +" AND code.rowDeleteFlag = '' "
                        +" AND code.appraisalYear = '"+req.query.appraisalYear+"' "
                    +" ORDER BY code; "
         // }
         //console.log(sql)
        UtilFunctions.execSql(sql, res)
    },//end ReadTotExemption
};
