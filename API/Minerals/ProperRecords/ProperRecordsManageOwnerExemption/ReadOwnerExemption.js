var UtilFunctions = require('../../../UtilFunctions.js');

module.exports = {
    ReadOwnerExemption: function (req, res) {
        //console.log("Called ReadOwnerExemption");
		var appraisalYear = req.query.appraisalYear;
		var ownerId = req.query.ownerId;
		
		if(appraisalYear && ownerId){
			var sql = ""
				+"DECLARE "
					+"@appraisalYear smallint = '"+appraisalYear+"', "
					+"@ownerId int = '"+ownerId+"' "
				+"SELECT "
					+"oe.idOwnerExemption, "
					+"oe.ownerId as 'ownerId', "
					+"oe.exemptionCd, "
					+"oe.exemptionTypeCd as 'exemptionType', "
					+"CONCAT(o.name1,' ',o.name2,' ',o.name3) as 'ownerName', "
					+"CASE WHEN exemptionTypeCd=3440 THEN 'Disabled Vet' ELSE 'Total Exemption' END as 'exemptionTypeDesc', "
					+"code.description as 'exemptionCdDesc', "
					+"CASE WHEN exemptionTypeCd=3440 THEN (select exemptionValue from Exemption where appraisalYear=@appraisalYear and exemptionTypeCd=3 and exemptionCd=oe.exemptionCd) ELSE NULL END as 'amount', "
					+"REPLACE(CONVERT(VARCHAR(10),oe.effectiveDt,110),'-','/') as effectiveDt, "
					+"REPLACE(CONVERT(VARCHAR(10),oe.expirationDt,110),'-','/') as expirationDt "
				+"FROM "
					+"OwnerExemption as oe  "
					+"INNER JOIN Owner as o "
					+"ON o.appraisalYear=@appraisalYear and oe.ownerId = o.ownerId "
					+"INNER JOIN SystemCode as code "
					+"ON code.appraisalYear=@appraisalYear and code.codeType=exemptionTypeCd and code=exemptionCd "
				+"WHERE "
					+"NOT oe.rowDeleteFlag='D' and "
					+"oe.appraisalYear = @appraisalYear and "
					+"oe.ownerId=@ownerId ;"
			UtilFunctions.execSql(sql, res);   
		}else{
			res.status(200).json({"success":true,"data":"invalid year/id"});
		}
		 
    }
};