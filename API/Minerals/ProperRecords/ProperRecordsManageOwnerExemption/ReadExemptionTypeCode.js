module.exports = {
    ReadExemptionTypeCode: function (req, res) {
        //console.log("Called ReadExemptionTypeCode")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = "";
        if(req.query.codeType){
        sql = " SELECT "
                        +" codeType as exemptionType, "
                        +" description as exemptionCdDesc, "
                        +" code as exemptionCd, "
                        +" CASE WHEN codeType=3440 THEN (select exemptionValue from Exemption where appraisalYear='"+req.query.appraisalYear+"' and exemptionTypeCd=3 and exemptionCd=code.code) ELSE NULL END as 'exemptionValue', "
                        +" code.rowDeleteFlag "
                    +" FROM  "
                        +" SystemCode as code "
                    +" WHERE "
                        +" codeType ='"+req.query.codeType+"' "
                        +" AND code.rowDeleteFlag = '' "
                        +" AND code.appraisalYear = '"+req.query.appraisalYear+"' "
                    +" ORDER BY code; "
         }
        UtilFunctions.execSql(sql, res)
    },//end ReadExemptionTypeCode
};
