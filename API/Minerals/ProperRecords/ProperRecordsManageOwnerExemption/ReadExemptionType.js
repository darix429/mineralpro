module.exports = {
    ReadExemptionType: function (req, res) {
        //console.log("Called ReadExemptionType")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = "SELECT DISTINCT "
                       +" codeType as exemptionType, "
                       +" CASE WHEN codeType=3440 THEN 'DISABLED VET' ELSE 'TOTAL EXEMPTION' END as 'exemptionTypeDesc', "
                       +" rowDeleteFlag "
                +" FROM SystemCodeType"
                +" WHERE codeType = 3430 OR codeType = 3440 "
                +" OR columnName like 'EXEMPTION_C%'"
                +" AND rowDeleteFlag = '' ";
                
        UtilFunctions.execSql(sql, res)
    },//end ReadExemptionType
};
