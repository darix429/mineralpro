module.exports = {
    ReadDisableVet: function (req, res) {
        //console.log("Called ReadDisableVet")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = "";
        // if(req.query.codeType){
        sql = " SELECT "
                        +" codeType as codeType, "
                        +" description as disableVetDesc, "
                        +" code as disableVetCd, "
                        +" CASE WHEN codeType=3440 THEN (select exemptionValue from Exemption where appraisalYear='"+req.query.appraisalYear+"' and exemptionTypeCd=3 and exemptionCd=code.code) ELSE NULL END as 'amount', "
                        +" code.rowDeleteFlag "
                    +" FROM  "
                        +" SystemCode as code "
                    +" WHERE "
                        +" codeType ='3440' "
                        +" AND code.rowDeleteFlag = '' "
                        +" AND code.appraisalYear = '"+req.query.appraisalYear+"' "
                    +" ORDER BY code; "
         // }
        UtilFunctions.execSql(sql, res)
    },//end ReadDisableVet
};
