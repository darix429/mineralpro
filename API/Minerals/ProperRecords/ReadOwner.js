module.exports = {
    ReadOwner: function (req, res) {
        //console.log("Called ReadOwner")
        var UtilFunctions = require('../../UtilFunctions.js');        
        var sql = "SELECT "
        		+"name1, "
                +"name2, "
                +"name1+'-'+CAST(ownerId as varchar(15)) as fieldName, "
                        +"name1 as displayField, "
                +"ownerId, "
                +"addrLine1, "
                +"addrLine2, "
                +"addrLine3, "
                +"city, "
                +"stateCd, "
                +"( SELECT description FROM SystemCode WHERE appraisalYear='"+ req.query.appraisalYear +"' AND rowDeleteFlag = '' AND codeType = 790 AND code = stateCd) as state, "
                +"zipcode, "

        		+"ownerId as valueField "
        		+"FROM Owner "
        		+"WHERE rowDeleteFlag = '' "
        		+"AND appraisalYear = '"+req.query.appraisalYear+"' "
                +"ORDER BY name1;"
        UtilFunctions.execSql(sql, res)
    },//end ReadOwner
};
