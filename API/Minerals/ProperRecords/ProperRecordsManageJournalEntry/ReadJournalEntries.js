module.exports = {
    Read: function (req, res) {      
        var UtilFunctions = require('../../../UtilFunctions.js');  
        sql = "SELECT "
                +"appUser, "
                +"( SELECT oc.nameAbbreviation FROM OrganizationContact oc "
                +"INNER JOIN AppUser au ON oc.idOrganizationContact = au.idOrganizationContact AND au.rowDeleteFlag = ''  "
                +"WHERE au.idAppUser = appUser AND oc.rowDeleteFlag = '') as nameAbbreviation, "
                +"( SELECT reportId FROM Report where fileName = 'JOURNALENTRY.RPT' and rowDeleteFlag = '' and appraisalYear = '"+req.query.appraisalYear+"' ) as reportId, "
                +"appraisalYear selectedAppraisalYear, "
                +"idJournalEntry, "
                +"batchId, "
                +"SUBSTRING(REPLACE(CONVERT(VARCHAR(10),openDt,111),'/','-'),1,11) as openDt, "
                +"SUBSTRING(REPLACE(CONVERT(VARCHAR(10),closeDt,111),'/','-'),1,11) as closeDt, "
                +"jeReasonCd, "
               // +"( SELECT shortDescription FROM SystemCode WHERE code = jeReasonCd AND rowDeleteFlag = '' AND codeType = 2170 AND appraisalYear = '"+req.query.appraisalYear+"') +'-'+"
                +"( SELECT description FROM SystemCode WHERE code = jeReasonCd AND rowDeleteFlag = '' AND codeType = 2170 AND appraisalYear = '"+req.query.appraisalYear+"') as jeReason, "
                +"description, "
                +"taxOfficeNotifyCd, "
                +"( SELECT description FROM SystemCode WHERE code = taxOfficeNotifyCd AND rowDeleteFlag = '' AND codeType = 5240 AND appraisalYear = '"+req.query.appraisalYear+"') as taxOfficeNotify, "
                +"jeHoldCd, "
                +"( SELECT description FROM SystemCode WHERE code = jeHoldCd AND rowDeleteFlag = '' AND codeType = 5250 AND appraisalYear = '"+req.query.appraisalYear+"') as jeHold, "
                //+"collectionExtractDt, "
                +"SUBSTRING(REPLACE(CONVERT(VARCHAR(10),collectionExtractDt,111),'/','-'),1,11) as collectionExtractDt, "
                +"rowUpdateUserid, "
                +"rowUpdateDt "
            +"FROM JournalEntry WHERE rowDeleteFlag = '' "
            +"AND batchId = "+req.query.selectedId+" ORDER BY idJournalEntry DESC;";              
        UtilFunctions.execSql(sql, res)
    },
};
