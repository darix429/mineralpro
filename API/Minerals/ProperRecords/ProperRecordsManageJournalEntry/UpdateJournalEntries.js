module.exports = {
    UpdateJournalEntries: function (req, res) {
        //console.log("Called UpdateJournalEntries")
        var UtilFunctions = require('../../../UtilFunctions.js');
         if(req.body && req.query.updateLength){
			var arrData = req.body;
			if(req.query.updateLength == 1){arrData = [req.body];}

			var sql = "";   
			if (req.query.process == 'processJe') {
				sql += "UPDATE JournalEntry SET "
						+ "batchId =  (SELECT Top 1 ISNULL(idJournalEntryBatch, 1 ) FROM JournalEntryBatch order by batchId DESC), "
						+ "collectionExtractDt = SYSDATETIME() "
						+ "WHERE batchId = 99999999 AND jeHoldCd = 0; ";		

				sql += "UPDATE JournalEntryBatch SET "
						+ "batchId =  (SELECT Top 1 ISNULL(idJournalEntryBatch, 1 ) FROM JournalEntryBatch order by batchId DESC), "
						+ "jeCount = (SELECT count(idJournalEntry) FROM JournalEntry WHERE batchId = (SELECT Top 1 ISNULL(idJournalEntryBatch, 1 ) FROM JournalEntryBatch order by batchId DESC) and jeHoldCd = 0), "
						+ "processDt = SYSDATETIME() "
						+ "WHERE batchId = 99999999; ";
				
				sql += "INSERT INTO JournalEntryBatch (batchId,jeCount) VALUES (99999999, (SELECT count(idJournalEntry) FROM JournalEntry WHERE batchId = 99999999 )); ";	
			}else{
				for (var ctr = 0; ctr < arrData.length; ctr++) {
					sql += "UPDATE JournalEntry SET "
						+" jeHoldCd = '" + arrData[ctr].jeHoldCd + "',"
						+" jeReasonCd = '" + arrData[ctr].jeReasonCd + "',"
						+" taxOfficeNotifyCd = '" + arrData[ctr].taxOfficeNotifyCd + "',"
                        +" description = UPPER('" + arrData[ctr].description + "'),"
                        +" rowUpdateDt = SYSDATETIME(), "
						+" appUser = '" + arrData[ctr].appUser + "', "
						+ "rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "', "
                        +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                    +" WHERE idJournalEntry = '" + arrData[ctr].idJournalEntry + "';";

				}
			}
			UtilFunctions.execSql(sql, res);
		}else{ 
			res.status(400).json({"success":false,"data":"Invalid request body"});
		}
    }//end UpdateJournalEntries
};
