module.exports = {
    CreateManageAgent: function (req, res) {
        //console.log("Called CreateManageAgent")
        var UtilFunctions = require('../../../UtilFunctions.js');
        
        if(req.body){
			var arrData = req.body;
			if(req.query.createLength == 1){arrData = [req.body];}
			var sql = "DECLARE @year smallint; ";
			
			
            sql += " IF OBJECT_ID(N'tempdb..#tempNewAgentIds',N'U') IS NOT NULL "
                + "  BEGIN "
                + "  DROP TABLE #tempNewAgentIds "
                + "  END; "
                + " CREATE TABLE #tempNewAgentIds"
                + " ( "
                + "   agencyCdx bigint "
                + " ) "
                + " IF OBJECT_ID(N'tempdb..#newAgentYears',N'U') IS NOT NULL "
                + "  BEGIN "
                + "  DROP TABLE #newAgentYears "
                + "  END; "
                + " CREATE TABLE #newAgentYears"
                + " ( "
                + "   year smallint "
                + " ) "
				
			var current_year = (new Date()).getFullYear();
			var appraisal_year = req.query.appraisalYear;
			var years = [];
			var last_prior = (appraisal_year <= (current_year-6))? appraisal_year : (current_year-6);
			for(year = last_prior; year<=current_year; year++){
				years.push(parseInt(year));
			}
			for(var i=0; i<years.length; i++){
				sql+="INSERT INTO #newAgentYears VALUES ('"+years[i]+"'); "
			}
			sql += " DECLARE @numOfYears int; "
				+ " DECLARE @agencyCdx VARCHAR(6); "
            // for(var ctr=0; ctr<arrData.length; ctr++){
            for(var ctr = (arrData.length - 1); ctr >= 0; ctr--){
				sql += "SET @numOfYears = (SELECT Count(*) FROM   #newAgentYears); "
					+"SET @agencyCdx = RIGHT('00000'+CONVERT(VARCHAR, (SELECT MAX(CAST(agencycdx AS INT)) AS agencyCdx FROM agency)+1),5); "
					+"WHILE @numOfYears > 0 "
					+"BEGIN "
					+" 	SET @year = ( "
					+" 		select tbl.year "
					+" 		from "
					+" 		( "
					+" 			select row_number() over(order by year asc) as rnum, year  "
					+" 			from  "
					+" 			#newAgentYears n "
					+" 		)tbl "
					+" 		where tbl.rnum = @numOfYears "
					+" 	); "
				
				sql += ""
					+"UPDATE Agency SET "
					+"agencyName = UPPER('"+arrData[ctr].agencyName+"'), "
					+"statusCd = '"+arrData[ctr].statusCd+"', "
					+"addrLine1 = UPPER('"+arrData[ctr].addrLine1+"'), "
					+"addrLine2 = UPPER('"+arrData[ctr].addrLine2+"'), "
					+"addrLine3 = UPPER('"+arrData[ctr].addrLine3+"'), "
					+"city = UPPER('"+arrData[ctr].city+"'), "
					+"stateCd = '"+arrData[ctr].stateCd+"', "
					+"zipcode = UPPER('"+arrData[ctr].zipcode+"'), "
					+"countryCd = '"+arrData[ctr].countryCd+"', "
					+"phoneNum = '"+arrData[ctr].phoneNum+"', "
					+"faxNum = '"+arrData[ctr].faxNum+"', "
					+"emailAddress = '"+arrData[ctr].emailAddress+"', "
					+"rowUpdateUserid = '"+arrData[ctr].rowUpdateUserid+"', "
					+"rowUpdateDt = GETDATE(), "
					+"rowDeleteFlag = '' "
					+"WHERE "
					+"appraisalYear=@year AND agencyCdx = @agencyCdx "
					+"IF @@ROWCOUNT=0 "
					+"INSERT INTO Agency ( "
					+" appraisalYear, "
					+" agencyCdx, "
					+" agencyName, "
					+" statusCd, "
					+" addrLine1, "
					+" addrLine2, "
					+" addrLine3, "
					+" city, "
					+" stateCd, "
					+" zipcode, "
					+" countryCd, "
					+" phoneNum, "
					+" faxNum, "
					+" emailAddress, "
					+" rowDeleteFlag , "
					+" rowUpdateUserid "
					+" )VALUES( "
					+" @year, "
					+" @agencyCdx, "
					+" UPPER('"+arrData[ctr].agencyName+"'), "
					+" '"+arrData[ctr].statusCd+"', "
					+" UPPER('"+arrData[ctr].addrLine1+"'), "
					+" UPPER('"+arrData[ctr].addrLine2+"'), "
					+" UPPER('"+arrData[ctr].addrLine3+"'), "
					+" UPPER('"+arrData[ctr].city+"'), "
					+" '"+arrData[ctr].stateCd+"', "
					+" UPPER('"+arrData[ctr].zipcode+"'), "
					+" '"+arrData[ctr].countryCd+"', "
					+" '"+arrData[ctr].phoneNum+"', "
					+" '"+arrData[ctr].faxNum+"', "
					+" '"+arrData[ctr].emailAddress+"', "
					+" '"+arrData[ctr].rowDeleteFlag+"' , "
					+" '"+arrData[ctr].rowUpdateUserid+"' "
					+" );"
					
				sql+="set @numOfYears = @numOfYears - 1; "
					+"INSERT INTO #tempNewAgentIds select @agencyCdx; "
					+"END; ";
           	}
           	var readNewlyInsertedAgent = " DECLARE @appraisalYear VARCHAR(4) = '"+req.query.appraisalYear+"'; "
				+" SELECT DISTINCT "
				+" 		count(o.agencyCdx) enableDeleteCount, "
				+" 		a.idAgency, "
				+" 		a.appraisalYear, "
				+" 		a.agencyCdx, "
				+" 		a.agencyName, "
				+" 		a.addrLine1, "
				+" 		a.addrLine2, "
				+" 		a.addrLine3, "
				+" 		a.city, "
				+" 		a.stateCd, "
				+" 		sc.description as stateName, "
				+" 		a.zipcode, "
				+" 		a.countryCd, "
				+" 		cc.description as countryName,"
				+" 		a.phoneNum, "
				+" 		a.faxNum, "
				+" 		a.emailAddress, "
				+" 		a.statusCd, "
				+" 		s.description, "
				+" 		a.rowDeleteFlag "
                +" FROM Agency a "
                +" 		INNER JOIN SystemCode sc ON a.stateCd = sc.code AND sc.CodeType = 790 AND sc.rowDeleteFlag = '' AND sc.appraisalYear = @appraisalYear "
                +" 		INNER JOIN SystemCode cc ON a.countryCd = cc.code AND cc.CodeType = 810 AND cc.rowDeleteFlag = '' AND cc.appraisalYear = @appraisalYear "
                +" 		LEFT JOIN SystemCode s ON s.code = a.statusCd AND s.rowDeleteFlag= '' AND s.codeType = 30 AND s.appraisalYear = @appraisalYear "
                +" 		LEFT JOIN Owner o ON a.agencyCdx = o.agencyCdx AND o.appraisalYear = a.appraisalYear AND o.rowDeleteFlag = ''"
                +" WHERE EXISTS(SELECT temp.agencyCdx from #tempNewAgentIds temp where temp.agencyCdx = a.agencyCdx )"
                +" 		AND a.rowDeleteFlag = '' "
                +" 		AND a.appraisalYear = @appraisalYear"
                +" GROUP BY a.idAgency, "
				+"		a.appraisalYear, "
				+"		a.agencyCdx, "
				+"		a.agencyName, "
				+"		a.addrLine1, "
				+"		a.addrLine2, "
				+"		a.addrLine3, "
				+"		a.city, "
				+"		a.stateCd, "
				+"		sc.description, "
				+"		a.zipcode, "
				+"		a.countryCd, "
				+"		cc.description,"
				+"		a.phoneNum, "
				+"		a.faxNum, "
				+"		a.emailAddress, "
				+"		a.statusCd, "
				+"		s.description, "
				+"		a.rowDeleteFlag "
				+" ORDER BY a.idAgency DESC ";
			sql += readNewlyInsertedAgent;
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"No data received"});
		}
    },//end CreateMaintainAgent
    
};