module.exports = {
    UpdateManageAgent: function (req, res) {
        //console.log("Called UpdateManageAgent")
        var UtilFunctions = require('../../../UtilFunctions.js');
        if(req.body && req.query.updateLength){
			var arrData = req.body;
			if(req.query.updateLength == 1){arrData = [req.body];}
			
			var sql = "";     
			for (var ctr = 0; ctr < arrData.length; ctr++) {
                sql += "UPDATE Agency SET "
                    +" agencyCdx = '" + arrData[ctr].agencyCdx + "',"
                    +" agencyName = UPPER('" + arrData[ctr].agencyName + "'),"
                    +" statusCd = '" + arrData[ctr].statusCd + "',"
                    +" addrLine1 = UPPER('" + arrData[ctr].addrLine1 + "')," 
                    +" addrLine2 = UPPER('" + arrData[ctr].addrLine2 + "')," 
                    +" addrLine3 = UPPER('" + arrData[ctr].addrLine3 + "')," 
                    +" city = UPPER('" + arrData[ctr].city + "')," 
                    +" stateCd = '" + arrData[ctr].stateCd + "'," 
                    +" zipcode = UPPER('" + arrData[ctr].zipcode + "')," 
                    +" countryCd = '" + arrData[ctr].countryCd + "'," 
                    +" phoneNum = '" + arrData[ctr].phoneNum + "'," 
                    +" faxNum = '" + arrData[ctr].faxNum + "'," 
                    +" emailAddress = '" + arrData[ctr].emailAddress + "'," 
                    +" rowUpdateDt = GETDATE(), "
                    +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "', "
                    +" rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "'"
                +" WHERE idAgency = '" + arrData[ctr].idAgency + "' "
                +" AND appraisalYear= '" + req.query.appraisalYear + "' ; ";
                
                if(arrData[ctr].rowDeleteFlag == 'D'){
                    sql += "UPDATE Owner SET agencyCdx = '', rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                        +" WHERE agencyCdx in (SELECT agencyCdx FROM Agency "
                        +" WHERE idAgency = '" + arrData[ctr].idAgency + "' "
                        +" AND appraisalYear= '" + req.query.appraisalYear + "') "
                        +" AND appraisalYear= '" + req.query.appraisalYear + "'; ";
                }
                
           }
           //console.log(sql);
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"Invalid request body"});
		}
    }//end UpdateMaintainAgent
};
 