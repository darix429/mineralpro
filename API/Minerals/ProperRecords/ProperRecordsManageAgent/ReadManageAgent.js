module.exports = {
    ReadManageAgent: function (req, res) {
        //console.log("Called ReadManageAgent")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = "UPDATE LastUpdateTime SET dataRefreshTime = getdate() "
                  +"WHERE tableName = 'Agency' AND currentUserId = '"+req.query.idAppUser+"' "
                  +"IF @@ROWCOUNT=0 "
                  +"BEGIN "
                  +"INSERT INTO LastUpdateTime (tableName, currentUserId) VALUES('Agency', '"+req.query.idAppUser+"')"
                  +"END "
                +"SELECT DISTINCT "
                       +" count(o.agencyCdx) enableDeleteCount, "
                       +" a.idAgency, "
                       +" a.appraisalYear, "
                       +" a.agencyCdx, "
                       +" a.agencyName, "
                       +" a.addrLine1, "
                       +" a.addrLine2, "
                       +" a.addrLine3, "
                       +" a.city, "
                       +" a.stateCd, "
                       +" sc.description as stateName, "
                       +" a.zipcode, "
                       +" a.countryCd, "
                       +" cc.description as countryName,"
                       +" a.phoneNum, "
                       +" a.faxNum, "
                       +" a.emailAddress, "
                       +" a.statusCd, "
                       +" s.description, "
                       +" a.rowDeleteFlag "
                +" FROM Agency a "
                +" INNER JOIN SystemCode sc ON a.stateCd = sc.code AND sc.CodeType = 790 AND sc.rowDeleteFlag = '' AND sc.appraisalYear ='" +req.query.appraisalYear+ "' "
                +" INNER JOIN SystemCode cc ON a.countryCd = cc.code AND cc.CodeType = 810 AND cc.rowDeleteFlag = '' AND cc.appraisalYear ='" +req.query.appraisalYear+ "' "
                +" LEFT JOIN SystemCode s ON s.code = a.statusCd AND s.rowDeleteFlag= '' AND s.codeType = 30 AND s.appraisalYear ='" +req.query.appraisalYear+ "' "
                +" LEFT JOIN Owner o ON a.agencyCdx = o.agencyCdx AND o.appraisalYear = a.appraisalYear AND o.rowDeleteFlag = ''"
                +" WHERE a.rowDeleteFlag = '' "
                +" AND a.appraisalYear = '"+ req.query.appraisalYear + "'"
                +" GROUP BY a.idAgency, "
                       +" a.appraisalYear, "
                       +" a.agencyCdx, "
                       +" a.agencyName, "
                       +" a.addrLine1, "
                       +" a.addrLine2, "
                       +" a.addrLine3, "
                       +" a.city, "
                       +" a.stateCd, "
                       +" sc.description, "
                       +" a.zipcode, "
                       +" a.countryCd, "
                       +" cc.description,"
                       +" a.phoneNum, "
                       +" a.faxNum, "
                       +" a.emailAddress, "
                       +" a.statusCd, "
                       +" s.description, "
                       +" a.rowDeleteFlag "
                       +"ORDER BY a.idAgency DESC ";
                
                // sql = "select top 1 * from Agency;"
                if(req.query.dataCount == 0){      
                    UtilFunctions.execSql(sql, res)
                }else{
                    var sqlCond = "SELECT * FROM LastUpdateTime "
                        +"WHERE lastUpdateTime > dataRefreshTime "
                        +"AND tableName = 'Agency' "
                        +"AND currentUserId = '"+req.query.idAppUser+"'";

                    UtilFunctions.execSqlCallBack(sqlCond, res, function (rec) {
                        if(rec.length > 0){
                            UtilFunctions.execSql(sql, res)
                        }else{
                            res.status(200).json({"success":false,"data":"Do not refresh the data"});
                        }
                    });                     
                }
    },//end ReadMaintainAgent
};
