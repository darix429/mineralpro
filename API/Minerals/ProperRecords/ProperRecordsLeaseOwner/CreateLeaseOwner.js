module.exports = {
    Create: function (req, res) {
        var UtilFunctions = require('../../../UtilFunctions.js');
        //console.log("Called CreateLeaseOwner");		
		var data = req.body;
		if(req.query.createLength==1){ data = [req.body]; }
		if(req.query.appraisalYear && data){
			var sql ="DECLARE @maxYearLife int = 25, @unitId varchar(25) "
			+"SELECT @unitId = unitId FROM Lease WHERE leaseId = '"+data[0].leaseId+"' AND appraisalYear = '"+req.query.appraisalYear+"' AND rowDeleteFlag = '' "   
			
			+"IF @unitId = 0 BEGIN "
			+"SELECT @maxYearLife = max([maxYearLife]) FROM Appraisal WHERE subjectId = "+data[0].leaseId+" AND not rowDeleteFlag='D' AND appraisalYear = '"+req.query.appraisalYear+"' "		
			+"END "
			+"ELSE BEGIN "
			+"SELECT @maxYearLife = max([maxYearLife]) FROM Appraisal WHERE subjectId = @unitId AND not rowDeleteFlag='D' AND appraisalYear = '"+req.query.appraisalYear+"' "
			+"END ";
			for(var i=0; i<data.length; i++){	
					sql +="UPDATE LeaseOwner SET "
						+"appraisalYear= '"+req.query.appraisalYear+"', "
						+"leaseId= '"+data[i].leaseId+"', "
						+"ownerId= "+data[i].ownerId+", "
						+"interestTypeCd= '"+data[i].interestTypeCd+"', "
						+"interestPercent= '"+parseFloat(data[i].interestPercent).toFixed(8)+"', "
						+"rowUpdateUserid= '"+data[i].rowUpdateUserid+"', "
						+"rowUpdateDt = GETDATE(), "
						+"rowDeleteFlag= '"+data[i].rowDeleteFlag+"' "
					+"WHERE "
						+"appraisalYear= '"+req.query.appraisalYear+"' "
						+"AND leaseId= '"+data[i].leaseId+"' "
						+"AND ownerId= "+data[i].ownerId+" "
						+"AND interestTypeCd= '"+data[i].interestTypeCd+"' "
					+"IF @@ROWCOUNT=0 "
					+"INSERT INTO LeaseOwner( "
						+"appraisalYear, "
						+"leaseId, "
						+"ownerId, "
						+"interestTypeCd, "
						+"interestPercent, "
						+"rowUpdateUserid, "
						+"rowDeleteFlag "
					+")VALUES( "
						+"'"+req.query.appraisalYear+"', "
						+"'"+data[i].leaseId+"', "
						+" "+data[i].ownerId+", "
						+"'"+data[i].interestTypeCd+"', "
						+"'"+parseFloat(data[i].interestPercent).toFixed(8)+"', "
						+"'"+data[i].rowUpdateUserid+"', "
						+"'"+data[i].rowDeleteFlag+"' "
					+"); ";
				// if(column!="unassigned"){
					// sql+="UPDATE LeaseValueSummary SET "
					// +" "+column+" = (select sum(interestPercent) from LeaseOwner where appraisalYear='"+req.query.appraisalYear+"' and leaseId='"+data[i].leaseId+"' and interestTypeCd='"+data[i].interestTypeCd+"' and not rowDeleteFlag='D') "
					// +"WHERE appraisalYear='"+req.query.appraisalYear+"' "
					// +"AND leaseId='"+data[i].leaseId+"'; ";
				// }		
			}

			//to execute calculation once all the new record added on query
			sql+="EXECUTE spCalcLeaseValueSummaryViaLeaseOwner "+data[0].leaseId+","+req.query.appraisalYear+" "
				+"EXECUTE spCalcUnitValueSummaryViaLeaseValueSummary @unitId,'"+req.query.appraisalYear+"', '" + data[0].rowUpdateUserid + "' ";

			//sql+="EXECUTE spCalcAmortizationYears  "+data[i].rowUpdateUserid+","+req.query.appraisalYear+", @maxYearLife, 1, 2, "+data[i].leaseId+", 0 ";
			sql+=" IF @unitId = 0 BEGIN "	
			+"EXECUTE spCalcAmortizationYears  "+data[0].rowUpdateUserid+","+req.query.appraisalYear+", @maxYearLife, 1, 2, "+data[0].leaseId+", 0 "
			+"END "	
			+"ELSE BEGIN "		   	
					+"EXECUTE spCalcAmortizationYears  "+data[0].rowUpdateUserid+","+req.query.appraisalYear+", @maxYearLife, 1, 1, @unitId, 0 "
			+"EXECUTE spCalcAmortizationYears  "+data[0].rowUpdateUserid+","+req.query.appraisalYear+", @maxYearLife, 1, 2, "+data[0].leaseId+", 0 "
			+"END ";

			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"Error"});
		}
		
    },
};

