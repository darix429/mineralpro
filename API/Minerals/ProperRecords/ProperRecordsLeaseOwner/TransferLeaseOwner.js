module.exports = {
    Update: function (req, res) {
        var UtilFunctions = require('../../../UtilFunctions.js');
        //console.log("Called TransferLeaseOwner");
        var data = req.body;
        if (req.query.updateLength && req.query.appraisalYear
                && req.query.ownerId && req.query.leaseId && req.query.PctTransfer
                && req.query.interestTypeCd && req.query.ownerTransferPct) {

            if (req.query.updateLength == 1) {
                data = [req.body];
            }

            //var sql = "DROP TABLE IF EXISTS #tempOwner; "
            var sql ="IF OBJECT_ID (N'tempdb..#tempOwner', N'U') IS NOT NULL "
                    +"DROP TABLE #tempOwner "
//                    +"\nGO \n"
                    + "DECLARE @appraisalYear varchar(10) = '" + req.query.appraisalYear + "', "
                    + "@rowUpdateUserId varchar(15) = '" + req.query.rowUpdateUserid + "', "            
                    + "@leaseId varchar(15) = '" + req.query.leaseId + "', " 
                    + "@interestTypeCd varchar(15) = '" + req.query.interestTypeCd + "', " 
                    + "@outerOwnerId varchar(15) = '" + req.query.ownerId + "', " 
                    + "@ownerTransferPct varchar(15) = '" + req.query.ownerTransferPct + "', " 
                    + "@PctTransfer varchar(15) = '" + req.query.PctTransfer + "', " 
                    + "@ownerId varchar(15), "
                    + "@ownerReceivePct varchar(15), "
                    + "@ownerReceiveValue varchar(15), "
                    + "@exist int "
                           
                for (var i = 0; i < data.length; i++) {
                    if (i == 0) {
                        sql += "SELECT '" + data[i].ownerId + "' ownerId, '" + data[i].ownerReceivePct + "' ownerReceivePct , '" + data[i].OwnerReceiveValue + "' ownerReceiveValue INTO #tempOwner "
                    } else {
                        sql += "UNION SELECT '" + data[i].ownerId + "' ownerId, '" + data[i].ownerReceivePct + "' ownerReceivePct , '" + data[i].OwnerReceiveValue + "' ownerReceiveValue "
                    }
                }

                if(req.query.PctTransfer == '100'){
                    sql += "UPDATE LeaseOwner SET rowDeleteFlag = 'D', ownerValue = 0, interestPercent = 0, rowUpdateDt = SYSDATETIME(), rowUpdateUserId = @rowUpdateUserId  "
                        + "WHERE leaseId = @leaseId AND ownerId = @outerOwnerId "
                        + "AND appraisalYear = @appraisalYear AND interestTypeCd = @interestTypeCd ";
                }else{
                    sql += "UPDATE LeaseOwner "
                            +"SET interestPercent = CAST(lo.interestPercent AS FLOAT) - CAST(@ownerTransferPct AS FLOAT), "
                                +"rowUpdateDt = SYSDATETIME(), "
                                + "rowUpdateUserId = @rowUpdateUserId, "
                                +"ownerValue = ((100 - CAST(@PctTransfer AS FLOAT)) * CAST(lo.ownerValue AS FLOAT))/100 "
                            +"FROM LeaseOwner lo "
                            +"WHERE leaseId = @leaseId "
                            +"AND ownerId = @outerOwnerId AND appraisalYear = @appraisalYear AND rowDeleteFlag = '' AND interestTypeCd = @interestTypeCd ";
                }

                sql += "DECLARE tempOwner CURSOR FOR "
                    + "SELECT * FROM #tempOwner "

                    + "OPEN tempOwner "
                    + "FETCH NEXT FROM tempOwner INTO @ownerId, @ownerReceivePct, @ownerReceiveValue "

                    + "WHILE @@FETCH_STATUS = 0 "
                    + "BEGIN "

                    + "IF(CAST(@ownerReceivePct AS FLOAT) > 0) "          
                    + "BEGIN "

                    + "UPDATE LeaseOwner "
                    + "SET interestTypeCd = @interestTypeCd, "
                            + "rowUpdateDt = SYSDATETIME(), "
                            + "rowUpdateUserId = @rowUpdateUserId, "
                            + "interestPercent = @ownerReceivePct, "
                            + "ownerValue = @ownerReceiveValue, "
                            + "rowDeleteFlag = '' "
                    + "WHERE appraisalYear = @appraisalYear "
                    + "AND leaseId = @leaseId "
                    + "AND ownerId = @ownerId "
                    + "AND rowDeleteFlag = 'D' AND interestTypeCd = @interestTypeCd "

                    + "IF @@ROWCOUNT = 0 "
                        + "BEGIN "
                        + "SELECT @exist = count(1) FROM LeaseOwner WHERE appraisalYear = @appraisalYear AND leaseId = @leaseId AND ownerId = @ownerId AND interestTypeCd = @interestTypeCd "

                        + "IF (@exist > 0) "
                        + "BEGIN "
                            + "UPDATE LeaseOwner "
                            + "SET interestTypeCd = @interestTypeCd, "
                                + "rowUpdateDt = SYSDATETIME(), "
                                + "rowUpdateUserId = @rowUpdateUserId, "
                                + "interestPercent = CAST(lo.interestPercent AS FLOAT) + CAST(@ownerReceivePct  AS FLOAT), "
                                + "ownerValue = CAST(lo.ownerValue AS FLOAT) + CAST(@ownerReceiveValue AS FLOAT), "
                                + "rowDeleteFlag = '' "
                            + "FROM LeaseOwner lo "
                            + "WHERE appraisalYear = @appraisalYear "
                            + "AND leaseId = @leaseId "
                            + "AND ownerId = @ownerId AND interestTypeCd = @interestTypeCd "
                        + "END "
                        + "ELSE "
                        + "BEGIN "
                            + "INSERT INTO LeaseOwner (appraisalYear, leaseId, ownerId, interestTypeCd, interestPercent, ownerValue, rowUpdateUserId) "
                                + "SELECT @appraisalYear,@leaseId,@ownerId,@interestTypeCd,@ownerReceivePct,@ownerReceiveValue, @rowUpdateUserId "
                        + "END "

                        + "END "
                        
                        + "END "                                 
                    + "FETCH NEXT FROM tempOwner INTO @ownerId, @ownerReceivePct, @ownerReceiveValue "
                    + "END "

                    + "CLOSE tempOwner "
                    + "DEALLOCATE tempOwner ";

                   // + "EXECUTE spCalcLeaseValueSummaryViaLeaseOwner @leaseId, @appraisalYear " 

                   // + "EXECUTE spCalcAmortizationYears @rowUpdateUserId, @appraisalyear, 25, 1, 2, @leaseId, 0 "; 

            UtilFunctions.execSql(sql, res);
        } else {
            res.status(400).json({"success": false, "data": "Error"});
        }

    },
};

