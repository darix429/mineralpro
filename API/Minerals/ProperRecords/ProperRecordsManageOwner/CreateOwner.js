module.exports = {
    CreateOwner: function (req, res) {
        //console.log("Called CreateOwner")
        var UtilFunctions = require('../../../UtilFunctions.js');

         if(req.body){
            var arrData = req.body;
            if(req.query.createLength == 1){arrData = [req.body];}
            var sql = "DECLARE @ownerId bigint, @year smallint; ";
            var confi = "N";
            var exemptionTypeCd = ['3440','3430'];
            var exemptionCd = [];
            var effectiveDt = [];
            var expirationDt = [];
           
            sql += " IF OBJECT_ID(N'tempdb..#tempNewOwnerIds',N'U') IS NOT NULL "
                + "  BEGIN "
                + "  DROP TABLE #tempNewOwnerIds "
                + "  END; "
                + " CREATE TABLE #tempNewOwnerIds"
                + " ( "
                + "   ownerId bigint "
                + " ) "
            sql += " IF OBJECT_ID(N'tempdb..#newOwnerYears',N'U') IS NOT NULL "
                + "  BEGIN "
                + "  DROP TABLE #newOwnerYears "
                + "  END; "
                + " CREATE TABLE #newOwnerYears"
                + " ( "
                + "   year smallint "
                + " ) "
				
			var current_year = (new Date()).getFullYear();
			var appraisal_year = req.query.appraisalYear;
			var years = [];
			var last_prior = (appraisal_year <= (current_year-6))? appraisal_year : (current_year-6);
			for(year = last_prior; year<=current_year; year++){
				years.push(parseInt(year));
			}
			for(var i=0; i<years.length; i++){
				sql+=" INSERT INTO #newOwnerYears VALUES ('"+years[i]+"'); "
			}
			sql += " DECLARE @numOfYears int; "
            for(var ctr=0; ctr<arrData.length; ctr++){
				exemptionCd = [arrData[ctr].disableVetCd,arrData[ctr].totalExemptionCd];
				effectiveDt = [arrData[ctr].disableVetEffectiveDt,arrData[ctr].totalExemptionEffectiveDt];
				expirationDt = [arrData[ctr].disableVetExpirationDt,arrData[ctr].totalExemptionExpirationDt];
				var zipcodeValue = arrData[ctr].zipcode;
				arrData[ctr].zipcode = zipcodeValue.replace('-','');
				
				if(arrData[ctr].confidential == "Yes"){
					confi = "Y"
				}else{
					confi = "N"
				}
				
				sql+="SET @numOfYears = (SELECT Count(*) FROM   #newowneryears); "
					+"SET @ownerId = (SELECT MAX(ownerId)+1 FROM Owner); "
					+"WHILE @numOfYears > 0 "
					+"BEGIN "
					+" SET @year = ( "
					+" 	select tbl.year "
					+" 	from "
					+" 	( "
					+" 		select row_number() over(order by year asc) as rnum, year  "
					+" 		from  "
					+" 		#newOwnerYears n "
					+" 	)tbl "
					+" 	where tbl.rnum = @numOfYears "
					+" ); "
					+"INSERT INTO Owner ( "
					+"	appraisalYear, "
					+" 	ownerId, "
					+" 	agencyCdx, "
					+" 	name1, "
					+" 	name2, "
					+" 	birthDt, "
					+" 	addrLine1, "
					+" 	addrLine2, "
					+" 	addrLine3, "
					+" 	city, "
					+" 	stateCd, "
					+" 	zipcode, "
					+" 	countryCd, "
					+" 	phoneNum, "
					+" 	faxNum, "
					+" 	emailAddress, "
					+" 	ownerChangeCd, "
					// +" ownerTypeCdx, "
					+" 	rowUpdateUserid, "
					+" 	confidentialInd, "
					+" 	rowDeleteFlag "
					+")VALUES ("
					+" 	@year, "
					+" 	@ownerId, "
					+" 	'"+arrData[ctr].agencyCdx+"', "
					+" 	UPPER('"+arrData[ctr].name+"'), "
					+" 	UPPER('"+arrData[ctr].name2+"'), "
					+" 	'"+arrData[ctr].birthDt+"', "
					+" 	UPPER('"+arrData[ctr].inCareOf+"'), "
					+" 	UPPER('"+arrData[ctr].addrLine2+"'), "
					+" 	UPPER('"+arrData[ctr].addrLine3+"'), "
					+" 	UPPER('"+arrData[ctr].city+"'), "
					+" 	'"+arrData[ctr].stateCd+"', "
					+" 	UPPER('"+arrData[ctr].zipcode+"'), "
					+" 	'"+arrData[ctr].countryCd+"', "
					+" 	'"+arrData[ctr].phoneNum+"', "
					+" 	'"+arrData[ctr].faxNum+"', "
					+" 	'"+arrData[ctr].emailAddress+"', "
					+" 	'"+arrData[ctr].ownerChangeCd+"', "
					// 	+" '"+arrData[ctr].ownerTypeCdx+"', "
					+" 	'"+arrData[ctr].rowUpdateUserid+"', "
					+" 	'"+confi+"', "
					+" 	'"+arrData[ctr].rowDeleteFlag+"' );"
					

					for (var i = 0; i < 2; i++) {
						if (exemptionCd[i] != 0) {
							sql += ""
								+"IF @year = '"+appraisal_year+"' "
								+"BEGIN "
								+"UPDATE OwnerExemption SET rowDeleteFlag='' ,effectiveDt= '"+ effectiveDt[i] +"',expirationDt='"+ expirationDt[i] +"' ,rowUpdateDt = GETDATE(), rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
								+"WHERE appraisalYear = @year AND ownerId=(SELECT max(ownerId) FROM Owner) AND exemptionTypeCd='"+ exemptionTypeCd[i] +"' AND exemptionCd='"+ exemptionCd[i] +"' "
								+"IF @@ROWCOUNT=0 "
								+"INSERT INTO OwnerExemption(appraisalYear, ownerId, exemptionTypeCd, exemptionCd,effectiveDt,expirationDt,rowUpdateUserid) "
								+"VALUES( "
									+"@year, "
									+"@ownerId, "
									+"'"+ exemptionTypeCd[i] +"', "
									+"'"+ exemptionCd[i] +"', "
									+"'"+ effectiveDt[i] +"', "
									+"'"+ expirationDt[i] +"', "
									+"'"+ arrData[ctr].rowUpdateUserid +"'"
									+"); "
								+""
								+"END; ";
						}
					}
					
					sql+=" set @numOfYears = @numOfYears - 1; "
						+" END; "
						+" INSERT INTO #tempNewOwnerIds select @ownerId; "
				
			}
                
            sql += " SELECT DISTINCT o.* FROM owner o join #tempNewOwnerIds n on o.ownerId = n.ownerId WHERE o.appraisalYear = '"+req.query.appraisalYear+"' AND o.rowDeleteFlag = '';"
            UtilFunctions.execSql(sql, res);
        }else{
            res.status(400).json({"success":false,"data":"No data received"});
        }
    },//end CreateMaintainOwner

};
