module.exports = {
    UpdateOwner: function (req, res) {
        //console.log("Called UpdateOwner")
        var UtilFunctions = require('../../../UtilFunctions.js');
        
        if(req.body && req.query.updateLength){
			var arrData = req.body;
			if(req.query.updateLength == 1){arrData = [req.body];}
			
			var sql = "";
            var confi = "N";     
            var exemptionTypeCd = ['3440','3430'];
            var exemptionCd = [];
            var exemptionID = [];
            var effectiveDt = [];
            var expirationDt = [];
            var idOwnerExemption = [];
			for (var ctr = 0; ctr < arrData.length; ctr++) {
                 exemptionCd = [arrData[ctr].disableVetCd,arrData[ctr].totalExemptionCd];
                 exemptionID = [arrData[ctr].disableIdOwnerExemption,arrData[ctr].totalIdOwnerExemption];
                effectiveDt = [arrData[ctr].disableVetEffectiveDt,arrData[ctr].totalExemptionEffectiveDt];
                expirationDt = [arrData[ctr].disableVetExpirationDt,arrData[ctr].totalExemptionExpirationDt];
                idOwnerExemption = [arrData[ctr].disableIdOwnerExemption,arrData[ctr].totalIdOwnerExemption];
                var zipcodeValue = arrData[ctr].zipcode;
                arrData[ctr].zipcode = zipcodeValue.replace('-','');
                
                if(arrData[ctr].confidential == "Yes"){
                    confi = "Y"
                }else{
                    confi = "N"
                }
                sql += "UPDATE Owner SET "
                    +" agencyCdx = '"+arrData[ctr].agencyCdx+"', "
                    +" name1 = UPPER('"+arrData[ctr].name+"'), "
                    +" name2 = UPPER('"+arrData[ctr].name2+"'), "
                    +" birthDt = '"+arrData[ctr].birthDt+"', "
                    +" addrLine1 = UPPER('"+arrData[ctr].inCareOf+"'), "
                    +" addrLine2 = UPPER('"+arrData[ctr].addrLine2+"'), "
                    +" addrLine3 = UPPER('"+arrData[ctr].addrLine3+"'), "
                    +" city = UPPER('"+arrData[ctr].city+"'), "
                    +" stateCd = '"+arrData[ctr].stateCd+"', "
                    +" zipcode = UPPER('"+arrData[ctr].zipcode+"'), "
                    +" countryCd = '"+arrData[ctr].countryCd+"', "
                    +" phoneNum = '"+arrData[ctr].phoneNum+"', "
                    +" faxNum = '"+arrData[ctr].faxNum+"', "
                    +" emailAddress = '"+arrData[ctr].emailAddress+"', "
                    +" ownerChangeCd = '"+arrData[ctr].ownerChangeCd+"', "
                    // +" ownerTypeCdx = '"+arrData[ctr].ownerTypeCdx+"', "
                    +" rowUpdateUserid = '"+arrData[ctr].rowUpdateUserid+"', "
                    +" confidentialInd = '"+confi+"', "
                    +" rowDeleteFlag = '"+arrData[ctr].rowDeleteFlag+"', "
                    +" rowUpdateDt = GETDATE() "
                +" WHERE ownerId = '" + arrData[ctr].ownerId + "'"
                +" AND appraisalYear = '"+req.query.appraisalYear+"'; "  

                if (arrData[ctr].rowDeleteFlag != "D") {
                    for (var i = 0; i < 2; i++) {
                    sql +=""

                    sql +="IF (SELECT idOwnerExemption FROM OwnerExemption WHERE idOwnerExemption = '"+ exemptionID[i] +"' AND ownerId='"+ arrData[ctr].ownerId +"' AND exemptionTypeCd='"+ exemptionTypeCd[i] +"') > 0 BEGIN "
                        //UPDATE
                        if (exemptionCd[i]!=0) {
                            sql +="UPDATE OwnerExemption SET rowDeleteFlag='' ,  "
                            +"effectiveDt= '"+ effectiveDt[i] +"',  "
                            +"expirationDt='"+ expirationDt[i] +"' ,  "
                            +"rowUpdateDt = GETDATE(),  "
                            +"exemptionCd='"+ exemptionCd[i] +"',  "
                            +"rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "'  "
                            +"WHERE appraisalYear = '"+ req.query.appraisalYear +"'  "
                            +"AND ownerId='"+ arrData[ctr].ownerId +"'  "
                            +"AND exemptionTypeCd='"+ exemptionTypeCd[i] +"'  "
                            +"AND idOwnerExemption ='"+ exemptionID[i] +"' "
                        }else{
                            sql +="UPDATE OwnerExemption SET rowDeleteFlag='D' ,  "
                            +"effectiveDt= '"+ effectiveDt[i] +"',  "
                            +"expirationDt='"+ expirationDt[i] +"' ,  "
                            +"rowUpdateDt = GETDATE(),  "
                            +"exemptionCd='"+ exemptionCd[i] +"',  "
                            +"rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "'  "
                            +"WHERE appraisalYear = '"+ req.query.appraisalYear +"'  "
                            +"AND ownerId='"+ arrData[ctr].ownerId +"'  "
                            +"AND exemptionTypeCd='"+ exemptionTypeCd[i] +"'  "
                            +"AND idOwnerExemption ='"+ exemptionID[i] +"' "
                        };
                        // +"IF (SELECT idOwnerExemption FROM OwnerExemption WHERE exemptionCd = '"+ exemptionCd[i] +"' AND ownerId='"+ arrData[ctr].ownerId +"' AND exemptionTypeCd='"+ exemptionTypeCd[i] +"') > 0 BEGIN "
                        //     +"UPDATE OwnerExemption SET rowDeleteFlag='' ,  "
                        //     +"effectiveDt= '"+ effectiveDt[i] +"',  "
                        //     +"expirationDt='"+ expirationDt[i] +"' ,  "
                        //     +"rowUpdateDt = GETDATE(),  "
                        //     +"exemptionCd='"+ exemptionCd[i] +"',  "
                        //     +"rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "'  "
                        //     +"WHERE appraisalYear = '"+ req.query.appraisalYear +"'  "
                        //     +"AND ownerId='"+ arrData[ctr].ownerId +"'  "
                        //     +"AND exemptionTypeCd='"+ exemptionTypeCd[i] +"'  "
                        //     +"AND idOwnerExemption ='"+ exemptionID[i] +"' "
                        // +"END ELSE BEGIN "
                        //     +"UPDATE OwnerExemption SET rowDeleteFlag ='D',  "
                        //     +"effectiveDt= '"+ effectiveDt[i] +"',  "
                        //     +"expirationDt='"+ expirationDt[i] +"' ,  "
                        //     +"rowUpdateDt = GETDATE(),  "
                        //     +"exemptionCd='"+ exemptionCd[i] +"',  "
                        //     +"rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "'  "
                        //     +"WHERE exemptionTypeCd='"+ exemptionTypeCd[i] +"' "
                        //     +"AND ownerId='"+ arrData[ctr].ownerId +"'  "
                        //     +"AND appraisalYear='"+ req.query.appraisalYear +"'  "
                        // +"END "
                    sql +="END ELSE BEGIN "
                        //INSERT
                        if (exemptionCd[i]!=0) {
                            sql +="UPDATE OwnerExemption SET rowDeleteFlag='' , "
                            +"effectiveDt= '"+ effectiveDt[i] +"', "
                            +"expirationDt='"+ expirationDt[i] +"' , "
                            +"rowUpdateDt = GETDATE(), "
                            +"exemptionCd='"+ exemptionCd[i] +"', "
                            +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                        +"WHERE appraisalYear = '"+ req.query.appraisalYear +"' "
                            +"AND ownerId='"+ arrData[ctr].ownerId +"' "
                            +"AND exemptionTypeCd='"+ exemptionTypeCd[i] +"' "
                        +"IF @@ROWCOUNT=0 "
                            +"INSERT INTO OwnerExemption(appraisalYear, ownerId, exemptionTypeCd, exemptionCd,effectiveDt,expirationDt,rowUpdateUserid) "
                            +"VALUES( "
                                +"'"+ req.query.appraisalYear +"', "
                                +"'"+ arrData[ctr].ownerId +"', "
                                +"'"+ exemptionTypeCd[i] +"', "
                                +"'"+ exemptionCd[i] +"', "
                                +"'"+ effectiveDt[i] +"', "
                                +"'"+ expirationDt[i] +"', "
                                +"'"+ arrData[ctr].rowUpdateUserid +"'"                            
                                +");"
                        }else{
                            sql +=" PRINT('do nothing'); "
                        };
                         
                    sql+="END "
                    //start temp comment
                    // +"IF (SELECT idOwnerExemption FROM OwnerExemption WHERE exemptionCd = '"+ exemptionCd[i] +"' AND ownerId='"+ arrData[ctr].ownerId +"' AND exemptionTypeCd='"+ exemptionTypeCd[i] +"') > 0 BEGIN "
                    //     +"UPDATE OwnerExemption SET rowDeleteFlag ='D' WHERE exemptionTypeCd='"+ exemptionTypeCd[i] +"' "
                    //     +"AND ownerId='"+ arrData[ctr].ownerId +"'  "
                    //     +"AND appraisalYear='"+ req.query.appraisalYear +"'  "
                        
                    //     +"UPDATE OwnerExemption SET rowDeleteFlag='' ,  "
                    //     +"effectiveDt= '"+ effectiveDt[i] +"',  "
                    //     +"expirationDt='"+ expirationDt[i] +"' ,  "
                    //     +"rowUpdateDt = GETDATE(),  "
                    //     +"exemptionCd='"+ exemptionCd[i] +"',  "
                    //     +"rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "'  "
                    //     +"WHERE appraisalYear = '"+ req.query.appraisalYear +"'  "
                    //     +"AND ownerId='"+ arrData[ctr].ownerId +"'  "
                    //     +"AND exemptionTypeCd='"+ exemptionTypeCd[i] +"'  "
                    //     +"AND idOwnerExemption = (SELECT idOwnerExemption FROM OwnerExemption WHERE exemptionCd = '"+ exemptionCd[i] +"' AND ownerId='"+ arrData[ctr].ownerId +"' AND exemptionTypeCd='"+ exemptionTypeCd[i] +"') "
                    // +"END ELSE BEGIN "
                    //     +"UPDATE OwnerExemption SET rowDeleteFlag='' ,  "
                    //     +"effectiveDt= '"+ effectiveDt[i] +"',  "
                    //     +"expirationDt='"+ expirationDt[i] +"' ,  "
                    //     +"rowUpdateDt = GETDATE(),  "
                    //     +"exemptionCd='"+ exemptionCd[i] +"',  "
                    //     +"rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "'  "
                    //     +"WHERE appraisalYear = '"+ req.query.appraisalYear +"'  "
                    //     +"AND ownerId='"+ arrData[ctr].ownerId +"'  "
                    //     +"AND exemptionTypeCd='"+ exemptionTypeCd[i] +"'  "
                    //     +" IF @@ROWCOUNT = 0 "
                    //     +"INSERT INTO OwnerExemption(appraisalYear, ownerId, exemptionTypeCd, exemptionCd,effectiveDt,expirationDt,rowUpdateUserid) "
                    //     +"VALUES( "
                    //         +"'"+ req.query.appraisalYear +"', "
                    //         +"'"+ arrData[ctr].ownerId +"', "
                    //         +"'"+ exemptionTypeCd[i] +"', "
                    //         +"'"+ exemptionCd[i] +"', "
                    //         +"'"+ effectiveDt[i] +"', "
                    //         +"'"+ expirationDt[i] +"', "
                    //         +"'"+ arrData[ctr].rowUpdateUserid +"'"                            
                    //         +");"
                    // +"END "
                    //end  temp comment

                    // sql += ""
                        // +"UPDATE OwnerExemption SET rowDeleteFlag='' , "
                        //     +"effectiveDt= '"+ effectiveDt[i] +"', "
                        //     +"expirationDt='"+ expirationDt[i] +"' , "
                        //     +"rowUpdateDt = GETDATE(), "
                        //     +"exemptionCd='"+ exemptionCd[i] +"', "
                        //     +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                        // +"WHERE appraisalYear = '"+ req.query.appraisalYear +"' "
                        //     +"AND ownerId='"+ arrData[ctr].ownerId +"' "
                        //     +"AND exemptionTypeCd='"+ exemptionTypeCd[i] +"' "
                        // +"IF @@ROWCOUNT=0 "
                        // +"INSERT INTO OwnerExemption(appraisalYear, ownerId, exemptionTypeCd, exemptionCd,effectiveDt,expirationDt,rowUpdateUserid) "
                        // +"VALUES( "
                        //     +"'"+ req.query.appraisalYear +"', "
                        //     +"'"+ arrData[ctr].ownerId +"', "
                        //     +"'"+ exemptionTypeCd[i] +"', "
                        //     +"'"+ exemptionCd[i] +"', "
                        //     +"'"+ effectiveDt[i] +"', "
                        //     +"'"+ expirationDt[i] +"', "
                        //     +"'"+ arrData[ctr].rowUpdateUserid +"'"                            
                        //     +");"
                    //     +"";
                    };
                };
           }
            // console.log(sql)
		      UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"Invalid request body"});
		}
    }
}