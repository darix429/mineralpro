module.exports = {
    ReadManageOwnerNote: function (req, res) {
        //console.log("Called ReadManageOwnerNote")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var viewtype = 0

        if (req.query.ownerViewType==1) {
            viewtype = 2;
        }else if(req.query.ownerViewType==2) {
             viewtype = 1;
        }else{
            viewtype = 0;
        };

        var sql = ""
         if(req.query.ownerViewType && req.query.ownerNoteFilter){
            sql =   "SELECT TOP 10"
                 +"ont.idNote,  "
                +"ont.subjectId,  "
                +"ont.createDt,  "
                +"ont.referenceAppraisalYear,  "
                +"ont.note ownerNote,  "
                +"ont.seqNumber,  "
                +"ont.securityCd ownerSecurityDesc,  "
                +"ont.rowUpdateUserid,  "
                +"sc.description ,  "
                +"au.idOrganizationContact,  "
                +"concat(oc.firstName,' ' ,oc.lastName) as appraiserName  "
                +"FROM Note ont "
                +"INNER JOIN SystemCode sc ON ont.securityCd = sc.code AND sc.codeType = 530 AND sc.rowDeleteFlag = '' AND sc.appraisalYear = '"+req.query.appraisalYear+"'  "
                +"INNER JOIN AppUser au ON ont.rowUpdateUserid = au.idAppUser AND au.rowDeleteFlag = '' "
                +"INNER JOIN OrganizationContact oc ON au.idOrganizationContact = oc.idOrganizationContact AND oc.rowDeleteFlag ='' "
                +"WHERE ont.rowDeleteFlag = '' AND ont.subjectId = '"+req.query.ownerNoteFilter+"'  AND ont.securityCd != '"+viewtype+"' ORDER BY ont.seqNumber DESC "

            // sql = +"SELECT "
            //     +"ont.idNote,  "
            //     +"ont.subjectId,  "
            //     +"ont.createDt,  "
            //     +"ont.referenceAppraisalYear,  "
            //     +"ont.note ownerNote,  "
            //     +"ont.seqNumber,  "
            //     +"ont.securityCd ownerSecurityDesc,  "
            //     +"ont.rowUpdateUserid,  "
            //     +"sc.description ,  "
            //     +"au.idOrganizationContact,  "
            //     +"concat(oc.firstName,' ' ,oc.lastName) as appraiserName  "
            //     +"FROM Note ont  "
            //     +"INNER JOIN SystemCode sc ON ont.securityCd = sc.code AND sc.codeType = 530 AND sc.rowDeleteFlag = '' AND sc.appraisalYear = '"+req.query.appraisalYear+"'   "
            //     +"INNER JOIN AppUser au ON ont.rowUpdateUserid = au.idAppUser AND au.rowDeleteFlag = '' "
            //     +"INNER JOIN OrganizationContact oc ON au.idOrganizationContact = oc.idOrganizationContact AND oc.rowDeleteFlag ='' "
            //     +"WHERE ont.rowDeleteFlag = '' AND ont.subjectId = '"+req.query.ownerNoteFilter"+'  AND ont.securityCd != '"+viewtype"+' ORDER BY ont.seqNumber DESC "
            UtilFunctions.execSql(sql, res)
        }
    },//end ReadMaintainOwnerNote
};