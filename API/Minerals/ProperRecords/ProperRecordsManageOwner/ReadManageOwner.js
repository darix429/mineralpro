module.exports = {
    ReadManageOwner: function (req, res) {
        //console.log("Called ReadManageOwner")
        var UtilFunctions = require('../../../UtilFunctions.js');

        // var logger = UtilFunctions.winstonLogger();
        // logger.info('ReadManageOwner');

        var generateSql = function(whereClause){
            var needSql = 
                "UPDATE LastUpdateTime SET dataRefreshTime = getdate() "
                +"WHERE tableName = 'Owner' AND currentUserId = '"+req.query.idAppUser+"' "
                +" IF @@ROWCOUNT=0 "
                +" BEGIN "
                +" INSERT INTO LastUpdateTime (tableName, currentUserId) VALUES('Owner', '"+req.query.idAppUser+"')"
                +" END "
                +" IF OBJECT_ID (N'tempdb..#owner', N'U') IS NOT NULL "
                +" DROP TABLE #owner "
                // +"\nGO \n"
                +" IF OBJECT_ID (N'tempdb..#agency', N'U') IS NOT NULL "
                +" DROP TABLE #agency "
                // +"\nGO \n"
                +" IF OBJECT_ID (N'tempdb..#systemcode3190', N'U') IS NOT NULL "
                +" DROP TABLE #systemcode3190 "
                // +"\nGO \n"
                +" IF OBJECT_ID (N'tempdb..#systemcode3420', N'U') IS NOT NULL "
                +" DROP TABLE #systemcode3420 "
                // +"\nGO \n"
                +" IF OBJECT_ID (N'tempdb..#systemcode790', N'U') IS NOT NULL "
                +" DROP TABLE #systemcode790 "
                // +"\nGO \n"
                +" IF OBJECT_ID (N'tempdb..#systemcode810', N'U') IS NOT NULL "
                +" DROP TABLE #systemcode810 "
                // +"\nGO \n"
                +" SELECT * INTO #owner FROM owner WHERE appraisalYear = '"+req.query.appraisalYear+"' AND rowDeleteFlag = '' ; "
                +" SELECT agencyName,agencyCdx INTO #agency FROM Agency WHERE appraisalYear = '"+req.query.appraisalYear+"' AND rowDeleteFlag = ''; "
                +" SELECT code,description,codeType INTO #systemcode3190 FROM SystemCode WHERE appraisalYear = '"+req.query.appraisalYear+"' AND rowDeleteFlag = '' AND codeType = 3190; "
                +" SELECT code,description,codeType INTO #systemcode810 FROM SystemCode WHERE appraisalYear = '"+req.query.appraisalYear+"' AND rowDeleteFlag = '' AND codeType = 810; "
                +" SELECT code,description,codeType INTO #systemcode790 FROM SystemCode WHERE appraisalYear = '"+req.query.appraisalYear+"' AND rowDeleteFlag = '' AND codeType = 790; "
                +" SELECT code,description,codeType INTO #systemcode3420 FROM SystemCode WHERE appraisalYear = '"+req.query.appraisalYear+"' AND rowDeleteFlag = '' AND codeType = 3420; ";
            var sql = 
                " SELECT "
                    +" COUNT(op.ownerId) + COUNT(lo.ownerId) enableDeleteCount,"
                    +" ( SELECT reportId FROM Report where fileName = 'NoticeOfDetermination.RPT' and rowDeleteFlag = '' and appraisalYear = '"+req.query.appraisalYear+"' ) as reportId, "
                    +" o.ownerId, "
                    +" o.idOwner, "
                    +" o.name1 AS name, "
                    +" o.addrLine1 AS inCareOf, "
                    +" o.name2,  "
                    +" a.agencyName AS agencyName, "
                    // +"o.ownerTypeCdx,  "
                    // +"sc4.description as ownerType,  "
                    +" o.addrLine3 "
                   //  +"sc1.description as changeReason, "
                   //  +"(CASE WHEN o.confidentialInd = 'N' THEN 'No' ELSE 'Yes' END) as confidential "
                   // +"REPLACE(CONVERT(VARCHAR(10),o.birthDt,110),'-','/') as birthDt, "
                   // +"o.addrLine1, "
                   // +"o.addrLine2, "
                   // +"o.addrLine3, "
                   // +"o.city, "
                   // +"(CASE  WHEN sc3.description = 'UNASSIGNED' THEN '' ELSE sc3.description END) as stateName, "
                   // +"o.zipcode, "
                   // +"sc2.description as countryName, "
                   // +"o.phoneNum, "
                   // +"o.faxNum, "
                   // +"o.ownerChangeCd, "
                   // +"o.stateCd, "
                   // +"o.countryCd, "
                   // +"a.agencyCdx, "
                   // +"o.emailAddress, "
                   // +"count(lo.leaseId) as totalLeaseHolding, "
                   // +"sum(lvs.appraisedValue*lo.interestPercent) as totalLeaseHoldingValue, "
                   // +"sc5.description as disableVetDesc, "
                   // +"oe1.idOwnerExemption as disableIdOwnerExemption, "
                   // +"oe1.exemptionCd as disableVetCd , "
                   // +"REPLACE(CONVERT(VARCHAR(10),oe1.effectiveDt,110),'-','/') as disableVetEffectiveDt, "
                   // +"REPLACE(CONVERT(VARCHAR(10),oe1.expirationDt,110),'-','/')  as disableVetExpirationDt, "
                   // +"e.exemptionValue as amount, "
                   // +"sc6.description as totalExemptionDesc, "
                   // +"oe2.idOwnerExemption as totalIdOwnerExemption, "
                   // +"oe2.exemptionCd as totalExemptionCd , "
                   // +"REPLACE(CONVERT(VARCHAR(10),oe2.effectiveDt,110),'-','/') as totalExemptionEffectiveDt, "
                   // +"REPLACE(CONVERT(VARCHAR(10),oe2.expirationDt,110),'-','/') as totalExemptionExpirationDt  "
                +" FROM #owner o  "
                    +" LEFT JOIN #agency a ON o.agencyCdx = a.agencyCdx  "
                    +" LEFT JOIN #systemcode3190 sc1 ON o.ownerChangeCd = sc1.code  "
                    +" LEFT JOIN #systemcode810 sc2 ON o.countryCd = sc2.code  "
                    +" LEFT JOIN #systemcode790 sc3 ON o.stateCd = sc3.code  "
                    // +"LEFT JOIN #systemcode3420 sc4 ON (CASE WHEN o.ownerTypeCdx = 'R' THEN 0 ELSE o.ownerTypeCdx END)= sc4.code  "
                    +" LEFT JOIN LeaseOwner lo ON lo.ownerId = o.ownerId AND lo.appraisalYear = '"+req.query.appraisalYear+"' AND lo.rowDeleteFlag = '' "
                    +" LEFT JOIN Lease l ON lo.leaseId = l.leaseId AND l.appraisalYear = '"+req.query.appraisalYear+"' AND l.rowDeleteFlag = '' "
                    +" LEFT JOIN LeaseValueSummary lvs ON lo.leaseId = lvs.leaseId AND lvs.appraisalYear = '"+req.query.appraisalYear+"' AND lo.rowDeleteFlag = ''  "
                    +" LEFT JOIN SystemCode sc ON sc.code = lo.interestTypeCd AND sc.appraisalYear='"+req.query.appraisalYear+"' AND sc.rowDeleteFlag = '' AND sc.codeType=3000 "
                    +" LEFT JOIN Operator op ON op.ownerId = o.ownerId AND op.appraisalYear = '"+req.query.appraisalYear+"' AND op.rowDeleteFlag = ''  "
                    +" LEFT JOIN OwnerExemption oe1 ON o.ownerId = oe1.ownerId AND oe1.appraisalYear = '"+req.query.appraisalYear+"' AND oe1.rowDeleteFlag = ''  AND oe1.exemptionTypeCd = 3440 "
                    +" LEFT JOIN OwnerExemption oe2 ON o.ownerId = oe2.ownerId AND oe2.appraisalYear = '"+req.query.appraisalYear+"' AND oe2.rowDeleteFlag = ''  AND oe2.exemptionTypeCd = 3430  "
                    +" LEFT JOIN SystemCode sc5 ON oe1.exemptionCd = sc5.code AND sc5.codeType = 3440 and sc5.appraisalYear = '"+req.query.appraisalYear+"' "
                    +" LEFT JOIN SystemCode sc6 ON oe2.exemptionCd = sc6.code AND sc6.codeType = 3430 and sc6.appraisalYear = '"+req.query.appraisalYear+"' "
                    +" LEFT JOIN Exemption e ON oe1.exemptionCd = e.exemptionCd AND e.exemptionTypeCd = 3 AND e.appraisalYear = '"+req.query.appraisalYear+"' AND e.rowDeleteFlag='' "
                +" GROUP BY o.idOwner, o.ownerId, o.name1, o.name2, a.agencyName, sc1.description, o.addrLine1, o.addrLine3, "
                +" (CASE WHEN o.confidentialInd = 'N' THEN 'No' ELSE 'Yes' END) "
                // +"REPLACE(CONVERT(VARCHAR(10),o.birthDt,110),'-','/'), "
                // +"o.addrLine1, "    
                // +"o.addrLine2, "
                // +"o.addrLine3, "
                // +"o.city, "
                // +"(CASE  WHEN sc3.description = 'UNASSIGNED' THEN '' ELSE sc3.description END), "

                // +"o.zipcode, "
                // +"sc2.description, "
                // +"o.phoneNum, "
                // +"o.faxNum, "
                // +"o.ownerChangeCd, "
                // +"o.stateCd, "
                // +"o.countryCd, "
                // +"a.agencyCdx, "
                // +"o.emailAddress, "
                // +"sc5.description, "
                // +"oe1.effectiveDt, "
                // +"oe1.expirationDt, "
                // +"e.exemptionValue, "
                // +"sc6.description, "
                // +"oe2.effectiveDt , "
                // +"oe1.exemptionCd , "
                // +"oe2.exemptionCd, "
                // +"oe2.expirationDt, "
                // +"oe1.idOwnerExemption, "
                // +"oe2.idOwnerExemption  "
                +" ";
            var orderSql = "ORDER BY idOwner DESC;";
            if(whereClause){
                sql = " SELECT tbl.* FROM ( "+sql+" )tbl "+whereClause+" ";
            }
            /*------------------------------ START ------------------------------
            | Enclose genSql inside a try catch incase the filter values exceed the maximum value for the field it's trying to compare value from like int
            */
            sql = " "
                    +" BEGIN TRY "
                    +       sql + orderSql
                    +" END TRY "
                    +" BEGIN CATCH "
                    // +"     SELECT  ERROR_NUMBER() AS ErrorNumber   "
                    // +"       ,ERROR_SEVERITY() AS ErrorSeverity   "
                    // +"       ,ERROR_STATE() AS ErrorState   "
                    // +"       ,ERROR_PROCEDURE() AS ErrorProcedure   "
                    // +"       ,ERROR_LINE() AS ErrorLine   "
                    // +"       ,ERROR_MESSAGE() AS ErrorMessage;   "
                    +" END CATCH; "
                
            sql = needSql + sql;
            return sql;
        };

        var filters = req.query.filter ? JSON.parse(req.query.filter) || [] : [];
        var filterBy = req.query.filterBy || 'start';
        var sql = '';
        var whereClause = '';
        if(filters.length>0){
            whereClause = 'WHERE ';
            filters.forEach(function(filter){
                whereClause = whereClause=='WHERE ' ? whereClause : whereClause+' AND ';
                var value = filter.value || '';
                value = value.toString();
                value = value.toUpperCase();
                if(filterBy == 'start'){
                    if(typeof value === 'number'){
                        whereClause += 'tbl.'+filter.property+" >= "+value;
                    }
                    else if(typeof value === 'string'){
                        whereClause += 'tbl.'+filter.property+" LIKE '"+value+"%'";
                    }
                }
                else if(filterBy == 'exact'){
                    value = typeof value == 'string' ? "'"+value+"'" : value;
                    whereClause += 'tbl.'+filter.property+" = "+value;
                }
                else if(filterBy == 'contain'){
                    whereClause += 'tbl.'+filter.property+" LIKE '%"+value+"%'";
                }
            });
        }
        if(whereClause){
            sql = generateSql(whereClause);
        }
        else{
            sql = generateSql();
        }
        // logger.debug(sql);

        if(req.query.dataCount > 0){
            var sqlCond = "SELECT * FROM LastUpdateTime "
            +"WHERE lastUpdateTime > dataRefreshTime "
            +"AND tableName = 'Owner' "
            +"AND currentUserId = '"+req.query.idAppUser+"'";

            UtilFunctions.execSqlCallBack(sqlCond, res, function (rec) {
                if(rec.length == 0 && !whereClause){
                    res.status(200).json({"success":false,"data":"Do not refresh the data"});
                }
            });
        }
        if(sql){
            UtilFunctions.execSql(sql, res);
        }
        else{
            res.status(500).json({'success':false,'error':'Failed to generate Sql Query'});
        }       
    },//end ReadMaintainOwner
};
