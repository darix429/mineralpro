module.exports = {
    UpdateManageOwnerNote: function (req, res) {
        //console.log("Called UpdateManageOwnerNote")
        var UtilFunctions = require('../../../UtilFunctions.js');
        if(req.body && req.query.updateLength){
			var arrData = req.body;
			if(req.query.updateLength == 1){arrData = [req.body];}
			//console.log(arrData);
			
			var sql = "";     
			for (var ctr = 0; ctr < arrData.length; ctr++) {
	                sql += "UPDATE Note SET "
	                +"note = UPPER('"+arrData[ctr].ownerNote+"'),"
	                +"securityCd = '"+arrData[ctr].ownerSecurityDesc+"', "
	                +" rowUpdateUserid = '"+arrData[ctr].rowUpdateUserid+"', "
					+" rowDeleteFlag = '"+arrData[ctr].rowDeleteFlag+"', "
					 +"rowUpdateDt = GETDATE() "
	                +"WHERE "
	                +"subjectId = '"+arrData[ctr].ownerId+"' "
	                +"AND idNote = '"+arrData[ctr].idNote+"'"
            	}
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"Invalid request body"});
		}
    }
}
