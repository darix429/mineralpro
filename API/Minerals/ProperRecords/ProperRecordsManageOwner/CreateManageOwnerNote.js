module.exports = {
    CreateManageOwnerNote: function (req, res) {
        //console.log("Called CreateManageOwnerNote")
        var UtilFunctions = require('../../../UtilFunctions.js');
        if(req.body){
			var arrData = req.body;
			if(req.query.createLength == 1){arrData = [req.body];}
			//console.log(arrData);
			var sql = "";
            for(var ctr=0; ctr<arrData.length; ctr++){
                    sql += "INSERT INTO Note ( "
					+"subjectId, "
					+"seqNumber, "
					+"createDt, "
					+"noteTypeCd, "
					+"quickNoteCd, "
					+"securityCd, "
					+"groupCd, "
					+"referenceAppraisalYear, "
					+"note, "
					+"rowUpdateUserid, "
					+"rowDeleteFlag) "
					+"VALUES ("
					+"'"+arrData[ctr].ownerId+"', "
					+"(SELECT max(seqNumber)+1 FROM Note), "
					+"SYSDATETIME(), "
					+"'0', "
					+"'0', "
					+"'"+arrData[ctr].ownerSecurityDesc+"', "
					+"'0', "
					+"'"+req.query.appraisalYear+"', "
					+"UPPER('"+arrData[ctr].ownerNote+"'), "
					+"'"+arrData[ctr].rowUpdateUserid+"', "
					+"'"+arrData[ctr].rowDeleteFlag+"');"
                }
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"No data received"});
		}
    }
}