module.exports = {
    CreateManageOperator: function (req, res) {
        //console.log("Called CreateManageOperator")
        var UtilFunctions = require('../../../UtilFunctions.js');

         if(req.body){
			var arrData = req.body;
			if(req.query.createLength == 1){arrData = [req.body];}
			//console.log(arrData);

			var sql = "DECLARE @operatorId int = 0, @appraisalYear smallint, @year smallint; ";
            sql += " IF OBJECT_ID(N'tempdb..#newOperatorYears',N'U') IS NOT NULL "
                + "  BEGIN "
                + "  DROP TABLE #newOperatorYears "
                + "  END "
                + " CREATE TABLE #newOperatorYears"
                + " ( "
                + "   year smallint "
                + " ) "
				
			var current_year = (new Date()).getFullYear();
			var appraisal_year = req.query.appraisalYear;
			var years = [];
			var last_prior = (appraisal_year <= (current_year-6))? appraisal_year : (current_year-6);
			for(year = last_prior; year<=current_year; year++){
				years.push(parseInt(year));
			}
			for(var i=0; i<years.length; i++){
				sql+="INSERT INTO #newOperatorYears VALUES ('"+years[i]+"'); "
			}
			
            for(var ctr=0; ctr<arrData.length; ctr++){
				
				sql += ""
					+"WHILE (select count(*) from #newOperatorYears) > 0 "
					+"BEGIN "
					+"SET @year = (select top 1 year from #newOperatorYears) "
					
				sql += ""
					+ "SELECT @operatorId = operatorId from Operator where operatorId = '"+arrData[ctr].operatorId+"' AND appraisalYear= @year "
                        +"IF @operatorId = 0 BEGIN "
                        +"INSERT INTO Operator ( "
                        +" appraisalYear, "
                        +" operatorId, "
                        +" operatorName, "
                        +" ownerId, "
                        +" rowDeleteFlag , "
                        +" rowUpdateUserid "
                        +" )VALUES( "
                        +" @year, "
                        +" '"+arrData[ctr].operatorId+"', "
                        +" UPPER('"+arrData[ctr].operatorName+"'), "
                        +" '"+arrData[ctr].ownerId+"', "
                        +" '"+arrData[ctr].rowDeleteFlag+"', "
                        +" '"+arrData[ctr].rowUpdateUserid+"' "
                        +" ) END "
                        +"ElSE BEGIN "
                        +"UPDATE Operator SET "
                        +" operatorId = @operatorId, "
                        +" operatorName = UPPER('" + arrData[ctr].operatorName + "'), "
                        +" ownerId = '" + arrData[ctr].ownerId + "',"
                        +" rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "', "
                        +"rowUpdateDt = GETDATE(), "
                        +"rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                        +"WHERE operatorId = @operatorId AND appraisalYear= @year "

                        // +"UPDATE Owner SET "
                        // +"rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "', "
                        // +"rowUpdateDt = GETDATE(), "
                        // +"rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                        // +"WHERE ownerId = '" + arrData[ctr].ownerId + "' ";
				sql += ""
                        +"END ; SET @operatorId=0 "
						
					sql+="DELETE FROM #newOperatorYears WHERE year = @year "
						+"END ";
                }
                //console.log(sql)
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"No data received"});
		}
    },//end CreateMaintainOperator
    
};
