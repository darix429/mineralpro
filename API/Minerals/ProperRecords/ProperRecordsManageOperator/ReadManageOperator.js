module.exports = {
    ReadManageOperator: function (req, res) {
        //console.log("Called ReadManageOperator")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = "UPDATE LastUpdateTime SET dataRefreshTime = getdate() "
                +"WHERE tableName = 'Operator' AND currentUserId = '"+req.query.idAppUser+"' "
                +"IF @@ROWCOUNT=0 "
                +"BEGIN "
                +"INSERT INTO LastUpdateTime (tableName, currentUserId) VALUES('Operator', '"+req.query.idAppUser+"')"
                +"END "
                +"SELECT "
                       +"COUNT(l.operatorId) + COUNT(u.operatorId) enableDeleteCount,"
                       +" o.idOperator, "
                       +" o.appraisalYear, "
                       +" o.operatorId, "
                       +" o.operatorName, "
                       +" o.ownerId, "
                       +" ow.name1+'-'+CAST(o.ownerId as varchar(15)) ownerName1, "
                       +" ow.agencyCdx, "
                       //+" a.agencyName, "
                       +" o.rowDeleteFlag "
                +" FROM Operator o "
                +" INNER JOIN Owner ow ON o.ownerId = ow.ownerId AND ow.rowDeleteFlag = '' AND ow.appraisalYear = '"+ req.query.appraisalYear + "'"
                //+" LEFT JOIN  Agency a ON ow.agencyCdx  = a.agencyCdx AND a.rowDeleteFlag = '' AND a.appraisalYear = '"+ req.query.appraisalYear + "'"
                +" LEFT JOIN Lease l on o.operatorId = l.operatorId AND l.appraisalYear = o.appraisalYear AND l.rowDeleteFlag = ''"
                +" LEFT JOIN Unit u on o.operatorId = u.operatorId AND u.appraisalYear = o.appraisalYear AND u.rowDeleteFlag = ''"
                +" WHERE o.rowDeleteFlag = '' AND o.appraisalYear = '"+ req.query.appraisalYear + "'"
                +" GROUP BY o.idOperator, "
                       +" o.appraisalYear, "
                       +" o.operatorId, "
                       +" o.operatorName, "
                       +" o.ownerId, "
                       +" ow.name1+'-'+CAST(o.ownerId as varchar(15)), "
                       +" ow.agencyCdx, "
                       +" o.rowDeleteFlag "
                +" ORDER BY o.idOperator desc;";
        
        if(req.query.dataCount == 0){      
            UtilFunctions.execSql(sql, res)
        }else{
            var sqlCond = "SELECT * FROM LastUpdateTime "
                +"WHERE lastUpdateTime > dataRefreshTime "
                +"AND tableName = 'Operator' "
                +"AND currentUserId = '"+req.query.idAppUser+"'";

            UtilFunctions.execSqlCallBack(sqlCond, res, function (rec) {
                if(rec.length > 0){
                    UtilFunctions.execSql(sql, res)
                }else{
                    res.status(200).json({"success":false,"data":"Do not refresh the data"});
                }
            });                     
        }
    },//end ReadMaintainOperator
};
