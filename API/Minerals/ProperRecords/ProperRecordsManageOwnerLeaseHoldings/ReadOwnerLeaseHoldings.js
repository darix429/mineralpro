
module.exports = {
    Read: function (req, res) {
        var UtilFunctions = require('../../../UtilFunctions.js');
        //console.log("Called ReadLeaseTaxUnit");		
		var appraisalYear = req.query.appraisalYear;
	    var ownerId = req.query.selectedId;
		//var ownerId = 708280;
		
        var sql = ""
				+"DECLARE  "
					+"@appraisalYear smallint = "+appraisalYear+", "
					+"@ownerId int = "+ownerId+" "
				+"SELECT  "
					+"lo.ownerId,"
					+"lo.idLeaseOwner,"
					+"o.name1 ownerName,"
					+"lo.leaseId,"
					+"l.operatorId,"
					+"CAST((select sum(interestPercent) from LeaseOwner where appraisalYear = "+appraisalYear+" and leaseId =  lo.leaseId and rowDeleteFlag = '') as varchar(15)) as totalInterestPct, "
					//+"op.operatorName," 
                                
                                        +" CASE WHEN l.UnitId = 0 THEN (SELECT operatorName FROM Operator WHERE operatorId = l.operatorId AND appraisalYear = "+ req.query.appraisalYear +") ELSE (SELECT operatorName FROM Operator WHERE operatorId = u.operatorId AND appraisalYear = "+ req.query.appraisalYear +") END as 'operatorName',"
                                
                                        
					+"l.leaseName,"
					+"CAST(lo.interestPercent as varchar(15)) interestPercent,"
					+"lo.interestTypeCd,"
					+"sc.description as interestTypeDesc, "
					+"lvs.appraisedValue, "
					//+"(lo.interestPercent * lvs.appraisedValue) as ownerValue "
                                        +"CASE WHEN lo.interestTypeCd = 4 "
                                        +"THEN "
                                            //+"CASE WHEN (lvs.workingInterestValue/lvs.workingInterestPercent *lo.interestPercent) > 0 "
									        +"CASE WHEN (lvs.workingInterestPercent *lo.interestPercent) > 0 "
                                            +"THEN "
                                            +"dbo.udfBankRound ((lvs.workingInterestValue/lvs.workingInterestPercent *lo.interestPercent),0) "
                                            // +"WHEN (lvs.workingInterestValue/lvs.workingInterestPercent *lo.interestPercent) > 0 "
                                            // +"AND  (lvs.workingInterestValue/lvs.workingInterestPercent *lo.interestPercent) < 1 "
                                            // +"THEN 1 "
                                            // +"ELSE 0 "
                                            +"END "
                                        +"ELSE "
                                            //+"CASE WHEN (lvs.royaltyInterestValue/(lvs.royaltyInterestPercent + lvs.oilPaymentPercent + lvs.overridingRoyaltyPercent) *lo.interestPercent) > 0 "
									        +"CASE WHEN ((lvs.royaltyInterestPercent + lvs.oilPaymentPercent + lvs.overridingRoyaltyPercent) *lo.interestPercent) > 0 "
									        +"THEN "
                                            +"dbo.udfBankRound ((lvs.royaltyInterestValue/(lvs.royaltyInterestPercent + lvs.oilPaymentPercent + lvs.overridingRoyaltyPercent) *lo.interestPercent),0) "
                                            // +"WHEN  (lvs.royaltyInterestValue/(lvs.royaltyInterestPercent + lvs.oilPaymentPercent + lvs.overridingRoyaltyPercent) *lo.interestPercent) > 0 "
                                            // +"AND  (lvs.royaltyInterestValue/(lvs.royaltyInterestPercent + lvs.oilPaymentPercent + lvs.overridingRoyaltyPercent) *lo.interestPercent) < 1 "
                                            // +"THEN 1 "
                                            // +"ELSE 0 "
                                            +"END "
                                        +"END as ownerValue "                                                                
                                        
				+"FROM LeaseOwner lo "
                                +"INNER JOIN Lease l ON lo.leaseId = l.leaseId AND l.appraisalYear = @appraisalYear AND l.rowDeleteFlag = '' "
                                +"INNER JOIN Owner o ON o.ownerId = lo.ownerId AND o.appraisalYear = @appraisalYear AND o.rowDeleteFlag = '' "
                                +"INNER JOIN LeaseValueSummary lvs ON lo.leaseId = lvs.leaseId AND lvs.appraisalYear = @appraisalYear AND lo.rowDeleteFlag = ''  "
                                +"INNER JOIN SystemCode sc ON sc.code = lo.interestTypeCd AND sc.appraisalYear=@appraisalYear AND sc.rowDeleteFlag = '' AND sc.codeType=3000 "
                                +"LEFT JOIN Unit u on l.unitId = u.unitId and u.rowDeleteFlag = '' AND u.appraisalYear = @appraisalYear "
                                //+"LEFT JOIN Operator op ON l.operatorId = op.operatorId AND op.appraisalYear=@appraisalYear AND op.rowDeleteFlag = ''  "
                                
				+"WHERE lo.appraisalYear = @appraisalYear AND lo.ownerId = @ownerId AND NOT (lo.rowDeleteFlag = 'D') "
				+"ORDER BY l.leaseName ASC; "
        UtilFunctions.execSql(sql, res);
    },
};

