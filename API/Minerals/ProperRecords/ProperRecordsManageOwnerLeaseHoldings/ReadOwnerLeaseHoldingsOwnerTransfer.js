module.exports = {
    Read: function (req, res) {
        //console.log("Called ReadOwnerLeaseHoldingsOwnerTransfer")
        var UtilFunctions = require('../../../UtilFunctions.js');        
        var sql = "SELECT "
        		+"concat(name1,' ',name2,' ',name3) as ownerName, "
                +"concat(addrLine1,' ',addrLine2,' ',addrLine3) as ownerAddress, "
        		+"ownerId "
        		+"FROM Owner "
        		+"WHERE rowDeleteFlag = '' "
        		+"AND appraisalYear = '"+req.query.appraisalYear+"' "
                +"ORDER BY name1;"
        UtilFunctions.execSql(sql, res)
    },//end Read
};
