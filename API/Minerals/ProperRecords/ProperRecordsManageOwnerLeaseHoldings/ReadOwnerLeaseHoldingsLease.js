
module.exports = {
    Read: function (req, res) {
        var UtilFunctions = require('../../../UtilFunctions.js');
		var appraisalYear = req.query.appraisalYear;
		
        var sql = ""
				+"SELECT "
				+"l.idLease, "
				+"l.leaseId, "
				+"l.leaseName, "
				+"lvs.appraisedValue, "
				+"CAST((select sum(interestPercent) from LeaseOwner where appraisalYear = '"+appraisalYear+"' and leaseId =  l.leaseId and rowDeleteFlag = '') as varchar(15)) as totalInterestPct, "
				+"op.operatorName "
				+"FROM "
				+"Lease l "
				+"LEFT JOIN Operator op ON l.operatorId = op.operatorId AND op.appraisalYear='"+appraisalYear+"' AND op.rowDeleteFlag = ''  "
				+"INNER JOIN LeaseValueSummary lvs ON l.leaseId = lvs.leaseId AND lvs.appraisalYear = '"+appraisalYear+"' AND lvs.rowDeleteFlag = ''  "				
				+"WHERE "
				+"l.appraisalYear = '"+appraisalYear+"' and l.rowDeleteFlag = '' "
				+"ORDER BY l.leaseName ASC ";
        UtilFunctions.execSql(sql, res);
    },
};

