module.exports = {
    ReadAgency: function (req, res) {
        //console.log("Called ReadAgency")
        var UtilFunctions = require('../../UtilFunctions.js');        
        var sql = "SELECT DISTINCT "
                       +" agencyCdx, "
                       +" agencyName "
                +" FROM Agency WHERE statusCd = '0' AND rowDeleteFlag = '' AND appraisalYear = '"+req.query.appraisalYear+"' ORDER BY agencyName ";
        UtilFunctions.execSql(sql, res)
    },//end ReadAgency
};