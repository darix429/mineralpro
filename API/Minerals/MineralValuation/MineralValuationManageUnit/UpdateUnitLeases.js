module.exports = {
    UpdateUnitLeases: function (req, res) {
        //console.log("Called UpdateUnitLeases")
        var UtilFunctions = require('../../../UtilFunctions.js');
        if (req.body && req.query.updateLength) {
            var arrData = req.body;
            if (req.query.updateLength == 1) {
                arrData = [req.body];
            }
            var sql ="DECLARE @maxYearLife int = 25"
                    +"SELECT @maxYearLife = max([maxYearLife]) FROM Appraisal WHERE subjectId = "+arrData[0].unitId+" AND not rowDeleteFlag='D' AND appraisalYear = '"+req.query.appraisalYear+"' ";
            for (var ctr = 0; ctr < arrData.length; ctr++) {
                //console.log("Asdasdasdsa")
                sql += ""
                if(arrData[ctr].rowDeleteFlag == 'D'){
                    sql +=" UPDATE lease SET "
                        + " unitId = '0', "
                        + " rowUpdateDt = GETDATE(), "
                        + " rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                        + " WHERE leaseId = '"+arrData[ctr].leaseId+"';"    

                }else{
                    sql +=" UPDATE leaseValueSummary SET "
                        + " tractNum = '" + arrData[ctr].tractNum + "', "
                        + " unitWorkingInterestPercent = '" + arrData[ctr].unitWorkingInterestPercent + "', "
                        + " unitRoyaltyInterestPercent = '" + arrData[ctr].unitRoyaltyInterestPercent + "', "
                        + " workingInterestValue = '" + arrData[ctr].workingInterestValue + "', "
                        + " royaltyInterestValue = '" + arrData[ctr].royaltyInterestValue + "', "
                        // + "appraisedValue = '" + parseFloat(arrData[ctr].royaltyInterestValue) + parseFloat(arrData[ctr].workingInterestValue) + "', "
                        + " rowUpdateDt = GETDATE(), "
                        + " rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                        + " WHERE leaseId = '" + arrData[ctr].leaseId + "'" 
                        + " AND appraisalYear  = '" + req.query.appraisalYear + "'"
                }               


                sql +=" EXECUTE spCalcUnitValueSummaryViaLeaseValueSummary '"+arrData[ctr].unitId+"', '"+req.query.appraisalYear+"', '" + arrData[ctr].rowUpdateUserid + "' "
                    + " UPDATE UnitValueSummary SET "
                    + " weightedWorkingInterestPercent = b.WgtWI ,"
                    + " weightedRoyaltyInterestPercent = b.WgtRI "
                    + " FROM UnitValueSummary uvs "
                    + " INNER JOIN ("
                    + "   SELECT "
                    + "     CASE WHEN d1 <> 0 AND d3 <> 0 THEN dbo.udfBankRound(d1/d3, 6) ELSE 0 END WgtWI,"
                    + "     CASE WHEN d2 <> 0 AND d4 <> 0 THEN dbo.udfBankRound(d2/d4, 6) ELSE 0 END WgtRI "
                    + "   FROM ("
                    + "     SELECT SUM(LVS.workingInterestPercent * LVS.unitWorkingInterestPercent) d1 "
                    + "       , SUM(LVS.unitRoyaltyInterestPercent * (LVS.royaltyInterestPercent +LVS.overridingRoyaltyPercent +LVS.oilPaymentPercent)) d2 "
                    + "       , SUM(LVS.unitWorkingInterestPercent) d3 "
                    + "       , SUM(LVS.unitRoyaltyInterestPercent) d4 "
                    + "     FROM Lease LSE "
                    + "     INNER JOIN LeaseValueSummary LVS "
                    + "       ON LVS.appraisalYear = LSE.appraisalYear "
                    + "       AND LVS.leaseId = LSE.leaseId "
                    + "       AND LVS.rowDeleteFlag = ''"
                    + "     WHERE LSE.appraisalYear = '"+req.query.appraisalYear+"' "
                    + "       AND LSE.unitId = '"+arrData[ctr].unitId+"' "
                    + "       AND LSE.unitId = '"+arrData[ctr].unitId+"' "
                    + "       AND LSE.rowDeleteFlag = ''"
                    + "   )a"
                    + " )b "
                    + "   ON 1 = 1 "
                    + " WHERE uvs.appraisalYear = '"+req.query.appraisalYear+"' "
                    + " AND uvs.unitId ='"+arrData[ctr].unitId+"' "
                    + " AND uvs.rowDeleteFlag = '' ";


                sql += " EXECUTE spCalcAmortizationYears  "+arrData[ctr].rowUpdateUserid+","+req.query.appraisalYear+", @maxYearLife, 1, 1, "+arrData[ctr].unitId+", 0 ";

                //                       sql +="UPDATE unitValueSummary SET "
                //                            +"royaltyInterestPercent = (SELECT SUM(unitRoyaltyInterestPercent) FROM leaseValueSummary WHERE leaseId IN(SELECT leaseId FROM Lease WHERE unitId = '"+arrData[ctr].unitId+"' AND appraisalYear = '"+req.query.appraisalYear+"' AND rowDeleteFlag = '')  AND appraisalYear = '"+req.query.appraisalYear+"'  AND rowDeleteFlag = ''), "
                //                            +"workingInterestpercent = (SELECT SUM(unitWorkingInterestPercent) FROM leaseValueSummary WHERE leaseId IN(SELECT leaseId FROM Lease WHERE unitId = '"+arrData[ctr].unitId+"' AND appraisalYear = '"+req.query.appraisalYear+"' AND rowDeleteFlag = '')  AND appraisalYear = '"+req.query.appraisalYear+"'  AND rowDeleteFlag = ''), "
                //                            +"weightedWorkingInterestpercent = (SELECT SUM(workingInterestPercent * unitWorkingInterestPercent ) FROM leaseValueSummary WHERE leaseId IN(SELECT leaseId FROM Lease WHERE unitId = '"+arrData[ctr].unitId+"' AND appraisalYear = '"+req.query.appraisalYear+"' AND rowDeleteFlag = '')  AND appraisalYear = '"+req.query.appraisalYear+"'  AND rowDeleteFlag = ''), "
                //                            +"weightedRoyaltyInterestPercent = (SELECT SUM(royaltyInterestPercent * unitRoyaltyInterestPercent) FROM leaseValueSummary WHERE leaseId IN(SELECT leaseId FROM Lease WHERE unitId = '"+arrData[ctr].unitId+"' AND appraisalYear = '"+req.query.appraisalYear+"' AND rowDeleteFlag = '')  AND appraisalYear = '"+req.query.appraisalYear+"'  AND rowDeleteFlag = ''), "
                //                            +"rowUpdateDt = GETDATE(), "
                //                            +"rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                //                        +"WHERE unitId = '"+arrData[ctr].unitId+"' ;"
            }
            //console.log("sql:",sql,arrData.length,arrData);
            UtilFunctions.execSql(sql, res);
        } else {
            res.status(400).json({"success": false, "data": "Invalid request body"});
        }
    }//end UpdateUnitLeases
};
