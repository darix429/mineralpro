module.exports = {
    ReadUnitHeaderDetail: function (req, res) {
        //console.log("Called ReadUnits")
        var UtilFunctions = require('../../../UtilFunctions.js');
        
        var sql = '';
        if(req.query.appraisalYear && req.query.selectedId){
            sql = "DECLARE @appraisalYear int = "+req.query.appraisalYear+", "
                         +"@unitId int = "+req.query.selectedId+" "
                +"SELECT u.unitId, "
                    +"u.unitName, "
                    +"u.operatorId, "
                    +"count(l.leaseId) numberofleases, "
                    +"CONCAT(u.operatorId,' - ',o.operatorName) operatorName, "
                    +"CAST(uvs.royaltyInterestPercent as varchar(15)) totalRoyaltyInterest, "
                    +"CAST(uvs.workingInterestPercent as varchar(15)) totalWorkingInterest, "
                    +"CAST(uvs.weightedRoyaltyInterestPercent as varchar(15)) weightedRoyaltyInterest, "
                    +"CAST(uvs.weightedWorkingInterestPercent as varchar(15)) weightedWorkingInterest, "
                    +"uvs.royaltyInterestValue, " 
                    +"uvs.workingInterestValue, "
                    +"uvs.royaltyInterestValue + uvs.workingInterestValue totalUnitValue "
                +"FROM Unit u "
                +"INNER JOIN Operator o ON u.operatorId = o.operatorId "
                    +"AND o.appraisalYear = u.appraisalYear AND o.rowDeleteFlag = '' "
                +"INNER JOIN UnitValueSummary uvs ON u.unitId = uvs.unitId "
                     +"AND uvs.appraisalYear = u.appraisalYear AND uvs.rowDeleteFlag = '' "
                +"LEFT JOIN lease l on l.unitId = u.unitId " 
                    +"AND l.appraisalYear = u.appraisalYear AND l.rowDeleteFlag = '' "        
                +"WHERE u.appraisalYear = @appraisalYear "
                +"AND u.unitId = @unitId "
                +"AND u.rowDeleteFlag = '' "
                +"GROUP BY "
                    +"u.unitId, u.unitName, u.operatorId, o.operatorName, uvs.royaltyInterestValue, uvs.workingInterestValue, uvs.royaltyInterestPercent, uvs.workingInterestPercent, uvs.weightedRoyaltyInterestPercent, uvs.weightedWorkingInterestPercent "
                +"ORDER BY u.unitId DESC"        
        }
                         
        UtilFunctions.execSql(sql, res)
    },//end ReadUnits
};
