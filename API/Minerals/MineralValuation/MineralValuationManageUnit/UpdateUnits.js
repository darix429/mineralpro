module.exports = {
    UpdateUnits: function (req, res) {
        //console.log("Called UpdateUnits")
        var UtilFunctions = require('../../../UtilFunctions.js');
        if (req.body && req.query.updateLength) {
            var arrData = req.body;
            if (req.query.updateLength == 1) {
                arrData = [req.body];
            }

            var sql = "";
            for (var ctr = 0; ctr < arrData.length; ctr++) {
                sql += "UPDATE Unit SET "
                        + "unitName = UPPER('" + arrData[ctr].unitName + "'), "
                        + "operatorId = '" + arrData[ctr].operatorId + "', "
                        + "rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "', "
                        +"rowUpdateDt = GETDATE(), "
                        + "rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                        + "WHERE unitId = '" + arrData[ctr].unitId + "';"
                
                        +"UPDATE UnitValueSummary SET "
                        + "rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "', "
                        +"rowUpdateDt = GETDATE(), "
                        + "rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                        + "WHERE unitId = '" + arrData[ctr].unitId + "';";
            }
            //console.log(sql);
            UtilFunctions.execSql(sql, res);
        } else {
            res.status(400).json({"success": false, "data": "Invalid request body"});
        }
    }//end UpdateLease
};