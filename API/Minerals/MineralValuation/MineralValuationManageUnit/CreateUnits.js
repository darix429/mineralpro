module.exports = {
    CreateUnits: function (req, res) {
        //console.log("Called CreateUnits")
        var UtilFunctions = require('../../../UtilFunctions.js');

        if (req.body) {
            var arrData = req.body;
            if (req.query.createLength == 1) {
                arrData = [req.body];
            }
            //console.log(arrData);
            var sql = "DECLARE @unitId int "
                    + " If(OBJECT_ID(N'tempdb..#tempNewlyInsertedUnits',N'U') Is Not Null) "
                    + " Begin "
                    + "     Drop Table #tempNewlyInsertedUnits "
                    + " End "
                    + " CREATE TABLE #tempNewlyInsertedUnits (unitId INT); "
                    + " DECLARE @appraisalYear VARCHAR(4) = '"+req.query.appraisalYear+"' ";
            for (var ctr = 0; ctr < arrData.length; ctr++) {
                sql += "SELECT @unitId = max(unitId)+1 FROM Unit "
                        +" INSERT INTO #tempNewlyInsertedUnits VALUES(@unitId); "
                        + "INSERT INTO Unit ( "
                        + " appraisalYear, "
                        + " unitId, "
                        + " unitName, "
                        + " operatorId, "
                        + " rowDeleteFlag, "
                        + " rowUpdateUserid "                
                        + " )VALUES( "
                        + " @appraisalYear, "
                        + " @unitId, "
                        + " UPPER('" + arrData[ctr].unitName + "'), "
                        + " '" + arrData[ctr].operatorId + "', "
                        + " '" + arrData[ctr].rowDeleteFlag + "', "
                        + " '" + arrData[ctr].rowUpdateUserid + "' "
                        + " );"

                        +"INSERT INTO UnitValueSummary ( "
                        + " appraisalYear, "
                        + " unitId, "
                        + " rowDeleteFlag, "
                        + " rowUpdateUserid "
                        + " )VALUES( "
                        + " @appraisalYear, "
                        + " @unitId, "
                        + " '" + arrData[ctr].rowDeleteFlag + "', "
                        + " '" + arrData[ctr].rowUpdateUserid + "' "
                        + " );";
            }
            sql += ""
                +" SELECT "
                +" COUNT(l.unitId) + CASE WHEN COUNT(a.subjectId) = 0  THEN 0 ELSE CASE WHEN (uvs.royaltyInterestValue + uvs.workingInterestValue) = 0 THEN 0 ELSE 1 END END enableDeleteCount, "
                +" u.unitId, "
                +" u.unitName, "
                +" u.operatorId, "
                +" o.operatorName, "
                +" CAST(uvs.royaltyInterestPercent as varchar(15)) totalRoyaltyInterest, "
                +" CAST(uvs.workingInterestPercent as varchar(15)) totalWorkingInterest, "
                +" CAST(uvs.weightedRoyaltyInterestPercent as varchar(15)) weightedRoyaltyInterest, "
                +" CAST(uvs.weightedWorkingInterestPercent as varchar(15)) weightedWorkingInterest, "
                +" uvs.royaltyInterestValue, " 
                +" uvs.workingInterestValue, "
                +" uvs.royaltyInterestValue + uvs.workingInterestValue totalUnitValue "
                +" FROM Unit u "
                + "   INNER JOIN #tempNewlyInsertedUnits nu"
                + "         ON u.unitId = nu.unitId "
                +"      INNER JOIN Operator o ON u.operatorId = o.operatorId "
                +"          AND o.appraisalYear = u.appraisalYear AND o.rowDeleteFlag = '' "
                +"      INNER JOIN UnitValueSummary uvs ON u.unitId = uvs.unitId "
                +"          AND uvs.appraisalYear = u.appraisalYear AND uvs.rowDeleteFlag = '' "
                +"      LEFT JOIN Lease l ON l.unitId = u.unitId AND l.appraisalYear = @appraisalYear AND l.rowDeleteFlag = ''"
                +"      LEFT JOIN Appraisal a ON a.subjectId = u.unitId AND a.subjectTypeCd = 1 AND a.appraisalYear = @appraisalYear AND a.rowDeleteFlag = ''"              
                +" WHERE u.appraisalYear = @appraisalYear "
                +"      AND u.rowDeleteFlag = ''"
                +" GROUP BY u.unitId, "
                +"      u.unitName, "
                +"      u.operatorId, "
                +"      o.operatorName, "
                +"      CAST(uvs.royaltyInterestPercent as varchar(15)), "
                +"      CAST(uvs.workingInterestPercent as varchar(15)), "
                +"      CAST(uvs.weightedRoyaltyInterestPercent as varchar(15)), "
                +"      CAST(uvs.weightedWorkingInterestPercent as varchar(15)), "
                +"      uvs.royaltyInterestValue, " 
                +"      uvs.workingInterestValue, "
                +"      uvs.royaltyInterestValue + uvs.workingInterestValue ";

            //console.log(sql);
            UtilFunctions.execSql(sql, res);
        } else {
            res.status(400).json({"success": false, "data": "No data received"});
        }
    }, //end CreateLease

};