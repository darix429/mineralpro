module.exports = {
    ReadUnitLeasesLease: function (req, res) {
        //console.log("Called ReadUnitLeasesLease")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var appraisalYear = req.query.appraisalYear;

       var sql = ""
                +"SELECT DISTINCT "
                +"l.idLease, "
                +"l.leaseId, "
                +"l.leaseName, "
                +"lvs.workingInterestPercent, "
                +"lvs.royaltyInterestPercent, "
                +"lvs.workingInterestValue, "
                +"lvs.royaltyInterestValue, "
                +"l.unitId, "
                +"op.operatorName, "
                +"a.subjectId "
                +"FROM "
                +"Lease l "
                +"LEFT JOIN Operator op ON l.operatorId = op.operatorId AND op.appraisalYear='"+appraisalYear+"' AND op.rowDeleteFlag = ''  "
                +"LEFT JOIN Appraisal a ON a.subjectId = l.leaseId AND a.appraisalYear='"+appraisalYear+"' AND a.rowDeleteFlag = '' AND a.subjectTypeCd= 2 "
                +"INNER JOIN LeaseValueSummary lvs ON l.leaseId = lvs.leaseId AND lvs.appraisalYear = '"+appraisalYear+"' AND lvs.rowDeleteFlag = ''  "				
                +"WHERE "
                +"l.appraisalYear = '"+appraisalYear+"' and l.rowDeleteFlag = ''"
                +"ORDER BY l.leaseName ASC ";             
        UtilFunctions.execSql(sql, res)
    },//end ReadUnitLeasesLease
};
