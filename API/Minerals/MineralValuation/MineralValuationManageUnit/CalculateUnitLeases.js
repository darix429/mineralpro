module.exports = {
    CalculateUnitLeases: function (req, res) {
        var UtilFunctions = require('../../../UtilFunctions.js');
        if (req.body) {
            if(!req.query.rowUpdateUserid || !req.query.appraisalYear || !req.query.unitId){
                res.status(400).json({"success": false, "error": "Please check your inputs and try again."});
            }
            var arrData = req.body;
            if (req.query.updateLength == 1) {
                arrData = [req.body];
            }
            var sql =" DECLARE @queueUserid bigint = '"+req.query.rowUpdateUserid+"',"
                    +"        @unitTypeCode smallint = 1,"
                    +"        @maxYearLife smallint = 25,"
                    +"        @appraisalYear smallint = '"+req.query.appraisalYear+"',"
                    +"        @subjectId int = '"+req.query.unitId+"'; ";
            for(var ctr = 0; ctr < arrData.length; ctr++) {
                var lease = arrData[ctr];
                if(lease.rowDeleteFlag == 'D'){
                    sql +=" UPDATE lease SET "
                        + " unitId = '0', "
                        + " rowUpdateDt = GETDATE(), "
                        + " rowUpdateUserid = @queueUserid "
                        + " WHERE leaseId = '"+lease.leaseId+"'; "    

                }else{
                    if(lease.new){
                        sql +=" UPDATE Lease SET "
                            + " unitId = @subjectId ,"
                            + " rowUpdateDt = GETDATE(), "
                            + " rowUpdateUserid = @queueUserid "
                            + " WHERE leaseId = '"+lease.leaseId+"' ; "

                            + " UPDATE leaseValueSummary SET "
                            + " tractNum = '" + lease.tractNum + "', "
                            + " unitWorkingInterestPercent = '" + lease.unitWorkingInterestPercent + "', "
                            + " unitRoyaltyInterestPercent = '" + lease.unitRoyaltyInterestPercent + "', "
                            + " workingInterestValue = '" + lease.workingInterestValue + "', "
                            + " royaltyInterestValue = '" + lease.royaltyInterestValue + "', "
                            // + " appraisedValue = '" + parseFloat(lease.royaltyInterestValue) + parseFloat(lease.workingInterestValue) + "', "
                            + " rowUpdateDt = GETDATE(), "
                            + " rowUpdateUserid = '" + lease.rowUpdateUserid + "' "
                            + " WHERE leaseId = '"+lease.leaseId+"' ;"
                    }

                    sql +=" UPDATE leaseValueSummary SET "
                        + " tractNum = '" + lease.tractNum + "', "
                        + " unitWorkingInterestPercent = '" + lease.unitWorkingInterestPercent + "', "
                        + " unitRoyaltyInterestPercent = '" + lease.unitRoyaltyInterestPercent + "', "
                        + " workingInterestValue = '" + lease.workingInterestValue + "', "
                        + " royaltyInterestValue = '" + lease.royaltyInterestValue + "', "
                        + " rowUpdateDt = GETDATE(), "
                        + " rowUpdateUserid = @queueUserid "
                        + " WHERE leaseId = '" + lease.leaseId + "'" 
                        + " AND appraisalYear  = @appraisalYear; "

                        + " EXECUTE spCalcUnitValueSummaryViaLeaseValueSummary @subjectId, @appraisalYear, @queueUserid; "

                        + " UPDATE UnitValueSummary SET "
                        + " weightedWorkingInterestPercent = b.WgtWI ,"
                        + " weightedRoyaltyInterestPercent = b.WgtRI "
                        + " FROM UnitValueSummary uvs "
                        + " INNER JOIN ("
                        + "   SELECT "
                        + "     CASE WHEN d1 <> 0 AND d3 <> 0 THEN dbo.udfBankRound(d1/d3, 6) ELSE 0 END WgtWI,"
                        + "     CASE WHEN d2 <> 0 AND d4 <> 0 THEN dbo.udfBankRound(d2/d4, 6) ELSE 0 END WgtRI "
                        + "   FROM ("
                        + "     SELECT SUM(LVS.workingInterestPercent * LVS.unitWorkingInterestPercent) d1 "
                        + "       , SUM(LVS.unitRoyaltyInterestPercent * (LVS.royaltyInterestPercent +LVS.overridingRoyaltyPercent +LVS.oilPaymentPercent)) d2 "
                        + "       , SUM(LVS.unitWorkingInterestPercent) d3 "
                        + "       , SUM(LVS.unitRoyaltyInterestPercent) d4 "
                        + "     FROM Lease LSE "
                        + "     INNER JOIN LeaseValueSummary LVS "
                        + "       ON LVS.appraisalYear = LSE.appraisalYear "
                        + "       AND LVS.leaseId = LSE.leaseId "
                        + "       AND LVS.rowDeleteFlag = ''"
                        + "     WHERE LSE.appraisalYear = @appraisalYear "
                        + "       AND LSE.unitId = @subjectId "
                        + "       AND LSE.unitId = @subjectId "
                        + "       AND LSE.rowDeleteFlag = ''"
                        + "   )a"
                        + " )b "
                        + "   ON 1 = 1 "
                        + " WHERE uvs.appraisalYear = @appraisalYear "
                        + " AND uvs.unitId = @subjectId "
                        + " AND uvs.rowDeleteFlag = '' ";
                        
                }
            }
            sql = " BEGIN TRANSACTION; "
                + " BEGIN TRY "
                // + "    --execute all your stored proc code here and then commit"
                + sql
                + "     EXECUTE spCalcAmortizationYears  @queueUserid,@appraisalYear, @maxYearLife, 0, 1, @subjectId, 0;"
                + "     select "
                + "         u.unitId"
                + "        , u.unitName"
                + "        , l.leaseId"
                + "        , l.leaseName"
                + "        , lv.tractNum"
                + "        , lv.unitWorkingInterestPercent"
                + "        , lv.unitRoyaltyInterestPercent"
                + "        , uv.royaltyInterestPercent"
                + "        , uv.weightedRoyaltyInterestPercent"
                + "        , (lv.workingInterestPercent * lv.unitWorkingInterestPercent) workingInterestPercentWeight"
                + "        , ((lv.royaltyInterestPercent + lv.overridingRoyaltyPercent +lv.oilPaymentPercent) * lv.unitRoyaltyInterestPercent) royaltyInterestPercentWeight"
                + "        , uv.workingInterestPercent"
                + "        , uv.weightedWorkingInterestPercent"
                + "        , Case When uv.weightedWorkingInterestPercent > 0 "
                + "           Then dbo.udfBankRound((crs.workingInterestTotalPV + crs.equipmentValue) / uv.weightedWorkingInterestPercent * lv.unitWorkingInterestPercent * lv.workingInterestPercent, 0)"
                + "           Else 0.0 End workingInterestValue"
                + "        , "
                + "          Case When uv.weightedRoyaltyInterestPercent > 0"
                + "           Then dbo.udfBankRound(crs.royaltyInterestTotalPV / uv.weightedRoyaltyInterestPercent * lv.unitRoyaltyInterestPercent * (lv.royaltyInterestPercent + lv.oilPaymentPercent + lv.overridingRoyaltyPercent), 0)"
                + "           Else 0.0 End royaltyInterestValue"
                + "     From LeaseValueSummary lv"
                // + "     --Join LEASE to get 'unitId' for each lease"
                + "     Join dbo.Lease l"
                + "       On l.appraisalYear = lv.appraisalYear"
                + "         And l.leaseId = lv.leaseId"
                + "         And l.rowDeleteFlag <> 'D'"
                // + "     --Sum all Appraisals for a unit (rrcNumber)"
                // + "     --Appraisal table was updated earlier in this procedure"
                // + "     --This limits the update to what was in the calc working set"
                + "     Join dbo.[vCalcResultSummary] crs"
                + "       On crs.appraisalYear = l.appraisalYear"
                + "         And crs.subjectId = l.unitId"
                + "         And crs.queueUserid = @queueUserid"
                + "         And crs.subjectTypeCd = @unitTypeCode"
                // + "     --Join UnitValueSummary for weighted percents"
                + "     Join dbo.UnitValueSummary uv"
                + "       On uv.appraisalYear = l.appraisalYear"
                + "         And uv.unitId = l.unitId"
                + "     JOIN dbo.Unit u"
                + "       ON uv.unitId = u.unitId"
                + "         AND uv.appraisalYear = u.appraisalYear"
                + "         AND u.rowDeleteFlag <> 'D'"
                + "     Where lv.rowDeleteFlag <> 'D'"
                + "       And lv.appraisalYear = @appraisalYear"
                + "     select crs.workingInterestTotalPV + crs.equipmentValue workingInterestValue"
                + "       , crs.royaltyInterestTotalPV royaltyInterestValue"
                + "       , crs.workingInterestTotalPV + crs.equipmentValue + crs.royaltyInterestTotalPV appraisedValue"
                + "     From [dbo].[vCalcResultSummary] crs"
                + "     Where crs.subjectId = @subjectId"
                + "       And crs.queueUserid = @queueUserid"
                + "       And crs.subjectTypeCd = @unitTypeCode"
                + "       AND crs.appraisalYear = @appraisalYear"
                + "     ROLLBACK;"
                + " END TRY"
                + " BEGIN CATCH"
                // + "    --if an exception occurs execute your rollback, also test that you have had some successful transactions"
                + "    IF @@TRANCOUNT > 0 ROLLBACK;  "
                + " END CATCH";
            
            // res.json({sql: sql});
            UtilFunctions.execSqlCallBack(sql, res, function(recordset){
                res.json({success: true, data: recordset})
            });
        } else {
            res.status(400).json({"success": false, "data": "Invalid request body"});
        }
    }//end UpdateUnitLeases
};
