 module.exports = {
    ReadUnitLeases: function (req, res) {
        //console.log("Called ReadUnitLeases")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = " SELECT "
                +" u.unitName [Unit Name], "
                +" u.unitId, "
//                +" CAST(uvs.royaltyInterestPercent as varchar(15)) royaltyInterestPercent, "
//                +" CAST(uvs.workingInterestPercent as varchar(15)) workingInterestPercent, "
                +" CAST((lvs.royaltyInterestPercent + lvs.overridingRoyaltyPercent +lvs.oilPaymentPercent) as varchar(15)) royaltyInterestPercent, "
                +" CAST(lvs.workingInterestPercent as varchar(15)) workingInterestPercent, "
                +" ((lvs.royaltyInterestPercent + lvs.overridingRoyaltyPercent +lvs.oilPaymentPercent) * lvs.unitRoyaltyInterestPercent) royaltyInterestPercentWeight, "
                +" (lvs.workingInterestPercent * lvs.unitWorkingInterestPercent) workingInterestPercentWeight, "
                +" uvs.weightedRoyaltyInterestPercent, "
                +" uvs.weightedWorkingInterestPercent, "
                +" (lvs.royaltyInterestValue) royaltyInterestValue, "
                +" (lvs.workingInterestValue) workingInterestValue, "
                +" uvs.workingInterestValue totalWorkingInterestValue, "
                +" uvs.royaltyInterestValue totalRoyaltyInterestValue, "
                +" l.leaseName, "
                +" l.leaseId, "
                +" lvs.tractNum, "
                +" CAST(lvs.unitWorkingInterestPercent as varchar(15)) unitWorkingInterestPercent, "
                +" CAST(lvs.unitRoyaltyInterestPercent as varchar(15)) unitRoyaltyInterestPercent  "
                +" from Unit u "
                +" INNER JOIN UnitValueSummary uvs on uvs.unitId = u.unitId and uvs.appraisalYear = '"+ req.query.appraisalYear + "' and uvs.rowDeleteFlag = '' "
                +" INNER JOIN lease l on l.unitId = u.unitId and l.appraisalYear = '"+ req.query.appraisalYear + "' and l.rowDeleteFlag = '' "
                +" INNER JOIN LeaseValueSummary lvs on lvs.leaseId = l.leaseId and lvs.appraisalYear = '"+ req.query.appraisalYear + "' and lvs.rowDeleteFlag = '' "
                +" WHERE "
                +" u.rowDeleteFlag = '' and u.appraisalYear = '"+ req.query.appraisalYear + "' and u.unitId = '"+ req.query.selectedId   + "'; ";



        UtilFunctions.execSql(sql, res)
    },//end ReadUnitLeases
};
