module.exports = {
    CreateUnitLeases: function (req, res) {
        //console.log("Called CreateUnitLeases")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var appraisalYear = req.query.appraisalYear
       if(req.body){
            var arrData = req.body;
            if(req.query.createLength == 1){arrData = [req.body];}
            //console.log(arrData);
            var sql ="DECLARE @maxYearLife int = 25"
                    +"SELECT @maxYearLife = max([maxYearLife]) FROM Appraisal WHERE subjectId = "+arrData[0].unitId+" AND not rowDeleteFlag='D' AND appraisalYear = '"+req.query.appraisalYear+"' ";
            for(var ctr=0; ctr<arrData.length; ctr++){
                sql += ""
                    +"UPDATE Lease SET "
                        +"unitId = '"+arrData[ctr].unitId+"' ,"
                        +"rowUpdateDt = GETDATE(), "
                        +"rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                    +"WHERE leaseId = '"+arrData[ctr].leaseId+"' ;"

                    +"UPDATE leaseValueSummary SET "
                        + "tractNum = '" + arrData[ctr].tractNum + "', "
                        + "unitWorkingInterestPercent = '" + arrData[ctr].unitWorkingInterestPercent + "', "
                        + "unitRoyaltyInterestPercent = '" + arrData[ctr].unitRoyaltyInterestPercent + "', "
                        + "workingInterestValue = '" + arrData[ctr].workingInterestValue + "', "
                        + "royaltyInterestValue = '" + arrData[ctr].royaltyInterestValue + "', "
                        // + "appraisedValue = '" + parseFloat(arrData[ctr].royaltyInterestValue) + parseFloat(arrData[ctr].workingInterestValue) + "', "
                        +"rowUpdateDt = GETDATE(), "
                        + "rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                    +"WHERE leaseId = '"+arrData[ctr].leaseId+"' ;"

                    +"EXECUTE spCalcUnitValueSummaryViaLeaseValueSummary '"+arrData[ctr].unitId+"', '"+req.query.appraisalYear+"', '" + arrData[ctr].rowUpdateUserid + "' "
                
                    +"EXECUTE spCalcAmortizationYears  "+arrData[ctr].rowUpdateUserid+","+req.query.appraisalYear+", @maxYearLife, 1, 1, "+arrData[ctr].unitId+", 0 ";	

//                    +"UPDATE unitValueSummary SET "
//                        +"royaltyInterestPercent = (SELECT SUM(unitRoyaltyInterestPercent) FROM leaseValueSummary WHERE leaseId IN(SELECT leaseId FROM Lease WHERE unitId = '"+arrData[ctr].unitId+"' AND appraisalYear = '"+req.query.appraisalYear+"' AND rowDeleteFlag = '')  AND appraisalYear = '"+req.query.appraisalYear+"'  AND rowDeleteFlag = ''), "
//                        +"workingInterestpercent = (SELECT SUM(unitWorkingInterestPercent) FROM leaseValueSummary WHERE leaseId IN(SELECT leaseId FROM Lease WHERE unitId = '"+arrData[ctr].unitId+"' AND appraisalYear = '"+req.query.appraisalYear+"' AND rowDeleteFlag = '')  AND appraisalYear = '"+req.query.appraisalYear+"'  AND rowDeleteFlag = ''), "
//                        +"rowUpdateDt = GETDATE(), "
//                        +"rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
//                    +"WHERE unitId = '"+arrData[ctr].unitId+"' ;"
           }
           //console.log(sql)
            UtilFunctions.execSql(sql, res);
        }else{
            res.status(400).json({"success":false,"data":"No data received"});
        }
    },//end CreateUnitLeases
};
