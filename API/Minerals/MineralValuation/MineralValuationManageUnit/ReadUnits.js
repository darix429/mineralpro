module.exports = {
    ReadUnits: function (req, res) {
        //console.log("Called ReadUnits")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var generateSql = function(generateSql){
            var requiredSql = " UPDATE LastUpdateTime SET dataRefreshTime = getdate() "
                            + " WHERE tableName = 'Unit' AND currentUserId = '"+req.query.idAppUser+"' "
                            + " IF @@ROWCOUNT=0 "
                            + " BEGIN "
                            + "     INSERT INTO LastUpdateTime (tableName, currentUserId) VALUES('Unit', '"+req.query.idAppUser+"')"
                            + " END "
                            + " DECLARE @appraisalYear VARCHAR(4) = '"+req.query.appraisalYear+"' "
            var genSql ="SELECT "
                    +"COUNT(l.unitId) + CASE WHEN COUNT(a.subjectId) = 0  THEN 0 ELSE CASE WHEN (uvs.royaltyInterestValue + uvs.workingInterestValue) = 0 THEN 0 ELSE 1 END END enableDeleteCount, "
                    +"u.unitId, "
                    +"u.unitName, "
                    +"u.operatorId, "
                    +"o.operatorName, "
                    +"CAST(uvs.royaltyInterestPercent as varchar(15)) totalRoyaltyInterest, "
                    +"CAST(uvs.workingInterestPercent as varchar(15)) totalWorkingInterest, "
                    +"CAST(uvs.weightedRoyaltyInterestPercent as varchar(15)) weightedRoyaltyInterest, "
                    +"CAST(uvs.weightedWorkingInterestPercent as varchar(15)) weightedWorkingInterest, "
                    +"uvs.royaltyInterestValue, " 
                    +"uvs.workingInterestValue, "
                    +"uvs.royaltyInterestValue + uvs.workingInterestValue totalUnitValue "
                    +"FROM Unit u "
                        +"INNER JOIN Operator o ON u.operatorId = o.operatorId "
                            +"AND o.appraisalYear = u.appraisalYear AND o.rowDeleteFlag = '' "
                        +"INNER JOIN UnitValueSummary uvs ON u.unitId = uvs.unitId "
                             +"AND uvs.appraisalYear = u.appraisalYear AND uvs.rowDeleteFlag = '' "
                        +"LEFT JOIN Lease l ON l.unitId = u.unitId AND l.appraisalYear = @appraisalYear AND l.rowDeleteFlag = ''"
                        +"LEFT JOIN Appraisal a ON a.subjectId = u.unitId AND a.subjectTypeCd = 1 AND a.appraisalYear = @appraisalYear AND a.rowDeleteFlag = ''"              
                        +"WHERE u.appraisalYear = @appraisalYear "
                        +"AND u.rowDeleteFlag = ''"
                        +"GROUP BY u.unitId, "
                            +"u.unitName, "
                            +"u.operatorId, "
                            +"o.operatorName, "
                            +"CAST(uvs.royaltyInterestPercent as varchar(15)), "
                            +"CAST(uvs.workingInterestPercent as varchar(15)), "
                            +"CAST(uvs.weightedRoyaltyInterestPercent as varchar(15)), "
                            +"CAST(uvs.weightedWorkingInterestPercent as varchar(15)), "
                            +"uvs.royaltyInterestValue, " 
                            +"uvs.workingInterestValue, "
                            +"uvs.royaltyInterestValue + uvs.workingInterestValue ";
            var orderSql = "ORDER BY unitId DESC";
            if(whereClause){
                genSql = " SELECT tbl.* FROM ( "+genSql+" )tbl "+whereClause+" ";
            }
            /*------------------------------ START ------------------------------
            | Enclose genSql inside a try catch incase the filter values exceed the maximum value for the field it's trying to compare value from like int
            */
            genSql = " "
                    +" BEGIN TRY "
                    +       genSql + orderSql
                    +" END TRY "
                    +" BEGIN CATCH "
                    // +"     SELECT  ERROR_NUMBER() AS ErrorNumber   "
                    // +"       ,ERROR_SEVERITY() AS ErrorSeverity   "
                    // +"       ,ERROR_STATE() AS ErrorState   "
                    // +"       ,ERROR_PROCEDURE() AS ErrorProcedure   "
                    // +"       ,ERROR_LINE() AS ErrorLine   "
                    // +"       ,ERROR_MESSAGE() AS ErrorMessage;   "
                    +" END CATCH; "
                
            genSql = requiredSql + genSql;
            return genSql;
        };
        
               
        
        var filters = req.query.filter ? JSON.parse(req.query.filter) || [] : [];
        var filterBy = req.query.filterBy || 'start';
        var sql = '';
        var whereClause = '';
        if(filters.length>0){
            whereClause = 'WHERE ';
            filters.forEach(function(filter){
                whereClause = whereClause=='WHERE ' ? whereClause : whereClause+' AND ';
                var value = filter.value;
                value = value.toUpperCase();
                if(filterBy == 'start'){
                    if(typeof value === 'number'){
                        whereClause += 'tbl.'+filter.property+" >= "+value;
                    }
                    else if(typeof value === 'string'){
                        whereClause += 'tbl.'+filter.property+" LIKE '"+value+"%'";
                    }
                }
                else if(filterBy == 'exact'){
                    value = typeof value == 'string' ? "'"+value+"'" : value;
                    whereClause += 'tbl.'+filter.property+" = "+value;
                }
                else if(filterBy == 'contain'){
                    whereClause += 'tbl.'+filter.property+" LIKE '%"+value+"%'";
                }
            });
        }
        if(whereClause){
            sql = generateSql(whereClause);
        }
        else{
            sql = generateSql();
        }
        
        UtilFunctions.execSql(sql, res)

        // if(req.query.dataCount == 0){      
        //     UtilFunctions.execSql(sql, res)
        // }else{
        //     var sqlCond = "SELECT * FROM LastUpdateTime "
        //         +"WHERE lastUpdateTime > dataRefreshTime "
        //         +"AND tableName = 'Unit' "
        //         +"AND currentUserId = '"+req.query.idAppUser+"'";

        //     UtilFunctions.execSqlCallBack(sqlCond, res, function (rec) {
        //         if(rec.length > 0){
        //             UtilFunctions.execSql(sql, res)
        //         }else{
        //             res.status(200).json({"success":false,"data":"Do not refresh the data"});
        //         }
        //     });                     
        // }
//        UtilFunctions.execSql(sql, res)
    },//end ReadUnits
};
