module.exports = {
    UploadLeaseOwner: function (req, res) {
        var UtilFunctions = require('../../../UtilFunctions.js');
        //console.log("Called UploadLeaseOwner");		
		var data = req.body;
		if(req.query.createLength==1){ data = [req.body]; }
		if(req.query.appraisalYear && data){
			var sql="";
			for(var i=0; i<data.length; i++){
				sql+=""
					+"UPDATE Owner SET "
						+"name1 = '"+data[i].Owner_Name+"',"
						+"name2 = '"+data[i].Owner_Name2+"',"
						+"addrLine1 = '"+data[i].Owner_Mailing1+"',"
						+"addrLine3 = '"+data[i].Owner_Mailing2+"',"
						+"city = '"+data[i].Owner_City+"',"
						+"stateCd = (SELECT code,description FROM SystemCode WHERE codeType = 790 AND appraisalYear = 2017 AND description like '"+data[i].Owner_ST+"'),"
						+"rowUpdateDt = GETDATE(), "
						+"rowDeleteFlag='', "
						+"rowUpdateUserid='"+data[i].rowUpdateUserid+"', "
						+"zipcode = '"+data[i].Owner_Zip+"'"
					+"WHERE "
						+"appraisalYear = '"+req.query.appraisalYear+"' "
						+"AND ownerId = = '"+data[i].Owner_ID+"' "
					+"IF @@ROWCOUNT=0 "
					+"INSERT INTO Owner ( "
	                    +" appraisalYear, "
	                    +" ownerId, "
	                    +" name1, "
	                    +" name2, "
	                    +" addrLine1, "
	                    +" addrLine3, "
	                    +" city, "
	                    +" stateCd, "
	                    +" zipcode, "
	                    +" rowUpdateUserid "
					+")VALUES ("
	                    +"'"+req.query.appraisalYear+"', "
	                    +"'"+data[i].Owner_ID+"', "
	                    +"'"+data[i].Owner_Name+"', "
	                    +"'"+data[i].Owner_Name2+"', "
	                    +"'"+data[i].Owner_Mailing1+"', "
	                    +"'"+data[i].Owner_Mailing2+"', "
	                    +"'"+data[i].Owner_City+"', "
	                    +"(SELECT code FROM SystemCode WHERE codeType = 790 AND appraisalYear = 2017 AND description like '"+data[i].Owner_ST+"'), "
	                    +"'"+data[i].Owner_Zip+"'"
	                    +"'"+data[i].rowUpdateUserid+"'); "


					+"UPDATE LeaseOwner SET "
						+"appraisalYear='"+req.query.appraisalYear+"', "
						+"leaseId='"+data[i].leaseId+"', "
						+"ownerId='"+data[i].Owner_ID+"', "
						+"interestTypeCd='"+data[i].Interest_Type+"', "
						+"interestPercent='"+data[i].Interest_Percent+"', "
						+"rowUpdateUserid='"+data[i].rowUpdateUserid+"', "
						+"rowUpdateDt = GETDATE(), "
						+"rowDeleteFlag='' "
					+"WHERE "
						+"appraisalYear='"+req.query.appraisalYear+"' "
						+"AND leaseId='"+data[i].leaseId+"' "
						+"AND ownerId='"+data[i].Owner_ID+"' "
						+"AND interestTypeCd='"+data[i].Interest_Type+"' "
					+"IF @@ROWCOUNT=0 "
					+"INSERT INTO LeaseOwner( "
						+"appraisalYear, "
						+"leaseId, "
						+"ownerId, "
						+"interestTypeCd, "
						+"interestPercent, "
						+"rowUpdateUserid"
					+")VALUES( "
						+"'"+req.query.appraisalYear+"', "
						+"'"+data[i].leaseId+"', "
						+"'"+data[i].Owner_ID+"', "
						+"(SELECT code FROM SystemCode WHERE codeType = 3000 AND appraisalYear = 2017 AND description like '"+data[i].Interest_Type+"'), "
						+"'"+data[i].Interest_Percent+"', "
						+"'"+data[i].rowUpdateUserid+"' "
					+"); ";
				// sql+="EXECUTE calculateLeaseValueSummary "+data[i].leaseId+","+req.query.appraisalYear+" ";
			}			
			//console.log(sql);
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"Error"});
		}
		
    },
};

