module.exports = {
    ReadLease: function (req, res) {
        //console.log("Called ReadLease")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var generateSql = function(whereClause){
            var declaration = ""
                + " DECLARE @idAppUser VARCHAR(50) = '"+ req.query.idAppUser +"'"
                + " DECLARE @appraisalYear VARCHAR(4) = '"+ req.query.appraisalYear +"'"
                + " UPDATE lastupdatetime "
                + " SET    datarefreshtime = Getdate() "
                + " WHERE  tablename = 'Lease' AND currentuserid = @idAppUser"
                + " IF @@ROWCOUNT = 0 "
                + " BEGIN "
                + "   INSERT INTO lastupdatetime (tablename, currentuserid) "
                + "     VALUES ('Lease', CAST(@idAppUser AS BIGINT)) "
                + " END"
            var genSql = ""
                + " SELECT DISTINCT "
                + " Count(a.subjectid) + CASE WHEN l.unitid = 0 THEN 0 ELSE 1 END "
                + " enableDeleteCount, "
                + " l.leaseId, "
                + " l.leaseName, "
                + " l.divisionOrderDescription, "
                //+ " -- l.divisionOrderDt, "
                + " Substring(Replace(CONVERT(VARCHAR(10), l.divisionorderdt, 111), '/', '-'), 1, 11) AS divisionOrderDt, "
                + " l.description, "
                + " l.unitid, "
                //+ " -- l.comment, "
                //+ " -- l.changeReasonCd, "
                //+ " -- l.cadCategoryCd, "
                + " CASE "
                + "   WHEN l.unitid = 0 THEN l.operatorid "
                + "   ELSE "
                + "   ("
                + "     SELECT operatorid "
                + "     FROM   operator "
                + "     WHERE  operatorid = u.operatorid "
                + "       AND appraisalyear = @appraisalYear"
                + "   )"
                + " END operatorId, "
                + " CASE "
                + "   WHEN l.unitid = 0 THEN "
                + "   ("
                + "     SELECT operatorname "
                + "     FROM   operator "
                + "     WHERE  operatorid = l.operatorid "
                + "       AND appraisalyear = @appraisalYear"
                + "   ) "
                + "   ELSE "
                + "   ("
                + "     SELECT operatorname "
                + "     FROM   operator "
                + "     WHERE  operatorid = u.operatorid "
                + "       AND appraisalyear = @appraisalYear"
                + "   ) "
                + " END operatorName, "
                //+ " -- ( SELECT count(idLeaseOwner) FROM LeaseOwner WHERE appraisalYear=+ req.query.appraisalYear  AND rowDeleteFlag = '' AND leaseId = l.leaseId) as TotalLeaseOwners, "
                //+ " -- ISNULL(( SELECT (sum(interestPercent)*lvs.appraisedValue) FROM LeaseOwner WHERE appraisalYear=+ req.query.appraisalYear  AND rowDeleteFlag = '' AND leaseId = l.leaseId AND interestTypeCd IN (1,2,3)),0) as totalRoyaltyValue, "
                //+ " -- ISNULL(( SELECT (sum(interestPercent)*lvs.appraisedValue) FROM LeaseOwner WHERE appraisalYear=+ req.query.appraisalYear  AND rowDeleteFlag = '' AND leaseId = l.leaseId AND interestTypeCd IN (4)),0) as totalWorkingValue, "
                //+ " -- l.acres, "
                //+ " -- l.UnitId, "
                //+ " -- u.unitName, "
                //+ " -- l.leaseYear, "
                //+ " -- lvs.appraisedValue, "
                //+ " -- lvs.royaltyInterestPercent as royaltyInterest, "
                //+ " -- lvs.oilPaymentPercent as oilPayment, "
                //+ " -- lvs.overridingRoyaltyPercent as overidingRoyalty, "
                //+ " -- lvs.royaltyInterestValue as totalRoyaltyValue, "
                //+ " -- (lvs.royaltyInterestPercent + lvs.oilPaymentPercent + lvs.overridingRoyaltyPercent) as totalRoyaltyInterest,"
                //+ " -- lvs.workingInterestPercent as totalWorkingInterest, "
                //+ " -- (lvs.royaltyInterestPercent + lvs.oilPaymentPercent + lvs.overridingRoyaltyPercent + lvs.workingInterestPercent) as totalInterestPercent,"
                //+ " -- lvs.workingInterestValue as totalWorkingValue, "
                + " l.rowdeleteflag "
                + " FROM   lease l "
                + "   LEFT JOIN unit u "
                + "     ON l.unitid = u.unitid "
                + "        AND u.appraisalyear = @appraisalYear "
                + "   LEFT JOIN appraisal a "
                + "     ON a.subjectid = l.leaseid "
                + "        AND a.subjecttypecd = 2 "
                + "        AND a.rowdeleteflag = '' "
                + "        AND a.appraisalyear = @appraisalYear "
                + "   INNER JOIN leasevaluesummary lvs "
                + "      ON l.leaseid = lvs.leaseid "
                + "         AND lvs.appraisalyear = @appraisalYear "
                + "         AND NOT lvs.rowdeleteflag = 'D' "
                + "         AND l.rowdeleteflag = '' "
                + "         AND l.appraisalyear = @appraisalYear "
                + " GROUP  BY l.leaseid, l.leasename, l.divisionorderdescription, "
                //+ " -- l.divisionOrderDt, "
                + " Substring(Replace(CONVERT(VARCHAR(10), l.divisionorderdt, 111), '/', '-'), 1, 11), "
                + " l.description, "
                //+ " -- l.comment, "
                //+ " -- l.changeReasonCd, "
                //+ " -- l.cadCategoryCd,"
                + " l.unitid, "
                + " l.operatorid, "
                + " u.operatorid, "
                + " l.rowdeleteflag "
            var orderSql = " ORDER  BY leaseid DESC;"
            if(whereClause){
                genSql = " SELECT tbl.* FROM ( "+genSql+" )tbl "+whereClause+" ";
            }
            /*------------------------------ START ------------------------------
            | Enclose genSql inside a try catch incase the filter values exceed the maximum value for the field it's trying to compare value from like int
            */
            genSql = " "
                    +" BEGIN TRY "
                    +       genSql + orderSql
                    +" END TRY "
                    +" BEGIN CATCH "
                    // +"     SELECT  ERROR_NUMBER() AS ErrorNumber   "
                    // +"       ,ERROR_SEVERITY() AS ErrorSeverity   "
                    // +"       ,ERROR_STATE() AS ErrorState   "
                    // +"       ,ERROR_PROCEDURE() AS ErrorProcedure   "
                    // +"       ,ERROR_LINE() AS ErrorLine   "
                    // +"       ,ERROR_MESSAGE() AS ErrorMessage;   "
                    +" END CATCH; "
                
            genSql = declaration + genSql;
            return genSql;
        }

        var filters = req.query.filter ? JSON.parse(req.query.filter) || [] : [];
        var filterBy = req.query.filterBy || 'start';
        var sql = '';
        var whereClause = '';
        if(filters.length>0){
            whereClause = 'WHERE ';
            filters.forEach(function(filter){
                whereClause = whereClause=='WHERE ' ? whereClause : whereClause+' AND ';
                var value = filter.value;
                value = value.toUpperCase();
                if(filterBy == 'start'){
                    if(typeof value === 'number'){
                        whereClause += 'tbl.'+filter.property+" >= "+value;
                    }
                    else if(typeof value === 'string'){
                        whereClause += 'tbl.'+filter.property+" LIKE '"+value+"%'";
                    }
                }
                else if(filterBy == 'exact'){
                    value = typeof value == 'string' ? "'"+value+"'" : value;
                    whereClause += 'tbl.'+filter.property+" = "+value;
                }
                else if(filterBy == 'contain'){
                    whereClause += 'tbl.'+filter.property+" LIKE '%"+value+"%'";
                }
            });
        }
        if(whereClause){
            sql = generateSql(whereClause);
        }
        else{
            sql = generateSql();
        }
        
        UtilFunctions.execSql(sql, res)

        // if(req.query.dataCount == 0){      
        //     UtilFunctions.execSql(sql, res)
        // }else{
        //     var sqlCond = "SELECT * FROM LastUpdateTime "
        //     +"WHERE lastUpdateTime > dataRefreshTime "
        //     +"AND tableName = 'Lease' "
        //     +"AND currentUserId = '"+req.query.idAppUser+"'";

        //     UtilFunctions.execSqlCallBack(sqlCond, res, function (rec) {
        //         if(rec.length > 0){
        //             UtilFunctions.execSql(sql, res)
        //         }else{
        //             res.status(200).json({"success":false,"data":"Do not refresh the data"});
        //         }
        //     });                     
        // }
    },//end ReadLease
};
