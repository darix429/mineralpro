
module.exports = {
    Read: function (req, res) {
        var UtilFunctions = require('../../../UtilFunctions.js');
        //console.log("Called ReadLeaseTaxUnit");      
        var appraisalYear = req.query.appraisalYear;
        var leaseId = req.query.selectedId;

        var sql = ""
            +"SELECT DISTINCT  "
            +"ltu.idLeaseTaxUnit, "
            +"ltu.leaseId, "
            +"l.leaseName, "
            +"ltu.taxUnitTypeCd, "
            +"(SELECT description FROM SystemCode WHERE codeType=970 AND rowDeleteFlag = '' AND appraisalYear='"+appraisalYear+"' AND code = ltu.taxUnitTypeCd) as taxUnitType, "
            +"ltu.taxUnitCd, "
            +"(SELECT description FROM SystemCode WHERE codeType=ltu.taxUnitTypeCd AND rowDeleteFlag = '' AND appraisalYear='"+appraisalYear+"' AND code = ltu.taxUnitCd) as taxUnitName, "
            +"CAST(ltu.taxUnitPercent as varchar(15)) taxUnitPercent "
            +"FROM LeaseTaxUnit ltu "
            +"INNER JOIN Lease l ON ltu.leaseId = l.leaseId AND l.appraisalYear = '"+appraisalYear+"' AND l.rowDeleteFlag = '' "
            +"WHERE ltu.appraisalYear = '"+appraisalYear+"' AND ltu.rowDeleteFlag = '' AND ltu.leaseId = '"+leaseId+"' ; "

            //console.log(sql)
            
    
        UtilFunctions.execSql(sql, res);
    },
};

