module.exports = {
    UpdateLeaseTaxUnit: function (req, res) {
        //console.log("Called UpdateLeaseTaxUnit")
        var UtilFunctions = require('../../../UtilFunctions.js');
        if(req.body && req.query.updateLength){
			var arrData = req.body;
			if(req.query.updateLength == 1){arrData = [req.body];}
			
			var sql = "";     
			for (var ctr = 0; ctr < arrData.length; ctr++) {
                sql += "UPDATE LeaseTaxUnit SET "
                    +" taxUnitTypeCd = '" + arrData[ctr].taxUnitTypeCd + "',"
                    +" taxUnitCd = '" + arrData[ctr].taxUnitCd + "',"
                    +" taxUnitPercent = '" + arrData[ctr].taxUnitPercent + "',"
                    +" rowUpdateDt = GETDATE(), "
                    +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "', "
                    +" rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "'"
                +" WHERE idLeaseTaxUnit = '" + arrData[ctr].idLeaseTaxUnit + "';";
           }
            //console.log(sql);
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"Invalid request body"});
		}
    }//end UpdateLeaseTaxUnit
};
 