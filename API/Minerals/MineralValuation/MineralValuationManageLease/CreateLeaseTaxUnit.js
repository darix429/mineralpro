module.exports = {
    CreateLeaseTaxUnit: function (req, res) {
        //console.log("Called CreateLeaseTaxUnit")
        var UtilFunctions = require('../../../UtilFunctions.js');
        
        if(req.body){
			var arrData = req.body;
			if(req.query.createLength == 1){arrData = [req.body];}
			//console.log(arrData);
			var sql = "";
            for(var ctr=0; ctr<arrData.length; ctr++){
                sql += ""
                    +" UPDATE LeaseTaxUnit SET "
                    +" taxUnitCd ='"+arrData[ctr].taxUnitCd+"',"
                    +" taxUnitPercent = '" + arrData[ctr].taxUnitPercent + "',"
                    +" rowDeleteFlag = '' "
                    +" WHERE "
                    +" appraisalYear='"+req.query.appraisalYear+"' "
                    +" AND leaseId ='"+arrData[ctr].leaseId+"' "
                    +" AND taxUnitTypeCd = '" + arrData[ctr].taxUnitTypeCd + "' "
                    +" AND taxUnitCd = '" + arrData[ctr].taxUnitCd + "' "
                    +" IF @@ROWCOUNT=0 "
                    +" INSERT INTO LeaseTaxUnit ( "
                    +" appraisalYear, "
                    +" leaseId, "
                    +" taxUnitTypeCd, "
                    +" taxUnitCd, "
                    +" taxUnitPercent, "
                    +" rowDeleteFlag, "
                    +" rowUpdateUserid"
                    +" )VALUES( "
                    +" '"+req.query.appraisalYear+"', "
                    +" "+arrData[ctr].leaseId+", "
                    +" "+arrData[ctr].taxUnitTypeCd+", "
                    +" "+arrData[ctr].taxUnitCd+", "
                    +" '"+arrData[ctr].taxUnitPercent+"', "
                    +" '"+arrData[ctr].rowDeleteFlag+"' , "
                    +" '"+arrData[ctr].rowUpdateUserid+"' "
                    +" ); ";          
           }
           console.log(sql)
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"No data received"});
		}
    },//end CreateLeaseTaxUnit
    
};