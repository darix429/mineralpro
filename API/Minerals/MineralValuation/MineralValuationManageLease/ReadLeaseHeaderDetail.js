module.exports = {
    ReadLeaseHeaderDetail: function (req, res) {
        //console.log("Called ReadLease")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = "";
        if(req.query.appraisalYear && req.query.selectedId){
            sql = "SELECT DISTINCT "
                       +" l.leaseId,"
                       +" l.leaseName,"
                       +" l.divisionOrderDescription,"
                       +" l.divisionOrderDt,"
                       +" l.description,"
                       +" l.comment,"
                       +" l.changeReasonCd,"
                       +" ( SELECT description FROM SystemCode WHERE appraisalYear="+ req.query.appraisalYear +" AND rowDeleteFlag = '' AND codeType = 3190 AND code = l.changeReasonCd) as changeReason, "
                       +" l.cadCategoryCd,"
                       +" CASE WHEN l.UnitId = 0 THEN l.operatorId ELSE (SELECT operatorId FROM Operator WHERE operatorId = u.operatorId AND appraisalYear = "+ req.query.appraisalYear +") END as 'operatorId',"
                       +" CASE WHEN l.UnitId = 0 THEN (SELECT operatorName FROM Operator WHERE operatorId = l.operatorId AND appraisalYear = "+ req.query.appraisalYear +") ELSE (SELECT operatorName FROM Operator WHERE operatorId = u.operatorId AND appraisalYear = "+ req.query.appraisalYear +") END as 'operatorName',"
                       +" CASE WHEN l.UnitId = 0 THEN (SELECT CONCAT(operatorId,' - ',operatorName) FROM Operator WHERE operatorId = l.operatorId AND appraisalYear = "+ req.query.appraisalYear +") ELSE (SELECT CONCAT(operatorId,' - ',operatorName) FROM Operator WHERE operatorId = u.operatorId AND appraisalYear = "+ req.query.appraisalYear +") END as 'operator',"
                       +" ( SELECT count(idLeaseOwner) FROM LeaseOwner WHERE appraisalYear="+ req.query.appraisalYear +" AND rowDeleteFlag = '' AND leaseId = l.leaseId) as TotalLeaseOwners, "
//                       +" ISNULL(( SELECT (sum(interestPercent)*lvs.appraisedValue) FROM LeaseOwner WHERE appraisalYear="+ req.query.appraisalYear +" AND rowDeleteFlag = '' AND leaseId = l.leaseId AND interestTypeCd IN (1,2,3)),0) as totalRoyaltyValue, "
//                       +" ISNULL(( SELECT (sum(interestPercent)*lvs.appraisedValue) FROM LeaseOwner WHERE appraisalYear="+ req.query.appraisalYear +" AND rowDeleteFlag = '' AND leaseId = l.leaseId AND interestTypeCd IN (4)),0) as totalWorkingValue, "
                       +"lvs.royaltyInterestValue totalRoyaltyValue,"
                       +"lvs.workingInterestValue totalWorkingValue,"
                       +" l.acres,"
                       +" l.UnitId,"
                       +" u.unitName,"
                       +" l.leaseYear,"
                       +" lvs.appraisedValue,"
                       +" lvs.royaltyInterestPercent as royaltyInterest,"
                       +" lvs.oilPaymentPercent as oilPayment,"
                       +" lvs.overridingRoyaltyPercent as overidingRoyalty,"
                       //+" lvs.royaltyInterestValue as totalRoyaltyValue,"
                       +" (lvs.royaltyInterestPercent + lvs.oilPaymentPercent + lvs.overridingRoyaltyPercent) as totalRoyaltyInterest,"
                       +" lvs.workingInterestPercent as totalWorkingInterest,"
                       +" (lvs.royaltyInterestPercent + lvs.oilPaymentPercent + lvs.overridingRoyaltyPercent + lvs.workingInterestPercent) as totalInterestPercent,"
                       //+" lvs.workingInterestValue as totalWorkingValue,"
                       +" l.rowDeleteFlag "
                +" FROM Lease l "
                +" LEFT JOIN Unit u ON l.UnitId = u.unitId AND u.appraisalYear = '"+ req.query.appraisalYear + "'"
                +" INNER JOIN LeaseValueSummary lvs ON l.leaseId = lvs.leaseId AND lvs.appraisalYear = '"+ req.query.appraisalYear + "' AND NOT lvs.rowDeleteFlag ='D'"
                +" AND l.rowDeleteFlag = '' "
                +" AND l.appraisalYear = '"+ req.query.appraisalYear + "' "
                +" AND l.leaseId = '"+ req.query.selectedId + "' "
//                +" ORDER BY l.leaseName ASC; ";
        }

        UtilFunctions.execSql(sql, res)
    },//end ReadLease
};
