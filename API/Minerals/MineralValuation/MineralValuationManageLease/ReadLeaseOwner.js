
module.exports = {
    Read: function (req, res) {
        //console.log("Called ReadLeaseOwner");      
        var UtilFunctions = require('../../../UtilFunctions.js');
		var appraisalYear = req.query.appraisalYear;
		var leaseId = req.query.selectedId
		
        var sql = ""
				+"SELECT "
				+"lo.idLeaseOwner, "
				+"lo.leaseId, "
				+"l.leaseName, "
				+"lo.ownerId, "
				+"o.name1  ownerName, "
				+"o.name1 as Owner_Name, "
				+"o.name2 as Owner_Name2, "
				+"CONCAT(o.addrLine1,' ',o.addrLine2) as Owner_Mailing1, "
				+"o.addrLine3 as Owner_Mailing2, "
				+"o.city as Owner_City, "
				+"o.zipcode as Owner_Zip, "
				+" ( SELECT description FROM SystemCode WHERE appraisalYear="+ req.query.appraisalYear +" AND rowDeleteFlag = '' AND codeType = 790 AND code = o.stateCd) as Owner_ST, "
				+"CAST((select sum(interestPercent) from LeaseOwner where appraisalYear = "+appraisalYear+" and leaseId =  lo.leaseId and rowDeleteFlag = '') as varchar(15)) as totalInterestPercent, "
				+"lo.interestTypeCd, "
				+"sc.description as interestType, "
				+"lo.interestPercent, "
				+"lvs.appraisedValue, "
				+"lo.ownerValue, "
				// +"(lo.interestPercent * lvs.appraisedValue) as OwnerValues "
                                +"CASE WHEN lo.interestTypeCd = 4 "
                                +"THEN "
									//+"CASE WHEN (lvs.workingInterestValue/lvs.workingInterestPercent *lo.interestPercent) > 0 "
									+"CASE WHEN (lvs.workingInterestPercent *lo.interestPercent) > 0 "
                                    +"THEN "
                                    +"dbo.udfBankRound ((lvs.workingInterestValue/lvs.workingInterestPercent *lo.interestPercent),0) "
                                    // +"WHEN (lvs.workingInterestValue/lvs.workingInterestPercent *lo.interestPercent) > 0 "
                                    // +"AND  (lvs.workingInterestValue/lvs.workingInterestPercent *lo.interestPercent) < 1 "
                                    // +"THEN 1 "
                                    // +"ELSE 0 "
                                    +"END "
                                +"ELSE "
                                    //+"CASE WHEN (lvs.royaltyInterestValue/(lvs.royaltyInterestPercent + lvs.oilPaymentPercent + lvs.overridingRoyaltyPercent) *lo.interestPercent) > 0 "
									+"CASE WHEN ((lvs.royaltyInterestPercent + lvs.oilPaymentPercent + lvs.overridingRoyaltyPercent) *lo.interestPercent) > 0 "
									+"THEN "
                                    +"dbo.udfBankRound ((lvs.royaltyInterestValue/(lvs.royaltyInterestPercent + lvs.oilPaymentPercent + lvs.overridingRoyaltyPercent) *lo.interestPercent),0) "
                                    // +"WHEN  (lvs.royaltyInterestValue/(lvs.royaltyInterestPercent + lvs.oilPaymentPercent + lvs.overridingRoyaltyPercent) *lo.interestPercent) > 0 "
                                    // +"AND  (lvs.royaltyInterestValue/(lvs.royaltyInterestPercent + lvs.oilPaymentPercent + lvs.overridingRoyaltyPercent) *lo.interestPercent) < 1 "
                                    // +"THEN 1 "
                                    // +"ELSE 0 "
                                    +"END "
								+"END as OwnerValues " 
								
				+"FROM LeaseOwner lo "
				+"INNER JOIN Lease l ON lo.leaseId = l.leaseId AND l.appraisalYear = '"+appraisalYear+"' AND l.rowDeleteFlag = '' "
				+"LEFT JOIN Owner o ON lo.ownerId = o.ownerId AND o.appraisalYear = '"+appraisalYear+"' "
				+"INNER JOIN LeaseValueSummary lvs ON lo.leaseId = lvs.leaseId AND lvs.appraisalYear = '"+appraisalYear+"' AND lo.rowDeleteFlag = ''  "
				+"INNER JOIN SystemCode sc on sc.code = lo.interesttypecd and sc.appraisalyear='"+appraisalYear+"' and sc.rowdeleteflag = '' and sc.codetype=3000 "
				+"WHERE lo.leaseId = '"+leaseId+"' AND lo.appraisalYear = '"+appraisalYear+"' AND NOT lo.rowDeleteFlag = 'D' "
				+"ORDER BY o.name1 ASC; "
				//console.log(sql)
	// res.send(sql)	
        UtilFunctions.execSql(sql, res);
    },
};

