module.exports = {
    Read: function (req, res) {
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = ""
			+"SELECT "
			+"codeType, "
			+"code 'cadCategoryCd', "
			+"CASE WHEN code = 0 THEN description ELSE CONCAT(shortDescription, ' - ' , description) END as 'cadCategory' "
			+"FROM "
			+"SystemCode "
			+"WHERE "
			+"codeType=640 "
			+"AND appraisalYear= '"+req.query.appraisalYear+"' ";

        UtilFunctions.execSql(sql, res)
    },
};
