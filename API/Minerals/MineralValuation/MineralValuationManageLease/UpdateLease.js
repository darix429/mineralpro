module.exports = {
    UpdateLease: function (req, res) {
        //console.log("Called UpdateLease")
        var UtilFunctions = require('../../../UtilFunctions.js');
        if(req.body && req.query.updateLength){
			var arrData = req.body;
			if(req.query.updateLength == 1){arrData = [req.body];}
			
			var sql = "";     
			for (var ctr = 0; ctr < arrData.length; ctr++) {
                sql += "UPDATE Lease SET "
                    +" leaseName = UPPER('" + arrData[ctr].leaseName + "'), "
                    +" divisionOrderDescription = UPPER('" + arrData[ctr].divisionOrderDescription + "'), "
                    +" divisionOrderDt = '" + arrData[ctr].divisionOrderDt.substr(0,10) + "', "
                    +" description = UPPER('" + arrData[ctr].description + "'), "
                    +" comment = UPPER('" + arrData[ctr].comment + "'), "
                    +" cadCategoryCd = '" + arrData[ctr].cadCategoryCd + "', "
                    +" changeReasonCd = '" + arrData[ctr].changeReasonCd + "', "
                    +" operatorId = '" + arrData[ctr].operatorId + "', "
                    +" acres = '" + arrData[ctr].acres + "', "
                    +" leaseYear = '" + arrData[ctr].leaseYear + "', "
                    +" rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "',"
                    +" rowUpdateDt = GETDATE(), "
                    +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                +" WHERE leaseId = " + arrData[ctr].leaseId + " ; "
                
                if(arrData[ctr].rowDeleteFlag == 'D'){
                sql +="UPDATE LeaseValueSummary SET "
                    +" rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "', "
                    +" rowUpdateDt = GETDATE(), "
                    +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                    +" WHERE leaseId = " + arrData[ctr].leaseId 
                    +" AND appraisalYear = '"+req.query.appraisalYear+"' ";
            
                sql +="UPDATE LeaseOwner SET "
                    +" rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "', "
                    +" rowUpdateDt = GETDATE(), "
                    +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                    +" WHERE leaseId = " + arrData[ctr].leaseId + ""
                    +" AND appraisalYear = '"+req.query.appraisalYear+"' ";
            
                sql +="UPDATE LeaseTaxUnit SET "
                    +" rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "', "
                    +" rowUpdateDt = GETDATE(), "
                    +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "
                    +" WHERE leaseId = " + arrData[ctr].leaseId + ""
                    +" AND appraisalYear = '"+req.query.appraisalYear+"' ";   
            
                if(parseInt(arrData[ctr].unitId) != 0){
                    sql+="UPDATE UnitValueSummary SET "
                       +"weightedWorkingInterestPercent = b.WgtWI "
                       +", weightedRoyaltyInterestPercent = b.WgtRI "
                       +"FROM UnitValueSummary uvs INNER JOIN "
                       +"(SELECT CASE WHEN d1 <> 0 AND d3 <> 0 THEN "
                       +"dbo.udfBankRound(d1/d3, 6) ELSE 0 END WgtWI,"
                       +"CASE WHEN d2 <> 0 AND d4 <> 0 THEN "
                       +"dbo.udfBankRound(d2/d4, 6) ELSE 0 END WgtRI "
                       +"FROM (SELECT "
                       +"SUM(LVS.workingInterestPercent * LVS.unitWorkingInterestPercent) d1 "
                       +",SUM(LVS.unitRoyaltyInterestPercent * (LVS.royaltyInterestPercent +LVS.overridingRoyaltyPercent +LVS.oilPaymentPercent)) d2 "
                       +",SUM(LVS.unitWorkingInterestPercent) d3 "
                       +",SUM(LVS.unitRoyaltyInterestPercent) d4 "
                       +"FROM Lease LSE "
                       +"INNER JOIN LeaseValueSummary LVS ON LVS.appraisalYear = LSE.appraisalYear "
                            +"AND LVS.leaseId = LSE.leaseId  "
                            +"AND LVS.rowDeleteFlag = ''  "
                        +"WHERE LSE.appraisalYear = '"+req.query.appraisalYear+"' "
                          +"AND LSE.unitId = '"+arrData[ctr].unitId+"' "
                          +"AND LSE.rowDeleteFlag = '')a)b ON 1= 1 "
                        +"WHERE uvs.appraisalYear = '"+req.query.appraisalYear+"' "
                        +"AND uvs.unitId ='"+arrData[ctr].unitId+"' "
                        +"AND uvs.rowDeleteFlag = '' "                  
                }
                
                sql+="EXECUTE spCalcAmortizationYears  "+arrData[ctr].rowUpdateUserid+","+req.query.appraisalYear+", 25, 1, 2, "+arrData[ctr].leaseId+", 0 ";	    
                }
           }
            //console.log(sql);
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"Invalid request body"});
		}
    }//end UpdateLease
};
