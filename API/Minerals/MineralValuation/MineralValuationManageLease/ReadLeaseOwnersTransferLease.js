
module.exports = {
    Read: function (req, res) {
        //console.log("Called ReadLeaseOwnersTransferLease");      
        var UtilFunctions = require('../../../UtilFunctions.js');
		var appraisalYear = req.query.appraisalYear;
		
       var sql = ""
				+"SELECT "
				+"l.idLease, "
				+"l.leaseId, "
				+"lvs.appraisedValue, "
				+"l.rowDeleteFlag, "
				+"l.leaseName "
				+"FROM "
				+"Lease l "			
				+"INNER JOIN LeaseValueSummary lvs ON l.leaseId = lvs.leaseId AND lvs.appraisalYear = '"+appraisalYear+"' AND lvs.rowDeleteFlag = ''  "
				+"WHERE "
				+"l.appraisalYear = "+appraisalYear+" and l.rowDeleteFlag = ''"
				+"ORDER BY l.leaseName ASC ";

		
        UtilFunctions.execSql(sql, res);
    },
};

