
module.exports = {
    Read: function (req, res) {
        var UtilFunctions = require('../../../UtilFunctions.js');
        //console.log("Called ReadLeaseTaxUnitCode");		
        var appraisalYear = req.query.appraisalYear;
        var taxUnitTypeCd = req.query.taxUnitTypeCd;	       
        var sql = ""
		
        if(req.query.taxUnitTypeCd){        
            sql = "SELECT  sc.code as 'taxUnitCd', "
            +"sc.description as 'taxUnitName' "
          //  +"tu.estimatedTaxRate "
            +"FROM SystemCode sc "
          //  +"LEFT JOIN TaxUnit tu ON tu.taxUnitTypeCd = sc.codeType AND tu.taxUnitCd = sc.code AND tu.rowDeleteFlag = '' AND tu.appraisalYear = '"+appraisalYear+"' "
            +"WHERE sc.appraisalYear = '"+appraisalYear+"'  "
            +"AND sc.rowDeleteFlag = '' "
            +"AND sc.codeType = '"+taxUnitTypeCd+"' ;" 
        }
	
        UtilFunctions.execSql(sql, res);
    },
};

