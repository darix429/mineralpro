module.exports = {
    Read: function (req, res) {
        //console.log("Called ReadFields")
        var UtilFunctions = require('../../../UtilFunctions.js');

        var er = "Missing Required Parameters";

        if (req.query.appraisalYear && req.query.selectedId) {
            //console.log(req.query.appraisalYear, req.query.subjectTypeCd, req.query.selectedId);
            var sql = ""
                    + "DECLARE @appraisalYear smallint = " + req.query.appraisalYear + ", "
                    + "@subjectTypeCd smallint = " + req.query.subjectTypeCd + ", "
                    + "@subjectId int = " + req.query.selectedId + " "

                    + "SELECT a.rrcNumber, a.fieldId, f.fieldName, f.wellDepth, a.lastAppraisalYear, description gravityDesc "
                    + "FROM Appraisal a "
                    + "LEFT JOIN Field f ON a.fieldId = f.fieldId AND f.appraisalYear = @appraisalYear "
                    + "AND f.rowDeleteFlag = '' "
                    + "INNER JOIN SystemCode sc ON a.gravityCd = sc.code AND sc.codeType = '3510' "
                    + "AND sc.appraisalYear = @appraisalYear AND sc.rowDeleteFlag = '' "
                    + "WHERE a.subjectTypeCd = @subjectTypeCd "
                    + "AND a.subjectId = @subjectId "
                    + "AND a.appraisalYear = @appraisalYear "
                    + "AND a.rowDeleteFlag = '' "
                    + "ORDER BY a.rrcNumber"
    
            UtilFunctions.execSql(sql, res)
        } else {
            res.status(400).json({"success": false, "data": er});
        }
    }, //end ReadLease
};
