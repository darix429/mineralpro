module.exports = {
    DeleteManageAppraisalDecline: function (req, res) {
        //console.log("Called ReadLease")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = "";
        var queueUserid = req.query.queueUserid;
        
        if (req.query.appraisalYear && req.query.selectedId && req.query.rrcNumber
                && req.query.subjectTypeCd && req.query.declineTypeCd) {  
            
        var data = [req.body]

        for (var ctr = 0; ctr < data.length; ctr++) {
            if(data[ctr].idApprDeclineSchedule){
                sql += "UPDATE ApprDeclineSchedule SET "
                        + "declineYears='0', "
                        + "declinePercent= '0', "
                        + "rowDeleteFlag='" + data[ctr].rowDeleteFlag + "', "
                        + "rowUpdateDt = GETDATE(), "
                        + "rowUpdateUserid = '" + data[ctr].rowUpdateUserid + "' "
                        + "WHERE appraisalYear='" + req.query.appraisalYear + "' "
                        + "AND idApprDeclineSchedule='" + data[ctr].idApprDeclineSchedule + "' "
                        + "AND subjectId='" + req.query.selectedId + "' "
                        + "AND rrcNumber='" + req.query.rrcNumber + "' "
                        + "AND subjectTypeCd='" + req.query.subjectTypeCd + "' "
                        + "AND declineTypeCd='" + req.query.declineTypeCd + "' "
                        + "AND rowDeleteFlag='' "   
            }
        }

        sql += "DELETE FROM ApprDeclineSchedule WHERE declineYears = '0' AND declinePercent = '0' "
            +"AND appraisalYear='" + req.query.appraisalYear + "' "
            + "AND subjectId='" + req.query.selectedId + "' "
            + "AND rrcNumber='" + req.query.rrcNumber + "' "
            + "AND subjectTypeCd='" + req.query.subjectTypeCd + "' "
            + "AND declineTypeCd='" + req.query.declineTypeCd + "' "
    
        sql += "Delete From CalcDeclineSchedule "
                +"Where queueUserid = '"+queueUserid+"' "

            +"Insert Into CalcDeclineSchedule "
            +"SELECT '"+queueUserid+"' "
                +",a.appraisalYear "
                +",a.subjectTypeCd "
                +",a.subjectId "
                +",a.rrcNumber "
                +",a.declineTypeCd "
                +",a.seqNumber "
                +",a.declineYears "
                +",a.declinePercent "
            +"FROM dbo.ApprDeclineSchedule a "
            +"Where a.rowDeleteFlag <> 'D' "
            +"And exists (Select 1 From CalcAppraisalSummary c "
                            +"Where c.queueUserid = '"+queueUserid+"' "
                            +"And c.appraisalYear = a.appraisalYear "
                            +"And c.subjectTypeCd = a.subjectTypeCd "
                            +"And c.subjectId = a.subjectId "
                            +"And c.rrcNumber = a.rrcNumber) "
        
           
        }
          
        UtilFunctions.execSql(sql, res)
    }, //end ReadLease
};
