module.exports = {
    ReadManageAppraisalDecline: function (req, res) {
        //console.log("Called ReadLease")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = "";

        if (req.query.appraisalYear && req.query.selectedId && req.query.rrcNumber 
                && req.query.subjectTypeCd && req.query.declineTypeCd) {
                       
            sql = "DECLARE @appraisalYear varchar(4) = '"+req.query.appraisalYear+"', "
                    +"@subjectId varchar(25) = '"+req.query.selectedId+"', "
                    +"@subjectTypeCd varchar(25) = '"+req.query.subjectTypeCd+"', "
                    +"@rrcNumber varchar(25) = '"+req.query.rrcNumber+"', "
                    +"@declineTypeCd varchar(25) = '"+req.query.declineTypeCd+"' "

                    +"SELECT idApprDeclineSchedule,"
                        +"subjectId, rrcNumber, declineYears, declinePercent, "
                        +"declineYears oriDeclineYears, "
                        +"declinePercent * 100 oriDecline, "
                        +"declinePercent * 100 decline "
                    +"FROM ApprDeclineSchedule "
                    +"WHERE appraisalYear = @appraisalYear "
                        +"AND subjectId = @subjectId "
                        +"AND subjectTypeCd = @subjectTypeCd "
                        +"AND rrcNumber = @rrcNumber "
                        +"AND declineTypeCd = @declineTypeCd "
                        +"AND rowDeleteFlag = '' ORDER BY seqNumber ASC";
            
        }
        UtilFunctions.execSql(sql, res)
    }, //end ReadLease
};
