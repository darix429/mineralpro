module.exports = {
    ReadManageAppraisalSubjectId: function (req, res) {
        //console.log("Called ReadLease")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = "";

        if (req.query.appraisalYear) {
            sql = "DECLARE @appraisalYear varchar(4) = '" + req.query.appraisalYear + "' "
//                    + "SELECT DISTINCT "
//                    + "a.subjectId, a.subjectTypeCd, "
//                    + "CASE WHEN subjectTypeCd = 1 THEN 'Unit' ELSE 'Lease' END subjectType, "
//                    + "CASE WHEN subjectTypeCd = 1 THEN u.unitName ELSE l.leaseName END subjectName, "
//                    + "ISNULL(u.operatorName, l.operatorName) operatorName, "
//                    + "ISNULL(u.agencyName, l.agencyName) agencyName "
//                    + "FROM Appraisal a "
//                    + "LEFT JOIN (SELECT iu.*, ino.operatorName, agc.agencyName "
//                    + "FROM Unit iu  "
//                    + "LEFT JOIN Operator ino ON iu.operatorId = ino.operatorId AND ino.rowDeleteFlag = '' AND ino.appraisalYear = @appraisalYear "
//                    + "LEFT JOIN Owner own ON ino.ownerId = own.ownerId AND own.rowDeleteFlag = '' AND own.appraisalYear = @appraisalYear "
//                    + "LEFT JOIN Agency agc ON agc.agencyCdx = own.agencyCdx AND agc.rowDeleteFlag = '' AND agc.appraisalYear = @appraisalYear "
//                    + "WHERE iu.rowDeleteFlag = '' AND iu.appraisalYear = @appraisalYear "
//                    + ") u ON a.subjectId = u.unitId AND a.subjectTypeCd = 1 AND u.appraisalYear = @appraisalYear AND u.rowDeleteFlag = '' "
//                    + "LEFT JOIN (SELECT il.*, ino.operatorName, agc.agencyName "
//                    + "FROM lease il "
//                    + "LEFT JOIN Operator ino ON il.operatorId = ino.operatorId AND ino.rowDeleteFlag = '' AND ino.appraisalYear = @appraisalYear "
//                    + "LEFT JOIN Owner own ON ino.ownerId = own.ownerId AND own.rowDeleteFlag = '' AND own.appraisalYear = @appraisalYear "
//                    + "LEFT JOIN Agency agc ON agc.agencyCdx = own.agencyCdx AND agc.rowDeleteFlag = '' AND agc.appraisalYear = @appraisalYear "
//                    + "WHERE il.rowDeleteFlag = '' AND il.appraisalYear = @appraisalYear "
//                    + ") l ON a.subjectId = l.leaseId AND a.subjectTypeCd = 2 "
//                    + "AND l.appraisalYear = @appraisalYear AND l.rowDeleteFlag = '' "
//                    + "LEFT JOIN Field f ON a.fieldId = f.fieldId "
//                    + "WHERE a.appraisalYear = @appraisalYear AND a.rowDeleteFlag = '' ";
            
                + "SELECT * FROM ( "
                + "SELECT leaseId subjectId, leaseName subjectName, '2' subjectTypeCd, 'Lease' subjectType "
                + "FROM Lease WHERE appraisalYear = @appraisalYear AND rowDeleteFlag = '' "
                + "UNION "
                + "SELECT unitId subjectId, unitName subjectName, '1' subjectTypeCd, 'Unit' subjectType "
                + "FROM Unit WHERE appraisalYear = @appraisalYear AND rowDeleteFlag = '' "
                + ") a "

            if(req.query.subjectType){                  
                var subjectTypeCd = req.query.subjectType == 'Unit' ? '1': '2';
                    sql += "WHERE a.subjectTypeCd = '" + subjectTypeCd+ "' ";
            }                       
                    sql += "ORDER BY subjectId DESC";
        }
        
        UtilFunctions.execSql(sql, res)
    }, //end ReadLease
};
