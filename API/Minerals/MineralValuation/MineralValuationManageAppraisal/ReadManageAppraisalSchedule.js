module.exports = {
    ReadManageAppraisalSchedule: function (req, res) {
        //console.log("Called ReadLease")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = "";

        if (req.query.appraisalYear && req.query.valueTypeCd) {
                       
            sql = "DECLARE @appraisalYear varchar(4) = '"+req.query.appraisalYear+"', "
                        +"@valueTypeCd varchar(5) = '"+req.query.valueTypeCd+"' "

                    +"SELECT "
                        +"ces.idCalcEquipmentSchedule, "
                        +"sc3160.description wellTypeDesc, "
                        +"sc3150.description wellType, "
                        +"ces.maxDepth, ces.salvageValue, ces.scheduleNumber, "
                        +"FORMAT(ces.scheduleNumber,'00','en-US') + ' ' + sc3150.description + ' - ' + sc3160.description + ' (' +CAST(ces.maxDepth AS VARCHAR) + ' - ' + CAST(ces.salvageValue AS VARCHAR) + ')' displayField, "
                        +"CAST(ces.scheduleNumber AS VARCHAR) valueField "
                    +"FROM SystemCode sc3160 "
                    +"INNER JOIN SystemCode sc3150 ON sc3150.codeType = 3150 AND sc3150.appraisalYear = @appraisalYear "
                            +"AND sc3150.activeInd = 'Y' AND sc3150.rowDeleteFlag = '' "
                    //+"INNER JOIN CalcEquipmentSchedule ces ON ces.wellTypeCd = sc3160.code AND ces.valueTypeCd = sc3150.code "
                    +"INNER JOIN CalcEquipmentSchedule ces ON ces.valueTypeCd = sc3160.code AND ces.wellTypeCd = sc3150.code "
                            +"AND ces.appraisalYear = @appraisalYear AND ces.rowDeleteFlag = '' "
                    +"WHERE sc3160.appraisalYear = @appraisalYear "
                        +"AND sc3160.activeInd = 'Y' AND sc3160.rowDeleteFlag = '' "
                        +"AND sc3160.codeType = 3160 "
                        +"AND sc3150.code = @valueTypeCd "
                    +"ORDER BY ces.valueTypeCd , ces.maxDepth";
            
        }
        UtilFunctions.execSql(sql, res)
    }, //end ReadLease
};
