module.exports = {
    UpdateManageAppraisal: function (req, res) {
        //console.log("Called UpdateLease")
        var UtilFunctions = require('../../../UtilFunctions.js');
        if (req.body && req.query.updateLength) {
            var data = req.body;
            if (req.query.updateLength == 1) {
                data = [req.body];
            }
            var appraisalYear = req.query.appraisalYear;
            var sql = "";

            for (var ctr = 0; ctr < data.length; ctr++) {
                sql += "UPDATE Appraisal SET "
//                        + "appraisalYear='" + appraisalYear + "', "
//                        + "subjectTypeCd='" + data[ctr].subjectTypeCd + "', "
//                        + "subjectId='" + data[ctr].subjectId + "', "
//                        + "rrcNumber='" + data[ctr].rrcNumber + "', "
//                        + "fieldId='" + data[ctr].fieldId + "', "
//                        + "gravityCd='" + data[ctr].gravityCd + "', "
//                        + "comment=UPPER('" + data[ctr].comment + "'), "
//                        + "wellDepth='" + data[ctr].wellDepth + "', "
//                        + "wellDescription=UPPER('" + data[ctr].wellDescription + "'), "
//                        + "wellTypeCd='" + data[ctr].wellTypeCd + "', "
//                        + "yearLife='" + data[ctr].yearLife + "', "
//                        + "maxYearLife='" + data[ctr].maxYearLife + "', "
//                        + "lastAppraisalYear='" + data[ctr].lastAppraisalYear + "', "
//                        + "discountRate='" + data[ctr].discountRate + "', "
//                        + "operatingExpense='" + data[ctr].operatingExpense + "', "
//                        + "reserveOilValue='" + data[ctr].reserveOilValue + "', "
//                        + "totalValue='" + data[ctr].totalValue + "', "
//                        + "productionEquipmentCount='" + data[ctr].productionEquipmentCount + "', "
//                        + "productionEquipmentSchedule='" + data[ctr].productionEquipmentSchedule + "', "
//                        + "productionEquipmentValue='" + data[ctr].productionEquipmentValue + "', "
//                        + "serviceEquipmentCount='" + data[ctr].serviceEquipmentCount + "', "
//                        + "serviceEquipmentSchedule='" + data[ctr].serviceEquipmentSchedule + "', "
//                        + "serviceEquipmentValue='" + data[ctr].serviceEquipmentValue + "', "
//                        + "injectionEquipmentCount='" + data[ctr].injectionEquipmentCount + "', "
//                        + "injectionEquipmentSchedule='" + data[ctr].injectionEquipmentSchedule + "', "
//                        + "injectionEquipmentValue='" + data[ctr].injectionEquipmentValue + "', "
//                        + "grossOilPrice='" + data[ctr].grossOilPrice + "', "
//                        + "grossProductPrice='" + data[ctr].grossProductPrice + "', "
//                        + "grossWorkingInterestGasPrice='" + data[ctr].grossWorkingInterestGasPrice + "', "
//                        + "grossRoyaltyInterestGasPrice='" + data[ctr].grossRoyaltyInterestGasPrice + "', "
//                        + "dailyAverageGas='" + data[ctr].dailyAverageGas + "', "
//                        + "dailyAverageOil='" + data[ctr].dailyAverageOil + "', "
//                        + "dailyAverageProduct='" + data[ctr].dailyAverageProduct + "', "
//                        + "accumGasProduction='" + data[ctr].accumGasProduction + "', "
//                        + "accumOilProduction='" + data[ctr].accumOilProduction + "', "
//                        + "accumProductProduction='" + data[ctr].accumProductProduction + "', "
//                        + "workingInterestGasValue='" + data[ctr].workingInterestGasValue + "', "
//                        + "workingInterestOilValue='" + data[ctr].workingInterestOilValue + "', "
//                        + "workingInterestProductValue='" + data[ctr].workingInterestProductValue + "', "
//                        + "workingInterestTotalPV='" + data[ctr].workingInterestTotalPV + "', "
//                        + "workingInterestTotalValue='" + data[ctr].workingInterestTotalValue + "', "
//                        + "royaltyInterestGasValue='" + data[ctr].royaltyInterestGasValue + "', "
//                        + "royaltyInterestOilValue='" + data[ctr].royaltyInterestOilValue + "', "
//                        + "royaltyInterestProductValue='" + data[ctr].royaltyInterestProductValue + "', "
//                        + "royaltyInterestTotalValue='" + data[ctr].royaltyInterestTotalValue + "', "
                        + " rowDeleteFlag='" + data[ctr].rowDeleteFlag + "', "
                        + " rowUpdateDt = GETDATE(), "
                        + " rowUpdateUserid = '" + data[ctr].rowUpdateUserid + "' "
                        + " WHERE "
                        + " idAppraisal='" + data[ctr].idAppraisal + "' ";
                
                sql +="UPDATE ApprAmortSchedule SET rowDeleteFlag = 'D' "
                    +"WHERE appraisalYear = '" + appraisalYear + "' "
                    +"AND subjectTypeCd = '" + data[ctr].subjectTypeCd + "' "
                    +"AND subjectId = '"+data[ctr].subjectId+"' "
                    +"AND rrcNumber = '"+data[ctr].rrcNumber+"' "
                    +"AND rowDeleteFlag = ''";

                sql +="UPDATE ApprDeclineSchedule SET rowDeleteFlag = 'D' "
                    +"WHERE appraisalYear = '" + appraisalYear + "' "
                    +"AND subjectTypeCd = '" + data[ctr].subjectTypeCd + "' "
                    +"AND subjectId = '"+data[ctr].subjectId+"' "
                    +"AND rrcNumber = '"+data[ctr].rrcNumber+"' "
                    +"AND rowDeleteFlag = ''";
                                  
                if (data[ctr].subjectTypeCd == 1) {
                    sql += "EXECUTE spCalcUnitValueSummaryViaAppraisal '" + data[ctr].subjectId + "', '" + appraisalYear + "' ";
                }
                if (data[ctr].subjectTypeCd == 2) {
                    sql += "EXECUTE spCalcLeaseValueSummaryViaAppraisal '" + data[ctr].subjectId + "', '" + appraisalYear + "' ";
                }
                
                sql += "EXECUTE spUpdateNoteViaAppraisal '" + data[ctr].subjectId + "','" + data[ctr].subjectTypeCd + "','" + appraisalYear + "' ";               
                sql += "EXECUTE spCalcAmortizationYears '" + data[ctr].rowUpdateUserid + "', '"+appraisalYear+"', 25, 1, '"+data[ctr].subjectTypeCd+"', '"+data[ctr].subjectId+"', 0 ";
            }
            UtilFunctions.execSql(sql, res);
        } else {
            res.status(400).json({"success": false, "data": "Invalid request body"});
        }
    }//end UpdateLease
};
