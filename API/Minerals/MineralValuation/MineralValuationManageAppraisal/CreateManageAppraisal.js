module.exports = {
    CreateManageAppraisal: function (req, res) {
//        console.log("Called CreateManageAppraisal")
        var UtilFunctions = require('../../../UtilFunctions.js');

        var data = req.body;

        if (req.query.appraisalYear && data) {
            if(req.query.createLength == 1){data = [req.body];}
            var appraisalYear = req.query.appraisalYear;
            var sql = ""
            for(var ctr=0; ctr<data.length; ctr++){
                sql +="INSERT INTO Appraisal (appraisalYear, subjectTypeCd, subjectId, rrcNumber, "
                    + "fieldId, gravityCd, comment, wellDepth, wellDescription, wellTypeCd, yearLife, "
                    + "maxYearLife, lastAppraisalYear, discountRate, operatingExpense, reserveOilValue, "
                    + "totalValue, productionEquipmentCount, productionEquipmentSchedule, productionEquipmentValue, "
                    + "serviceEquipmentCount, serviceEquipmentSchedule, serviceEquipmentValue, "
                    + "injectionEquipmentCount, injectionEquipmentSchedule ,injectionEquipmentValue, "
                    + "grossOilPrice, grossProductPrice, grossWorkingInterestGasPrice, grossRoyaltyInterestGasPrice, "
                    + "dailyAverageGas, dailyAverageOil, dailyAverageProduct, accumGasProduction, accumOilProduction, "
                    + "accumProductProduction, workingInterestGasValue, workingInterestOilValue, workingInterestProductValue, "
                    + "workingInterestTotalPV, workingInterestTotalValue, royaltyInterestGasValue, royaltyInterestOilValue, "
                    + "royaltyInterestProductValue, royaltyInterestTotalValue, rowDeleteFlag,rowUpdateUserid "
                    + ")VALUES('" + appraisalYear + "', "
                    + "'" + data[ctr].subjectTypeCd + "', "
                    + "'" + data[ctr].subjectId + "', "
                    + "'" + data[ctr].rrcNumber + "', "
                    + "'" + data[ctr].fieldId + "', "
                    + "'" + data[ctr].gravityCd + "', "
                    + "UPPER('" + data[ctr].comment + "'), "
                    + "'" + data[ctr].wellDepth + "', "
                    + "UPPER('" + data[ctr].wellDescription + "'), "
                    + "'" + data[ctr].wellTypeCd + "', "
                    + "'" + data[ctr].yearLife + "', "
                    + "'" + data[ctr].maxYearLife + "', "
                    + "'" + data[ctr].lastAppraisalYear + "', "
                    + "'" + data[ctr].discountRate + "', "
                    + "'" + data[ctr].operatingExpense + "', "
                    + "'" + data[ctr].reserveOilValue + "', "
                    + "'" + data[ctr].totalValue + "', "
                    + "'" + data[ctr].productionEquipmentCount + "', "
                    + "'" + data[ctr].productionEquipmentSchedule + "', "
                    + "'" + data[ctr].productionEquipmentValue + "', "
                    + "'" + data[ctr].serviceEquipmentCount + "', "
                    + "'" + data[ctr].serviceEquipmentSchedule + "', "
                    + "'" + data[ctr].serviceEquipmentValue + "', "
                    + "'" + data[ctr].injectionEquipmentCount + "', "
                    + "'" + data[ctr].injectionEquipmentSchedule + "', "
                    + "'" + data[ctr].injectionEquipmentValue + "', "
                    + "'" + data[ctr].grossOilPrice + "', "
                    + "'" + data[ctr].grossProductPrice + "', "
                    + "'" + data[ctr].grossWorkingInterestGasPrice + "', "
                    + "'" + data[ctr].grossRoyaltyInterestGasPrice + "', "
                    + "'" + data[ctr].dailyAverageGas + "', "
                    + "'" + data[ctr].dailyAverageOil + "', "
                    + "'" + data[ctr].dailyAverageProduct + "', "
                    + "'" + data[ctr].accumGasProduction + "', "
                    + "'" + data[ctr].accumOilProduction + "', "
                    + "'" + data[ctr].accumProductProduction + "', "
                    + "'" + data[ctr].workingInterestGasValue + "', "
                    + "'" + data[ctr].workingInterestOilValue + "', "
                    + "'" + data[ctr].workingInterestProductValue + "', "
                    + "'" + data[ctr].workingInterestTotalPV + "', "
                    + "'" + data[ctr].workingInterestTotalValue + "', "
                    + "'" + data[ctr].royaltyInterestGasValue + "', "
                    + "'" + data[ctr].royaltyInterestOilValue + "', "
                    + "'" + data[ctr].royaltyInterestProductValue + "', "
                    + "'" + data[ctr].royaltyInterestTotalValue + "', "
                    + "'" + data[ctr].rowDeleteFlag + "', "
                    + "'" + data[ctr].rowUpdateUserid + "'); ";
            }
            UtilFunctions.execSql(sql, res)
        } else {
            res.status(400).json({"success": false, "data": "Year/RequestBody Error"});
        }
    }, //end CreateLease

};