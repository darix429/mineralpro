module.exports = {
    CreateManageAppraisalDecline: function (req, res) {
        //console.log("Called ReadLease")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var data = req.body;

        var sql = "";
         var queueUserid = req.query.queueUserid;

        if (req.query.appraisalYear && req.query.selectedId && req.query.rrcNumber && req.query.declineTypeCd) {

            if (req.query.createLength == 1) {
                data = [req.body];
            }
            
           // if(req.query.rrcNumber != req.query.origRrcNumber){
           //     sql += "UPDATE ApprDeclineSchedule SET "
           //             + "rrcNumber='" + req.query.rrcNumber + "', "
           //             + "rowUpdateDt = GETDATE(), "
           //             + "rowUpdateUserid = '" + data[0].rowUpdateUserid + "' "
           //             + "WHERE appraisalYear='" + req.query.appraisalYear + "' "
           //             + "AND subjectId='" + req.query.selectedId + "' "
           //             + "AND rrcNumber='" + req.query.origRrcNumber + "' "
           //             + "AND subjectTypeCd='" + req.query.subjectTypeCd + "' "
           //             + "AND declineTypeCd='" + req.query.declineTypeCd + "' "
           //             + "AND rowDeleteFlag='' "
           // }

            for (var ctr = 0; ctr < data.length; ctr++) {
                if (data[ctr].declineYears != '0' || data[ctr].decline != '0') {
                    sql = "DECLARE @subjectTypeCd varchar(5) = '2', @cnt int "
                        + " SELECT @cnt = count(1) FROM Unit where unitId = '" + req.query.selectedId + "'"
                        + " IF @cnt > 0 BEGIN SET @subjectTypeCd = '1' END "
                        + " INSERT INTO ApprDeclineSchedule (subjectTypeCd, subjectId, rrcNumber, "
                        + " declineTypeCd, seqNumber, declineYears, declinePercent, rowUpdateUserId, "
                        + " rowUpdateDt, rowDeleteFlag, appraisalYear) VALUES(@subjectTypeCd, "
                        // + "rowUpdateDt, rowDeleteFlag, appraisalYear) VALUES( "
                        // + "'" + req.query.subjectTypeCd + "', "
                        + " '" + req.query.selectedId + "', "
                        + " '" + req.query.rrcNumber + "', "
                        + " '" + req.query.declineTypeCd + "', "
                        // + "(SELECT ISNULL(MAX(seqNumber),0)+1 FROM ApprDeclineSchedule WHERE subjectTypeCd = '" + req.query.subjectTypeCd + "' "
                        + " (SELECT ISNULL(MAX(seqNumber),0)+1 FROM ApprDeclineSchedule WHERE subjectTypeCd = @subjectTypeCd "
                        + " AND subjectId = '" + req.query.selectedId + "' AND rrcNumber = '" + req.query.rrcNumber + "' "
                        + " AND declineTypeCd = '" + req.query.declineTypeCd + "' AND appraisalYear = '" + req.query.appraisalYear + "'), "
                        + "'" + data[ctr].declineYears + "', "
                        + " CAST('" + data[ctr].decline + "' AS FLOAT)/100, "
                        + "'" + data[ctr].rowUpdateUserid + "', "
                        + " GETDATE(), "
                        + " '', "
                        + " '" + req.query.appraisalYear + "'); "
                        + " SELECT SCOPE_IDENTITY() idApprDeclineSchedule; ";
                }
            }
            sql +=" DELETE FROM ApprDeclineSchedule "
                + " WHERE declineYears = '0' AND declinePercent = '0' ";
                + "     AND appraisalYear='" + req.query.appraisalYear + "' "
                + "     AND subjectId='" + req.query.selectedId + "' "
                + "     AND rrcNumber='" + req.query.rrcNumber + "' "
                // + "     AND subjectTypeCd='" + req.query.subjectTypeCd + "' "
                + "     AND subjectTypeCd=@subjectTypeCd "
                + "     AND declineTypeCd='" + req.query.declineTypeCd + "' ";
                
            sql +=" Delete From CalcDeclineSchedule "
                + " Where queueUserid = '"+queueUserid+"' "
                
                + " Insert Into CalcDeclineSchedule "
                + " SELECT '"+queueUserid+"' "
                + "     ,a.appraisalYear "
                + "     ,a.subjectTypeCd "
                + "     ,a.subjectId "
                + "     ,a.rrcNumber "
                + "     ,a.declineTypeCd "
                + "     ,a.seqNumber "
                + "     ,a.declineYears "
                + "     ,a.declinePercent "
                + " FROM dbo.ApprDeclineSchedule a "
                + " Where a.rowDeleteFlag <> 'D' "
                + "     And exists("
                + "         Select 1 From CalcAppraisalSummary c "
                + "         Where c.queueUserid = '"+queueUserid+"' "
                + "             And c.appraisalYear = a.appraisalYear "
                + "             And c.subjectTypeCd = a.subjectTypeCd "
                + "             And c.subjectId = a.subjectId "
                + "             And c.rrcNumber = a.rrcNumber"
                + "         ) ";

        }
        // res.send(sql);
        UtilFunctions.execSql(sql, res)
    }, //end ReadLease
};
