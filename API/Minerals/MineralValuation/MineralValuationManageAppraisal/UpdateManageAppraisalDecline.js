module.exports = {
    UpdateManageAppraisalDecline: function (req, res) {
        //console.log("Called ReadLease")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = "";
         var queueUserid = req.query.queueUserid;
        if (req.query.appraisalYear && req.query.selectedId && req.query.rrcNumber
                && req.query.declineTypeCd) {

            var data = req.body;
            if (req.query.updateLength == 1 && !Array.isArray(data)) {
                data = [req.body];
            }

            sql = "DECLARE @subjectTypeCd varchar(5) = '2', @cnt int "
                   +"SELECT @cnt = count(1) FROM Unit where unitId = '" + req.query.selectedId + "'"
                   +"IF @cnt > 0 BEGIN SET @subjectTypeCd = '1' END "

                + " IF OBJECT_ID(N'tempdb..#tempIDs', N'U') IS NOT NULL "
                + " DROP TABLE #tempIDs "

                + " CREATE TABLE #tempIDs (idApprDeclineSchedule int NULL); "


            for (var ctr = 0; ctr < data.length; ctr++) {
                var insertSql =  " INSERT INTO ApprDeclineSchedule "
                            + " (subjectTypeCd, subjectId, rrcNumber, declineTypeCd, seqNumber, declineYears, declinePercent, rowUpdateUserId, rowUpdateDt, rowDeleteFlag, appraisalYear)"
                            + " VALUES "
                            + " ( @subjectTypeCd, "
                            // + " '" + req.query.subjectTypeCd + "', "//
                            + "     '" + req.query.selectedId + "', "
                            + "     '" + req.query.rrcNumber + "', "
                            + "     '" + req.query.declineTypeCd + "', "
                            // + "     (SELECT ISNULL(MAX(seqNumber),0)+1 FROM ApprDeclineSchedule WHERE subjectTypeCd = '" + req.query.subjectTypeCd + "' "//
                            + "     (SELECT ISNULL(MAX(seqNumber),0)+1 FROM ApprDeclineSchedule WHERE subjectTypeCd = @subjectTypeCd "
                            + "     AND subjectId = '" + req.query.selectedId + "' AND rrcNumber = '" + req.query.rrcNumber + "' "
                            + "     AND declineTypeCd = '" + req.query.declineTypeCd + "' AND appraisalYear = '" + req.query.appraisalYear + "'), "
                            + "     '" + data[ctr].declineYears + "', "
                            + "     CAST('" + data[ctr].decline + "' AS FLOAT)/100, "
                            + "     '" + data[ctr].rowUpdateUserid + "', "
                            + "     GETDATE(), "
                            + "     '', "
                            + "     '" + req.query.appraisalYear + "'"
                            + " ) "
                            + " INSERT INTO #tempIDs(idApprDeclineSchedule) VALUES (SCOPE_IDENTITY()) ";
                
                if(data[ctr].idApprDeclineSchedule){
                    sql +=" "
                        + " IF EXISTS( SELECT 1 FROM ApprDeclineSchedule WHERE idApprDeclineSchedule='" + data[ctr].idApprDeclineSchedule + "' ) "
                        + " BEGIN "
                        + "     UPDATE ApprDeclineSchedule SET "
                        + "         declineYears='" + data[ctr].declineYears + "', "
                        + "         declinePercent=CAST('" + data[ctr].decline + "' AS FLOAT)/100, "
                        + "         rowDeleteFlag='" + data[ctr].rowDeleteFlag + "', "
                        + "         rowUpdateDt = GETDATE(), "
                        + "         rowUpdateUserid = '" + data[ctr].rowUpdateUserid + "' "
                        + "     WHERE appraisalYear='" + req.query.appraisalYear + "' "
                        + "         AND idApprDeclineSchedule='" + data[ctr].idApprDeclineSchedule + "' "
                        + "         AND subjectId='" + req.query.selectedId + "' "
                        + "         AND rrcNumber='" + req.query.rrcNumber + "' "
                        // + "         AND subjectTypeCd='" + req.query.subjectTypeCd + "' "
                        + "         AND subjectTypeCd=@subjectTypeCd "
                        + "         AND declineTypeCd='" + req.query.declineTypeCd + "' "
                        + "         AND rowDeleteFlag='' "
                        + " END "
                        + " ELSE "
                        + " BEGIN "
                        +       insertSql
                        + " END "
                }else if (data[ctr].oriDecline != '0' || data[ctr].oriDeclineYears != '0'){
                    sql += "UPDATE ApprDeclineSchedule SET "
                            + "declineYears='" + data[ctr].declineYears + "', "
                            + "declinePercent=CAST('" + data[ctr].decline + "' AS FLOAT)/100, "
                            + "rowDeleteFlag='" + data[ctr].rowDeleteFlag + "', "
                            + "rowUpdateDt = GETDATE(), "
                            + "rowUpdateUserid = '" + data[ctr].rowUpdateUserid + "' "
                            + "WHERE appraisalYear='" + req.query.appraisalYear + "' "
                            + "AND declineYears='" + data[ctr].oriDeclineYears + "' "
                            + "AND declinePercent=CAST('" + data[ctr].oriDecline + "' AS FLOAT)/100 "
                            + "AND subjectId='" + req.query.selectedId + "' "
                            + "AND rrcNumber='" + req.query.rrcNumber + "' "
                           // + "AND subjectTypeCd='" + req.query.subjectTypeCd + "' "
                            + "AND subjectTypeCd=@subjectTypeCd "
                            + "AND declineTypeCd='" + req.query.declineTypeCd + "' "
                            + "AND rowDeleteFlag='' "   
                }else{
                    sql += insertSql;
                }
            }
            sql +=" DELETE FROM ApprDeclineSchedule "
                + " WHERE declineYears = '0' AND declinePercent = '0' ";
                + "     AND appraisalYear='" + req.query.appraisalYear + "' "
                + "     AND subjectId='" + req.query.selectedId + "' "
                + "     AND rrcNumber='" + req.query.rrcNumber + "' "
                // + "     AND subjectTypeCd='" + req.query.subjectTypeCd + "' "
                + "     AND subjectTypeCd=@subjectTypeCd "
                + "     AND declineTypeCd='" + req.query.declineTypeCd + "' "
            
            // sql +=" DECLARE @declineType int = 1; "
            //     + " DECLARE @declineCount int; "
            //     + " WHILE @declineType < 5 "
            //     + " BEGIN "
            //     + "     SELECT @declineCount = COUNT(*) "
            //     + "     FROM ApprDeclineSchedule "
            //     + "     WHERE appraisalYear = '" + req.query.appraisalYear + "' "
            //     + "         AND subjectId = '" + req.query.selectedId + "' "
            //     + "         AND rrcNumber = '" + req.query.rrcNumber + "' "
            //     + "         AND subjectTypeCd = @subjectTypeCd "
            //     + "         AND declineTypeCd = @declineType "
            //     + "     IF( @declineCount = 0) "
            //     + "     BEGIN "
            //     + "         INSERT INTO ApprDeclineSchedule "
            //     + "         (subjectTypeCd, subjectId, rrcNumber, declineTypeCd, seqNumber, declineYears, declinePercent, rowUpdateUserId, rowUpdateDt, rowDeleteFlag, appraisalYear)"
            //     + "         VALUES "
            //     + "         (@subjectTypeCd, '" + req.query.selectedId + "', '" + req.query.rrcNumber + "', @declineType, 1, '0', 0, '" + queueUserid + "', GETDATE(), '', '" + req.query.appraisalYear + "') "
            //     + "     END; "
            //     + "     SET @declineType = @declineType + 1 ; "
            //     + " END; "
            sql +=" Delete From CalcDeclineSchedule "
                + " Where queueUserid = '"+queueUserid+"' "
                
                + " Insert Into CalcDeclineSchedule "
                + " SELECT '"+queueUserid+"' "
                + "     ,a.appraisalYear "
                + "     ,a.subjectTypeCd "
                + "     ,a.subjectId "
                + "     ,a.rrcNumber "
                + "     ,a.declineTypeCd "
                + "     ,a.seqNumber "
                + "     ,a.declineYears "
                + "     ,a.declinePercent "
                + " FROM dbo.ApprDeclineSchedule a "
                + " Where a.rowDeleteFlag <> 'D' "
                + " And exists ( "
                + "     Select 1 From CalcAppraisalSummary c "
                + "     Where c.queueUserid = '"+queueUserid+"' "
                + "         And c.appraisalYear = a.appraisalYear "
                + "         And c.subjectTypeCd = a.subjectTypeCd "
                + "         And c.subjectId = a.subjectId "
                + "         And c.rrcNumber = a.rrcNumber"
                + " ) ";
            sql +=" SELECT * FROM #tempIDs ";
    
        }

        UtilFunctions.execSql(sql, res)
    }, //end ReadLease
};
