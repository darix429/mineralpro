module.exports = {
    Read: function (req, res) {
        var UtilFunctions = require('../../UtilFunctions.js');
        var sql = "DECLARE @appraisalYear varchar(10) = '"+req.query.appraisalYear+"' "
                +"SELECT "
                +"code wellTypeCd, "
                +"description wellTypeDesc "
                +"FROM SystemCode WHERE codeType = 3140 "
                +"AND appraisalYear = @appraisalYear "
                +"AND activeInd = 'Y' "
                +"AND rowDeleteFlag = ''";                       
        UtilFunctions.execSql(sql, res)
    },
};
