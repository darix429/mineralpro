
var xls = require('excel');
var path = require('path');
var formidable = require('formidable');
var xlstojson = require("xls-to-json-lc");
var xlsxtojson = require("xlsx-to-json-lc");
var excelExport = require('node-excel-export');
var fs = require("fs");
module.exports = {

    execSqlCallBack: function (sql_query, res,callback) {
        var SystemConfig = require('./SystemConfig.js'); 
        var mssql = require('mssql');   

        var conn = new mssql.Connection(SystemConfig.dbConfig);
                
        conn.connect().then(function () {
            var req = new mssql.Request(conn);
            req.query(sql_query).then(function (recordset) {;
                conn.close();                 
                callback(recordset)
            })
            .catch(function (err) {
                conn.close();
                callback(res.status(200).json(err))  
            });        
        })
        .catch(function (err) {
            callback(res.status(200).json(err))  
        });
    },
    
    execSql: function (sql_query, res) {
        var SystemConfig = require('./SystemConfig.js'); 
        var mssql = require('mssql');   

        var conn = new mssql.Connection(SystemConfig.dbConfig);

        conn.connect().then(function () {
            var req = new mssql.Request(conn);
            req.query(sql_query).then(function (recordset) {;
                conn.close();
                res.send("{'success': true, 'data': " + JSON.stringify(recordset) + "}")
            })
            .catch(function (err) {
                //console.log(err);
                conn.close();
                res.status(500).json({"success":false,"data": err});
            });        
        })
        .catch(function (err) {
            //console.log(err);
            res.status(500).json({"success":false,"data": err});
        });
    },
    
        execSqlColDef: function (sql_query, columnField, res) {
        var SystemConfig = require('./SystemConfig.js'); 
        var mssql = require('mssql');   

        var conn = new mssql.Connection(SystemConfig.dbConfig);

        conn.connect().then(function () {
            var req = new mssql.Request(conn);
            req.query(sql_query).then(function (recordset) {;
                conn.close();
                res.send("{'success': true, 'data': " + JSON.stringify(recordset) + ", 'columnField': '"+columnField+"'}")
            })
            .catch(function (err) {
                //console.log(err);
                conn.close();
                res.status(500).json({"success":false,"data": err});
            });        
        })
        .catch(function (err) {
            //console.log(err);
            res.status(500).json({"success":false,"data": err});
        });
    },
    
    execSqlLogin: function (e_id, raw_p, appraisalYear, res) {
		//console.log("Called Login");
        var SystemConfig = require('./SystemConfig.js'); 
        var mssql = require('mssql');   
		var bcrypt = require('bcrypt-nodejs');
		
		var ErMsg = "{'success': true, "
			+" 'data': [{'0':'1', "
			+" 'id_user':'1','1':'', 'user_name':'', "
			+" '2':'','password':'', "
			+" '3':'','email_address':'', "
			+" 'comment':'Invalid Email Address/Password','4':'Invalid Email Address/Password' , "
                        +" 'appraisalYear':'"+appraisalYear+"','4':'"+appraisalYear+"'}]}";

        var conn = new mssql.Connection(SystemConfig.dbConfig);
        conn.connect().then(function () {
            var req = new mssql.Request(conn);
			var fetch_user = "select emailAddress, password from AppUser where emailAddress = '"+e_id+"' and NOT rowDeleteFlag = 'D'";
            req.query(fetch_user).then(function (user_rec) {
				if(user_rec.length == 1){
					var user = JSON.parse(JSON.stringify(user_rec[0]));
					bcrypt.compare(raw_p,user.password,function(er,equal){
						if(!er){
							if(equal){
								//console.log("Login Success");
								var sql = "DECLARE @emailAddress varchar(100), "
										+" @password varchar(100), "
										+" @sql varchar(2500) "                    
										+" SET @emailAddress = '"+e_id+"' "
										+" SET @sql = 'SELECT "
										+"        au.idAppUser, "
										+"        o.idOrganization, "
										+"        o.organizationName, "
										+"        sg.idSecurityGroup, "
										+"        sg.groupName securityGroup, "
										+"        oc.nameAbbreviation, "								
										+"        au.emailAddress, "
										+"        ''Authenticated'' comment, "
                                        +"        ''"+appraisalYear+"'' appraisalYear "
										+"    FROM dbo.AppUser au "
										+"    INNER JOIN dbo.UserOrganization uo ON au.idAppUser = uo.idAppUser AND uo.rowDeleteFlag = '''' "
										+"    INNER JOIN dbo.Organization o ON uo.idOrganization = o.idOrganization AND o.rowDeleteFlag = '''' "
                                        +"    INNER JOIN dbo.OrganizationContact oc ON oc.idOrganizationContact = au.idOrganizationContact AND oc.rowDeleteFlag = '''' "
										+"    INNER JOIN dbo.UserOrgSecurity uos ON uo.idUserOrganization = uos.idUserOrganization AND uos.rowDeleteFlag = '''' "
										+"    INNER JOIN dbo.SecurityGroup sg ON uos.idSecurityGroup = sg.idSecurityGroup AND sg.rowDeleteFlag = '''' "
										+"    WHERE au.rowDeleteFlag = '''' "
										+"        AND au.emailAddress = ''' + @emailAddress +''''  "
										+"     EXEC (@sql) ;";   
								req.query(sql).then(function (recordset) {
									conn.close();
									if(recordset.length > 0){
										res.status(200).json({"success":true,"data":JSON.parse(JSON.stringify(recordset))});
									}else{
										res.send(ErMsg);
									}               
								})
								.catch(function (err) {
									//console.log(err);
									conn.close();
									res.status(500).json({"success":false,"data":"Database Error"});
								});       
							}else{
								conn.close();
								res.send(ErMsg);
							}
						}else{
							conn.close();
							res.send(ErMsg);
						}
					});
				}else{
					conn.close();
					res.send(ErMsg);
				}
			})
			.catch(function(er){
				//console.log(err);
				conn.close();
				res.status(500).json({"success":false,"data":er});
			});
            
        })
        .catch(function (err) {
            //console.log(err);
            res.status(500).json({"success":false,"data":err});
        });
    },
    
    getTreeData: function (sql_query, res) {
        var SystemConfig = require('./SystemConfig.js'); 
        var mssql = require('mssql');   

        var conn = new mssql.Connection(SystemConfig.dbConfig);

        conn.connect().then(function () {
            var req = new mssql.Request(conn);
            req.query(sql_query).then(function (recordset) {;
                conn.close();
                
                if(recordset.length > 0){
                    var outputTree = '';
                    var newNode = true;  
                    for(var a=0; a<recordset.length; a++){
                        var securityCd = recordset[a].securityCd;
                        var appName = recordset[a].appName;
                        var idSubSystemFunction = recordset[a].idSubSystemFunction;
                        var subSystemName = recordset[a].subSystemName;
                        var subSystemNameDisplay = recordset[a].subSystemNameDisplay;
                        var functionName = recordset[a].functionName;
                        var functionNameDisplay = recordset[a].functionNameDisplay;

                        if(newNode){
                            outputTree += "{'lockedInd':'"+securityCd+"', 'idSubSystemFunction':'"+idSubSystemFunction+"', 'text':'"+subSystemNameDisplay+"', 'expanded': true, 'leaf': false, 'children': ["
                            if (securityCd == 2) {
                                outputTree += "{'lockedInd':'"+securityCd+"', 'idSubSystemFunction':'"+idSubSystemFunction+"', 'text':'"+functionNameDisplay+"', 'expanded': false, 'leaf': true, 'icon':'..\/resources\/icons\/pencil.png', 'subSystemName': '"+subSystemName+"', 'functionName': '"+functionName+"', 'appName': '"+appName+"'}";                        
                            } else if (securityCd == 1) {
                                outputTree += "{'lockedInd':'"+securityCd+"', 'idSubSystemFunction':'"+idSubSystemFunction+"', 'text':'"+functionNameDisplay+"', 'expanded': false, 'leaf': true, 'icon':'..\/resources\/icons\/readOnly.png', 'subSystemName': '"+subSystemName+"', 'functionName': '"+functionName+"', 'appName': '"+appName+"'}";                        
                            } else {
                                outputTree += "{'lockedInd':'"+securityCd+"', 'idSubSystemFunction':'"+idSubSystemFunction+"', 'text':'"+functionNameDisplay+"', 'expanded': false, 'leaf': true, 'icon':'..\/resources\/icons\/delete.png', 'subSystemName': '"+subSystemName+"', 'functionName': '"+functionName+"', 'appName': '"+appName+"'}";                        
                            }                        
                        }else{
                            if (securityCd == 2) {
                                outputTree += ",{'lockedInd':'"+securityCd+"', 'idSubSystemFunction':'"+idSubSystemFunction+"', 'text':'"+functionNameDisplay+"', 'expanded': false, 'leaf': true, 'icon':'..\/resources\/icons\/pencil.png', 'subSystemName': '"+subSystemName+"', 'functionName': '"+functionName+"', 'appName': '"+appName+"'}";                        
                            } else if (securityCd == 1) {
                                outputTree += ",{'lockedInd':'"+securityCd+"', 'idSubSystemFunction':'"+idSubSystemFunction+"', 'text':'"+functionNameDisplay+"', 'expanded': false, 'leaf': true, 'icon':'..\/resources\/icons\/readOnly.png', 'subSystemName': '"+subSystemName+"', 'functionName': '"+functionName+"', 'appName': '"+appName+"'}";                        
                            } else {
                                outputTree += ",{'lockedInd':'"+securityCd+"', 'idSubSystemFunction':'"+idSubSystemFunction+"', 'text':'"+functionNameDisplay+"', 'expanded': false, 'leaf': true, 'icon':'..\/resources\/icons\/delete.png', 'subSystemName': '"+subSystemName+"', 'functionName': '"+functionName+"', 'appName': '"+appName+"'}";                        
                            }
                        } 

                        if (recordset[a+1] && subSystemNameDisplay.toUpperCase() == recordset[a+1].subSystemNameDisplay.toUpperCase()){
                            newNode = false;
                        }else{
                            if(recordset[a+1]){
                                outputTree += ']},'
                            }else{
                                outputTree += ']}'
                            }                      
                            newNode = true;
                        }
                    }
                    res.send("{'success': true, 'children': [" + outputTree + "]}")         
                }else{
                    res.send("{'success': true}")
                }        
            })
            .catch(function (err) {
                //console.log(err);
                conn.close();
                callback(res, err);
            });        
        })
        .catch(function (err) {
            //console.log(err);
            callback(res, err);
        });
    },
//READ EXCEL
    uploadExcel: function(req, res) {
        //console.log("upload excel")
        var uploadDir = path.join(__dirname, '/uploadfiles');
        var form = new formidable.IncomingForm()
        form.multiples = true
        form.keepExtensions = true
        var Filepath = ''
        
        form.parse(req, (err, fields, files) => {
        if (err) return res.status(500).json({ error: err })
            readExcel(Filepath,res)
        })
        form.on('fileBegin', function (name, file) {
            const [fileName, fileExt] = file.name.split('.')
            //console.log(file.name,fileName,fileExt)
            file.path = path.join(uploadDir, file.name)
            Filepath = file.path
        })
    },
//DOWNLOAD EXCEL
    downloadExcel: function(req, res) {
        var styles = {headerDark: {
            fill: {fgColor: {rgb: '0E1717'}},
            font: {color: {rgb: 'FFFFFFFF'},
            sz: 12,regular: true,}
            },
            headerLight: {
                fill: {
                    fgColor: {
                        rgb: 'FFFFFFFF'
                    }
                },
                font: {
                    color: {
                        rgb: 'FF000000'
                    },
                    sz: 12,
                    bold: true,
                    underline: false
                }
            },
            cellBlueL: {
                fill: {
                    fgColor: {
                        rgb: 'C4DFE6'
                    }
                },
                alignment: {
                    horizontal: 'left'
                }
            },
            cellWhiteL: {
                fill: {
                    fgColor: {
                        rgb: 'FFFFFFFF'
                    }
                },
                alignment: {
                    horizontal: 'left'
                }
            },
            cellBlueR: {
                fill: {
                    fgColor: {
                        rgb: 'C4DFE6'
                    }
                },
                alignment: {
                    horizontal: 'right'
                }
            },
            cellWhiteR: {
                fill: {
                    fgColor: {
                        rgb: 'FFFFFFFF'
                    }
                },
                alignment: {
                    horizontal: 'right'
                }
            },
            cellBlue: {fill: {fgColor: {rgb: 'C4DFE6'}}},
            cellWhite: {fill: {fgColor: {rgb: 'FFFFFFFF'}}}
        };

        var heading = [];
        var dataset = [];
        var merges = [];
        // var contents = fs.readFileSync("sample.json");
        //console.log(req.query.data)
        //var jsonContent = JSON.parse(req.query.data);
        var jsonContent = JSON.parse(req.body.data);
        var jsonFilename = req.body.fileName;
        var ExportType = req.body.exportType;
        if(ExportType == 'ExportDO'){
            
            
            var specification = {
                Owner_ID: {
                displayName: 'Owner_ID',
                headerStyle: styles.headerDark,
                
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlueL : styles.cellWhiteL;
                },
                width: 100
                },Owner_Name: {
                displayName: 'Owner_Name',
                headerStyle: styles.headerDark,
                
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlue : styles.cellWhite;
                },
                width: 220
                },Owner_Name2: {
                displayName: 'Owner_Name2',
                headerStyle: styles.headerDark,
                
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlue : styles.cellWhite;
                },
                width: 220
                },Owner_Mailing1: {
                displayName: 'Owner_Mailing1',
                headerStyle: styles.headerDark,
                
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlue : styles.cellWhite;
                },
                width: 220
                },Owner_Mailing2: {
                displayName: 'Owner_Mailing2',
                headerStyle: styles.headerDark,
                
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlue : styles.cellWhite;
                },
                width: 220
                },Owner_City: {
                displayName: 'Owner_City',
                headerStyle: styles.headerDark,
                
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlue : styles.cellWhite;
                },
                width: 220
                },Owner_ST: {
                displayName: 'Owner_ST',
                headerStyle: styles.headerDark,
                
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlue : styles.cellWhite;
                },
                width: 120
                },Owner_Zip: {
                displayName: 'Owner_Zip',
                headerStyle: styles.headerDark,
                
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlue : styles.cellWhite;
                },
                width: 120
                },Interest_Type: {
                displayName: 'Interest_Type',
                headerStyle: styles.headerDark,
                
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlue : styles.cellWhite;
                },
                width: 220
                },Interest_Percent: {
                displayName: 'Interest_Percent',
                headerStyle: styles.headerDark,
                
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlueL : styles.cellWhiteL;
                },
                width: 120
                }
            }
            
            for (var i = 0 ; i < jsonContent.data.length; i++) {
                //console.log(jsonContent.data[i].Owner_Id)
                var interestPercent = jsonContent.data[i].Interest_Percent;
                dataset[i] =  {
                    status_id: i,
                    Owner_ID:jsonContent.data[i].Owner_Id,
                    Owner_Name: jsonContent.data[i].Owner_Name,
                    Owner_Name2: jsonContent.data[i].Owner_Name2,
                    Owner_Mailing1: jsonContent.data[i].Owner_Mailing1,
                    Owner_Mailing2: jsonContent.data[i].Owner_Mailing2,
                    Owner_City: jsonContent.data[i].Owner_City,
                    Owner_ST: jsonContent.data[i].Owner_ST,
                    Owner_Zip: jsonContent.data[i].Owner_Zip,
                    Interest_Type: jsonContent.data[i].Interest_Type,
                    Interest_Percent: parseFloat(interestPercent).toFixed(8)*1,
                }
            };   
            var report = excelExport.buildExport([ 
                    {
                    name: 'Lease Owner', // <- Specify sheet name (optional) 
                    heading: heading, // <- Raw heading array (optional) 
                    merges: merges, // <- Merge cell ranges 
                    specification: specification, // <- Report specification 
                    data: dataset // <-- Report data 
                    }
                ]
            );
            res.attachment(''+jsonFilename+'.xlsx'); 
            return res.send(report);
        } else if(ExportType == 'ExportLeaseHoldings'){
            var headerName = req.body.headerName;
            heading = [
                [{value: headerName, style: styles.headerLight}]// <-- It can be only values 
            ];

            merges = [
                { start: { row: 1, column: 1 }, end: { row: 1, column: 7 } }
            ]

            var specification = {
                Lease_ID: {
                displayName: 'Lease_ID',
                headerStyle: styles.headerDark,
                
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlueL : styles.cellWhiteL;
                },
                width: 100
                },Lease_Name: {
                displayName: 'Lease_Name',
                headerStyle: styles.headerDark,
                
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlue : styles.cellWhite;
                },
                width: 220
                },Operator_ID: {
                displayName: 'Operator_ID',
                headerStyle: styles.headerDark,
                    
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlueL : styles.cellWhiteL;
                },
                width: 100
                },Operator: {
                displayName: 'Operator',
                headerStyle: styles.headerDark,
                        
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlue : styles.cellWhite;
                },
                width: 220
                },Interest_Type: {
                displayName: 'Interest_Type',
                headerStyle: styles.headerDark,
                
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlue : styles.cellWhite;
                },
                width: 220
                },Interest_Percent: {
                displayName: 'Interest_Percent',
                headerStyle: styles.headerDark,
                
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlueL : styles.cellWhiteL;
                },
                width: 100
                },Value: {
                displayName: 'Value',
                headerStyle: styles.headerDark,
                    
                cellStyle: function(value, row) {
                return ((row.status_id%2) == 0) ? styles.cellBlueR : styles.cellWhiteR;
                },
                width: 120
                }
            }
            
            for (var i = 0 ; i < jsonContent.data.length; i++) {
                //console.log(jsonContent.data[i].Owner_Id)
                var interestPercent = jsonContent.data[i].Interest_Percent;
                dataset[i] =  {
                    status_id: i,
                    Lease_ID:jsonContent.data[i].Lease_Id,
                    Lease_Name: jsonContent.data[i].Lease_Name,
                    Operator_ID: jsonContent.data[i].Operator_ID,
                    Operator: jsonContent.data[i].Operator,
                    Interest_Type: jsonContent.data[i].Interest_Type,
                    Interest_Percent: parseFloat(interestPercent).toFixed(8)*1,
                    Value: '$'+jsonContent.data[i].Value,
                }
            };   
            var report = excelExport.buildExport([ 
                    {
                    name: 'Owner', // <- Specify sheet name (optional) 
                    heading: heading, // <- Raw heading array (optional) 
                    merges: merges, // <- Merge cell ranges 
                    specification: specification, // <- Report specification 
                    data: dataset // <-- Report data 
                    }
                ]
            );
            res.attachment(''+jsonFilename+'.xlsx'); 
            return res.send(report);
        } else {
            return 'Contact Developer for this issue to be resolve.'
        }
    }

    
};

    function readExcel (file,res) {
        //console.log("read excel")
        var exceltojson;
      function convertToJSON(array) {
          var first = array[0].join()
          var headers = first.split(',');
          
          var jsonData = [];
          for ( var i = 1, length = array.length; i < length; i++ )
          {
           
            var myRow = array[i].join();
            var row = myRow.split(',');
            
            var data = {};
            for ( var x = 0; x < row.length; x++ )
            {
              data[headers[x]] = row[x];
            }
            jsonData.push(data);
         
          }
          return jsonData;
      };

        if(file.split('.')[file.split('.').length-1] === 'xlsx'){
            exceltojson = xlsxtojson;
        } else {
            exceltojson = xlstojson;
        }
        
        try {
        exceltojson({
            input: file,
            output: null, //since we don't need output.json
            lowerCaseHeaders:true
        }, function(err,result){
            var valid = true;
            var formatTxt = [ 'Owner_ID','Owner_Name','Owner_Name2','Owner_Mailing1','Owner_Mailing2','Owner_City','Owner_ST','Owner_Zip','Interest_Type','Interest_Percent' ]
            var format = [ result[0].Owner_ID,result[0].Owner_Name,result[0].Owner_Name2,result[0].Owner_Mailing1,result[0].Owner_Mailing2,result[0].Owner_City,result[0].Owner_ST,result[0].Owner_Zip,result[0].Interest_Type,result[0].Interest_Percent ]
            //console.log(result[0])
            if(err) {
                 res.send("{'success': false, data:"+err+"")
                 fs.unlink(file);
            } 
            for (var i = format.length - 1; i >= 0; i--) {
                //console.log(formatTxt[i],format[i])
                if(format[i] == undefined){
                    valid = false
                    break
                }
            };
            //console.log(valid)
                if (valid) {res.send("{'success': true, data:"+JSON.stringify(result)+"}")}
                else {res.send("{'success': false, 'data': Invalid Format}");fs.unlink(file);};                
            });
        } catch (e){
             res.send("{'success': false, data:Corupted excel file}")
             fs.unlink(file);
        }
    }
