
module.exports = {
    createPDF: function (req, res) {

        var UtilFunctions = require('../UtilFunctions.js');
        var fileName = req.query.fileName
        var sql = 'SELECT TOP 1 * FROM App'
        
        UtilFunctions.execSqlCallBack(sql, res, function (record) {
//            for (var i = 0; i < record.length; i++) {
//                txt += record[i].outputField + "\n";
//            }


            var phantom = require('phantom');
            var fs = require('fs');
            var time = new Date().getTime();
            var htmlPath = 'JEReport/JE_' + time + '.html';
            var pdfPath = 'JEReport/pdf/JE_' + time + '.pdf';
            var hostReportUrl = req.protocol + '://' + req.get('host') + '/' + htmlPath
            var html = '\n\
<!DOCTYPE html> \n\
<html> \n\
<head> \n\
	<meta charset="utf-8"> \n\
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> \n\
	<meta name="viewport" content="width=device-width, initial-scale=1"> \n\
	<title>Ector County Appraisal District</title> \n\
	<link rel="stylesheet" type="text/css" href="style.css"> \n\
	<!-- Bootstrap --> \n\
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"> \n\
</head> \n\
<body> \n\
<body> \n\
	<div class="container" id="content"> \n\
		<div class=""> \n\
			<div class="col-md-3  col-sm-3 col-xs-3"> \n\
				<img src="logo.jpg" alt="logo" class="logo"> \n\
			</div> \n\
			<div class="col-md-6 col-sm-6 col-xs-6 text-center"> \n\
				<h1>Ector County Appraisal District</h1> \n\
				<h4><strong>Mineral Correction / Journal Entry</strong></h4> \n\
				<h4><strong>8/18/2017</strong></h4> \n\
			</div> \n\
		</div> \n\
		<div class="row"> \n\
			<div class="col-md-6 col-sm-6 col-xs-6 top"> \n\
				<p>The Ector County Appraisal Dist. Appraisal Roll for the year indicated below is corrected, supplemented, and certified in accordance with the Texas State Property Tax Code.</p><br> \n\
				<dl class="dl-horizontal dltop"> \n\
					<dt>Correction Date:</dt> \n\
					<dd>02/03/17</dd> \n\
					<dt>Correction By:</dt> \n\
					<dd>Ann Beaver</dd> \n\
					<dt>Correction Reason:</dt> \n\
					<dd>25.25(b) Clerical</dd> \n\
					<dt>Tax Office Notify:</dt> \n\
					<dd></dd><br> \n\
					<dt>Tax Year:</dt> \n\
					<dd>2016</dd> \n\
					<dt>Lease ID:</dt> \n\
					<dd>43380</dd> \n\
					<dt>Owner ID:</dt> \n\
					<dd>701175</dd> \n\
					<dt>Interest Type:</dt> \n\
					<dd>RI</dd><br> \n\
				</dl> \n\
			</div> \n\
			<div class="col-md-6 col-sm-6 col-xs-6"> \n\
				<div class="horizontal-dl topdt"> \n\
					<dl class="dl-horizontal"> \n\
						<dt>Approved:<hr></dt><br> \n\
						<dd class="line"><strong>Asst. Chief Appraiser Initial</strong></dd> \n\
						<dt>Date:<hr></dt><br> \n\
						<dd class="line"></dd> \n\
						<dt>Certified:<hr></dt><br> \n\
						<dd class="line"><strong>Chief Appraiser Initial</strong></dd> \n\
						<dt>Date:<hr></dt><br> \n\
						<dd class="line"></dd> \n\
					</dl> \n\
				</div> \n\
			</div> \n\
		</div> \n\
		<div class="row"> \n\
			<div class="col-md-12 col-sm-12"> \n\
				<div class="row col-md-12 col-sm-12 col-xs-12"> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<p><strong>BEFORE JOURNAL ENTRY</strong></p> \n\
						<div class="horizontal-dl dltop"> \n\
							<dl class="dl-horizontal"> \n\
								<dt>Owner Name:</dt> \n\
								<dd>HELMERICH W H GRCHILDR 1980</dd> \n\
								<dt>In Care Of:</dt> \n\
								<dd>TRANSFERRED</dd> \n\
								<dt>Mailing Address:</dt> \n\
								<dd>5440 HARVEST HILL RD STE 232 DALLAS, TX 75230-6440</dd> \n\
							</dl> \n\
						</div> \n\
					</div> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<p><strong>AFTER JOURNAL ENTRY</strong></p> \n\
						<div class="horizontal-dl dltopempty"> \n\
							<dl class="dl-horizontal"> \n\
								<dt>Owner Name:</dt> \n\
								<dd></dd> \n\
								<dt>In Care Of:</dt> \n\
								<dd></dd> \n\
								<dt>Mailing Address:</dt> \n\
								<dd></dd> \n\
							</dl> \n\
						</div> \n\
					</div> \n\
				</div> \n\
				<div class="row col-md-12 col-sm-12 col-xs-12"> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<div class="horizontal-dl dltop"> \n\
							<dl class="dl-horizontal"> \n\
								<dt>Agent:</dt> \n\
								<dd>TAX MANAGEMENT GROUP</dd> \n\
							</dl> \n\
						</div> \n\
					</div> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<div class="horizontal-dl dltop"> \n\
							<dl class="dl-horizontal"> \n\
								<dt>Agent:</dt> \n\
								<dd></dd> \n\
							</dl> \n\
						</div> \n\
					</div> \n\
				</div> \n\
				<div class="row col-md-12 col-sm-12 col-xs-12"> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<div class="horizontal-dl dltop"> \n\
							<dl class="dl-horizontal"> \n\
								<dt>Interest Pct:</dt> \n\
								<dd>0.000325</dd> \n\
								<dt>Market Value:</dt> \n\
								<dd>2736</dd> \n\
								<dt>Exemptions:</dt> \n\
								<dd></dd><br> \n\
							</dl> \n\
						</div> \n\
					</div> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<div class="horizontal-dl dltop"> \n\
							<dl class="dl-horizontal"> \n\
								<dt>Interest Pct:</dt> \n\
								<dd>0</dd> \n\
								<dt>Market Value:</dt> \n\
								<dd>0</dd> \n\
								<dt>Exemptions:</dt> \n\
								<dd></dd><br> \n\
							</dl> \n\
						</div> \n\
					</div> \n\
				</div> \n\
		</div> \n\
		<div class="row col-md-12 col-sm-12 col-xs-12"> \n\
			<div class="col-md-6 col-sm-6 col-xs-6"> \n\
				<table class="table table-bordered"> \n\
					<thead> \n\
				    <tr> \n\
				    	<td>Tax Unit</td> \n\
				        <td>Exempt Amt</td> \n\
				        <td>Taxable Value</td> \n\
				        <td>Tax Amount</td> \n\
				    </tr> \n\
					 </thead> \n\
					 <tbody> \n\
					    <tr> \n\
					      <td>ECISD</td> \n\
					      <td></td> \n\
					      <td>2,736</td> \n\
					      <td>31.47</td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td>COU</td> \n\
					      <td></td> \n\
					      <td>2,736</td> \n\
					      <td>10.12</td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td>OC</td> \n\
					      <td></td> \n\
					      <td>2,736</td> \n\
					      <td>5.60</td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td>ECHD</td> \n\
					      <td></td> \n\
					      <td>2,736</td> \n\
					      <td>2.26</td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td colspan="4"></td> \n\
					    </tr> \n\
					    <tr> \n\
					      <th class="text-center">TOTAL</th> \n\
					      <td></td> \n\
					      <td></td> \n\
					      <td>49.45</td> \n\
					    </tr> \n\
					 </tbody> \n\
				</table> \n\
			</div> \n\
			<div class="col-md-6 col-sm-6 col-xs-6"> \n\
				<table class="table table-bordered"> \n\
					<thead> \n\
				    <tr> \n\
				    	<td>Tax Unit</td> \n\
				        <td>Exempt Amt</td> \n\
				        <td>Taxable Value</td> \n\
				        <td>Tax Amount</td> \n\
				    </tr> \n\
					 </thead> \n\
					 <tbody> \n\
					    <tr> \n\
					      <td>ECISD</td> \n\
					      <td></td> \n\
					      <td></td> \n\
					      <td></td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td>COU</td> \n\
					      <td></td> \n\
					      <td></td> \n\
					      <td></td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td>OC</td> \n\
					      <td></td> \n\
					      <td></td> \n\
					      <td></td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td>ECHD</td> \n\
					      <td></td> \n\
					      <td></td> \n\
					      <td></td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td colspan="4"></td> \n\
					    </tr> \n\
					    <tr> \n\
					      <th class="text-center">TOTAL</th> \n\
					      <td></td> \n\
					      <td></td> \n\
					      <td></td> \n\
					    </tr> \n\
					 </tbody> \n\
				</table> \n\
			</div> \n\
		</div> \n\
		<div class="row"> \n\
			<div class="col-md-12 col-sm-12"> \n\
				<div class="col-md-12 col-sm-12 col-xs-12"> \n\
					<div class="horizontal-dl dltop"> \n\
						<dl class="dl-horizontal"> \n\
							<dt>Comments:</dt> \n\
							<dd>OWNERSHIP SOLD IN 2013-PER DEED 2013-00015716. ECAD NEVER MADE TRANSFER</dd> \n\
						</dl> \n\
					</div> \n\
				</div> \n\
				<div class="col-md-12 col-sm-12"> \n\
					<div class="col-md-12 col-sm-12 col-xs-12 botp"> \n\
						<p><strong>Total Net Loss Market: </strong> (2,736)</p> \n\
						<p><strong>Total Net Loss Tax Amount: </strong> (49.45)</p><br> \n\
					</div> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<p><strong>Journal Entry ID: </strong> 123456</p> \n\
					</div> \n\
					<div class="col-md-6 col-sm-6 col-xs-6 botp"> \n\
						<p><strong>Collection Extract Date: </strong> 08/16/207</p> \n\
					</div> \n\
				</div> \n\
			</div> \n\
		</div> \n\
<!-- ---------------------------------------------Page 2--------------------------------------------- --> \n\
		<div class="container" id="content"> \n\
		<div class=""> \n\
			<div class="col-md-3  col-sm-3 col-xs-3"> \n\
				<img src="logo.jpg" alt="logo" class="logo"> \n\
			</div> \n\
			<div class="col-md-6 col-sm-6 col-xs-6 text-center"> \n\
				<h1>Ector County Appraisal District</h1> \n\
				<h4><strong>Mineral Correction / Journal Entry</strong></h4> \n\
				<h4><strong>8/18/2017</strong></h4> \n\
			</div> \n\
		</div> \n\
		<div class="row"> \n\
			<div class="col-md-6 col-sm-6 col-xs-6 top"> \n\
				<p>The Ector County Appraisal Dist. Appraisal Roll for the year indicated below is corrected, supplemented, and certified in accordance with the Texas State Property Tax Code.</p><br> \n\
				<dl class="dl-horizontal dltop"> \n\
					<dt>Correction Date:</dt> \n\
					<dd>02/03/17</dd> \n\
					<dt>Correction By:</dt> \n\
					<dd>Ann Beaver</dd> \n\
					<dt>Correction Reason:</dt> \n\
					<dd>25.25(b) Clerical</dd> \n\
					<dt>Tax Office Notify:</dt> \n\
					<dd></dd><br> \n\
					<dt>Tax Year:</dt> \n\
					<dd>2016</dd> \n\
					<dt>Lease ID:</dt> \n\
					<dd>43380</dd> \n\
					<dt>Owner ID:</dt> \n\
					<dd>701175</dd> \n\
					<dt>Interest Type:</dt> \n\
					<dd>RI</dd><br> \n\
				</dl> \n\
			</div> \n\
			<div class="col-md-6 col-sm-6 col-xs-6"> \n\
				<div class="horizontal-dl topdt"> \n\
					<dl class="dl-horizontal"> \n\
						<dt>Approved:<hr></dt><br> \n\
						<dd class="line"><strong>Asst. Chief Appraiser Initial</strong></dd> \n\
						<dt>Date:<hr></dt><br> \n\
						<dd class="line"></dd> \n\
						<dt>Certified:<hr></dt><br> \n\
						<dd class="line"><strong>Chief Appraiser Initial</strong></dd> \n\
						<dt>Date:<hr></dt><br> \n\
						<dd class="line"></dd> \n\
					</dl> \n\
				</div> \n\
			</div> \n\
		</div> \n\
		<div class="row"> \n\
			<div class="col-md-12 col-sm-12"> \n\
				<div class="row col-md-12 col-sm-12 col-xs-12"> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<p><strong>BEFORE JOURNAL ENTRY</strong></p> \n\
						<div class="horizontal-dl dltopempty"> \n\
							<dl class="dl-horizontal"> \n\
								<dt>Owner Name:</dt> \n\
								<dd></dd> \n\
								<dt>In Care Of:</dt> \n\
								<dd></dd> \n\
								<dt>Mailing Address:</dt> \n\
								<dd></dd> \n\
							</dl> \n\
						</div> \n\
					</div> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<p><strong>AFTER JOURNAL ENTRY</strong></p> \n\
						<div class="horizontal-dl dltop"> \n\
							<dl class="dl-horizontal"> \n\
								<dt>Owner Name:</dt> \n\
								<dd>SILVERADO OIL & GAS LLP</dd> \n\
								<dt class="sc">In Care Of:</dt> \n\
								<dd></dd> \n\
								<dt>Mailing Address:</dt> \n\
								<dd>PO BOX 52308 TULSA, OK 74152-0308</dd> \n\
							</dl> \n\
						</div> \n\
					</div> \n\
				</div> \n\
				<div class="row col-md-12 col-sm-12 col-xs-12"> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<div class="horizontal-dl dltop"> \n\
							<dl class="dl-horizontal"> \n\
								<dt>Agent:</dt><br> \n\
								<dd></dd> \n\
							</dl> \n\
						</div> \n\
					</div> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<div class="horizontal-dl dltop"> \n\
							<dl class="dl-horizontal"> \n\
								<dt>Agent:</dt><br> \n\
								<dd></dd> \n\
							</dl> \n\
						</div> \n\
					</div> \n\
				</div> \n\
				<div class="row col-md-12 col-sm-12 col-xs-12"> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<div class="horizontal-dl dltop"> \n\
							<dl class="dl-horizontal"> \n\
								<dt>Interest Pct:</dt> \n\
								<dd>0</dd> \n\
								<dt>Market Value:</dt> \n\
								<dd>0</dd> \n\
								<dt>Exemptions:</dt> \n\
								<dd></dd><br> \n\
							</dl> \n\
						</div> \n\
					</div> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<div class="horizontal-dl dltop"> \n\
							<dl class="dl-horizontal"> \n\
								<dt>Interest Pct:</dt> \n\
								<dd>0.000325</dd> \n\
								<dt>Market Value:</dt> \n\
								<dd>2736</dd> \n\
								<dt>Exemptions:</dt> \n\
								<dd></dd><br> \n\
							</dl> \n\
						</div> \n\
					</div> \n\
				</div> \n\
		</div> \n\
		<div class="row col-md-12 col-sm-12 col-xs-12"> \n\
			<div class="col-md-6 col-sm-6 col-xs-6"> \n\
				<table class="table table-bordered"> \n\
					<thead> \n\
				    <tr> \n\
				    	<td>Tax Unit</td> \n\
				        <td>Exempt Amt</td> \n\
				        <td>Taxable Value</td> \n\
				        <td>Tax Amount</td> \n\
				    </tr> \n\
					 </thead> \n\
					 <tbody> \n\
					    <tr> \n\
					      <td>ECISD</td> \n\
					      <td></td> \n\
					      <td></td> \n\
					      <td></td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td>COU</td> \n\
					      <td></td> \n\
					      <td></td> \n\
					      <td></td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td>OC</td> \n\
					      <td></td> \n\
					      <td></td> \n\
					      <td></td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td>ECHD</td> \n\
					      <td></td> \n\
					      <td></td> \n\
					      <td></td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td colspan="4"></td> \n\
					    </tr> \n\
					    <tr> \n\
					      <th class="text-center">TOTAL</th> \n\
					      <td></td> \n\
					      <td></td> \n\
					      <td></td> \n\
					    </tr> \n\
					 </tbody> \n\
				</table> \n\
			</div> \n\
			<div class="col-md-6 col-sm-6 col-xs-6"> \n\
				<table class="table table-bordered"> \n\
					<thead> \n\
				    <tr> \n\
				    	<td>Tax Unit</td> \n\
				        <td>Exempt Amt</td> \n\
				        <td>Taxable Value</td> \n\
				        <td>Tax Amount</td> \n\
				    </tr> \n\
					 </thead> \n\
					 <tbody> \n\
					    <tr> \n\
					      <td>ECISD</td> \n\
					      <td></td> \n\
					      <td>2,736</td> \n\
					      <td>31.47</td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td>COU</td> \n\
					      <td></td> \n\
					      <td>2,736</td> \n\
					      <td>10.12</td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td>OC</td> \n\
					      <td></td> \n\
					      <td>2,736</td> \n\
					      <td>5.60</td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td>ECHD</td> \n\
					      <td></td> \n\
					      <td>2,736</td> \n\
					      <td>2.26</td> \n\
					    </tr> \n\
					    <tr> \n\
					      <td colspan="4"></td> \n\
					    </tr> \n\
					    <tr> \n\
					      <th class="text-center">TOTAL</th> \n\
					      <td></td> \n\
					      <td></td> \n\
					      <td>49.45</td> \n\
					    </tr> \n\
					 </tbody> \n\
				</table> \n\
			</div> \n\
		</div> \n\
		<div class="row"> \n\
			<div class="col-md-12 col-sm-12"> \n\
				<div class="col-md-12 col-sm-12 col-xs-12"> \n\
					<div class="horizontal-dl dltop"> \n\
						<dl class="dl-horizontal"> \n\
							<dt>Comments:</dt> \n\
							<dd>OWNERSHIP SOLD IN 2013-PER DEED 2013-00015716. ECAD NEVER MADE TRANSFER</dd> \n\
						</dl> \n\
					</div> \n\
				</div> \n\
				<div class="col-md-12 col-sm-12"> \n\
					<div class="col-md-12 col-sm-12 col-xs-12 botp"> \n\
						<p><strong>Total Net Loss Market: </strong> (2,736)</p> \n\
						<p><strong>Total Net Loss Tax Amount: </strong> (49.45)</p><br> \n\
					</div> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<p><strong>Journal Entry ID: </strong> 123456</p> \n\
					</div> \n\
					<div class="col-md-6 col-sm-6 col-xs-6 botp"> \n\
						<p><strong>Collection Extract Date: </strong> 08/16/207</p> \n\
					</div> \n\
				</div> \n\
			</div> \n\
		</div> \n\
		<!-- ---------------------------------------------Page 3--------------------------------------------- --> \n\
		<div class="container" id="content"> \n\
		<div class=""> \n\
			<div class="col-md-3  col-sm-3 col-xs-3"> \n\
				<img src="logo.jpg" alt="logo" class="logo"> \n\
			</div> \n\
			<div class="col-md-6 col-sm-6 col-xs-6 text-center"> \n\
				<h1>Ector County Appraisal District</h1> \n\
				<h4><strong>Mineral Correction / Journal Entry</strong></h4> \n\
				<h4><strong>(Summary)</strong></h4> \n\
			</div> \n\
		</div> \n\
		<div class="row"> \n\
			<div class="col-md-12 col-sm-12 col-xs-12"> \n\
				<div class="col-md-4 col-sm-4 col-xs-5"> \n\
					<p><strong>August 2017 Supplemental Changes</strong></p> \n\
				</div> \n\
				<div class="col-md-4 col-sm-4 col-xs-3"> \n\
					<p class="text-center"><strong>8/18/2017</strong></p> \n\
				</div> \n\
				<div class="col-md-4 col-sm-4 col-xs-4"> \n\
					<p class="text-right"><strong>Tax Year 2017</strong></p> \n\
				</div> \n\
			</div> \n\
		</div> \n\
		<div class="row"> \n\
			<div class="col-md-6 col-sm-6 col-xs-6 top"> \n\
				<dl class="dl-horizontal dltop"> \n\
					<dt>Batch Id:</dt> \n\
					<dd>1234567</dd> \n\
					<dt>Batch Create Date:</dt> \n\
					<dd>08/01/2017</dd> \n\
					<dt>Batch Process Date:</dt> \n\
					<dd>08/18/2017</dd> \n\
					<dt>Batch Total JEs:</dt> \n\
					<dd>426</dd> \n\
					<dt>Tax Year:</dt> \n\
					<dd>2017</dd> \n\
					<dt>Tax Year JEs:</dt> \n\
					<dd>156</dd> \n\
					<dt>Lease Owners:</dt> \n\
					<dd>1,635</dd> \n\
					<dt>Non-Value Changes:</dt> \n\
					<dd>823</dd> \n\
					<dt>Value Changes:</dt> \n\
					<dd>812</dd> \n\
				</dl> \n\
			</div> \n\
			<div class="col-md-6 col-sm-6 col-xs-6"> \n\
				<div class="horizontal-dl topdt"> \n\
					<dl class="dl-horizontal"> \n\
						<dt>Approved:<hr></dt><br> \n\
						<dd class="line"><strong>Asst. Chief Appraiser Initial</strong></dd> \n\
						<dt>Date:<hr></dt><br> \n\
						<dd class="line"></dd> \n\
						<dt>Certified:<hr></dt><br> \n\
						<dd class="line"><strong>Chief Appraiser Initial</strong></dd> \n\
						<dt>Date:<hr></dt><br> \n\
						<dd class="line"></dd> \n\
					</dl> \n\
				</div> \n\
			</div> \n\
		</div> \n\
		<div class="row"> \n\
			<div class="col-md-12 col-sm-12"> \n\
				<div class="row col-md-12 col-sm-12 col-xs-12"> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<p><strong>BEFORE JOURNAL ENTRY</strong></p> \n\
						<div class="horizontal-dl dltop"> \n\
							<dl class="dl-horizontal"> \n\
								<dt>Total Market Value:</dt> \n\
								<dd>359,610</dd> \n\
								<dt>Total Exemptions:</dt> \n\
								<dd>0</dd> \n\
								<dt>Total Taxable Value:</dt> \n\
								<dd>359,610</dd> \n\
							</dl> \n\
						</div> \n\
					</div> \n\
					<div class="col-md-6 col-sm-6 col-xs-6"> \n\
						<p><strong>AFTER JOURNAL ENTRY</strong></p> \n\
						<div class="horizontal-dl dltop"> \n\
							<dl class="dl-horizontal"> \n\
								<dt>Total Market Value:</dt> \n\
								<dd>359,610</dd> \n\
								<dt>Total Exemptions:</dt> \n\
								<dd>0</dd> \n\
								<dt>Total Taxable Value:</dt> \n\
								<dd>359,610</dd> \n\
							</dl> \n\
						</div> \n\
					</div> \n\
				</div> \n\
			</div> \n\
			<div class="row col-md-12 col-sm-12 col-xs-12"> \n\
				<div class="col-md-6 col-sm-6 col-xs-6"> \n\
					<div class="col-md-12 col-sm-12 col-xs-12"> \n\
						<table class="table table-bordered tablefont"> \n\
							<thead> \n\
						    <tr> \n\
						    	<td>Tax Unit</td> \n\
						        <td>Taxable Value</td> \n\
						        <td>Tax Unit Value</td> \n\
						        <td>Tax Rate</td> \n\
						        <td>Tax Amount</td> \n\
						    </tr> \n\
							 </thead> \n\
							 <tbody> \n\
							    <tr> \n\
							      <td>ECISD</td> \n\
							      <td>239,794</td> \n\
							      <td>239,794</td> \n\
							      <td>0.01150000</td> \n\
							      <td>2,757.64</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>COU</td> \n\
							      <td>239,794</td> \n\
							      <td>239,794</td> \n\
							      <td>0.00370000</td> \n\
							      <td>1,109.24</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>OC</td> \n\
							      <td>304,794</td> \n\
							      <td>304,794</td> \n\
							      <td>0.00204650</td> \n\
							      <td>623.74</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>ECHD</td> \n\
							      <td>315,491</td> \n\
							      <td>315,491</td> \n\
							      <td>0.00082500</td> \n\
							      <td>260.28</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>ODE</td> \n\
							      <td>124,262</td> \n\
							      <td>124,262</td> \n\
							      <td>0.00470590</td> \n\
							      <td>584.77</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>ECUD</td> \n\
							      <td>36,513</td> \n\
							      <td>36,513</td> \n\
							      <td>0.00084985</td> \n\
							      <td>31.03</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>GOL</td> \n\
							      <td>0</td> \n\
							      <td>0</td> \n\
							      <td>0.00108722</td> \n\
							      <td>0.00</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <th class="text-center">TOTAL</th> \n\
							      <td>1,320,648</td> \n\
							      <td>1,320,648</td> \n\
							      <td></td> \n\
							      <td>5,366.70</td> \n\
							    </tr> \n\
							 </tbody> \n\
						</table> \n\
					</div> \n\
				</div> \n\
				<div class="col-md-6 col-sm-6 col-xs-6"> \n\
					<div class="col-md-12 col-sm-12 col-xs-12"> \n\
						<table class="table table-bordered tablefont"> \n\
							<thead> \n\
						    <tr> \n\
						    	<td>Tax Unit</td> \n\
						        <td>Taxable Value</td> \n\
						        <td>Tax Unit Value</td> \n\
						        <td>Tax Rate</td> \n\
						        <td>Tax Amount</td> \n\
						    </tr> \n\
							 </thead> \n\
							 <tbody> \n\
							    <tr> \n\
							      <td>ECISD</td> \n\
							      <td>229,794</td> \n\
							      <td>229,794</td> \n\
							      <td>0.01150000</td> \n\
							      <td>2,642.64</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>COU</td> \n\
							      <td>296,794</td> \n\
							      <td>296,794</td> \n\
							      <td>0.00370000</td> \n\
							      <td>1,098.14</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>OC</td> \n\
							      <td>294,794</td> \n\
							      <td>294,794</td> \n\
							      <td>0.00204650</td> \n\
							      <td>603.28</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>ECHD</td> \n\
							      <td>305,491</td> \n\
							      <td>305,491</td> \n\
							      <td>0.00082500</td> \n\
							      <td>252.03</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>ODE</td> \n\
							      <td>119,262</td> \n\
							      <td>119,262</td> \n\
							      <td>0.00470590</td> \n\
							      <td>561.24</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>ECUD</td> \n\
							      <td>36,513</td> \n\
							      <td>36,513</td> \n\
							      <td>0.00084985</td> \n\
							      <td>31.03</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>GOL</td> \n\
							      <td>0</td> \n\
							      <td>0</td> \n\
							      <td>0.00108722</td> \n\
							      <td>0.00</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <th class="text-center">TOTAL</th> \n\
							      <td>1,282,648</td> \n\
							      <td>1,282,648</td> \n\
							      <td></td> \n\
							      <td>5,188.36</td> \n\
							    </tr> \n\
							 </tbody> \n\
						</table> \n\
					</div> \n\
				</div> \n\
			</div> \n\
			<div class="row col-md-12 col-sm-12 col-xs-12"> \n\
				<div class="col-md-6 col-sm-6 col-xs-6" > \n\
				</div> \n\
				<div class="col-md-6 col-sm-6 col-xs-6"> \n\
					<p><strong>NET GAIN or (LOSS)</strong></p> \n\
					<div class="col-xs-12"> \n\
						<table class="table table-bordered tablefont"> \n\
							<thead> \n\
						    <tr> \n\
						    	<td>Tax Unit</td> \n\
						        <td>Taxable Value</td> \n\
						        <td>Tax Unit Value</td> \n\
						        <td>Tax Rate</td> \n\
						        <td>Tax Amount</td> \n\
						    </tr> \n\
							 </thead> \n\
							 <tbody> \n\
							    <tr> \n\
							      <td>ECISD</td> \n\
							      <td>(10,000)</td> \n\
							      <td>(10,000)</td> \n\
							      <td>0.01150000</td> \n\
							      <td>(115.00)</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>COU</td> \n\
							      <td>(3,000)</td> \n\
							      <td>(3,000)</td> \n\
							      <td>0.00370000</td> \n\
							      <td>(11.10)</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>OC</td> \n\
							      <td>(10,000)</td> \n\
							      <td>(10,000)</td> \n\
							      <td>0.00204650</td> \n\
							      <td>(20.46)</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>ECHD</td> \n\
							      <td>(10,000)</td> \n\
							      <td>(10,000)</td> \n\
							      <td>0.00082500</td> \n\
							      <td>(8.25)</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>ODE</td> \n\
							      <td>(5,000)</td> \n\
							      <td>(5,000)</td> \n\
							      <td>0.00470590</td> \n\
							      <td>(23.53)</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>ECUD</td> \n\
							      <td>0</td> \n\
							      <td>0</td> \n\
							      <td>0.00084985</td> \n\
							      <td>0.00</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <td>GOL</td> \n\
							      <td>0</td> \n\
							      <td>0</td> \n\
							      <td>0.00108722</td> \n\
							      <td>0.00</td> \n\
							    </tr> \n\
							    <tr> \n\
							      <th class="text-center">TOTAL</th> \n\
							      <td>(38,000)</td> \n\
							      <td>(38,000)</td> \n\
							      <td></td> \n\
							      <td>(178.34)</td> \n\
							    </tr> \n\
							 </tbody> \n\
						</table> \n\
					</div> \n\
				</div> \n\
			</div> \n\
	</div> \n\
	</body> \n\
</html>'


            fs.writeFile(htmlPath, html, function (er) {
                if (er)
                    throw er;

                phantom.create().then(function (ph) {
                    ph.createPage().then(function (page) {
                        page.open(hostReportUrl).then(function (status) {
                            page.property('paperSize', {
                                width: '8.5in',
                                height: '11in',
                            });
                            page.render(pdfPath).then(function () {
                                var data = fs.readFileSync(pdfPath);
                                res.contentType("application/pdf");
                                res.send(data);
                                ph.exit();
                            });
                        });
                    });
                });
            });
        });
    },
}