
module.exports = {
    Pull: function (req, res) {
		//console.log("==============================================================");
		var Git = require("nodegit");
		var url = "https://github.com/CAMApro/MineralPro.git";
		var dir = "../MineralPro";
		var username = "CAMApro";
		var repository;
		
		Git.Repository.open(dir)
		.then(function(repo){
			repository = repo;
			
			return repository.fetchAll({
				callbacks: {
					credentials: function(url, username){
						return Git.Cred.sshKeyFromAgent(username);
					},
					certificateCheck: function(){
						return 1;
					}
				}
			});
		})
		.then(function(){
			return repository.mergeBranches("Development", "origin/master");
		})
		.done(function(){
			//console.log("Done!");
		});
		
	}
}