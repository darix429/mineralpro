<?php

include "../../../../../api/SystemConfig.php";
include "../../../../../api/UtilFunctions.php";

$sql = "SELECT top 50
	a.accountNum,
	a.accountName,
	a.idAccount,
	(ao.name1 + ' ' + ao.name2 + ' ' + ao.name3)  name,
	ao.masterOwnerId,
	(ao.displayName1 + ' ' + ao.displayName2 + ' ' + ao.displayName3) displayName,
	alta.altAccountNum,
	aa.addressTypeCd,
	(aa.city +' '+ CONVERT(varchar(12),aa.streetAddress, 101) +' '+ CONVERT(varchar(12),aa.unit, 101) +' '+ aa.building) streetAddress,
	avs.landValue,
	avs.imprValue,
	avs.totalValue
    FROM dbo.Account a
	INNER JOIN dbo.AccountOwner ao ON ao.idAccount = a.idAccount AND ao.rowDeleteFlag = '' AND ao.seqNumber = 1
	INNER JOIN dbo.AltAccount alta ON alta.idAccountYear = a.idAccountYear AND alta.rowDeleteFlag = '' AND alta.typeCd = 1
	INNER JOIN dbo.AccountAddress aa ON aa.idAccountYear = a.idAccountYear AND aa.rowDeleteFlag = ''
	INNER JOIN dbo.AccountValueSummary avs ON avs.idAccountYear = a.idAccountYear AND avs.rowDeleteFlag = ''
    WHERE a.rowDeleteFlag = ''";
	
	if(isset($_GET['SearchOption'])){
        if($_GET['SearchOption'] == 'Account Number'){
            
            if($_GET['SearchType'] == 'Contains'){
                $sql = $sql." AND CHARINDEX('" . $_GET['SearchField'] . "', a.accountNum) > 0 ";
            }else if($_GET['SearchType'] == 'Start With'){
                $sql = $sql." AND a.accountNum LIKE '" . $_GET['SearchField'] . "%'";
            }else if($_GET['SearchType'] == 'Match Exactly'){
                $sql = $sql." AND a.accountNum = '" . $_GET['SearchField'] . "'";
            }
            
        }else if($_GET['SearchOption'] == 'GEO Account Number'){
            
            if($_GET['SearchType'] == 'Contains'){
                $sql = $sql." AND CHARINDEX('" . $_GET['SearchField'] . "', alta.altAccountNum) > 0 ";
            }else if($_GET['SearchType'] == 'Start With'){
                $sql = $sql." AND alta.altAccountNum LIKE '" . $_GET['SearchField'] . "%'";
            }else if($_GET['SearchType'] == 'Match Exactly'){
                $sql = $sql." AND alta.altAccountNum = '" . $_GET['SearchField'] . "'";
            }
            
        }else if($_GET['SearchOption'] == 'Owner'){
            
            if($_GET['SearchType'] == 'Contains'){
                $sql = $sql." AND CHARINDEX('" . $_GET['SearchField'] . "', ao.name1) > 0 ";
            }else if($_GET['SearchType'] == 'Start With'){
                $sql = $sql." AND ao.name1 LIKE '" . $_GET['SearchField'] . "%'";
            }else if($_GET['SearchType'] == 'Match Exactly'){
                $sql = $sql." AND ao.name1 = '" . $_GET['SearchField'] . "'";
            }
            
        }else if($_GET['SearchOption'] == 'Master Owner'){
            
            if($_GET['SearchType'] == 'Contains'){
                $sql = $sql." AND CHARINDEX('" . $_GET['SearchField'] . "', ao.masterOwnerId) > 0 ";
            }else if($_GET['SearchType'] == 'Start With'){
                $sql = $sql." AND ao.masterOwnerId LIKE '" . $_GET['SearchField'] . "%'";
            }else if($_GET['SearchType'] == 'Match Exactly'){
                $sql = $sql." AND ao.masterOwnerId = '" . $_GET['SearchField'] . "'";
            }
            
        }else if($_GET['SearchOption'] == 'Prior Owner'){
            
            if($_GET['SearchType'] == 'Contains'){
                $sql = $sql." AND CHARINDEX('" . $_GET['SearchField'] . "', ao.displayName1) > 0
                OR CHARINDEX('" . $_GET['SearchField'] . "', ao.displayName2) > 0
                OR CHARINDEX('" . $_GET['SearchField'] . "', ao.displayName3) > 0 ";
            }else if($_GET['SearchType'] == 'Start With'){
                $sql = $sql." AND ao.displayName1 LIKE '" . $_GET['SearchField'] . "%'
                OR ao.displayName2 LIKE '" . $_GET['SearchField'] . "%'
                OR ao.displayName3 LIKE '" . $_GET['SearchField'] . "%'";
            }else if($_GET['SearchType'] == 'Match Exactly'){
                $sql = $sql." AND ao.displayName1 = '" . $_GET['SearchField'] . "'
                OR ao.displayName2 = '" . $_GET['SearchField'] . "'
                OR ao.displayName3 = '" . $_GET['SearchField'] . "'";
            }
            
        }else if($_GET['SearchOption'] == 'Situs Address'){

        }else if($_GET['SearchOption'] == 'Legal information'){

        }else if($_GET['SearchOption'] == 'DBA'){

        }else if($_GET['SearchOption'] == 'Alt Account Number'){

        }
    }
$rows = execSQL('select', $sql);

$output = "{'success': true, 'data': " . json_encode($rows) . "}";

# print out the results in json format
echo $output;
?>



