<?php

include "../../../../../api/SystemConfig.php";
include "../../../../../api/UtilFunctions.php";
$time = round(microtime(true) * 1000);

$sql = "SET NOCOUNT ON; 
        DECLARE @cols AS NVARCHAR(MAX),
        @query  AS NVARCHAR(MAX)

    IF OBJECT_ID('tempdb..temp_account_".$time."') is not null
    DROP TABLE temp_account_".$time."
    SELECT top 50
	a.accountNum,
	a.accountName,
	a.idAccount,
        RTRIM(LTRIM((ao.name1 + ' ' + ao.name2 + ' ' + ao.name3)))  name,
	ao.masterOwnerId,
	RTRIM(LTRIM((ao.displayName1 + ' ' + ao.displayName2 + ' ' + ao.displayName3))) displayName,
	alta.altAccountNum,
	aa.addressTypeCd,
	RTRIM(LTRIM((aa.city +' '+ CONVERT(varchar(12),aa.streetAddress, 101) +' '+ CONVERT(varchar(12),aa.unit, 101) +' '+ aa.building))) streetAddress,
	avs.landValue,
	avs.imprValue,
	avs.totalValue
        INTO temp_account_".$time."
    FROM dbo.Account a
	INNER JOIN dbo.AccountOwner ao ON ao.idAccount = a.idAccount AND ao.rowDeleteFlag = '' AND ao.seqNumber = 1
	INNER JOIN dbo.AltAccount alta ON alta.idAccountYear = a.idAccountYear AND alta.rowDeleteFlag = '' AND alta.typeCd = 1
	INNER JOIN dbo.AccountAddress aa ON aa.idAccountYear = a.idAccountYear AND aa.rowDeleteFlag = ''
	INNER JOIN dbo.AccountValueSummary avs ON avs.idAccountYear = a.idAccountYear AND avs.rowDeleteFlag = ''
    WHERE a.rowDeleteFlag = ''";
	
	if(isset($_GET['SearchOption'])){
        if($_GET['SearchOption'] == 'Account Number'){
            
            if($_GET['SearchType'] == 'Contains'){
                $sql = $sql." AND CHARINDEX('" . $_GET['SearchField'] . "', a.accountNum) > 0 ";
            }else if($_GET['SearchType'] == 'Start With'){
                $sql = $sql." AND a.accountNum LIKE '" . $_GET['SearchField'] . "%'";
            }else if($_GET['SearchType'] == 'Match Exactly'){
                $sql = $sql." AND a.accountNum = '" . $_GET['SearchField'] . "'";
            }
            
        }else if($_GET['SearchOption'] == 'GEO Account Number'){
            
            if($_GET['SearchType'] == 'Contains'){
                $sql = $sql." AND CHARINDEX('" . $_GET['SearchField'] . "', alta.altAccountNum) > 0 ";
            }else if($_GET['SearchType'] == 'Start With'){
                $sql = $sql." AND alta.altAccountNum LIKE '" . $_GET['SearchField'] . "%'";
            }else if($_GET['SearchType'] == 'Match Exactly'){
                $sql = $sql." AND alta.altAccountNum = '" . $_GET['SearchField'] . "'";
            }
            
        }else if($_GET['SearchOption'] == 'Owner'){
            
            if($_GET['SearchType'] == 'Contains'){
                $sql = $sql." AND CHARINDEX('" . $_GET['SearchField'] . "', ao.name1) > 0 ";
            }else if($_GET['SearchType'] == 'Start With'){
                $sql = $sql." AND ao.name1 LIKE '" . $_GET['SearchField'] . "%'";
            }else if($_GET['SearchType'] == 'Match Exactly'){
                $sql = $sql." AND ao.name1 = '" . $_GET['SearchField'] . "'";
            }
            
        }else if($_GET['SearchOption'] == 'Master Owner'){
            
            if($_GET['SearchType'] == 'Contains'){
                $sql = $sql." AND CHARINDEX('" . $_GET['SearchField'] . "', ao.masterOwnerId) > 0 ";
            }else if($_GET['SearchType'] == 'Start With'){
                $sql = $sql." AND ao.masterOwnerId LIKE '" . $_GET['SearchField'] . "%'";
            }else if($_GET['SearchType'] == 'Match Exactly'){
                $sql = $sql." AND ao.masterOwnerId = '" . $_GET['SearchField'] . "'";
            }
            
        }else if($_GET['SearchOption'] == 'Prior Owner'){
            
            if($_GET['SearchType'] == 'Contains'){
                $sql = $sql." AND CHARINDEX('" . $_GET['SearchField'] . "', ao.displayName1) > 0
                OR CHARINDEX('" . $_GET['SearchField'] . "', ao.displayName2) > 0
                OR CHARINDEX('" . $_GET['SearchField'] . "', ao.displayName3) > 0 ";
            }else if($_GET['SearchType'] == 'Start With'){
                $sql = $sql." AND ao.displayName1 LIKE '" . $_GET['SearchField'] . "%'
                OR ao.displayName2 LIKE '" . $_GET['SearchField'] . "%'
                OR ao.displayName3 LIKE '" . $_GET['SearchField'] . "%'";
            }else if($_GET['SearchType'] == 'Match Exactly'){
                $sql = $sql." AND ao.displayName1 = '" . $_GET['SearchField'] . "'
                OR ao.displayName2 = '" . $_GET['SearchField'] . "'
                OR ao.displayName3 = '" . $_GET['SearchField'] . "'";
            }
            
        }else if($_GET['SearchOption'] == 'Situs Address'){

        }else if($_GET['SearchOption'] == 'Legal information'){

        }else if($_GET['SearchOption'] == 'DBA'){

        }else if($_GET['SearchOption'] == 'Alt Account Number'){

        }
    }


$sql = $sql . " 	
SET @cols = STUFF((SELECT ',' + QUOTENAME(accountNum) 
                    from temp_account_".$time."
                    group by accountNum
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'')

SET @query = N'
			SELECT ''Geo Account Number'' fieldName,' + @cols + N' from 
             (
                select CAST(altAccountNum as VARCHAR(1000)) altAccountNum, accountNum
                FROM  temp_account_".$time."
            ) x
            pivot 
            (
				max(altAccountNum)
                for accountNum in (' + @cols + N')
            ) p
			UNION
			SELECT ''Owner Name'' fieldName,' + @cols + N' from 
             (
                select CAST(name as VARCHAR(1000)) name, accountNum
                FROM  temp_account_".$time."
            ) x
            pivot 
            (
				max(name)
                for accountNum in (' + @cols + N')
            ) p 
			UNION
			SELECT ''Master Owner Id'' fieldName,' + @cols + N' from 
             (
                select CAST(masterOwnerId as VARCHAR(1000)) masterOwnerId, accountNum
                FROM  temp_account_".$time."
            ) x
            pivot 
            (
				max(masterOwnerId)
                for accountNum in (' + @cols + N')
            ) p
			UNION
			SELECT ''Prior Owner Name'' fieldName,' + @cols + N' from 
             (
                select CAST(displayName as VARCHAR(1000)) displayName, accountNum
                FROM  temp_account_".$time."
            ) x
            pivot 
            (
				max(displayName)
                for accountNum in (' + @cols + N')
            ) p

			UNION
			SELECT ''Situs Address'' fieldName,' + @cols + N' from 
             (
                select CAST(streetAddress as VARCHAR(1000)) streetAddress, accountNum
                FROM  temp_account_".$time."
            ) x
            pivot 
            (
				max(streetAddress)
                for accountNum in (' + @cols + N')
            ) p
			UNION
			SELECT ''Legal Information'' fieldName,' + @cols + N' from 
             (
                select '''' legalInfo, accountNum
                FROM  temp_account_".$time."
            ) x
            pivot 
            (
				max(legalInfo)
                for accountNum in (' + @cols + N')
            ) p
			UNION
			SELECT ''DBA'' fieldName,' + @cols + N' from 
             (
                select '''' dba, accountNum
                FROM  temp_account_".$time."
            ) x
            pivot 
            (
				max(dba)
                for accountNum in (' + @cols + N')
            ) p
			UNION
			SELECT ''Alt Account Number'' fieldName,' + @cols + N' from 
             (
                select '''' altAccountNum, accountNum
                FROM  temp_account_".$time."
            ) x
            pivot 
            (
				max(altAccountNum)
                for accountNum in (' + @cols + N')
            ) p
			UNION
			SELECT ''Total Land Value'' fieldName,' + @cols + N' from 
             (
                select CAST(landValue as VARCHAR(1000)) landValue, accountNum
                FROM  temp_account_".$time."
            ) x
            pivot 
            (
				max(landValue)
                for accountNum in (' + @cols + N')
            ) p
			UNION
			SELECT ''Total Improvement Value'' fieldName,' + @cols + N' from 
             (
                select CAST(imprValue as VARCHAR(1000)) imprValue, accountNum
                FROM  temp_account_".$time."
            ) x
            pivot 
            (
				max(imprValue)
                for accountNum in (' + @cols + N')
            ) p
            UNION
            SELECT ''Total Market Value'' fieldName,' + @cols + N' from 
             (
                select CAST(totalValue as VARCHAR(1000)) totalValue, accountNum
                FROM  temp_account_".$time."
            ) x
            pivot 
            (
				max(totalValue)
                for accountNum in (' + @cols + N')
            ) p
            UNION
            SELECT ''Exemption List'' fieldName,' + @cols + N' from 
             (
                select '''' excemptList, accountNum
                FROM  temp_account_".$time."
            ) x
            pivot 
            (
                max(excemptList)
                for accountNum in (' + @cols + N')
            ) p'

EXEC (@query );";

$rows = execSQL('select', $sql);

$sql = "SET NOCOUNT ON; 
        SELECT STUFF((SELECT ',' + QUOTENAME(accountNum) 
                    from temp_account_".$time."
                    group by accountNum
            FOR XML PATH(''), TYPE
            ).value('.', 'NVARCHAR(MAX)') 
        ,1,1,'') columnField
        DROP TABLE temp_account_".$time;

$cols = execSQL('select', $sql);

$output = "{'success': true
        ,'data': " . json_encode($rows) . " 
        ,'columnField': " . json_encode($cols) . "}";

# print out the results in json format
echo $output;
?>



