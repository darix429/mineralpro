module.exports = {
    Read: function (req, res) {
        var UtilFunctions = require('../../UtilFunctions.js');
        var year = req.query.appraisalYear;
        if(!year){
            res.status(400).json({status: false, error: 'Appraisal Year is required.'})
        }
        
        var sql = `
            DECLARE @year smallint = ${year}, @y smallint, @tenYr smallint;
            IF @year=year(getDate()) 
            BEGIN
                SET @year = @year - 1;
            END
            
            SET @y = @year;
            SET @tenYr = @year - 10;

            IF OBJECT_ID(N'tempdb..#tempFillGap',N'U') IS NOT NULL
            DROP TABLE #tempFillGap

            IF OBJECT_ID(N'tempdb..#tempResult',N'U') IS NOT NULL
            DROP TABLE #tempResult

            CREATE TABLE #tempFillGap(
                appraisalYear smallint null,
                number int null
            )
            WHILE @y >= @tenYr
            BEGIN
                INSERT INTO #tempFillGap(appraisalYear, number) VALUES(@y, 0)
                SET @y = @y - 1
            END

            SELECT tf.appraisalYear, SUM(ISNULL(tbl.number,0) + ISNULL(tf.number,0)) number
            INTO #tempResult
            FROM
            (
                select appraisalYear
                , count(*) number
                from JournalEntry
                where appraisalYear >= @year - 10 AND appraisalYear <= @year
                group by appraisalYear
            )tbl
            FULL JOIN #tempFillGap tf on tbl.appraisalYear = tf.appraisalYear
            GROUP BY tf.appraisalYear
            
            DECLARE @cols AS NVARCHAR(MAX),
                @query  AS NVARCHAR(MAX)

            select @cols = STUFF((SELECT ',' + QUOTENAME(appraisalYear) 
                                from #tempResult
                                group by appraisalYear, number
                                order by appraisalYear
                        FOR XML PATH(''), TYPE
                        ).value('.', 'NVARCHAR(MAX)') 
                    ,1,1,'')

            set @query = N'SELECT ' + @cols + N' from 
                         (
                            select number, appraisalYear
                            from #tempResult
                        ) x
                        pivot 
                        (
                            max(number)
                            for appraisalYear in (' + @cols + N')
                        ) p '

            exec sp_executesql @query;
        `;
        
        UtilFunctions.execSql(sql, res);
    }
};