module.exports = {
    Read: function (req, res) {
        var UtilFunctions = require('../../UtilFunctions.js');
        var year = req.query.appraisalYear;
        if(!year){
            res.status(400).json({status: false, error: 'Appraisal Year is required.'})
        }
        var sql = `
            DECLARE @appraisalYear smallint = ${year};
            IF OBJECT_ID(N'tempdb..#tempProg',N'U') IS NOT NULL
            DROP TABLE #tempProg


            SELECT SUM(CASE WHEN tbl.selectedAppraisalYear = tbl.lastYearAppraisalWasDone THEN tbl.numberOfAppraisals ELSE 0 END) AS complete
            , SUM(CASE WHEN tbl.selectedAppraisalYear = tbl.lastYearAppraisalWasDone THEN 0 ELSE tbl.numberOfAppraisals END) AS incomplete
            INTO #tempProg
            FROM
            (
                SELECT 
                appraisalYear selectedAppraisalYear,
                lastAppraisalYear  lastYearAppraisalWasDone,
                count(1) numberOfAppraisals
                FROM Appraisal
                WHERE rowDeleteFlag <> 'D'
                AND appraisalYear = @appraisalYear
                GROUP BY appraisalYear, lastAppraisalYear
            )tbl

            SELECT complete as data, 'complete' as name
            FROM #tempProg
            UNION ALL
            SELECT incomplete as data, 'incomplete' as name
            FROM #tempProg

        `;
        UtilFunctions.execSql(sql, res);

    }
};