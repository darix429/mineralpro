module.exports = {
    Read: function (req, res) {
        var UtilFunctions = require('../../UtilFunctions.js');
        var year = req.query.appraisalYear;
        if(!year){
            res.status(400).json({status: false, error: 'Appraisal Year is required.'})
        }
        var sql = `
            SELECT 
            appraisalYear selectedAppraisalYear,
            lastAppraisalYear  lastYearAppraisalWasDone,
            count(1) numberOfAppraisals
            FROM Appraisal
            WHERE rowDeleteFlag <> 'D'
            AND appraisalYear = '${year}'
            AND lastAppraisalYear <> '${year}'
            GROUP BY appraisalYear, lastAppraisalYear

        `;
        UtilFunctions.execSql(sql, res);

    }
};