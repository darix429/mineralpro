module.exports = {
    Read: function (req, res) {
        var UtilFunctions = require('../../UtilFunctions.js');
        var year = req.query.appraisalYear;
        if(!year){
            res.status(400).json({status: false, error: 'Appraisal Year is required.'})
        }
        var sql = `
            /****** Script for Chief Appraiser Dashboard  ******/
            -- these variables can be set as parameters if this is convereted to a stored procedure
            DECLARE @selectedAppraisalYear int = ${year} ,
                    @yearsBack int = 10,         -- for the most recent 10 years
                    @cutOffAppraisalYear int

            SET @cutOffAppraisalYear = @selectedAppraisalYear - @yearsBack


            IF OBJECT_ID('tempdb..#tempResult') IS NOT NULL
                DROP TABLE #tempResult
            IF OBJECT_ID('tempdb..#tempLeaseTaxUnit') IS NOT NULL
                DROP TABLE #tempLeaseTaxUnit

            IF OBJECT_ID('tempdb..#tempAppLeaseTaxUnit') IS NOT NULL
                DROP TABLE  #tempAppLeaseTaxUnit

            IF OBJECT_ID('tempdb..#tempLeaseTaxUnitOwner') IS NOT NULL
                DROP TABLE  #tempLeaseTaxUnitOwner

            IF OBJECT_ID('tempdb..#temptOwnerExemption') IS NOT NULL
                DROP TABLE  #temptOwnerExemption

            IF OBJECT_ID('tempdb..#tempOwnerExemptValue') IS NOT NULL
                DROP TABLE #tempOwnerExemptValue

            --=== create a temp table for holding the data ----
            CREATE TABLE #tempResult (
                itemName varchar(150) null,
                appraisalYear int null,
                taxUnit varchar(50) null,
                number bigint null
            )

            -----------------------------------------
            -- get lease, lease value, and tax unit for each lease
            SELECT a.appraisalYear, a.leaseId, d.unitId, c.taxUnitAbbr
                , SUM(a.appraisedValue*b.taxUnitPercent) taxUnitLeaseValue
                , MAX(taxUnitPercent) taxUnitPercent        
                INTO #tempLeaseTaxUnit
                FROM [MINERALPRO].[dbo].LeaseValueSummary a
                JOIN [MINERALPRO].[dbo].[LeaseTaxUnit] b
                    ON a.appraisalYear = b.appraisalYear
                    AND a.leaseId = b.leaseId
                    AND a.rowDeleteFlag <> 'D'
                    AND b.rowDeleteFlag <> 'D'
                    AND b.taxUnitPercent is not null
                JOIN [MINERALPRO].[dbo].[TaxUnit] c
                    ON a.appraisalYear = c.appraisalYear
                    AND b.taxUnitTypeCd = c.taxUnitTypeCd
                    AND b.taxUnitCd = c.taxUnitCd
                    AND c.taxingInd = 'Y'
                    AND c.rowDeleteFlag <> 'D'
                    AND c.taxUnitAbbr IS NOT NULL
                    AND LEN(c.taxUnitAbbr) > 0
                JOIN [MINERALPRO].[dbo].[Lease] d
                    ON a.appraisalYear = d.appraisalYear
                    AND a.leaseId = d.leaseId
                    AND d.rowDeleteFlag <> 'D'
                WHERE a.appraisalYear <= @selectedAppraisalYear
                AND a.appraisalYear >= @cutOffAppraisalYear
                GROUP BY a.appraisalYear, a.leaseId, unitId, c.taxUnitAbbr


            -- ==== Total Appraised Value =========
            INSERT INTO #tempResult
                (itemName, appraisalYear, taxUnit, number)
                SELECT 'Total Appraised Value', appraisalYear, taxUnitAbbr, CAST(ROUND(SUM(taxUnitLeaseValue), 0) as bigint)
                FROM #tempLeaseTaxUnit
                GROUP BY appraisalYear, taxUnitAbbr
                ORDER BY appraisalYear, taxUnitAbbr

            --select * from #tempResult

            --== # of Unit/Lease Appraisals, # of leases in Units ------
            --DECLARE @selectedAppraisalYear int = 2018 ,
            --      @yearsBack int = 9,         -- for the most recent 10 years
            --      @cutOffAppraisalYear int
            --SET @cutOffAppraisalYear = @selectedAppraisalYear - @yearsBack

            SELECT a.[appraisalYear]
                  ,[subjectTypeCd]
                  ,[subjectId]
                  ,[rrcNumber]
                  ,b.taxUnitAbbr
                  ,b.taxUnitPercent
                  ,productionEquipmentValue
                  ,serviceEquipmentValue
                  ,injectionEquipmentValue
                  ,[productionEquipmentCount]       --** to be used for # of Production Wells
                  ,[serviceEquipmentCount]          --** to be used for # of Service Wells
                  ,[injectionEquipmentCount]        --** to be used for # of Injection Wells
              INTO #tempAppLeaseTaxUnit
              FROM [MINERALPRO].[dbo].Appraisal a
              JOIN  #tempLeaseTaxUnit b
              ON a.appraisalYear = b.appraisalYear
              AND a.subjectId = b.unitId
              AND a.subjectTypeCd = 1  -- only get units
              --AND a.totalValue > 0   -- include appraisal value = 0
              AND a.totalValue IS NOT NULL
              AND b.unitId IS NOT NULL AND b.unitId > 0
              AND a.rowDeleteFlag <> 'D'  --AND b.rowDeleteFlag <> 'D' 
              WHERE b.appraisalYear >= @cutOffAppraisalYear -- last 10 years =  (2018 - 9)
              AND b.appraisalYear <= @selectedAppraisalYear
            UNION ALL
            SELECT c.[appraisalYear]
                  ,[subjectTypeCd]
                  ,[subjectId]
                  ,[rrcNumber]
                  ,d.taxUnitAbbr
                  ,d.taxUnitPercent
                  ,[productionEquipmentValue]
                  ,[serviceEquipmentValue]
                  ,[injectionEquipmentValue]
                  ,[productionEquipmentCount]       --** to be used for # of Production Wells
                  ,[serviceEquipmentCount]          --** to be used for # of Service Wells
                  ,[injectionEquipmentCount]        --** to be used for # of Injection Wells
              FROM [MINERALPRO].[dbo].[Appraisal] c
              JOIN #tempLeaseTaxUnit d
              ON c.appraisalYear = d.appraisalYear
              AND c.subjectId = d.leaseId
              AND c.subjectTypeCd = 2  -- only get leases
              AND totalValue IS NOT NULL
              AND c.rowDeleteFlag <> 'D'
              WHERE c.appraisalYear >= @cutOffAppraisalYear -- last 10 years =  (2018 - 9)
              AND c.appraisalYear <= @selectedAppraisalYear
              ORDER BY a.appraisalYear, a.subjectTypeCd, a.subjectId, a.rrcNumber


            -- ==== # of Unit Appraisals =========
            INSERT INTO #tempResult
                (itemName, appraisalYear, taxUnit, number)
                SELECT '# of Unit Appraisals', d.appraisalYear, taxUnitAbbr, count(1)
                FROM #tempAppLeaseTaxUnit d
                WHERE d.subjectTypeCd = 1  -- only select Unit Appraisals
                GROUP BY d.appraisalYear, taxUnitAbbr
                ORDER BY d.appraisalYear, taxUnitAbbr

            -- ==== # of Leases in Units =========
            INSERT INTO #tempResult
                (itemName, appraisalYear, taxUnit, number)
                SELECT '# of Leases in Units', a.appraisalYear, taxUnitAbbr, count(1)
                FROM #tempLeaseTaxUnit a
                WHERE a.unitId > 0  -- only select Unit Appraisals
                GROUP BY a.appraisalYear, taxUnitAbbr
                ORDER BY a.appraisalYear, taxUnitAbbr


            -- ==== # of Lease Appraisals =========
            INSERT INTO #tempResult
                (itemName, appraisalYear, taxUnit, number)
                SELECT '# of Lease Appraisals', d.appraisalYear, taxUnitAbbr, count(1)
                FROM #tempAppLeaseTaxUnit d
                WHERE d.subjectTypeCd = 2  -- only select Lease Appraisals
                GROUP BY d.appraisalYear, taxUnitAbbr
                ORDER BY d.appraisalYear, taxUnitAbbr

            -- get lease owner and interest type information, based on (lease, owner, interest type)
            SELECT DISTINCT a.appraisalYear, a.taxUnitAbbr, a.taxUnitPercent
                , b.leaseId, b.ownerId, b.interestTypeCd, b.ownerValue
                INTO #tempLeaseTaxUnitOwner
                FROM #tempLeaseTaxUnit a
                JOIN MINERALPRO.dbo.LeaseOwner b
                ON a.appraisalYear = b.appraisalYear
                AND a.leaseId = b.leaseId
                AND b.rowDeleteFlag <> 'D'
                WHERE b.ownerId is not null
                AND b.ownerValue is not null

            -- ==== # of D.O. Owners =========
            INSERT INTO #tempResult
                (itemName, appraisalYear, taxUnit, number)
                SELECT '# of D.O. Owners', a.appraisalYear, taxUnitAbbr, count(1)
                FROM (
                    SELECT DISTINCT appraisalYear, taxUnitAbbr, ownerId
                    FROM #tempLeaseTaxUnitOwner
                ) a
                GROUP BY a.appraisalYear, taxUnitAbbr
                ORDER BY a.appraisalYear, taxUnitAbbr

            -- ==== # of D.O. Owner Interests ========= 
            INSERT INTO #tempResult
                (itemName, appraisalYear, taxUnit, number)
                SELECT '# of D.O. Owner Interests', a.appraisalYear, taxUnitAbbr, count(1)
                FROM #tempLeaseTaxUnitOwner a
                GROUP BY a.appraisalYear, taxUnitAbbr
                ORDER BY a.appraisalYear, taxUnitAbbr


            -- ==== Equipment Value ========= adjusted by taxUnitPercent
            INSERT INTO #tempResult
                (itemName, appraisalYear, taxUnit, number)
                SELECT 'Equipment Value', d.appraisalYear, taxUnitAbbr, CAST(ROUND(SUM(equipmentValue), 0) as bigint)
                FROM (
                    SELECT appraisalYear, taxUnitAbbr
                    , (productionEquipmentValue+serviceEquipmentValue+injectionEquipmentValue)*taxUnitPercent equipmentValue
                    FROM #tempAppLeaseTaxUnit
                ) d
                GROUP BY d.appraisalYear, taxUnitAbbr
                ORDER BY d.appraisalYear, taxUnitAbbr


            -- ==== # of Production Wells ========= adjusted by taxUnitPercent
            INSERT INTO #tempResult
                (itemName, appraisalYear, taxUnit, number)
                SELECT '# of Production Wells', d.appraisalYear, taxUnitAbbr
                    , CAST(ROUND(SUM(productionEquipmentCount*taxUnitPercent), 0) as int)
                FROM #tempAppLeaseTaxUnit d
                GROUP BY d.appraisalYear, taxUnitAbbr
                ORDER BY d.appraisalYear, taxUnitAbbr


            -- ==== # of Service Wells ========= adjusted by taxUnitPercent
            INSERT INTO #tempResult
                (itemName, appraisalYear, taxUnit, number)
                SELECT '# of Service Wells', d.appraisalYear, taxUnitAbbr
                    , CAST(ROUND(SUM(serviceEquipmentCount*taxUnitPercent), 0) as int)
                FROM #tempAppLeaseTaxUnit d
                GROUP BY d.appraisalYear, taxUnitAbbr
                ORDER BY d.appraisalYear, taxUnitAbbr

            -- ==== # of Injection Wells ========= adjusted by taxUnitPercent
            INSERT INTO #tempResult
                (itemName, appraisalYear, taxUnit, number)
                SELECT '# of Injection Wells', d.appraisalYear, taxUnitAbbr
                    , CAST(ROUND(SUM(injectionEquipmentCount*taxUnitPercent), 0) as int)
                FROM #tempAppLeaseTaxUnit d
                GROUP BY d.appraisalYear, taxUnitAbbr
                ORDER BY d.appraisalYear, taxUnitAbbr


            -- getting exemption values ---
            SELECT a.appraisalYear, a.ownerId, a.exemptionTypeCd, a.exemptionCd
                , b.exemptionValue, s.description exemptDesc
                INTO #temptOwnerExemption
                FROM [MINERALPRO].[dbo].[OwnerExemption] a
                --ON w.appraisalYear = a.appraisalYear
                --AND w.ownerId = a.ownerId
                JOIN [MINERALPRO].[dbo].[Exemption]  b
                ON a.exemptionCd = b.exemptionCd
                AND a.appraisalYear = b.appraisalYear
                AND a.exemptionTypeCd = (
                    CASE b.exemptionTypeCd 
                    WHEN 3 THEN 3440        -- disable Vet
                    WHEN 10 THEN 3430       -- total Exemption
                    ELSE 0 END)
                AND a.rowDeleteFlag <> 'D' AND b.rowDeleteFlag <> 'D' 
                AND DATEPART(yy, a.effectiveDt) <= a.appraisalYear
                AND DATEPART(yy, a.expirationDt) > a.appraisalYear
                LEFT JOIN [MINERALPRO].[dbo].SystemCode s
                ON a.exemptionCd = s.code 
                AND s.codeType = a.exemptionTypeCd 
                AND s.appraisalYear = a.appraisalYear
                AND s.rowDeleteFlag <> 'D'
                WHERE a.exemptionTypeCd*100+a.exemptionCd <> 343023  -- excluding UNDER $500, exemptionTypeCd = 3430 and exemptionCd = 23
                ORDER BY a.appraisalYear, s.description, a.ownerId

            -- drop table #tempOwnerExemptValue
            SELECT a.appraisalYear, a.taxUnitAbbr, a.ownerId, a.ownerValue, a.taxUnitPercent
                , b.exemptDesc, b.exemptionTypeCd, b.exemptionValue
                INTO #tempOwnerExemptValue
                FROM (
                    SELECT appraisalYear, taxUnitAbbr, ownerId
                        , SUM(ownerValue) ownerValue, MAX(taxUnitPercent) taxUnitPercent
                    FROM #tempLeaseTaxUnitOwner
                    GROUP BY appraisalYear, ownerId, taxUnitAbbr
                ) a
                LEFT JOIN #temptOwnerExemption b
                ON a.appraisalYear = b.appraisalYear
                AND a.ownerId = b.ownerId

            --==== Totally Exempt Value ======
            INSERT INTO #tempResult
                (itemName, appraisalYear, taxUnit, number)
                SELECT 'Totally Exempt Value', appraisalYear, taxUnitAbbr
                    , CAST(ROUND(SUM(ownerValue*taxUnitPercent), 0) as bigint)
                FROM #tempOwnerExemptValue
                WHERE exemptionTypeCd = 3430  -- total exemption    
                GROUP BY appraisalYear, taxUnitAbbr

            --==== # of Owners-Total Exempt Desc ======
            INSERT INTO #tempResult
                (itemName, appraisalYear, taxUnit, number)
                SELECT '# of Owners-'+exemptDesc, appraisalYear, taxUnitAbbr, COUNT(1)
                FROM (
                    SELECT DISTINCT appraisalYear, taxUnitAbbr, exemptDesc, ownerId
                    FROM #tempOwnerExemptValue
                    WHERE exemptionTypeCd = 3430  -- total exemption    
                ) a
                GROUP BY appraisalYear, taxUnitAbbr, exemptDesc

            --==== Under $500 Exempt Value ======
            INSERT INTO #tempResult
                (itemName, appraisalYear, taxUnit, number)
                SELECT 'Under $500 Exempt', appraisalYear, taxUnitAbbr
                    , CAST(ROUND(SUM(ownerValue*taxUnitPercent), 0) as bigint)
                FROM (
                    SELECT *
                    FROM #tempOwnerExemptValue
                    WHERE exemptionTypeCd is null  -- has no other exemption    
                    AND ownerValue < 500
                ) a
                GROUP BY appraisalYear, taxUnitAbbr

            --==== # of Owners Under $500 ======
            INSERT INTO #tempResult
                (itemName, appraisalYear, taxUnit, number)
                SELECT '# of Owners-UNDER $500', appraisalYear, taxUnitAbbr
                    , COUNT(1) numOfOwners
                FROM (
                    SELECT DISTINCT appraisalYear, taxUnitAbbr, ownerId
                    FROM #tempOwnerExemptValue
                    WHERE exemptionTypeCd is null  -- has no other exemption    
                    AND ownerValue < 500
                ) a
                GROUP BY appraisalYear, taxUnitAbbr

            --==== Taxible Value ======
            INSERT INTO #tempResult
                (itemName, appraisalYear, taxUnit, number)
                SELECT 'Taxable Value', a.appraisalYear, a.taxUnit
                    , a.appraisedValue-ISNULL(b.totalExemptValue,0)-ISNULL(c.disableVetExemptValue,0)-ISNULL(d.under500Value,0)
                FROM (
                    SELECT appraisalYear, taxUnit, number appraisedValue
                    FROM #tempResult
                    WHERE itemName = 'Total Appraised Value'     --Totally Exempt Value
                ) a
                LEFT JOIN (
                    SELECT appraisalYear, taxUnit, number totalExemptValue
                    FROM #tempResult
                    WHERE itemName = 'Totally Exempt Value'
                ) b
                ON a.appraisalYear = b.appraisalYear
                AND a.taxUnit = b.taxUnit
                LEFT JOIN (
                    SELECT appraisalYear, taxUnitAbbr
                        , CAST(ROUND(SUM(ownerValue*taxUnitPercent), 0) as bigint) disableVetExemptValue
                    FROM #tempOwnerExemptValue
                    WHERE exemptionTypeCd = 3440  -- disable vet exemption  
                    GROUP BY appraisalYear, taxUnitAbbr
                ) c
                ON a.appraisalYear = c.appraisalYear
                AND a.taxUnit = c.taxUnitAbbr   
                LEFT JOIN (
                    SELECT appraisalYear, taxUnit, number under500Value
                    FROM #tempResult
                    WHERE itemName = 'Under $500 Exempt'
                ) d
                ON a.appraisalYear = d.appraisalYear
                AND a.taxUnit = d.taxUnit

             select * from #tempResult
        `;
        
        UtilFunctions.execSql(sql, res);
    }
};