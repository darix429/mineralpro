module.exports = {
	Read: function (req, res) {
		var UtilFunctions = require('../../UtilFunctions.js');
		var year = req.query.appraisalYear;
        if(!year){
            res.status(400).json({status: false, error: 'Appraisal Year is required.'})
        }
		var sql = `
			DECLARE @appraisalYear smallint = ${year};
			DECLARE @prevAppraisal bigint;

			--=== create a temp table for holding the data ----
			IF OBJECT_ID(N'tempdb..#tempResult',N'U') IS NOT NULL
			DROP TABLE #tempResult

			IF OBJECT_ID(N'tempdb..#tempAppraisals',N'U') IS NOT NULL
			DROP TABLE #tempAppraisals

			CREATE TABLE #tempResult (
				appraisalYear smallint null,
				rrcNumber int null,
				variance float null,
				fifthYear bigint null,
				fourthYear bigint null,
				thirdYear bigint null,
				secondYear bigint null,
				firstYear bigint null
			)
			INSERT INTO #tempResult(appraisalYear, rrcNumber, variance, fifthYear, fourthYear, thirdYear, secondYear, firstYear)
			SELECT appraisalYear, rrcNumber
			, CASE WHEN five - four = 0 OR four = 0 THEN 0 ELSE (five-four)/four END variance
			, five, four, three, two, one
			FROM 
			(
				SELECT  a.appraisalYear, a.rrcNumber
				, ISNULL((select sum(totalValue) from appraisal WHERE rrcNumber = a.rrcNumber AND appraisalYear = a.appraisalYear GROUP BY appraisalYear),0) five
				, ISNULL((select sum(totalValue) from appraisal WHERE rrcNumber = a.rrcNumber AND appraisalYear = a.appraisalYear - 1 GROUP BY appraisalYear),0) four
				, ISNULL((select sum(totalValue) from appraisal WHERE rrcNumber = a.rrcNumber AND appraisalYear = a.appraisalYear - 2 GROUP BY appraisalYear),0) three
				, ISNULL((select sum(totalValue) from appraisal WHERE rrcNumber = a.rrcNumber AND appraisalYear = a.appraisalYear - 3 GROUP BY appraisalYear),0) two
				, ISNULL((select sum(totalValue) from appraisal WHERE rrcNumber = a.rrcNumber AND appraisalYear = a.appraisalYear - 4 GROUP BY appraisalYear),0) one
				FROM Appraisal a
				WHERE appraisalYear = @appraisalYear
				GROUP BY a.appraisalYear, a.rrcNumber
			)tbl

			select appraisalYear, rrcNumber, ISNULL(variance, 0) variance, fifthYear, fourthYear, thirdYear, secondYear, firstYear
			from #tempResult
		`;
		UtilFunctions.execSql(sql, res);
	}
}