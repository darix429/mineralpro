
module.exports = function(app, socket){
    var GIT = require("./Git.js");
	var CrystalReports = require("./CrystalReports.js");
	app.get("/api/Crystal/syncToDB", CrystalReports.syncToDB);
      
//REQUIRE FILE
    var UtilFunctions = require('./UtilFunctions.js');
    var GetApp = require("./CpHome/CPHome.js");

//---------------Packages - CPRegion js File----------------
    var GetHeaderData = require('./Packages/CPRegion/GetHeaderData.js');
    var GetUserFunction = require('./Packages/CPRegion/GetUserFunction.js');
    var ProcessFavoriteTree = require('./Packages/CPRegion/ProcessFavoriteTree.js');
    var GetFavoriteTree = require('./Packages/CPRegion/GetFavoriteTree.js');
    var GetMenuTree = require('./Packages/CPRegion/GetMenuTree.js');
    var GetJEData = require('./Packages/CPRegion/GetJEData.js');
    var CreateJEData = require('./Packages/CPRegion/CreateJEData.js');										
    var ChangeAppraisalYear = require('./Packages/CPRegion/ChangeAppraisalYear.js');										

//---------------Packages - CPLogin js File-----------------
    var GetUser = require('./Packages/CPLogin/GetUser.js');  
    
//---------------Packages - CommonFunctionSearch js File --------------    
    var ReadMineralSearch = require("./Packages/MineralPro/CommonFunctionsSearch/ReadMineralSearch.js");   

//---------------Packages - MineralPro js File-----------------
    var ReadCountry = require('./Packages/MineralPro/ReadCountry.js');  
    var ReadState = require('./Packages/MineralPro/ReadState.js');  
    var ReadAppraisalYear = require('./Packages/MineralPro/ReadAppraisalYear.js'); 
	var ReadJeHoldCodeType = require('./Packages/MineralPro/ReadJeHoldCodeType.js');
    var ReadReasonCodeType = require('./Packages/MineralPro/ReadReasonCodeType.js');
    var ReadTaxOfficeNotifyCodeType = require('./Packages/MineralPro/ReadTaxOfficeNotifyCodeType.js');	
    // var SendMailToDevs = require('./Packages/MineralPro/SendMailToDevs.js');
//---------------ProperRecord js File-----------------
    var ReadAgency = require('./Minerals/ProperRecords/ReadAgency.js'); 
    var ReadOwner = require('./Minerals/ProperRecords/ReadOwner.js');   
    var ReadExemption = require('./Minerals/ProperRecords/ReadExemption.js');   

//---------------InformationService - OrganizationList js File --------------
    var ReadOrganizationList = require('./InformationServices/OrganizationOrganizationList/ReadOrganization.js');
    var CreateOrganization = require('./InformationServices/OrganizationOrganizationList/CreateOrganization.js');
    var UpdateOrganization = require('./InformationServices/OrganizationOrganizationList/UpdateOrganization.js');

//---------------InformationService - SecurityUserList js File --------------
    var ReadOrgContact = require('./InformationServices/SystemAdminSecuritySecurityUserList/ReadOrgContact.js');
    var ReadOrganizationName = require('./InformationServices/SystemAdminSecuritySecurityUserList/ReadOrganizationName.js');
    var ReadSecurityGroup = require('./InformationServices/SystemAdminSecuritySecurityUserList/ReadSecurityGroup.js');
    var ReadUserList = require('./InformationServices/SystemAdminSecuritySecurityUserList/ReadUserList.js');
    var CreateUserList = require('./InformationServices/SystemAdminSecuritySecurityUserList/CreateUserList.js');
    var UpdateUserList = require('./InformationServices/SystemAdminSecuritySecurityUserList/UpdateUserList.js');

//---------------InformationService - SecurityUserGroup js File --------------
    var ReadGroupList = require('./InformationServices/SystemAdminSecuritySecurityGroup/ReadGroupList.js');
    var ReadSecurityGroup = require('./InformationServices/SystemAdminSecuritySecurityGroup/ReadSecurityGroup.js');
    var CreateGroupList = require('./InformationServices/SystemAdminSecuritySecurityGroup/CreateGroupList.js');
    var UpdateGroupList = require('./InformationServices/SystemAdminSecuritySecurityGroup/UpdateGroupList.js');

//---------------InformationService - SystemAdminSecuritySecurityFunctionList js File --------------    
    var ReadSecurityFunctionListName = require("./InformationServices/SystemAdminSecuritySecurityFunctionList/ReadSecurityFunctionListName.js");
    var ReadSecurityFunctionList = require("./InformationServices/SystemAdminSecuritySecurityFunctionList/ReadSecurityFunctionList.js");
    var UpdateSecurityFunctionList = require("./InformationServices/SystemAdminSecuritySecurityFunctionList/UpdateSecurityFunctionList.js");
    var CreateSecurityFunctionList = require("./InformationServices/SystemAdminSecuritySecurityFunctionList/CreateSecurityFunctionList.js");

//---------------InformationService - OrganizationOrganizationContact js File --------------  
    var UpdateOrganizationContact = require("./InformationServices/OrganizationOrganizationContact/UpdateOrganizationContact.js")
    var ReadOrganizationContact = require("./InformationServices/OrganizationOrganizationContact/ReadOrganizationContact.js")
    var ReadOrganization = require("./InformationServices/OrganizationOrganizationContact/ReadOrganization.js")
    var CreateOrganizationContact = require("./InformationServices/OrganizationOrganizationContact/CreateOrganizationContact.js")

//---------------InformationService - SystemAdminCodeType js File --------------    
    var ReadCodeType = require("./InformationServices/SystemAdminCodeType/ReadCodeType.js");
    var CreateCodeType = require("./InformationServices/SystemAdminCodeType/CreateCodeType.js");
    var UpdateCodeType = require("./InformationServices/SystemAdminCodeType/UpdateCodeType.js");
    var ReadCodeTypeCombo = require("./InformationServices/SystemAdminCodeType/ReadCodeTypeCombo.js");

//---------------InformationService - InformationServices/readStatusCodeType js File --------------    
    var ReadStatusCodeType = require("./InformationServices/readStatusCodeType.js");        
    
//---------------ProperRecords - ProperRecordsManageOperator js File --------------    
    var ReadManageOperator = require("./Minerals/ProperRecords/ProperRecordsManageOperator/ReadManageOperator.js");
    var CreateManageOperator = require("./Minerals/ProperRecords/ProperRecordsManageOperator/CreateManageOperator.js");
    var UpdateManageOperator = require("./Minerals/ProperRecords/ProperRecordsManageOperator/UpdateManageOperator.js");   
    
//---------------ProperRecords - ProperRecordsManageField js File --------------    
    var ReadManageField = require("./Minerals/ProperRecords/ProperRecordsManageField/ReadManageField.js");
    var CreateManageField = require("./Minerals/ProperRecords/ProperRecordsManageField/CreateManageField.js");
    var UpdateManageField = require("./Minerals/ProperRecords/ProperRecordsManageField/UpdateManageField.js"); 

//---------------ProperRecords - ProperRecordsManageOwner js File --------------    
    var ReadManageOwner = require("./Minerals/ProperRecords/ProperRecordsManageOwner/ReadManageOwner.js");
    var ReadManageOwnerHeaderDetail = require("./Minerals/ProperRecords/ProperRecordsManageOwner/ReadManageOwnerHeaderDetail.js");
    var CreateManageOwner = require("./Minerals/ProperRecords/ProperRecordsManageOwner/CreateOwner.js");
    var ReadChangeReason = require('./Minerals/ProperRecords/ProperRecordsManageOwner/ReadChangeReason.js');  
    var ReadOwnerType = require('./Minerals/ProperRecords/ProperRecordsManageOwner/ReadOwnerType.js');  
    var UpdateOwner = require('./Minerals/ProperRecords/ProperRecordsManageOwner/UpdateOwner.js');    

//---------------ProperRecords - ProperRecordsManageOwnerNote js File --------------    
    var ReadManageNote = require('./Minerals/ProperRecords/ProperRecordsManageNote/ReadManageNote.js'); 
    var ReadQuickNote = require('./Minerals/ProperRecords/ProperRecordsManageNote/ReadQuickNote.js'); 
    var ReadNoteType = require('./Minerals/ProperRecords/ProperRecordsManageNote/ReadNoteType.js'); 
    var CreateManageNote = require('./Minerals/ProperRecords/ProperRecordsManageNote/CreateManageNote.js'); 
    var UpdateManageNote = require('./Minerals/ProperRecords/ProperRecordsManageNote/UpdateManageNote.js'); 
    
//---------------ProperRecords - ProperRecordsManageOwnerNote js File --------------    
    var ReadOwnerNoteViewType = require("./Minerals/ProperRecords/ProperRecordsManageOwner/ReadOwnerNoteViewType.js");
    var ReadManageOwnerNote = require('./Minerals/ProperRecords/ProperRecordsManageOwner/ReadManageOwnerNote.js'); 
    var CreateManageOwnerNote = require('./Minerals/ProperRecords/ProperRecordsManageOwner/CreateManageOwnerNote.js'); 
    var UpdateManageOwnerNote = require('./Minerals/ProperRecords/ProperRecordsManageOwner/UpdateManageOwnerNote.js'); 

//---------------ProperRecords - ProperRecordsManageAgent js File --------------    
    var ReadManageAgent = require("./Minerals/ProperRecords/ProperRecordsManageAgent/ReadManageAgent.js");
    var CreateManageAgent = require("./Minerals/ProperRecords/ProperRecordsManageAgent/CreateManageAgent.js");  
    var UpdateManageAgent = require("./Minerals/ProperRecords/ProperRecordsManageAgent/UpdateManageAgent.js");  

//---------------ProperRecords - ProperRecordsManageJournalEntry js File --------------    
    var ReadJEBatches = require("./Minerals/ProperRecords/ProperRecordsManageJournalEntry/ReadJEBatches.js");
    var ReadJournalEntries = require("./Minerals/ProperRecords/ProperRecordsManageJournalEntry/ReadJournalEntries.js");  
    var ReadLeaseOwners = require("./Minerals/ProperRecords/ProperRecordsManageJournalEntry/ReadLeaseOwners.js");
    var UpdateJournalEntries = require("./Minerals/ProperRecords/ProperRecordsManageJournalEntry/UpdateJournalEntries.js");
    var UpdateJEBatches = require("./Minerals/ProperRecords/ProperRecordsManageJournalEntry/UpdateJEBatches.js");
	
//---------------Minerals - MineralValuationManageLease js File --------------    
    var ReadLeaseOwner = require("./Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseOwner.js");
    var ReadLeaseOwnersTransferLease = require("./Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseOwnersTransferLease.js");
    // var CreateLeaseOwner = require("./Minerals/MineralValuation/MineralValuationManageLease/CreateLeaseOwner.js");  
    // var UpdateLeaseOwner = require("./Minerals/MineralValuation/MineralValuationManageLease/UpdateLeaseOwner.js");


//---------------ProperRecords - ProperRecordManageOwnerExemption js File --------------    

    var ReadTotExemption = require('./Minerals/ProperRecords/ProperRecordsManageOwnerExemption/ReadTotExemption.js');    
    var ReadDisableVet = require('./Minerals/ProperRecords/ProperRecordsManageOwnerExemption/ReadDisableVet.js');  

    var ReadOwnerExemption = require("./Minerals/ProperRecords/ProperRecordsManageOwnerExemption/ReadOwnerExemption.js");
    var CreateOwnerExemption = require("./Minerals/ProperRecords/ProperRecordsManageOwnerExemption/CreateOwnerExemption.js");
    var UpdateOwnerExemption = require("./Minerals/ProperRecords/ProperRecordsManageOwnerExemption/UpdateOwnerExemption.js");
    var ReadExemptionType = require("./Minerals/ProperRecords/ProperRecordsManageOwnerExemption/ReadExemptionType.js");
    var ReadExemptionTypeCode = require("./Minerals/ProperRecords/ProperRecordsManageOwnerExemption/ReadExemptionTypeCode.js");

//---------------ProperRecords - Lease --------------    
    var ReadOwnerLeaseHoldings = require("./Minerals/ProperRecords/ProperRecordsManageOwnerLeaseHoldings/ReadOwnerLeaseHoldings.js");
    var ReadOwnerLeaseHoldingsLease = require("./Minerals/ProperRecords/ProperRecordsManageOwnerLeaseHoldings/ReadOwnerLeaseHoldingsLease.js");
    var ReadOwnerLeaseHoldingsOwnerTransfer = require("./Minerals/ProperRecords/ProperRecordsManageOwnerLeaseHoldings/ReadOwnerLeaseHoldingsOwnerTransfer.js");
    var ReadOwnerTaxUnit = require("./Minerals/ProperRecords/ProperRecordsManageOwnerTaxUnit/ReadOwnerTaxUnit.js");
	
//---------------Minerals - MineralValuation/MineralValuationManageLease js File --------------    
    var ReadLease = require("./Minerals/MineralValuation/MineralValuationManageLease/ReadLease.js");
    var ReadLeaseHeaderDetail = require("./Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseHeaderDetail.js");
    var CreateLease = require("./Minerals/MineralValuation/MineralValuationManageLease/CreateLease.js");
    var UpdateLease = require("./Minerals/MineralValuation/MineralValuationManageLease/UpdateLease.js");
    var UploadLeaseOwner = require("./Minerals/MineralValuation/MineralValuationManageLease/UploadLeaseOwner.js");

////---------------Minerals - TaxUnit --------------
    var ReadLeaseTaxUnit = require("./Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseTaxUnit.js");
    var ReadLeaseTaxUnitType = require("./Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseTaxUnitType.js");
    var ReadLeaseTaxUnitCode = require("./Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseTaxUnitCode.js");
    var CreateLeaseTaxUnit = require("./Minerals/MineralValuation/MineralValuationManageLease/CreateLeaseTaxUnit.js");
    var UpdateLeaseTaxUnit = require("./Minerals/MineralValuation/MineralValuationManageLease/UpdateLeaseTaxUnit.js");

//---------------Minerals - MineralValuation/MineralValuationManageUnit js File --------------    
    var ReadUnits = require("./Minerals/MineralValuation/MineralValuationManageUnit/ReadUnits.js");
    var ReadUnitHeaderDetail = require("./Minerals/MineralValuation/MineralValuationManageUnit/ReadUnitHeaderDetail.js");
    var CreateUnits = require("./Minerals/MineralValuation/MineralValuationManageUnit/CreateUnits.js");
    var UpdateUnits = require("./Minerals/MineralValuation/MineralValuationManageUnit/UpdateUnits.js");
    var ReadUnitLeases = require("./Minerals/MineralValuation/MineralValuationManageUnit/ReadUnitLeases.js");
    var ReadUnitLeasesLease = require("./Minerals/MineralValuation/MineralValuationManageUnit/ReadUnitLeasesLease.js");
    var UpdateUnitLeases = require("./Minerals/MineralValuation/MineralValuationManageUnit/UpdateUnitLeases.js"); 
    var CalculateUnitLeases = require("./Minerals/MineralValuation/MineralValuationManageUnit/CalculateUnitLeases.js");  
    var CreateUnitLeases = require("./Minerals/MineralValuation/MineralValuationManageUnit/CreateUnitLeases.js");  

//---------------Minerals - MineralValuation/MineralValuationAppraisal js File --------------    
    var ReadAppraisal = require("./Minerals/MineralValuation/MineralValuationAppraisal/ReadAppraisal.js");
    var CreateAppraisal = require("./Minerals/MineralValuation/MineralValuationAppraisal/CreateAppraisal.js");
    var UpdateAppraisal = require("./Minerals/MineralValuation/MineralValuationAppraisal/UpdateAppraisal.js");
    var CalculateAppraisal = require("./Minerals/MineralValuation/MineralValuationAppraisal/CalculateAppraisal.js");
    var ReadYearAnalysisLink = require("./Minerals/MineralValuation/MineralValuationAppraisal/ReadYearAnalysisLink.js");
    
//---------------Minerals - MineralValuation/MineralValuationManageAppraisal js File --------------    
    var ReadManageAppraisal = require("./Minerals/MineralValuation/MineralValuationManageAppraisal/ReadManageAppraisal.js");
    var ReadManageAppraisalSubjectId = require("./Minerals/MineralValuation/MineralValuationManageAppraisal/ReadManageAppraisalSubjectId.js");
    var CreateManageAppraisal = require("./Minerals/MineralValuation/MineralValuationManageAppraisal/CreateManageAppraisal.js");
    var UpdateManageAppraisal = require("./Minerals/MineralValuation/MineralValuationManageAppraisal/UpdateManageAppraisal.js");     
    var ReadManageAppraisalDecline = require("./Minerals/MineralValuation/MineralValuationManageAppraisal/ReadManageAppraisalDecline.js");
    var CreateManageAppraisalDecline = require("./Minerals/MineralValuation/MineralValuationManageAppraisal/CreateManageAppraisalDecline.js");
    var UpdateManageAppraisalDecline = require("./Minerals/MineralValuation/MineralValuationManageAppraisal/UpdateManageAppraisalDecline.js");
    var DeleteManageAppraisalDecline = require("./Minerals/MineralValuation/MineralValuationManageAppraisal/DeleteManageAppraisalDecline.js");    
    var ReadManageAppraisalSchedule = require("./Minerals/MineralValuation/MineralValuationManageAppraisal/ReadManageAppraisalSchedule.js");
    
    
//---------------Minerals - MineralValuation/MineralValuationFields js File --------------    
    var ReadFields = require("./Minerals/MineralValuation/MineralValuationFields/ReadFields.js");        

//---------------Minerals - InformationServices/readReportExport js File --------------    
    var ReadExportReport = require("./InformationServices/readReportExport.js"); 
    var ReadExportReportId = require("./InformationServices/readReportExportId.js"); 
    var ReadExportExport = require("./InformationServices/readReportExportExport.js"); 

//---------------Minerals - InformationServices/SystemFunctionsUploadHPDIData js File --------------    
    var ReadUploadHPDIData = require("./InformationServices/SystemFunctionsUploadHPDIData/ReadUploadHPDIData.js"); 
    var UpdateUploadHPDIData = require("./InformationServices/SystemFunctionsUploadHPDIData/UpdateUploadHPDIData.js"); 
    var CreateUploadHPDIData = require("./InformationServices/SystemFunctionsUploadHPDIData/CreateUploadHPDIData.js");     
    var UploadFile = require("./InformationServices/SystemFunctionsUploadHPDIData/UploadFile.js");    

//---------------Minerals - InformationServices/readReportExport js File --------------    
    var CreateCADReport = require("./InformationServices/ReportsExportsCADReports/createReportExportCAD.js");  
    var UpdateCADReport = require("./InformationServices/ReportsExportsCADReports/updateReportExportCAD.js");  
    var ExportRecord = require("./InformationServices/ReportsExportsCADReports/exportRecord.js");  
     
    var CreateStateReport = require("./InformationServices/ReportsExportsStateReports/createReportExportState.js");  
    var UpdateStateReport = require("./InformationServices/ReportsExportsStateReports/updateReportExportState.js");  
    
//---------------Minerals - ProperRecords/ProperRecordsLeaseOwner js File --------------    
    var CreateLeaseOwner = require("./Minerals/ProperRecords/ProperRecordsLeaseOwner/CreateLeaseOwner.js");
    var UpdateLeaseOwner = require("./Minerals/ProperRecords/ProperRecordsLeaseOwner/UpdateLeaseOwner.js");
    var TransferLeaseOwner = require("./Minerals/ProperRecords/ProperRecordsLeaseOwner/TransferLeaseOwner.js");


//----------------JEReports - JEReport/generateReport.js    
    var JEReport = require("./JEReport/generateReport.js");
	
	//GENERIC READS
	var ReadOwnerLeaseHoldingsInterestType = require("./Minerals/ProperRecords/ProperRecordsManageOwnerLeaseHoldings/ReadOwnerLeaseHoldingsInterestType.js");
	var ReadOperatorCombo = require("./Minerals/MineralValuation/ReadOperatorCombo.js");
        var ReadFieldCombo = require("./Minerals/MineralValuation/ReadFieldCombo.js");
        var ReadGravityCode = require("./Minerals/ProperRecords/ReadGravityCode.js"); 
	var ReadLeaseStatus = require("./Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseStatus.js");
	var ReadLeaseOwnerInterestType = require("./Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseOwnerInterestType.js");
	var ReadChangeReason2 = require("./Minerals/MineralValuation/MineralValuationManageLease/ReadChangeReason.js");
	var ReadWellTypeCombo = require("./Minerals/MineralValuation/ReadWellTypeCombo.js");

//---------------ChiefAppraiser - DashboardShowOverallSummary js File --------------    
    var ReadAppraisalProgress = require("./ChiefAppraiser/DashboardShowOverallSummary/ReadAppraisalProgress.js");
    var ReadAppraisalProgressDetails = require("./ChiefAppraiser/DashboardShowOverallSummary/ReadAppraisalProgressDetails.js");
    var ReadTenYrAppraisal = require("./ChiefAppraiser/DashboardShowOverallSummary/ReadTenYrAppraisal");
    var ReadFiveYrVariance = require("./ChiefAppraiser/DashboardShowOverallSummary/ReadFiveYrVariance");
    var ReadNumberOfJEs = require("./ChiefAppraiser/DashboardShowOverallSummary/ReadNumberOfJEs");
//API
//-------------CRUD Listeners for Package MineralPro (use Get for read and Post for others)----------------
    app.get('/api/Packages/MineralPro/ReadCountry', ReadCountry.ReadCountry);
    app.get('/api/Packages/MineralPro/ReadState', ReadState.ReadState);
    app.get('/api/Packages/MineralPro/ReadAppraisalYear', ReadAppraisalYear.ReadAppraisalYear);
	app.get('/api/Packages/MineralPro/ReadJeHoldCodeType', ReadJeHoldCodeType.ReadJeHoldCodeType);
    app.get('/api/Packages/MineralPro/ReadReasonCodeType', ReadReasonCodeType.ReadReasonCodeType);
    app.get('/api/Packages/MineralPro/ReadTaxOfficeNotifyCodeType', ReadTaxOfficeNotifyCodeType.ReadTaxOfficeNotifyCodeType);

//---------------Minerals - SendEmailToDevs js File --------------    
    // app.post('/api/Packages/MineralPro/SendMailToDevs', SendMailToDevs.SendMailToDevs);


//-------------CRUD Listeners for ProperRecords (use Get for read and Post for others)----------------
    app.get('/api/Minerals/ProperRecords/ReadAgency', ReadAgency.ReadAgency);
    app.get('/api/Minerals/ProperRecords/ReadOwnerCombo', ReadOwner.ReadOwner);
    app.get('/api/Minerals/ProperRecords/ReadExemption', ReadExemption.Read);

//-------------CRUD Listeners for OrganizationList (use Get for read and Post for others)----------------
    app.get('/api/CPHome/GetNavigationTab', GetApp.GetApp);

//---------------CRUD Listeners for SystemAdminCodeType)--------------
    app.get('/api/InformationServices/SystemAdminCodeType/ReadCodeType',ReadCodeType.ReadCodeType);
    app.post('/api/InformationServices/SystemAdminCodeType/CreateCodeType',CreateCodeType.CreateCodeType);
    app.post('/api/InformationServices/SystemAdminCodeType/UpdateCodeType',UpdateCodeType.UpdateCodeType);
    app.get('/api/InformationServices/SystemAdminCodeType/ReadCodeTypeCombo',ReadCodeTypeCombo.ReadCodeTypeCombo);

//----------CRUD Listeners for CPRegion Use (use Get for read and Post for others)---------------
    app.get('/api/Packages/CPRegion/GetHeaderData', GetHeaderData.GetHeaderData);
    app.get('/api/Packages/CPRegion/GetUserFunction', GetUserFunction.GetUserFunction);    
    app.get('/api/Packages/CPRegion/ProcessFavoriteTree', ProcessFavoriteTree.ProcessFavoriteTree);    
    app.get('/api/Packages/CPRegion/GetFavoriteTree', GetFavoriteTree.GetFavoriteTree);    
    app.get('/api/Packages/CPRegion/GetMenuTree', GetMenuTree.GetMenuTree);
    app.get('/api/Packages/CPRegion/GetJEData', GetJEData.GetJEData);
    app.post('/api/Packages/CPRegion/CreateJEData', CreateJEData.CreateJEData);
    app.post('/api/Packages/CPRegion/ChangeAppraisalYear', ChangeAppraisalYear.ChangeAppraisalYear);
    
//----------CRUD Listeners for CPLogin Use (use Get for read and Post for others)---------------
    app.all('/api/Packages/CPLogin/GetUser', GetUser.GetUser);

//-------------CRUD Listeners for OrganizationList (use Get for read and Post for others)----------------
    app.get('/api/InformationServices/OrganizationOrganizationList/ReadOrganizationList', ReadOrganizationList.ReadOrganization);
    app.post('/api/InformationServices/OrganizationOrganizationList/CreateOrganizationList', CreateOrganization.CreateOrganization);
    app.post('/api/InformationServices/OrganizationOrganizationList/UpdateOrganizationList', UpdateOrganization.UpdateOrganization);

//---------------CRUD Listeners for SecurityUserList (use Get for read and Post for others))--------------
    app.get('/api/InformationService/SystemAdminSecuritySecurityUserList/ReadOrgContact', ReadOrgContact.ReadOrgContact);    
    app.get('/api/InformationService/SystemAdminSecuritySecurityUserList/ReadOrganizationName', ReadOrganizationName.ReadOrganizationName);
    app.get('/api/InformationService/SystemAdminSecuritySecurityUserList/ReadSecurityGroup', ReadSecurityGroup.ReadSecurityGroup);
    app.get('/api/InformationService/SystemAdminSecuritySecurityUserList/ReadUserList', ReadUserList.ReadUserList);
    app.post('/api/InformationService/SystemAdminSecuritySecurityUserList/CreateUserList', CreateUserList.CreateUserList);
    app.post('/api/InformationService/SystemAdminSecuritySecurityUserList/UpdateUserList', 
        function(req, res){
            UpdateUserList.UpdateUserList(req, res, socket)
        });

//---------------CRUD Listeners for SecurityUGroup (use Get for read and Post for others))--------------
    app.get('/api/InformationService/SystemAdminSecuritySecurityGroup/ReadGroupList', ReadGroupList.ReadGroupList);    
    app.get('/api/InformationService/SystemAdminSecuritySecurityGroup/ReadSecurityGroup', ReadSecurityGroup.ReadSecurityGroup);
    app.post('/api/InformationService/SystemAdminSecuritySecurityGroup/CreateGroupList', CreateGroupList.CreateGroupList);    
    app.post('/api/InformationService/SystemAdminSecuritySecurityGroup/UpdateGroupList', UpdateGroupList.UpdateGroupList);

//---------------CRUD Listeners for CommonFunctionSearch (use Get for read and Post for others))--------------
    app.get('/api/Packages/MineralPro/CommonFunctionSearch/ReadMineralSearch',ReadMineralSearch.ReadMineralSearch); // waiting for comfirming the source table

//---------------CRUD Listeners for SystemAdminSecuritySecurityFunctionList (use Get for read and Post for others))--------------
    app.get('/api/InformationServices/SystemAdminSecuritySecurityFunctionList/ReadSecurityFunctionListName',ReadSecurityFunctionListName.ReadSecurityFunctionListName);
    app.get('/api/InformationServices/SystemAdminSecuritySecurityFunctionList/ReadSecurityFunctionList',ReadSecurityFunctionList.ReadSecurityFunctionList);
    app.post('/api/InformationServices/SystemAdminSecuritySecurityFunctionList/UpdateSecurityFunctionList',UpdateSecurityFunctionList.UpdateSecurityFunctionList);
    app.post('/api/InformationServices/SystemAdminSecuritySecurityFunctionList/CreateSecurityFunctionList',CreateSecurityFunctionList.CreateSecurityFunctionList);

//---------------CRUD Listeners for OrganizationOrganizationContact (use Get for read and Post for others))--------------
    app.get('/api/InformationServices/OrganizationOrganizationContact/ReadOrganization',ReadOrganization.ReadOrganization);
    app.get('/api/InformationServices/OrganizationOrganizationContact/ReadOrganizationContact',ReadOrganizationContact.ReadOrganizationContact);
    app.post('/api/InformationServices/OrganizationOrganizationContact/UpdateOrganizationContact',UpdateOrganizationContact.UpdateOrganizationContact);
    app.post('/api/InformationServices/OrganizationOrganizationContact/CreateOrganizationContact',CreateOrganizationContact.CreateOrganizationContact);

//---------------CRUD Listeners for InformationServices/OrganizationOrganizationContact/ReadStatusCodeType (use Get for read and Post for others))--------------   
    app.get('/api/InformationServices/OrganizationOrganizationContact/ReadStatusCodeType',ReadStatusCodeType.read);

//---------------CRUD Listeners for ProperRecords/ProperRecordsManageOperator (use Get for read and Post for others))--------------
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageOperator/ReadManageOperator',ReadManageOperator.ReadManageOperator);
    app.post('/api/Minerals/ProperRecords/ProperRecordsManageOperator/CreateManageOperator',CreateManageOperator.CreateManageOperator);
    app.post('/api/Minerals/ProperRecords/ProperRecordsManageOperator/UpdateManageOperator',UpdateManageOperator.UpdateManageOperator);

//---------------CRUD Listeners for ProperRecords/ProperRecordsManageField (use Get for read and Post for others))--------------
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageField/ReadManageField',ReadManageField.ReadManageField);
    app.post('/api/Minerals/ProperRecords/ProperRecordsManageField/CreateManageField',CreateManageField.CreateManageField);
    app.post('/api/Minerals/ProperRecords/ProperRecordsManageField/UpdateManageField',UpdateManageField.UpdateManageField);

//---------------CRUD Listeners for ProperRecords/ProperRecordsManageOwner (use Get for read and Post for others))--------------
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageOwner/ReadManageOwner',ReadManageOwner.ReadManageOwner);
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageOwner/ReadManageOwnerHeaderDetail',ReadManageOwnerHeaderDetail.ReadManageOwnerHeaderDetail); 
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageOwner/ReadChangeReason', ReadChangeReason.ReadChangeReason);
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageOwner/ReadOwnerType', ReadOwnerType.ReadOwnerType);
    app.post('/api/Minerals/ProperRecords/ProperRecordsManageOwner/CreateOwner',CreateManageOwner.CreateOwner);
    app.post('/api/Minerals/ProperRecords/ProperRecordsManageOwner/UpdateOwner',UpdateOwner.UpdateOwner);

//---------------CRUD Listeners for ProperRecords/ProperRecordsManageNote (use Get for read and Post for others))--------------
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageNote/ReadManageNote',ReadManageNote.Read);
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageNote/ReadQuickNote',ReadQuickNote.Read);
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageNote/ReadNoteType',ReadNoteType.Read);
    app.post('/api/Minerals/ProperRecords/ProperRecordsManageNote/CreateManageNote',CreateManageNote.Create);
    app.post('/api/Minerals/ProperRecords/ProperRecordsManageNote/UpdateManageNote',UpdateManageNote.Update);

//---------------CRUD Listeners for ProperRecords/ProperRecordsManageOwnerNote (use Get for read and Post for others))--------------
    app.get('/api/Minerals/ProperRecords/ProperRecordsOwner/ReadOwnerNoteViewType',ReadOwnerNoteViewType.ReadOwnerNoteViewType);
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageOwner/ReadManageOwnerNote',ReadManageOwnerNote.ReadManageOwnerNote);
    app.post('/api/Minerals/ProperRecords/ProperRecordsOwner/CreateManageOwnerNote',CreateManageOwnerNote.CreateManageOwnerNote);
    app.post('/api/Minerals/ProperRecords/ProperRecordsOwner/UpdateManageOwnerNote',UpdateManageOwnerNote.UpdateManageOwnerNote);
  
//---------------CRUD Listeners for ProperRecords/ProperRecordsManageAgent (use Get for read and Post for others))--------------
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageAgent/ReadManageAgent',ReadManageAgent.ReadManageAgent);
    app.post('/api/Minerals/ProperRecords/ProperRecordsManageAgent/CreateManageAgent',CreateManageAgent.CreateManageAgent);
    app.post('/api/Minerals/ProperRecords/ProperRecordsManageAgent/UpdateManageAgent',UpdateManageAgent.UpdateManageAgent);

  
//---------------CRUD Listeners for ProperRecords/ProperRecordsManageOwnerExemption (use Get for read and Post for others))--------------
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageOwnerExemption/ReadExemptionType',ReadExemptionType.ReadExemptionType);
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageOwnerExemption/ReadExemptionTypeCode',ReadExemptionTypeCode.ReadExemptionTypeCode);
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageOwnerExemption/ReadOwnerExemption',ReadOwnerExemption.ReadOwnerExemption);
    app.post('/api/Minerals/ProperRecords/ProperRecordsManageOwnerExemption/CreateOwnerExemption',CreateOwnerExemption.CreateOwnerExemption);
    app.post('/api/Minerals/ProperRecords/ProperRecordsManageOwnerExemption/UpdateOwnerExemption',UpdateOwnerExemption.UpdateOwnerExemption);
    
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageOwnerExemption/ReadTotalExemption',ReadTotExemption.ReadTotExemption);
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageOwnerExemption/ReadDisableVet',ReadDisableVet.ReadDisableVet);

	app.get('/api/Minerals/ProperRecords/ProperRecordsManageOwnerLeaseHoldings/ReadOwnerLeaseHoldings',ReadOwnerLeaseHoldings.Read);
	app.get('/api/Minerals/ProperRecords/ProperRecordsManageOwnerLeaseHoldings/ReadOwnerLeaseHoldingsLease',ReadOwnerLeaseHoldingsLease.Read);
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageOwnerLeaseHoldings/ReadOwnerLeaseHoldingsOwnerTransfer',ReadOwnerLeaseHoldingsOwnerTransfer.Read);
	app.get('/api/Minerals/ProperRecords/ProperRecordsManageOwnerLeaseHoldings/ReadOwnerLeaseHoldingsInterestType',ReadOwnerLeaseHoldingsInterestType.Read);
	app.get('/api/Minerals/ProperRecords/ProperRecordsManageOwnerTaxUnit/ReadOwnerTaxUnit',ReadOwnerTaxUnit.Read);


//---------------CRUD Listeners for Minerals/MineralValuation/MineralValuationManageLease (use Get for read and Post for others))--------------
    app.get('/api/Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseTaxUnit',ReadLeaseTaxUnit.Read);
    app.get('/api/Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseTaxUnitType',ReadLeaseTaxUnitType.Read);
    app.get('/api/Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseTaxUnitCode',ReadLeaseTaxUnitCode.Read);
    app.post('/api/Minerals/MineralValuation/MineralValuationManageLease/CreateLeaseTaxUnit',CreateLeaseTaxUnit.CreateLeaseTaxUnit);
    app.post('/api/Minerals/MineralValuation/MineralValuationManageLease/UpdateLeaseTaxUnit',UpdateLeaseTaxUnit.UpdateLeaseTaxUnit);

    app.get('/api/Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseOwner',ReadLeaseOwner.Read);
    app.post('/api/Minerals/MineralValuation/MineralValuationManageLease/UpdateLeaseOwner',UpdateLeaseOwner.Update);
    app.post('/api/Minerals/MineralValuation/MineralValuationManageLease/CreateLeaseOwner',CreateLeaseOwner.Create);
    app.get('/api/Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseOwnersTransferLease',ReadLeaseOwnersTransferLease.Read);


//---------------CRUD Listeners for Minerals/MineralValuation/MineralValuationManageLease (use Get for read and Post for others))--------------   
    app.get('/api/MineralPro/MineralValuation/MineralValuationManageLease/ReadLease',ReadLease.ReadLease);
    app.get('/api/MineralPro/MineralValuation/MineralValuationManageLease/ReadLeaseHeaderDetail',ReadLeaseHeaderDetail.ReadLeaseHeaderDetail);
    app.post('/api/MineralPro/MineralValuation/MineralValuationManageLease/CreateLease',CreateLease.CreateLease);
    app.post('/api/MineralPro/MineralValuation/MineralValuationManageLease/UpdateLease',UpdateLease.UpdateLease);
    app.post('/api/MineralPro/MineralValuation/MineralValuationManageLease/UploadLeaseOwner',UploadLeaseOwner.UploadLeaseOwner); 

//---------------CRUD Listeners for Minerals/MineralValuation/MineralValuationManageUnit (use Get for read and Post for others))--------------   
    app.get('/api/Minerals/MineralValuation/MineralValuationManageUnit/ReadUnits',ReadUnits.ReadUnits);
    app.get('/api/Minerals/MineralValuation/MineralValuationManageUnit/ReadUnitHeaderDetail',ReadUnitHeaderDetail.ReadUnitHeaderDetail);
    
    app.post('/api/Minerals/MineralValuation/MineralValuationManageUnit/CreateUnits',CreateUnits.CreateUnits);
    app.post('/api/Minerals/MineralValuation/MineralValuationManageUnit/UpdateUnits',UpdateUnits.UpdateUnits);
    app.get('/api/Minerals/MineralValuation/MineralValuationManageUnit/ReadUnitLeases',ReadUnitLeases.ReadUnitLeases);
    app.get('/api/Minerals/MineralValuation/MineralValuationManageUnit/ReadUnitLeasesLease',ReadUnitLeasesLease.ReadUnitLeasesLease);
    app.post('/api/Minerals/MineralValuation/MineralValuationManageUnit/UpdateUnitLeases',UpdateUnitLeases.UpdateUnitLeases);
    app.post('/api/Minerals/MineralValuation/MineralValuationManageUnit/CalculateUnitLeases',CalculateUnitLeases.CalculateUnitLeases);
    app.post('/api/Minerals/MineralValuation/MineralValuationManageUnit/CreateUnitLeases',CreateUnitLeases.CreateUnitLeases);

//---------------CRUD Listeners for Minerals/MineralValuation/MineralValuationAppraisal (use Get for read and Post for others))--------------   
    app.get('/api/Minerals/MineralValuation/MineralValuationAppraisal/ReadAppraisal',ReadAppraisal.Read);
    app.post('/api/Minerals/MineralValuation/MineralValuationAppraisal/CreateAppraisal',CreateAppraisal.Create);
    app.post('/api/Minerals/MineralValuation/MineralValuationAppraisal/UpdateAppraisal',UpdateAppraisal.Update);
    app.all('/api/Minerals/MineralValuation/MineralValuationAppraisal/CalculateAppraisal',CalculateAppraisal.Calculate);
    app.get('/api/Minerals/MineralValuation/MineralValuationAppraisal/ReadYearAnalysisLink',ReadYearAnalysisLink.Read);



//---------------CRUD Listeners for Minerals/MineralValuation/MineralValuationManageAppraisal (use Get for read and Post for others))--------------   
    app.get('/api/Minerals/MineralValuation/MineralValuationManageAppraisal/ReadManageAppraisal',ReadManageAppraisal.ReadManageAppraisal);
    app.get('/api/Minerals/MineralValuation/MineralValuationManageAppraisal/ReadManageAppraisalSubjectId',ReadManageAppraisalSubjectId.ReadManageAppraisalSubjectId);
    app.get('/api/Minerals/MineralValuation/MineralValuationManageAppraisal/ReadManageAppraisalDecline',ReadManageAppraisalDecline.ReadManageAppraisalDecline);
    app.get('/api/Minerals/MineralValuation/MineralValuationManageAppraisal/ReadManageAppraisalSchedule',ReadManageAppraisalSchedule.ReadManageAppraisalSchedule);
    app.post('/api/Minerals/MineralValuation/MineralValuationManageAppraisal/CreateManageAppraisal',CreateManageAppraisal.CreateManageAppraisal);
    app.post('/api/Minerals/MineralValuation/MineralValuationManageAppraisal/UpdateManageAppraisal',UpdateManageAppraisal.UpdateManageAppraisal);
    app.post('/api/Minerals/MineralValuation/MineralValuationManageAppraisal/CreateManageAppraisalDecline',CreateManageAppraisalDecline.CreateManageAppraisalDecline);
    app.post('/api/Minerals/MineralValuation/MineralValuationManageAppraisal/UpdateManageAppraisalDecline',UpdateManageAppraisalDecline.UpdateManageAppraisalDecline);
    app.post('/api/Minerals/MineralValuation/MineralValuationManageAppraisal/DeleteManageAppraisalDecline',DeleteManageAppraisalDecline.DeleteManageAppraisalDecline);
          
//---------------CRUD Listeners for Minerals/MineralValuation/MineralValuationFields (use Get for read and Post for others))--------------   
    app.get('/api/Minerals/MineralValuation/MineralValuationFields/ReadFields',ReadFields.Read);

//---------------CRUD Listeners for InformationServices/ReportsExports (use Get for read and Post for others))--------------   
    app.get('/api/InformationServices/ReportsExports/ReadReports/:cat',ReadExportReport.read);
    app.get('/api/InformationServices/ReportsExports/ReadReports/',ReadExportReportId.read);
    app.get('/api/InformationServices/ReportsExports/ReadExports/',ReadExportExport.read);

//---------------CRUD Listeners for InformationServices/SystemFunctionsUploadHPDIData (use Get for read and Post for others))--------------   
    app.get('/api/InformationServices/SystemFunctionsUploadHPDIData/ReadUploadHPDIData',ReadUploadHPDIData.ReadUploadHPDIData);
    app.post('/api/InformationServices/SystemFunctionsUploadHPDIData/UpdateUploadHPDIData',UpdateUploadHPDIData.UpdateUploadHPDIData);
    app.post('/api/InformationServices/SystemFunctionsUploadHPDIData/CreateUploadHPDIData',CreateUploadHPDIData.create);    
    app.post('/api/InformationServices/SystemFunctionsUploadHPDIData/UploadFile',UploadFile.UploadFile);    

//---------------CRUD Listeners for InformationServices/ReportsExportsCADReports (use Get for read and Post for others))--------------   
    app.post('/api/InformationServices/ReportsExportsCADReports/CreateCADReports',CreateCADReport.createReportExportCAD);
    app.post('/api/InformationServices/ReportsExportsCADReports/UpdateCADReports',UpdateCADReport.updateReportExportCAD);
    
    app.post('/api/InformationServices/ReportsExportsStateReports/CreateStateReports',CreateStateReport.createReportExportState);
    app.post('/api/InformationServices/ReportsExportsStateReports/UpdateStateReports',UpdateStateReport.updateReportExportState);
   
	app.get('/api/InformationServices/ReportsExportsCADReports/ExportPublicRecords',ExportRecord.publicRecords);
	app.get('/api/InformationServices/ReportsExportsCADReports/ExportPublicRecordsSummary',ExportRecord.publicRecordsSummary);
	app.get('/api/InformationServices/ReportsExportsCADReports/ExportPTDSubmission',ExportRecord.PTDSubmission);
	app.get('/api/InformationServices/ReportsExportsCADReports/ExportPTDSubmissionSummary',ExportRecord.PTDSubmissionSummary);
	app.get('/api/InformationServices/ReportsExportsCADReports/ExportCertifiedFormat',ExportRecord.CertifiedFormat);
	app.get('/api/InformationServices/ReportsExportsCADReports/ExportCertifiedFormatSummary',ExportRecord.CertifiedFormatSummary);
	
// -------------CRUD Listeners for ChiefAppraiser(use Get for read and Post for others))--------------
    app.get('/api/ChiefAppraiser/DashboardShowOverallSummary/ReadAppraisalProgress',ReadAppraisalProgress.Read);
    app.get('/api/ChiefAppraiser/DashboardShowOverallSummary/ReadAppraisalProgressDetails',ReadAppraisalProgressDetails.Read);
    app.get('/api/ChiefAppraiser/DashboardShowOverallSummary/ReadTenYrAppraisal',ReadTenYrAppraisal.Read);
    app.get('/api/ChiefAppraiser/DashboardShowOverallSummary/ReadFiveYrVariance',ReadFiveYrVariance.Read);
    app.get('/api/ChiefAppraiser/DashboardShowOverallSummary/ReadNumberOfJEs',ReadNumberOfJEs.Read);

	app.get('/api/download/:filename/:path',ExportRecord.Download);


//---------------CRUD Listeners for Minerals/ProperRecords/ProperRecordsLeaseOwner (use Get for read and Post for others))--------------   
    app.post('/api/Minerals/ProperRecords/ProperRecordsLeaseOwner/CreateLeaseOwner',CreateLeaseOwner.Create);
    app.post('/api/Minerals/ProperRecords/ProperRecordsLeaseOwner/UpdateLeaseOwner',UpdateLeaseOwner.Update);
    app.post('/api/Minerals/ProperRecords/ProperRecordsLeaseOwner/TransferLeaseOwner',TransferLeaseOwner.Update);

//---------------CRUD Listeners for ProperRecords/ProperRecordsManageJournalEntry (use Get for read and Post for others))--------------
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageJournalEntry/ReadJEBatches',ReadJEBatches.Read);
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageJournalEntry/ReadJournalEntries',ReadJournalEntries.Read);
    app.get('/api/Minerals/ProperRecords/ProperRecordsManageJournalEntry/ReadLeaseOwners',ReadLeaseOwners.Read);
    app.post('/api/Minerals/ProperRecords/ProperRecordsManageJournalEntry/UpdateJournalEntries',UpdateJournalEntries.UpdateJournalEntries);
    app.post('/api/Minerals/ProperRecords/ProperRecordsManageJournalEntry/UpdateJEBatches',UpdateJEBatches.UpdateJEBatches);
	
	//GENERIC GET
    app.get('/api/Minerals/MineralValuation/ReadOperatorCombo',ReadOperatorCombo.Read);
    app.get('/api/Minerals/MineralValuation/ReadFieldCombo',ReadFieldCombo.Read);
    app.get('/api/Minerals/ProperRecords/ReadGravityCode',ReadGravityCode.ReadGravityCode);
    app.get('/api/Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseOwnerInterestType',ReadLeaseOwnerInterestType.Read);
    app.get('/api/Minerals/MineralValuation/MineralValuationManageLease/ReadLeaseStatus',ReadLeaseStatus.Read);
    app.get('/api/Minerals/MineralValuation/MineralValuationManageLease/ReadChangeReason',ReadChangeReason2.Read);
    app.get('/api/Minerals/MineralValuation/ReadWellTypeCombo',ReadWellTypeCombo.Read);
        
//---------------GIT--------------
   app.post('/api/git/pull',GIT.Pull); 

//---------------Excel------------
   app.post('/api/excel/upload',UtilFunctions.uploadExcel);
   app.all('/api/excel/download',UtilFunctions.downloadExcel);
   
   //used for report
   app.all('/api/jereport/pdf', JEReport.createPDF);
   
   //test for pdf file
   app.all('/api/pdf/sampleShortPaper',
    function(req, res){
      var phantom = require('phantom');   
      var fs = require('fs');   
		phantom.create().then(function(ph) {
			ph.createPage().then(function(page) {                                
				page.open("http://localhost/JEReport/index.html").then(function(status) {
                                        page.property('paperSize', {
                                            width: '8.5in',
                                            height: '11in',
                                        });
					page.render('JEReport/google.pdf').then(function() {                                           
                                                var data =fs.readFileSync('JEReport/google.pdf');
                                                res.contentType("application/pdf");
                                                res.send(data);                                           
//						console.log('Page Rendered');
						ph.exit();
					});
				});
			});
		});
});
} 