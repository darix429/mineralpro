module.exports = {
    GetUser: function (req, res) {
        //console.log("Called GetUser")
        var UtilFunctions = require('../../UtilFunctions.js');
        var cookie = require('cookie');  
        var cookies = cookie.parse(req.headers.cookie || ''); 
        
        var cookieExtend = {
            maxAge: 60 * 60 * 1 // 1 hr 
        };
        
         var cookieExpire = {
            maxAge: 1 // 1 sec 
        };
                               
        if(req.query.process){   
            var e_id = req.query.email_address;
            var raw_p = req.query.password;
            var appraisalYear = req.query.appraisalYear;
            
			if(e_id && raw_p){
				UtilFunctions.execSqlLogin(e_id,raw_p, appraisalYear, res);
			}else{
				var ErMsg = "{'success': true, "
					+" 'data': [{'0':'1', "
					+" 'id_user':'1','1':'', 'user_name':'', "
					+" '2':'','password':'', "
					+" '3':'','email_address':'', "
					+" 'comment':'Invalid Email Address/Password','4':'Invalid Email Address/Password'}]}";
				res.send(ErMsg);
			}
            
         
         } else if (req.query.logout) {
            var setCookies = [];
                setCookies.push(cookie.serialize('idAppUser', cookies.idAppUser, cookieExpire));
                setCookies.push(cookie.serialize('idOrganization', cookies.idOrganization, cookieExpire));
                setCookies.push(cookie.serialize('organizationName', cookies.organizationName, cookieExpire));
                setCookies.push(cookie.serialize('idSecurityGroup', cookies.idSecurityGroup, cookieExpire));
                setCookies.push(cookie.serialize('securityGroup', cookies.securityGroup, cookieExpire));
                setCookies.push(cookie.serialize('emailAddress', cookies.emailAddress, cookieExpire));
				setCookies.push(cookie.serialize('nameAbbreviation', cookies.nameAbbreviation, cookieExpire));			  
                setCookies.push(cookie.serialize('comment', cookies.comment, cookieExpire));
                setCookies.push(cookie.serialize('appraisalYear', cookies.appraisalYear, cookieExpire));
            
            res.setHeader('Set-Cookie',  setCookies);            
            res.send("{'success': true}");

        } else if (req.body.setCookie) {
            var postData = req.body;
            var setCookies = [];
                setCookies.push(cookie.serialize('idAppUser', postData.idAppUser, cookieExtend));
                setCookies.push(cookie.serialize('idOrganization', postData.idOrganization, cookieExtend));
                setCookies.push(cookie.serialize('organizationName', postData.organizationName, cookieExtend));
                setCookies.push(cookie.serialize('idSecurityGroup', postData.idSecurityGroup, cookieExtend));
                setCookies.push(cookie.serialize('securityGroup', postData.securityGroup, cookieExtend));
                setCookies.push(cookie.serialize('emailAddress', postData.emailAddress, cookieExtend));
				setCookies.push(cookie.serialize('nameAbbreviation', postData.nameAbbreviation, cookieExtend));						   
                setCookies.push(cookie.serialize('comment', postData.comment, cookieExtend));
                setCookies.push(cookie.serialize('appraisalYear', postData.appraisalYear, cookieExtend));
            
            res.setHeader('Set-Cookie',  setCookies);              
            res.send("{'success': true}");    


        //this portion reset the user session
        } else if (cookies.emailAddress) { 

            var output = "[{"
                    +" 'idAppUser': '"+cookies.idAppUser +"'"
                    +", 'idOrganization': '"+cookies.idOrganization +"'"
                    +", 'organizationName': '"+cookies.organizationName +"'"
                    +", 'idSecurityGroup': '"+cookies.idSecurityGroup +"'"
                    +", 'securityGroup': '"+cookies.securityGroup +"'"
                    +", 'emailAddress': '"+cookies.emailAddress+"'" 
					+", 'nameAbbreviation': '"+cookies.nameAbbreviation+"'"  
                    +", 'comment': '"+cookies.comment +"'"
                    +", 'appraisalYear': '"+cookies.appraisalYear +"'"
                    +"}]";

            var setCookies = [];
                setCookies.push(cookie.serialize('idAppUser', cookies.idAppUser, cookieExtend));
                setCookies.push(cookie.serialize('idOrganization', cookies.idOrganization, cookieExtend));
                setCookies.push(cookie.serialize('organizationName', cookies.organizationName, cookieExtend));
                setCookies.push(cookie.serialize('idSecurityGroup', cookies.idSecurityGroup, cookieExtend));
                setCookies.push(cookie.serialize('securityGroup', cookies.securityGroup, cookieExtend));
                setCookies.push(cookie.serialize('emailAddress', cookies.emailAddress, cookieExtend));
				setCookies.push(cookie.serialize('nameAbbreviation', cookies.nameAbbreviation, cookieExtend));				  
                setCookies.push(cookie.serialize('comment', cookies.comment, cookieExtend));
                setCookies.push(cookie.serialize('appraisalYear', cookies.appraisalYear, cookieExtend));
            
            res.setHeader('Set-Cookie',  setCookies);        

            res.send("{'success': true, 'data': " + output + "}")
           
        }else {
            res.send("{'success': true}");
        }
       
    },//end GetUser
};
