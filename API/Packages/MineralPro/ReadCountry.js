module.exports = {
    ReadCountry: function (req, res) {
        //console.log("Called ReadCountry")
        var UtilFunctions = require('../../UtilFunctions.js');
        var sql = "SELECT "
        			+"description as countryName,"
        			+"code as countryCd "
        			+"FROM SystemCode "
        			+"WHERE codeType = 810 "
        			+"AND appraisalYear = '"+req.query.appraisalYear+"' AND rowDeleteFlag = '';"
        UtilFunctions.execSql(sql, res)
    },//end ReadCountry
};

/*-get from SystemCode where codeType =810 and appraisalYear = req.query.appraisalYear
-return field description as countryName
-return field code as countryCd*/