module.exports = {
    ReadMineralSearch: function(req,res){
        //console.log("Called ReadAccount")
        var UtilFunctions = require('../../../UtilFunctions.js');
        var sql = ''
		
        if(req.query.searchOption && req.query.searchValue && req.query.appraisalYear){
            var searchValue = req.query.searchValue
            var searchOption = req.query.searchOption
            var appraisalYear = req.query.appraisalYear
            var columnField = ''
			
			var appraisal = ["MINERAL APPRAISAL RRC#","MINERAL FIELD NAME"];
			var leaseinfo = ["MINERAL LEASE #","MINERAL LEASE NAME","MINERAL LEGAL DESCRIPTION","OPERATOR NAME (Lease)"];
			var optrappraisal = ["OPERATOR NAME (Appraisal)"];
			var poolunit = ["MINERAL UNIT #","MINERAL UNIT NAME","OPERATOR NAME (Unit)"];
			var leaseholdings = ["OPERATOR NAME (Mineral Rights)","OWNER # (Mineral Rights)","OWNER NAME (Mineral Rights)","OWNER ADDRESS (Mineral Rights)","AGENT NAME (Mineral Rights)"];
			
			//console.log("search",searchOption,searchValue);
			
			//rrc#, lease name, lease#, field, appraisal value
			if(appraisal.indexOf(searchOption) > -1 ){
				//console.log("APPRAISAL");
                sql = "SELECT DISTINCT a.rrcNumber 'RRC #', "
                                +" ISNULL(l.leaseName, '') 'Lease/Unit Name',"
                                +" ISNULL(l.leaseId, '') 'Lease #',"
                                +" ISNULL(f.fieldName, '') 'Field',"
                                +" a.totalValue 'Appraisal Value'"  
                     +" FROM Appraisal a"
                     +" INNER JOIN Field f ON a.fieldId = f.fieldId AND f.rowDeleteFlag = '' AND a.appraisalYear=f.appraisalYear "
                     +" INNER JOIN Lease l ON a.subjectId = l.leaseId AND l.rowDeleteFlag = '' AND a.subjectTypeCd = 2 AND a.appraisalYear=l.appraisalYear"
                
                if(searchOption == 'MINERAL APPRAISAL RRC#'){
                    sql += " WHERE a.rrcNumber like '%"+searchValue+"%'"
                }else if (searchOption == 'MINERAL FIELD NAME'){
                    sql += " WHERE f.fieldName like '%"+searchValue+"%'"
                }
                sql += " AND a.appraisalYear = '"+appraisalYear+"'";
                columnField = "RRC #, Lease/Unit Name, Lease #, Field, Appraisal Value";
			}
			//lease#, lease name, legal desc, operator, lease value , owners
			else if(leaseinfo.indexOf(searchOption) > -1){
				//console.log("LEASE INFO");
				sql = ""
				+"SELECT DISTINCT "
					+"L.leaseId 'Lease #', "
					+"L.leaseName 'Lease Name', "
					+"L.description 'Legal Desc', "
					+"O.operatorName 'Operator', "
					+"A.totalValue 'Lease Value', "
					+"(select count(*) from LeaseOwner where appraisalYear=2017 and leaseId = L.leaseId) 'Owners' "
				+"FROM "
					+"Appraisal as A "
					+"INNER JOIN Lease as L "
					+"ON A.subjectId = L.leaseId AND A.subjectTypeCd = 2 AND A.appraisalYear=L.appraisalYear "
					+"LEFT JOIN Operator as O "
					+"ON L.operatorId = O.operatorId AND L.appraisalYear = O.appraisalYear "
				+"WHERE A.appraisalYear = 2017 "
				+"AND NOT(A.rowDeleteFlag='D' OR L.rowDeleteFlag='D' OR O.rowDeleteFlag='D') ";
				
				sql+= (searchOption == 'MINERAL LEASE #')? "AND L.leaseId LIKE '%"+searchValue+"%' ":"";
				sql+= (searchOption == 'MINERAL LEASE NAME')? "AND L.leaseName LIKE '%"+searchValue+"%' ":"";
				sql+= (searchOption == 'MINERAL LEGAL DESCRIPTION')? "AND L.description LIKE '%"+searchValue+"%' ":"";
				sql+= (searchOption == 'OPERATOR NAME (Lease)')? "AND O.operatorName LIKE '%"+searchValue+"%' ":"";
				
                columnField = "Lease #, Lease Name, Legal Desc, Operator, Lease Value, Owners";
			}
			//lease#, lease name, legal desc, pipeline, lease value , owners
			else if(optrappraisal.indexOf(searchOption) > -1){
				//console.log("OPERATOR NAME (Appraisal)");
				sql = ""
				+"SELECT DISTINCT "
					+"L.leaseId 'Lease #', "
					+"L.leaseName 'Lease Name', "
					+"L.description 'Legal Desc', "
					+"'' as 'Pipeline', "
					+"A.totalValue 'Lease Value', "
					+"(select count(*) from LeaseOwner where appraisalYear=2017 and leaseId = L.leaseId) 'Owners' "
				+"FROM "
					+"Appraisal as A "
					+"INNER JOIN Lease as L "
					+"ON A.subjectId = L.leaseId AND A.subjectTypeCd = 2 AND A.appraisalYear=L.appraisalYear "
					+"LEFT JOIN Operator as O "
					+"ON L.operatorId = O.operatorId AND L.appraisalYear = O.appraisalYear "
				+"WHERE A.appraisalYear = '"+appraisalYear+"' "
				+"AND NOT(A.rowDeleteFlag='D' OR L.rowDeleteFlag='D' OR O.rowDeleteFlag='D') ";
				
				// sql+= (searchOption == 'OPERATOR NAME (Appraisal)')? "AND O.operatorName LIKE '%"+searchValue+"%' ":"";
				
				columnField = "Lease #, Lease Name, Legal Desc, Pipeline, Lease Value, Owners";
			}
			//econ unit#, unit name, operator, unit value, leases
			else if(poolunit.indexOf(searchOption) > -1){
				//console.log("POOL UNIT");
				sql = ""
                +"SELECT DISTINCT "
					+"PU.unitId 'Econ Unit #', "
					+"PU.unitname 'Unit Name', "
					+"O.operatorName 'Operator', "
					+"A.totalValue 'Unit Value', "
					+"(select count(*) from lease where unitId = PU.unitId) as 'Leases' "
				+"FROM "
					+"Appraisal as A "
					+"INNER JOIN Unit as PU "
					+"ON A.subjectTypeCd = 1 AND A.subjectId = PU.unitId AND A.appraisalYear=PU.appraisalYear "
					+"LEFT JOIN Operator as O "
					+"ON PU.operatorId = O.operatorId AND PU.appraisalYear = O.appraisalYear "
				+"WHERE A.appraisalYear = '"+appraisalYear+"' "
				+"AND NOT(A.rowDeleteFlag='D' OR PU.rowDeleteFlag='D' OR O.rowDeleteFlag='D') ";
				
				sql+= (searchOption == 'MINERAL UNIT #')? "AND PU.unitId LIKE '%"+searchValue+"%' ":"";
				sql+= (searchOption == 'MINERAL UNIT NAME')? "AND PU.unitname LIKE '%"+searchValue+"%' ":"";
				sql+= (searchOption == 'OPERATOR NAME (Unit)')? "AND O.operatorName LIKE '%"+searchValue+"%' ":"";
				
				columnField = "Econ Unit #, Unit Name, Operator, Unit Value, Leases";
			}
			//owner#, owner, in care of, street address, city, holdings value, holdings
			else if(leaseholdings.indexOf(searchOption) > -1 ){
				//console.log("LEASE HOLDINGS");
				sql = ""
				+"SELECT DISTINCT "
					+"A.subjectId, "
					+"O.ownerId 'Owner#', "
					+"name1 'Owner', "
					+"name2 'In Care Of', "
					+"O.addrLine3 'Street Address', "
					+"O.city 'City', "
					+"A.totalValue 'Holdings Value', "
					+"(select count(*) from LeaseOwner where ownerId=O.ownerId) 'Holdings' "
				+"FROM "
					+"Appraisal as A "
					+"INNER JOIN Owner as O "
					+"ON A.subjectTypeCd = 3 and A.subjectId = O.ownerId AND A.appraisalYear = O.appraisalYear "
					+"LEFT JOIN Operator as Optr "
					+"ON Optr.ownerId = O.ownerId AND O.appraisalYear = Optr.appraisalYear "
					+"LEFT JOIN Agency as Agc "
					+"ON O.agencyCdx = Agc.agencyCdx AND Optr.appraisalYear = Agc.appraisalYear "
				+"WHERE "
				+"A.appraisalYear = '"+appraisalYear+"' "
				+"AND NOT(A.rowDeleteFlag='D' OR O.rowDeleteFlag='D' OR Optr.rowDeleteFlag='D' OR Agc.rowDeleteFlag='D') ";
				
				sql+= (searchOption == 'OPERATOR NAME (Mineral Rights)')? "AND Optr.operatorName LIKE '%"+searchValue+"%' ":"";
				sql+= (searchOption == 'OWNER # (Mineral Rights)')? "AND O.ownerId LIKE '%"+searchValue+"%' ":"";
				sql+= (searchOption == 'OWNER NAME (Mineral Rights)')? "AND O.name1 LIKE '%"+searchValue+"%' ":"";
				sql+= (searchOption == 'OWNER ADDRESS (Mineral Rights)')? "AND CONCAT(O.addrLine1,O.addrLine2,O.addrLine3) LIKE '%"+searchValue+"%' ":"";
				sql+= (searchOption == 'AGENT NAME (Mineral Rights)')? "AND Agc.agencyName LIKE '%"+searchValue+"%' ":"";
				
				columnField = "Owner #, Owner, In Care Of, Street Address, City, Holdings Value, Holdings";
			}
			UtilFunctions.execSqlColDef(sql, columnField, res)
        }else{
			res.send("{'success':true,'data':'','error':'invalid query params'}");
		}
        
    }
}
//subjectTypeCd
//1 -- POOLUNIT
//2 -- LEASE
//3 – OWNER
//
//subjectId
//unitId
//leaseId
//ownerId


//  'MINERAL APPRAISAL RRC#',
// 'MINERAL FIELD NAME',
// 'MINERAL LEASE #',
// 'MINERAL LEASE NAME',
// 'MINERAL LEGAL DESCRIPTION',
// 'MINERAL UNIT #',
// 'MINERAL UNIT NAME',
// 'OPERATOR NAME (Lease)',
// 'OPERATOR NAME (Unit)',
// 'OPERATOR NAME (Appraisal)',
// 'OPERATOR NAME (Mineral Rights)',
// 'OWNER # (Mineral Rights)',
// 'OWNER NAME (Mineral Rights)',
// 'OWNER ADDRESS (Mineral Rights)',
// 'AGENT NAME (Mineral Rights)',
