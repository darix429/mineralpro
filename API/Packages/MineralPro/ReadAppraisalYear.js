module.exports = {
    ReadAppraisalYear: function (req, res) {
        //console.log("Called ReadAppraisalYear")
        var UtilFunctions = require('../../UtilFunctions.js');
        var sql = "SELECT DISTINCT "
                    +"ay.appraisalYear,"
                    +"ay.currentInd activeAppraisalYear, "
                    +"je.appUser, "
                    +"je.batchId, "
                    +"je.idJournalEntry "
                    +"FROM AppraisalYear ay "
                    +"LEFT JOIN JournalEntry je ON je.appraisalYear = ay.appraisalYear AND je.closeDt = '1900-01-01 00:00:00.000' AND je.appUser = '"+req.query.idAppUser+"' "
                    +"ORDER BY ay.appraisalYear Desc"
        UtilFunctions.execSql(sql, res)
    },//end ReadCountry
};

/*-get from SystemCode where codeType =810 and appraisalYear = req.query.appraisalYear
-return field description as countryName
-return field code as countryCd*/