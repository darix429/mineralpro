module.exports = {
    ReadReasonCodeType: function (req, res) {
        var UtilFunctions = require('../../UtilFunctions.js');  
      // req.query.appraisalYear
        var sql = "SELECT code as valueField ,"
                  +" description as displayField, "
                  +" shortDescription as shortdesc "
                  +" FROM SystemCode "
                  +" WHERE appraisalYear = 2017 AND codeType = 2170 AND rowDeleteFlag = '' ";
        UtilFunctions.execSql(sql, res)
    },//end ReadReasonCodeType
};