module.exports = {
    SendMailToDevs: function (req, res) {
        var SystemConfig = require('../../SystemConfig.js'); 
        var SupportEmailList = SystemConfig.supportEmailList.join(',');
        var nodemailer = require('nodemailer');
        var messageBody = req.body.message || '';
        var messageError = req.body.error || '';
        var idAppUser = req.body.idAppUser || 0;

        var name = '';
        var email = '';
        var dbError = ''; 
        var sendMessage = function(){
            let transporter = nodemailer.createTransport({
                service: 'gmail',
                secure: false,
                port: 25,
                auth: {
                    user: 'mineralpro2@gmail.com',
                    pass: 'MPro@dmin'
                },
                tls: {
                    rejectUnauthorized: false
                }
            });

            messageBody = `
                <p>idAppUser: ${idAppUser}</p>
                <p>User: ${name}</p>
                <p>Email: ${email}</p>
                <hr>
                <b>User Message</b>
                <p>${messageBody}</p>
                <hr>
                <b>Error Details Encountered by the User</b>
                <p>${messageError}</p>
            `;
            if(dbError){
                messageBody += `
                    <b>Additional Error</b>
                    <p>Encountered an error while connecting the database</p><p>Details: ${dbError}</p>
                `;
            }

            let HelperOptions = {
                from: `"MineralPro" <mineralpro2@gmail.com`,
                to: SupportEmailList,
                subject: 'MineralPro Help',
                html: messageBody
            };
            transporter.sendMail(HelperOptions, (error, info) => {
                if (error) {
                    res.status(500).json({'success':false,'msg':error});
                }
                res.status(200).json({'success':true,'msg':"The Developers have been notified. We're sorry for the inconvinience."})
            });
        };

        var sql = `
            select emailAddress email, nameAbbreviation name from AppUser where idAppUser=${idAppUser}
        `;
        /*------------------------------ START ------------------------------
        | query app user from db
        */
            
        var mssql = require('mssql');   
        var conn = new mssql.Connection(SystemConfig.dbConfig);
         
        conn.connect().then(function () {
            var req = new mssql.Request(conn);
            req.query(sql_query).then(function (recordset) {;
                conn.close();                 
                name = recordset[0] ? recordset[0].name : '';
                email = recordset[0] ? recordset[0].email : '';
                sendMessage();
            })
            .catch(function (err) {
                conn.close();
                dbError = JSON.stringify(err);
                sendMessage();
            });        
        })
        .catch(function (err) {
            dbError = JSON.stringify(err);
            sendMessage();
        });
        /*
        | query app user from db
        | ------------------------------ END ------------------------------
        */
    }
};
