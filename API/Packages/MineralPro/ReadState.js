module.exports = {
    ReadState: function (req, res) {
        //console.log("Called ReadState")
        var UtilFunctions = require('../../UtilFunctions.js');
        var sql = "SELECT "
              +"description as stateName,"
              +"code as stateCd "
              +"FROM SystemCode "
              +"WHERE codeType = 790 "
              +"AND appraisalYear = '"+req.query.appraisalYear+"' AND rowDeleteFlag = '';"
        UtilFunctions.execSql(sql, res)
    },//end ReadState
};

/*-get from SystemCode where codeType = 790 and appraisalYear = req.query.appraisalYear
-return field description as stateName
-return field code as stateCd*/