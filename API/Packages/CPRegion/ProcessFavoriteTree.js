module.exports = {
    ProcessFavoriteTree: function (req, res) {
        var UtilFunctions = require('../../UtilFunctions.js');
        var sql = '';  
        if (req.query.process) {
            var process = req.query.process;
            var idAppUser = req.query.idAppUser;
            var idSubSystemFunction =  req.query.idSubSystemFunction;
                                 
            if(process == "addFavorite"){
                sql = "INSERT INTO AppUserSubSystemFavorite "
                    +" (idAppUser, idSubSystemFunction, rowUpdateUserid, rowUpdateDt) "
                    +" VALUES "
                    +" ('"+idAppUser+"', '"+idSubSystemFunction+"', '"+idAppUser+"', GETDATE());";
            
            }else if(process == "removeFavorite"){               
                sql = "DELETE FROM AppUserSubSystemFavorite "
                    +" WHERE idAppUser = '"+idAppUser+"' "
                    +" AND idSubSystemFunction = '"+idSubSystemFunction+"';";
            }            
        }
        UtilFunctions.execSql(sql, res); 
//        callback(res, sql);
    },//end ProcessFavoriteTree
    
};
