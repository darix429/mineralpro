module.exports = function(app){

//----------CRUD Listeners for CPRegion Use (use Get for read and Post for others)---------------
    app.get('/api/Packages/CPRegion/GetHeaderData', function(req, res){ 
        var GetHeaderData = require('./GetHeaderData.js');   
        GetHeaderData.GetHeaderData(req, res);        
    });

    app.get('/api/Packages/CPRegion/GetUserFunction', function(req, res){ 
        var GetUserFunction = require('./GetUserFunction.js');   
        GetUserFunction.GetUserFunction(req, res);        
    });
    
    app.get('/api/Packages/CPRegion/ProcessFavoriteTree', function(req, res){ 
        var ProcessFavoriteTree = require('./ProcessFavoriteTree.js');   
        ProcessFavoriteTree.ProcessFavoriteTree(req, res);        
    });
    
    app.get('/api/Packages/CPRegion/GetFavoriteTree', function(req, res){ 
        var GetFavoriteTree = require('./GetFavoriteTree.js');   
        GetFavoriteTree.GetFavoriteTree(req, res);        
    });
    
    app.get('/api/Packages/CPRegion/GetMenuTree', function(req, res){ 
        var GetMenuTree = require('./GetMenuTree.js');   
        GetMenuTree.GetMenuTree(req, res);        
    });
    //other routes..
}