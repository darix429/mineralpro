module.exports = {
    GetUserFunction: function (req, res) {
        var UtilFunctions = require('../../UtilFunctions.js');
        var sql = '';
        if (req.query.process) {
            var process = req.query.process;
            var idAppUser = req.query.idAppUser;
            var idOrganization = req.query.idOrganization;
            var appName = req.query.appName;

            var appraisalYear = req.query.appraisalYear;
            var currentYear = new Date().getFullYear();
            var securityCd = "sf.securityCd, ";

            var getCurYear = "SELECT appraisalYear FROM AppraisalYear WHERE currentInd = 'Y'";

            UtilFunctions.execSqlCallBack(getCurYear, res, function (record) {
                currentYear = record[0].appraisalYear

                //use readOnly Access if appraisalYear != to currentYear
                if (appraisalYear != currentYear) {
                    securityCd = "CASE WHEN sf.securityCd = 2 THEN '1' ELSE sf.securityCd END securityCd, ";
                }

                if (process == "select") {
                    var idSecurityGroup = req.query.idSecurityGroup;
                    sql = "SELECT a.appName, "
                            + " uf.idAppUser, "
                            + " ssf.idSubSystemFunction, "
                            + " uo.idOrganization, "
                            + " ss.subSystemName, "
                            + " ss.displayName subSystemNameDisplay, "
                            + " ssf.functionName, "
                            + " ssf.displayName functionNameDisplay, "
                            + securityCd
                            //+" sf.securityCd, "
                            + " uf.activeTabInd, "
                            + " uf.idUserFunction "
                            + " FROM UserFunction uf "
                            + " INNER JOIN SubSystemFunction ssf ON uf.idSubSystemFunction = ssf.idSubSystemFunction AND ssf.rowDeleteFlag = '' "
                            + " INNER JOIN SubSystem ss ON ssf.idSubSystem = ss.idSubSystem AND ss.rowDeleteFlag = '' "
                            + " INNER JOIN FunctionSecurity sf ON ssf.idSubSystemFunction = sf.idSubSystemFunction AND sf.rowDeleteFlag = '' "
                            + " INNER JOIN AppSubSystem ass on ss.idSubSystem = ass.idSubSystem AND ass.rowDeleteFlag = '' "
                            + " INNER JOIN App a ON ass.idApp = a.idApp AND a.rowDeleteFlag = '' "
                            + " INNER JOIN UserOrganization uo ON uf.idAppUser =uo.idAppUser AND uo.rowDeleteFlag = '' "
                            + " WHERE lower(a.appName) = lower('" + appName + "') "
                            + "     AND uf.idAppUser = '" + idAppUser + "' "
                            + "     AND sf.idSecurityGroup = '" + idSecurityGroup + "' "
                            + "     AND uo.idOrganization = '" + idOrganization + "' "
                            + "     AND uf.rowDeleteFlag = '' "
                            + " ORDER BY uf.idUserFunction;";

                } else {
                    var idSubSystemFunction = req.query.idSubSystemFunction;
                    if (process == "insert") {
                        sql = "UPDATE UserFunction "
                                + " SET activeTabInd = 'N' "
                                + " FROM UserFunction uf "
                                + " INNER JOIN SubSystemFunction ssf ON uf.idSubSystemFunction = ssf.idSubSystemFunction AND ssf.rowDeleteFlag = '' "
                                + " INNER JOIN SubSystem ss ON ssf.idSubSystem = ss.idSubSystem AND ss.rowDeleteFlag = '' "
                                + " INNER JOIN AppSubSystem ass on ss.idSubSystem = ass.idSubSystem AND ass.rowDeleteFlag = '' "
                                + " INNER JOIN App a ON ass.idApp = a.idApp AND a.rowDeleteFlag = '' "
                                + " INNER JOIN UserOrganization uo ON uf.idAppUser =uo.idAppUser AND uo.rowDeleteFlag = '' "
                                + " WHERE lower(a.appName) = lower('" + appName + "') "
                                + "     AND uf.idAppUser = '" + idAppUser + "' "
                                + "     AND uo.idOrganization = '" + idOrganization + "' "
                                + "     AND uf.rowDeleteFlag = ''; "

                        sql += "INSERT INTO UserFunction "
                                + " (idAppUser, idSubSystemFunction, activeTabInd, rowUpdateUserid) "
                                + " VALUES ('" + idAppUser + "', '" + idSubSystemFunction + "', 'Y', '" + idAppUser + "');";


                    } else if (process == "delete") {
                        sql = "DELETE UserFunction "
                                + " FROM UserFunction uf "
                                + " INNER JOIN SubSystemFunction ssf ON uf.idSubSystemFunction = ssf.idSubSystemFunction AND ssf.rowDeleteFlag = '' "
                                + " INNER JOIN SubSystem ss ON ssf.idSubSystem = ss.idSubSystem AND ss.rowDeleteFlag = '' "
                                + " INNER JOIN AppSubSystem ass on ss.idSubSystem = ass.idSubSystem AND ass.rowDeleteFlag = '' "
                                + " INNER JOIN App a ON ass.idApp = a.idApp AND a.rowDeleteFlag = '' "
                                + " INNER JOIN UserOrganization uo ON uf.idAppUser =uo.idAppUser AND uo.rowDeleteFlag = '' "
                                + " WHERE lower(a.appName) = lower('" + appName + "') "
                                + "     AND uf.idSubSystemFunction = '" + idSubSystemFunction + "' "
                                + "     AND uf.idAppUser = '" + idAppUser + "' "
                                + "     AND uo.idOrganization = '" + idOrganization + "' "
                                + "     AND uf.rowDeleteFlag = '';";

                    } else if (process == "update") {
                        sql = "UPDATE UserFunction "
                                + " SET activeTabInd = 'N' "
                                + " FROM UserFunction uf "
                                + " INNER JOIN SubSystemFunction ssf ON uf.idSubSystemFunction = ssf.idSubSystemFunction AND ssf.rowDeleteFlag = '' "
                                + " INNER JOIN SubSystem ss ON ssf.idSubSystem = ss.idSubSystem AND ss.rowDeleteFlag = '' "
                                + " INNER JOIN AppSubSystem ass on ss.idSubSystem = ass.idSubSystem AND ass.rowDeleteFlag = '' "
                                + " INNER JOIN App a ON ass.idApp = a.idApp AND a.rowDeleteFlag = '' "
                                + " INNER JOIN UserOrganization uo ON uf.idAppUser =uo.idAppUser AND uo.rowDeleteFlag = '' "
                                + " WHERE lower(a.appName) = lower('" + appName + "') "
                                + "     AND uf.idAppUser = '" + idAppUser + "' "
                                + "     AND uo.idOrganization = '" + idOrganization + "' "
                                + "     AND uf.rowDeleteFlag = '';";

                        sql += "UPDATE UserFunction "
                                + " SET activeTabInd = 'Y' "
                                + " FROM UserFunction uf "
                                + " INNER JOIN SubSystemFunction ssf ON uf.idSubSystemFunction = ssf.idSubSystemFunction AND ssf.rowDeleteFlag = '' "
                                + " INNER JOIN SubSystem ss ON ssf.idSubSystem = ss.idSubSystem AND ss.rowDeleteFlag = '' "
                                + " INNER JOIN AppSubSystem ass on ss.idSubSystem = ass.idSubSystem AND ass.rowDeleteFlag = '' "
                                + " INNER JOIN App a ON ass.idApp = a.idApp AND a.rowDeleteFlag = '' "
                                + " INNER JOIN UserOrganization uo ON uf.idAppUser =uo.idAppUser AND uo.rowDeleteFlag = '' "
                                + " WHERE lower(a.appName) = lower('" + appName + "') "
                                + "     AND uf.idSubSystemFunction = '" + idSubSystemFunction + "' "
                                + "     AND uf.idAppUser = '" + idAppUser + "' "
                                + "     AND uo.idOrganization = '" + idOrganization + "' "
                                + "     AND uf.rowDeleteFlag = '';";
                    }
                }
                UtilFunctions.execSql(sql, res);
            })
        }

    }, //end GetUserFunction

};
