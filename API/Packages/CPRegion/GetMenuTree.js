module.exports = {
    GetMenuTree: function (req, res) {
        var UtilFunctions = require('../../UtilFunctions.js');
        if(req.query.process){        
            var idSecurityGroup = req.query.idSecurityGroup;
            var idAppUser = req.query.idAppUser;
            var appName = req.query.appName;
            var appraisalYear = req.query.appraisalYear;
            var currentYear=new Date().getFullYear();
            var securityCd = "sf.securityCd, ";
            var optionJE = req.query.optionJE;
            
            var getCurYear = "SELECT appraisalYear FROM AppraisalYear WHERE currentInd = 'Y'";
            
            UtilFunctions.execSqlCallBack(getCurYear, res, function(record) {
                 currentYear = record[0].appraisalYear
                //use readOnly Access if appraisalYear != to currentYear           
                if(appraisalYear != currentYear && optionJE != 'startJE'){
                    securityCd = "CASE WHEN sf.securityCd = 2 THEN '1' ELSE sf.securityCd END securityCd, ";
                }
                var sql = "SELECT "
                        +securityCd
                        //+"     sf.securityCd, "
                        +"     ssf.idSubSystemFunction, "
                        +"     a.appName, "
                        +"     ss.subSystemName, "
                        +"     ss.displayName subSystemNameDisplay, "
                        +"     ssf.functionName, "
                        +"     ssf.displayName functionNameDisplay "
                        +" FROM App a "
                        +" INNER JOIN AppSubSystem ass ON a.idApp = ass.idApp AND ass.rowDeleteFlag = '' "
                        +" INNER JOIN SubSystem ss ON ass.idSubSystem = ss.idSubSystem AND ss.rowDeleteFlag = '' "
                        +" INNER JOIN SubSystemFunction ssf ON ss.idSubSystem = ssf.idSubSystem AND ssf.rowDeleteFlag = '' "
                        +" INNER JOIN FunctionSecurity sf ON ssf.idSubSystemFunction = sf.idSubSystemFunction AND sf.rowDeleteFlag = '' "
                        +" INNER JOIN SecurityGroup sg ON sf.idSecurityGroup = sg.idSecurityGroup AND sg.rowDeleteFlag = '' "
                        +" WHERE lower(a.appName) = lower('"+appName+"') "
                        +"     AND sg.idSecurityGroup = '"+idSecurityGroup+"' "
                        +"     AND ssf.idSubSystemFunction NOT IN ( "
                        +"             SELECT idSubSystemFunction  "
                        +"             FROM AppUserSubSystemFavorite  "
                        +"             WHERE idAppUser = '"+idAppUser+"' "
                        +"                     AND rowDeleteFlag = ''"
                        +"             ) "
                        +"     AND a.rowDeleteFlag = ''"  
                        +"     ORDER BY ss.displayName, ssf.displayName;";
                UtilFunctions.getTreeData(sql, res);
            })                    
        }               
    },//end GetFavoriteTree
};
