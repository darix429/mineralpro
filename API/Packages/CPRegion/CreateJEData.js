module.exports = {
    CreateJEData: function (req, res) {
        //console.log("Called CreateJEData")
        var UtilFunctions = require('../../UtilFunctions.js');
        if (req.body) {
            var arrData = req.body;
            //if(req.query.createLength == 1){
            //arrData = [req.body];
            //}
            if (req.query.jeProcess == 'start') {
                var sql = "INSERT INTO JournalEntry "
                        + " (appUser,appraisalYear,batchId,openDt,closeDt,jeReasonCd,description,taxOfficeNotifyCd,jeHoldCd,collectionExtractDt,rowUpdateUserid,rowUpdateDt,rowDeleteFlag) "
                        + " VALUES ( "
                        + " '" + arrData.rowUpdateUserid + "', "
                        + " '" + arrData.selectedAppraisalYear + "', "
                        //+" (SELECT top 1 ISNULL(batchId, 0 )+1 FROM JournalEntry order by idJournalEntry DESC), "
                        + " 99999999, "
                        + " SYSDATETIME(), "
                        + " '" + arrData.closeDt + "', "
                        + " '" + arrData.jeReasonCd + "', "
                        + " '" + arrData.description + "', "
                        + " '" + arrData.taxOfficeNotifyCd + "', "
                        + " '" + arrData.jeHoldCd + "', "
                        + " '" + arrData.collectionExtractDt + "', "
                        + " '" + arrData.rowUpdateUserid + "', "
                        + " SYSDATETIME(), "
                        + " '' "
                        + " ) ";

            } else {
                var sql = "UPDATE JournalEntry SET "
                        + "closeDt = SYSDATETIME() "
                        + "WHERE "
                        + "idJournalEntry = '" + arrData.idJournalEntry + "'; ";

//                sql += " UPDATE JournalEntryBatch SET processDt = SYSDATETIME() WHERE batchId ='" + arrData.batchId + "'; "
                sql += " UPDATE JournalEntryBatch SET "
                        +"jeCount = (SELECT count(idJournalEntry) FROM JournalEntry WHERE batchId = '99999999'), "
                        +"leaseOwnerCount = (SELECT count(jelo.idJeLeaseOwner) FROM jeLeaseOwner jelo "
                                            +"INNER JOIN JournalEntry je ON je.idJournalEntry = jelo.idJournalEntry "
                                            +"WHERE jelo.seqNumber = 1 and je.batchId = '99999999'), "
                        +"beforeOwnerValue = (SELECT ISNULL(sum(jelo.ownerValue),0) FROM jeLeaseOwner jelo "
                                            +"INNER JOIN JournalEntry je ON je.idJournalEntry = jelo.idJournalEntry "
                                            +"WHERE jelo.seqNumber = 0 and je.batchId = '99999999'), "
                        +"afterOwnerValue = (SELECT ISNULL(sum(jelo.ownerValue),0) FROM jeLeaseOwner jelo "
                                            +"INNER JOIN JournalEntry je ON je.idJournalEntry = jelo.idJournalEntry "
                                            +"WHERE jelo.seqNumber = 1 and je.batchId = '99999999') "
                        +"WHERE batchId ='99999999'; "
                       + " IF @@ROWCOUNT=0 BEGIN "
                        + " INSERT INTO JournalEntryBatch "
                        + " (batchId, createDt, processDt, jeCount,leaseOwnerCount, beforeOwnerValue, afterOwnerValue ) VALUES "
                        + " ( '" + arrData.batchId + "', "
                        + " SYSDATETIME(), "
                        + " '" + arrData.closeDt + "', 1, 1, "
                        + " (SELECT ISNULL(sum(jelo.ownerValue),0) FROM jeLeaseOwner jelo "
                                            +"INNER JOIN JournalEntry je ON je.idJournalEntry = jelo.idJournalEntry "
                                            +"WHERE jelo.seqNumber = 0 and je.batchId = '99999999'), "
                        + " (SELECT ISNULL(sum(jelo.ownerValue),0) FROM jeLeaseOwner jelo "
                                            +"INNER JOIN JournalEntry je ON je.idJournalEntry = jelo.idJournalEntry "
                                            +"WHERE jelo.seqNumber = 1 and je.batchId = '99999999') "                  
                        +" ); "
                        + " END ";
//                sql +=  " INSERT INTO JournalEntryBatch "
//                        + " (batchId, createDt, processDt, rowUpdateUserId) VALUES "
//                        + " ( '" + arrData.batchId + "', "
//                        + " SYSDATETIME(), "
//                        + " SYSDATETIME(), '" + arrData.batchId + "' ); "

            }
            UtilFunctions.execSql(sql, res);
        } else {
            res.status(400).json({"success": false, "data": "No data received"});
        }
    }
}
