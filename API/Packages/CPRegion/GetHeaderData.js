module.exports = {
    GetHeaderData: function (req, res) {
        var UtilFunctions = require('../../UtilFunctions.js');
        if(req.query.accountNumber){        
            var accountNumber = req.query.accountNumber;
            var appraisalYear = req.query.appraisalYear;
            
            var sql ="SELECT  a.accountNum accountNumber, "
                        +" a.appraisalYear, "
                        +" alta.altAccountNum geoAccountNumber, "
                        +" ao.name1 + ' ' + ao.name2 ownerName, "
                        +" acca.streetAddress situsAddress, "
                        +" avs.totalValue totalMarketValue, "
                        +" ss.shortDescription lastValueMethod, "
                        +" avs.lastAppraisalYear lastValuationYear, "
                        +" 'Exemption List' exemptionList "
                    +" FROM Account a "
                    +" LEFT JOIN AltAccount alta ON a.idAccountYear = alta.idAccountYear AND alta.typeCd = 1 "
                    +"         AND alta.rowDeleteFlag = '' "
                    +" LEFT JOIN AccountOwner ao ON a.idAccount = ao.idAccount AND ao.rowDeleteFlag = '' "
                    +"         AND ao.seqNumber = (SELECT max(seqNumber) FROM AccountOwner WHERE idAccount = a.idAccount) "
                    +" LEFT JOIN AccountAddress acca ON a.idAccountYear = acca.idAccountYear AND acca.addressTypeCd = 1 "
                    +"         AND acca.rowDeleteFlag = '' "
                    +" LEFT JOIN AccountValueSummary avs ON a.idAccountYear = avs.idAccountYear AND avs.rowDeleteFlag = '' "
                    +" LEFT JOIN SystemCode ss ON avs.appraisalDt = ss.code AND ss.codeType = 150 "
                    +"         AND ss.rowDeleteFlag = '' "
                    +" WHERE a.accountNum = '"+accountNumber+"' "
                    +"     AND a.appraisalYear = '"+appraisalYear+"';";
        
        }
        UtilFunctions.execSql(sql, res);
    },//end GetHeaderData
    
};
