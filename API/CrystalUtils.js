var config = require('./SystemConfig.js');
var http = require('http');
var parser = require('xml2js');
var request = require('request');
var jp = require('jsonpath');

module.exports = {
	getToken: function(callback){
		var token = "";
		var xml = '<attrs xmlns="http://www.sap.com/rws/bip">'
			+'<attr name="auth" type="string" possibilities="secEnterprise,secLDAP,secWinAD,secSAPR3">secEnterprise</attr>'
			+'<attr name="userName" type="string">'+config.CrystalServer.user+'</attr>'
			+'<attr name="password" type="string">'+config.CrystalServer.password+'</attr>'
			+'</attrs>'
		var options = {
			hostname: config.CrystalServer.url,
			port: config.CrystalServer.CMCport,
			path: '/biprws/logon/long',
			method: 'POST',
			headers: {
				'accept':'application/xml',
				'content-type':'application/xml'
			},
		}
		
		var req = http.request(options,function(result){
			result.setEncoding('utf8');
			result.on('data',function(body){
				parser.parseString(body,{explicitArray:false, ignoreAttrs:true},function(e,r){
					token = r.entry.content.attrs.attr
					callback(null,token);
				});
			});
		});
		req.on('error',function(er){
			console.log("error encountered...");
			callback(er,null);
		});
		req.write(xml);
		req.end();
	},
	
	getReports: function(callback){
		module.exports.getToken(function(e,token){
			var targetUrl = 'http://'+config.CrystalServer.url+':'+config.CrystalServer.CMCport+'/biprws/infostore/'+config.CrystalServer.ReportsFolderId+'/children';
			var options = {
				url: targetUrl,
				headers: {
					'X-SAP-LogonToken': token,
					'content-type':'application/xml'
				}
			}
			request(options,function(e,r,body){
				parser.parseString(body,{explicitArray:false, ignoreAttrs:true},function(e,r){
					if(e){ callback(e,null); }
					var reports = (r.feed.entry);
					var result = {"reports":[]};
					for(var i=0; i<reports.length; i++){
						var data = reports[i].content.attrs;
						var description = data.attr[2];
						var report = {
							"reportId": data.attr[3],
							"fileName": data.attr[1],
							"reportDesc": description,
							"appraisalYear": '',
							"reportCategory": 'report for CAD',
							"reportType": 'CRX',
							"displayOrder": ''
						};
						
						var copy = jp.query(result, '$..reports[?(@.reportDesc=="'+description+'")]');
						if(copy.length>0){
							copy[0].reportId = (report.reportId > copy[0].reportId)? report.reportId : copy[0].reportId;
							copy[0].fileName = report.fileName;
						}else
							result.reports.push(report);
					}
					callback(null,result.reports);
				});
			});
		});
	},
	
}