module.exports = {
	UpdateSecurityFunctionList: function(req,res){
		//console.log("Called UpdateSecurityFunctionList")
		var UtilFunctions = require('../../UtilFunctions.js');

        var updateLength = req.query.updateLength;
        var arrData = req.body;
        var sql ="";

        if(req.body && req.query.updateLength){
			var arrData = req.body;
			if(req.query.updateLength == 1){arrData = [req.body];}
			
			var del=[];
			for(var i=0; i<arrData.length; i++){
				var data=arrData[i];
				if(data.rowDeleteFlag=='D'){
					var delData = JSON.parse('{"appId":'+data.idApp+',"subsystemId":'+data.idSubSystem+',"functionId":"("}');
					if(del.indexOf(JSON.stringify(delData))===-1){
						del.push(JSON.stringify(delData));
					}
				}
			}
			for(var i=0; i<arrData.length; i++){
				var data=arrData[i];
				if(data.rowDeleteFlag=='D'){
					for(var j=0; j<del.length; j++){
						var json=JSON.parse(del[j]);
						if(json.appId==data.idApp && json.subsystemId==data.idSubSystem){
							json.functionId = json.functionId + (data.idSubSystemFunction+",");
							del[j] = JSON.stringify(json);
						}
					}
				}
			}
			for(var i=0;i<del.length;i++){
				var data = JSON.parse(del[i]);
				var functionId = data.functionId;
				functionId = functionId.substring(0,functionId.length-1)+")";
				data.functionId = functionId;
				del[i]=data;
			}
			
			//VAR DECLARATION
			sql+=""
			+"DECLARE "
			+"@del int, "
			+"@old_idApp bigint, "
			+"@old_idSubSystem bigint, "
			+"@idSubSystemFunction bigint, "
			+"@old_functionName varchar(50), "
			+"@old_functionDisplayName varchar(50), "
			+"@old_systemName varchar(50), "
			+"@old_systemDisplayName varchar(50), "
			+"@new_idApp bigint, "
			+"@new_idSubSystem bigint, "
			+"@new_functionName varchar(50), "
			+"@new_functionDisplayname varchar(50), "
			+"@new_systemName varchar(50), "
			+"@new_systemDisplayName varchar(50), "
			+"@create_system bit, "
			+"@create_function bit, "
			+"@new_function bit; "
			for(var i=0; i<arrData.length; i++){
				var data=arrData[i];
				
				sql+=""
				+"select @old_systemName=subSystemName, @old_systemDisplayName=displayName from SubSystem where idSubSystem="+data.idSubSystem+"; "
				+"select @old_idSubSystem=idSubSystem, @old_functionName=functionName, @old_functionDisplayName=displayName from SubSystemFunction where idSubSystemFunction = "+data.idSubSystemFunction+"; "
				+"SET @old_idApp=(select idApp from AppSubSystem where idAppSubSystem='"+data.idAppSubSystem+"'); "
				+"SET @new_idApp="+data.idApp+"; "
				+"SET @new_idSubSystem="+data.idSubSystem+"; "
				+"SET @new_functionName='"+data.functionName+"'; "
				+"SET @new_functionDisplayname='"+data.functionNameDisplay+"'; "
				+"SET @new_systemName='"+data.subSystemName+"'; "
				+"SET @new_systemDisplayName='"+data.subSystemNameDisplay+"'; "
				+"SET @idSubSystemFunction='"+data.idSubSystemFunction+"'; "
				+"SET @create_function=0; "
				+"SET @create_system=0; "
				+"SET @new_function=0; "
				
				var delFns="(0)";
				for(var j=0; j<del.length; j++){
					if(del[j].appId==data.idApp && del[j].subsystemId==data.idSubSystem){
						delFns = del[j].functionId;
					}
				}

				sql+="SET @del=( "
					+"select count(*) from  "
					+"appsubsystem ass inner join  "
					+"subsystemfunction ssf on ass.idSubSystem=ssf.idSubSystem "
					+"where "
					+"idApp='"+data.idApp+"' and "
					+"ass.idSubSystem='"+data.idSubSystem+"' and "
					+"ssf.rowDeleteFlag<>'D' and "
					+"idSubSystemFunction not in "+delFns+" "
				+") "
				+"IF @del=0 BEGIN " //remove subsystem from app
					+"UPDATE AppSubSystem SET "
					+"rowUpdateDt=GETDATE(), "
					+"rowUpdateUserid='"+data.rowUpdateUserid+"', "
					+"rowDeleteFlag='D' "
					+"WHERE idApp='"+data.idApp+"' AND idSubSystem='"+data.idSubSystem+"' "
				+"END ELSE BEGIN "
					//update subsystem
					+"IF @old_systemName<>@new_systemName BEGIN "
						+"UPDATE SubSystem SET "
						+"subSystemName=@new_systemName, "
						+"displayName=@new_systemDisplayName, "
						+"displaySeqNumber='"+data.subSystemNameOrder+"', "
						+"rowUpdateUserid='"+data.rowUpdateUserid+"', "
						+"rowUpdateDt=GETDATE(), "
						+"rowDeleteFlag='' "
						+"WHERE "
						+"subSystemName = @new_systemName "
						+"IF @@ROWCOUNT=0 "
						+"SET @create_system=1 "
						+"ELSE BEGIN "
						+"SET @new_idSubSystem=(select idSubSystem from SubSystem where subSystemName=@new_systemName) "
						+"UPDATE SubSystemFunction SET rowDeleteFlag='D' WHERE idSubSystemFunction='"+data.idSubSystemFunction+"' "
						+"END "
					+"END ELSE BEGIN "
						+"UPDATE SubSystem SET "
						+"subSystemName=@new_systemName, "
						+"displayName=@new_systemDisplayName, "
						+"displaySeqNumber='"+data.subSystemNameOrder+"', "
						+"rowUpdateUserid='"+data.rowUpdateUserid+"', "
						+"rowUpdateDt=GETDATE(), "
						+"rowDeleteFlag='' "
						+"WHERE "
						+"subSystemName = @old_systemName "
						+"IF @@ROWCOUNT=0 "
						+"SET @create_system=1 "
					+"END "
					
					+"IF @create_system=1 BEGIN "
					+"	INSERT INTO SubSystem (subSystemName,displayName,displaySeqNumber)VALUES( "
					+"	@new_systemName,@new_systemDisplayName,'"+data.subSystemNameOrder+"' "
					+"	) "
					+"	SET @new_idSubSystem=(select max(idSubSystem) from SubSystem) "
					+"END "
					
					+"UPDATE AppSubSystem SET rowDeleteFlag='', rowUpdateDt=GETDATE(), rowUpdateUserid='"+data.rowUpdateUserid+"' WHERE idApp=@new_idApp AND idSubSystem=@new_idSubSystem "
					+"IF @@ROWCOUNT=0 "
					+"INSERT INTO AppSubSystem(idApp,idSubSystem)VALUES(@new_idApp,@new_idSubSystem) "
										
					//update function
					+"UPDATE SubSystemFunction SET "
					+"functionName=@new_functionName, "
					+"displayName=@new_functionDisplayname, "
					+"displaySeqNumber='"+data.functionNameOrder+"', "
					+"rowUpdateUserid='"+data.rowUpdateUserid+"', "
					+"rowUpdateDt=GETDATE(), "
					+"rowDeleteFlag='"+data.rowDeleteFlag+"' "
					+"WHERE "
					+"idSubSystem=@new_idSubSystem AND (functionName=@new_functionName OR displayName=@new_functionDisplayname) "
					+"IF @@ROWCOUNT=0 BEGIN "
					+"	INSERT INTO SubSystemFunction(idSubSystem,functionName,displayName,displaySeqNumber) "
					+"	VALUES (@new_idSubSystem,@new_functionName, @new_functionDisplayname, '"+data.functionNameOrder+"') "
					+"	UPDATE SubSystemFunction SET rowDeleteFlag='D', rowUpdateDt=GETDATE(), rowUpdateUserid='"+data.rowUpdateUserid+"' WHERE idSubSystem=@new_idSubSystem AND idSubSystemFunction='"+data.idSubSystemFunction+"' "
					+"	SET @new_function=1 "
					+"	SET @idSubSystemFunction=(SELECT MAX(idSubSystemFunction) FROM SubSystemFunction) "
					+"END "
					
					+"IF @new_function=1 BEGIN "
					+"INSERT INTO FunctionSecurity (idSubSystemFunction,idSecurityGroup,securityCd,rowUpdateUserid) SELECT @idSubSystemFunction, idSecurityGroup, '0', '"+data.rowUpdateUserid+"' FROM SecurityGroup; "
					+"END; " 
					
					//change app
					+"IF @old_idApp<>@new_idApp BEGIN "
					+"UPDATE AppSubSystem SET "
					+"rowUpdateDt=GETDATE(), "
					+"rowUpdateUserid='"+data.rowUpdateUserid+"', "
					+"rowDeleteFlag='' "
					+"WHERE idApp=@new_idApp AND idSubSystem=@new_idSubSystem "
						+"IF @@ROWCOUNT=0 BEGIN "
						+"INSERT INTO AppSubSystem(idApp,idSubSystem) VALUES(@new_idApp,@new_idSubSystem) "
						+"END ELSE BEGIN "
						+"UPDATE AppSubSystem SET rowDeleteFlag='D' WHERE idAppSubSystem='"+data.idAppSubSystem+"' "
						+"END "
					+"END "
					
				+"END "
                                if(data.rowDeleteFlag == 'D'){
                                    sql+="EXECUTE spDeleteSubSystemFunction '"+data.idSubSystemFunction+"','"+data.rowUpdateUserid+"' "
                                }
                                    
			}
			// console.log(sql);
			// res.status(200).json({"success":false,"data":"success"});
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"Invalid request body"});
		}
	}
}
