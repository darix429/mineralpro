module.exports = {

	CreateSecurityFunctionList: function(req,res){
		//console.log("Called CreateSecurityFunctionList")
		var UtilFunctions = require('../../UtilFunctions.js');

        var createLength = req.query.createLength;
        var arrData = req.body;
        var sqlSelect = ""
        var finalSql = ''
        var idSubSystem = ""

		if(req.body){
			var arrData = req.body;
			if(req.query.createLength == 1){arrData = [req.body];}
			//console.log(arrData);
			
			var sql = "DECLARE "
					+"@idApp bigint, "
					+"@idSubSystem bigint, "
					+"@new_function bigint, "
					+"@idSubSystemFunction bigint ";
			for(var i=0; i<arrData.length; i++){
				sql+="SET @idApp='"+arrData[i].idApp+"' "
					+"SET @idSubSystem=(select idSubSystem from SubSystem where subSystemName='"+arrData[i].subSystemName+"') "
					+"SET @new_function=0; "
					
					+"UPDATE SubSystem SET "
						+"displayName='"+arrData[i].subSystemNameDisplay+"', "
						+"displaySeqNumber='"+arrData[i].subSystemNameOrder+"', "
						+"rowUpdateUserid='"+arrData[i].rowUpdateUserid+"', "
						+"rowUpdateDt=GETDATE(), "
						+"rowDeleteFlag='' "
						+"WHERE "
						+"subSystemName='"+arrData[i].subSystemName+"' "
					+"IF @@ROWCOUNT=0 BEGIN "
						+"INSERT INTO SubSystem( "
						+"subSystemName, "
						+"displayname, "
						+"displaySeqNumber) "
						+"VALUES( "
						+"'"+arrData[i].subSystemName+"', "
						+"'"+arrData[i].subSystemNameDisplay+"', "
						+"'"+arrData[i].subSystemNameOrder+"' "
						+") "
						+"SET @idSubSystem=(SELECT MAX(idSubSystem) FROM SubSystem) "			
					+"END "
					
					+"UPDATE AppSubSystem SET "
						+"rowDeleteFlag='"+arrData[i].rowDeleteFlag+"', "
						+"rowUpdateDt=GETDATE(), "
						+"rowUpdateUserid='"+arrData[i].rowUpdateUserid+"' "
						+"WHERE "
						+"idApp=@idApp AND idSubSystem=@idSubSystem "
					+"IF @@ROWCOUNT=0 BEGIN "
						+"INSERT INTO AppSubSystem(idApp,idSubSystem,rowDeleteFlag) "
						+"VALUES(@idApp,@idSubSystem,'"+arrData[i].rowDeleteFlag+"') "
					+"END "
					
					+"UPDATE SubSystemFunction SET "
						+"idSubSystem=@idSubSystem, "
						+"functionName='"+arrData[i].functionName+"', "
						+"displayName='"+arrData[i].functionNameDisplay+"', "
						+"displaySeqNumber='"+arrData[i].functionNameOrder+"', "
						+"rowUpdateUserid='"+arrData[i].rowUpdateUserid+"', "
						+"rowUpdateDt=GETDATE(), "
						+"rowDeleteFlag='' "
						+"WHERE "
						+"idSubSystem=@idSubSystem AND (functionName='"+arrData[i].functionName+"' OR displayName='"+arrData[i].functionNameDisplay+"') "
					+"IF @@ROWCOUNT=0 BEGIN "
						+"INSERT INTO SubSystemFunction( "
						+"idSubsystem, "
						+"functionName, "
						+"displayName, "
						+"displaySeqNumber, "
						+"rowDeleteFlag "
						+")VALUES( "
						+"@idSubSystem, "
						+"'"+arrData[i].functionName+"', "
						+"'"+arrData[i].functionNameDisplay+"', "
						+"'"+arrData[i].functionNameOrder+"', "
						+"'"+arrData[i].rowDeleteFlag+"' "
						+") "
						+"SET @idSubSystemFunction=(SELECT MAX(idSubSystemFunction) from SubSystemFunction) "
						+"SET @new_function=1 "
					+"END ELSE BEGIN "
						+"SET @idSubSystemFunction=(SELECT idSubSystemFunction FROM SubSystemFunction WHERE idSubSystem=@idSubSystem AND (functionName='"+arrData[i].functionName+"' OR displayName='"+arrData[i].functionNameDisplay+"')) "
					+"END "
					
					+"IF @new_function=1 BEGIN "
					+"INSERT INTO FunctionSecurity (idSubSystemFunction,idSecurityGroup,securityCd,rowUpdateUserid) SELECT @idSubSystemFunction, idSecurityGroup, '0', '"+arrData[i].rowUpdateUserid+"' FROM SecurityGroup "
					+"END "
			}
			//console.log(sql)
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"No data received"});
		}

    }
}