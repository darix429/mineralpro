<?php

include "../../../../../api/SystemConfig.php";
include "../../../../../api/UtilFunctions.php";

$createLength = $_GET['createLength'];

$info = file_get_contents('php://input');
echo $info;

$arrdata = json_decode($info, true);

//ob_start();
//var_dump($arrdata);
//$output = ob_get_clean();
//
//$file = "output.txt";
//file_put_contents($file, $output, FILE_APPEND | LOCK_EX);

$arr_len = count($arrdata);

if ($createLength == 1) {

    $sql = "SELECT TOP 1 ss.idSubSystem FROM SubSystem ss
            INNER JOIN AppSubSystem ass ON ss.idSubSystem = ass.idSubSystem AND ass.rowDeleteFlag = ''
            WHERE ss.lower(subSystemName) = ss.lower('" . $arrdata['subSystemName'] . "' 
                AND ass.idApp = '" . $arrdata['idApp'] . "'
                AND ss.rowDeleteFlag = ''
             )";
    
    $rows = execSQL('select', $sql);
    if (count($rows) > 0) {             
        $sql = "INSERT INTO SubSystemFunction 
                (idSubSystem, functionName, displayName, displaySeqNumber, rowUpdateUserid, rowDeleteFlag)
                VALUES
                ('$idSubSystem',
                 '" . $arrdata['functionName'] . "',  
                 '" . $arrdata['functionNameDisplay'] . "',  
                 '" . $arrdata['functionNameOrder'] . "', 
                 '" . $arrdata['rowUpdateUserid'] . "',
                 '" . $arrdata['rowDeleteFlag'] . "'
                )";
        $idSubSystemFunction = execSQL('insert', $sql);
    } else {
        $sql = "INSERT INTO SubSystem
                (subSystemName, displayName, displaySeqNumber, rowUpdateUserid, rowDeleteFlag)
                VALUES
                ('" . $arrdata['subSystemName'] . "',  
                 '" . $arrdata['subSystemNameDisplay'] . "',  
                 '" . $arrdata['subSystemNameOrder'] . "', 
                 '" . $arrdata['rowUpdateUserid'] . "',
                 '" . $arrdata['rowDeleteFlag'] . "'
                )";
        $idSubSystem = execSQL('insert', $sql);
        
        $sql = "INSERT INTO AppSubSystem(idApp, idSubSystem)
                VALUES (
                    '" . $arrdata['idApp'] . "',  
                    '$idSubSystem'
                )";
        execSQL('insert', $sql);

        $sql = "INSERT INTO SubSystemFunction 
                (idSubSystem, functionName, displayName, displaySeqNumber, rowUpdateUserid, rowDeleteFlag)
                VALUES
                ('$idSubSystem',
                 '" . $arrdata['functionName'] . "',  
                 '" . $arrdata['functionNameDisplay'] . "',  
                 '" . $arrdata['functionNameOrder'] . "', 
                 '" . $arrdata['rowUpdateUserid'] . "',
                 '" . $arrdata['rowDeleteFlag'] . "'
                )";
        $idSubSystemFunction = execSQL('insert', $sql);
    }

    $sql = "INSERT INTO FunctionSecurity 
            (idSubSystemFunction , idSecurityGroup, securityCd, rowUpdateUserid, rowDeleteFlag)
            SELECT '$idSubSystemFunction', idSecurityGroup, '0', '" . $arrdata['rowUpdateUserid'] . "', '' FROM SecurityGroup";

    print_r($sql);
    
    execSQL('insert', $sql);
    
} else {
    for ($ctr = $arr_len - 1; $ctr > -1; $ctr--) {
        $sql = "SELECT TOP 1 ss.idSubSystem FROM SubSystem ss
            INNER JOIN AppSubSystem ass ON ss.idSubSystem = ass.idSubSystem AND ass.rowDeleteFlag = ''
            WHERE ss.lower(subSystemName) = ss.lower('" . $arrdata[$ctr]['subSystemName'] . "' 
                AND ass.idApp = '" . $arrdata[$ctr]['idApp'] . "'
                AND ss.rowDeleteFlag = ''
             )";
            
        $rows = execSQL('select', $sql);
        if (count($rows) > 0) {            
            $sql = "INSERT INTO SubSystemFunction 
                (idSubSystem, functionName, displayName, displaySeqNumber, rowUpdateUserid, rowDeleteFlag)
                VALUES
                ('$idSubSystem',
                 '" . $arrdata[$ctr]['functionName'] . "',  
                 '" . $arrdata[$ctr]['functionNameDisplay'] . "',  
                 '" . $arrdata[$ctr]['functionNameOrder'] . "', 
                 '" . $arrdata[$ctr]['rowUpdateUserid'] . "',
                 '" . $arrdata[$ctr]['rowDeleteFlag'] . "'
                )";
            $idSubSystemFunction = execSQL('insert', $sql);
        } else {
            $sql = "INSERT INTO SubSystem
                (subSystemName, displayName, displaySeqNumber, rowUpdateUserid, rowDeleteFlag)
                VALUES
                ('" . $arrdata[$ctr]['subSystemName'] . "',  
                 '" . $arrdata[$ctr]['subSystemNameDisplay'] . "',  
                 '" . $arrdata[$ctr]['subSystemNameOrder'] . "', 
                 '" . $arrdata[$ctr]['rowUpdateUserid'] . "',
                 '" . $arrdata[$ctr]['rowDeleteFlag'] . "'
                )";
            $idSubSystem = execSQL('insert', $sql);

            $sql = "INSERT INTO AppSubSystem(idApp, idSubSystem)
                VALUES (
                    '" . $arrdata[$ctr]['idApp'] . "',  
                    '$idSubSystem'
                )";
            execSQL('insert', $sql);

            $sql = "INSERT INTO SubSystemFunction 
                (idSubSystem, functionName, displayName, displaySeqNumber, rowUpdateUserid, rowDeleteFlag)
                VALUES
                ('$idSubSystem',
                 '" . $arrdata[$ctr]['functionName'] . "',  
                 '" . $arrdata[$ctr]['functionNameDisplay'] . "',  
                 '" . $arrdata[$ctr]['functionNameOrder'] . "', 
                 '" . $arrdata[$ctr]['rowUpdateUserid'] . "',
                 '" . $arrdata[$ctr]['rowDeleteFlag'] . "'
                )";
            $idSubSystemFunction = execSQL('insert', $sql);
        }

        $sql = "INSERT INTO FunctionSecurity 
            (idSubSystemFunction , idSecurityGroup, securityCd, rowUpdateUserid, rowDeleteFlag)
            SELECT '$idSubSystemFunction', idSecurityGroup, '0', '" . $arrdata[$ctr]['rowUpdateUserid'] . "', '' FROM SecurityGroup";

        execSQL('insert', $sql);
    }
}
?>




