<?php

include "../../../../../api/SystemConfig.php";
include "../../../../../api/UtilFunctions.php";

$sql = "SELECT a.idApp,
	   a.appName,
	   ss.idSubSystem,
	   ss.subSystemName,
	   ss.displayName subSystemNameDisplay,
	   ss.displaySeqNumber subSystemNameOrder,
	   ssf.idSubSystemFunction,
	   ssf.functionName,
	   ssf.displayName functionNameDisplay,
	   ssf.displaySeqNumber functionNameOrder,
	   ssf.idSubSystemFunction noteFilter,
	   ssf.rowDeleteFlag
        FROM App a
        INNER JOIN AppSubSystem ass ON a.idApp = ass.idApp AND ass.rowDeleteFlag = ''
        INNER JOIN SubSystem ss ON ass.idSubSystem = ss.idSubSystem AND ss.rowDeleteFlag = ''
        INNER JOIN SubSystemFunction ssf ON ss.idSubSystem = ssf.idSubSystem AND ssf.rowDeleteFlag = ''
        WHERE a.rowDeleteFlag = ''
        ORDER BY idApp DESC, ss.displaySeqNumber, ssf.displaySeqNumber ASC";

$rows = execSQL('select', $sql);

$output = "{'success': true, 'data': " . json_encode($rows) . "}";

# print out the results in json format
echo $output;
?>



