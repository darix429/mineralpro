module.exports = {
	ReadSecurityFunctionList: function(req,res){
		//console.log("Called ReadSecurityFunctionList")
		var UtilFunctions = require('../../UtilFunctions.js');

		var sql = "SELECT a.idApp,"
		+"a.appName, "
		+"ass.idAppSubSystem, "
		+"ss.idSubSystem, "
		+"ss.subSystemName, "
		+"ss.displayName subSystemNameDisplay, "
		+"ss.displaySeqNumber subSystemNameOrder, "
		+"ssf.idSubSystemFunction, "
		+"ssf.functionName, "
		+"ssf.displayName functionNameDisplay, "
		+"ssf.displaySeqNumber functionNameOrder, "
		+"ssf.idSubSystemFunction noteFilter, "
		+"ssf.rowDeleteFlag "
		+"FROM App a "
		+"INNER JOIN AppSubSystem ass ON a.idApp = ass.idApp AND ass.rowDeleteFlag = '' "
		+"INNER JOIN SubSystem ss ON ass.idSubSystem = ss.idSubSystem AND ss.rowDeleteFlag = '' "
		+"INNER JOIN SubSystemFunction ssf ON ss.idSubSystem = ssf.idSubSystem AND ssf.rowDeleteFlag = '' "
		+"WHERE a.rowDeleteFlag = '' "
		+"ORDER BY idApp DESC, ss.displaySeqNumber, ssf.displaySeqNumber ASC"; 
		
		UtilFunctions.execSql(sql, res)

	}
}