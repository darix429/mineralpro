module.exports = {
    UpdateOrganization: function (req, res) {
        //console.log("Called UpdateOrganization")
        var UtilFunctions = require('../../UtilFunctions.js');
        
        if(req.body && req.query.updateLength){
            var arrData = req.body;
            if(req.query.updateLength == 1){arrData = [req.body];}
            
            var sql = "";     
            for (var ctr = 0; ctr < arrData.length; ctr++) {
                sql += "UPDATE organization SET "
                        +" organizationNumber = '" + arrData[ctr].organizationNumber + "',"
                        +" organizationName = UPPER('" + arrData[ctr].organizationName + "'),"
                        +" statusCd = '" + arrData[ctr].code + "',"
                        +" rowUpdateDt = GETDATE(), "
                        +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "'," 
                        +" rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "'"
                    +" WHERE idOrganization = '" + arrData[ctr].idOrganization + "';";                  
            }
            UtilFunctions.execSql(sql, res);
        }else{
            res.status(400).json({"success":false,"data":"Invalid request body"});
        }
    }//end UpdateOrganization
};
