module.exports = function(app){

//-------------CRUD Listeners for OrganizationList (use Get for read and Post for others)----------------
    app.get('/api/InformationServices/OrganizationOrganizationList/ReadOrganizationList', function(req, res){ 
        var ReadOrganization = require('./ReadOrganization.js');   
        ReadOrganization.ReadOrganization(req, res);        
    });

    app.post('/api/InformationServices/OrganizationOrganizationList/CreateOrganizationList', function(req, res){ 
        var CreateOrganization = require('./CreateOrganization.js');   
        CreateOrganization.CreateOrganization(req, res);        
    });

    app.post('/api/InformationServices/OrganizationOrganizationList/UpdateOrganizationList', function(req, res){ 
        var UpdateOrganization = require('./UpdateOrganization.js');   
        UpdateOrganization.UpdateOrganization(req, res);        
    });
//----------------------------------End Listener for OrganizationList-------------------------------------
}