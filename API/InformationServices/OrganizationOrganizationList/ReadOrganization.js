module.exports = {
    ReadOrganization: function (req, res) {
        //console.log("Called ReadOrganization")
        var UtilFunctions = require('../../UtilFunctions.js');
        var sql = "SELECT o.idOrganization, "
            +" count(uo.idUserOrganization) + count(oc.idOrganizationContact) enableDeleteCount, "
            +" o.organizationNumber, "
            +" o.organizationName, "
            +" sc.code, "
            +" sc.description "
            +" FROM Organization o "
            +" LEFT JOIN OrganizationContact oc ON oc.idOrganization = o.idOrganization AND oc.rowDeleteFlag = '' "
            +" LEFT JOIN UserOrganization uo ON uo.idOrganization = o.idOrganization AND uo.rowDeleteFlag= '' "
            +" LEFT JOIN SystemCode sc ON sc.code = o.statusCd AND sc.rowDeleteFlag= '' AND sc.codeType = 30 AND sc.appraisalYear ='" +req.query.appraisalYear+ "' "
            +" WHERE o.rowDeleteFlag = '' "
            +" GROUP BY o.idOrganization, o.organizationNumber, o.organizationName, sc.description, sc.code "
            +" ORDER BY o.idOrganization;";
        UtilFunctions.execSql(sql, res)
    },//end ReadOrganization
};
