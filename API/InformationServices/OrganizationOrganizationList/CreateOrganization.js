module.exports = {
    CreateOrganization: function (req, res) {
        //console.log("Called CreateOrganization")
        var UtilFunctions = require('../../UtilFunctions.js');
        var createLength = req.query.createLength;
        var arrData = req.body;
        var sql = "";

        if(req.body){
            var arrData = req.body;
            if(req.query.createLength == 1){arrData = [req.body];}
            //console.log(arrData);
                
            for(var ctr=0; ctr<arrData.length; ctr++){
                sql += "UPDATE Organization SET "
                    +"organizationName = UPPER('"+arrData[ctr].organizationName+"'), "
                    +"statusCd = '"+arrData[ctr].code+"', "
                    +"rowUpdateUserid = '"+arrData[ctr].rowUpdateUserid+"', "
                    +"rowDeleteFlag = '"+arrData[ctr].rowDeleteFlag+"' "
                    +"WHERE "
                    +"organizationNumber = '"+arrData[ctr].organizationNumber+"' "
                    +"IF @@ROWCOUNT = 0 "
                sql += "INSERT INTO Organization ( "
                    +" organizationNumber, "
                    +" organizationName, "
                    +" statusCd, "
                    +" rowUpdateUserid, "
                    +" rowDeleteFlag "
                    +" )VALUES( "
                    +" '"+arrData[ctr].organizationNumber+"', "
                    +" UPPER('"+arrData[ctr].organizationName+"'), "
                    +" '"+arrData[ctr].code+"', "
                    +" '"+arrData[ctr].rowUpdateUserid+"', "
                    +" '"+arrData[ctr].rowDeleteFlag+"' "
                    +" );";
            }
            // console.log(sql)
            UtilFunctions.execSql(sql, res);
        }else{
            res.status(400).json({"success":false,"data":"No data received"});
        }
    },//end CreateOrganization
    
};
