module.exports = {
    read: function (req, res) {
        // console.log("Called readReportExport")
        var UtilFunctions = require('../UtilFunctions.js');
        var cat = (req.params.cat == 'CAD' ? 'Report for CAD' : req.params.cat == 'STATE' ? 'Report to State': null)
        // console.log(cat)
        var sql = ""
            +"SELECT "
            +"reportId, "
            +"fileName, "
            +"appraisalYear,"
            +"reportCategoryCdx as reportCategory,"
            +"reportTypeCdx as reportType,"
            +"ltrim(description) as reportDesc,"
            +"displaySeqNumber as displayOrder "
            +"FROM Report "
            +"WHERE appraisalYear = '"+req.query.appraisalYear+"'"
            +"AND reportCategoryCdx = '"+cat+"'"
            +"AND rowDeleteFlag = '' "
			+"AND description NOT IN ('Public Records','Notice Of Determination','Appraisal Year By Year Analysis','JournalEntry');";
        UtilFunctions.execSql(sql, res)
    },//end readReportExportCAD
};

//Appraisal Year, Report Category, Report Type, Report Description, Display Order