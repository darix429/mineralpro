
module.exports = function(app){        
    require('./OrganizationOrganizationList/OrganizationList.js')(app);
    require('./SystemAdminSecuritySecurityUserList/SecurityUserList.js')(app);
    require('./SystemAdminSecuritySecurityGroup/SecurityGroup.js')(app);
}
