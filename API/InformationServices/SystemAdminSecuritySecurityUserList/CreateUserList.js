module.exports = {
    CreateUserList: function (req, res) {
        //console.log("Called CreateUserList");
        var UtilFunctions = require('../../UtilFunctions.js');
        var bcrypt = require('bcrypt-nodejs');
		if(req.body){
			var arrData = req.body;
			if(req.query.createLength == 1){arrData = [req.body];}			
			var sql = "";
			for(var i=0; i<arrData.length; i++){
				if(arrData[i].newpassword != ''){
					var hash_p = bcrypt.hashSync(arrData[i].newpassword);
				}else{
					var hash_p = bcrypt.hashSync(arrData[i].password);
				}
				sql += ""
					+"UPDATE AppUser SET "
						+"emailAddress='" + arrData[i].emailAddress + "', "
						+"password='" + hash_p + "', "
						+"idOrganizationContact='" + arrData[i].idOrganizationContact + "', "
						+"rowUpdateDt = GETDATE(), "
						+"rowUpdateUserid='" + arrData[i].rowUpdateUserid + "', "
						+"rowDeleteFlag='' "
						+"WHERE "
						+"idOrganizationContact='" + arrData[i].idOrganizationContact + "' "
						+"AND rowDeleteFlag='D' "
					+"IF @@ROWCOUNT=0 BEGIN"	
						+" INSERT INTO AppUser (emailAddress, password, idOrganizationContact, rowUpdateUserid, rowDeleteFlag) "
						+" VALUES("
						+" '" + arrData[i].emailAddress + "', "
						+" '" + hash_p + "', "
						+" '" + arrData[i].idOrganizationContact + "', "    
						+" '" + arrData[i].rowUpdateUserid + "', "
						+" '" + arrData[i].rowDeleteFlag + "'); "

						+" INSERT INTO UserOrganization (idAppUser, idOrganization, rowUpdateUserid, rowDeleteFlag) "
						+" SELECT SCOPE_IDENTITY(), "
						+" '"+arrData[i].idOrganization+"', "
						+" '"+arrData[i].rowUpdateUserid+"', "
						+" '"+arrData[i].rowDeleteFlag+"'; "

						+" INSERT INTO UserOrgSecurity (idUserOrganization, idSecurityGroup, rowUpdateUserid, rowDeleteFlag) "
						+" SELECT SCOPE_IDENTITY(), "
						+" '"+arrData[i].idSecurityGroup+"', "
						+" '"+arrData[i].rowUpdateUserid+"', "
						+" '"+arrData[i].rowDeleteFlag+"'; "                                                                                      
					+"END;"
					+"ELSE BEGIN "
						+"UPDATE UserOrganization SET rowDeleteFlag='' WHERE idAppUser=(select idAppUser from AppUser where idOrganizationContact='"+arrData[i].idOrganizationContact+"'); "
						+"UPDATE UserOrgSecurity SET rowDeleteFlag='' WHERE idUserOrganization=(select idUserOrganization from UserOrganization where idAppUser=(select idAppUser from AppUser where idOrganizationContact='"+arrData[i].idOrganizationContact+"')); "
					+"END; "
                                
                                        +"UPDATE AppUser "
                                        +"SET nameAbbreviation = oc.nameAbbreviation "
                                        +"FROM AppUser a "
                                        +"JOIN OrganizationContact oc ON a.idOrganizationContact = oc.idOrganizationContact "
                                        +"WHERE a.idOrganizationContact = '" + arrData[i].idOrganizationContact + "'; "
			}
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"No data received"});
		}
    },//end CreateUserList
};
