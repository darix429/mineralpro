module.exports = {
    UpdateUserList: function (req, res) {
        //console.log("Called UpdateUserList")
        var UtilFunctions = require('../../UtilFunctions.js');               
        var bcrypt = require('bcrypt-nodejs');
		if(req.body && req.query.updateLength){
			var arrData = req.body;
			if(req.query.updateLength == 1){arrData = [req.body];}
			
			var sql = "";     
			for (var ctr = 0; ctr < arrData.length; ctr++) {
				if (arrData[ctr].newpassword != null && arrData[ctr].newpassword != '') {
					var hash_p = bcrypt.hashSync(arrData[ctr].newpassword);
					sql += "UPDATE AppUser SET "
						+" idOrganizationContact= '" + arrData[ctr].idOrganizationContact + "', "
						+" emailAddress='" + arrData[ctr].emailAddress + "', "
						+" rowUpdateDt = GETDATE(), "
						+" rowUpdateUserid='" + arrData[ctr].rowUpdateUserid + "', "
						+" rowDeleteFlag ='" + arrData[ctr].rowDeleteFlag + "', "
						+" password='" + hash_p + "' "
					+" WHERE idAppUser = '" + arrData[ctr].idAppUser + "'; ";                                        
                                        CLIENT.broadcast.emit('passwordChanged', arrData[ctr].idAppUser);  //logout the user
					} else {
						sql += "UPDATE AppUser SET "
							+" idOrganizationContact= '" + arrData[ctr].idOrganizationContact + "', "
							+" emailAddress='" + arrData[ctr].emailAddress + "', "
							+" rowUpdateDt = GETDATE(), "
							+" rowUpdateUserid='" + arrData[ctr].rowUpdateUserid + "', "
							+" rowDeleteFlag ='" + arrData[ctr].rowDeleteFlag + "' "
						+" WHERE idAppUser = '" + arrData[ctr].idAppUser + "'; ";
					}

					sql += "UPDATE dbo.UserOrganization SET "
							+" idAppUser = '" + arrData[ctr].idAppUser + "', "
							+" idOrganization = '" + arrData[ctr].idOrganization + "', "
							+" rowUpdateDt = GETDATE(), "
							+" rowUpdateUserid='" + arrData[ctr].rowUpdateUserid + "', "    
							+" rowDeleteFlag= '" + arrData[ctr].rowDeleteFlag + "' "
						+" WHERE idUserOrganization = '" + arrData[ctr].idUserOrganization + "'; "

					+" UPDATE dbo.UserOrgSecurity SET "
							+" idUserOrganization = '" + arrData[ctr].idUserOrganization + "', "
							+" idSecurityGroup = '" + arrData[ctr].idSecurityGroup + "', "
							+" rowUpdateDt = GETDATE(), "
							+" rowUpdateUserid='" + arrData[ctr].rowUpdateUserid + "', "  
							+" rowDeleteFlag= '" + arrData[ctr].rowDeleteFlag + "' "
						+" WHERE idUserOrgSecurity = '" + arrData[ctr].idUserOrgSecurity + "'; ";
                                        
                                        if(arrData[ctr].rowDeleteFlag == 'D'){
                                            sql +="EXECUTE spDeleteAppUser '" + arrData[ctr].rowUpdateUserid + "','" + arrData[ctr].idAppUser + "'";
                                        }
			}

			//console.log(sql)
			UtilFunctions.execSql(sql, res);
		}else{
			res.status(400).json({"success":false,"data":"Invalid request body"});
		}
    },//end UpdateUserList
};
