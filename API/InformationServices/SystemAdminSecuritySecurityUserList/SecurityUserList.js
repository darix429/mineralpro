module.exports = function(app){

//---------------CRUD Listeners for SecurityUserList (use Get for read and Post for others))--------------
    app.get('/api/InformationService/SystemAdminSecuritySecurityUserList/ReadOrgContact', function(req, res){ 
        var ReadOrgContact = require('./ReadOrgContact.js');   
        ReadOrgContact.ReadOrgContact(req, res);        
    });
    
    app.get('/api/InformationService/SystemAdminSecuritySecurityUserList/ReadOrganizationName', function(req, res){ 
        var ReadOrganizationName = require('./ReadOrganizationName.js');   
        ReadOrganizationName.ReadOrganizationName(req, res);        
    });

    app.get('/api/InformationService/SystemAdminSecuritySecurityUserList/ReadSecurityGroup', function(req, res){ 
        var ReadSecurityGroup = require('./ReadSecurityGroup.js');   
        ReadSecurityGroup.ReadSecurityGroup(req, res);        
    });

    app.get('/api/InformationService/SystemAdminSecuritySecurityUserList/ReadUserList', function(req, res){ 
        var ReadUserList = require('./ReadUserList.js');   
        ReadUserList.ReadUserList(req, res);        
    });

    app.post('/api/InformationService/SystemAdminSecuritySecurityUserList/CreateUserList', function(req, res){ 
        var CreateUserList = require('./CreateUserList.js');   
        CreateUserList.CreateUserList(req, res);        
    });

    app.post('/api/InformationService/SystemAdminSecuritySecurityUserList/UpdateUserList', function(req, res){ 
        var UpdateUserList = require('./UpdateUserList.js');   
        UpdateUserList.UpdateUserList(req, res);        
    });
//----------------------------------End Listener for SecurityUserList-------------------------------------
}