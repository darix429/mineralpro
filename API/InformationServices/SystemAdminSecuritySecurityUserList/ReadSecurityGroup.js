module.exports = {
    ReadSecurityGroup: function (req, res) {
        //console.log("Called ReadSecurityGroup")
        var UtilFunctions = require('../../UtilFunctions.js');
        var sql = "SELECT idSecurityGroup, "
                    +" groupName "
                +" FROM SecurityGroup "  
                +" WHERE rowDeleteFlag = '' " 
                +" ORDER BY groupName ASC;";
        UtilFunctions.execSql(sql, res)
    },//end ReadSecurityGroup   
};
