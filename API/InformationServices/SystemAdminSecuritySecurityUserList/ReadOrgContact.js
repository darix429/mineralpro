module.exports = {
    ReadOrgContact: function (req, res) {
        //console.log("Called ReadOrgContact")
        var UtilFunctions = require('../../UtilFunctions.js');
        var idOrganization = req.query.idOrganization;
        var sql = "SELECT "
                    +" oc.idOrganizationContact, "
                    +" sc.description, "
                    +" oc.firstName + ' ' + oc.lastName organizationContact "
                +" FROM dbo.OrganizationContact oc "
                +" LEFT JOIN SystemCode sc ON sc.code = oc.statusCd AND sc.rowDeleteFlag= '' AND sc.codeType = 30 AND sc.appraisalYear ='" +req.query.appraisalYear+ "' "
                +" WHERE idOrganization = '"+idOrganization+"' "
                    // +" AND idOrganizationContact NOT IN (SELECT idOrganizationContact FROM AppUser WHERE NOT rowDeleteFlag='D')"
                // +" AND rowDeleteFlag = '' AND statusCd = 0 "
                +"ORDER BY firstName ASC;";
        UtilFunctions.execSql(sql, res)
    },//end ReadOrgContact
};