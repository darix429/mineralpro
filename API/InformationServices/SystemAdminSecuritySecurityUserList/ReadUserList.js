module.exports = { 
    ReadUserList: function (req, res) {
        //console.log("Called ReadUserList")
        var UtilFunctions = require('../../UtilFunctions.js');
        var sql = "SELECT "
            +" ISNULL(v.cnt, 0) enableDeleteCount,"
            +" au.idAppUser, "
            +" au.emailAddress, "
            +" '' newpassword, "
            +" au.idOrganizationContact, "
            +" o.idOrganization, "
            +" o.organizationName, "
            +" uo.idUserOrganization, "
            +" sg.groupName, "
            +" oc.statusCd, "
            +" sc.description, "
            +" sg.idSecurityGroup, "
            +" uos.idUserOrgSecurity, "
            +" oc.firstName + ' ' + oc.lastName organizationContact "
        +" FROM AppUser au "
        +" INNER JOIN UserOrganization uo ON au.idAppUser = uo.idAppUser AND uo.rowDeleteFlag = '' "
        +" INNER JOIN OrganizationContact oc ON au.idOrganizationContact = oc.idOrganizationContact AND oc.rowDeleteFlag = '' "
        +" INNER JOIN Organization o ON o.idOrganization = uo.idOrganization AND o.rowDeleteFlag = '' "
        +" INNER JOIN UserOrgSecurity uos ON uo.idUserOrganization = uos.idUserOrganization AND uos.rowDeleteFlag = '' "
        +" INNER JOIN SecurityGroup sg ON uos.idSecurityGroup = sg.idSecurityGroup AND sg.rowDeleteFlag = '' "
        +" LEFT JOIN SystemCode sc ON sc.code = oc.statusCd AND sc.rowDeleteFlag= '' AND sc.codeType = 30 AND sc.appraisalYear ='" +req.query.appraisalYear+ "' "
        +" LEFT JOIN vAppUserSummary v ON v.rowUpdateUserid = au.idAppUser "
        +" WHERE au.rowDeleteFlag = '' ORDER BY au.idAppUser DESC;";
        UtilFunctions.execSql(sql, res)
    },//end ReadUserList
};
