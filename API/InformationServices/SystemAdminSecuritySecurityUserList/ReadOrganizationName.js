module.exports = {
    ReadOrganizationName: function (req, res) {
        //console.log("Called ReadOrganizationName")
        var UtilFunctions = require('../../UtilFunctions.js');
        var sql = "SELECT o.idOrganization, "
                    +" o.organizationName "
		+" FROM dbo.Organization o "
		+" WHERE o.rowDeleteFlag = '' "
                +" ORDER BY o.organizationName ASC;";
        UtilFunctions.execSql(sql, res)
    },//end ReadOrganizationName
};
