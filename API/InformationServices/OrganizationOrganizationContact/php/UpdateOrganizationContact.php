<?php

include "../../../../../api/SystemConfig.php";
include "../../../../../api/UtilFunctions.php";

$updateLength = $_GET['updateLength'];

$info = file_get_contents('php://input');
echo $info;

$arrdata = json_decode($info, true);
$arr_len = count($arrdata);

if ($updateLength == 1) {
    $sql = "UPDATE OrganizationContact SET
                idOrganization = '" . $arrdata['idOrganization'] . "',
                contactNumber = '" . $arrdata['contactNumber'] . "',  
                firstName = '" . $arrdata['firstName'] . "',  
                lastName = '" . $arrdata['lastName'] . "',  
                middleInitial = '" . $arrdata['middleInitial'] . "',  
                nameAbbreviation = '" . $arrdata['nameAbbreviation'] . "',  
                title = '" . $arrdata['title'] . "',      
                statusCd = '" . $arrdata['statusCd'] . "',
                rowUpdateUserid = '" . $arrdata['rowUpdateUserid'] . "', 
                rowDeleteFlag = '" . $arrdata['rowDeleteFlag'] . "'     
            WHERE idOrganizationContact = '" . $arrdata['idOrganizationContact'] . "'";
    execSQL('update', $sql);
    
} else {
    for ($ctr = $arr_len - 1; $ctr > -1; $ctr--) {
        $sql = "UPDATE OrganizationContact SET
                idOrganization = '" . $arrdata[$ctr]['idOrganization'] . "',
                contactNumber = '" . $arrdata[$ctr]['contactNumber'] . "',  
                firstName = '" . $arrdata[$ctr]['firstName'] . "',  
                lastName = '" . $arrdata[$ctr]['lastName'] . "',  
                middleInitial = '" . $arrdata[$ctr]['middleInitial'] . "',  
                nameAbbreviation = '" . $arrdata[$ctr]['nameAbbreviation'] . "',  
                title = '" . $arrdata[$ctr]['title'] . "',      
                statusCd = '" . $arrdata[$ctr]['statusCd'] . "',
                rowUpdateUserid = '" . $arrdata[$ctr]['rowUpdateUserid'] . "', 
                rowDeleteFlag = '" . $arrdata[$ctr]['rowDeleteFlag'] . "'     
            WHERE idOrganizationContact = '" . $arrdata[$ctr]['idOrganizationContact'] . "'";
        execSQL('update', $sql);
    }
}
?>


