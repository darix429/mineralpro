
<?php

include "../../../../../api/SystemConfig.php";
include "../../../../../api/UtilFunctions.php";

$sql = "SELECT oc.idOrganizationContact,
	   oc.idOrganization,
	   o.organizationName,
	   oc.contactNumber,
	   oc.firstName,
	   oc.lastName,
	   oc.middleInitial,
	   oc.nameAbbreviation,
	   oc.title,
	   oc.statusCd
        FROM OrganizationContact oc
        INNER JOIN Organization o on oc.idOrganization = o.idOrganization AND o.rowDeleteFlag = ''
        WHERE oc.rowDeleteFlag = ''";

$rows = execSQL('select', $sql);

$output = "{'success': true, 'data': " . json_encode($rows) . "}";

# print out the results in json format
echo $output;

?>







