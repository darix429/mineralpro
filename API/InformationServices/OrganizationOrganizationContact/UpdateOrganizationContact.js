module.exports = {
	UpdateOrganizationContact: function(req,res){
		//console.log("Called UpdateOrganizationContact")
		var UtilFunctions = require('../../UtilFunctions.js');

       if(req.body && req.query.updateLength){
            var arrData = req.body;
            if(req.query.updateLength == 1){arrData = [req.body];}
            
            var sql = "";     
            for (var ctr = 0; ctr < arrData.length; ctr++) {
				sql += "UPDATE OrganizationContact SET "
	                +"idOrganization = '" + arrData[ctr]['idOrganization'] + "',"
					+"contactNumber = '" + arrData[ctr]['contactNumber'] + "',"
	                +"firstName = UPPER('" + arrData[ctr]['firstName'] + "'),"
	                +"lastName = UPPER('" + arrData[ctr]['lastName'] + "'),"
	                +"middleInitial = UPPER('" + arrData[ctr]['middleInitial'] + "'),"
	                +"nameAbbreviation = UPPER('" + arrData[ctr]['nameAbbreviation'] + "'),"
	                +"title = UPPER('" + arrData[ctr]['title'] + "'),"
	                +"statusCd = '" + arrData[ctr]['code'] + "',"
					+"rowUpdateDt = GETDATE(), "
	                +"rowUpdateUserid = '" + arrData[ctr]['rowUpdateUserid'] + "',"
	                +"rowDeleteFlag = '" + arrData[ctr]['rowDeleteFlag'] + "' "
	            	+"WHERE idOrganizationContact = '" + arrData[ctr]['idOrganizationContact'] + "'; "
                
                        +"UPDATE AppUser "
                        +"SET nameAbbreviation = oc.nameAbbreviation "
                        +"FROM AppUser a "
                        +"JOIN OrganizationContact oc ON a.idOrganizationContact = oc.idOrganizationContact "
                        +"WHERE a.idOrganizationContact = '" + arrData[ctr].idOrganizationContact + "'; ";
		    }
            UtilFunctions.execSql(sql, res);
        }else{
            res.status(400).json({"success":false,"data":"Invalid request body"});
        };

	}
}
