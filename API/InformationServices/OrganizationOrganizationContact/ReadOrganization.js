module.exports = {
	ReadOrganization: function(req,res){
		//console.log("Called ReadOrganization")
		var UtilFunctions = require('../../UtilFunctions.js');

		var sql = "SELECT idOrganization,"
					+"organizationName "
        			+"FROM Organization "
        			+"WHERE rowDeleteFlag = '' "
        			+"ORDER BY organizationName";
		
		UtilFunctions.execSql(sql, res)

	}
}