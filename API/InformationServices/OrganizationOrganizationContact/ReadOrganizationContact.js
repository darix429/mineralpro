module.exports = {
	ReadOrganizationContact: function(req,res){
		//console.log("Called ReadOrganizationContact")
		var UtilFunctions = require('../../UtilFunctions.js');
		var sql = "SELECT oc.idOrganizationContact, "
				+"oc.idOrganization, "
				+"o.organizationName, "
				+"oc.contactNumber, "
				+"oc.firstName, "
				+"oc.lastName, "
				+"oc.middleInitial, "
				+"oc.nameAbbreviation, "
				+"count(au.idAppUser) enableDeleteCount, "
				+"oc.title, "
				+"sc.code, "
				+"sc.description "
				+"FROM OrganizationContact oc "
				+"INNER JOIN Organization o on oc.idOrganization = o.idOrganization AND o.rowDeleteFlag = '' "
				+"LEFT JOIN AppUser au on au.idOrganizationContact = oc.idOrganizationContact AND au.rowDeleteFlag = '' "
				+" LEFT JOIN SystemCode sc ON sc.code = oc.statusCd AND sc.rowDeleteFlag= '' AND sc.codeType = 30 AND sc.appraisalYear ='" +req.query.appraisalYear+ "' "
				+"WHERE oc.rowDeleteFlag = '' "
				+"GROUP BY oc.idOrganizationContact, oc.idOrganization, o.organizationName, oc.contactNumber, oc.firstName, oc.lastName, oc.middleInitial, oc.nameAbbreviation, oc.title, sc.description, sc.code";
		
		UtilFunctions.execSql(sql, res)

	}
}
