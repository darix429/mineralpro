module.exports = {
	CreateOrganizationContact: function(req,res){
		//console.log("Called CreateOrganizationContact")
		var UtilFunctions = require('../../UtilFunctions.js');

		var createLength = req.query.createLength;
        var arrData = req.body;
        var sql = "";

        if(req.body){
            var arrData = req.body;
            if(req.query.createLength == 1){arrData = [req.body];}
            //console.log(arrData);
                
            for(var ctr=0; ctr<arrData.length; ctr++){
            	 sql += ""
						+"UPDATE OrganizationContact SET "
							+"idOrganization='" + arrData[ctr].idOrganization + "', "
							+"contactNumber='" + arrData[ctr].contactNumber + "', "
							+"firstName=UPPER('" + arrData[ctr].firstName + "'), "
							+"lastName=UPPER('" + arrData[ctr].lastName + "'), "
							+"middleInitial=UPPER('" + arrData[ctr].middleInitial + "'), "
							+"nameAbbreviation=UPPER('" + arrData[ctr].nameAbbreviation + "'), "
							+"title=UPPER('" + arrData[ctr].title + "'), "
							+"statusCd='" + arrData[ctr].statusCd + "', "
							+"rowUpdateUserid='" + arrData[ctr].rowUpdateUserid + "', "
							+"rowUpdateDt = GETDATE(), "
							+"rowDeleteFlag='' "
						+"WHERE "
							+"(contactNumber='" + arrData[ctr].contactNumber + "' "
							+"OR nameAbbreviation='" + arrData[ctr].nameAbbreviation + "') "
							+"AND rowDeleteFlag='D' "
						+"IF @@ROWCOUNT=0 "
						+"INSERT INTO OrganizationContact ("
							+"idOrganization,  "
							+"contactNumber, "
							+"firstName, "
							+"lastName, "
							+"middleInitial, "
							+"nameAbbreviation, "
							+"title, "
							+"statusCd, "
							+"rowUpdateUserid, "
							+"rowDeleteFlag "
						+")VALUES( "
							+"'" + arrData[ctr].idOrganization + "', "
							+"'" + arrData[ctr].contactNumber + "', "
							+"UPPER('" + arrData[ctr].firstName + "'), "
							+"UPPER('" + arrData[ctr].lastName + "'), "
							+"UPPER('" + arrData[ctr].middleInitial + "'), "
							+"UPPER('" + arrData[ctr].nameAbbreviation + "'), "
							+"UPPER('" + arrData[ctr].title + "'), "
							+"'" + arrData[ctr].code + "', "
							+"'" + arrData[ctr].rowUpdateUserid + "',   "
							+"'" + arrData[ctr].rowDeleteFlag + "'   "
						+")"; 
            }
            //console.log(sql)
            UtilFunctions.execSql(sql, res);
        }else{
            res.status(400).json({"success":false,"data":"No data received"});
        }
		
		//console.log("CreateOrganizationContact END")
	}
}