module.exports = {
	createReportExportState: function(req,res){
		//console.log("Called CreateOrganizationContact")
		var UtilFunctions = require('../../UtilFunctions.js');

		var createLength = req.query.createLength;
        var arrData = req.body;
        var sql = "";

        if(req.body){
            var arrData = req.body;
            if(req.query.createLength == 1){arrData = [req.body];}
            //console.log(arrData);
            for(var ctr=0; ctr<arrData.length; ctr++){
            	sql +=""
                    // +"UPDATE Report SET "
                    // +"description = '"+arrData[ctr].reportDesc+"', "
                    // +"reportTypeCdx = '"+arrData[ctr].reportType+"', "
                    // +"reportCategory = 'Report for CAD', "
                    // +"rowUpdateUserid = '"+arrData[ctr].rowUpdateUserid+"', "
                    // +"rowDeleteFlag = '"+arrData[ctr].rowDeleteFlag+"' "
                    // +"WHERE "
                    // +"appraisalYear = '"+req.query.appraisalYear+"'"
                    // +"IF @@ROWCOUNT = 0 "
                    +"INSERT INTO Report (reportId,"
                        +"appraisalYear,"
                        +"fileName,"
                        +"reportTypeCdx,"
                        +"reportCategoryCdx,"
                        +"description,"
                        +"displaySeqNumber,"
                        +"rowUpdateUserid,"
                        +"rowDeleteFlag) "
                    +"VALUES("
                        // +"(SELECT count(reportId)+1 FROM Report WHERE appraisalYear = '"+req.query.appraisalYear+"' ),"
                        +"'"+arrData[ctr].reportId+"',"
                        +"'"+req.query.appraisalYear+"',"
                        +"UPPER('"+arrData[ctr].fileName+".rpt'),"
                        +"'"+arrData[ctr].reportType+"',"
                        +"'Report to State',"
                        +"UPPER('"+arrData[ctr].reportDesc+"'),"
                        +"'"+arrData[ctr].displayOrder+"',"
                        +"'"+arrData[ctr].rowUpdateUserid+"',"
                        +"'"+arrData[ctr].rowDeleteFlag+"'"
                    +");"
            }
            // console.log(sql)
            UtilFunctions.execSql(sql, res);
        }else{
            res.status(400).json({"success":false,"data":"No data received"});
        }
		
		//console.log("CreateOrganizationContact END")
	}
}