module.exports = {
	updateReportExportState: function(req,res){
		//console.log("Called updateReportExportCAD")
		var UtilFunctions = require('../../UtilFunctions.js');
        
        if(req.body && req.query.updateLength){
            var arrData = req.body;
            if(req.query.updateLength == 1){arrData = [req.body];}
            
            var sql = "";     
            for (var ctr = 0; ctr < arrData.length; ctr++) {
                sql += "UPDATE Report SET "
                        +" reportTypeCdx = '" + arrData[ctr].reportType + "',"
                        +" fileName = UPPER('" + arrData[ctr].fileName + "'),"
                        +" reportCategoryCdx = 'Report to State',"
                        +" description = UPPER('" + arrData[ctr].reportDesc + "'),"
                        +" displaySeqNumber = '" + arrData[ctr].displayOrder + "',"
                        +" rowUpdateDt = GETDATE(), "
                        +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "'," 
                        +" rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "'"
                    +" WHERE reportId = '" + arrData[ctr].reportId + "' AND appraisalYear = '"+req.query.appraisalYear+"';";                  
            }
            UtilFunctions.execSql(sql, res);
        }else{
            res.status(400).json({"success":false,"data":"Invalid request body"});
        }
		
		//console.log("updateReportExportCAD END")
	}
}