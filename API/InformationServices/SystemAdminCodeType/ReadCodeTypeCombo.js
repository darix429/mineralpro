module.exports = {
    ReadCodeTypeCombo: function (req, res) {
        //console.log("Called SystemAdminCodeType")
        var UtilFunctions = require('../../UtilFunctions.js');

        var sql = "SELECT DISTINCT codeType FROM SystemCode "
                +"WHERE appraisalYear = '"+ req.query.appraisalYear + "' "
                +"ORDER BY codeType";
      
        UtilFunctions.execSql(sql, res)
    },//end ReadCodeType
};

