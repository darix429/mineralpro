module.exports = {
    CreateCodeType: function (req, res) {
        //console.log("Called CreateCodeType")
        var UtilFunctions = require('../../UtilFunctions.js');
        var createLength = req.query.createLength;
        var arrData = req.body;
        var sql = "";

        if(req.body){
            var arrData = req.body;
            if(req.query.createLength == 1){arrData = [req.body];}
            //console.log(arrData);
                
            for(var ctr=0; ctr<arrData.length; ctr++){
                sql += ""
                    +" UPDATE SystemCode SET "
                    +" shortDescription = UPPER('" + arrData[ctr].shortDescription + "')," 
                    +" description = UPPER('" + arrData[ctr].description + "')," 
                    +" activeInd = '" + arrData[ctr].activeInd + "'," 
                    +" staticInd = '" + arrData[ctr].staticInd + "',"
                    +" rowUpdateDt = GETDATE(), "
                    +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "', " 
                    +" rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "'"
                    +" WHERE codeType = '" + arrData[ctr].codeType + "' AND code = '" + arrData[ctr].code + "' AND appraisalYear = '" +req.query.appraisalYear+ "';"

                    +" IF @@ROWCOUNT=1 "

                    +" UPDATE SystemCodeType SET"
                    +" description = UPPER('" + arrData[ctr].sctDesc + "')," 
                    +" columnName = UPPER('" + arrData[ctr].columnName + "'),"
                    +" rowUpdateDt = GETDATE(), "
                    +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "  
                    //+" rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "'"
                    +" WHERE codeType = '" + arrData[ctr].codeType + "';"   

                    +" IF @@ROWCOUNT=0 "
                    +"BEGIN "
                    +"INSERT INTO SystemCode ( "
                    +" appraisalYear, "
                    +" codeType, "
                    +" code, "
                    +" shortDescription, "
                    +" description, "
                    +" activeInd, "
                    +" rowUpdateUserid, "
                    +" rowDeleteFlag, "
                    +" staticInd "
                    +" )VALUES( "
                    +" '"+req.query.appraisalYear+"', "
                    +" '"+arrData[ctr].codeType+"', "
                    +" '"+arrData[ctr].code+"', "
                    +" UPPER('"+arrData[ctr].shortDescription+"'), "
                    +" UPPER('"+arrData[ctr].description+"'), "
                    +" '"+arrData[ctr].activeInd+"', "
                    +" '"+arrData[ctr].rowUpdateUserid +"', "
                    +" '"+arrData[ctr].rowDeleteFlag +"', "
                    +" '"+arrData[ctr].staticInd+"' "
                    +" );"  

                    +" IF NOT EXISTS(SELECT top 1 codeType FROM SystemCodeType Where codeType = '"+arrData[ctr].codeType+"' AND rowDeleteFlag = '') "
                    +" BEGIN "
                    
                    +" INSERT INTO SystemCodeType ( "
                    +" codeType, "
                    +" description, "
                    +" rowUpdateUserid, "
                    +" rowDeleteFlag, "
                    +" columnName "
                    +" )VALUES( "
                    +" '"+arrData[ctr].codeType+"', "
                    +" UPPER('"+arrData[ctr].sctDesc+"'), "
                    +" '"+arrData[ctr].rowUpdateUserid +"', "
                    +" '"+arrData[ctr].rowDeleteFlag +"', "
                    +" UPPER('"+arrData[ctr].columnName+"') "
                    +" );"
                    +" END "   
                    +" END " 
            }       
            
//            res.send(sql)
            UtilFunctions.execSql(sql, res);
        }else{
            res.status(400).json({"success":false,"data":"No data received"});
        }

    },//end CreateCodeType
    
};