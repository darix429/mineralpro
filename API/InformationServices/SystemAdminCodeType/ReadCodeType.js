module.exports = {
    ReadCodeType: function (req, res) {
        //console.log("Called SystemAdminCodeType")
        var UtilFunctions = require('../../UtilFunctions.js');

        var sql = "SELECT sc.codeType,"
                    +"ISNULL(v.cnt, 0) enableDeleteCount,"
                    +"sc.code,"
                    +"sc.idSystemCode,"
                    +"sc.appraisalYear,"
                    +"sc.shortDescription,"
                    +"sc.description,"
                    +"sc.activeInd,"
                    +"sc.staticInd,"
                    +"sct.description sctDesc,"
                    +"sct.idSystemCodeType,"
                    +"sct.columnName,"
                    +"sct.tableNames "
                    +"FROM SystemCode sc "
                    +"INNER JOIN SystemCodeType sct ON sct.codeType = sc.codeType AND sct.rowDeleteFlag = ''"
                    +"LEFT JOIN vCodeTypeSummary v ON v.columnName = sct.columnName AND v.cdValue = sc.code AND v.appraisalYear = sc.appraisalYear "
                    +"WHERE sc.rowDeleteFlag = '' AND sc.appraisalYear = '"+ req.query.appraisalYear + "'";
        UtilFunctions.execSql(sql, res)
    },//end ReadCodeType
};

