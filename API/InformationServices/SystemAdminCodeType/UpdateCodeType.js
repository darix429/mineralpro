module.exports = {
    UpdateCodeType: function (req, res) {
        //console.log("Called UpdateAdminCodeType")
        var UtilFunctions = require('../../UtilFunctions.js');
        var updateLength = req.query.updateLength;
        var arrData = req.body;
        var sql = "";

        if(req.body && req.query.updateLength){
            var arrData = req.body;
            if(req.query.updateLength == 1){arrData = [req.body];}
            
            var sql = "";     
            for (var ctr = 0; ctr < arrData.length; ctr++) {
                 sql +=" UPDATE SystemCode SET "
                    +" appraisalYear = '" +arrData[ctr].appraisalYear+ "',"
                    +" codeType = '" + arrData[ctr].codeType + "',"
                    +" code = '" + arrData[ctr].code + "',"
                    +" shortDescription = UPPER('" + arrData[ctr].shortDescription + "')," 
                    +" description = UPPER('" + arrData[ctr].description + "')," 
                    +" activeInd = '" + arrData[ctr].activeInd + "'," 
                    +" staticInd = '" + arrData[ctr].staticInd + "',"
                    +" rowUpdateDt = GETDATE(), "
                    +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "', " 
                    +" rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "'"
                    +" WHERE idSystemCode = '" + arrData[ctr].idSystemCode + "';"

                    +" UPDATE SystemCodeType SET"
                    +" codeType = '" + arrData[ctr].codeType + "',"
                    +" description = UPPER('" + arrData[ctr].sctDesc + "')," 
                    +" columnName = UPPER('" + arrData[ctr].columnName + "'),"
                    +" rowUpdateDt = GETDATE(), "
                    +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "' "  
                    // +" rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "'"
                    +" WHERE idSystemCodeType = '" + arrData[ctr].idSystemCodeType + "';"   
            }
            //console.log(sql);
            UtilFunctions.execSql(sql, res);
        }else{
            res.status(400).json({"success":false,"data":"Invalid request body"});
        }
    }//end UpdateCodeType
};