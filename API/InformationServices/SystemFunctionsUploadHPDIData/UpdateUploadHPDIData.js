module.exports = {
    UpdateUploadHPDIData: function (req, res) {
        var UtilFunctions = require('../../UtilFunctions.js');
        
        if(req.body && req.query.updateLength){
            var arrData = req.body;
            if(req.query.updateLength == 1){arrData = [req.body];}
            
            var sql = "";  
            if(req.query.run){
                arrData = req.body;
                sql += "DECLARE @MSG AS VARCHAR(80) "

                +"SET @MSG = 'HPDI BULK INSERT AND LOAD TABLES - STARTED' "
                +"PRINT ' ' "
                +"PRINT @MSG "

                
                for (var ctr = 0; ctr < arrData.length; ctr++) {
                    var path = "C:\\MineralPro_v0.1\\hpdiFileUpload"
                    sql += ""
                        +"SET @MSG = 'BULK INSERT " +arrData[ctr].tableName+ "' "
                        +"PRINT ' ' "
                        +"PRINT @MSG "
                        +"Truncate table [dbo].[" +arrData[ctr].tableName+ "] "
                        +"BULK INSERT [dbo].[" +arrData[ctr].tableName+ "] "
                        +"FROM '"+path+'\\'+ arrData[ctr].csv +"' "
                        +"With (FORMATFILE = '"+path+'\\' + arrData[ctr].fileFormat + "',FirstRow = 2) "
                }
                sql += " "

                +"SET @MSG = 'EXEC spPROD_UPDATE = STARTED' "
                +"PRINT ' ' "
                +"PRINT @MSG "

                +"EXEC spPROD_UPDATE "

                +"SET @MSG = 'EXEC spPROD_UPDATE = ENDED' "
                +"PRINT ' ' "
                +"PRINT @MSG "

                +"SET @MSG = 'HPDI BULK INSERT AND LOAD TABLES - ENDED' "
                +"PRINT ' ' "
                +"PRINT @MSG ";
                

                //console.log(sql)
            }else{   
                for (var ctr = 0; ctr < arrData.length; ctr++) {
                    sql +=" UPDATE HPDI_file SET "
                        +" tableName = '" +arrData[ctr].tableName+ "',"
                        +" dataFileLocation = '" + arrData[ctr].csv + "',"
                        +" fileFormatLocation = '" + arrData[ctr].fileFormat + "',"
                        +" rowUpdateDt = GETDATE(), "
                        +" rowUpdateUserid = '" + arrData[ctr].rowUpdateUserid + "', "  
                        +" rowDeleteFlag = '" + arrData[ctr].rowDeleteFlag + "'"
                        +" WHERE idHPDI_file = '" + arrData[ctr].idHPDI_file + "';"   
                }
            }
               // console.log(sql);
            UtilFunctions.execSql(sql, res);
        }else{
            res.status(400).json({"success":false,"data":"Invalid request body"});
        }
        
    },
};