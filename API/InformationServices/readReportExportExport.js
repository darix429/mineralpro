module.exports = {
    read: function (req, res) {
        recordset = [{
                "displayName":"Export Public Records", 
                "url":"/api/InformationServices/ReportsExportsCADReports/ExportPublicRecords"
            },{
                "displayName":"Export Public Records Summary", 
                "url":"/api/InformationServices/ReportsExportsCADReports/ExportPublicRecordsSummary"
            },{
                "displayName":"Export PTD Submission", 
                "url":"/api/InformationServices/ReportsExportsCADReports/ExportPTDSubmission"
            },{
                "displayName":"Export PTD Submission Summary", 
                "url":"/api/InformationServices/ReportsExportsCADReports/ExportPTDSubmissionSummary"
            },{
                "displayName":"Export Certified Format", 
                "url":"/api/InformationServices/ReportsExportsCADReports/ExportCertifiedFormat"
            },{
                "displayName":"Export Certified Format Summary", 
                "url":"/api/InformationServices/ReportsExportsCADReports/ExportCertifiedFormatSummary"
            }];
        res.send("{'success': true, 'data': " + JSON.stringify(recordset) + "}")
    },//end readReportExports
};