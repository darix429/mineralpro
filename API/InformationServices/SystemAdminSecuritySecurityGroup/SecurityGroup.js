module.exports = function(app){

//---------------CRUD Listeners for SecurityUserList (use Get for read and Post for others))--------------
    app.get('/api/InformationService/SystemAdminSecuritySecurityGroup/ReadGroupList', function(req, res){ 
        var ReadGroupList = require('./ReadGroupList.js');   
        ReadGroupList.ReadGroupList(req, res);        
    });
    
    app.get('/api/InformationService/SystemAdminSecuritySecurityGroup/ReadSecurityGroup', function(req, res){ 
        var ReadSecurityGroup = require('./ReadSecurityGroup.js');   
        ReadSecurityGroup.ReadSecurityGroup(req, res);        
    });

    app.post('/api/InformationService/SystemAdminSecuritySecurityGroup/CreateGroupList', function(req, res){ 
        var CreateGroupList = require('./CreateGroupList.js');   
        CreateGroupList.CreateGroupList(req, res);        
    });
    
    app.post('/api/InformationService/SystemAdminSecuritySecurityGroup/UpdateGroupList', function(req, res){ 
        var UpdateGroupList = require('./UpdateGroupList.js');   
        UpdateGroupList.UpdateGroupList(req, res);        
    });
    
//----------------------------------End Listener for SecurityUserList-------------------------------------
}