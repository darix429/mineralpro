module.exports = {
    ReadGroupList: function (req, res) {
        //console.log("Called ReadGroupList")
        var UtilFunctions = require('../../UtilFunctions.js');
        if(req.query.sec){
            var SecurityGroup = req.query.sec
            var groupWhere = 'groupName';
            if (SecurityGroup == parseInt(SecurityGroup, 10)){
                groupWhere = 'idSecurityGroup';
            }                        
           var sql = "SELECT "
                    +" sg.idSecurityGroup, "
                    +" sg.groupName, "
                    +" sf.idFunctionSecurity, "
                    +" sf.securityCd, "
                    +" ssf.idSubSystemFunction, "
                    +" ssf.displayName functionNameDisplay, "
                    +" ss.idSubSystem, "
                    +" ss.displayName subSystemNameDisplay, "
                    +" a.appName "
                    +" FROM SecurityGroup sg "
                    +" INNER JOIN FunctionSecurity sf ON sg.idSecurityGroup = sf.idSecurityGroup AND sf.rowDeleteFlag = '' "
                    +" INNER JOIN SubSystemFunction ssf ON sf.idSubSystemFunction = ssf.idSubSystemFunction AND ssf.rowDeleteFlag = '' "
                    +" INNER JOIN SubSystem ss on ssf.idSubSystem = ss.idSubsystem AND ss.rowDeleteFlag = '' "
                    +" INNER JOIN AppSubSystem ass ON ss.idSubSystem = ass.idSubSystem AND ass.rowDeleteFlag = '' "
                    +" INNER JOIN App a on ass.idApp = a.idApp AND a.rowDeleteFlag = '' "
                    +" WHERE sg."+groupWhere+" = '"+SecurityGroup+"'"
                    +"     AND sg.rowDeleteFlag = ''"
                    +" ORDER BY sg.groupName, a.appName, ss.displayName, ssf.displayName;";                      
        }
        //res.send(groupWhere)
        //console.log(sql)
        UtilFunctions.execSql(sql, res)
    },//end ReadGroupList
};