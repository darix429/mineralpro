module.exports = {
    UpdateGroupList: function (req, res) {
        //console.log("Called UpdateGroupList")
        var UtilFunctions = require('../../UtilFunctions.js');
        var updateLength = req.query.updateLength;
        var sql = "";
        //console.log(req.body.groupName)
        if(updateLength){
            var arrData = req.body;
            if(req.query.updateLength == 1){arrData = [req.body];}

            for (var ctr = 0; ctr < arrData.length; ctr++) {
                sql += "UPDATE FunctionSecurity SET "
                +" securityCd = '"+arrData[ctr].securityCd+"', "
                +" rowUpdateDt = GETDATE(), "
                +" rowUpdateUserid = '"+arrData[ctr].rowUpdateUserid+"', "
                +" rowDeleteFlag = '"+arrData[ctr].rowDeleteFlag+"' "
                +" WHERE idFunctionSecurity = '"+arrData[ctr].idFunctionSecurity+"';";                  
            }

        }else{
            var SecurityGroup = req.body.groupName
            var groupWhere = 'groupName';
            if (SecurityGroup == parseInt(SecurityGroup, 10)){
                groupWhere = 'idSecurityGroup';
            }
            
            sql = "UPDATE FunctionSecurity SET "
                +" rowDeleteFlag = 'D' "
                +" WHERE idSecurityGroup = (SELECT idSecurityGroup  "
                +" FROM SecurityGroup where "+groupWhere+" = '"+SecurityGroup+"'); ";
        
            sql += "UPDATE UserOrgSecurity SET "
                +" rowDeleteFlag = 'D' "
                +" WHERE idSecurityGroup = (SELECT idSecurityGroup  "
                +" FROM SecurityGroup where "+groupWhere+" = '"+SecurityGroup+"'); ";     

            sql += "UPDATE dbo.SecurityGroup SET "
                +" rowDeleteFlag = 'D' "
                +" WHERE "+groupWhere+" = '"+SecurityGroup+"';";
        }
      
        //console.log(sql)
        UtilFunctions.execSql(sql, res);
    },//end UpdateGroupList
};