var UtilFunctions = require('../../UtilFunctions.js');
var fs = require('fs');
var downloader = require("request");
var randomstring = require("randomstring");

module.exports = {
	publicRecords: function(req,res){
		if(req.query.appraisal_year){
			var filename = "Public Records.txt";
			var path = randomstring.generate({length:5});
			var txt = "";
			var sql = "EXEC spExportPublicRecords "+req.query.appraisal_year;
			
            UtilFunctions.execSqlCallBack(sql, res, function(record){
				for(var i=0; i<record.length; i++){
					txt+=record[i].outputField+"\n";
				}
				fs.writeFile(path,txt,function(er){
					if(er) throw er;
					res.status(200).json({"path":path,"filename":filename});
				});			
			});
		}else{
            res.status(400).json({"success":false,"data":"Invalid request body"});
        }
	},
	publicRecordsSummary: function(req,res){
		if(req.query.appraisal_year){
			var filename = "Public Records Summary.txt";
			var path = randomstring.generate({length:5});
			var txt = "";
			var sql = "EXEC spExportPublicRecords_Summary "+req.query.appraisal_year;
			
            UtilFunctions.execSqlCallBack(sql, res, function(record){
				for(var i=0; i<record.length; i++){
					txt+=record[i].outputField+"\n";
				}
				fs.writeFile(path,txt,function(er){
					if(er) throw er;
					res.status(200).json({"path":path,"filename":filename});
				});			
			});
		}else{
            res.status(400).json({"success":false,"data":"Invalid request body"});
        }
	},
	PTDSubmission: function(req,res){
		if(req.query.appraisal_year){
			var filename = "PTD Submission.txt";
			var path = randomstring.generate({length:5});
			var txt = "";
			var sql = "EXEC spExportPDVSubmissionA "+req.query.appraisal_year;
			
            UtilFunctions.execSqlCallBack(sql, res, function(record){
				for(var i=0; i<record.length; i++){
					txt+=record[i].outputField+"\n";
				}
				fs.writeFile(path,txt,function(er){
					if(er){
						res.status(400).json({"success":false,"data":"write file failed"});
					}
					if(er) throw er;
					res.status(200).json({"path":path,"filename":filename});
				});			
			});
		}else{
            res.status(400).json({"success":false,"data":"Invalid request body"});
        }
	},
	PTDSubmissionSummary: function(req,res){
		if(req.query.appraisal_year){
			var filename = "PTD Submission Summary.txt";
			var path = randomstring.generate({length:5});
			var txt = "";
			var sql = "EXEC spExportPDVSubmession_Summary "+req.query.appraisal_year;
			
            UtilFunctions.execSqlCallBack(sql, res, function(record){
				for(var i=0; i<record.length; i++){
					txt+=record[i].outputField+"\n";
				}
				fs.writeFile(path,txt,function(er){
					if(er){
						res.status(400).json({"success":false,"data":"write file failed"});
					}
					if(er) throw er;
					res.status(200).json({"path":path,"filename":filename});
				});			
			});
		}else{
            res.status(400).json({"success":false,"data":"Invalid request body"});
        }
	},
	CertifiedFormat: function(req,res){
		if(req.query.appraisal_year){
			var filename = "Certified Format.txt";
			var path = randomstring.generate({length:5});
			var txt = "";
			var sql = "EXEC spExportCertifiedFormat "+req.query.appraisal_year;
			
            UtilFunctions.execSqlCallBack(sql, res, function(record){
				for(var i=0; i<record.length; i++){
					txt+=record[i].outputField+"\n";
				}
				fs.writeFile(path,txt,function(er){
					if(er){
						res.status(400).json({"success":false,"data":"write file failed"});
					}
					if(er) throw er;
					res.status(200).json({"path":path,"filename":filename});
				});			
			});
		}else{
            res.status(400).json({"success":false,"data":"Invalid request body"});
        }
	},
	CertifiedFormatSummary: function(req,res){
		if(req.query.appraisal_year){
			var filename = "Certified Format Summary.txt";
			var path = randomstring.generate({length:5});
			var txt = "";
			var sql = "EXEC spExportCertifiedFormat_Summary "+req.query.appraisal_year;
			
            UtilFunctions.execSqlCallBack(sql, res, function(record){
				for(var i=0; i<record.length; i++){
					txt+=record[i].outputField+"\n";
				}
				fs.writeFile(path,txt,function(er){
					if(er){
						res.status(400).json({"success":false,"data":"write file failed"});
					}
					if(er) throw er;
					res.status(200).json({"path":path,"filename":filename});
				});			
			});
		}else{
            res.status(400).json({"success":false,"data":"Invalid request body"});
        }
	},
	Download: function(req,res){
		res.download(req.params.path,req.params.filename,function(){
			fs.unlink(req.params.path,function(){
			});
		});
	}
}