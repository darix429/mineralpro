var express = require('express');
var compression = require('compression')
var bodyParser = require('body-parser');
var rootDir = __dirname;
var webDir = __dirname + '/web';
var prodDir = __dirname + '/prod';
var reportViewer = __dirname + '/CrystalReport';
var JEReport = __dirname + '/JEReport';

var app = express();
var server = require('http').Server(app);
var io = require('socket.io')(server);
var port = 8080;


app.use(compression());
//app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({
//  extended: true
//}));

app.use(bodyParser.json({limit: '100mb'}));
app.use(bodyParser.urlencoded({
    extended: true, 
    limit: '100mb'
}));

app.use('/CrystalReport/:id',express.static(reportViewer));
app.use('/dev', express.static(webDir));
app.use('/JEReport', express.static(JEReport));
app.use(express.static(prodDir));
io.sockets.on('connection', function (socket) {
	CLIENT = socket;
});
require('./API/Router.js')(app);

//app.use(function(req, res, next){
//	res.status(404);
//	res.redirect("/cphome");
//	res.end();
//	return;
//});

app.get('/uploadUI', function(req, res){
  //res.sendFile(path.join(__dirname, 'views/index.html'));
  res.writeHead(200, {'Content-Type': 'text/html'});
    res.write('<form action="/api/excel/upload" method="post" enctype="multipart/form-data">');
    res.write('<input type="file" name="filetoupload"><br>');
    res.write('<input type="submit">');
    res.write('</form>');
    return res.end();
});

server.listen(port);

server.timeout = 300000;

